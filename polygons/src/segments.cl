;;;
;;; segments
;;;
;;; contains functions that work with segments and polygons
;;;
;;;  1-May-2004 I. Kalet created from code in the contour editor
;;;

(in-package :polygons)

;;;-----------------------------------

(defun segments-overlap (a b x y p q)

  "segments-overlap a b x y p q 

Returns t iff any point of segment ((a,b), (x,y)) coincides with
any point of segment ((x,y), (p,q)) with the exception of the shared
vertex (x,y)."

  (declare (single-float a b x y p q))
  (and (collinear-p a b x y p q)
       (not (in-bounding-box a b x y p q))))

;;;----------------------------------

(defun segments-intersect (a b c d p q r s)

  "segments-intersect a b c d p q r s

Returns t iff the segments defined by ((a,b),(c,d)) and ((p,q),(r,s))
intersect, nil otherwise."

  ;; Algorithm: first, determine whether the two edges are parallel or
  ;; coincident to each other, or not.  If they're parallel or
  ;; coincident, determine which (if parallel, then they don't
  ;; intersect, so return nil; if coincident, they do intersect, so
  ;; return t).  If they're not parallel or coincident then find the
  ;; intersection point between the two lines running through the pair
  ;; of segments, and test to see if this intersection point actually
  ;; lies within each of the two segments.  If so, return t.  If not,
  ;; don't.

  (let* ((k       (- c a))
         (l       (- d b))
         (m       (- r p))
         (n       (- s q))
         (den     (- (* m l) (* n k))))
    (declare (single-float a b c d p q r s k l m n den))

    ;; if den is zero, the two lines have the same slope (dy/dx is the same
    ;; for both) -- so they must be parallel or coincident 
    (if (nearly-equal den 0.0)  

        ;; Below is true exactly when the lines are coincident and they
        ;; share some overlap.  The lines determined by the segments will
        ;; be coincident when they are collinear.  The segments will share
        ;; some overlap when one of the three cases is true:
        ;;    cd is between pq and rs
        ;;    ab is between pq and rs
        ;;    pq is between ab and cd
	(and (collinear-p a b c d p q)
	     (or (in-bounding-box p q c d r s)
		 (in-bounding-box p q a b r s)
		 (in-bounding-box a b p q c d)))

      ;; below is executed for segs that are neither // nor coincident
      (let* ((x (float (/ (+ (* k m q) (* m l a)
			     (- (* k m b)) (- (* k n p)))
			  den)))
             (y (float (/ (+ (* k n b) (* l n p)
			     (- (* l m q)) (- (* l n a))) 
			  (- den)))))
	(declare (single-float x y))
	;; is the point of intersection on both segs?
	(and (in-bounding-box a b x y c d)
             (in-bounding-box p q x y r s))))))

;;;-----------------------------------

(defun segment-crosses-polygon (segptr)

  "segment-crosses-polygon segptr

Segptr is assumed to be a pointer to a circular list of x y x y vertex
pairs, constituting a polygon.  Returns t iff the second segment
pointed to by segptr crosses or touches any other segment of the
polygon, excepting the first and third segments pointed to by segptr,
which share adjacent vertices with the second segment but may not
overlap with that segment - nil otherwise."

  ;; test first and second segments for overlap
  (when (segments-overlap (first segptr) (second segptr)
			  (third segptr) (fourth segptr)
			  (fifth segptr) (sixth segptr))
    (return-from segment-crosses-polygon t))

  ;; test second and third segments for overlap
  (setf segptr (cddr segptr))
  (when (segments-overlap (first segptr) (second segptr)
			  (third segptr) (fourth segptr)
			  (fifth segptr) (sixth segptr))
    (return-from segment-crosses-polygon t))

  ;; test rest of segments against second in loop
  (do ((next (nthcdr 4 segptr) (cddr next))
       (a (first segptr))
       (b (second segptr))
       (c (third segptr))
       (d (fourth segptr)))
      ((eq (cddr next) segptr))
    (when (segments-intersect a b c d 
			      (first next) (second next)
			      (third next) (fourth next))
      (return-from segment-crosses-polygon t)))

  nil)					; all clear if get this far

;;;------------------------------------

(defun simple-polygon (flattened-vertex-list)

  "simple-polygon flattened-vertex-list

Returns t iff none of the segments of the vertex list flattened-vertex-list
intersect or touch, excepting adjacent segments, which touch at their shared
vertices; nil otherwise.  Two adjacent segments which share any more
than a vertex is considered an intersection.  The segment connecting
the ends of flattened-vertex-list together is also explicitly tested."

  ;; Close a copy of flattened-vertex-list together and test each segment
  ;; against all other segments, allowing for the two segments adjacent to
  ;; the segment currently being tested to touch, but not to coincide.
  ;; When exiting, ...
  ;;
  (let ((verts (copy-list flattened-vertex-list)))
    (setf (cdr (last verts)) verts)
    (when (segment-crosses-polygon verts) ; test first segment outside loop
      (return-from simple-polygon nil))
    (do ((next (cddr verts) (cddr next)))
        ((eq next verts))
      (when (segment-crosses-polygon next)
        (return-from simple-polygon nil)))
    t))			;; return true if all segments check out

;;;------------------------------------
;;; End.
