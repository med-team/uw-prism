;;;
;;; math
;;;
;;; replication of some functions from prism misc module to make the
;;; polygons package independent.  Also includes package definition
;;; and related global information.
;;;
;;;  1-Apr-1993 I. Kalet created
;;; 07-Aug-1994 J. Unger add optional EPSILON param to NEAR so caller can
;;; specify the value for EPSILON.
;;;  8-Jan-1995 I. Kalet remove proclaim optimize form
;;;  1-Sep-1995 I. Kalet change macros to functions
;;;  1-Mar-1997 I. Kalet change keyword :epsilon to &optional
;;;  8-May-1997 BobGian add SQR; inline SQR, AVERAGE.
;;; 23-Jun-1997 BobGian add declaration for vars in NEARLY-EQUAL.
;;; 03-Jul-1997 BobGian move NEARLY-EQUAL, NEARLY-INCREASING, and
;;;  NEARLY-DECREASING from misc.cl to this file; they were duplicated there.
;;;  All are now in here and in the POLYGONS package.  (PRISM system now
;;;   explicitly depends on POLYGONS system.)  Updated all calls throughout
;;;   PRISM to use the new definitions.  Made optional EPSILON argument
;;;   explicit in arglist rather than using DEFCONSTANTed *EPSILON*
;;;   - this allows individual tuning.
;;;  Note that NEARLY-xxx all use a default EPSILON of 1.0e-5
;;;   while NEAR uses default EPSILON of 1.0e-4 at this time.
;;;  Also: NEARLY-xxx functions ALL take args as SINGLE-FLOAT only.
;;;  Rename NEAR -> NEAR-POINTS - change makes its name no longer a
;;;   very common substring - much easier to find using search/grep.
;;;  Add NEAR-COORDS with functionality identical to that of
;;;   NEAR-POINTS except that it takes args as 4 coordinate values
;;;   (pt1-x pt1-y pt2-x pt2-y) rather than as two points (sublists).
;;;   This makes it more like the NEARLY-xxx group and simplifies
;;;   argument destructuring in many places.
;;;  3-Oct-1997 BobGian remove AVERAGE, LO-HI-COMPARE - now expanded in-place.
;;; 27-Oct-1997 BobGian redefine SQR as macro to force compiler to inline it.
;;;  Allegro compiler does not obey INLINE decl for user-defined functions,
;;;  which is perfectly legal by CommonLisp spec.
;;; 30-May-2001 BobGian inline NEARLY-EQUAL in NEAR-COORDS, add THE decls.
;;; 20-Jun-2009 I. Kalet move defpackage and other globals here to be
;;; independent of the system def file.
;;;

(defpackage "POLYGONS"
  (:nicknames "POLY")
  (:use "COMMON-LISP")
  (:export "AREA-OF-POLYGON" "AREA-OF-TRIANGLE"
	   "BOUNDING-BOX"
	   "CANONICAL-CONTOUR" "CENTROID" "CLOCKWISE-TRAVERSAL-P"
	   "COLLINEAR-P" "CONTOUR-ENCLOSES-P" "CONVEX-HULL"
	   "IN-BOUNDING-BOX"
	   "NEARLY-EQUAL" "NEARLY-INCREASING" "NEARLY-DECREASING"
	   "ORTHO-EXPAND-CONTOUR"
	   "ROTATE-VERTICES"
	   "SCALE-CONTOUR" "SIMPLE-POLYGON"
	   "VERTEX-LIST-DIFFERENCE"
	   ))

;;;---------------------------------------------

(in-package :polygons)

(defconstant *pi-over-180* (coerce (/ pi 180.0) 'single-float))

;;;---------------------------------------------

(defun near-points (p1 p2 &optional (epsilon 1.0e-4))
  ;; Don't reduce the 1.0e-4 too much!

  "NEAR-POINTS p1 p2 &optional (epsilon 1.0e-4)

Returns T if the point P1 is within EPSILON of the point P2,
NIL otherwise.  EPSILON is an optional parameter and defaults to 1.0e-4.
All coordinates must be SINGLE-FLOAT."

  (near-coords (first p1) (second p1) (first p2) (second p2) epsilon))

;;;--------

(defun near-coords (p1x p1y p2x p2y &optional (epsilon 1.0e-4))
  ;; Don't reduce the 1.0e-4 too much!

  "near-coords p1x p1y p2x p2y &optional (epsilon 1.0e-4)

Returns T if the point (P1X P1Y) is within EPSILON of the point (P2X P2Y),
NIL otherwise.  EPSILON is an optional parameter and defaults to 1.0e-4.
All arguments must be SINGLE-FLOAT."

  (declare (single-float p1x p1y p2x p2y epsilon))

  (and (<= (- p1x p2x) epsilon)
       (<= (- p2x p1x) epsilon)
       (<= (- p1y p2y) epsilon)
       (<= (- p2y p1y) epsilon)))

;;;---------------------------------------------

(defun nearly-equal (this that &optional (epsilon 1.0e-5))

  "NEARLY-EQUAL this that &optional (epsilon 1.0e-5)

Returns T if THIS is within EPSILON of THAT, inclusive, NIL otherwise.
Note that the default EPSILON is arbitrary.  Your calculation may require
a coarser or finer grain.  Args all SINGLE-FLOAT."

  (declare (single-float this that epsilon))
  (and (<= (- this that) epsilon)
       (<= (- that this) epsilon)))

;;;------------------------------------------

(defun nearly-increasing (a b c &optional (epsilon 1.0e-5))

  "NEARLY-INCREASING a b c &optional (epsilon 1.0e-5)

Returns T if A, B, C form a nondecreasing sequence relaxed by EPSILON;
that is, if (<= (- A EPSILON) B (+ C EPSILON)) is true (and returns NIL
otherwise).  Note that the default EPSILON is arbitrary.  Your calculation
may require a coarser or finer grain.  Args all SINGLE-FLOAT."

  (declare (single-float a b c epsilon))
  (<= (- a epsilon) b (+ c epsilon)))

;;;------------------------------------------

(defun nearly-decreasing (a b c &optional (epsilon 1.0e-5))

  "NEARLY-DECREASING a b c &optional (epsilon 1.0e-5)

Returns T if A, B, C form a nonincreasing sequence relaxed by EPSILON;
that is, if (>= (+ A EPSILON) B (- C EPSILON)) is true (and returns NIL
otherwise).  Note that the default EPSILON is arbitrary.  Your calculation
may require a coarser or finer grain.  Args all SINGLE-FLOAT."

  (declare (single-float a b c epsilon))
  (>= (+ a epsilon) b (- c epsilon)))

;;;------------------------------------------

(defmacro sqr (x)

  "SQR x
Returns X squared (single-float in/out only)."

  (cond ((symbolp x)
	 ;; Simple case - can evaluate arg twice because it is a variable.
	 `(the single-float (* (the single-float ,x)
			       (the single-float ,x))))
	;;
	;; Slightly harder case - want to avoid double evaluation
	;; of argument form.
	(t (let ((var (gensym)))
	     `(let ((,var (the single-float ,x)))
		(the single-float
		  (* (the single-float ,var) (the single-float ,var))))))))

;;;---------------------------------------------
;;; End.
