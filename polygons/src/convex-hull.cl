;;;
;;; convex-hull
;;;
;;; A collection of routines to find the convex hull of an input list of 
;;; points.
;;; NEAR-COORDS is defined in math .
;;;
;;; 31-Mar-1993 J. Unger/I. Kalet from earlier version of ptvt files.
;;;   Included scale-contour here since it uses convex-hull but nothing
;;;   else.
;;; 23-Apr-1992 J. Unger fix bug in get-internal-point.
;;;  7-May-1997 BobGian changed (EXPT (some-form) 2) to (SQR (some-form))
;;;    in SCALE-CONTOUR.
;;; 24-Jun-1997 BobGian convert all instances of PI to
;;;    #.(coerce PI 'SINGLE-FLOAT) and ditto for (* 2.0 PI) --
;;;    got to keep all flonums in Prism as SINGLE-FLOATs.
;;; 03-Jul-1997 BobGian changed calls to NEAR to call NEAR-COORDS
;;;    with appropriate argument convention.
;;; 25-Aug-1997 BobGian changed #.(expression (coerce PI 'SINGLE-FLOAT))
;;;                          to #.(coerce (expression PI))
;;;  that is, do math in double-precision first and then coerce to
;;;  single-float at end, all inside read-time computation.
;;; 26-Sep-1997 BobGian replace ANGLE-MEASURE with ANGLE-SUBTENDED and
;;;  delete ANGLE-MEASURE - identical functionality, same package.
;;; 30-Sep-1997 BobGian destructure args to ANGLE-SUBTENDED (faster and less
;;;  garbage created).  Rename CENTER -> POLYCENTER (less easily confused).
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization, right margin).
;;; 

(in-package :polygons)

;;;----------
;;;
;;; Simple doubly-linked list manipulation functions.
;;;
;;;----------

;;;  Node construction routines.

(defun make-node (&optional data prev next) (list data prev next))

(defun data-node (cur)  (first cur))
(defun set-data-node (cur data)  (setf (first cur) data))
(defsetf data-node set-data-node)

(defun prev-node (cur)  (first (rest cur)))
(defun set-prev-node (cur prev)  (setf (first (rest cur)) prev))
(defsetf prev-node set-prev-node)

(defun next-node (cur)  (first (rest (rest cur))))
(defun set-next-node (cur next)  (setf (first (rest (rest cur))) next))
(defsetf next-node set-next-node)

;;;----------
;;; Cdll operations
;;;----------

(defun make-cdll-node (&optional data)                  ; returns a 1-elt cdll
  (let ((cdll (make-node data)))
    (setf (next-node cdll) cdll)
    (setf (prev-node cdll) cdll)
    cdll))

(defun insert-cdll-node (ins cur location)             ;; returns ins in place
  (case location
    (:before
      (setf (prev-node ins) (prev-node cur))
      (setf (next-node ins) cur)
      (setf (next-node (prev-node cur)) ins)
      (setf (prev-node cur) ins))
    (:after
      (setf (next-node ins) (next-node cur))
      (setf (prev-node ins) cur)            
      (setf (prev-node (next-node cur)) ins)
      (setf (next-node cur) ins))
    (t 
      (error "insert-cdll-node: location must be :before or :after. ~%")))
  ins)

(defun delete-cdll-node (cur)                           ; deletes cur -- won't
  (setf (next-node (prev-node cur)) (next-node cur))    ; handle 1-elt cdll
  (setf (prev-node (next-node cur)) (prev-node cur))
  (setf cur nil)
  (values))

(defun print-cdll (cdll &optional first-node)
  (unless (eq cdll first-node) 
    (format t "~a ~%" (data-node cdll))
    (print-cdll (next-node cdll) (or first-node cdll))))

;;;----------

(defun get-internal-point (pts)

  "GET-INTERNAL-POINT pts

  Returns a point internal to the polygon determined by the list of points,
  by returning the affine combination of three points on the list."

  ;; Note - the internal point can't coincide with any of the supplied pts,
  ;; or else the ANGLE-SUBTENDED comparison blows up when the program attempts
  ;; to sort that point by polar angle.  So we have to check the candidate
  ;; internal point against all points in the contour, and try another one,
  ;; if it turns out to be coincident with any of the supplied pts. 

  (loop
    (let* ((p1 (first pts))  
	   (p2 (second pts))  
	   (p3 (third pts))
	   (all-clear t)
	   (candidate-x (+ (/ (first p1) 3.0) 
			   (/ (first p2) 3.0) 
			   (/ (first p3) 3.0)))
	   (candidate-y (+ (/ (second p1) 3.0) 
			   (/ (second p2) 3.0) 
			   (/ (second p3) 3.0))))
      (dolist (pt pts)
	(when (near-coords (first pt) (second pt) candidate-x candidate-y)
	  (setf all-clear nil)))
      (when all-clear
	(return-from get-internal-point (list candidate-x candidate-y)))
      (setf pts (cdr pts)))))

;;;----------

(defun polar-smaller (q a b)

  "polar-smaller q a b

  Returns t if the polar angle of segment (a q) is smaller than that of
  (b q), both with respect to the positive x axis.  Otherwise, returns nil."

  (let ((q1 (first q))
	(q2 (second q)))
    (< (angle-subtended 100000.0 q2
			q1 q2
			(first a) (second a))
       (angle-subtended 100000.0 q2
			q1 q2
			(first b) (second b)))))

;;;----------

(defun list-to-cdll (lst)

  "list-to-cdll lst

  Makes and returns a circular doubly linked list from the simple list."

  (if (null (rest lst))
      (make-cdll-node (first lst))
      (insert-cdll-node 
	(make-cdll-node (first lst))
	(list-to-cdll (rest lst)) 
	:before)))

;;;----------

(defun get-rightmost-cdll-node (cdll &optional first-node)

  "get-rightmost-cdll-node cdll &optional first-node

  Returns the rightmost node (the one with the largest y coordinate) of 
  the input circular doubly linked list.  The optional argument is only
  to be used internally by recursive calls."

  (if (eq cdll first-node)
      first-node
      (let ((rt (get-rightmost-cdll-node
		  (next-node cdll) (or first-node cdll))))
	(if (> (first (data-node cdll)) (first (data-node rt)))
	    cdll
	    rt))))

;;;----------

(defun left-turn-p (a b c)

  "left-turn-p a b c

  Returns true if traversing the points a, b, c makes a left turn;
  nil otherwise."

  (let ((ax (first a))  (ay (second a))
	(bx (first b))  (by (second b))
	(cx (first c))  (cy (second c)))
    (plusp
      (- 
	(+ (* ax by) (* cx ay) (* bx cy))
	(+ (* cx by) (* ax cy) (* bx ay))))))

;;;----------

(defun cdll-to-list (cdll &optional first-node)

  "cdll-to-list cdll &optional first-node

  Makes and returns a simple list from a circular doubly linked list.
  The optional argument is only to be used internally by recursive calls."

  (unless (eq cdll first-node)
    (cons 
      (data-node cdll) 
      (cdll-to-list (next-node cdll) (or first-node cdll)))))

;;;----------

(defun convex-hull (pts)

  "convex-hull pts

  Given pts, a list of xy pairs, finds and returns a subset of this list
  which constitutes the convex hull of the points, using Graham's Scan."

  (let* ((pts2     (copy-tree pts))
	 (q        (get-internal-point pts2))
	 (sorted   (sort pts2 #'(lambda (a b) (polar-smaller q a b))))
	 (cdll     (list-to-cdll sorted))
	 (start    (get-rightmost-cdll-node cdll))
	 (v        start)
	 (w        (prev-node v))
	 (f        nil))

    (loop
      (when (and f (eq (next-node v) start)) (return))
      (when (eq (next-node v) w) (setq f t))
      (if (left-turn-p 
	    (data-node v) 
	    (data-node (next-node v)) 
	    (data-node (next-node (next-node v))))
	  (setq v (next-node v))
	  (progn
	    (delete-cdll-node (next-node v))
	    (setq v (prev-node v)))))

    (cdll-to-list start)))

;;;------------------

(defun scale-contour (vertices scale-factors) 

  "scale-contour vertices scale-factors

returns a list of vertices consisting of convex hull of original
contour list of vertices expanded out from the center of the contour
by the amount of the scale-factor in each direction.  Scale-factor is
a two element list whose first element is the scale factor in the x
direction and the second is the scale factor in the y direction.  The
center of the contour is computed by averaging the extrema of the
vertex coordinates."

  (let* ((verts (convex-hull vertices))
	 (ctr (polycenter verts))
	 (xc (first ctr))
	 (yc (second ctr))
	 (scale-x (first scale-factors))
	 (scale-y (second scale-factors))
	 )
    (mapcar #'(lambda (vertex)
		(let* ((x (first vertex))
		       (y (second vertex))
		       (r (sqrt (+ (sqr (- x xc)) (sqr (- y yc)))))
		       )
		  (list (+ x (* scale-x (/ (- x xc) r)))
			(+ y (* scale-y (/ (- y yc) r))))))
      verts)))

;;;--------------------
;;; End.
