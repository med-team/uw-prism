;;;
;;; contour-algebra
;;;
;;; provides contour-union and contour-differences and other related
;;; functions
;;;
;;;  4-Mar-1991 J. Unger write CONTOUR-DIFFERENCE routine and
;;; supporting code.
;;; 20-Aug-1991 J. Unger optimize critical parts of Weiler code.
;;; 20-Aug-1991 J. Unger add code to CONTOUR-DIFFERENCE to insure that all
;;;              contours are made CCW before being operated on.
;;; 25-Sep-1991 J. Unger work on contour-diff code: optimization mods,
;;;              enhance to return multiple contour pieces (if orig gets
;;;              split during subtraction), to return correct result in
;;;              all cases of "nonintersection", to handle partially
;;;              coincident contours, and fixed bug in sum-of-angles.
;;;             Changed CONTOUR-DIFFERENCE interface and added
;;;              VERTEX-LIST-DIFFERENCE routine to replace old CONTOUR-DIFF.
;;; 16-Jan-1992 J. Unger add contour union and intersection code.
;;; 19-Feb-1992 J. Unger fix CLOCKWISE-TRAVERSAL-P to handle polygons with
;;;              edges that fold back on themselves.
;;; 28-Feb-1992 J. Unger fix MAKE-NEAR-ANNULUS so it properly handles some
;;;              kinds of concave inner contours (in particular, ones which,
;;;              when intersected with a vertical line, partition the contour
;;;              into more than two pieces.
;;; 10-Mar-1992 J. Unger redo MAKE-NEAR-ANNULUS to conform to oncologists'
;;;              specification - 'connecting tube' width made nonzero and
;;;              function internals reworked.
;;; 19-May-1992 J. Unger enhance MAKE-NEAR-ANNULUS to avoid constructing
;;;              an annulus through an optionally supplied tumor contour
;;;              to VERTEX-LIST-DIFFERENCE.
;;; 30-Mar-1993 I. Kalet split off from old contour-functions, make
;;;              independent of prism etc. - still needs the NEARLY- stuff
;;;  6-May-1994 J. Unger modify EDGE-EDGE-INTERSECT to provide option
;;;              to always return the intersection point.  Also added
;;;              ORTHO-EXPAND-CONTOUR and CENTROID functions.
;;; 14-Jul-1994 J. Unger fix bug in ORTHO-EXPAND-CONTOUR (calls to
;;;              EDGE-EDGE-INTERSECT could return T - strip out the T's).
;;; 21-Jul-1994 J. Unger add bounding-box from prism package.
;;;  7-Aug-1994 J. Unger add REMOVE-ADJACENT-REDUNDANT-VERTICES here from
;;;              contour-editor module in prism package (formerly called
;;;              remove-repeats).  Add REMOVE-ADJACENT-COLLINEAR-VERTICES.
;;; 13-Sep-1994 J. Unger add AREA-OF-TRIANGLE and AREA-OF-POLYGON functions.
;;; 23-Sep-1994 J. Unger add PERIMETER-OF-POLYGON function.
;;;  1-Dec-1994 J. Jacky In REMOVE-ADJACENT-REDUNDANT-VERTICES, change
;;;              0.1 in NEAR to 0.03, try to solve 1 mm leaf creep
;;;  8-Jan-1995 I. Kalet remove proclaim form and make *pi-over-180*
;;;  local to the polygons package, not the geometry package.
;;;  1-Sep-1995 I. Kalet change some macros to functions
;;;  1-Mar-1997 I. Kalet change keyword :epsilon to &optional
;;;  6-May-1997 BobGian fix AREA-OF-TRIANGLE to return true area (had
;;;              returned double it) and fix AREA-OF-POLYGON correspondingly.
;;; 21-Jun-1997 BobGian convert miscellanous REVERSE -> NREVERSE
;;;              where safe (result of PUSH-building a list) - for efficiency.
;;;             Also standardize indentation, comments, linewidths, etc.
;;; 24-Jun-1997 BobGian convert all instances of PI to
;;;              #.(coerce PI 'SINGLE-FLOAT) and ditto for (* 2.0 PI) --
;;;              must keep all flonums in Prism as SINGLE-FLOATs.
;;;  2-Jul-1997 BobGian rewrite CLOCKWISE-TRAVERSAL-P with simpler algorithm.
;;;             Also exporte it in polygon-system.cl since it is used in
;;;              beam-dose calculations.
;;;             Replace *BIG* by its DEFCONSTANTed value - used only in two
;;;              places (VERTEX-IN-CONTOUR and GET-FAR-POINT) for apparently
;;;              different purposes; this way values can be optimized
;;;              independently.
;;;             Flush X-MAKE-NEAR-ANNULUS (apparent left-over cruft).
;;;             Rename *TUMOR* -> *TUMOR-CONTOUR* (consistent w comments).
;;;  3-Jul-1997 BobGian uniformize IN-BOUNDING-BOX and COLLINEAR - that
;;;              is, there were two functions of same name and similar
;;;              functionality but different argument conventions in
;;;              different packages (PRISM and POLYGONS).  Replace both
;;;              with single functions (defined here, in POLYGONS package),
;;;              used simpler arg convention and added optional EPSILON
;;;              arg with default value appropriate for each to be passed
;;;              to NEARLY-xxx functions within.  Update all calls to them.
;;;             For IN-BETWEEN do all of same except for collapsing two
;;;              versions into one - exists only in this file.
;;;             In EDGE-EDGE-INTERSECT - add opt arg EPSILON with default
;;;              value 1.0e-4 (same val as previously-used *EPSILON* const)
;;;              which is passed to internal NEARLY-xxx predicates.
;;;             In SCAN-FOR-COINCIDENT-SEGMENTS - replace previously-used
;;;              *EPSILON* by its value, resulting in scaled val of 1.0e-2 .
;;;  3-Jul-1997 BobGian change calls to NEAR to call NEAR-POINTS
;;;    or NEAR-COORDS with appropriate argument convention.
;;;  7-Jul-1997 BobGian add CANONICAL-CONTOUR to combine functionality
;;;              of REMOVE-ADJACENT-COLLINEAR-VERTICES (fix bug and rewrite)
;;;              and REMOVE-ADJACENT-REDUNDANT-VERTICES.
;;;  9-Jul-1997 BobGian change CANONICAL-CONTOUR to return NIL for a
;;;              degenerate contour: three collinear vertices or fewer
;;;              than three supplied - zero enclosed area in either case.
;;; 25-Jul-1997 BobGian fix two stupid bugs I introduced earlier in
;;;              CLOCKWISE-TRAVERSAL-P (symptoms: inf loop & wrong result).
;;; 25-Aug-1997 BobGian change #.(expression (coerce PI 'SINGLE-FLOAT))
;;;                         to #.(coerce (expression PI))
;;;    that is, do math in double-precision first and then coerce to
;;;    single-float at end, all inside read-time computation.
;;;  7-Sep-1997 BobGian place tests in CLOCKWISE-TRAVERSAL-P for correct
;;;    datatype/format in input vertex list [to track down persistent bug].
;;;    Also added fast tests for traversal direction in certain special cases.
;;; 23-Sep-1997 BobGian flush *TUMOR-CONTOUR* global - pass explicitly
;;;    from VERTEX-LIST-DIFFERENCE to MAKE-NEAR-ANNULUS.
;;; 30-Sep-1997 thru 14-Oct-1997 BobGian:
;;;   Destructure args to ANGLE-SUBTENDED (faster and less garbage created).
;;;   General reorganization - place defns in top-down order and grouped by
;;;     relatedness to aid readability.
;;;   Rename COLLINEAR -> COLLINEAR-P (CommonLisp predicate convention,
;;;     and far too many grep hits otherwise).
;;;   Move data-integrity test in CLOCKWISE-TRAVERSAL-P to separate
;;;     function: CHECK-CONTOUR, for debugging.  Bug now found, so fcn
;;;     left in file but commented out.  Related debug code removed.
;;;   Cleanup to REMOVE-ADJACENT-REDUNDANT-VERTICES.
;;;   Comment-out VERTEX-LIST-UNION and GET-UNION-CIRCUITS - nowhere used.
;;;   Rename CENTER -> POLYCENTER (less easily confused).  Also add decls,
;;;     inline-expand AVERAGE, LO-HI-COMPARE - simpler, tighter, more robust.
;;;   General cleanup and recoding of BUILD-CIRCUIT-LIST, BUILD-STRAND,
;;;     FIND-ALL, PERIMETER-OF-POLYGON, ORTHO-EXPAND-CONTOUR, GET-PIPE.
;;;     Inline GET-PIPE in MAKE-NEAR-ANNULUS and remove it.
;;;   PERTURB-SEGMENT: convert macro to function - works by side-effect on
;;;     first two arguments which are 2-lists, not by modifying parameter
;;;     directly, and thus can be factored out as a function with significant
;;;     savings of duplicated code.
;;;     Pass both lists to be modified as explicit args rather than as 1st
;;;     and 2nd items on single list - avoids need for wrap-around lists.
;;;   Improve (for later use) and comment-out (not currently used):
;;;     AREA-OF-TRIANGLE, AREA-OF-POLYGON, PERIMETER-OF-POLYGON.
;;;   Add fcn DE-ANNOTATE to undo effects of annotation spliced in by fcn
;;;     CONTOUR-CONTOUR-INTERSECT [which violates spec for vertex lists by
;;;     appending third element to coordinate lists - so splice it out here].
;;;   Speedups: Change some instances of EQUALP to EQ [to detect end of
;;;     traversal of circular chain of VERTEX objects] or to EQUAL [to detect
;;;     list equality (not EQness) in FIND-ALL and STRAND-EQUAL].
;;;   VERTEX-LIST-INTERSECTION: convert to predicate since that is only usage
;;;     here - original version saved (commented-out) in case of restorage.
;;;   Move CONTOUR-ENCLOSES-P to POLYGONS package, this file.  Replace former
;;;     VERTEX-IN-CONTOUR with it due to possibly incorrect operation
;;;     of former in certain case (ray to infinity tangent to contour vertex).
;;;     This also allows flushing of redundant CLOCKWISE-TRAVERSAL-P tests.
;;;   Fix CONTOUR-ENCLOSES-P so it returns NIL for point ON (not INSIDE)
;;;     the contour.
;;;   EDGE-EDGE-INTERSECT: wire epsilon value in code and make all args
;;;     required (opt args are major efficiency lossage in inner loop fcns);
;;;     change to return ONE vertex or NIL (not LIST of >= 1 vertices, and
;;;     not T for coincident segments).  Change interfaces with its callers.
;;;   Make SCAN-FOR-COINCIDENT-SEGMENTS and FIND-CONTOUR-INTERSECTIONS return
;;;     multiple values instead of list of several items.
;;;   EDGE-CONTOUR-INTERSECT: takes contour arg as unwrapped list - last
;;;     element not repeated - un-CDRed contour passed as additional arg so
;;;     we can find closing element (first on contour) when CDRing off end.
;;;   CONTOUR-CONTOUR-INTERSECT: same transformation applied to first arg.
;;;     Second arg is also an unwrapped contour, supplied only to be passed
;;;     to EDGE-CONTOUR-INTERSECT.
;;;   FOUND-INTERSECTIONS -> FOUND-INTERSECTIONS? .
;;;   VERTEX-LIST-DIFFERENCE: Insert test for contour orientation.
;;;  9-Nov-1997 BobGian improve CLOCKWISE-TRAVERSAL-P one more time.
;;;  8-Jan-1998 BobGian correct bad declaration in CLOCKWISE-TRAVERSAL-P.
;;; 22-Jan-1998 BobGian add declarations for speedup to CONTOUR-ENCLOSES-P.
;;; 26-Mar-1998 I. Kalet in ortho-expand-contour, check if
;;;   edge-edge-intersect returned nil before adding to result list.
;;; 22-May-1998 BobGian cosmetic tuneup to CLOCKWISE-TRAVERSAL-P.
;;; 01-Jun-1998 BobGian fix mistake in CONTOUR-ENCLOSES-P function --
;;;   missing expression in collinearity test.  Also fix error in Doc
;;;   string -- polarity of return value was mistakenly reversed.
;;; 03-Feb-2000 BobGian returned AREA-OF-TRIANGLE and AREA-OF-POLYGON to
;;;   active duty (exported and used in electron dosecalc); cosmetic fixes.
;;;   Change RETURN-FROM to RETURN where semantically equivalent.
;;; 11-May-2000 BobGian found another double-float PI - coerced to
;;;   single-float in ROTATE-VERTICES.
;;; 06-Sep-2000 BobGian fix ambiguous error messages in CLOCKWISE-TRAVERSAL-P.
;;; 30-May-2001 BobGian:
;;;   Wrap generic arithmetic with THE-declared types.
;;;   Wrap SQRT in THE declarations to allow inlining.
;;;   Inline AREA-OF-TRIANGLE in AREA-OF-POLYGON.
;;;   Comment-out AREA-OF-TRIANGLE (nowhere used).
;;;

;;;
;;;  Includes:
;;;
;;;  Weiler algorithm for determining contour difference.  The contours in
;;;  the published algorithms have been renamed circuits here to avoid
;;;  confusion with our own CLOS contour objects.  See the contour functions
;;;  implemention report for a general overview of the algorithm.
;;;
;;;  References:
;;;
;;; (1) J.D. Foley et al: Computer Graphics, 2nd Edition, pp 937-945
;;; (2) K. Weiler: "Polygon Comparison using a Graph Representation" in
;;;       SIGGRAPH '80, pp 10-18.
;;;
;;; See the comment in MAKE-NEAR-ANNULUS for modifying the 'tube width'.
;;;
;;; Future possible things to do:
;;;  o  optimize EDGE-EDGE-INTERSECT with bounding box tests
;;;
;;; All contours are represented as vertex lists which are OPEN: first element
;;; is NOT repeated as last, meaning the contour contains an implied edge from
;;; last vertex to first.  Most functions which need to CDR down such vertex
;;; lists take two args for the contour - the list being CDRed down, and the
;;; original (unCDRed) list to supply the first element (closing vertex of
;;; contour) when CDRing off the end of the CDRed list.

(in-package :polygons)

;;;--------------------

(defstruct circuit
  owner
  strand)

(defstruct vertex
  coords
  intersect-p
  owner
  next
  prev)

;; The strand field of a circuit is a pointer to a doubly linked list
;; of vertex structures (each vertex structure linked through its next
;; and prev fields).

;;;--------------------

(defun see-strand (s)

  "see-strand s

Prints the coordinates of each member of a strand on the screen.
Used for debugging."

  (let ((r s))
    (loop
      (format t "~S ~S ~S ~S ~S ~S ~S ~%"
	      (vertex-coords r)
	      (vertex-owner r)
	      (vertex-intersect-p r)
	      (vertex-coords (vertex-next r))
	      (vertex-owner (vertex-next r))
	      (vertex-coords (vertex-prev r))
	      (vertex-owner (vertex-prev r)))
      (cond ((eq (vertex-next r) s)
	     (return (values)))
	    (t (setq r (vertex-next r)))))))

;;;--------------------

(defun bounding-box (cntr)

  "bounding-box cntr

Given CNTR, a list of two-element vertices, returns list of two vertices,
the lower left corner and upper right corner of the bounding box."

  (if (null cntr) '((0.0 0.0) (0.0 0.0))
      (let ((xs (mapcar #'first cntr))
	    (ys (mapcar #'second cntr)))
	(list (list (apply #'min xs) (apply #'min ys))
	      (list (apply #'max xs) (apply #'max ys))))))

;;;--------------------

(defun collinear-p (v1x v1y ptx pty v2x v2y &optional (epsilon 1.0e-2))

  "collinear-p v1x v1y ptx pty v2x v2y &optional (epsilon 1.0e-2))

Returns T iff (V1X V1Y), (PTX PTY), and (V2X V2Y) are collinear
to within EPSILON."

  ;;  Note - if EPSILON is too small, then some triples of points which
  ;;         are truly collinear will not be detected as such.  At EPSILON
  ;;         = 1.0e-4, it's definitely too small.
  ;;
  ;; 3 points are collinear if cross-product of vector from 1st to 2nd and
  ;; vector from 1st to 3rd is "nearly" zero.  Rather than compare difference
  ;; of two quantities with zero, we compare two quantities with each other.
  ;; 1st is (V1X V1Y), 2nd is (PTX PTY), and 3rd is (V2X V2Y).

  (declare (single-float v1x v1y ptx pty v2x v2y epsilon))

  (< (- epsilon)
     (- (* (- ptx v1x) (- v2y v1y))
	(* (- pty v1y) (- v2x v1x)))
     epsilon))

;;;--------------------

(defun in-between (v1x v1y ptx pty v2x v2y &optional (epsilon 1.0e-5))

  "in-between v1x v1y ptx pty v2x v2y &optional (epsilon 1.0e-5)

Returns T if (PTX, PTY) is on the line segment determined by (V1X, V1Y)
and (V2X, V2Y), widened by EPSILON in both directions, NIL otherwise."

  ;; COLLINEAR-P and IN-BOUNDING-BOX use different EPSILONs.  Value used
  ;; here is IN-BOUNDING-BOX's value, which is scaled by 1000.0 for
  ;; use by COLLINEAR-P .  Reason for difference: one uses tolerance
  ;; for actual coordinate values while other uses it for comparing
  ;; cross-product to zero.  Cruft-up-the-wazoo here.

  (declare (single-float v1x v1y ptx pty v2x v2y epsilon))

  (and (collinear-p v1x v1y ptx pty v2x v2y (* epsilon 1.0e3))
       (in-bounding-box v1x v1y ptx pty v2x v2y epsilon)))

;;;--------------------

(defun in-bounding-box (a b x y c d &optional (epsilon 1.0e-5))

  "in-bounding-box a b x y c d &optional (epsilon 1.0e-5)

Returns T if (x, y) is in the bounding box determined by (a,b) and (c,d),
NIL otherwise."

  (declare (single-float a b x y c d epsilon))

  (and (or (nearly-increasing a x c epsilon)
	   (nearly-decreasing a x c epsilon))
       (or (nearly-increasing b y d epsilon)
	   (nearly-decreasing b y d epsilon))))

;;;--------------------

(defun canonical-contour (verts)

  "canonical-contour verts

Post-processes VERTS (a list of vertices representing a contour, with
implied wraparound from last to first) by removing adjacent redundant
vertices (those extremely close to each other) and vertices internal
to chains that comprise adjacent collinear segments.  If resultant
contour would enclose zero area or is otherwise invalid, returns NIL."

  (let ((output (remove-adjacent-collinear-vertices
		  (remove-adjacent-redundant-vertices verts nil))))
    ;;
    (cond ((null (cddr output))
	   ;; Degenerate case - interior vertices of collinear chain
	   ;; have been removed leaving only 2 vertices - or fewer than
	   ;; 3 vertices were supplied in the first place.  Return NIL
	   ;; to indicate degenerate (zero area) contour.
	   nil)
	  (t output))))

;;;--------------------

(defun remove-adjacent-redundant-vertices (sv internal?)

  "remove-adjacent-redundant-vertices sv internal?

Deletes all adjacent duplicate vertices of SV, including the endpoints.
The INTERNAL? parameter is T only during recursive calls."

  (cond (internal?
	  (cond ((cdr sv)                      ; need >= 2 vertices to compare
		 (let ((pt1 (car sv))
		       (rem (cdr sv)))
		   (loop
		     (cond ((and rem         ;Use 0.03 below, not 0.1 or 10e-4
				 (near-points pt1 (first rem) 0.03))
			    (setq rem (cdr rem)))
			   (t (return))))
		   (cons pt1 (remove-adjacent-redundant-vertices rem t))))
		(t sv)))
	;;
	;; Else, if called from outide, first call on the entire list,
	;; and then check explicitly front and end for duplicate points.
	;;
	(t (let ((result (remove-adjacent-redundant-vertices sv t)))
	     (if (and (cdr result)                  ;Length >= 2
		      (near-points (first result)
				   (car (last result))
				   0.03))         ;Same epsilon as used above.
		 ;; First point "near" last - throw away first point.
		 (cdr result)
		 ;; Otherwise return whole list.
		 result)))))

;;;----------------------------------

(defun remove-adjacent-collinear-vertices (verts)

  "remove-adjacent-collinear-vertices verts

Strips out vertices in VERTS found to lie in the middle of chains
of adjacent collinear points, with the endpoints treated as wrapping
around to the beginning again."

  (setq verts (remove-adjacent-collinear-vertices-int verts))
  (cond ((null (cdddr verts))
	 ;;Must be at least 4 vertices - if three or fewer are returned
	 ;;by REMOVE-ADJACENT-COLLINEAR-VERTICES-INT, we know they can't
	 ;;be collinear.
	 verts)
	;;
	(t (let* ((tail (nthcdr (- (length verts) 2) verts))
		  (s1 (first verts))
		  (s2 (second verts))
		  (e1 (first tail))
		  (e2 (second tail))
		  (s1x (first s1))
		  (s1y (second s1))
		  (s2x (first s2))
		  (s2y (second s2))
		  (e1x (first e1))
		  (e1y (second e1))
		  (e2x (first e2))
		  (e2y (second e2))
		  (collin-e1-e2-s1? (collinear-p e1x e1y e2x e2y s1x s1y))
		  (collin-e2-s1-s2? (collinear-p e2x e2y s1x s1y s2x s2y)))
	     (cond ((and collin-e1-e2-s1? collin-e2-s1-s2?)
		    ;; First two and last two collinear -
		    ;; trim first and last.
		    (butlast (cdr verts)))
		   (collin-e2-s1-s2?
		     ;; Last and first two collinear - trim first.
		     (cdr verts))
		   (collin-e1-e2-s1?
		     ;; Last two and first collinear - trim last.
		     (butlast verts))
		   (t verts))))))

;;;----

(defun remove-adjacent-collinear-vertices-int (verts)
  ;;
  ;; Utility used only by REMOVE-ADJACENT-COLLINEAR-VERTICES.
  ;; Removes interiors of collinear triples without end/beginning wrapping.
  ;;
  (cond ((null (cddr verts))                   ; < 3 verts can't be collinear.
	 ;; Return them all because inner recursive call is building
	 ;; partial result upward at this point.
	 verts)
	;;
	((let ((v1 (first verts))
	       (v2 (second verts))
	       (v3 (third verts)))
	   (collinear-p (first v1) (second v1)
			(first v2) (second v2)
			(first v3) (second v3)))
	 ;; First three collinear - remove middle one and try again
	 ;; without shifting along in list.
	 (remove-adjacent-collinear-vertices-int
	   (cons (first verts) (cddr verts))))
	;;
	;; Otherwise, shift by one and examine next set of three vertices.
	(t (cons (first verts)
		 (remove-adjacent-collinear-vertices-int (cdr verts))))))

;;;--------------------
;;; Error-check on validity of vertex-list inputs.
;;; Left here (commented-out) as debugging aid.

#+Ignore
(defun check-contour (vlist)

  (unless (and (consp vlist)                        ;Must be a list
	       (cddr vlist)                         ; of length at least 3
	       (dolist (vert vlist t)               ; and each vertex
		 (unless (and (consp vert)          ; must be a list
			      (= (length vert) 2)   ; of length 2
			      (dolist (num vert t)  ; and each element
				(unless (typep num 'single-float)  ; a s-float
				  (return nil))))
		   (return nil))))
    (error "CHECK-CONTOUR - Bad vertex-list: ~S" vlist))

  vlist)                               ;Pass-through for convenience of caller

;;;--------------------

(defun clockwise-traversal-p (vlist)

  "clockwise-traversal-p vlist

returns T if the vertex order in V-LIST (list of vertices, each a list
of X, Y coords) is a clockwise traversal; NIL if counter-clockwise."

  ;; Scan for leftmost vertex [minimal X-coordinate], saving both first and
  ;; last one found, which may be the same if leftmost vertex is unique.

  (unless (consp vlist)
    (error "CLOCKWISE-TRAVERSAL-P [1] Empty contour."))

  (do ((v1s vlist v2s)                      ;List starting with First of three
       (v2s (cdr vlist) v3s)                        ;List starting with Second
       (v3s (cddr vlist) (or (cdr v3s) vlist))      ;List st w Third
       (xmin #.most-positive-single-float)          ;Accum for min X coord
       (vprev)                            ;Vertex one back from first leftmost
       (vleft)                                    ;First leftmost vertex found
       (vnext)                             ;Vertex one fwd from first leftmost
       (test 0.0))                                  ;Local var for comparison
      ((and (consp vleft)                         ;Done when we have an answer
	    (eq v1s vlist))                ;and input recycles to beg of VLIST

       ;; After scan, VLEFT holds first leftmost vertex found.
       ;; Others with same X coord are ignored.
       (when (or (eq vleft vnext)
		 (eq vleft vprev))
	 (error "CLOCKWISE-TRAVERSAL-P [2] Duplicated vertex."))

       ;; Compare slope of line from VLEFT to VNEXT (slopeN) with slope
       ;; of line from VLEFT to VPREV (slopeP).
       ;;    either slopeN or slopeP infinite -> vertical line -> non-unique
       ;;      leftmost vertex -- determine orientation by comparing Y coords.
       ;;    slopeN = slopeP is impossible -- collinear triple,
       ;;    slopeN > slopeP -> contour is CW,
       ;;    slopeN < slopeP -> contour is CCW,
       (let ((vpx (first vprev))
	     (vpy (second vprev))
	     (vlx (first vleft))
	     (vly (second vleft))
	     (vnx (first vnext))
	     (vny (second vnext)))
	 (declare (single-float vpx vpy vlx vly vnx vny))
	 (cond
	   ((= vpx vlx)
	    (cond ((< vpy vly) t)                   ;CW traversal
		  ((> vpy vly) nil)                 ;CCW traversal
		  (t (error "CLOCKWISE-TRAVERSAL-P [3] Dup prev vertex."))))
	   ((= vlx vnx)
	    (cond ((< vly vny) t)                   ;CW traversal
		  ((> vly vny) nil)                 ;CCW traversal
		  (t (error "CLOCKWISE-TRAVERSAL-P [4] Dup next vertex."))))
	   (t (let ((slopeN (/ (- vny vly)
			       (- vnx vlx)))
		    (slopeP (/ (- vpy vly)
			       (- vpx vlx))))
		(declare (single-float slopeN slopeP))
		(cond ((> slopeN slopeP) t)         ;CW traversal
		      ((< slopeN slopeP) nil)       ;CCW traversal
		      (t (error "CLOCKWISE-TRAVERSAL-P [5] Collinear."))))))))
    ;;
    (declare (single-float test xmin))
    ;;
    ;; Find and track first instance of leftmost vertex so far.
    (when (< (setq test (caar v2s)) xmin)
      (setq xmin test
	    vprev (car v1s)
	    vleft (car v2s)
	    vnext (car v3s)))))

;;;--------------------
;;;
;;; There currently is no invariant in prism about clockwise/counterclockwise
;;; contours, so we need to insure that all contours input here are COUNTER
;;; clockwise, before doing anything with them.
;;;
;;; When Prism is finished, contours and polylines with redundant vertices
;;; will be screened out at the user-input phase.  At the moment, however,
;;; bad contours/polylines may be lurking around so we explicitly remove
;;; the redundant vertices before getting down to business.
;;;
;;;---------------------------------------------------------------------------
;;; There is a test for contour orientation (and reversal if not already CCW)
;;; here.  Since I am unsure if this is always required, and since it is NOT
;;; on inner calls, whether to do this test is controlled by an optional
;;; fourth argument.   - BobGian
;;;---------------------------------------------------------------------------
;;;

(defun vertex-list-difference (a1 a2 &optional a3 inputs-known-CCW?)

  "vertex-list-difference a1 a2 &optional a3 inputs-known-CCW?

Given two vertex lists A1 and A2, each a list of (x, y) pairs which
implicitly encloses a region of the plane, returns a list of vertex lists
which enclose the region of space that remains when A2 is subtracted from
A1.  NIL is returned if A1 lies completely within A2; an annulus shaped
vertex list is returned if A2 lies completely within A1.  More than one
vertex list may be on the returned list if the resulting difference consists
of several separate regions.  If an optional vertex list A3 is supplied,
then in the case that A1 lies completely within A2, attempts will be made to
prevent the resulting annulus vertex list from crossing A3.  This cannot be
guaranteed, though.  All input contours must normally be CCW, and they are
tested and reversed if necessary unless INPUTS-KNOWN-CCW? is non-NIL."

  ;; The last step of the 'preprocessing' is to perturb vertices of any
  ;; segments coincident to both contours, so that no such coincident
  ;; segments remain - this will make the merge step easier, should the
  ;; perturbed contours still intersect (see FIND-CONTOUR-INTERSECTIONS).
  ;;
  (let ((v1 (remove-redundant-vertices a1))
	(v2 (remove-redundant-vertices a2)))
    ;;
    ;; Setup ... Make sure all contours are traversed in CCW direction.
    ;; Don't bother if not necessary - this could be time-expensive for
    ;; inner calls.
    (unless inputs-known-CCW?
      (when (clockwise-traversal-p v1)
	(setq v1 (reverse v1)))
      (when (clockwise-traversal-p v2)
	(setq v2 (reverse v2)))
      (when (and (consp a3)
		 (clockwise-traversal-p a3))
	(setq a3 (reverse a3))))
    ;;
    (multiple-value-bind (isecs-1 isecs-2)
	(find-contour-intersections v1 v2)
      ;;
      (if (found-intersections? isecs-1)
	  ;;
	  ;; contours intersect - merge them, determine which of the merged
	  ;; circuits are external to both V1 and V2, extract contours from
	  ;; these circuits, and return on a list.
	  ;;
	  ;; BUILD-CIRCUIT-LIST "de-annotates" vertices in intersection lists.
	  ;;
	  (extract-contours
	    (get-difference-circuits
	      (determine-owners
		(merge-circuits
		  (build-circuit-list isecs-1 isecs-2)))))
	  ;;
	  ;; Contours don't intersect - use CONTOUR-ENCLOSES-P to test whether
	  ;; V1 is inside V2 (return NIL if so); else use test again to see if
	  ;; V2 is inside V1 (return near annulus if so); else two contours
	  ;; must be completely separate, so return V1 unchanged.  Note that
	  ;; a singleton list must be returned if result is not NIL, for
	  ;; consistency with above case.
	  ;;
	  ;; CAAR gets FIRST of FIRST - ie, X coord of first elem of contour.
	  ;; CADAR gets SECOND of FIRST - ie, Y coord of first elem of contour.
	  (cond ((contour-encloses-p isecs-2 (caar isecs-1) (cadar isecs-1))
		 nil)
		((contour-encloses-p isecs-1 (caar isecs-2) (cadar isecs-2))
		 (list (make-near-annulus (de-annotate isecs-1)
					  (de-annotate isecs-2)
					  a3)))
		(t (list v1)))))))

;;;--------------------

(defun get-difference-circuits (circuits)

  "get-difference-circuits circuits

Finds and returns the list of circuits that consists of vertices internal to
the first original contour and external to the second original contour,
by finding all occurrences of circuits owned by both X and A, and then
removing the duplicate circuits.  One circuit will be returned for each
distinct region of the difference.  If no such circuits exist, then it
must be the case that the two contours intersect but do not 'overlap';
this can occur if the two touch but do not share any common area (in
which case the circuit corresponding to the first contour is returned)
or if the first is completely inside the second, but touches the second
somewhere (in which case nil is returned)."

  (or

    ;; If there are any circuits owned by AX, return them.

    (remove-duplicates
      (append
	(find-all '(A X) circuits :key #'circuit-owner)
	(find-all '(X A) circuits :key #'circuit-owner))
      :test #'strand-equal
      :key #'circuit-strand)

    ;; Otherwise, if there are any circuits owned by B, this is an indication
    ;; that the 'A' contour lies outside of the 'B' contour, (and nothing
    ;; interrupts the 'B' contour line), so find and return the 'A' contour.

    (if (find-all '(B) circuits :key #'circuit-owner)
	(remove-duplicates
	  (find-all '(A) circuits :key #'circuit-owner)
	  :test #'strand-equal
	  :key #'circuit-strand)

	;; Otherwise, the 'A' contour lies inside the 'B' contour; return NIL.

	nil)))

;;;--------------------

(defun make-near-annulus (c1 c2 c3)

  "make-near-annulus c1 c2 c3

Returns a near annulus (a 'C' with a narrow opening) constructed from
C1 and C2, the former of which is assumed to completely enclose the
latter.  C3 is optional 3rd arg sent to VERTEX-LIST-DIFFERENCE.
Supplied contours must be counter-clockwise."

  ;; Algorithm: We will drill a pipe between the inner contour (C2) and
  ;; the outer one (C1) to connect the interior of C2 with the 'outside
  ;; world'.  When the implication is that C2 is a critical structure contour
  ;; and C1 is a target contour, care must be taken to insure that the pipe
  ;; does not pass through the tumor from which the target was originally
  ;; generated, if possible.  Thus, an effort will be made to direct the
  ;; pipe away from the tumor contour (C3).  This is done by determining
  ;; the centers of C2 and C3 and orienting the pipe on the line from the
  ;; center of C2 directly away from the center of C3.  Denote the centers
  ;; of C2 and C3 as P2 and P3 respectively.  A ray from P3 through P2 is
  ;; considered, and the furthest point of intersection between the ray
  ;; and C2 is determined (this point is on the 'outside surface' of C2).
  ;; This ray is actually the segment from P3 to PF, a point far away along
  ;; the ray.  Then find the closest point of intersection between C1 and
  ;; the ray pointing in the same direction but starting at V.  This point
  ;; of intersection is on the 'inside surface' of C1; denote it W.
  ;; Then define a thin vertical rectangular 'pipe' contour one end of which
  ;; extends from V slightly across C2 and the other extending from W slightly
  ;; across C1. The pipe is centered lengthwise about the VW segment.  Take
  ;; the VERTEX-LIST-DIFFERENCE of the pipe from C1, and then take the
  ;; VERTEX-LIST-DIFFERENCE of C2 from this result.  The second result
  ;; will be the desired annulus - the outer contour with the pipe and
  ;; inner contour removed from it

  ;; NOTE - There are some configurations of C1, C2, and C3 for which this
  ;; algorithm will NOT guarantee that the pipe does not pass through C3.
  ;; In particular, it is impossible if C2 lies entirely within C3.  If this
  ;; condition occurs, a warning is issued.  THERE ARE OTHER configurations
  ;; of the three contours as well, for which this problem occurs, which are
  ;; not checked.  If C3 snakes around C2 entirely, but does not actually
  ;; intersect it, for example.  It is assumed that the contours returned by
  ;; MAKE-NEAR-ANNULUS will be available for manual editing if this should be
  ;; necessary.

  ;; NOTE - C3 is obtained from the optional third argument to
  ;; VERTEX-LIST-DIFFERENCE.  It is NIL if there is no need to be
  ;; concerned about pipe-tumor intersections, in which case
  ;; an upward pointing pipe is created.

  (let* ((p2 (polycenter c2))
	 (p3 (if c3 (polycenter c3)
		 (list (first p2) (- (the single-float (second p2)) 1.0))))
	 (pf (get-far-point p3 p2))
	 (v (first (sort (edge-contour-intersect p3 pf c2 c2)
			 #'(lambda (a b)
			     (in-between (first p3) (second p3)
					 (first b) (second b)
					 (first a) (second a))))))
	 (w (first (sort (edge-contour-intersect v pf c1 c1)
			 #'(lambda (a b)
			     (in-between (first v) (second v)
					 (first a) (second a)
					 (first b) (second b))))))
	 ;;
	 (r1 (vertex-list-difference
	       c1
	       ;; This is the "pipe" of width 0.2, aligned lengthwise along the
	       ;; segment from V to W.  The 'tube width' can be adjusted by
	       ;; changing the constant 0.2 below.   - Jon Unger
	       ;; NB: The pipe is actually 0.4 wide, since we perturb each side
	       ;; 0.2 units from starting position in direction away from its
	       ;; opposite side.  - BobGian
	       (let ((v1 (copy-list v))    ;Copy all 4 vertices to be modified
		     (v2 (copy-list v))
		     (w1 (copy-list w))
		     (w2 (copy-list w)))
		 (perturb-segment v1 w1 0.2)       ;Perturb long sides outward
		 (perturb-segment w2 v2 0.2)
		 (perturb-segment w1 w2 0.2)      ;Perturb short sides outward
		 (perturb-segment v2 v1 0.2)
		 (list w1 w2 v2 v1))                ;CCW traversal around pipe
	       nil                                  ;Pipe already supplied.
	       t))                      ;Argument orientation already checked.
	 (r2 (vertex-list-difference (first r1) c2 nil t)))
    ;;
    (when c3
      (if (not (vertex-list-difference c2 c3 nil t))
	  (warn "A critical structure lies completely within the target.~%")
	  (when (vertex-list-intersection c2 c3)
	    (warn "A critical structure intersects the target.~%"))))
    ;;
    (first r2)))

;;;--------------------

(defun polycenter (poly)

  "polycenter poly

Returns a two element list, the x and y coordinate of the center of
the polygon poly.  The coordinates are determined only by finding the
min and max values in each axis and then taking the mid point between."

  (let ((min-x #.most-positive-single-float)
	(max-x #.most-negative-single-float)
	(min-y #.most-positive-single-float)
	(max-y #.most-negative-single-float))
    (declare (single-float min-x max-x min-y max-y))
    (dolist (vert poly)
      (let ((x (first vert))
	    (y (second vert)))
	(declare (single-float x y))
	(when (< x min-x)
	  (setq min-x x))
	(when (> x max-x)
	  (setq max-x x))
	(when (< y min-y)
	  (setq min-y y))
	(when (> y max-y)
	  (setq max-y y))))
    (list (* 0.5 (+ min-x max-x))
	  (* 0.5 (+ min-y max-y)))))

;;;--------------------

(defun get-far-point (p q)

  "get-far-point p q

Returns a point along the ray from p through q, very far away from both.
Used by MAKE-NEAR-ANNULUS."

  ;; NOTE - if 1.0e5 is too big (say 1.0e10) here, for unknown reasons
  ;; things screw up!  [Overflow, perhaps?]

  (let ((px (first p))
	(py (second p))
	(qx (first q))
	(qy (second q)))
    (list (+ px (* 1.0e5 (- qx px)))
	  (+ py (* 1.0e5 (- qy py))))))

;;;--------------------

#+Ignore                                ;Nowhere Used
(defun vertex-list-union (a1 a2)

  "vertex-list-union a1 a2

Given two vertex lists, A1 and A2, each a list of (x y) pairs which
implicitly encloses a region of the plane, returns a list of vertex
lists which enclose the region of space that results from the union of
the regions enclosed by A1 and A2.  If A1 and A2 share some overlap,
then this list consists of a single vertex list; otherwise, A1 and A2
are returned on the list unchanged.

NOTE that in the case where the union of two vertex lists results in a
vertex list with one or more holes in the middle, this algorithm will
return only the outermost vertex list and not the holes."

  ;; The last step of the 'preprocessing' is to perturb vertices of any
  ;; segments coincident to both contours, so that no such coincident
  ;; segments remain - this will make the merge step easier, should the
  ;; perturbed contours still intersect (see FIND-CONTOUR-INTERSECTIONS).
  ;;
  (let ((v1 (remove-redundant-vertices a1))
	(v2 (remove-redundant-vertices a2)))
    (multiple-value-bind (isecs-1 isecs-2)
	(find-contour-intersections v1 v2)
      ;;
      (if (found-intersections? isecs-1)
	  ;;
	  ;; contours intersect - merge them, determine which of the merged
	  ;; circuits are internal to V1 but external to V2, extract contours
	  ;; from these circuits and return on a list.
	  ;;
	  ;; BUILD-CIRCUIT-LIST "de-annotates" vertices in intersection lists.
	  ;;
	  (extract-contours
	    (get-union-circuits
	      (determine-owners
		(merge-circuits
		  (build-circuit-list isecs-1 isecs-2)))))
	  ;;
	  ;; Contours don't intersect - use CONTOUR-ENCLOSES-P to test whether
	  ;; V1 is inside V2 (return V2 if so); else use test again to see if
	  ;; V2 is inside V1 (return V1 if so); else two contours must be
	  ;; completely separate, so return them both unchanged.  Note that
	  ;; a singleton list must be returned if result is a single contour,
	  ;; for consistency with separate contours case.
	  ;;
	  ;; CAAR gets FIRST of FIRST - ie, X coord of first elem of contour.
	  ;; CADAR gets SECOND of FIRST - ie, Y coord of first elem of contour.
	  (cond ((contour-encloses-p isecs-2 (caar isecs-1) (cadar isecs-1))
		 (list v2))
		((contour-encloses-p isecs-1 (caar isecs-2) (cadar isecs-2))
		 (list v1))
		(t (list v1 v2)))))))

;;;--------------------

#+Ignore                                ;Nowhere Used
(defun get-union-circuits (circuits)

  "get-union-circuits circuits

Finds and returns the list of circuits that consists of vertices
internal either to the first or second original contours, by finding
all occurrences of circuits owned solely by X, and then removing the
duplicate circuits.  Under normal operation, the result should be
a single, distinct circuit when the two original contours overlap.

NOTE that if the union of the two original contours results in a
contour with holes in it, this routine will return only the outermost
contour and not the holes."

  ;; find any circuits owned solely by X.

  (let ((x-circs
	  (remove-duplicates
	    (find-all '(X) circuits :key #'circuit-owner)
	    :test #'strand-equal
	    :key #'circuit-strand)))

    ;; If there are more than one of these circuits, then what was output was
    ;; an outer contour with holes in it.  Figure out which are the holes
    ;; by picking a point from the first contour and testing to see if that
    ;; point is inside any of the other contours.  The outer contour is the
    ;; contour that it is inside, or if no points are inside, then the contour
    ;; from which the point was taken is the outer contour.  Return only the
    ;; the outer contour.  Otherwise, return the single circuit which was
    ;; generated.

    (if (> (length x-circs) 1)
	(let* ((extracted (extract-contours x-circs))
	       (vert (first (first extracted)))
	       (outer-index
		 (position t (mapcar #'(lambda (vlist)
					 (contour-encloses-p vlist
							     (first vert)
							     (second vert)))
			       (cdr extracted))))
	       (outer (if outer-index
			  (nth (1+ outer-index) x-circs) ; select from x-circs
			  (first x-circs))))        ; it was vert's contour
	  (list outer))
	x-circs)))

;;;--------------------
;;; This version is written as a predicate (returning only T/NIL) since
;;; that is the only usage of this function in the POLYGONS system (and
;;; this function is referenced nowhere else).

(defun vertex-list-intersection (a1 a2)

  "vertex-list-intersection a1 a2

Given two vertex lists, A1 and A2, each a list of (x y) pairs which
implicitly encloses a region of the plane, returns T if the regions of
space enclosed by the contours they represent intersect, NIL otherwise."

  ;; This version of VERTEX-LIST-INTERSECTION does NOT depend on the contours
  ;; being traversed in CCW orientation, but for consistency with other code
  ;; that condition SHOULD be true [ie, WILL be true modulo bugs].
  ;;
  (let ((v1 (remove-redundant-vertices a1))
	(v2 (remove-redundant-vertices a2)))
    (multiple-value-bind (isecs-1 isecs-2)
	(find-contour-intersections v1 v2)
      ;;
      (cond
	((found-intersections? isecs-1)
	 ;; Contours intersect, implying spatial regions also intersect.
	 t)
	;;
	;; Contours don't intersect - use CONTOUR-ENCLOSES-P to test if V1
	;; is enclosed by V2 or V2 is enclosed by V1 - Return T if so.
	;; CAAR gets FIRST of FIRST: X coord of contour first elem.
	;; CADAR gets SECOND of FIRST: Y coord of contour first elem.
	((or (contour-encloses-p isecs-2 (caar isecs-1) (cadar isecs-1))
	     (contour-encloses-p isecs-1 (caar isecs-2) (cadar isecs-2))))
	;; Otherwise contours must be completely separate, so return NIL.
	(t nil)))))

;;;--------------------
;;; This is the saved version of the function, in case we ever need to
;;; restore it to its original functionality (returning a list of contour
;;; intersections rather than being used only as a predicate).

#+Ignore
(defun vertex-list-intersection (a1 a2)

  "vertex-list-intersection a1 a2

Given two vertex lists, A1 and A2, each a list of (x y) pairs which
implicitly encloses a region of the plane, returns a list of vertex
lists which enclose the region of space that results from the
intersection of the regions enclosed by A1 and A2.  If A1 and A2 share
some overlap, this list will contain one vertex list for each separate
region of the intersection (there may be several); otherwise, NIL is
returned."

  ;; This version of VERTEX-LIST-INTERSECTION depends on the contours
  ;; being traversed in CCW orientation, which condition SHOULD be true
  ;; [ie, WILL be true modulo bugs].
  ;;
  ;; The last step of the 'preprocessing' is to perturb vertices of any
  ;; segments coincident to both contours, so that no such coincident
  ;; segments remain - this will make the merge step easier, should the
  ;; perturbed contours still intersect (see FIND-CONTOUR-INTERSECTIONS).
  ;;
  (let ((v1 (remove-redundant-vertices a1))
	(v2 (remove-redundant-vertices a2)))
    (multiple-value-bind (isecs-1 isecs-2)
	(find-contour-intersections v1 v2)
      ;;
      (if (found-intersections? isecs-1)
	  ;;
	  ;; Contours intersect - merge them, determine which of the merged
	  ;; circuits are internal to V1 but external to V2, extract contours
	  ;; from these circuits and return on a list.
	  ;;
	  ;; BUILD-CIRCUIT-LIST "de-annotates" vertices in intersection lists.
	  ;;
	  (extract-contours
	    (get-intersection-circuits
	      (determine-owners
		(merge-circuits
		  (build-circuit-list isecs-1 isecs-2)))))
	  ;;
	  ;; Contours don't intersect - use CONTOUR-ENCLOSES-P to see if V1 is
	  ;; inside V2 (return V1 if so); else use test again to see if V2 is
	  ;; inside V1 (return V2 if so); else two contours must be completely
	  ;; separate, so return nil.  Must return a singleton list if result
	  ;; is a single contour, for consistency with other cases.
	  ;;
	  ;; CAAR gets FIRST of FIRST - ie, X coord of first elem of contour.
	  ;; CADAR gets SECOND of FIRST - ie, Y coord of first elem of contour.
	  (cond ((contour-encloses-p isecs-2 (caar isecs-1) (cadar isecs-1))
		 (list v1))
		((contour-encloses-p isecs-1 (caar isecs-2) (cadar isecs-2))
		 (list v2))
		(t nil))))))

;;;--------------------

(defun get-intersection-circuits (circuits)

  "get-intersection-circuits circuits

Finds and returns the list of circuits that consists of vertices
internal both the first and second original contours, by finding all
occurrences of circuits owned by A and B, and then removing the
duplicate circuits.  One circuit will be returned for each distinct
region of the intersection.  If no such circuits exist, then it must
be the case that the two contours intersect but do not 'overlap'; this
can occur if the two touch but do not share any common area (in which
case nil is returned) or if the first is completely inside the second,
but touches the second somewhere (in which case the first is
returned), or vice versa (in which the second is returned)."

  ;; if there are any circuits owned by AB, return them.

  (or
    (remove-duplicates
      (append
	(find-all '(A B) circuits :key #'circuit-owner)
	(find-all '(B A) circuits :key #'circuit-owner))
      :test #'strand-equal
      :key #'circuit-strand)

    ;; otherwise, if there are any circuits owned by AX, this is an indication
    ;; that the 'B' contour lies within the 'A' contour, so find and return
    ;; the B contour.

    (if (or
	  (find-all '(A X) circuits :key #'circuit-owner)
	  (find-all '(X A) circuits :key #'circuit-owner))
	(remove-duplicates
	  (find-all '(B) circuits :key #'circuit-owner)
	  :test #'strand-equal
	  :key #'circuit-strand))


    ;; otherwise, if there are any circuits owned by BX, this is an indication
    ;; that the 'A' contour lies within the 'B' contour, so find and return
    ;; the A contour.

    (if (or
	  (find-all '(B X) circuits :key #'circuit-owner)
	  (find-all '(X B) circuits :key #'circuit-owner))
	(remove-duplicates
	  (find-all '(A) circuits :key #'circuit-owner)
	  :test #'strand-equal
	  :key #'circuit-strand))

    ;; otherwise, the 'A' contour lies outside the 'B' contour, so return nil.

    nil))

;;;--------------------

(defun remove-redundant-vertices (c)

  "remove-redundant-vertices c

Removes redundant (ie: two identical-within-EPSILON consecutive)
vertices from a contour."

  ;; NEAR-POINTS Uses default EPSILON of 1.0e-4 here.
  (remove-duplicates c :test #'near-points))

;;;--------------------

(defun find-contour-intersections (c1 c2)

  "find-contour-intersections c1 c2

Finds the points of intersection between the two supplied contours, marks
them, inserts them into the contours, and returns the two contours as a
list.  Each of the two returned contours is an \"annotated contour list\"
of which the member vertices are of the form (x y) if they are not
intersection points or (x y t) if they are.  For details on the intersec-
tion algorithm, see EDGE-EDGE-INTERSECT.  Note that care is taken here to
insure that there are no coincident segments found on the two intersection
lists -- if so, then the vertices of these two segments are perturbed so
that the segments no longer intersect, and all the intersections over the
two vertex lists are recalculated."

  (multiple-value-bind (perturbed? ilist-1 ilist-2)
      (scan-for-coincident-segments
	(contour-contour-intersect c1 c1 c2)
	(contour-contour-intersect c2 c2 c1))
    ;;
    ;; PERTURBED?, if T, means coincident segments were found and the
    ;; vertex intersection lists have been perturbed -- apply function
    ;; recursively to redetermine intersections.
    ;;
    (if perturbed?
	(find-contour-intersections ilist-1 ilist-2)
	(values ilist-1 ilist-2))))

;;;--------------------

(defun scan-for-coincident-segments (isec-1 isec-2 &aux perturb?)

  "scan-for-coincident-segments isec-1 isec-2

If any two consecutive vertices of both of the two augmented vertex lists
which comprise the intersecs list are themselves both intersection
vertices (marked with 'T'), then that segment is coincident to both vertex
lists.  To eliminate this special case of coincident segments, the
relevant vertices on one vertex list are slightly perturbed so they no
longer form a coincident segment.  Three values are returned: the first
item is a boolean to indicate whether any coincident segments were found.
The second and third items are (in the case that coincident segments were
found) the original two intersection lists but with vertices of coincident
segments PERTURBED, so that they are no longer coincident.  In the case
that no coincident segments were found, second and third items on the
returned list are the original two intersection lists unchanged."

  ;; Both args to this function are freshly-consed (at all levels).
  ;; Therefore, we need not worry about PERTURB-SEGMENTS causing side
  ;; effects to be propagated back through shared structure.
  ;;
  ;; Look for pairs of intersection vertices on ISEC-1.  Then look for the
  ;; same pairs of adjacent intersection vertices on ISEC-2 - for each
  ;; match, perturb 1st and 2nd vertices of pair on ISEC-2 by same amount.
  ;;
  (do ((w isec-1 (cdr w))
       (w1) (w2) (u1) (u2)
       (w1x 0.0) (w1y 0.0)
       (w2x 0.0) (w2y 0.0)
       (u1x 0.0) (u1y 0.0)
       (u2x 0.0) (u2y 0.0))
      ((null w))
    (declare (single-float w1x w1y w2x w2y u1x u1y u2x u2y))
    (setq w1 (first w)
	  w2 (or (second w) (first isec-1)))
    (when (and (third w1) (third w2))
      (do ((u isec-2 (cdr u)))
	  ((null u))
	(setq w1x (first w1)
	      w1y (second w1)
	      w2x (first w2)
	      w2y (second w2)
	      u1 (first u)
	      u1x (first u1)
	      u1y (second u1)
	      u2 (or (second u) (first isec-2))
	      u2x (first u2)
	      u2y (second u2))
	(when (and (or (near-coords w1x w1y u1x u1y)
		       (near-coords w2x w2y u1x u1y))
		   (or (near-coords w1x w1y u2x u2y)
		       (near-coords w2x w2y u2x u2y)))
	  (setq perturb? t)
	  (perturb-segment u1 u2 1.0e-2)))))
  ;;
  ;; if PERTURB? is true, send back a flag (the 't' at the head of what is
  ;; returned) to indicate that coincidences were found, followed by the two
  ;; lists resulting from the perturbation.
  ;;
  (if perturb?
      (values
	t
	;; De-Annotate the vertex lists being returned, since the
	;; perturbation hack should have eliminated coincidental edges.
	(de-annotate isec-1)
	(de-annotate isec-2))
      (values                                       ;Else,
	nil                                         ; return NIL (no perturbs)
	;; LEAVE any vertex annotations in place, for here they signal
	;; intersections between NON-COINCIDENTAL segments.
	isec-1                               ; + two vertex intersection lists
	isec-2)))

;;;--------------------

(defun contour-contour-intersect (c1-start c1 c2)

  "contour-contour-intersect c1-start c1 c2

Recursively finds the points of intersection between two contours and
returns a copy of the first contour (C1) with the intersection points
identified and spliced into the contour.  The second contour (C2)
is passed through unaffected.  C1-START is original C1 contour, not
CDRed on recursive calls, so we can wrap around to first element when
traversing past last element of C1."

  ;; I-LIST is the list of intersection points for the first edge of C1
  ;; and the contour C2, sorted in order of closest proximity to V1;
  ;; ANNOTS is the annotated vertex intersection list.
  ;;
  (when (consp c1)
    (let* ((v1 (first c1))
	   (v2 (or (second c1)             ;If C1 is at end, get "wrap-around"
		   (first c1-start)))      ;element from head of original list
	   (v1x (first v1))
	   (v1y (second v1))
	   (i-list (sort (edge-contour-intersect v1 v2 c2 c2)
			 #'(lambda (a b)
			     (in-between v1x v1y
					 (first a) (second a)
					 (first b) (second b)))))
	   ;; NB: The MAPCAR and APPEND (applied to its FIRST arg) guarantee
	   ;; that ANNOTS here is fully freshly-consed at all levels, safely
	   ;; prepared for ultimate user of this function's return list
	   ;; (such list may be modified destructively soon).
	   (annots (mapcar #'(lambda (v)
			       (append v '(t))) i-list)))
      ;;
      ;; If no intersections of edge (V1, V2) with contour C2 were found,
      ;; or if the first intersection point is far from V1, then push V1
      ;; onto the annotated vertex intersection list.  Finally, recur
      ;; on the CDR of C1 and all of C2.
      ;;
      (unless (and (consp i-list)
		   (near-points v1 (first i-list)))
	;; Make sure value pushed is freshly-consed, for reasons above.
	(push (copy-list v1) annots))
      ;;
      (append annots (contour-contour-intersect c1-start (cdr c1) c2)))))

;;;--------------------

(defun edge-contour-intersect (v1 v2 contour-start contour)

  "edge-contour-intersect v1 v2 contour-start contour

Recursively finds the points of intersection between an edge, determined
by the vertices V1 and V2, and a contour CONTOUR, and returns the intersection
points on a list.  CONTOUR-START is initial CONTOUR (not CDRed in recursive
calls) for finding closing first element when CDRing off end of CONTOUR."

  (when (consp contour)
    (let ((intersec (edge-edge-intersect v1 v2
					 (first contour)
					 (or (second contour)
					     (first contour-start))
					 nil)))
      (cond ((consp intersec)
	     (cons intersec
		   (edge-contour-intersect v1 v2 contour-start (cdr contour))))
	    (t (edge-contour-intersect v1 v2 contour-start (cdr contour)))))))

;;;--------------------
;;;
;;; Souped up for efficiency, since this is in the middle of a
;;; frequently executed loop.
;;;

(defun edge-edge-intersect (v1 v2 v3 v4 always-return-isec?)

  "edge-edge-intersect v1 v2 v3 v4 always-return-isec?

Finds the intersection between two edges, determined by the pair V1, V2 and
the pair V3, V4.  If they're parallel (and non-coincident), returns NIL.  If
coincident, then returns V3 when it lies between V1 & V2, V1 when it lies
between V3 & V4, and NIL otherwise.  If not parallel, then computes the
intersection point between the two lines running through the pair of
segments and tests to see if this intersection point actually lies within
each of the two segments.  If so, or if ALWAYS-RETURN-ISEC? is non-NIL,
returns that point and otherwise returns NIL."

  (let* ((a (first v1))
	 (b (second v1))
	 (c (first v2))
	 (d (second v2))
	 (p (first v3))
	 (q (second v3))
	 (r (first v4))
	 (s (second v4))
	 (K (- c a))
	 (L (- d b))
	 (M (- r p))
	 (N (- s q))
	 (den (- (* M L) (* N K))))

    ;; IN-BETWEEN, NEARLY-xxx, and NEAR-COORDS have different default
    ;; EPSILONs.  Values passed here are 1.0e-4 and 1.0e-5 - be careful.

    (declare (single-float a b c d p q r s K L M N den))

    (cond ((< -1.0e-4 den 1.0e-4)
	   ;;
	   ;; Parallel or coincident if true.  In the parallel case,
	   ;; implicitly return NIL regardless of ALWAYS-RETURN-ISEC?
	   ;;
	   (when (< -1.0e-4
		    (- (* M (- (* a L) (* b K)))
		       (* K (- (* p N) (* q M))))
		    1.0e-4)
	     (cond ((and (in-between a b p q c d 1.0e-5)
			 (not (near-coords p q c d 1.0e-4)))
		    v3)
		   ((and (in-between p q a b r s 1.0e-5)
			 (not (near-coords a b r s 1.0e-4)))
		    v1)
		   (t nil))))
	  ;;
	  ;; Otherwise, for segs neither parallel nor coincident ...
	  ;;
	  (t (let* ((x (/ (+ (* K M q)
			     (* M L a)
			     (- (* K M b))
			     (- (* K N p)))
			  den))
		    (y (/ (+ (* K N b)
			     (* L N p)
			     (- (* L M q))
			     (- (* L N a)))
			  (- den))))
	       (declare (single-float x y))
	       (cond ((or always-return-isec?
			  (and (in-bounding-box a b x y c d)
			       (in-bounding-box p q x y r s)
			       (not (near-coords x y c d 1.0e-4))
			       (not (near-coords x y r s 1.0e-4))))
		      ;;
		      ;; If flag is true, or the point of intersection is on
		      ;; both segs and not coincident with either V2 or V4,
		      ;; return the new intersection point.
		      ;;
		      (list x y))
		     ;;
		     ;; Otherwise return NIL.
		     (t nil)))))))

;;;--------------------

(defun found-intersections? (c)

  "found-intersections? c

Returns T if any intersections are on the annotated contour list C,
NIL otherwise."

  (dolist (v c nil)                                 ; None if finished loop.
    (when (third v)                                 ; Found intersec.
      (return t))))

;;;--------------------

(defun build-circuit-list (c1 c2)

  "build-circuit-list c1 c2

Given a pair of counter-clockwise contours, builds the list of circuit data
structures.  A reverse copy of each contour is made and the vertices in the
pair are assigned owners (a unique identifier for one contour, the symbol X
for its pair).  Each circuit is closed to form a loop and the four circuits
are returned on a list."

  ;; It is absolutely critical that the input contours be counter-clockwise;
  ;; otherwise the algorithm will return incorrect results.  Make sure that
  ;; outputs from FIND-CONTOUR-INTERSECTIONS are CCW.  Tests here can be
  ;; removed after correct operation verified.
  (when (or (clockwise-traversal-p c1)
	    (clockwise-traversal-p c2))
    (error "BUILD-CIRCUIT-LIST: Passed CW contour."))
  ;;
  (let ((c1-rev (reverse c1))
	(c2-rev (reverse c2)))
    (list
      (make-circuit :strand (build-strand c1-rev 'A))
      (make-circuit :strand (build-strand c1 'X))
      (make-circuit :strand (build-strand c2-rev 'B))
      (make-circuit :strand (build-strand c2 'X)))))

;;;--------------------

(defun build-strand (v-list owner)

  "build-strand v-list owner

Builds and returns a strand out of the list of annotated vertex pairs V-LIST.
Every vertex in the strand is owned by OWNER."

  ;; Fill in the COORDS, INTERSECT-P, and OWNER fields of each
  ;; vertex strand structure.

  (let ((strand (mapcar #'(lambda (elt)
			    (make-vertex
			      :coords (list (first elt) (second elt))
			      :intersect-p (third elt)
			      :owner owner))
		  v-list)))

    ;; Wire the vertex strand structures together via the next and prev
    ;; pointers.  For upcoming operations, the members of the strand will
    ;; be referenced via these pointers, and not by their 'top level'
    ;; list structure (a vestige from the v-list organization).

    (do ((w strand (cdr w)))
	((null (cdr w))
	 ;; (CAR W) is now the last element of STRAND [NOT (last strand) !!]
	 (setf (vertex-prev (first strand)) (car w))
	 (setf (vertex-next (car w)) (first strand)))
      ;;
      (setf (vertex-next (first w)) (second w))
      (setf (vertex-prev (second w)) (first w)))

    (first strand)))

;;;--------------------

(defun merge-circuits (circuits)

  "merge-circuits circuits

Merges the circuits together by re-wiring strands at intersections so that
the plane is partitioned into regions of ownership."

  ;; This is the key to and the most complex part of the entire contour
  ;; differencing mechanism.

  ;; Algorithm: from the list of 4 original circuits (A, X, B, and X), derive
  ;; an intersection list of 4-tuples ('v-lists'), one such tuple for each
  ;; intersection among the two contours.  Each member of a 4-tuple is a
  ;; pointer to that intersection as it is found on one the 4 original circuit
  ;; lists.  For each member of every 4-tuple, determine the successor strand
  ;; that is to be spliced into that strand at its head, replacing the current
  ;; 'rest' of the strand.  See FIND-SUCCESSOR-STRAND for the details on which
  ;; strand gets selected.  Then this successor strand is spliced into place,
  ;; and the resulting strand is made into a circuit and pushed onto the list
  ;; with the original 4 circuits.

  ;; Note - in certain cases, the 4-tuples above may be 3-tuples (when the
  ;; two contours touch at a point but do not cross each other).

  (dolist (4-tuple (find-circuit-intersections circuits) circuits)
    (let ((adj-list (get-adjacent-vertices 4-tuple))
	  (prev-links (mapcar #'vertex-prev 4-tuple)))
      (do ((vs 4-tuple (cdr vs))
	   (v-prevs prev-links (cdr v-prevs))
	   (v) (v-prev) (successor))
	  ((null vs))
	(setq v (car vs)
	      v-prev (car v-prevs)
	      successor (find-successor-strand v 4-tuple adj-list))
	(setf (vertex-next v-prev) successor)
	(setf (vertex-prev successor) v-prev)
	(push (make-circuit
		:strand (vertex-prev v))            ; add what's behind us
	      circuits)))))

;;;--------------------

(defun find-circuit-intersections (circuit-list)

  "find-circuit-intersections circuit-list

The circuit list consists of 4 sublists: the inside and outside
strands of both contours.  This routine finds the points of
intersection between the two original contours on each of the 4
sublists.  For each intersection point, the 4 occurrences of that
intersection point on the sublists are gathered together into a
4-tuple.  These occurrences are not removed from their respective
sublists, so the entirety of each sublist can still be accessed
through the VERTEX-NEXT and VERTEX-PREV fields of each of the 4 points
at the head of the tuple.  All such 4-tuples (one per intersection
point) are pushed onto a master list, which is returned."

  (let* ((v (circuit-strand (first circuit-list)))
	 (w v)
	 (result '()))
    (loop
      (when (vertex-intersect-p w)
	(push
	  (cons w (mapcar #'find-strand-intersections
		      (list w w w)
		    (cdr circuit-list)))
	  result))
      (cond ((eq (vertex-next w) v)
	     (return (nreverse result)))
	    (t (setq w (vertex-next w)))))))

;;;--------------------

(defun find-strand-intersections (v circuit)

  "find-strand-intersections v circuit

Finds the vertex v in the circuit and returns the found vertex structure.
Note that the vertices on the circuit before and after the found vertex
will still be accessible from the returned vertex structure through its
VERTEX-NEXT and VERTEX-PREV fields."

  (let ((w (circuit-strand circuit)))
    (loop
      (if (near-points (vertex-coords v) (vertex-coords w))
	  (return w)
	  (setf w (vertex-next w))))))

;;;--------------------

(defun get-adjacent-vertices (tuple)

  "get-adjacent-vertices tuple

Given the tuple of vertex structures (each vertex's coordinates
referencing the same intersection point, but on a different strand),
this routine returns a copy of the vertex structures immediately
preceeding and following each vertex (the copies returned on a list in
the same order as the vertices in the tuple list).  This original info
is needed since the neighbor info will change as the vertex is rewired
during the merge phase."

  (mapcar #'(lambda (v)
	      (list
		(copy-vertex (vertex-prev v))
		(copy-vertex (vertex-next v))))
    tuple))

;;;--------------------

(defun find-successor-strand (v tuple adj-list)

  "find-successor-strand v tuple adj-list

Given the intersection vertex structure v, the tuple of vertex
intersections from which v comes, and an adj-list which indicates the
original relationship between the next and previous neighbors of each
vertex in the tuple, this routine finds and returns the new vertex
structure (and implicitly its strand) that should follow v when the
vertices are later rewired into regions of ownership.  The strand that
should follow v is essentially the one whose VERTEX-NEXT element (as
determined by adj-list) subtends the smallest angle with v, as
measured from v's right."

  (do ((vs tuple (cdr vs))
       (adj-1 adj-list (cdr adj-1))
       (tuple-value))
      ((null vs))
    (when (eq v (car vs))
      (let ((prev-coords (vertex-coords (caar adj-1)))
	    (v-coords (vertex-coords v)))
	(do ((pc1 (first prev-coords))
	     (pc2 (second prev-coords))
	     (vc1 (first v-coords))
	     (vc2 (second v-coords))
	     (adj-2 adj-list (cdr adj-2))
	     (tuple-elements tuple (cdr tuple-elements))
	     (a-coords) (angle 0.0)
	     (min-angle #.most-positive-single-float))
	    ((null adj-2))
	  (declare (single-float pc1 pc2 vc1 vc2 angle min-angle))
	  (setq a-coords (vertex-coords (second (car adj-2))))
	  (when (< (setq angle (angle-subtended
				 pc1 pc2
				 vc1 vc2
				 (first a-coords) (second a-coords)))
		   min-angle)
	    (setq min-angle angle
		  tuple-value (car tuple-elements)))))
      (return tuple-value))))

;;;--------------------

(defun angle-subtended (p1 p2 q1 q2 r1 r2)

  "angle-subtended p1 p2 q1 q2 r1 r2

Determines the measure of the 'directed' angle (p1 p2) -> (q1 q2) -> (r1 r2),
that is, the counter-clockwise angle PQR, even if it is > 180 degrees."

  (declare (single-float p1 p2 q1 q2 r1 r2))

  (let* ((a      (- p1 q1))
	 (b      (- p2 q2))
	 (c      (- r1 q1))
	 (d      (- r2 q2))
	 (dot    (+ (* a c) (* b d)))
	 (cross  (- (* a d) (* b c)))
	 (len-ab (the (single-float 0.0 *)
		   (sqrt (the (single-float 0.0 *) (+ (* a a) (* b b))))))
	 (len-cd (the (single-float 0.0 *)
		   (sqrt (the (single-float 0.0 *) (+ (* c c) (* d d))))))
	 (quot   (/ dot (* len-ab len-cd)))
	 (ctheta (max (min quot 1.0) -1.0))         ; don't let round off
	 (theta  (acos ctheta)))                    ; throw us into imag

    (declare (single-float a b c d dot cross len-ab len-cd quot ctheta theta))

    ;; define 0 angles as 2 PI (first clause of the cond) so the algorithm
    ;; won't be confused by selecting the strand from the same original
    ;; contour but running in the opposite direction.

    (cond ((near-coords p1 p2 r1 r2)
	   #.(coerce (* 2.0d0 pi) 'single-float))
	  ((plusp cross) theta)
	  (t (- #.(coerce (* 2.0d0 pi) 'single-float) theta)))))

;;;--------------------

(defun determine-owners (circuits)

  "determine-owners circuits

Traverses each circuit and determines its composite owner."

  (when circuits
    (let* ((circuit  (first circuits))
	   (v        (circuit-strand circuit))
	   (front    v))
      (loop
	(pushnew (vertex-owner v) (circuit-owner circuit))

	(cond ((eq (vertex-next v) front)
	       (return (cons circuit (determine-owners (cdr circuits)))))
	      (t (setq v (vertex-next v))))))))

;;;--------------------

(defun extract-contours (circuits)

  "extract-contours circuits

Extracts and returns the vertices in the strand of each circuit as a list
of coordinate pairs."

  (mapcar #'(lambda (circuit)
	      (let* ((v (circuit-strand circuit))
		     (front v)
		     (result '()))
		(loop
		  (push (vertex-coords v) result)
		  (cond ((eq (vertex-next v) front)
			 (return (nreverse result)))
			(t (setq v (vertex-next v)))))))
    circuits))

;;;--------------------

(defun de-annotate (vlist)

  "de-annotate vlist

Returns a copy of VLIST in form as specified for a vertex-list, e.g.,
a list of (X Y) pairs with any third-element T values stripped off."

  (mapcar #'(lambda (vert)
	      (list (first vert) (second vert)))
    vlist))

;;;----------------------------------------------------

(defun contour-encloses-p (vlist px py)

  "contour-encloses-p vlist px py

Returns T if contour VLIST (an OPEN vertex list - first vertex NOT repeated
as last) encloses in either direction the point with coords (PX PY) - NIL if
point is ON contour (matches a vertex or on an edge) or is outside."

  ;; As VLIST is an open list representing a closed contour, there is an
  ;; implied edge present from last to first vertex.  Traversal can be in
  ;; either direction, CW or CCW.
  ;;
  (declare (single-float px py))
  ;;
  (let* ((bck-vert (car (last vlist)))
	 (bnx (- (the single-float (first bck-vert)) px))
	 (bny (- (the single-float (second bck-vert)) py))
	 (accum-angle 0.0))
    ;;
    (declare (single-float bnx bny accum-angle))
    ;;
    (do ((fwd-verts vlist (cdr fwd-verts))
	 (fwd-vert)                                 ;Actual Vertex
	 (fx 0.0) (fy 0.0)                       ;Rotating coords of FWD point
	 (bx bnx fx)                            ;Rotating X coord of BCK point
	 (by bny fy)                            ;Rotating Y coord of BCK point
	 (crossprod 0.0)
	 (dotprod 0.0))
	((null fwd-verts))
      ;;
      (declare (single-float bx by fx fy crossprod dotprod))
      ;;
      (setq fwd-vert (car fwd-verts)
	    fx (- (the single-float (first fwd-vert)) px)
	    fy (- (the single-float (second fwd-vert)) py))
      ;;
      ;; Check whether testpoint matches a contour vertex or lies
      ;; on one of the edges between vertices -- return NIL if so.
      (setq crossprod (- (* bx fy) (* by fx))
	    dotprod (+ (* bx fx) (* by fy)))
      ;;
      (when (or (and (= bx 0.0)                 ;Testpoint matches back vertex
		     (= by 0.0))
		(and (= fx 0.0)                  ;Testpoint matches fwd vertex
		     (= fy 0.0))
		(and (= 0.0 crossprod)      ;Testpoint on back/fwd-vertex edge
		     (< dotprod 0.0)))
	(return-from contour-encloses-p nil))
      ;;
      ;; Accumulate the included angle between vectors from testpoint
      ;; to contour line segment endpoints.  Included angle here is ARCTAN
      ;; of cross-product divided by dot-product of test-point-to-initial-end
      ;; vector and test-point-to-terminal-end vector.  Positive sign of
      ;; ACCUM-ANGLE indicates COUNTER-CLOCKWISE angle, negative sign a
      ;; clockwise angle.
      ;;
      (incf accum-angle (the single-float
			  (atan crossprod (+ (* bx fx)
					     (* by fy))))))
    ;;
    ;; Point (PX PY) is INSIDE contour if accumulated angle is 2*PI
    ;; [CCW enclosure] or -2*PI [CW enclosure].
    ;; Point (PX PY) is OUTSIDE contour if accumulated angle is zero.
    ;; Testing accumulated angle against threshold of +or- PI
    ;; [more negative than -PI or more positive than +PI] should leave
    ;; plenty of room for accumulated roundoff error.
    ;;
    (or (< accum-angle #.(coerce (- pi) 'single-float))
	(> accum-angle #.(coerce pi 'single-float)))))

;;;--------------------

(defun strand-equal (str1 str2)

  "strand-equal str1 str2

Returns T if the two strands consist of the same ordered list of vertices.
This is used to cull duplicates from the list of merged strands when
looking for strands owned by a particular combination of A, X, & B."

  ;; Get the two strands lined up, by running down STR1 with the pointer S,
  ;; setting STR1 to S if the first elt of S matches that of STR2, or returning
  ;; NIL if S advances all the way around the loop and matches STR1 again; or
  ;; advancing S otherwise.

  (let ((s (vertex-next str1)))
    (loop
      (cond ((and (equal (vertex-coords s) (vertex-coords str2))
		  (equal (vertex-owner s) (vertex-owner str2)))
	     (setq str1 s)
	     (return))
	    ((eq s str1)
	     (return-from strand-equal nil))
	    (t (setq s (vertex-next s))))))

  ;; March down both strands in step (with V and W), checking for equality
  ;; of vertices at each step.  If the vertices are ever not equal, return
  ;; NIL; if V loops all the way around to STR1, then check if W has looped
  ;; back to STR2, returning T if so and NIL if not.

  (let ((v (vertex-next str1))
	(w (vertex-next str2)))
    (loop
      (cond ((not (and (equal (vertex-coords v) (vertex-coords w))
		       (equal (vertex-owner v) (vertex-owner w))))
	     (return nil))
	    ((eq v str1)
	     (return (eq w str2)))
	    (t (setq v (vertex-next v)
		     w (vertex-next w)))))))

;;;--------------------

(defun find-all (item seq &key key)

  "find-all item seq &key key

Finds all occurrences of ITEM in SEQ and returns a list of those found
items in order."

  (let ((result (member item seq :test #'equal :key key)))
    (when (consp result)
      (cons (car result)
	    (find-all item (cdr result) :key key)))))

;;;--------------------

(defun centroid (con)

  "centroid con

Returns the centroid of contour con, ie: the point whose x & y coords are
the arithmetic average of the x & y coords, respectively, of the vertices."

  (list (float (/ (apply #'+ (mapcar #'first con)) (length con)))
	(float (/ (apply #'+ (mapcar #'second con)) (length con)))))

;;;--------------------

(defun ortho-expand-contour (con amt &aux (expanded-segs '()))

  "ortho-expand-contour con amt

Expands a contour, CON (a list of (x y) vertex pairs) by an amount, AMT,
in such a way that each segment of CON is translated outward in a direction
perpendicular to the segment by AMT."

  ;;  1. Ensure that the contour is counter-clockwise; reverse it if not.
  ;;
  (when (clockwise-traversal-p con)
    (setq con (reverse con)))
  ;;
  ;;  2. Perturb each successive pair of vertices by AMT.  This produces a
  ;;       list of expanded segments.  Each vertex of the old contour list
  ;;       will correspond to the endpoints of two successive pairs of
  ;;       expanded segments.
  ;;
  (do ((ptr con (cdr ptr))
       (vert-1) (vert-2))
      ((null ptr))
    ;; Copy vertices which PERTURB-SEGMENT will modify destructively.
    ;; The modified copies are then saved in EXPANDED-SEGS.
    (setq vert-1 (copy-list (first ptr))
	  vert-2 (copy-list (or (second ptr) (first con))))
    (perturb-segment vert-1 vert-2 amt)
    (push (list vert-1 vert-2) expanded-segs))
  ;;
  ;;  3. Find the intersection between each successive pair of segment
  ;;       endpoints.  These points of intersection are the vertices of
  ;;       the resulting expanded contour.
  ;;
  (do ((ptr expanded-segs (cdr ptr))
       (seg-1) (seg-2) (inter) (result '()))
      ((null ptr) result)
    (setq seg-1 (first ptr)
	  seg-2 (or (second ptr) (first expanded-segs))
	  inter (edge-edge-intersect
		  (first seg-1) (second seg-1)
		  (first seg-2) (second seg-2) t))
    (if inter (push inter result))))

;;;--------------------

(defun perturb-segment (a-vert b-vert amt)

  "perturb-segment a-vert b-vert amt

Perturbs A-VERT and B-VERT to the right (looking from A-VERT to B-VERT)
by AMT.  Works by side-effecting lists in first two arguments."

  ;; Motivation: we're looking for a point C such that (1) segment CB is
  ;; at right angles to AB, (2) C is to the right of AB (looking from B to A)
  ;; and (3) the length of CB is AMT.  If we take the cross product of AB and
  ;; some vector XB where XB points straight up out of the plane, we'll get
  ;; a resulting vector that satisfies (1) and (2).  Now if the length of
  ;; XB is AMT/LEN [LEN is length of vector (DX, DY) or segment AB], then the
  ;; area of the parallelogram determined by XB and AB will be AMT.  And the
  ;; magnitude of the cross product of two vectors is equal to the area of the
  ;; parallelogram they determine, so the magnitude of CB will also be equal
  ;; to AMT, the desired quantity.  So we calculate this general cross product
  ;; and add the x-component of the result to the x-components of A and B, and
  ;; likewise for the y-components, thus perturbing the two vertices A-VERT
  ;; and B-VERT by the appropriate quantity.

  (declare (single-float amt))
  (let* ((ax (first a-vert))
	 (ay (second a-vert))
	 (bx (first b-vert))
	 (by (second b-vert))
	 (dx  (- bx ax))
	 (dy  (- by ay))
	 (fac (/ amt (the (single-float 0.0 *)
		       (sqrt (the (single-float 0.0 *)
			       (+ (* dx dx) (* dy dy)))))))
	 (pdx (* fac dx))
	 (pdy (* fac dy)))
    (declare (single-float ax ay bx by dx dy fac pdx pdy))
    ;; Return results by modifying destructively the lists passed
    ;; as first two args here.
    (setf (first a-vert) (+ ax pdy))
    (setf (second a-vert) (- ay pdx))
    (setf (first b-vert) (+ bx pdy))
    (setf (second b-vert) (- by pdx))))

;;;---------------------------------------

(defun rotate-vertices (verts c-degrees)

  "rotate-vertices verts c-degrees

Rotates the vertices on VERTS, a list of (x y) pairs,
by the angle C-DEGREES in a counter-clockwise direction."

  (declare (single-float c-degrees))
  ;;
  (let* ((c-radians (* c-degrees #.(coerce (/ pi 180.0d0) 'single-float)))
	 (sin-c (sin c-radians))
	 (cos-c (cos c-radians)))
    (declare (single-float c-radians sin-c cos-c))
    (mapcar #'(lambda (v)
		(let ((vx (first v))
		      (vy (second v)))
		  (declare (single-float vx vy))
		  (list (- (* vx cos-c)
			   (* vy sin-c))
			(+ (* vy cos-c)
			   (* vx sin-c)))))
      verts)))

;;;---------------------------------------------
;;; The AREA-OF-TRIANGLE and AREA-OF-POLYGON functions are taken from
;;; "Computational Geometry in C" by Joseph O'Rourke, p 26.

#+Ignore
(defun area-of-triangle (a b c)

  "area-of-triangle a b c

Computes and returns the (oriented) area of the triangle determined
by three points a, b, and c -- each point is an (x y) coordinate pair."

  (let ((ax (first a))
	(ay (second a))
	(bx (first b))
	(by (second b))
	(cx (first c))
	(cy (second c)))
    (declare (single-float ax ay bx by cx cy))
    (* (- (+ (* ax by)
	     (* ay cx)
	     (* bx cy))
	  (+ (* ay bx)
	     (* ax cy)
	     (* cx by)))
       0.5)))

;;;---------------------------------------------

(defun area-of-polygon (poly &aux (vert (first poly)) (ax (first vert))
			(ay (second vert)) (bx 0.0) (by 0.0) (sum 0.0)
			(cx 0.0) (cy 0.0))

  "area-of-polygon poly

Computes and returns the (non-oriented) area of polygon poly,
a list of (x y) pairs."

  (declare (type list poly vert)
	   (single-float sum ax ay bx by cx cy))

  (setq poly (cdr poly)
	vert (car poly)
	bx (first vert)
	by (second vert)
	poly (cdr poly))

  (loop

    (setq vert (car poly)
	  cx (first vert)
	  cy (second vert))

    (incf sum (- (+ (* ax by)
		    (* ay cx)
		    (* bx cy))
		 (+ (* ay bx)
		    (* ax cy)
		    (* cx by))))

    (cond ((consp (setq poly (cdr poly)))
	   (setq bx cx by cy))
	  (t (return (cond ((> sum 0.0)             ;Inline absolute value.
			    (* 0.5 sum))
			   (t (* -0.5 sum))))))))

;;;---------------------------------------------

#+Ignore
(defun perimeter-of-polygon (poly)

  "perimeter-of-polygon poly

Computes and returns the perimeter of polygon POLY, a list of (x y) pairs."

  (do ((sum 0.0)
       (p poly (cdr p))
       (A) (B) (dx 0.0) (dy 0.0))
      ((null p)
       sum)
    (declare (single-float sum dx dy))
    (setq A (first p)
	  B (or (second p) (first poly))
	  dx (- (the single-float (first A))
		(the single-float (first B)))
	  dy (- (the single-float (second A))
		(the single-float (second B))))
    (incf sum (the (single-float 0.0 *)
		(sqrt (the (single-float 0.0 *)
			(+ (* dx dx)
			   (* dy dy))))))))

;;;---------------------------------------------
;;; End.
