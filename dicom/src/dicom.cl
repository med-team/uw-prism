;;;
;;; dicom - contains package definition and common globals
;;;
;;; 20-Jun-2009 I. Kalet created from dicom-common.system
;;; 16-Sep-2009 I. Kalet add requires to avoid autoloading in
;;; standalone system with ACL.
;;;  5-Oct-2009 I. Kalet add streama to requires.
;;; 18-Jul-2011 I. Kalet move export for dicom package here from
;;; wrapper-client.
;;;

;;;=============================================================

#+allegro
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :acldns) ;; needed for network connections
  (require :ssl)) ;; and encryption

#+allegro
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :streama) ;; testing shows this is needed
  (require :streamc)) ;; also needed for extended stream I/O

;;;=============================================================
;;; Package definitions.

(defpackage :dicom
  (:use :common-lisp)
  (:export "RUN-CLIENT"))

(defpackage :prism
  (:nicknames "PR")
  (:use :common-lisp)
  (:export "ACQ-DATE" "ACQ-TIME"
	   "CONTOUR" "CONTOURS"
	   "DESCRIPTION" "DISPLAY-COLOR"
	   "HOSP-NAME"
	   "ID" "IMAGE-2D" "IMAGE-SET-ID" "IMG-TYPE"
	   "NAME"
	   "ORGAN" "ORIGIN"
	   "PATIENT-ID" "PAT-POS" "PIX-PER-CM" "PIXELS"
	   "RANGE"
	   "SCANNER-TYPE" "SIZE"
	   "TARGET" "THICKNESS" "TUMOR"
	   "UID" "UNITS"
	   "VERTICES"
	   "X-ORIENT"
	   "Y-ORIENT"
	   "Z"
	   ))

(in-package :dicom)

;;;=============================================================
;;; System Constants -- not user-configurable.

;;; TCP-Buffer must be larger than max PDU datafield size by enough to hold
;;; all the PDU bytes outside the datafield or message plus any shifted bytes
;;; left over from parsing a prior fragment.
(defconstant PDU-Bufsize 65536)                     ;Max PDU DataField Length

;;; Include 1 KB for leeway.
(defconstant TCP-Bufsize (+ PDU-Bufsize PDU-Bufsize 1024))

;;;=============================================================
;;; Dicom Upper Layer State Variables.
;;; These special variables define the DUL state and are all bound on server
;;; connection acceptance or client invocation so PDS can stack state and run
;;; a client as a subsystem of the server.

(defvar *mode* nil)            ; :Client or :Server - on per-association basis
(defvar *state* nil)                                ;Current state as symbol
(defvar *event* nil)                               ;Activating event as symbol
(defvar *args* nil)                    ;Communication between action functions
(defvar *remote-IP-string* nil)     ;Remembered far-end IP Address for logging

(defvar *calling-AE-name* nil)                 ;Remembered AE name for logging
(defvar *called-AE-name* nil)                  ;Remembered AE name for logging
(defvar *SOP-class-name* nil)                ;Remembered SOP class for logging

(defvar *max-datafield-len* nil)                    ;Max size to use for PDU

(defvar *status-code* nil)                 ;NIL or fixnum - reported by client
(defvar *status-message* nil)              ;NIL or string - reported by client

;;; Continuation object for PARSE-OBJECT in case it must be suspended
;;; and restarted due to PDU fragmentation during parse of an object.
(defvar *parser-state* nil)

;;; Stream Client opens to Server.  Used in common code, so must
;;; be declared in common and bound by both Client and Server.
(defvar *connection-strm* nil)

;;; Common SSL variables
(defvar *use-ssl* nil)
(defvar *ssl-port* 2762)
(defvar *certificate* "/radonc/prism/cacert.pem")
(defvar *private-key* "/radonc/prism/privkey.pem")

;;; Stores environment checkpointed at start of cmd execution for restoration
;;; at end [to prevent environment overgrowth on successive commands].
(defvar *checkpointed-environment* nil)

;;; PDU end index passed to REPORT-ERROR to bracket relevant portion
;;; of TCP buffer for error reporting.  Start index is always zero.
(defvar *PDU-tail* 0)

;;; Handle on parsed Dicom header data for error-reporting functions.
;;; Must be bound outside main loop in case error happens there.
(defvar *dicom-alist* nil)

;;; Ranges for Group numbers to be ignored when parsing objects:
;;; Value NIL -> ignore no ranges [group not found in dictionary -> error].
;;; Otherwise list of CONS pairs where CAR is an inclusive lower bound
;;; and CDR is an exclusive upper bound.  For example, the value
;;;   (( #x5000 . #x5100 ) ( #x6000 . #x6100 ))
;;; causes the 50xx and 60xx ranges to be logged and ignored.
(defvar *ignorable-groups-list* nil)

;;;=============================================================
;;; System Parameters -- not user-configurable.

;;; Don't do name-server lookup when printing stream;
;;; takes time, and most hosts don't have names anyway.
(defparameter socket:*print-hostname-in-stream* nil)

;;; Specified by DICOM Standard -- not configurable.
(defparameter *Echo-Verification-Service* "1.2.840.10008.1.1")
(defparameter *Structure-Set-Storage-Service* "1.2.840.10008.5.1.4.1.1.481.3")
(defparameter *RTPlan-Storage-Service* "1.2.840.10008.5.1.4.1.1.481.5")

;;; Codes for the seven basic PDU types.
(defparameter *Code/PDUtype-Alist*
  '((#x01 . :A-Associate-RQ)
    (#x02 . :A-Associate-AC)
    (#x03 . :A-Associate-RJ)
    (#x04 . :P-Data-TF)
    (#x05 . :A-Release-RQ)
    (#x06 . :A-Release-RSP)
    (#x07 . :A-Abort)))

(defparameter *Image-Storage-Services*
  (list "1.2.840.10008.5.1.4.1.1.1"                 ;Computed Radiography
	"1.2.840.10008.5.1.4.1.1.128"               ;PET-Image-Storage-Service
	"1.2.840.10008.5.1.4.1.1.2"                 ;CT-Image-Storage-Service
	"1.2.840.10008.5.1.4.1.1.4"                 ;MR-Image-Storage-Service
	"1.2.840.10008.5.1.4.1.1.6"                ;US-Image-Storage [Retired]
	"1.2.840.10008.5.1.4.1.1.6.1"               ;US-Image-Storage-Service
	))

(defparameter *Object-Storage-Services*             ;C-Store SOP classes
  (append *Image-Storage-Services*
	  (list *Structure-Set-Storage-Service*
		*RTPlan-Storage-Service*)))

(defparameter *All-Services*                 ;All Server-supported SOP classes
  (list* *Echo-Verification-Service*
	 "1.2.840.10008.1.20.1"    ;Faking Storage Commitment SOP [Push Model]
	 *Object-Storage-Services*))

;(defparameter *Service-Dispatch-Table*
;  `(()))

(defparameter *Application-Context-Name* "1.2.840.10008.3.1.1.1")
(defparameter *Transfer-Syntax-Name* "1.2.840.10008.1.2")

;;;=============================================================
;;; Version name and UID applicable to Prism Dicom System.
;;; Used in both Server and Client but slightly different values in
;;; each.  Set in run-server and run-client.

(defvar *Implementation-Version-Name* "")
(defvar *Implementation-Class-UID* "")

;;;=============================================================
;;; System Parameters -- Configurable via "pds.config" file in directory
;;; "/radonc/prism" for the server.  For the client, any parameters overriding
;;; defaults are configured in "/radonc/prism/prism.config".

(defvar *artim-timeout* 300)                        ;5 minutes

;;; Logging goes to Standard-Output, which is background window if PDS is run
;;; in Prism [as Client] or can be redirected to a file if PDS is run as a
;;; background job [as Server].
;;;
;;; Level is set to 2 for the Prism Client for current testing - probably will
;;; be set to zero for ultimate value.
;;;
;;; Set Log Level for Server in config file "pds.config".
;;;
(defvar *log-level* 0)                ;Logging detail level: 0, 1, 2, 3, or 4.


;;;=============================================================
;;; End.
