;;;
;;; wrapper-client
;;;
;;; Client-mode wrappers and functions.
;;; Contains functions used in Client only.
;;;
;;; 26-Dec-2000 BobGian add log messages for rule/state compilation.
;;; 21-Jun-2001 BobGian target configuration parameters now determined
;;;   from machine definition file rather than from startup dialog.
;;; 10-Aug-2001 BobGian wrap IGNORE-ERRORS around DICOM-CLIENT to catch
;;;   and report otherwise uncaught errors rather than crashing Prism.
;;; 10-Sep-2001 BobGian remove IGNORE-ERRORS around DICOM-CLIENT -
;;;   should be debugged rather than ignored or just logged.
;;; 11-Jan-2002 BobGian remodularize functionality so full DUL state is
;;;   encapsulated in state of special variables bound on client entry,
;;;   so that PDS can stack state and run client as a subsystem of server.
;;; 20-Jan-2002 BobGian *PRINT-PRETTY* -> NIL except in logging functions.
;;; 19-Mar-2002 BobGian add version-identifying banner printout at startup.
;;; 27-Apr-2002 BobGian *PRINT-PRETTY* -> T except in index-file functions.
;;; 04-May-2002 BobGian initialize TCP-Buffer to #\* so unused elements
;;;   can be spotted easily in dump.
;;; 06-Aug-2002 BobGian *PRINT-PRETTY* -> NIL unless overridden.
;;; 30-Aug-2002 BobGian Calling-AE-Title looked up by RUN-CLIENT from
;;;   pr::*DICOM-AE-TITLES* (indexed by hostname) rather than looked up
;;;   in machine IDENT slot by caller and passed this function.  This
;;;   enables multiple clients, each with a unique AE title.
;;; 17-Sep-2002 BobGian *PRINT-ARRAY* -> NIL always except when needed to be T.
;;; 23-Sep-2002 BobGian pr::*DICOM-AE-TITLES* -> DICOM package.
;;; 24-Sep-2002 BobGian add binding for *DICOM-ALIST*.
;;; 08-May-2003 BobGian: *PRINT-PRETTY* T unless it needs to be NIL.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 08-Nov-2003 BobGian - remove *PRINT-RIGHT-MARGIN* and *PRINT-PRETTY*
;;;   bindings; new data dumper makes them irrelevant.
;;; 09-Nov-2003 BobGian - remove startup banner; DICOM version number now
;;;   incorporated into Prism version number.
;;; 09-Nov-2004 BobGian changed args to DICOM-CLIENT to modularize functional
;;;   dispatch mechanism.
;;; 20-Jun-2009 I. Kalet move export and globals here to make
;;; independent of defsystem
;;; 17-Jul-2011 I. Kalet move export to dicom defpackage form in
;;; dicom.cl to eliminate warning about export at top level
;;;

(in-package :dicom)

;;;=============================================================
;;; System Parameter -- user-configurable.

(defvar *dicom-ae-titles* nil
  "Mapping from hostnames to AE titles for Dicom RT clients.")

;;;=============================================================

(defun run-client (cmd host port called-ae data instance-uid-str
		   &aux
		   (calling-ae
		     (or (second (assoc (sys:getenv "HOST")
					*dicom-ae-titles* :test #'string=))
			 "No Title"))            ;In case of misconfiguration.
		   (*calling-AE-name* calling-ae) (*print-array* nil)
		   (*called-AE-name* called-ae) (*remote-IP-string* nil)
		   (*status-code* -1) (*status-message* nil)
		   (*dicom-alist* nil))

  "CMD: :C-Echo-RQ (Echo Verify) or :C-Store-RTPlan-RQ (Send RTPlan)."

  ;; All internal state variables are bound to initial values on client
  ;; startup so that server can push environment to run a client.

  (declare (type keyword cmd)
	   (type simple-base-string host called-ae calling-ae instance-uid-str)
	   (type (or null simple-base-string) *status-message*)
	   (type list data)
	   (type fixnum port *status-code*))

  (setf *Implementation-Version-Name* "PDR_1.0"
	*Implementation-Class-UID* "1.2.840.113994.100.10.1.2")

  (cond ((eq cmd :C-Echo-RQ)
	 (let* ((echo-sop-str *Echo-Verification-Service*)
		(echo-sop-len (length echo-sop-str)))
	   (declare (type simple-base-string echo-sop-str)
		    (type fixnum echo-sop-len))
	   (setq *SOP-class-name* echo-sop-str)
	   (catch :Abandon-Client
	     (dicom-client
	       `((Command . ,cmd)
		 (Remote-Hostname . ,host)
		 (Remote-Port . ,port)
		 (Calling-AE-Title . ,calling-ae)
		 (Called-AE-Title . ,called-ae)
		 (SOP-Class-UID-Len . ,echo-sop-len)
		 (SOP-Class-UID-Str . ,echo-sop-str)
		 (Role-SOP-Class-UID-Len . ,echo-sop-len)
		 (Role-SOP-Class-UID-Str . ,echo-sop-str))))))

	((eq cmd :C-Store-RTPlan-RQ)
	 (let* ((rtplan-sop-str *RTPlan-Storage-Service*)
		(rtplan-sop-len (length rtplan-sop-str)))
	   (declare (type simple-base-string rtplan-sop-str)
		    (type fixnum rtplan-sop-len))
	   (setq *SOP-class-name* rtplan-sop-str)
	   (catch :Abandon-Client
	     (dicom-client
	       `((Command . ,cmd)
		 (Remote-Hostname . ,host)
		 (Remote-Port . ,port)
		 (Calling-AE-Title . ,calling-ae)
		 (Called-AE-Title . ,called-ae)
		 (SOP-Class-UID-Len . ,rtplan-sop-len)
		 (SOP-Class-UID-Str . ,rtplan-sop-str)
		 (Role-SOP-Class-UID-Len . ,rtplan-sop-len)
		 (Role-SOP-Class-UID-Str . ,rtplan-sop-str)
		 (Store-SOP-Instance-UID-Len . ,(even-length instance-uid-str))
		 (Store-SOP-Instance-UID-Str . ,instance-uid-str)
		 (RTPlan-DataSet ,@data))))))

	(t (error "RUN-CLIENT [1] Bad command: ~S ~S ~S" cmd host port)))

  (values *status-code* (or *status-message* "Unknown error")))

;;;-------------------------------------------------------------

(defun dicom-client (initial-environment &aux (*max-datafield-len* nil)
		     (*connection-strm* nil) (*checkpointed-environment* '()))

  (declare (type list *checkpointed-environment*))

  (unwind-protect
      (dicom-mainloop
	(make-array #.TCP-Bufsize                   ;TCP buffer
		    :element-type '(unsigned-byte 8)
		    ;; Initialize to #\* so unused elements can be
		    ;; spotted easily in dump.
		    :initial-element #.(char-code #\*))
	nil                                         ;TCP stream
	initial-environment            ;All args passed as initial environment
	:Client                                     ;Role or Mode
	'event-01)                                  ;Initial activating Event
    (when *connection-strm*
      (close *connection-strm*)
      (setq *connection-strm* nil))))

;;;=============================================================
;;; End.
