;;;
;;; prism-data
;;;
;;; Definitions for objects used in Images and Structure-Sets.
;;; Contains declarations used in Server only.
;;;
;;; Jul/Aug 2002 BobGian add defns for classes used for Structure-Sets:
;;;   GENERIC-PRISM-OBJECT, PSTRUCT, ORGAN, POLYLINE, and CONTOUR.
;;; 18-Sep-2002 BobGian add PAT-POS slot to IMAGE class for describing
;;;   patient position as scanned (Head-First Supine, etc).
;;; 06-May-2003 BobGian add TUMOR and TARGET class defs (for structure-sets).
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :prism)

;;;=============================================================

(defclass generic-prism-object ()

  ((name :type string
	 :accessor name
	 :initarg :name
	 :documentation "The name string for each instance of an
object, e.g., patient name, or plan name.")

   )

  (:default-initargs :name "Generic Prism object.")

  (:documentation "This is the basic prism object definition for
objects that will have names and be created and deleted via selector
panels, and with their own editing panels.")

  )

;;;-------------------------------------------------------------

(defclass pstruct (generic-prism-object)

  ((contours :initarg :contours
	     :type list
	     :accessor contours
	     :documentation "A list of contours representing the
surface of the volume.")

   (display-color :initarg :display-color
		  :accessor display-color)

   )

  (:default-initargs :name "" :contours nil :display-color 'sl:white)

  (:documentation "A pstruct is any kind of 3-d geometric structure
pertaining to the case, either an organ, with density to be used in
the dose computation, or an organ with no density, but whose dose
histogram should be known, or a target, whose dose should be
analyzed.")

  )

;;;-------------------------------------------------------------

(defclass organ (pstruct)

  ((tolerance-dose :type single-float
		   :initarg :tolerance-dose
		   :accessor tolerance-dose
		   :documentation "The accepted value for radiation
tolerance for this organ type, in rads.")

   (density :initarg :density
	    :accessor density
	    :documentation "The density to be used in the dose
computation for inhomogeneity corrections.  It can be nil or a number,
so the type is not specified here.  If nil, the organ is not used in
the dose computation for inhomogeneity corrections.")

   #+ignore
   (organ-name :initarg :organ-name
	       :reader organ-name
	       :documentation "One of the known organ names.")

   )

  (:default-initargs :tolerance-dose 0.0 :density nil
		     :display-color 'sl:green)

  (:documentation "This class includes both organs that represent
inhomogeneities and organs for which there is a tolerance dose not to
be exceeded.  Some organs are of both types.")

  )

;;;--------------------------------------

(defclass tumor (pstruct)

  ((t-stage :type symbol
	    :initarg :t-stage
	    :accessor t-stage
	    :documentation "The tumor's t-stage - one of 't1, 't2,
't3, t4, or nil if unspecified.")

   (m-stage :type symbol
	    :initarg :m-stage
	    :accessor m-stage
	    :documentation "The tumor's m-stage.")

   (n-stage :type symbol
	    :initarg :n-stage
	    :accessor n-stage
	    :documentation "The tumor's n-stage - one of 'n0, 'n1,
'n2, 'n3, or nil if unspecified.")

   (cell-type :type symbol
	      :initarg :cell-type
	      :accessor cell-type
	      :documentation "One of a list of numerous cell types, or
nil if unspecified.")

   (site :type symbol
	 :initarg :site
	 :accessor site
	 :documentation "One of the known tumor sites, a symbol, as
determined by the anatomy tree.")

   (region :type symbol
	   :initarg :region
	   :accessor region
	   :documentation "For lung tumors, a region of the lung.  Nil
if unspecified or for other tumor sites, or one of 'hilum, 'upper-lobe,
'lower-lobe, or 'mediastinum.")

   (side :type symbol
	 :initarg :side
	 :accessor side
	 :documentation "For lung tumors, the side of the lung that
the tumor is on.  Nil if unspecified or for other tumor sites, or one
of 'left or 'right.")

   (fixed :type symbol
	  :initarg :fixed
	  :accessor fixed
	  :documentation "For lung tumors, an indication of whether
the tumor is fixed to the chest wall or not.  Nil if unspecified of
for other tumor sites, or one of 'yes or 'no.")

   (pulm-risk :type symbol
	      :initarg :pulm-risk
	      :accessor pulm-risk
	      :documentation "For lung tumors, the tumor's pulmonary
risk.  Nil if unspecified or for other tumor sites, or one of 'high
or 'low.")

   (grade :initarg :grade
	  :accessor grade
	  :documentation "The tumor's grade")

   )

  (:default-initargs :t-stage nil :n-stage nil :m-stage nil
		     :cell-type nil :site 'body :region nil
		     :side nil :fixed nil :pulm-risk nil
		     :grade nil :display-color 'sl:cyan)

  (:documentation "There may be more than one tumor volume for a
patient.")

  )

;;;--------------------------------------

(defclass target (pstruct)

  ((site :initarg :site
	 :accessor site
	 :documentation "One of the known tumor sites")

   (required-dose :type single-float
		  :initarg :required-dose
		  :accessor required-dose)

   (region :initarg :region
	   :accessor region)

   (target-type :initarg :target-type
		:accessor target-type
		:documentation "One of either initial or boost")

   (nodes :initarg :nodes
	  :accessor nodes
	  :documentation "Nodes to treat")

   (average-size :type single-float
		 :initarg :average-size
		 :accessor average-size)

   (how-derived :initarg :how-derived
		:accessor how-derived)

   )

  (:default-initargs :site 'body :required-dose 0.0
		     :region nil :target-type "unspecified"
		     :how-derived "Manual"
		     :display-color 'sl:blue)

  (:documentation "There may be more than one target volume for a
patient, e.g., the boost volume and the large volume.  Also, the tumor
volume and the target volume are different.")

  )

;;;-------------------------------------------------------------

(defclass polyline ()

  ((z :type single-float
      :initarg :z
      :accessor z)                           ; z coord. of plane of definition

   (vertices :type list
	     :initarg :vertices
	     :accessor vertices
	     :documentation "A list of 2-d coordinate pairs")

   (display-color :type symbol
		  :initarg :display-color
		  :accessor display-color)

   )

  (:default-initargs :vertices nil :display-color 'sl:magenta)

  (:documentation "Polylines represent any unconstrained curve in the
plane, like a clipped isodose contour or a physician's signature.")

  )

;;;-------------------------------------------------------------

(defclass contour (polyline)

  ()

  (:documentation "Contours are always part of some object, the type
of which determines the definition plane.  The vertices are a list of
coordinate pairs because there is nothing about points that would make
it worth having a list of point instances instead.  Structurally, a
contour is the same as a polyline but the implicit difference between
them is that contours are non-self-intersecting, must enclose non-zero
area, no three adjacent vertices can be collinear, and no vertices are
duplicated. It is also understood that the last point is connected to
the first, though it is not explicitly repeated in the vertices
list.")

  )

;;;=============================================================

(defclass image ()

  ((id :type fixnum
       :accessor id)

   (uid :type string
	:accessor uid)

   (patient-id :type fixnum
	       :accessor patient-id
	       :documentation "The Prism Patient ID of the patient this
image belongs to.")

   (image-set-id :type fixnum
		 :accessor image-set-id
		 :documentation "The Prism image set ID of the primary
image set the image belongs to; can also be changed in order to make it
part of another image set.")

   (pat-pos :type string
	    :accessor pat-pos
	    :initarg :pat-pos
	    :documentation "String, one of \"HFP\", \"HFS\", \"FFP\", \"FFS\"
describing patient position as scanned (Head/Feet-First Prone/Supine, etc).
Also legal but not used in Prism are \"HFDR\", \"HFDL\", \"FFDR\", \"FFDL\"
for Head/Feet-first Decubitus Right/Left.")

   (description :type string
		:accessor description)

   (acq-date :type string
	     :accessor acq-date)

   (acq-time :type string
	     :accessor acq-time)

   (scanner-type :type string
		 :accessor scanner-type)            ;GE9800, SOMATOM-DR, etc

   (hosp-name :type string
	      :accessor hosp-name)

   (img-type :type string
	     :accessor img-type)                    ;CT, NMR, PET, etc

   (origin :type (vector single-float 3)
	   :accessor origin
	   :documentation "Origin refers to the location in patient
space of the corner of the image as defined by the point at pixel
array reference 0 0 or voxel array reference 0 0 0 -- see the pixels
and voxels slot in the respective image-2D and image-3D subclasses.")

   (size :type list                          ; of two or three elements, x y z
	 :accessor size
	 :documentation "The size slot refers to the overall size of
the image in each dimension, measured in centimeters in patient
space.")

   (range :type fixnum                              ;4095 fixed stub
	  :accessor range
	  :documentation "Range refers to the maximum pixel/voxel
value possible for this type of image.")

   (units :type string
	  :accessor units)                          ;eg: Hounsfield numbers

   )

  (:documentation "The basis for all kinds of geometric studies upon
patients, including 2-D images, 3-D images, 2-D image sets, like a
series of CT slices, and 3-D image sets.  The information here defines
all the parameters relevant to the moment of study itself and to
parameters found in all images.")

  )

;;;-------------------------------------------------------------

(defclass image-2D (image)

  ((thickness :type single-float
	      :accessor thickness)

   (x-orient :type (vector single-float 3)
	     :accessor x-orient
	     :documentation "The x-orient and y-orient slots are
vectors in patient space that define the orientation of the X and Y
axes of the image respectively, relative to the patient coordinate
system.")

   (y-orient :type (vector single-float 3)
	     :accessor y-orient
	     :documentation "See x-orient.")

   (pix-per-cm :type single-float
	       :accessor pix-per-cm)

   (pixels :type (simple-array (unsigned-byte 8) 1)
	   ;; Prism PIXEL array is (simple-array (unsigned-byte 16) 2) but
	   ;; DICOM treats it [effectively via overlay] as an array of type
	   ;; (simple-array (unsigned-byte 8) 1) .
	   :accessor pixels
	   :documentation "Pixels is the array of image data itself.
The value at each index of the array refers to a sample taken from the
center of the region indexed, and values for images with non-zero
thickness refer to points mid-way through the image's thickness.  The
origin of the pixels array is in the upper left hand corner, and the
array is stored in row-major order so values are indexed as row,
column pairs, i.e., the dimensions are y, x.")

   )

  (:documentation "An image-2D depicts some 2-D slice, cross section
or projected view of a patient's anatomy and is typically a single CT
image, an interpolated cross section of a volume, or the result of ray
tracing through a volume from an eyepoint to a viewing plane.")

  )

;;;=============================================================
;;; End.
