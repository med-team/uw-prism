;;;
;;; functions
;;;
;;; Message parser/generator and environment lookup utilities.
;;; Contains functions common to Client and Server.
;;;
;;; 26-Dec-2000 BobGian wrap IGNORE-ERRORS around error-prone fcns.
;;;   Include error-recovery options in case those fcns barf.
;;;   Change a few local variable names for consistency.
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 04-May-2002 BobGian add TCP buffer overrun check to PARSE-MESSAGE.
;;; 26-Jun-2002 BobGian PARSE-MESSAGE reports error and does hex dump of
;;;   TCP buffer data in case of failed parse.
;;; Jul/Aug 2002 BobGian better messages in error-reporting functions.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================
;;; Parser for DICOM Messages.

(defun parse-message (env tcp-buffer head tail &aux val-1 val-2)

  "Returns on Success:  Message-Type + Environment.  Failure:  :Fail + NIL."

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum head tail))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%PARSE-MESSAGE [1] Parsing message (~D bytes).~%"
	    (the fixnum (- tail head))))

  (unless (< tail #.TCP-Bufsize)
    (mishap env tcp-buffer "PARSE-MESSAGE [2] Buffer overrun - TAIL: ~D."
	    tail))

  (dolist (msgtype *Message-Type-List* (setq val-1 :Fail))

    (multiple-value-bind (input-cont new-env)
	(parse-group (get msgtype :Parser-Rule) env tcp-buffer head tail)

      (declare (type fixnum input-cont)
	       (ignore input-cont))

      ;; PARSE-GROUP returns :Fail [as second value] if parse fails,
      ;; indicating that this rule does not match the message.  Try others.
      (unless (eq new-env :Fail)
	(setq val-1 msgtype val-2 new-env)
	(return))))

  ;; Return values -- First:  Symbol naming message or :Fail.
  ;;                 Second:  Environment structure or NIL.
  (when (>= (the fixnum *log-level*) 3)
    (format t "~%PARSE-MESSAGE [3] Returning (val 1): ~S~%" val-1))

  (when (eq val-1 :FAIL)
    (report-error env nil "PARSE-MESSAGE [4] Failed parse.")
    (dump-bytestream "Message in TCP buffer" tcp-buffer head tail))

  (values val-1 val-2))

;;;=============================================================
;;; Performs pattern-matching [sequential recursion, not tree recursion] to
;;; extract values from ENV but adds nothing to environment, so side-effects
;;; cannot be passed via environment from one arg to another.
;;;
;;; Grammar allows any self-evaluating Lisp atomic object, although only
;;; FIXNUMs are used in rules so far.  Note that symbols here evaluate to
;;; themselves, not to their value slot.  Uses ordinary non-tail recursion
;;; because recursion depth [length of argument list] is small -- usually
;;; zero, max of 2 or 3.  Uses tree recursion to instantiate args to
;;; functions embedded inside inside other args.

(defun eval-args (argument-list env &aux object)

  (declare (type list argument-list env))

  (cond
    ((null argument-list) nil)

    ((atom argument-list)
     (mishap env nil "EVAL-ARGS [1] Bad argument-list: ~S" argument-list))

    ;; By now, ARGUMENT-LIST guaranteed to be CONSP.
    ((atom (setq object (car argument-list)))
     ;; ATOMIC elements of argument list evaluate to themselves.
     (cons object (eval-args (cdr argument-list) env)))

    ;; By now, OBJECT guaranteed to be CONSP.
    ((eq (first object) '<lookup-var)               ;DICOM Variable
     ;; DICOM variables evaluate to their values as bound in environment.
     ;; Presently, access chain is used by Generator but not by Parser.
     ;; Access chain, if present, is passed to ITEM-LOOKUP.
     (cons (apply #'item-lookup (second object) env t (cddr (cdddr object)))
	   (eval-args (cdr argument-list) env)))

    ;; Lisp functions called with args as provided explicitly.
    ((eq (first object) '<funcall)
     (cons (apply (second object) (eval-args (cddr object) env))
	   (eval-args (cdr argument-list) env)))

    (t (mishap env nil "EVAL-ARGS [2] Bad argument-list: ~S" argument-list))))

;;;-------------------------------------------------------------

(defun item-lookup (varname env punt-if-missing? &rest access-chain)

  (declare (type symbol varname)
	   (type (member nil t) punt-if-missing?)
	   (type list env access-chain))

  (let ((pair (cond ((null access-chain)
		     (assoc varname env :test #'eq))
		    (t (assoc varname (cdr (item-present? access-chain env))
			      :test #'eq)))))

    (declare (type list pair))

    (cond ((consp pair)
	   ;; Binding, whether required or not, was present.  Return value.
	   (cdr pair))
	  (punt-if-missing?
	    ;; Spec and proper operation requres variable to be bound
	    ;; in this context.  Punt if not.
	    (mishap env nil "ITEM-LOOKUP [1] Variable ~S missing in chain:~%~S"
		    varname access-chain))
	  ;; In this context, binding is optional.  If missing, return NIL.
	  (t nil))))

;;;-------------------------------------------------------------

(defun set-lookup (env &rest access-chain &aux tmp)

  (declare (type list env access-chain tmp))

  (unless (consp access-chain)
    (mishap env nil "SET-LOOKUP [1] Null access-chain."))

  (setq tmp (cdr access-chain))

  (do ((key (car access-chain))
       (items (cond ((consp tmp)
		     (cdr (item-present? tmp env)))
		    (t env))
	      (cdr items))
       (output-list '())
       (item))
      ((null items)
       (nreverse output-list))

    (declare (type symbol key)
	     (type list items output-list))

    (cond
      ((atom (setq item (car items)))
       (mishap env nil "SET-LOOKUP [2] Bad item ~S in sub-environment." item))
      ((eq key (car item))
       (push (cdr item) output-list)))))

;;;=============================================================
;;; End.
