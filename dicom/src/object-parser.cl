;;;
;;; object-parser
;;;
;;; Parser for DICOM Objects.
;;; Contains functions used in Server only.
;;;
;;; 27-Dec-2000 BobGian add Group-name to Element-name logging printout.
;;;   Improve logging of lists/strings and object descriptors.
;;; 23-Apr-2001 BobGian update PDU de-fragmentation to work when multiple
;;;   PDUs can be read into TCP buffer in single iteration.
;;; 09-May-2001 BobGian update PARSE-OBJECT to parse any object, not just
;;;   a PDV containing images.  Extension is to handle RTPlans for now.
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 16-Mar-2002 BobGian remove overflow-byte-shifting kludge.
;;; 15-Apr-2002 BobGian remove SOP class (element 0) from continuation.
;;;   PARSE-OBJECT will now parse any object.  Caller must check SOP classes.
;;; 02-May-2002 Bobgian fix PARSE-OBJECT: Assoc tags for objects of type
;;;   SEQUENCE and ITEM-IN-SEQUENCE were not being pushed onto the alist
;;;   representing slot value.
;;; 02-May-2002 Bobgian fix GET-TEXT to preserve all characters in string
;;;   and to strip non-significant leading/trailing spaces depending on
;;;   datatype.  If stripping, do so on each string when multiplicity > 1.
;;;   Also strings of type ST, LT must have multiplicity 1, and character
;;;   #\\ is not used as delimiter and thus may be contained in string.
;;; 03-May-2002 BobGian push onto output alist the SQ [Sequence] token
;;;   but not the IT [Item in Sequence] or delimiter tokens ITDL or SQDL.
;;; 04-May-2002 BobGian DICOM spec allows fragmentation border anywhere
;;;   within objects or inside Group/Element/Length fields, subject only
;;;   to being on an even byte boundary.  Implemented byte-shifting scheme
;;;   to down-shift interrupted header or object bytes to low region of TCP
;;;   buffer [portion already parsed] and passing length of shifted bytes back
;;;   via continuation so READ of next fragment offsets over shifted bytes.
;;;   This also requires left-over bytes from previous instantiation to be
;;;   up-shifted on next instantiation to region continguous with continuation
;;;   of the interrupted object, overlying no-longer-needed bytes from
;;;   PDU and PDV header.
;;; 04-May-2002 BobGian add TCP buffer overrun check to PARSE-OBJECT.
;;; 26-Jun-2002 BobGian PARSE-OBJECT does hex dump of tag/length/data-field
;;;   in case of data definition missing in dictionary.
;;; 18-Aug-2002 BobGian temp fix to object parser loosening standard to accept
;;;   null-padding on nominally space-padded strings (as sent by possibly
;;;   non-conformant clients).  Marked ";Null-Padding Fix here." in code.
;;; 17-Sep-2002 BobGian DICOM-ALIST passed to REPORT-ERROR and MISHAP for dump.
;;;   Done in functions PARSE-OBJECT, GET-FIXNUM-LE-VM, GET-FIXNUM-LE, and
;;;   GET-TEXT (passed to last three for error-reporting purposes).
;;; 24-Sep-2002 BobGian:
;;;   Remove 3rd arg (DICOM-ALIST) to REPORT-ERROR and MISHAP and passage
;;;   to them via intermediate functions.  Same functionality is now obtainable
;;;   via special variable set when data is available.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 21-Dec-2003 BobGian: Add arg to PARSE-OBJECT which passes value of global
;;;   variable *IGNORABLE-GROUPS-LIST*.  Value is a [possibly-empty] list of
;;;   CONS pairs, each representing a range of group numbers to ignore.  CAR
;;;   of each is lower limit [inclusive], and CDR is upper limit [exclusive].
;;;   Any object slots containing values with group numbers in such a range
;;;   will have that fact logged but will otherwise be ignored [precisely,
;;;   will be treated exactly as a PRIVATE ELEMENT - will be decoded as an
;;;   uninterpreted string by object parser and dumped when logging level is
;;;   sufficiently high but otherwise will be skipped].
;;; 03-Nov-2004 BobGian flushed symbol naming group from *GROUPNAME-ALIST*
;;;   while preserving group tag and string name.  Symbol was used only
;;;   in error messages.
;;;  1-Dec-2008 I. Kalet new CT scanner sends floating-poinit data,
;;; not needed but was treated as a mishap.  Now just ignored.  See
;;; call to mishap [22] below in parse-object
;;;

(in-package :dicom)

;;;=============================================================

(defun parse-object (env tcp-buffer head tail last-frag? continuation
		     ignorable-groups-list &aux (log-level *log-level*)
		     (virt-idx head) (context-list '()) (output-stack '())
		     (dicom-alist '()) (conttype :New) (pixel-padder 0))

  ;; HEAD [and PDV-IDX, which increments starting from HEAD] is index in
  ;; current TCP buffer of start of an object's Group-Number slot.  8 bytes
  ;; later object itself starts.  If HEAD shifts due to fragmentation-fixup
  ;; [unread bytes from previous instantiation], the moved unread bytes to
  ;; which HEAD is reset will always start with an object's Group-Number slot.
  ;;
  ;; VIRT-IDX is index of current byte in de-fragmented virtual PDV dataset,
  ;; considered to be concatenation of all P-Data-TF PDUs that make up the
  ;; data, and as referenced to zero at beginning of PDU in first fragment.
  ;; First PDV in PDU starts 12 bytes into P-Data-TF PDU -- that is value of
  ;; HEAD when PARSE-OBJECT is first called on a new PDU.  If called on
  ;; multiple PDVs in same PDU, later values of HEAD will be larger.
  ;;
  ;; If unread-bytes were down-shifted on the prior instantiation and moved
  ;; to contiguity with rest of object in current fragment, VIRT-IDX is NOT
  ;; changed - only HEAD [the local TCP buffer index] is.

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type (or null (simple-array t (10))) continuation)
	   (type list ignorable-groups-list context-list dicom-alist
		 output-stack env)
	   (type (integer #x0000 #xFFFF) pixel-padder)
	   (type fixnum head tail virt-idx log-level))

  (when (>= log-level 3)
    (format t
	    #.(concatenate 'string
			   "~%PARSE-OBJECT [1] Entering, PDV-Head:"
			   " ~D, PDV-Tail: ~D, ~A fragment.~%")
	    head tail (if last-frag? "Last" "Internal")))

  (unless (< tail #.TCP-Bufsize)
    (mishap env tcp-buffer "PARSE-OBJECT [2] Buffer overrun - TAIL: ~D." tail))

  (when (arrayp continuation)
    (let ((unread-byte-len (aref (the (simple-array t (10)) continuation) 0)))
      (declare (type fixnum unread-byte-len))
      (when (> unread-byte-len 0)
	;; Unread bytes from the previous instantiation of this function were
	;; moved to the beginning of the TCP buffer.  Next TCP READ started
	;; after them, placing bytes that have already been parsed as PDU
	;; header and initial contents of current PDV.  Now move "left-over"
	;; bytes up to be contiguous with rest of bytes of the data portion
	;; of current PDV, reset the HEAD pointer to beginning of moved bytes,
	;; and continue parsing where we left off in previous instantiation.
	;; This hack is necessary to handle fragmented messags.  Note that the
	;; bytes being overwritten in this move [PDU/PDV header, etc] have
	;; already been parsed and so are no longer needed.  Bytes shifted
	;; up -> move in descending order.
	(when (>= log-level 3)
	  (format
	    t "~%PARSE-OBJECT [3] Shift ~D unread bytes 0 -> ~D to ~D -> ~D.~%"
	    unread-byte-len unread-byte-len
	    (the fixnum (- head unread-byte-len)) head))
	(do ((in-ptr (the fixnum (1- head)) (the fixnum (1- in-ptr)))
	     (out-ptr (the fixnum (1- unread-byte-len))
		      (the fixnum (1- out-ptr))))
	    ((< out-ptr 0)
	     (setq head (the fixnum (1+ in-ptr))))
	  (declare (type fixnum in-ptr out-ptr))
	  (setf (aref tcp-buffer in-ptr) (aref tcp-buffer out-ptr)))))
    (setq dicom-alist (aref (the (simple-array t (10)) continuation) 1))
    (setq output-stack (aref (the (simple-array t (10)) continuation) 2))
    (setq context-list (aref (the (simple-array t (10)) continuation) 3))
    (setq virt-idx (aref (the (simple-array t (10)) continuation) 4))
    (setq conttype (aref (the (simple-array t (10)) continuation) 5))
    (setq pixel-padder (aref (the (simple-array t (10)) continuation) 6))
    (when (>= log-level 3)
      (format t "~%PARSE-OBJECT [4] Continuation type ~A at ~D/~D.~%"
	      conttype head virt-idx)))

  (do ((pdv-idx head)                               ;Start of GroupNum field
       (val-idx 0)                ;Start of object value field [= PDV-IDX + 8]
       (groupnum 0) (elemnum 0) (itemlen) (elemdata) (VR-symbol)
       (VR-descriptor) (datatype) (string-padding) (lisptype) (elemname "")
       (gn-alist *groupname-alist*) (groupdata) (groupname "")
       (g/e-alist *group/elemname-alist*) (datatype-alist *datatype-alist*)
       (itemvalue nil nil))
      ((>= pdv-idx tail)
       (unless (= pdv-idx tail)
	 (mishap env tcp-buffer "PARSE-OBJECT [5] Index overrun at ~D/~D."
		 pdv-idx virt-idx))
       (cond
	 (last-frag?
	   (when (or (consp output-stack)
		     (consp context-list))
	     (mishap env tcp-buffer "PARSE-OBJECT [6] Bad context at ~D/~D."
		     pdv-idx virt-idx))
	   (when (>= log-level 3)
	     (format t "~%PARSE-OBJECT [7] Done at ~D/~D.~%" pdv-idx virt-idx))
	   (setq *parser-state* nil)
	   ;; Return Alist representing parsed object.
	   ;; Non-NIL value signals that parsing has completed.
	   (nreverse dicom-alist))
	 (t (setq *parser-state*
		  (vector 0 dicom-alist output-stack context-list virt-idx
			  :New pixel-padder nil 0 0))
	    (when (>= log-level 3)
	      (format t "~%PARSE-OBJECT [8] EOB on object end at ~D/~D.~%"
		      pdv-idx virt-idx))
	    nil)))

    (declare (type list groupdata gn-alist g/e-alist datatype-alist elemdata
		   VR-descriptor datatype)
	     (type simple-base-string groupname elemname)
	     (type symbol VR-symbol string-padding)
	     (type fixnum pdv-idx val-idx groupnum elemnum))

    (when (eq conttype :Pixel-Array)
      (do ((tcp-idx pdv-idx (the fixnum (+ tcp-idx 2)))
	   (pixel-array (aref (the (simple-array t (10)) continuation) 7))
	   (pix-idx (aref (the (simple-array t (10)) continuation) 8)
		    (the fixnum (+ pix-idx 2)))
	   (pixarray-len (aref (the (simple-array t (10)) continuation) 9))
	   (low-byte 0) (high-byte 0)
	   (pixel-padder-lo (logand #x00FF pixel-padder))
	   (pixel-padder-hi (ash pixel-padder -8)))
	  (( ))

	(declare (type (integer #x00 #xFF) pixel-padder-lo pixel-padder-hi)
		 (type (simple-array (unsigned-byte 8) 1) pixel-array)
		 (type fixnum tcp-idx pix-idx pixarray-len low-byte high-byte))

	(cond
	  ((= pix-idx pixarray-len)
	   ;; Check for scan done first, in case scan-done and reaching
	   ;; TCP buffer end both happen at same time.  If so, we will
	   ;; handle end-of-buffer at start of next iteration.
	   (setq virt-idx
		 (the fixnum (+ virt-idx (the fixnum (- tcp-idx pdv-idx)))))
	   (when (>= log-level 3)
	     (format t
		     #.(concatenate
			 'string
			 "~%PARSE-OBJECT [9] PixArray done "
			 "[~D bytes] at ~D/~D.~%")
		     pixarray-len tcp-idx virt-idx))
	   ;; Completed Pixel Array -- prepare to scan next object.
	   ;; Value multiplicity is explicitly one here.
	   (push (list (cons #x7FE0 #x0010) pixel-array) dicom-alist)
	   (setq pdv-idx tcp-idx conttype :New)
	   (go NEXT-CYCLE))
	  ((= tcp-idx tail)
	   (setq virt-idx
		 (the fixnum (+ virt-idx (the fixnum (- tcp-idx pdv-idx)))))
	   (when (>= log-level 3)
	     (format t
		     #.(concatenate
			 'string
			 "~%PARSE-OBJECT [10] PixArray EOB "
			 "[~D of ~D bytes] at ~D/~D.~%")
		     pix-idx pixarray-len tcp-idx virt-idx))
	   ;; Encountered end of TCP buffer before completing Pixel Array.
	   (cond (last-frag?
		   ;; Byte count wrong -- last fragment should contain
		   ;; complete Pixel Array.
		   (mishap env tcp-buffer
			   #.(concatenate
			       'string
			       "PARSE-OBJECT [11] PixArray buffer overrun."
			       "~%  ~D bytes at ~D/~D.")
			   (the fixnum (- pixarray-len pix-idx))
			   tcp-idx virt-idx))
		 (t (setq *parser-state*
			  (vector 0 dicom-alist output-stack context-list
				  virt-idx :Pixel-Array pixel-padder
				  pixel-array pix-idx pixarray-len))
		    ;; Set continuation and return to await next fragment.
		    (return-from parse-object nil)))))

	;; Terms "low" and "high" refer to packet byte order or TCP buffer
	;; addressing order -- ie, network byte order [little endian] --
	;; which is same as fixnum byte significance but not necessarily the
	;; same as machine byte order [same on Little-Endian machine only].
	(setq low-byte (aref tcp-buffer tcp-idx)
	      high-byte (aref tcp-buffer (the fixnum (1+ tcp-idx))))

	;; Detect Pixel-Padding-Value and convert to Zero.
	(when (or (and (= low-byte pixel-padder-lo)
		       (= high-byte pixel-padder-hi))
		  ;; Fix to avoid pixel-value overflow.
		  ;; HIGH-BYTE >= 16 -> pixel value > 4095.
		  (>= high-byte 16))
	  (setq high-byte #x00 low-byte #x00))

	;; Possible conversion Network (Little) to Machine Endianism.
	#+little-endian                             ;No need for byte-swapping
	(setf (aref pixel-array pix-idx) low-byte)
	#+little-endian
	(setf (aref pixel-array (the fixnum (1+ pix-idx))) high-byte)
	#+big-endian                                ;Must do byte-swapping
	(setf (aref pixel-array pix-idx) high-byte)
	#+big-endian
	(setf (aref pixel-array (the fixnum (1+ pix-idx))) low-byte)))

    (when (> (the fixnum (+ pdv-idx 8)) tail)
      ;; TCP buffer does not contain entire Group/Element Number and Length
      ;; fields, which are necessary for parsing the next object.  Shift
      ;; remainder of bytes, set continuation, and return to await next
      ;; fragment.  Bytes shifted down -> move in ascending order.
      (do ((in-ptr 0 (the fixnum (1+ in-ptr)))
	   (out-ptr pdv-idx (the fixnum (1+ out-ptr))))
	  ((= out-ptr tail)
	   (when (>= log-level 3)
	     (format t
		     #.(concatenate
			 'string
			 "~%PARSE-OBJECT [12] Buffer overrun"
			 " before G/E decode at ~D/~D.~%"
			 "Shifting ~D unread bytes ~D -> ~D to 0 -> ~D.~%")
		     pdv-idx virt-idx in-ptr pdv-idx tail in-ptr))
	   (setq *parser-state*
		 (vector in-ptr dicom-alist output-stack context-list virt-idx
			 :New pixel-padder nil 0 0)))
	(declare (type fixnum in-ptr out-ptr))
	(setf (aref tcp-buffer in-ptr) (aref tcp-buffer out-ptr)))
      (return-from parse-object nil))

    (setq groupnum (get-fixnum-LE tcp-buffer pdv-idx 2)
	  elemnum (get-fixnum-LE tcp-buffer (the fixnum (+ pdv-idx 2)) 2)
	  ;; ITEMLEN is either a FIXNUM [required by spec to be EVEN]
	  ;; or the symbol :Undefined for indeterminate-length sequences
	  ;; or objects of value-representation OB or OW.
	  itemlen (get-fixnum-LE tcp-buffer (the fixnum (+ pdv-idx 4)) 4))

    (cond
      ((oddp groupnum)
       (setq groupname "*")
       (cond ((= elemnum #x0000)
	      (setq VR-symbol 'UL elemname "Group Length"))
	     (t (setq VR-symbol 'PE elemname "Private Element"))))

      ((and (consp (setq groupdata (assoc groupnum gn-alist :test #'=)))
	    (consp (setq elemdata (assoc (cons groupnum elemnum)
					 g/e-alist :test #'equal))))
       ;; Item/Sequence delimiters handled here.
       (setq groupname (second groupdata)
	     VR-symbol (second elemdata)
	     elemname (third elemdata)))

      ((dolist (group ignorable-groups-list nil)
	 (when (and (<= (the fixnum (car group)) groupnum)
		    (< groupnum (the fixnum (cdr group))))
	   (return t)))
       (setq groupname "*")
       (cond ((= elemnum #x0000)
	      (setq VR-symbol 'UL elemname "Group Length"))
	     (t (setq VR-symbol 'IE elemname "Ignorable Element")))
       (format t "~%PARSE-OBJECT [13] Ignorable slot: (~4,'0X:~4,'0X)~%"
	       groupnum elemnum))

      (t (setq groupname "*")
	 (setq VR-symbol 'MD elemname "Missing Definition")
	 ;; Missing Group/Element definition in Data Dictionary.  Log
	 ;; message but otherwise situation is harmless, unless value
	 ;; of that slot is needed later, in which case accessor will
	 ;; call MISHAP.  This is a programming error rather than a
	 ;; run-time error.
	 (report-error env nil
		       #.(concatenate
			   'string
			   "PARSE-OBJECT [14] Missing data definition."
			   "~%  8+~S bytes at ~D/~D~%"
			   "  Group: ~4,'0X, Element: ~4,'0X")
		       itemlen pdv-idx virt-idx groupnum elemnum)
	 (dump-bytestream "Object in TCP buffer"
			  tcp-buffer pdv-idx
			  (the fixnum (+ pdv-idx itemlen 8)))))

    (cond
      ((null (setq VR-descriptor (assoc VR-symbol datatype-alist :test #'eq)))
       ;; Missing data type definition in Data Dictionary.
       (mishap env tcp-buffer
	       #.(concatenate
		   'string
		   "PARSE-OBJECT [15] Bad type: ~S~%"
		   "  8+~S bytes at ~D/~D~%"
		   "  Group: ~4,'0X, Element: ~4,'0X, Name: ~S ~S")
	       VR-symbol itemlen pdv-idx virt-idx groupnum elemnum
	       groupname elemname))

      ((cddr (the cons VR-descriptor))              ;List-Length is at least 3

       (when (and (eq itemlen :Undefined)           ;Legal, but should be rare
		  (or (eq VR-symbol 'OB)          ;Lisptype: (unsigned-byte 8)
		      (eq VR-symbol 'OW)))       ;Lisptype: (unsigned-byte 16)
	 (mishap env tcp-buffer
		 #.(concatenate
		     'string
		     "PARSE-OBJECT [16] Indeterminate-length OB or OW data.~%"
		     "  This case is legal but not implemented in PDS.~%"
		     "  Object at ~D/~D~%  Group: ~4,'0X,"
		     " Element: ~4,'0X, Name: ~S ~S~%  ~S")
		 pdv-idx virt-idx groupnum elemnum
		 groupname elemname VR-descriptor))

       (unless (and (typep itemlen 'fixnum)
		    (evenp (the fixnum itemlen)))
	 ;; Bad value length in Element Length slot.
	 ;; DICOM spec requires ITEMLEN to be an EVEN FIXNUM for explicit
	 ;; length objects.  Can be :Undefined only for SQ, OB, OW data.
	 (mishap env tcp-buffer
		 #.(concatenate
		     'string
		     "PARSE-OBJECT [17] Bad itemlen: ~S~%"
		     "  Object at ~D/~D~%  Group: ~4,'0X,"
		     " Element: ~4,'0X, Name: ~S ~S~%  ~S")
		 itemlen pdv-idx virt-idx groupnum elemnum
		 groupname elemname VR-descriptor))

       (setq string-padding (fourth VR-descriptor)
	     datatype (third VR-descriptor)
	     lisptype (first datatype)
	     val-idx (the fixnum (+ pdv-idx 8)))

       (cond
	 ;; First encounter with Pixel Array -- allocate it and start copy.
	 ((and (= groupnum #x7FE0)
	       (= elemnum #x0010))

	  (when (>= log-level 2)
	    (format
	      t
	      #.(concatenate
		  'string
		  "~%PARSE-OBJECT [18] 8+~S bytes at ~D/~D~%  Group: ~4,'0X, "
		  "Element: ~4,'0X, Name: ~S ~S~%  ~S: Pixel-Array~%")
	      itemlen pdv-idx virt-idx groupnum elemnum
	      groupname elemname VR-descriptor))

	  (do ((tcp-idx val-idx (the fixnum (+ tcp-idx 2)))
	       (pix-idx 0 (the fixnum (+ pix-idx 2)))
	       (pixel-array
		 (make-array (the fixnum itemlen)
			     :element-type '(unsigned-byte 8)
			     :initial-element 0))
	       (low-byte 0) (high-byte 0)
	       (pixel-padder-lo (logand #x00FF pixel-padder))
	       (pixel-padder-hi (ash pixel-padder -8)))
	      (( ))

	    (declare (type fixnum tcp-idx pix-idx low-byte high-byte)
		     (type (integer #x00 #xFF) pixel-padder-lo pixel-padder-hi)
		     (type (simple-array (unsigned-byte 8) 1) pixel-array))

	    (cond
	      ((= pix-idx (the fixnum itemlen))
	       ;; Check for scan-done first, in case scan-done and reaching
	       ;; end-of-buffer condition both happen at same time.  If so, we
	       ;; handle end-of-buffer condition at start of next iteration.
	       (setq virt-idx
		     (the fixnum
		       (+ virt-idx (the fixnum (- tcp-idx pdv-idx)))))
	       (when (>= log-level 3)
		 (format t
			 #.(concatenate
			     'string
			     "~%PARSE-OBJECT [19] PixArray done "
			     "[~D bytes] at ~D/~D.~%")
			 itemlen  tcp-idx virt-idx))
	       ;; Value multiplicity is explicitly one here.
	       (push (list (cons #x7FE0 #x0010) pixel-array) dicom-alist)
	       ;; Completed Pixel Array -- prepare to scan next object.
	       (setq pdv-idx tcp-idx conttype :New)
	       (go NEXT-CYCLE))

	      ((= tcp-idx tail)
	       ;; Encountered end of TCP buffer before completing
	       ;; Pixel Array -- set continuation and return.
	       (setq virt-idx
		     (the fixnum
		       (+ virt-idx (the fixnum (- tcp-idx pdv-idx)))))
	       (when (>= log-level 3)
		 (format t
			 #.(concatenate
			     'string
			     "~%PARSE-OBJECT [20] PixArray EOB "
			     "[~D of ~D bytes] at ~D/~D.~%")
			 pix-idx itemlen tcp-idx virt-idx))
	       (setq *parser-state*
		     (vector 0 dicom-alist output-stack context-list virt-idx
			     :Pixel-Array pixel-padder pixel-array pix-idx
			     itemlen))
	       (return-from parse-object nil)))

	    ;; Terms "low" and "high" refer to packet byte order or TCP buffer
	    ;; which is same as fixnum byte significance but not necessarily
	    ;; the same as machine byte order [same on Little-Endian machine].
	    (setq low-byte (aref tcp-buffer tcp-idx)
		  high-byte (aref tcp-buffer (the fixnum (1+ tcp-idx))))

	    ;; Detect Pixel-Padding-Value and convert to Zero.
	    (when (or (and (= low-byte pixel-padder-lo)
			   (= high-byte pixel-padder-hi))
		      ;; Fix to avoid pixel-value overflow.
		      ;; HIGH-BYTE >= 16 -> pixel value > 4095.
		      (>= high-byte 16))
	      (setq high-byte #x00 low-byte #x00))

	    ;; Possible conversion Network (Little) to Machine Endianism.
	    #+little-endian                         ;No need for byte-swapping
	    (setf (aref pixel-array pix-idx) low-byte)
	    #+little-endian
	    (setf (aref pixel-array (the fixnum (1+ pix-idx))) high-byte)
	    #+big-endian                            ;Must do byte-swapping
	    (setf (aref pixel-array pix-idx) high-byte)
	    #+big-endian
	    (setf (aref pixel-array (the fixnum (1+ pix-idx))) low-byte)))

	 ;; All other slot types -- check whether TCP buffer contains entire
	 ;; object.  If not, set continuation and return for next fragment.
	 ;; Bytes shifted down -> move in ascending order.
	 ;; Shift starting with the Group/Element tag so that on next
	 ;; instantiation parsing will start synchronously with object's
	 ;; header rather than with random bits of object itself.
	 ((> (the fixnum (+ val-idx (the fixnum itemlen))) tail)
	  (do ((in-ptr 0 (the fixnum (1+ in-ptr)))
	       (out-ptr pdv-idx (the fixnum (1+ out-ptr))))
	      ((= out-ptr tail)
	       (when (>= log-level 3)
		 (format t
			 #.(concatenate
			     'string
			     "~%PARSE-OBJECT [21] Object overrun at ~D/~D.~%"
			     "Shifting ~D unread bytes ~D -> ~D to 0 -> ~D.~%")
			 pdv-idx virt-idx in-ptr pdv-idx tail in-ptr))
	       (setq *parser-state*
		     (vector in-ptr dicom-alist output-stack context-list
			     virt-idx :New pixel-padder nil 0 0)))
	    (declare (type fixnum in-ptr out-ptr))
	    (setf (aref tcp-buffer in-ptr) (aref tcp-buffer out-ptr)))
	  (return-from parse-object nil)))

       ;; Now process all slot type objects other than Pixel Array.
       ;; All objects are known to be contained in full in TCP buffer.
       ;; Private elements are logged but not passed through, treated as
       ;; datatype PE [ie, arbitrary uninterpreted Long Text strings].
       (cond
	 ((eq lisptype 'fixnum)    ;ITEMVALUE is a list of one or more fixnums
	  (setq itemvalue (get-fixnum-LE-VM
			    tcp-buffer val-idx (second datatype) itemlen))
	  ;; Don't transmit Group Length fields.
	  (unless (= elemnum #x0000)
	    ;; Remember "Pixel Padding Value" for later use.
	    (when (and (= groupnum #x0028)
		       (= elemnum #x0120))
	      (setq pixel-padder (car itemvalue)))
	    ;; Value multiplicity is arbitrary here.
	    ;; ITEMVALUE is a list of fixnums.
	    (push (cons (cons groupnum elemnum) itemvalue) dicom-alist)))

	 ((or (eq lisptype 'single-float)
	      (eq lisptype 'double-float)) ;; ignore floats for now IK 1-Dec-08
	  )
	 #|
	  (mishap env tcp-buffer
		  #.(concatenate
		      'string
		      "PARSE-OBJECT [22] Non-implemented flonum.~%"
		      "  8+~S bytes at ~D/~D~%  Group: "
		      "~4,'0X, Element: ~4,'0X, Name: ~S ~S~%  ~S")
		  itemlen pdv-idx virt-idx groupnum elemnum
		  groupname elemname VR-descriptor))
		  |#
	 ((eq VR-symbol 'PE))                  ;Don't transmit Private Element
	 ((eq VR-symbol 'MD))                   ;or Missing Definition fields.

	 ((eq lisptype 'string)
	  ;; ITEMVALUE is a list of zero or more strings [NIL for NO-VALUE].
	  (setq itemvalue (get-text tcp-buffer val-idx itemlen tail
				    string-padding VR-symbol))
	  ;; Value multiplicity is arbitrary here.
	  ;; ITEMVALUE is a list of strings.
	  (push (cons (cons groupnum elemnum) itemvalue) dicom-alist))

	 ;; Don't transmit Byte or Word strings, other than Pixel Data
	 ;; which is tranmitted specially by :Pixel-Array code above.
	 ;; These are the datatypes which are allowed to have :Undefined
	 ;; lengths [as value of ITEMLEN] other than type SQ.
	 ((eq VR-symbol 'OB))                     ;Lisptype: (unsigned-byte 8)
	 ((eq VR-symbol 'OW))                    ;Lisptype: (unsigned-byte 16)

	 ;; Missing implementation of data type defined in Data Dictionary.
	 (t (mishap env tcp-buffer
		    #.(concatenate 'string
				   "PARSE-OBJECT [23] Type not implemented.~%"
				   "  8+~S bytes at ~D/~D~%"
				   "  Group: ~4,'0X, Element: ~4,'0X,"
				   " Name: ~S ~S~%  ~S")
		    itemlen pdv-idx virt-idx groupnum elemnum
		    groupname elemname VR-descriptor)))

       (when (>= log-level 2)
	 (format
	   t
	   #.(concatenate
	       'string
	       "~%PARSE-OBJECT [24] 8+~S bytes at ~D/~D~%  Group: ~4,'0X, "
	       "Element: ~4,'0X, Name: ~S ~S~%  ~S: ~S~%")
	   itemlen pdv-idx virt-idx groupnum elemnum
	   groupname elemname VR-descriptor
	   (cond ((eq lisptype 'fixnum)
		  itemvalue)                      ;List of one or more fixnums
		 ((eq lisptype 'string)             ;Character strings
		  itemvalue)  ;String list - length equals value multiplicity.
		 (t "[Unknown type]"))))

       (let ((increment (the fixnum (+ 8 (the fixnum itemlen)))))
	 (declare (type fixnum increment))
	 (setq pdv-idx (the fixnum (+ pdv-idx increment)))
	 (setq virt-idx (the fixnum (+ virt-idx increment)))))

      ;; Sequence and Item markers initiate parse of contained sub-objects
      ;; but do not themselves constitute an "object" that must fit in buffer.
      ;; They cause stacking and clearing of DICOM-ALIST so that it can
      ;; accumulate the interior objects.
      ;; ITEMLEN is allowed to be :Undefined from here to end.
      ((or (eq VR-symbol 'IT)
	   (eq VR-symbol 'SQ))
       (when (>= log-level 3)
	 (format t "~%PARSE-OBJECT [25] Open delimiter at ~D/~D: ~S~%"
		 pdv-idx virt-idx VR-symbol))
       (push dicom-alist output-stack)
       ;; Push the SQ [Sequence] token but not the IT [Item in Sequence]
       ;; token or the delimiter tokens ITDL [Item Delimiter] or SQDL
       ;; [Sequence Delimiter].
       (setq dicom-alist (cond ((eq VR-symbol 'SQ)
				(list (cons groupnum elemnum)))
			       (t '())))
       (push (cons VR-symbol
		   (cond ((typep itemlen 'fixnum)
			  (the fixnum (+ (the fixnum (+ virt-idx 8))
					 (the fixnum itemlen))))
			 (t itemlen)))
	     context-list)
       (setq pdv-idx (the fixnum (+ pdv-idx 8)))
       (setq virt-idx (the fixnum (+ virt-idx 8))))

      ;; Ditto for end-markers for Sequences or Items of :Undefined length.
      ((or (eq VR-symbol 'ITDL)
	   (eq VR-symbol 'SQDL))
       ;;
       (when (>= log-level 3)
	 (format t "~%PARSE-OBJECT [26] Close delimiter at ~D/~D: ~S~%"
		 pdv-idx virt-idx VR-symbol))
       (unless (and (typep itemlen 'fixnum)
		    (= (the fixnum itemlen) 0))
	 ;; Delimiters must contain value field length of zero.
	 (mishap env tcp-buffer
		 #.(concatenate
		     'string
		     "PARSE-OBJECT [27] Bad delimiter field.~%"
		     "  8+~S bytes at ~D/~D~%  Group: "
		     "~4,'0X, Element: ~4,'0X, Name: ~S ~S~%  ~S")
		 itemlen pdv-idx virt-idx groupnum elemnum
		 groupname elemname VR-descriptor))

       (unless (consp context-list)
	 ;; Context/delimiter asymmetry problem.
	 (mishap env tcp-buffer
		 #.(concatenate
		     'string
		     "PARSE-OBJECT [28] Context asymmetry.~%"
		     "  8+~S bytes at ~D/~D~%  Group: "
		     "~4,'0X, Element: ~4,'0X, Name: ~S ~S~%  ~S")
		 itemlen pdv-idx virt-idx groupnum elemnum
		 groupname elemname VR-descriptor))

       (let ((context (pop context-list)))
	 (declare (type cons context))
	 (let ((objtype (car context))
	       (objend (cdr context)))
	   (unless (and (or (and (eq objtype 'IT)
				 (eq VR-symbol 'ITDL))
			    (and (eq objtype 'SQ)
				 (eq VR-symbol 'SQDL)))
			(eq objend :Undefined))
	     ;; Item/Sequence delimiters used only on :Undefined
	     ;; length sequences or items.
	     (mishap env tcp-buffer
		     #.(concatenate 'string
				    "PARSE-OBJECT [29] Bad delimiter.~%"
				    "  8+~S bytes at ~D/~D~%"
				    "  Group: ~4,'0X, Element: ~4,'0X,"
				    " Name: ~S ~S~%  ~S")
		     itemlen pdv-idx virt-idx groupnum elemnum
		     groupname elemname VR-descriptor))))

       ;; End of composite-object -- DICOM-ALIST contains reversed list
       ;; of the sub-objects.  Insert that list [reversed to forward order]
       ;; as the object of the sequence being accumulated.
       (let ((itemdata (nreverse dicom-alist)))
	 (declare (type cons itemdata))
	 (setq dicom-alist (pop output-stack))      ;Restore state
	 (push itemdata dicom-alist))            ;Put composite object on list
       (setq pdv-idx (the fixnum (+ pdv-idx 8)))
       (setq virt-idx (the fixnum (+ virt-idx 8))))

      ;; Unrecognized situation.
      (t (mishap env tcp-buffer
		 #.(concatenate
		     'string
		     "PARSE-OBJECT [30] Unrecognized situation.~%"
		     "  8+~S bytes at ~D/~D~%  Group: "
		     "~4,'0X, Element: ~4,'0X, Name: ~S ~S~%  ~S")
		 itemlen pdv-idx virt-idx groupnum elemnum
		 groupname elemname VR-descriptor)))

    NEXT-CYCLE

    (tagbody
      ;; Must iterate end condition test because several nested structures
      ;; (items/sequences) might be terminating at same index.
      END-CONDITION
      (when (consp context-list)
	;; Check for end of sequence or item of explicit length.

	(when (>= log-level 3)
	  (format t
		  #.(concatenate
		      'string
		      "~%PARSE-OBJECT [31] Composite-object end"
		      " check at ~D/~D.~%")
		  pdv-idx virt-idx))

	(let ((context (car context-list)))
	  (declare (type cons context))
	  (let ((objend (cdr context)))
	    (when (and (typep objend 'fixnum)
		       (>= virt-idx (the fixnum objend)))
	      ;; Same end-of-sequence actions as when composite object
	      ;; is terminated by an explicit delimiter.
	      (let ((itemdata (nreverse dicom-alist)))
		(declare (type cons itemdata))
		(setq dicom-alist (pop output-stack))   ;Restore state
		(push itemdata dicom-alist))     ;Put composite object on list

	      (when (>= log-level 3)
		(format t
			#.(concatenate
			    'string
			    "~%PARSE-OBJECT [32] Composite object"
			    " end at ~D/~D: ~S~%")
			pdv-idx virt-idx context))

	      (setq context-list (cdr context-list))
	      (go END-CONDITION))))))))

;;;=============================================================
;;; Utilities for Object Parsing.
;;; Reads Little-Endian fixnums of Variable-Multiplicity from buffer.

(defun get-fixnum-LE-VM (tcp-buffer idx fixnum-length field-length)

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum idx fixnum-length field-length))

  (cond
    ;; Value multiplicity greater than one -- return list of fixnums.
    ((< fixnum-length field-length)
     (do ((idx2 idx (the fixnum (+ idx2 fixnum-length)))
	  (limit (the fixnum (+ idx field-length)))
	  (accumulator '()))
	 ((>= idx2 limit)
	  (unless (= idx2 limit)
	    (mishap nil tcp-buffer
		    #.(concatenate
			'string
			"GET-FIXNUM-LE-VM [1] Bad field/item lengths."
			"  Object in TCP-Buffer~%  "
			"at idx ~D (~D byte fixnum, ~D byte field).")
		    idx fixnum-length field-length))
	  (nreverse accumulator))
       (declare (type list accumulator)
		(type fixnum idx2 limit))
       (push (get-fixnum-LE tcp-buffer idx2 fixnum-length) accumulator)))

    ;; Invalid field-width/value-multiplicity combination.
    ((> fixnum-length field-length)
     (mishap nil tcp-buffer
	     #.(concatenate 'string
			    "GET-FIXNUM-LE-VM [2] Bad field/item lengths."
			    "  Object in TCP-Buffer~%  "
			    "at idx ~D (~D byte fixnum, ~D byte field).")
	     idx fixnum-length field-length))

    ;; Value multiplicity = one -- return list of the single fixnum.
    (t (list (get-fixnum-LE tcp-buffer idx fixnum-length)))))

;;;-------------------------------------------------------------
;;; Reads a single Little-Endian fixnum of fixed length from buffer;
;;; ie, value multiplicity is ONE.

(defun get-fixnum-LE (tcp-buffer idx fixnum-length &aux (byte-0 0)
		      (byte-1 0) (byte-2 0) (byte-3 0))

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type (integer #x00 #xFF) byte-0 byte-1 byte-2 byte-3)
	   (type fixnum idx fixnum-length))

  (cond

    #+ignore              ;1-byte fixnums not used, but may be useful someday.
    ((= fixnum-length 1)
     (the (integer #x0000 #x00FF) (aref tcp-buffer idx)))

    ((= fixnum-length 2)
     (logior (the (integer #x0000 #xFF00)
	       (ash (the (integer #x00 #xFF)
		      (aref tcp-buffer (the fixnum (1+ idx)))) 8))
	     (the (integer #x0000 #x00FF) (aref tcp-buffer idx))))

    ;; Largest mask really should be #xFF000000, but using smaller value
    ;; keeps it a POSITIVE FIXNUM, and no value will exceed 536870911.
    ((= fixnum-length 4)
     (setq byte-0 (aref tcp-buffer idx)
	   byte-1 (aref tcp-buffer (the fixnum (1+ idx)))
	   byte-2 (aref tcp-buffer (the fixnum (+ idx 2)))
	   byte-3 (aref tcp-buffer (the fixnum (+ idx 3))))
     (cond ((and (= byte-0 #xFF)
		 (= byte-1 #xFF)
		 (= byte-2 #xFF)
		 (= byte-3 #xFF))
	    ;; This code used for undefined-length itemlists and sequences.
	    :Undefined)
	   ;; Largest mask really should be #xFF000000, but smaller value
	   ;; keeps everything a POSITIVE FIXNUM, and no value will exceed
	   ;; 536870911.
	   (t (logior (the (integer #x00000000 #x1F000000)
			(ash (the (integer #x00 #x1F) (logand #x1F byte-3))
			     24))
		      (the (integer #x00000000 #x00FF0000) (ash byte-2 16))
		      (the (integer #x00000000 #x0000FF00) (ash byte-1 8))
		      byte-0))))

    ;; Fixnum value out of range and not :Undefined.
    (t (mishap nil tcp-buffer
	       #.(concatenate
		   'string
		   "GET-FIXNUM-LE [1] Fixnum out of range.~%"
		   "  Object in TCP-Buffer at idx ~D (~D byte fixnum).")
	       idx fixnum-length))))

;;;-------------------------------------------------------------
;;; For strings, value multiplicity is determined by presence of delimiter
;;; characters.  If present, multiplicity is greater than one and GET-TEXT
;;; returns a list of substrings.  If not present, multiplicity is equal to
;;; one and GET-TEXT returns singleton list of the string.  If FIELD-LENGTH
;;; is zero, GET-TEXT returns an empty list.

(defun get-text (tcp-buffer idx field-length tail string-padding VR-symbol)

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type symbol string-padding VR-symbol)
	   (type fixnum idx field-length tail))

  (when (> field-length 0)
    ;; For empty strings, return NIL so value can be skipped over by OR when
    ;; scrounging DICOM slots for data to stuff into Prism object slots.
    (do ((ptr idx (the fixnum (1+ ptr)))
	 (output-charlist '())
	 (limit (min tail (the fixnum (+ idx field-length)))))
	((>= ptr limit)
	 (cond
	   ((eq string-padding :No-Pad)
	    (setq output-charlist (nreverse output-charlist)))
	   ((eq string-padding :Space-Pad)
	    ;; Trim off trailing #\Space chars [at front of backwards list].
	    (do ((charlist output-charlist (cdr charlist))
		 (ch #\*))
		((or (null charlist)
		     ;Null-Padding Fix here.
		     (not (or (eq (setq ch (car charlist)) #\Space)
			      (eq ch #\Null))))
		 (setq output-charlist charlist)))
	    (setq output-charlist (nreverse output-charlist))
	    ;; Trim off leading #\Space chars [at front of forwards list].
	    (when (and (eq (car output-charlist) #\Space)
		       (not (eq VR-symbol 'LT))
		       (not (eq VR-symbol 'ST)))
	      (do ((charlist output-charlist (cdr charlist)))
		  ((or (null charlist)
		       (not (eq (car charlist) #\Space)))
		   (setq output-charlist charlist)))))
	   ((eq string-padding :Null-Pad)
	    (when (eq (car output-charlist) #\Null)
	      (setq output-charlist (cdr output-charlist)))
	    (setq output-charlist (nreverse output-charlist)))
	   (t (mishap nil tcp-buffer
		      "GET-TEXT [1] Bad padding: ~D bytes at TCP-Buf: ~D"
		      field-length idx)))
	 ;; If output string contains delimiters [Value Multiplicity > 1]
	 ;; divide string into fragments and return LIST of the fragments.
	 ;; If VM = 1 [no delimiters], return singleton list of the string.
	 (let ((output-string
		 (make-array (length output-charlist)
			     :element-type 'base-char
			     :initial-contents output-charlist)))
	   (declare (type simple-base-string output-string))
	   (cond
	     ((or (eq VR-symbol 'LT)
		  (eq VR-symbol 'ST))
	      (list output-string))
	     (t (do ((delimiter
		       (position #\\ output-string :test #'char=)
		       (position #\\ output-string :test #'char=))
		     (char-bag '(#\Space #\Null))
		     (multiple-strings '()))
		    ((null delimiter)
		     (cond ((consp multiple-strings)
			    ;; VM > 1: return list of substrings.
			    (cond
			      ((> (length output-string) 0)
			       ;Null-Padding Fix here.
			       (nreverse
				 (cons (string-trim char-bag output-string)
				       multiple-strings)))
			      (t (nreverse multiple-strings))))
			   ((> (length output-string) 0)
			    ;; VM = 1: return singleton list.  Was already
			    ;; STRING-TRIMed when still a char list.
			    (list output-string))
			   ;; No value: return NIL.
			   (t nil)))
		  (declare (type list char-bag multiple-strings))
		  (when (> (the fixnum delimiter) 0)
		    ;Null-Padding Fix here.
		    (push (string-trim char-bag
				       (subseq output-string 0 delimiter))
			  multiple-strings))
		  (setq output-string
			(subseq output-string
				(the fixnum (1+ (the fixnum delimiter))))))))))

      (declare (type list output-charlist)
	       (type fixnum ptr limit))

      (push (code-char (aref tcp-buffer ptr)) output-charlist))))

;;;=============================================================
;;; End.
