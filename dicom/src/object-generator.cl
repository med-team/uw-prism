;;;
;;; object-generator
;;;
;;; Generator for DICOM Objects.
;;; Contains functions used in Client only.
;;;
;;; 27-Dec-2000 BobGian change args to GENERATE-OBJECT - globals supplied
;;;   locally rather than passed as parameters.
;;;   Improve logging of lists/strings and object descriptors.
;;; 16-Apr-2002 BobGian MISHAP called in GENERATE-OBJECT prints
;;;   list-structure representation of output generated so far.
;;; 16-Apr-2002 BobGian convert GENERATE-OBJECT to return list structure
;;;   which is passed back to SEND-PDU for fragmentation [if needed]
;;;   and packaging into TCP-Buffer for transmission.
;;; 19-Jun-2002 BobGian float->string conversions round to one digit after
;;;   decimal place.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 21-Nov-2003 Bobgian: Encoding variable-length strings with fixed min/max
;;;   lengths - string padded to min or truncated to max length rather than
;;;   invoking error call (MISHAP).
;;; 27-Apr-2004 BobGian: Rounding of float values [was to 2 decimal places,
;;;   to avoid Elekta rounding problem] changed to 4 decimal places, to allow
;;;   greater accuracy in dose values in Gray [2-decimal-place conversion was
;;;   causing inconsistent rounding of dose for Dose-Monitoring points].
;;;

(in-package :dicom)

;;;=============================================================

(defun generate-object (object env output-itemlist &aux object-start)

  (declare (type list object output-itemlist object-start))

  (do ((groupnum 0) (elemnum 0) (elemdata) (itemlen 0)
       (VR-symbol) (VR-descriptor) (datatype) (string-padding)
       (lisptype) (data-item) (data-values)
       (g/e-alist *group/elemname-alist*)
       (datatype-alist *datatype-alist*)
       (input-itemlist object (cdr input-itemlist))
       (tag object))                            ;Binding for declaration only.
      ((null input-itemlist)
       output-itemlist)

    (declare (type symbol VR-symbol string-padding)
	     (type list input-itemlist g/e-alist datatype-alist data-values
		   elemdata VR-descriptor datatype)
	     (type cons tag)
	     (type fixnum groupnum elemnum itemlen))

    (setq data-item (car input-itemlist)
	  tag (car data-item)
	  data-values (cdr data-item)
	  groupnum (car tag)
	  elemnum (cdr tag))

    (cond ((oddp groupnum)
	   (mishap env output-itemlist "GENERATE-OBJECT [1] Private group: ~S"
		   data-item))

	  ((consp (setq elemdata (assoc tag g/e-alist :test #'equal)))
	   ;; Item/Sequence delimiters handled here.
	   (setq VR-symbol (second elemdata)))

	  (t (mishap env output-itemlist
		     "GENERATE-OBJECT [2] Missing data definition: ~S"
		     data-item)))

    (unless (consp (setq VR-descriptor
			 (assoc VR-symbol datatype-alist :test #'eq)))
      ;; Missing data type definition in Data Dictionary.
      (mishap env output-itemlist "GENERATE-OBJECT [3] Bad type: ~S"
	      data-item))

    (push (list 'fixnum 2 :Little-Endian groupnum) output-itemlist)
    (push (list 'fixnum 2 :Little-Endian elemnum) output-itemlist)
    ;; Save starting index of the object so Value-Length slot can be
    ;; backpatched after representation of object is generated.  Length value
    ;; is always encoded [in default Transfer Syntax] as a 4-byte Little-Endian
    ;; fixnum.  It is stored 4 bytes before Object itself.
    ;; This is a token representing the length field whose value will be
    ;; filled in later by measuring the length of the object representation
    ;; back to [but not including] this length field.
    (push (list 'fixnum 4 :Little-Endian nil) output-itemlist)
    ;; OBJECT-START servers as pointer to start of object for length
    ;; computation as well as pointer to token just pushed above which
    ;; is the token to be filled in with length value later.
    (setq object-start output-itemlist)

    (cond
      ((cddr (the cons VR-descriptor))              ;List-Length is at least 3
       (setq string-padding (fourth VR-descriptor)
	     datatype (third VR-descriptor)
	     lisptype (first datatype))

       (cond
	 ((eq lisptype 'fixnum)        ;Could be single fixnum or list of them
	  (setq itemlen (second datatype))
	  (dolist (itemval data-values)
	    (unless (typep itemval 'fixnum)
	      (mishap env output-itemlist "GENERATE-OBJECT [4] Bad value: ~S"
		      data-item))
	    (unless (or (= itemlen 2)
			(= itemlen 4))
	      (mishap env output-itemlist "GENERATE-OBJECT [5] Bad length: ~S"
		      data-item))
	    (push (list 'fixnum itemlen :Little-Endian itemval)
		  output-itemlist)))

	 ((eq lisptype 'string)        ;Could be single string or list of them

	  (do ((items data-values (cdr items))
	       (strlen-low (second datatype))
	       (strlen-high (or (third datatype) (second datatype)))
	       (itemval) (totlen 0))
	      ((null items)
	       ;; After constructing the entire string [concatenation of all
	       ;; components, if more than one] pad the composite string if
	       ;; needed.  Spec requires all slots in C-Store command and data
	       ;; PDVs to be of even length.
	       (cond ((evenp totlen))
		     ((eq string-padding :Null-Pad)
		      (push 0 output-itemlist))
		     (t (push #.(char-code #\Space) output-itemlist))))
	    (declare (type list items)
		     (type fixnum totlen strlen-low strlen-high))
	    (setq itemval (car items))
	    ;; "String" items may be presented as other datatypes,
	    ;; with generator expected to convert them to appropriate string.
	    (cond
	      ((typep itemval 'simple-base-string)) ;Already string.
	      ((typep itemval 'fixnum)              ;Integer -> string.
	       (setq itemval (format nil "~D" itemval)))
	      ((typep itemval 'single-float)        ;Float -> string.
	       ;; Round float->string conversions to four digits after decimal.
	       ;; Needed due to Elekta rounding problem.
	       (setq itemval (format nil "~,4F" itemval)))
	      (t (mishap env output-itemlist
			 "GENERATE-OBJECT [6] Bad string: ~S" data-item)))
	    ;; After conversion to string, we can check component lengths.
	    ;; ITEMLEN is len of each component, not of entire composite str.
	    (setq itemlen (length (the simple-base-string itemval)))
	    (cond ((> itemlen strlen-high)
		   (setq itemval (subseq itemval 0 strlen-high))
		   (setq itemlen strlen-high))
		  ((< itemlen strlen-low)
		   (setq itemval (concatenate
				   'string
				   itemval
				   (make-string
				     (the fixnum (- strlen-low itemlen))
				     :initial-element #\Space)))
		   (setq itemlen strlen-low)))
	    (setq totlen (the fixnum (+ totlen itemlen)))
	    ;; Non-padded components had better already be even length.
	    (when (eq string-padding :No-Pad)
	      (unless (evenp itemlen)
		(mishap env output-itemlist
			"GENERATE-OBJECT [7] Bad len/pad: ~S" data-item)))
	    (push (list 'string itemlen :No-Pad itemval) output-itemlist)
	    (when (cdr items)
	      ;; If value multiplicity > 1, separate strings by #\\ delimiter.
	      (push #.(char-code #\\) output-itemlist)
	      (setq totlen (the fixnum (1+ totlen))))))

	 ;; Missing implementation of data type defined in Data Dictionary.
	 (t (mishap env output-itemlist
		    "GENERATE-OBJECT [8] Non-implemented datatype: ~S"
		    data-item))))

      ;; Sequence markers initiate generation of contained sub-objects.
      ;; Sequences are represented with Implicit VR = SQ of items, each an
      ;; item of explicit length [in Implicit VR default transfer syntax].
      ((eq VR-symbol 'SQ)
       (dolist (itemval data-values)
	 ;;Tag for Item in Sequence.
	 (push (list 'fixnum 2 :Little-Endian #xFFFE) output-itemlist)
	 (push (list 'fixnum 2 :Little-Endian #xE000) output-itemlist)
	 ;;Item Length field to be backpatched.
	 (push (list 'fixnum 4 :Little-Endian nil) output-itemlist)
	 ;;Save length-field token for later backpatching.
	 (let ((item-start output-itemlist))
	   (declare (type list item-start))
	   (setq output-itemlist (generate-object itemval env output-itemlist))
	   ;; Backpatch deferred item length field.
	   (setf (fourth (car item-start))
		 (object-length output-itemlist item-start)))))

      ;; Unrecognized situation.
      (t (mishap env output-itemlist
		 "GENERATE-OBJECT [9] Unrecognized object: ~S" data-item)))

    ;; Backpatch deferred Value-Length slot for entire object [or sub-object,
    ;; if this is a recursive call to generate a component in a sequence].
    (setf (fourth (car object-start))
	  (object-length output-itemlist object-start))))

;;;=============================================================
;;; End.
