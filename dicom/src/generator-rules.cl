;;;
;;; generator-rules
;;;
;;; Rules for DICOM PDU and Message Generation.
;;; Contains declarations common to Client and Server.
;;;
;;; 27-Apr-2001 BobGian change A-Associate-AC rule to echo Called-AE-Title
;;;   used by caller rather than global value from configuration file.
;;; 23-Apr-2002 BobGian UIDs in A-Assoc-RQ/AC :Null-Pad -> :No-Pad.
;;; 25-Apr-2002 BobGian change rule for C-Store-RTPlan SOP to send command
;;;   and data PDVs in separate PDUs - needed for fragmentation fix.
;;; 30-Apr-2002 BobGian replace Presentation Context ID with constant #x01.
;;; 10-May-2002 BobGian replace call to procedural computation of PDU and
;;;   PDV length fields in rules by :Place-Holder tokens, since computation
;;;   is done in SEND-PDU and would be redundant in rule expansion.
;;; 30-Jul-2002 BobGian :SOP-Class-Ext-Neg-Item-AC (optional item) removed
;;;   from :A-Associate-AC PDU (already not used in :A-Associate-RQ PDU).
;;; Jul/Aug 2002 BobGian:
;;;   SOP-Class-Extended-Negotiation-Item removed from Assoc-RP PDU - not
;;;     echoed, even if present in Assoc-RQ.
;;;   Extended-Negotiation documented as "not supported" in conformance report.
;;;   Environment accessor name change: <MCH -> <PDV-MCH.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================
;;; Rules for Generating Transmitted PDUs.
;;;
;;; Variables commented "Global Env" in <ENCODE-VAR forms have values
;;; transmitted via arguments to SEND-PDU, by extending environment with
;;; top-level accessible pairs.
;;;
;;; Otherwise, variables get their values from the environment via an access
;;; chain provided as explicit arguments in <ENCODE-VAR or <LOOKUP-VAR terms.

(defparameter *Generator-Rule-List*
  `(

    ;;=============================================
    ;; PDU Generation Rules.
    ;;=============================================

    ;; A-Associate-RQ PDU rule == COMPLETE PDU.

    (:A-Associate-RQ                                ;SCU-only

      #x01                               ;A-Associate-RQ PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      ;; Protocol Version [2-byte bitstring, val = #x0001]
      (=fixnum-bytes #x0001 2 :Big-Endian)

      (=constant-bytes #x00 2)                      ;Reserved field [2 bytes]

      ;; Called AE Title [16-byte string] -- Remote host being called.
      (<encode-var Called-AE-Title string 16 :Space-Pad)    ;Global Env

      ;; Calling AE Title [16-byte string] -- Client name may depend on target.
      (<encode-var Calling-AE-Title string 16 :Space-Pad)   ;Global Env

      (=constant-bytes #x00 32)                     ;Reserved field [32 bytes]

      :Application-Context-Item

      ;; Single Presentation Context Item -- global env carries proposals.
      ;; If desired to present more than one, encode them here explicitly.
      :Presentation-Context-Item-RQ

      :User-Information-Item-RQ)

    ;;---------------------------------------------
    ;; Presentation Context Item rule for Assoc-RQ PDU.

    (:Presentation-Context-Item-RQ                  ;SCU-only

      #x20                        ;Presentation Context Item type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Field Length [2 bytes -- PC-ID to end of last Transfer Syntax Item]
      (<item-length 2 :Big-Endian)

      #x01                                   ;Presentation Context ID [1 byte]

      (=constant-bytes #x00 3)                      ;Reserved field [3 bytes]

      :Abstract-Syntax-Item-RQ

      ;; 1 or more Transfer Syntax Items allowed.  DicomRT uses NEMA default.
      :Transfer-Syntax-Item)

    ;;---------------------------------------------
    ;; Abstract Syntax Item rule for Assoc-RQ PDU.

    (:Abstract-Syntax-Item-RQ                       ;SCU-only

      #x30                             ;Abstract Syntax Item type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Abstract Syntax Name [SOP Class UID String] field length [2 bytes]
      (<encode-var SOP-Class-UID-Len fixnum 2 :Big-Endian)  ;Global Env

      ;; Abstract Syntax Name [SOP Class UID String] -- variable-length.
      (<encode-var SOP-Class-UID-Str                ;Global Env
		   string
		   (<lookup-var SOP-Class-UID-Len)  ;Global Env
		   :No-Pad))

    ;;---------------------------------------------
    ;; User Information Item rule for Assoc-RQ PDU.

    (:User-Information-Item-RQ                      ;SCU-only

      #x50                            ;User Information Item Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      (<item-length 2 :Big-Endian)      ;User Data Item Field Length [2 bytes]

      :Max-DataField-Len-Item

      :Implementation-Class-UID-Item

      ;; Optional Asynchronous Operations Item.
      :Asynchronous-Ops-Item

      ;; Optional SCP/SCU Role Item.
      :SCP/SCU-Role-Item-RQ

      :Implementation-Version-Name-Item

      ;; Optional SOP Class Extended Negotiation Item.
      #+ignore
      :SOP-Class-Ext-Neg-Item-RQ)                   ;Not currently used.

    ;;---------------------------------------------
    ;; SCP/SCU Role Item rule for Assoc-RQ PDU.

    (:SCP/SCU-Role-Item-RQ                          ;SCU-only

      #x54                                     ;SCP/SCU Role Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      (<item-length 2 :Big-Endian)   ;SCP/SCU Role Item field length [2 bytes]

      ;; SOP Class UID Item Field Length [2 bytes]
      (<encode-var Role-SOP-Class-UID-Len fixnum 2 :Big-Endian) ;Global Env

      ;; SOP Class UID String [variable-len byte string]
      (<encode-var Role-SOP-Class-UID-Str           ;Global Env
		   string
		   (<lookup-var Role-SOP-Class-UID-Len) ;Global Env
		   :No-Pad)

      #x01                         ;Requester's proposal to be SCU [requested]

      #x00)                    ;Requester's proposal to be SCP [not-requested]

    ;;---------------------------------------------
    ;; Optional SOP Class Extended Negotiation Item rule for Assoc-RQ PDU.
    ;; Values for variable instantiation must be pushed onto environment
    ;; so as to be available to instantiator functions.

    #+ignore             ;SCU-only, but DicomRT doesn't implement this anyway.
    (:SOP-Class-Ext-Neg-Item-RQ

      #x56                   ;SOP Class Extended Negotiation Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Extended Negotiation Item Field Length [2 bytes] (not used at present)
      (<encode-var Ext-Negotiation-Len fixnum 2 :Big-Endian)    ;Global Env

      ;; SOP Class UID Item Field Length [2 bytes] (not used at present)
      (<encode-var EN-SOP-Class-UID-Len fixnum 2 :Big-Endian)   ;Global Env

      ;; SOP Class UID String [variable-len byte string] (not used at present)
      (<encode-var EN-SOP-Class-UID-Str             ;Global Env
		   string
		   (<lookup-var EN-SOP-Class-UID-Len)   ;Global Env
		   :No-Pad)

      ;; Extended Negotiation data (not used at present)
      (<encode-var Ext-Negotiation-Str              ;Global Env
		   string
		   (<funcall -
			     (<lookup-var Ext-Negotiation-Len)  ;Global Env
			     (<lookup-var EN-SOP-Class-UID-Len) ;Global Env
			     2)
		   :No-Pad))

    ;;=============================================
    ;; A-Associate-AC PDU rule == COMPLETE PDU.

    (:A-Associate-AC

      #x02                               ;A-Associate-AC PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      ;; Protocol Version [2-byte bitstring, val = #x0001]
      (=fixnum-bytes #x0001 2 :Big-Endian)

      (=constant-bytes #x00 2)                      ;Reserved field [2 bytes]

      ;; Called AE Title [16-byte string] -- Local host accepting association.
      (<encode-var Called-AE-Title string 16 :Space-Pad :A-Associate-RQ)

      ;; Calling AE Title [16-byte string] -- Remote host requesting assoc.
      (<encode-var Calling-AE-Title string 16 :Space-Pad :A-Associate-RQ)

      (=constant-bytes #x00 32)                     ;Reserved field [32 bytes]

      :Application-Context-Item

      ;; 1 or more Presentation Context Items -- global environment
      ;; carries proposals.
      (:Set :Presentation-Context-Item-AC)

      :User-Information-Item-AC)

    ;;---------------------------------------------
    ;; Presentation Context Item rule for Assoc-AC PDU.

    (:Presentation-Context-Item-AC

      #x21                        ;Presentation Context Item type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Field Length [2 bytes -- PC-ID to end of last Transfer Syntax Item]
      (<item-length 2 :Big-Endian)

      (<encode-var PC-ID fixnum 1)    ;Presentation Context ID [1 byte] Global

      #x00                                          ;Reserved field [1 byte]

      ;; Result/Reason slot:
      ;;   0: Acceptance
      ;;   1: User-Rejecttion
      ;;   2: No-Reason-Given (Provider-Rejection)
      ;;   3: Abstract-Syntax Not Supported (Provider-Rejection)
      ;;   4: Transfer-Syntax Not Supported (Provider-Rejection)
      (<encode-var Result/Reason fixnum 1)          ;Global Env

      #x00                                          ;Reserved field [1 byte]

      ;; Transfer Syntax Item is significant only if Result/Reason
      ;; is zero [Acceptance]; it is ignored if Result/Reason is non-zero
      ;; [indicating Rejection].  If accepting, server selects only the
      ;; NEMA default transfer syntax.
      :Transfer-Syntax-Item)

    ;;---------------------------------------------
    ;; User Information Item rule for Assoc-AC PDU.

    (:User-Information-Item-AC

      #x50                            ;User Information Item Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      (<item-length 2 :Big-Endian)      ;User Data Item Field Length [2 bytes]

      :Max-DataField-Len-Item

      :Implementation-Class-UID-Item

      ;; Optional Asynchronous Operations Item -- Echo if Assoc-RQ had it.
      (<if Item-Present?
	   (:Asynchronous-Ops-Item :User-Information-Item-RQ :A-Associate-RQ)
	   :Asynchronous-Ops-Item)

      ;; Optional SCP/SCU Role Item -- Echo if Assoc-RQ included this item.
      ;; It is legal for Acceptor not to respond to Requestor's item, in
      ;; which case Requestor defaults to SCU and Acceptor to SCP.
      #+ignore                                      ;Not currently used.
      (<if Item-Present?
	   (:SCP/SCU-Role-Item :User-Information-Item-RQ :A-Associate-RQ)
	   :SCP/SCU-Role-Item-AC)

      :Implementation-Version-Name-Item

      ;; Optional SOP Class Extended Negotiation Item -- One per SOP-Class-UID
      ;; item received in Assoc-RQ PDU.  Currently implemented as a single
      ;; optional item.  Correct version would answer one for EACH one received
      ;; in A-Associate-RQ.  Note that Item-Present? answers Yes [non-NIL] if
      ;; ANY of one or more are present.
      #+ignore
      (<if Item-Present?
	   (:SOP-Class-Ext-Neg-Item :User-Information-Item-RQ :A-Associate-RQ)
	   :SOP-Class-Ext-Neg-Item-AC))             ;Not currently used.

    ;;---------------------------------------------
    ;; SCP/SCU Role Item rule for Assoc-AC PDU.

    (:SCP/SCU-Role-Item-AC

      #x54                                     ;SCP/SCU Role Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      (<item-length 2 :Big-Endian)   ;SCP/SCU Role Item field length [2 bytes]

      ;; SOP Class UID Item Field Length [2 bytes]
      (<encode-var Role-SOP-Class-UID-Len fixnum 2 :Big-Endian
		   :SCP/SCU-Role-Item
		   :User-Information-Item
		   :A-Associate-RQ)

      ;; SOP Class UID String [variable-len byte string]
      (<encode-var Role-SOP-Class-UID-Str
		   string
		   (<lookup-var Role-SOP-Class-UID-Len
				:SCP/SCU-Role-Item
				:User-Information-Item
				:A-Associate-RQ)
		   :No-Pad
		   :SCP/SCU-Role-Item
		   :User-Information-Item
		   :A-Associate-RQ)

      #x01                          ;Requester's proposal to be SCU [accepted]

      #x00)                         ;Requester's proposal to be SCP [rejected]

    ;;---------------------------------------------
    ;; Optional SOP Class Extended Negotiation Item rule for Assoc-AC PDU.
    ;; Currently implemented as a single optional item.  Correct version
    ;; would answer one for EACH one received in RQ.  How to differentiate
    ;; between them?

    #+ignore             ;SCU-only, but DicomRT doesn't implement this anyway.
    (:SOP-Class-Ext-Neg-Item-AC

      #x56                   ;SOP Class Extended Negotiation Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Extended Negotiation Item Field Length [2 bytes] (not used at present)
      (<encode-var Ext-Negotiation-Len fixnum 2 :Big-Endian ;Local Env
		   :SOP-Class-Ext-Neg-Item      ;Parsed but currently ignored.
		   :User-Information-Item
		   :A-Associate-RQ)

      ;; SOP Class UID Item Field Length [2 bytes] (not used at present)
      (<encode-var EN-SOP-Class-UID-Len fixnum 2 :Big-Endian    ;Local Env
		   :SOP-Class-Ext-Neg-Item      ;Parsed but currently ignored.
		   :User-Information-Item
		   :A-Associate-RQ)

      ;; SOP Class UID String [variable-len byte string] (not used at present)
      (<encode-var EN-SOP-Class-UID-Str             ;Local Env
		   string
		   (<lookup-var EN-SOP-Class-UID-Len    ;Local Env
				:SOP-Class-Ext-Neg-Item ;Currently ignored.
				:User-Information-Item
				:A-Associate-RQ)
		   :No-Pad
		   :SOP-Class-Ext-Neg-Item          ;Currently ignored.
		   :User-Information-Item
		   :A-Associate-RQ)

      ;; Extended Negotiation data -- varies with SOP class (not used)
      (<encode-var Ext-Negotiation-Str              ;Local Env
		   string
		   (<funcall -
			     (<lookup-var Ext-Negotiation-Len   ;Local Env
					  :SOP-Class-Ext-Neg-Item   ;Ignored.
					  :User-Information-Item
					  :A-Associate-RQ)
			     (<lookup-var EN-SOP-Class-UID-Len  ;Local Env
					  :SOP-Class-Ext-Neg-Item   ;Ignored.
					  :User-Information-Item
					  :A-Associate-RQ)
			     2)
		   :No-Pad
		   :SOP-Class-Ext-Neg-Item      ;Parsed but currently ignored.
		   :User-Information-Item
		   :A-Associate-RQ))

    ;;=============================================
    ;; Application Context Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Application-Context-Item

      #x10                         ;Application Context Item type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Application Context Name Length [2 bytes]
      (=fixnum-bytes (length *Application-Context-Name*) 2 :Big-Endian)

      ;; Application Context Name [variable length]
      (=string-bytes *Application-Context-Name*
		     (length *Application-Context-Name*)
		     :No-Pad))

    ;;---------------------------------------------
    ;; Transfer Syntax Item rule for Assoc-RQ and Assoc-AC PDUs.
    ;; We propose and accept proposals only of the NEMA default.

    (:Transfer-Syntax-Item

      #x40                             ;Transfer Syntax Item type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Transfer Syntax Name field length [2 bytes]
      (=fixnum-bytes (length *Transfer-Syntax-Name*) 2 :Big-Endian)

      ;; Transfer Syntax Name [variable-length byte string]
      ;; Proposing Implicit Little-Endian Transfer Syntax, NEMA.
      ;; All systems must support this TSN, and it is only one we support.
      (=string-bytes *Transfer-Syntax-Name*
		     (length *Transfer-Syntax-Name*)
		     :No-Pad))

    ;;---------------------------------------------
    ;; Maximum DataField Length Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Max-DataField-Len-Item

      #x51                         ;Maximum Length Sub-Item field tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Maximum Length Received field length [val = 4, 2 bytes]
      (=fixnum-bytes 4 2 :Big-Endian)

      ;; Maximum PDU Length as 4 byte integer.  Zero -> no limit.
      ;; TCP buffer is statically allocated.  Some scanners send weird PDU
      ;; length when unlimited PDU datalength field option is used.
      (=fixnum-bytes #.PDU-Bufsize 4 :Big-Endian))

    ;;---------------------------------------------
    ;; Implementation Class UID Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Implementation-Class-UID-Item

      #x52                         ;Implementation Class UID Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Implementation Class UID Item Field Length [2 bytes]
      (=fixnum-bytes (length *Implementation-Class-UID*) 2 :Big-Endian)

      ;; Implementation Class UID [variable-length byte string]
      (=string-bytes *Implementation-Class-UID*
		     (length *Implementation-Class-UID*)
		     :No-Pad))

    ;;---------------------------------------------
    ;; Asynchronous Operations Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Asynchronous-Ops-Item

      #x53                          ;Asynchronous Operations Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Asynchronous Operations Item field length [val = 4, 2 bytes]
      (=fixnum-bytes 4 2 :Big-Endian)

      ;; Synchronous operation only supported by our system.
      ;; Max Num Ops Invoked Asynchronously [val = 1, 0 -> unlimited, 2 bytes]
      (=fixnum-bytes 1 2 :Big-Endian)

      ;; Synchronous operation only supported.
      ;; Max Number of Operations Performed Asynchronously
      ;; [val = 1, 0 -> unlimited, 2 bytes]
      (=fixnum-bytes 1 2 :Big-Endian))

    ;;---------------------------------------------
    ;; Implementation Version Name Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Implementation-Version-Name-Item

      #x55                      ;Implementation Version Name Item tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      ;; Implementation Version Name Item Field Length [2 bytes]
      (=fixnum-bytes (length *Implementation-Version-Name*) 2 :Big-Endian)

      ;; Implementation Version Name [variable-len byte string]
      (=string-bytes *Implementation-Version-Name*
		     (length *Implementation-Version-Name*)
		     :No-Pad))

    ;;=============================================
    ;; A-Associate-RJ PDU rule == COMPLETE PDU.

    (:A-Associate-RJ

      #x03                               ;A-Associate-RJ PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      #x00                                          ;Reserved field [1 byte]

      ;; 1: Rejection-Permanent
      ;; 2: Rejection-Transient
      (<encode-var RJ-Result fixnum 1)              ;Global Env

      ;; 1: UL Service-User
      ;; 2: UL Service-Provider [ACSE]
      ;; 3: UL Service-Provider [Presentation Layer]
      (<encode-var RJ-Source fixnum 1)              ;Global Env

      ;; If RJ-Source = 1:
      ;;   1: No-Reason-Given
      ;;   2: Application-Context-Name-Not-Supported
      ;;   3: Calling-AE-Title-Not-Recognized
      ;;   4-6: Reserved
      ;;   7: Called-AE-Title-Not-Recognized
      ;;   8-10: Reserved
      ;;
      ;; If RJ-Source = 2:
      ;;   1: No-Reason-Given
      ;;   2: Protocol-Version-Not-Supported
      ;;
      ;; If RJ-Source = 3:
      ;;   0: Reserved
      ;;   1: Temporary-Congestion
      ;;   2: Local-Limit-Exceeded
      ;;   3-7: Reserved
      (<encode-var RJ-Diagnostic fixnum 1))         ;Global Env

    ;;=============================================
    ;; A-Release-RQ PDU rule == COMPLETE PDU.

    (:A-Release-RQ

      #x05                                 ;A-Release-RQ PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      (=constant-bytes #x00 4))                     ;Reserved field [4 bytes]

    ;;=============================================
    ;; A-Release-RSP PDU rule == COMPLETE PDU.

    (:A-Release-RSP

      #x06                                ;A-Release-RSP PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      (=constant-bytes #x00 4))                     ;Reserved field [4 bytes]

    ;;=============================================
    ;; A-Abort PDU rule == COMPLETE PDU.

    (:A-Abort

      #x07                                      ;A-Abort PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      (=constant-bytes #x00 2)                      ;Reserved field [2 bytes]

      ;; 0: UL Service-User-initiated
      ;; 1: Reserved
      ;; 2: UL Service-Provider-initiated
      (<encode-var Abort-Source fixnum 1)           ;Global Env

      ;; If Abort-Source = 0:
      ;;   Not Significant [ignored when received]
      ;;
      ;; If Abort-Source = 2:
      ;;   0: Reason Not Specified
      ;;   1: Unrecognized PDU
      ;;   2: Unexpected PDU
      ;;   3: Reserved
      ;;   4: Unrecognized PDU Parameter
      ;;   5: Unexpected PDU Parameter
      ;;   6: Invalid PDU Parameter Value
      (<encode-var Abort-Diagnostic fixnum 1))      ;Global Env

    ;;=============================================
    ;; DICOM Message Generation Rules.
    ;;=============================================

    ;; C-Echo-RQ PDU Command/Message rule == COMPLETE PDU.

    (:C-Echo-RQ

      #x04                                    ;P-Data-TF PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      ;; PDV Length [4 bytes]
      ;; Length is that of PC-ID byte + Control-Header byte + Message length.
      :Place-Holder                              ;PDV-Message length + 2 bytes

      #x01                                   ;Presentation Context ID [1 byte]

      (<pdv-mch :Command)                     ;Message Control Header [1 byte]

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length
      ;;
      (<item-length 4 :Little-Endian)               ;Value

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      ;; Echo-Verification SOP Class UID Item Field Length [2 bytes]
      (=fixnum-bytes
	(even-length *Echo-Verification-Service*) 4 :Little-Endian)

      ;; Echo-Verification SOP Class UID [variable-length byte string]
      (=string-bytes *Echo-Verification-Service*
		     (even-length *Echo-Verification-Service*)
		     :Null-Pad)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0030 2 :Little-Endian)       ;Code for C-Echo-RQ

      ;;--------- Element 4: Message ID [message being responded to]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0110)
      (=fixnum-bytes #x0110 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes 1 2 :Little-Endian)            ;Message ID

      ;;--------- Element 5: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0101 2 :Little-Endian))      ;Code for No-Data

    ;;=============================================
    ;; C-Echo-RSP PDU Command/Message rule == COMPLETE PDU.

    (:C-Echo-RSP

      #x04                                    ;P-Data-TF PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      ;; PDV Length [4 bytes]
      ;; Length is that of PC-ID byte + Control-Header byte + Message length.
      :Place-Holder                              ;PDV-Message length + 2 bytes

      (<encode-var PC-ID fixnum 1)    ;Presentation Context ID [1 byte] Global

      (<pdv-mch :Command)                     ;Message Control Header [1 byte]

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length
      ;;
      (<item-length 4 :Little-Endian)               ;Value

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      (<encode-var Echo-SOP-Class-UID-Len fixnum 4 :Little-Endian   ;Length
		   :C-Echo-RQ)

      (<encode-var Echo-SOP-Class-UID-Str           ;Value
		   string
		   (<lookup-var Echo-SOP-Class-UID-Len :C-Echo-RQ)
		   :Null-Pad
		   :C-Echo-RQ)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x8030 2 :Little-Endian)       ;Code for C-Echo-RSP

      ;;--------- Element 4: Message ID [message being responded to]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0120)
      (=fixnum-bytes #x0120 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (<encode-var Echo-Msg-ID fixnum 2 :Little-Endian :C-Echo-RQ)  ;Value

      ;;--------- Element 5: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0101 2 :Little-Endian)       ;Code for No-Data

      ;;--------- Element 6: Response Status
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0900)
      (=fixnum-bytes #x0900 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0000 2 :Little-Endian))      ;Code for Success

    ;;=============================================
    ;; C-Store-RQ PDU for RTPlan Command == COMPLETE PDU.

    (:C-Store-RTPlan-Command

      #x04                                    ;P-Data-TF PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      :Place-Holder                              ;PDV-Message length + 2 bytes

      #x01                                   ;Presentation Context ID [1 byte]

      (<pdv-mch :Command)                     ;Message Control Header [1 byte]

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length

      (<item-length 4 :Little-Endian)               ;Value

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      ;; RTPlan SOP Class UID Item Field Length [2 bytes]
      (=fixnum-bytes (even-length *RTPlan-Storage-Service*) 4 :Little-Endian)

      ;; RTPlan SOP Class UID [variable-length byte string]
      (=string-bytes *RTPlan-Storage-Service*
		     (even-length *RTPlan-Storage-Service*)
		     :Null-Pad)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0001 2 :Little-Endian)       ;Code for C-Store-RQ

      ;;--------- Element 4: Message ID [message being sent]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0110)
      (=fixnum-bytes #x0110 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes 1 2 :Little-Endian)            ;Value

      ;;--------- Element 5: Priority
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0700)
      (=fixnum-bytes #x0700 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      ;; #x0002 -> LOW, #x0000 -> MEDIUM, #x0001 -> HIGH
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Value

      ;;--------- Element 6: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Code for Data-Present

      ;;--------- Element 7: Affected SOP Instance UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,1000)
      (=fixnum-bytes #x1000 2 :Little-Endian)

      (<encode-var Store-SOP-Instance-UID-Len fixnum 4 :Little-Endian) ;Length

      (<encode-var Store-SOP-Instance-UID-Str       ;Value
		   string
		   (<lookup-var Store-SOP-Instance-UID-Len) ;Global Env
		   :Null-Pad))

    ;;---------------------------------------------
    ;; C-Store-RQ PDU for RTPlan Data == COMPLETE PDU.
    ;; Since fragmentation likely will be required, we send each
    ;; data fragment in a PDU containing but a single PDV .

    (:C-Store-RTPlan-Data

      #x04                                    ;P-Data-TF PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      :Place-Holder                              ;PDV-Message length + 2 bytes

      #x01                                   ;Presentation Context ID [1 byte]

      (<pdv-mch :Data)                        ;Message Control Header [1 byte]

      (<encode-data RTPlan-DataSet))                ;Dataset for entire RTPlan

    ;;=============================================
    ;; C-Store-RSP PDU Command/Message rule == COMPLETE PDU.

    (:C-Store-RSP

      #x04                                    ;P-Data-TF PDU Type tag [1 byte]

      #x00                                          ;Reserved field [1 byte]

      :Place-Holder                                 ;PDU Length [4 bytes]

      ;; PDV Length [4 bytes]
      ;; Length is that of PC-ID byte + Control-Header byte + Message length.
      :Place-Holder                              ;PDV-Message length + 2 bytes

      (<encode-var PC-ID fixnum 1)    ;Presentation Context ID [1 byte] Global

      (<pdv-mch :Command)                     ;Message Control Header [1 byte]

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length

      (<item-length 4 :Little-Endian)               ;Value

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      (<encode-var Store-SOP-Class-UID-Len fixnum 4 :Little-Endian  ;Length
		   :C-Store-RQ)

      (<encode-var Store-SOP-Class-UID-Str          ;Value
		   string
		   (<lookup-var Store-SOP-Class-UID-Len :C-Store-RQ)
		   :Null-Pad
		   :C-Store-RQ)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x8001 2 :Little-Endian)       ;Code for C-Store-RSP

      ;;--------- Element 4: Message ID [message being responded to]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0120)
      (=fixnum-bytes #x0120 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (<encode-var Store-Msg-ID fixnum 2 :Little-Endian ;Length
		   :C-Store-RQ)

      ;;--------- Element 5: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0101 2 :Little-Endian)       ;Code for No-Data

      ;;--------- Element 6: Response Status
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0900)
      (=fixnum-bytes #x0900 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Code for Success

      ;;--------- Element 7: Affected SOP Instance UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,1000)
      (=fixnum-bytes #x1000 2 :Little-Endian)

      (<encode-var Store-SOP-Instance-UID-Len fixnum 4 :Little-Endian  ;Length
		   :C-Store-RQ)

      (<encode-var Store-SOP-Instance-UID-Str       ;Value
		   string
		   (<lookup-var Store-SOP-Instance-UID-Len :A-Associate-RQ)
		   :Null-Pad
		   :C-Store-RQ))

    ;;=============================================

    ))

;;;=============================================================

(eval-when (EVAL LOAD)
  (compile-rules *Generator-Rule-List* :Generator-Rule)
  (setq *Generator-Rule-List* nil))

;;;=============================================================
;;; End.
