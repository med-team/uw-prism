;;;
;;; actions-client
;;;
;;; DICOM Upper-Layer Protocol Action functions for Client only.
;;; Contains functions used in Client only.
;;;
;;; 26-Dec-2000 BobGian wrap IGNORE-ERRORS around error-prone fcns.
;;;  Include error-recovery options in case those fcns barf.
;;;  Change a few local variable names for consistency.
;;; 26-Dec-2000 BobGian replace used-once locals in OPEN-CONNECTION.
;;; 11-Apr-2001 BobGian remove name-server lookup and printing of hostname
;;;  in OPEN-CONNECTION.  IP addr has same information and is much faster.
;;; 11-Apr-2001 BobGian add more explicit error reporting to OPEN-CONNECTION.
;;; 10-Sep-2001 BobGian remove IGNORE-ERRORS - errors at this level
;;;  should be debugged rather than ignored or just logged.
;;; 23-Jan-2002 BobGian install REPORT-ERROR specialized to Client mode.
;;;  Install stubs: PARSE-OBJECT and WRITE-DICOM-OUTPUT [used by Server only].
;;; 13-Mar-2002 BobGian REPORT-ERROR dumps environment and TCP-BUFFER
;;;  if args supplied.  Start/End indices saved in global vars.
;;; 16-Apr-2002 BobGian extend REPORT-ERROR to print output PDU currently
;;;  under construction as list structure before being written to TCP-buffer.
;;; 16-Apr-2002 BobGian MISHAP called in WRITE-DICOM-OUTPUT [stub] prints
;;;   list-structure representation of its input if called accidently.
;;; 19-Apr-2002 BobGian second arg to REPORT-ERROR can be used to print
;;;   arbitrary list structure or to dump TCP-Buffer.
;;; 23-Apr-2002 BobGian add *MAX-DATAFIELD-LEN* to REPORT-ERROR.
;;;   Also AE-03 caches max PDU size for all subsequent PDU sends.
;;; 06-May-2002 BobGian optional add error message arg to REPORT-ERROR
;;;   and MISHAP.  Sometimes message in embedded call to ERROR gets lost.
;;; 10-May-2002 BobGian AE-03 checks *MAX-DATAFIELD-LEN* for maximum value
;;;   and for being EVEN when association is accepted.
;;; 06-Aug-2002 BobGian *PRINT-PRETTY* -> T in REPORT-ERROR.
;;; Jul/Aug 2002 BobGian labels in REPORT-ERROR used to identify variables
;;;   improved (made more consistent with var name and function).
;;; 17-Sep-2002 BobGian:
;;;   REPORT-ERROR accepts 3rd arg DICOM-ALIST to print conditionally.
;;; 24-Sep-2002 BobGian:
;;;   Remove 3rd arg (DICOM-ALIST) to REPORT-ERROR and MISHAP.  Same
;;;   functionality now obtained via special variable set when data available.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 08-May-2003 BobGian - REPORT-ERROR no longer binds *PRINT-PRETTY*.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 03-Nov-2003 BobGian - Add *STANDARD-OUTPUT* arg to DUMP-DICOM-DATA.
;;; 21-Dec-2003 BobGian: Add arg to dummy PARSE-OBJECT for ignorable slots.
;;; 02-Mar-2004 BobGian: Fix to output formatting in REPORT-ERROR.
;;; 08-Nov-2004 BobGian remove stubs: PARSE-OBJECT and WRITE-DICOM-OUTPUT
;;;   [used by Server only].
;;; 18-Apr-2005 I. Kalet add SSL support per Tung Le in open-connection.
;;; 24-Jun-2009 I. kalet spell out socket: for acl-socket symbols
;;;

(in-package :dicom)

;;;=============================================================

;;; All Action functions must return either an updated environment [CONSP]
;;; resulting from parsing a command or NIL.  Don't accidently let some
;;; random trailing value get returned.

;;;=============================================================
;;; Association Establishment Actions.

(defun ae-01 (env tcp-buffer tcp-strm)

  "Issue CONNECT request to TCP"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore tcp-buffer tcp-strm))

  ;; Must push Remote-Hostname and Remote-Port onto environment
  ;; [hostname and port number of server our client is calling]
  ;; at global level before invoking this action function.
  (open-connection (item-lookup 'Remote-Hostname env t) ;Global Env
		   (item-lookup 'Remote-Port env t))    ;Global Env

  (setq *event* 'event-02)                   ;Signal EVENT-02: Open Successful

  nil)

;;;-------------------------------------------------------------

(defun ae-02 (env tcp-buffer tcp-strm)

  "Send A-Associate-RQ PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (send-pdu :A-Associate-RQ env tcp-buffer tcp-strm)

  nil)

;;;-------------------------------------------------------------

(defun ae-03 (env tcp-buffer tcp-strm)

  "Issue A-Associate confirmation accepted message"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore tcp-buffer tcp-strm))

  ;; Server either accepted client's proposed max PDU size or proposed its own.
  ;; Set variable to cache it for all remaining PDUs during this association.
  (let ((limit (item-lookup 'Max-DataField-Len env nil
			    :Max-DataField-Len-Item
			    :User-Information-Item
			    :A-Associate-AC)))
    (cond ((typep limit 'fixnum)
	   ;; Spec requires all P-Data-TF PDUs, and therefore all PDVs,
	   ;; to be of even length.
	   (unless (evenp (the fixnum limit))
	     (mishap env nil "AE-03 [1] Odd datafield length: ~S" limit))
	   (setq *max-datafield-len* (min (the fixnum limit) #.PDU-Bufsize)))
	  (t (setq *max-datafield-len* #.PDU-Bufsize))))

  (when (>= (the fixnum *log-level*) 1)
    (format t "~%AE-03: Server accepted A-Associate-RQ.~%")
    (format t "~&Max PDU size negotiated: ~D~%" *max-datafield-len*))

  (setq *event* 'event-09)       ;Signal EVENT-09: Ready to send P-Data-TF PDU

  nil)

;;;-------------------------------------------------------------

(defun ae-04 (env tcp-buffer tcp-strm)

  "Issue A-Associate REJECTED message, close connection, leave DUL main loop"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore tcp-buffer))

  (let ((result (item-lookup 'RJ-Result env t :A-Associate-RJ))
	(source (item-lookup 'RJ-Source env t :A-Associate-RJ))
	(diagno (item-lookup 'RJ-Diagnostic env t :A-Associate-RJ))
	(errorstring "Unknown - DUL error"))

    (declare (type simple-base-string errorstring)
	     (type fixnum result source diagno))

    (format
      t "~%~A~%"
      (setq *status-message*
	    (concatenate
	      'string
	      (format nil
		      "Server rejected A-Associate-RQ.~%Result: ~A~%"
		      (cond ((= result 1) "Rejection-Permanent")
			    ((= result 2) "Rejection-Transient")
			    (t errorstring)))
	      (cond
		((= source 1)
		 (format nil "Source: UL Service-User~%Diagnostic: ~A"
			 (cond ((= diagno 1) "No Reason Given")
			       ((= diagno 2)
				"Application Context Name Not Supported")
			       ((= diagno 3) "Calling AE Title Not Recognized")
			       ((= diagno 7) "Called AE Title Not Recognized")
			       (t errorstring))))
		((= source 2)
		 (format nil
			 "Source: UL Service-Provider [ACSE]~%Diagnostic: ~A"
			 (cond ((= diagno 1) "No Reason Given")
			       ((= diagno 2) "Protocol Version Not Supported")
			       (t errorstring))))
		((= source 3)
		 (format nil "Source: UL Service-Provider~%Diagnostic: ~A"
			 (cond ((= diagno 1) "Temporary Congestion")
			       ((= diagno 2) "Local Limit Exceeded")
			       (t errorstring))))
		(t (format nil "Source: ~A" errorstring)))))))

  (close-connection tcp-strm)

  nil)

;;;=============================================================
;;; Data-Transfer Actions.
;;; This function is invoked by desire to send a P-Data-TF PDU containing
;;; a Command or Data-Set, as indicated by Event-09 being signalled.
;;; Currently, it sends only the :C-Echo-RQ or :C-Store-RTPlan-RQ requests.

(defun dt-01 (env tcp-buffer tcp-strm)

  "Send P-Data-TF PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  ;; Note that message sending is accomplished by giving SEND-PDU a complete
  ;; PDU to send, in which are embedded operators to construct the message,
  ;; rather than by sending a :P-Data-TF with an embedded PDV-Message variable
  ;; bound in the sending environment.  SEND-PDU handles fragmentation, if
  ;; needed.  NB: Fragmentation only works correctly if a PDU which might
  ;; require it contains ONLY a single PDV.

  (let ((cmd (item-lookup 'Command env t)))         ;Global Env

    (cond ((eq cmd :C-Echo-RQ)
	   (send-pdu :C-Echo-RQ env tcp-buffer tcp-strm))

	  ((eq cmd :C-Store-RTPlan-RQ)
	   ;; The command fits in a single PDU.
	   (send-pdu :C-Store-RTPlan-Command env tcp-buffer tcp-strm)
	   ;; The data portion is rule-defined to be a single PDV, but likely
	   ;; it will be fragmented into multiple single-PDV PDUs by SEND-PDU.
	   (send-pdu :C-Store-RTPlan-Data env tcp-buffer tcp-strm))

	  (t (mishap env tcp-buffer "DT-01 [1] Bogus COMMAND: ~S" cmd))))

  nil)

;;;=============================================================
;;; Association Release Actions.

(defun ar-01 (env tcp-buffer tcp-strm)

  "Send A-Release-RQ PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (send-pdu :A-Release-RQ env tcp-buffer tcp-strm)

  nil)

;;;-------------------------------------------------------------

(defun ar-03 (env tcp-buffer tcp-strm)

  "Issue A-Release confirmation message, close connection, leave DUL main loop"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AR-03: A-Release confirmation.~%"))

  (close-connection tcp-strm)

  nil)

;;;-------------------------------------------------------------

(defun ar-06 (env tcp-buffer tcp-strm)

  "Issue P-Data message -- handle P-Data PDU arriving out of order"

  ;; This action is to handle P-Data-TF PDUs that arrive out of order,
  ;; when the client has initiated a release but the server has not processed
  ;; the PDU and is still sending Data-Set PDUs.  Client must handle data and
  ;; continue waiting for the A-Release-RSP PDU [loop to STATE-07].
  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer tcp-strm))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AR-06: P-Data.~%"))

  nil)

;;;-------------------------------------------------------------

(defun ar-09 (env tcp-buffer tcp-strm)

  "Send A-Release-RSP PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (send-pdu :A-Release-RSP env tcp-buffer tcp-strm)

  nil)

;;;=============================================================
;;; Client TCP stream operations.

(defun open-connection (hostname port)

  (declare (type simple-base-string hostname)
	   (type fixnum port))

  (multiple-value-bind (val report)
      (ignore-errors
	(let* ((tcp-strm
		 (socket:make-socket
		   :address-family :Internet :type :Stream :format :Binary
		   :connect :Active :remote-host hostname :remote-port port))
	       (remote-IP-addr (socket:remote-host tcp-strm))
	       (remote-IP-string
		 (or (ignore-errors (socket:ipaddr-to-dotted remote-IP-addr))
		     (format nil "~D" remote-IP-addr)))
	       (local-IP-addr (socket:local-host tcp-strm))
	       (local-IP-string
		 (or (ignore-errors (socket:ipaddr-to-dotted local-IP-addr))
		     (format nil "~D" local-IP-addr))))

	  (declare (type simple-base-string remote-IP-string local-IP-string))

	  (setq *remote-IP-string* remote-IP-string)
	  (when (>= (the fixnum *log-level*) 1)
	    (format t
		    #.(concatenate
			'string
			"~%OPEN-CONNECTION: Opening connection.~%"
			"  FROM   IP address: ~A, Port ~D~%"
			"  TO     IP address: ~A, Port ~D~%")
		    local-IP-string
		    (socket:local-port tcp-strm)
		    remote-IP-string
		    (socket:remote-port tcp-strm)))

	  (setq *connection-strm* (if *use-ssl* 
				      (socket:make-ssl-client-stream tcp-strm)
				    tcp-strm))))

    (declare (ignore val))

    (when (typep report 'condition)
      (format t "~%~A~%"
	      (setq *status-message* "Error opening TCP connection:"))
      (throw :Abandon-Client nil))))

;;;-------------------------------------------------------------

(defun close-connection (tcp-strm)

  (when (>= (the fixnum *log-level*) 1)
    (format t "~%CLOSE-CONNECTION: Closing connection.~%Stream: ~S~%"
	    tcp-strm))

  (unless (streamp tcp-strm)
    ;; This detects a fault in control structure -- attempt to call
    ;; CLOSE-CONNECTION when *CONNECTION-STRM* is already NIL.
    (mishap nil nil "CLOSE-CONNECTION [1] Stream already closed:~%~S"
	    tcp-strm))

  (unless (close tcp-strm)
    ;; If stream was open, CLOSE closes it and returns T.
    ;; If it was already open, CLOSE returns NIL.
    ;; This detects attempt to close an already closed connection
    ;; when *CONNECTION-STRM* is non-NIL.
    (mishap nil nil "CLOSE-CONNECTION [2] Stream already closed:~%~S"
	    tcp-strm))

  (setq *connection-strm* nil))

;;;=============================================================
;;; This version of REPORT-ERROR is specialized to Client functionality.
;;; It reports only global vars used by Client.

(defun report-error (env data &optional msg &rest format-args)

  ;; Reports useful information [previously cached as values of global vars]
  ;; to logging stream in case of run-time errors.

  (declare (type list env format-args)
	   (type (or null simple-base-string) msg)
	   (type
	     (or null list (simple-array (unsigned-byte 8) (#.TCP-Bufsize)))
	     data))

  (format t "~%REPORT-ERROR:~%")
  (when (typep msg 'simple-base-string)
    (apply #'cl:format t msg format-args))

  ;; Date, Time:
  (format t "~&~%  Date/Time:~44T~A~%~%" (date/time))

  ;; Identification of communication entities:
  (format t "~&  Remote IP Address:~44T~S~%" *remote-IP-string*)
  (format t "~&  Calling AE Name:~44T~S~%" *calling-AE-name*)
  (format t "~&  Called AE Name:~44T~S~%" *called-AE-name*)
  (format t "~&  Max PDU Size:~44T~S~%~%"*max-datafield-len*)

  ;; Operation being performed:
  (format t "~&  SOP Class Name:~44T~S~%~%" *SOP-class-name*)

  ;; State of PDU/Object parsers and protocol controller:
  (format t "~&  State:~44T~S (~A)~%" *state* (get *state* 'documentation))
  (format t "~&  Event:~44T~S (~A)~%" *event* (get *event* 'documentation))
  (format t "~&  Arguments:~44T~S~%~%" *args*)

  ;; Status reports:
  (format t "~&  Status Message:~44T~S~%"
	  (or *status-message* "Unknown error"))
  (format t "~&  Status Code:~44T~S~%" *status-code*)

  ;; State of current Environment:
  (when (consp env)
    (print-environment env))

  (cond ((consp data)
	 ;; State of current list-structure object being constructed:
	 ;; PDU datalist is constructed backwards [items CONSed to front].
	 (format t "~%  Output PDU or raw data:~%  ~S~%" data))

	;; Contents of current TCP buffer:
	((arrayp data)
	 ;; This will dump any shifted bytes from prior PARSE-OBJECT call.
	 ;; New PDU will start at HEAD, which may be non-zero.
	 (dump-bytestream "TCP buffer" data 0 *PDU-tail*)))

  ;; Used only in Server, or in Client performing Server functionality.
  (when (consp *dicom-alist*)
    (dump-dicom-data *dicom-alist* *standard-output*)))

;;;=============================================================
;;; Stubs.  For now, these functions are used only by Server, but calls
;;; to them appear [in non-invoked conditional branches] in Common code.

(defun parse-object (env tcp-buffer head tail last-frag? continuation
		     ignorable-groups-list)
  (declare (ignore tcp-buffer head tail last-frag? continuation
		   ignorable-groups-list))
  (mishap env nil "PARSE-OBJECT called in Client."))

;;;=============================================================
;;; End.
