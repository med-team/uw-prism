;;;
;;; generator
;;;
;;; Rule-based PDU Instantiation for DICOM Message Generation.
;;; Contains functions common to Client and Server.
;;;
;;; 11-Apr-2001 BobGian convert TCP stream reading/writing code to work
;;;  in ACL Version 6.0 (READ-SEQUENCE, WRITE-SEQUENCE slightly buggy).
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 18-Aug-2001 BobGian WRITE-VECTOR -> WRITE-SEQUENCE.  More portable.
;;; 24-Jan-2002 BobGian full PDU dump only at log level 4 [full debug mode].
;;; 16-Apr-2002 BobGian MISHAP called in any generator function prints
;;;   list-structure representation of output generated so far.
;;; 16-Apr-2002 BobGian convert GENERATE-GROUP, GENERATE-ITEM, GENERATE-TERM,
;;;   and GENERATE-PDU to return list structure which SEND-PDU then fragments
;;;   [if needed] and packs into TCP-Buffer for transmission.
;;; 16-Apr-2002 BobGian PUT-FIXNUM-{LE,BE}{1,2,4} inlined into INSTANTIATE-PDU.
;;; 24-Apr-2002 BobGian move OBJECT-LENGTH here to fix dependency.
;;;   Needed in both Client and Server.
;;; 30-Apr-2002 BobGian fix bug in <PDV-MCH implementation.
;;; 05-May-2002 BobGian enforce constraint that P-Data-TF fragmentation
;;;   can occur only on an even byte boundary.
;;; 10-May-2002 BobGian *MAX-DATAFIELD-LEN* checked once when association
;;;   accepted rather than in every PDU sent.
;;; 10-May-2002 BobGian modify SEND-PDU to compute PDU and PDV lengths and
;;;   substitute values in appropriate fields rather than doing expansion in
;;;   rules.  :Place-Holder token is used to mark field in rules.  This change
;;;   is required to make fragmentation work correctly.
;;; Jul/Aug 2002 BobGian accessor name change: <MCH -> <PDV-MCH .
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================
;;; Use 16384 as a max PDU size during association negotiation process,
;;; ie, for A-Assoc-RQ/AC PDUs.  After that, *MAX-DATAFIELD-LEN* holds
;;; negotiated value to be used for rest of association.

(defun send-pdu (pdutype env tcp-buffer tcp-strm &rest args
		 &aux (limit (or *max-datafield-len* 16384))
		 (log-level *log-level*))

  "ARGS is sequence of alternating KEY/VALUE pairs.
A key can be a Dicom variable or the tag :Set"

  (declare (type symbol pdutype)
	   (type list env args)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum limit log-level))

  (when (>= log-level 2)
    (format t "~%SEND-PDU: Sending ~A PDU, ~A."
	    (get pdutype 'documentation) (date/time))
    (when (and (consp args)
	       (>= log-level 3))
      (format t "  Args (dec):~%")
      (do ((args2 args (cddr args2))
	   (key) (val))
	  ((null args2))
	(setq key (first args2) val (second args2))
	(cond ((eq key :Set)
	       (unless (consp val)
		 (mishap env nil "SEND-PDU [1] Bad :Set args: ~S" args))
	       (format t "~%  Multiple-Valued Environment values:")
	       (do ((items val (cdr items))
		    (cnt 1 (the fixnum (1+ cnt))))
		   ((null items))
		 (declare (type list items)
			  (type fixnum cnt))
		 (format t "~%    Set ~D:" cnt)
		 (dolist (pair (car items))
		   (format t "~%      Arg: ~A~38TValue: ~S"
			   (car pair) (cdr pair)))))
	      (t (format t "~%  Arg: ~S~38TValue: ~S" key val)))))
    (terpri))

  (do ((args2 args (cddr args2)))
      ((null args2))
    ;; BINDING rather than UPDATING environment because change is temporary.
    ;; If structured value is being used as environmental value, argument
    ;; value passed to SEND-PDU must be appropriately structured.
    (push (cons (first args2) (second args2)) env))

  ;; GENERATE-PDU returns list-structure for the entire unfragmented PDU.
  ;; This loop fragments it into separate PDVs [if necessary], adding the
  ;; appropriate PDU header terms in front of each PDV.
  (do ((pdulist (generate-pdu pdutype env nil)))
      ((null pdulist))

    (declare (type list pdulist))

    (when (>= log-level 4)
      (format t "~%PDU to be fragmented and transmitted [decimal]:~%  ~S~%"
	      pdulist))

    (do ((ptr pdulist) (instantiated-length 0) (term-val) (tag) (term-len 0)
	 (termlist '()) (pc-id) (mch) (bytes-transmitted 0) (odd-separator?))
	((null ptr)
	 (setq termlist (nreverse termlist))       ;Put back in forward order.

	 ;; All PDUs get their length instantiated here.
	 ;; Decrement BYTES-TRANSMITTED by 6 for the 6 PDU header bytes not
	 ;; counted in PDU length field.  Result is number of message/data
	 ;; bytes to be transmitted in the current [last or only] PDU.
	 (unless (eq (third termlist) :Place-Holder)
	   (mishap env nil "SEND-PDU [2] Bad termlist: ~S" termlist))
	 (setq bytes-transmitted (the fixnum (- bytes-transmitted 6)))
	 (setf (third termlist)                     ;PDU Length field.
	       (list 'fixnum 4 :Big-Endian bytes-transmitted))
	 ;Only P-Data-TF PDUs can be fragmented.
	 (when (eq (first termlist) #x04)
	   ;; In addition, P-Data-TF PDUs get additional terms instantiated.
	   ;; If MCH term is a list (<PDV-MCH :Command) or (<PDV-MCH :Data)
	   ;; then it is a Message Control Header to be expanded here.
	   ;; #b******XY  [* is don't-care bit, X and Y are 2 low-order bits]
	   ;;  Bit X = 0 -> Message is NOT last fragment.
	   ;;  Bit X = 1 -> Message IS last fragment.
	   ;;  Bit Y = 0 -> Message is Data-Set.
	   ;;  Bit Y = 1 -> Message is a Command.
	   (unless (and (eq (second termlist) #x00)
			(eq (fourth termlist) :Place-Holder)
			(typep (fifth termlist) 'fixnum)    ;Pres Context ID.
			(consp (setq mch (sixth termlist)))
			(eq (first mch) '<pdv-mch))
	     (mishap env nil "SEND-PDU [3] Bad termlist: ~S" termlist))
	   (setf (fourth termlist)
		 (list 'fixnum 4 :Big-Endian
		       (the fixnum (- bytes-transmitted 4))))  ;PDV Len field.
	   (setf (sixth termlist)                 ;Update MCH - LAST fragment.
		 (cond ((eq (second mch) :Command) #b00000011)
		       (t #b00000010))))

	 ;; And transmit the PDU.
	 (setq instantiated-length (instantiate-pdu termlist tcp-buffer limit))
	 (when (>= log-level 2)
	   (format t "~%SEND-PDU: Sending All or Last Fragment, ~D bytes.~%"
		   instantiated-length)
	   (when (>= log-level 4)
	     (dump-bytestream "Outgoing PDU [All or Last Fragment]"
			      tcp-buffer 0 instantiated-length)))
	 (write-sequence tcp-buffer tcp-strm :start 0 :end instantiated-length)
	 (force-output tcp-strm)

	 (setq pdulist nil))

      (declare (type list ptr termlist)
	       (type (or list (integer #x00 #xFF)) mch)
	       (type (member nil t) odd-separator?)
	       (type fixnum term-len bytes-transmitted instantiated-length))

      (setq term-len
	    (cond ((consp (setq term-val (car ptr)))
		   (cond ((or (eq (setq tag (first term-val)) 'fixnum)
			      (eq tag 'string))
			  ;; For FIXNUM or STRING terms, second element is
			  ;; length field [including padding for strings].
			  (second term-val))
			 ((eq tag '<pdv-mch)
			  1)
			 (t (mishap env nil "SEND-PDU [4] Bad term: ~S"
				    term-val))))
		  ;;
		  ;; :Place-Holder is used to expand 4-byte Big-Endian
		  ;; length field in PDU or PDV.
		  ((eq term-val :Place-Holder)
		   4)
		  ;;
		  ;; Otherwise term must be a single-byte fixnum
		  ;; [checked previously].
		  ((typep term-val 'fixnum)
		   1)
		  ;;
		  ;; Otherwise we forgot something.
		  (t (mishap env nil "SEND-PDU [5] Bad term: ~S" term-val))))

      ;; BYTES-TRANSMITTED is total number of bytes [including header terms]
      ;; to be transmitted in next PDU.  Spec requires fragmentation to be on
      ;; an even byte boundary.  If a string ending on an odd boundary has just
      ;; been transmitted, it will be followed by a separator or padding byte.
      ;; If the string fits, so does the separator/padding byte [since LIMIT
      ;; is even].  If a string ending on an even boundary has just been
      ;; transmitted, it will NOT be followed by a padding byte but it MIGHT
      ;; be followed by a separator byte.  If so, and the separator byte fits
      ;; but the next item does not, the fragment would end on an odd boundary
      ;; unless we move the separator byte from the last to the next fragment.
      ;;
      ;; For PDUs other than P-Data-TF the even-length constraint does not
      ;; apply, but such PDUs will not get fragmented anyway and so this
      ;; branch will never be triggered.
      (cond
	((<= (setq bytes-transmitted
		   (the fixnum (+ bytes-transmitted term-len)))
	     limit)
	 (push term-val termlist)              ;Accumulate all terms that fit.
	 (setq ptr (cdr ptr)))

	;; Must modify PDU template for transmission of current fragment and
	;; defer rest of terms in original input list to next fragment [with
	;; appropriate header terms prepended].  Decrement BYTES-TRANSMITTED
	;; by size of current term NOT transmitted in upcoming PDU.  Note that
	;; fragmentation only works if the PDU being fragmented was constructed
	;; via a rule specifying it to contain a single PDV.  After sending
	;; each accumulated fragment, we construct and prepend to the remaining
	;; data terms the header terms for a new single-PDV-containing PDU.
	(t (setq bytes-transmitted (the fixnum (- bytes-transmitted term-len)))
	   (setq odd-separator? nil)

	   ;; If after splitting the fragments we discover the last was of odd
	   ;; length, the only way that could happen was that a separator byte
	   ;; just fit, bringing the length to odd, and the next string did not
	   ;; fit. ; Any other situation is an error condition.  Move separator
	   ;; byte from the pre-split fragment to the post-split fragment.
	   (unless (evenp bytes-transmitted)
	     (cond ((eq (car termlist) #.(char-code #\\))
		    (setq termlist (cdr termlist))
		    (setq bytes-transmitted
			  (the fixnum (1- bytes-transmitted)))
		    (setq odd-separator? t))
		   (t (mishap
			env nil
			"Send-PDU [6] Odd-length frag ends on weird byte."))))

	   (setq termlist (nreverse termlist))     ;Put back in forward order.

	   ;; Only P-Data-TF PDUs can be fragmented.
	   (unless (and (eq (first termlist) #x04)
			(eq (second termlist) #x00)
			(eq (third termlist) :Place-Holder)
			(eq (fourth termlist) :Place-Holder)
			(typep (setq pc-id (fifth termlist)) 'fixnum)
			(consp (setq mch (sixth termlist)))
			(eq (first mch) '<pdv-mch))
	     (mishap env nil "SEND-PDU [7] Bad termlist: ~S" termlist))

	   ;; Decrement BYTES-TRANSMITTED by 6 for the 6 PDU header bytes not
	   ;; counted in PDU length field.  Result is number of message/data
	   ;; bytes to be transmitted in the current [fragmented] PDU.
	   (setq bytes-transmitted (the fixnum (- bytes-transmitted 6)))
	   (setf (third termlist)                   ;PDU Length field.
		 (list 'fixnum 4 :Big-Endian bytes-transmitted))
	   (setf (fourth termlist)                  ;PDV Length field.
		 (list 'fixnum 4 :Big-Endian
		       (the fixnum (- bytes-transmitted 4))))
	   (setf (sixth termlist)        ;MCH for current [NOT-LAST] fragment.
		 (cond ((eq (second mch) :Command) #b00000001)
		       (t #b00000000)))

	   ;; And transmit the PDU.
	   (setq instantiated-length
		 (instantiate-pdu termlist tcp-buffer limit))
	   (when (>= log-level 2)
	     (format t "~%SEND-PDU: Sending Non-Last Fragment, ~D bytes.~%"
		     instantiated-length)
	     (when (>= log-level 4)
	       (dump-bytestream "Outgoing PDU [Non-Last Fragment]"
				tcp-buffer 0 instantiated-length)))
	   (write-sequence tcp-buffer tcp-strm
			   :start 0 :end instantiated-length)
	   (force-output tcp-strm)
	   (setq pdulist
		 (list* #x04
			#x00
			;; PDU Length field for next PDU [fragment].
			;; Placeholder for length value which will be filled in
			;; before PDU is instantiated - when length is known.
			:Place-Holder
			;; Ditto but PDV Length field.
			:Place-Holder
			pc-id                       ;Presentation Context ID.
			mch               ;Restore MCH term for next fragment.
			(cond (odd-separator?
				;; If separator byte was moved from last to
				;; next fragment [due to last fragment ending
				;; on odd boundary], stick it back in as first
				;; data byte in next fragment.
				(cons #.(char-code #\\) ptr))
			      (t ptr))))
	   ;; Reset PTR to beginning of newly-inserted header terms.
	   (setq ptr pdulist)
	   ;; Reset count of bytes to be sent in next PDU.
	   (setq bytes-transmitted 0)
	   ;; Reset TERMLIST to begin accumulation anew.
	   (setq termlist nil))))))

;;;-------------------------------------------------------------
;;; INSTANTIATE-PDU transfers list-structure representing [an already
;;; fragmented, if necessary] PDU to the TCP buffer.  LIMIT must be EVEN
;;; [ie, fragmentation can only be done on an even byte boundary].

(defun instantiate-pdu (output-itemlist tcp-buffer limit &aux code (tail 0))

  (declare (type list output-itemlist)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum tail limit))

  (dolist (term output-itemlist)
    (cond
      ((atom term)
       (unless (and (typep term 'fixnum)
		    (<= 0 (the fixnum term) #xFF))
	 (setq *PDU-tail* tail)
	 (mishap nil tcp-buffer
		 "INSTANTIATE-PDU [1] Bad atomic fixnum term: ~S" term))
       (unless (< tail limit)
	 (setq *PDU-tail* tail)
	 (mishap nil tcp-buffer
		 "INSTANTIATE-PDU [2] Buffer overrun on atomic term: ~S" term))
       (setf (aref tcp-buffer tail) term)
       (setq tail (the fixnum (1+ tail))))

      ((eq (setq code (first term)) 'fixnum)
       (let ((size (second term))
	     (endian (third term))
	     (val (fourth term)))
	 (declare (type (member :Big-Endian :Little-Endian) endian)
		  (type fixnum size val))
	 (cond
	   ((> (the fixnum (+ size tail)) limit)
	    (setq *PDU-tail* tail)
	    (mishap nil tcp-buffer
		    "INSTANTIATE-PDU [3] Buffer overrun on fixnum term: ~S"
		    term))
	   ((= size 1)
	    (setf (aref tcp-buffer tail) val)
	    (setq tail (the fixnum (1+ tail))))
	   ((= size 2)
	    (cond ((eq endian :Little-Endian)
		   (setf (aref tcp-buffer tail) (logand #x00FF val))
		   (setf (aref tcp-buffer (the fixnum (1+ tail)))
			 (ash (logand #xFF00 val) -8)))
		  (t (setf (aref tcp-buffer tail) (ash (logand #xFF00 val) -8))
		     (setf (aref tcp-buffer (the fixnum (1+ tail)))
			   (logand #x00FF val))))
	    (setq tail (the fixnum (+ tail 2))))
	   ((= size 4)
	    ;; Largest mask should be #xFF000000, but using smaller value
	    ;; keeps it POSITIVE FIXNUM, and no value will exceed 536870911.
	    (cond ((eq endian :Little-Endian)
		   (setf (aref tcp-buffer tail) (logand #x000000FF val))
		   (setf (aref tcp-buffer (the fixnum (1+ tail)))
			 (ash (logand #x0000FF00 val) -8))
		   (setf (aref tcp-buffer (the fixnum (+ tail 2)))
			 (ash (logand #x00FF0000 val) -16))
		   (setf (aref tcp-buffer (the fixnum (+ tail 3)))
			 (ash (logand #x1F000000 val) -24)))
		  (t (setf (aref tcp-buffer tail)
			   (ash (logand #x1F000000 val) -24))
		     (setf (aref tcp-buffer (the fixnum (1+ tail)))
			   (ash (logand #x00FF0000 val) -16))
		     (setf (aref tcp-buffer (the fixnum (+ tail 2)))
			   (ash (logand #x0000FF00 val) -8))
		     (setf (aref tcp-buffer (the fixnum (+ tail 3)))
			   (logand #x000000FF val))))
	    (setq tail (the fixnum (+ tail 4))))
	   (t (setq *PDU-tail* tail)
	      (mishap nil tcp-buffer "INSTANTIATE-PDU [4] Bad fixnum term: ~S"
		      term)))))

      ((eq code 'string)
       (let* ((strval (fourth term))
	      (strlen (length strval))
	      (strpad (third term))
	      (varlen (second term)))
	 (declare (type simple-base-string strval)
		  (type (member :No-Pad :Space-Pad :Null-Pad) strpad)
		  (type fixnum strlen varlen))
	 (when (> (the fixnum (+ varlen tail)) limit)
	   (setq *PDU-tail* tail)
	   (mishap nil tcp-buffer
		   "INSTANTIATE-PDU [5] Buffer overrun on string term: ~S"
		   term))
	 (do ((from-idx 0 (the fixnum (1+ from-idx)))
	      (to-idx tail (the fixnum (1+ to-idx))))
	     ((= from-idx strlen)
	      (cond
		((= strlen varlen)
		 ;; If :No-Pad string length [STRLEN] did not match
		 ;; required length [VARLEN], GENERATE-TERM triggers
		 ;; an error and we never get this far.
		 (setq tail to-idx))
		((eq strpad :Null-Pad)
		 (setf (aref tcp-buffer to-idx) 0)
		 (setq tail (the fixnum (1+ to-idx))))
		((eq strpad :Space-Pad)
		 (do ((idx to-idx (the fixnum (1+ idx)))
		      (cnt strlen (the fixnum (1+ cnt))))
		     ((= cnt varlen)
		      (setq tail idx))
		   (declare (type fixnum idx cnt))
		   (setf (aref tcp-buffer idx) #.(char-code #\Space))))
		(t (mishap nil tcp-buffer
			   "INSTANTIATE-PDU [6] Bad length/padding in term: ~S"
			   term))))
	   (declare (type fixnum from-idx to-idx))
	   (setf (aref tcp-buffer to-idx)
		 (char-code (aref strval from-idx))))))

      (t (setq *PDU-tail* tail)
	 (mishap nil tcp-buffer "INSTANTIATE-PDU [7] Bad unknown term: ~S"
		 term))))

  tail)

;;;-------------------------------------------------------------
;;; GENERATE-PDU returns list-structure for the entire unfragmented PDU.
;;; SEND-PDU fragments it [if necessary] into separate PDUs, each containing
;;; header terms plus a single PDV.

(defun generate-pdu (pdutype env output-itemlist)

  (declare (type symbol pdutype)
	   (type list env output-itemlist))

  (let ((rulebody (get pdutype :Generator-Rule)))
    (cond ((consp rulebody)
	   ;; All generator functions up to now have CONSed new items onto
	   ;; front of output list.  Here we reverse it to present final PDU
	   ;; in forward order.
	   (nreverse (generate-group rulebody env output-itemlist)))
	  (t (mishap env nil "GENERATE-PDU [1] Missing PDU definition: ~S"
		     pdutype)))))

;;;-------------------------------------------------------------

(defun generate-group (termlist env output-itemlist
		       &aux (backpatch-stack '()) slotlen dataend)

  (declare (type list termlist env output-itemlist backpatch-stack))

  (dolist (term termlist)
    (cond
      ((and (consp term)
	    (eq (first term) '<item-length))
       ;; An <ITEM-LENGTH element causes insertion at that point in the list
       ;; representing an item [as defined by a clause in a rule] of the
       ;; length-to-end as a list of 2 or 4 bytes, big or little endian, where
       ;; "length" means the number of bytes from the END of the <ITEM-LENGTH
       ;; element [ie, the beginning of the NEXT field] to the end of the
       ;; entire item.  An <ITEM-LENGTH can be any element of an item AFTER
       ;; the first, and an item can contain multiple <ITEM-LENGTH elements.
       (cond
	 ((and (typep (setq slotlen (second term)) 'fixnum)
	       (or (= (the fixnum slotlen) 2)
		   (= (the fixnum slotlen) 4))
	       (keywordp (setq dataend (third term)))
	       (or (eq dataend :Big-Endian)
		   (eq dataend :Little-Endian)))
	  ;; Push a backpatch-target token with information indicating
	  ;; how to perform backpatch substitution later when length is known.
	  (push (list 'fixnum slotlen dataend nil) output-itemlist)
	  (push output-itemlist backpatch-stack))

	 (t (mishap env output-itemlist "GENERATE-GROUP [1] Bad term: ~S"
		    term))))

      (t (setq output-itemlist (generate-term term env output-itemlist)))))

  ;; Backpatch any deferred <ITEM-LENGTH fields.
  (do ((items backpatch-stack (cdr items))
       (backpatch-pointer))
      ((null items))
    (declare (type list items backpatch-pointer))
    (setq backpatch-pointer (car items))
    (setf (fourth (car backpatch-pointer))
	  (object-length output-itemlist backpatch-pointer)))

  output-itemlist)

;;;-------------------------------------------------------------

(defun object-length (output-itemlist object-start)

  (declare (type list output-itemlist object-start))

  (do ((ptr output-itemlist (cdr ptr))
       (term) (tag)
       (byte-count 0))
      ((eq ptr object-start)
       byte-count)

    (declare (type list ptr)
	     (type fixnum byte-count))

    (cond
      ((consp (setq term (car ptr)))
       (cond ((eq (setq tag (first term)) '<pdv-mch)
	      (setq byte-count (the fixnum (1+ byte-count))))
	     ((or (eq tag 'fixnum)
		  (eq tag 'string))
	      ;; For FIXNUM or STRING terms, second element is length field
	      ;; [including padding for strings].
	      (setq byte-count
		    (the fixnum (+ byte-count (the fixnum (second term))))))
	     (t (mishap nil nil "OBJECT-LENGTH [1] Bad term: ~S" term))))

      #+ignore
      ;; OBJECT-LENGTH should never see this term, since only length fields
      ;; of PDUs are expanded procedurally via this term.  OBJECT-LENGTH is
      ;; only used on structure internal to the data in a PDU.
      ((eq term :Place-Holder)
       ;; :Place-Holder is used to expand 4-byte Big-Endian
       ;; length field in PDU or PDV.
       (setq byte-count (the fixnum (+ byte-count 4))))

      ((typep term 'fixnum)
       (setq byte-count (the fixnum (1+ byte-count))))

      ;; If we mistakenly run off end [missing OBJECT-START], PTR will be NIL,
      ;; thus so will TERM, and this branch will catch the error.
      (t (mishap nil nil "OBJECT-LENGTH [2] Bad term: ~S" term)))))

;;;-------------------------------------------------------------

(defun generate-term (term env output-itemlist &aux tag varname varval vartype
		      varlen varend-pad access-chain term-2 term-3)

  (declare (type list env output-itemlist)
	   (type symbol varname vartype varend-pad))

  (cond
    ((or (eq term :Place-Holder)     ;Filled in by procedural expansion later.
	 (typep term 'fixnum))       ;Value expanded at rule-compilation time.
     (push term output-itemlist))                   ;Direct pass-through.

    ((keywordp term)                           ;Invoke sub-rule for expansion.
     (setq output-itemlist (generate-item term env output-itemlist)))

    ((atom term)                                    ;Oops!
     (mishap env output-itemlist "GENERATE-TERM [1] Bad atomic term: ~S" term))

    ;; All terms from this point onward are known to be non-empty LISTs.
    ;; Must set all locals to subterms here, whether used immediately or not.
    ((eq (setq term-2 (second term)                ;Preset to be used as local
	       term-3 (third term)                 ;Preset to be used as local
	       tag (first term))            ;Preset as local and do comparison
	 :Set)
     ;; The :Set operator causes the instantiation of a set of items, each
     ;; with its own local environment in which its "global" variable values
     ;; are dereferenced.  Used to instantiate multiple Presentation Contexts.
     ;; NB: There can exist only one :Set-valued item in the environment;
     ;; if more than one needed, will have to implement different tags to
     ;; distinguish them.
     (dolist (local-env (item-lookup :Set env t))
       (setq output-itemlist
	     (generate-item term-2 local-env output-itemlist))))

    ((eq tag '<if)                                  ;DICOM Conditional
     ;; Predicate function [second element] is applied to two arguments --
     ;; an arbitrary unevaluated argument passed as third element of the term,
     ;; and the environment.  If predicate returns TRUE, then expand fourth
     ;; element of the term as a rule item.
     (when (funcall term-2 term-3 env)
       (setq output-itemlist
	     (generate-item (fourth term) env output-itemlist))))

    ((eq tag '<encode-var)               ;DICOM Variable as term instantiation

     (setq varname term-2                           ;All variables have names
	   vartype term-3                           ;All variables have types
	   varlen (fourth term)                     ;All objects have lengths
	   access-chain (cddr (cdddr term))         ;Starts with SIXTH element
	   varval (apply #'item-lookup varname env t access-chain))

     (cond
       ((typep varlen 'fixnum))

       ((consp varlen)
	(cond
	  ((eq (first varlen) '<lookup-var)
	   ;; DICOM Variable environmental lookup.
	   (setq varlen
		 (apply #'item-lookup (second varlen) env t access-chain)))

	  ((eq (first varlen) '<funcall)            ;Lisp Function
	   (setq varlen (apply (second varlen) (eval-args (cddr varlen) env))))

	  (t (mishap env output-itemlist
		     "GENERATE-TERM [2] Bad length ~S in:~%~S" varlen term)))

	(unless (and (typep varlen 'fixnum)
		     (<= 0 (the fixnum varlen) 10240))
	  (mishap env output-itemlist "GENERATE-TERM [3] Bad length ~S in:~%~S"
		  varlen term)))

       (t (mishap env output-itemlist "GENERATE-TERM [4] Bad length ~S in:~%~S"
		  varlen term)))

     ;; :Big-Endian or :Little-Endian for FIXNUMs.
     ;; :No-Pad, :Space-Pad, or :Null-Pad for STRINGs.
     ;; Can be left off [ie, NIL] for 1-byte fixnums.
     (setq varend-pad (fifth term))

     (cond
       ((eq vartype 'fixnum)
	(unless (typep varval 'fixnum)
	  (mishap env output-itemlist "GENERATE-TERM [5] Bad value ~S in:~%~S"
		  varval term))
	(cond ((= (the fixnum varlen) 1)
	       (push varval output-itemlist))
	      ((and (or (= (the fixnum varlen) 2)
			(= (the fixnum varlen) 4))
		    (or (eq varend-pad :Big-Endian)
			(eq varend-pad :Little-Endian)))
	       (push (list 'fixnum varlen varend-pad varval) output-itemlist))
	      (t (mishap env output-itemlist
			 "GENERATE-TERM [6] Bad Length/Endian in:~%~S" term))))

       ((eq vartype 'string)
	(let ((strlen 0))
	  (declare (type fixnum strlen))
	  (unless (and (typep varval 'simple-base-string)
		       (setq strlen (length (the simple-base-string varval)))
		       (or (and (eq varend-pad :No-Pad)
				(= strlen (the fixnum varlen)))
			   (and (eq varend-pad :Space-Pad)
				(<= 1 strlen (the fixnum varlen)))
			   (and (eq varend-pad :Null-Pad)
				(<= (the fixnum (1- varlen))
				    strlen
				    (the fixnum varlen)))))
	    (mishap env output-itemlist "GENERATE-TERM [7] Bad term: ~S" term))
	  (push (list 'string varlen varend-pad varval) output-itemlist)))

       (t (mishap env output-itemlist "GENERATE-TERM [8] Bad type ~S in:~%~S"
		  vartype term))))

    ((eq tag '<encode-data)
     (setq output-itemlist
	   (generate-object (item-lookup (second term) env t)
			    env output-itemlist)))

    ;; <PDV-MCH terms will be expanded by SEND-PDU after fragmentation
    ;; needs are established.
    ((eq tag '<pdv-mch)
     (push term output-itemlist))

    (t (mishap env output-itemlist "GENERATE-TERM [9] Bad compound term: ~S"
	       term)))

  output-itemlist)

;;;-------------------------------------------------------------

(defun generate-item (item env output-itemlist)

  (declare (type symbol item)
	   (type list env output-itemlist))

  (let ((rulebody (get item :Generator-Rule)))
    (cond
      ((consp rulebody)
       (generate-group rulebody env output-itemlist))
      (t (mishap env output-itemlist "GENERATE-ITEM [1] Bad item: ~S" item)))))

;;;=============================================================
;;; End.
