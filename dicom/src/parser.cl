;;;
;;; parser
;;;
;;; Rule-based Recursive-Descent Parser for DICOM Message Interpretation.
;;; Contains functions common to Client and Server.
;;;
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 09-Nov-2003 BobGian - remove debugging printout code from parsing routines.
;;;

(in-package :dicom)

;;;=============================================================
;;; In parser, TAIL never changes [set during TCP read operation] and simply
;;; keeps track of the end of the PDU [point in buffer beyond which one
;;; should not read].  This assumes that all PDUs fit within the buffer, so
;;; no buffered reading is necessary.
;;;
;;; In parser, HEAD advances over input stream bytes as they are parsed,
;;; always pointing to the next byte to be parsed ["continuation pointer".
;;; HEAD is reset on backtracking to the "backtrack pointer" returned by
;;; parser functions on parse failure.

(defun parse-group (rule env tcp-buffer head tail
		    &aux (init-head head) (init-env env))

  "Success Returns:  Buffer-Head [continuation pointer]  Environment
Failure Returns:  Buffer-Head [backtrack pointer]  :Fail"

  (declare (type list rule env init-env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum head tail init-head))

  (dolist
    (term
      (cdr rule)
      (progn
	;; If nothing added to environment, return it unchanged.
	;; If anything added, package items added during parse of this
	;; group into a tagged structure and add it at front.
	(unless (eq env init-env)
	  (do ((item env (cdr item))
	       (next (cdr env) (cdr next)))
	      ((eq next init-env)
	       (setf (cdr item) nil)
	       (setq env (cons (car rule) (nreverse env)))
	       (setq env (cond ((equal env (first init-env))
				;; If environment additions duplicate items
				;; already there, ignore them.
				init-env)
			       ;; Otherwise prepend new material.
			       (t (cons env init-env)))))
	    (declare (type list item next))))
	(values head env)))

    (multiple-value-bind (input-cont new-env)
	(parse-term term env tcp-buffer head tail)
      (declare (type fixnum input-cont))
      (cond ((eq new-env :Fail)
	     (return (values init-head :Fail)))
	    (t (setq head input-cont env new-env))))))

;;;-------------------------------------------------------------

(defun parse-term (term env tcp-buffer head tail &aux tag varname
		   varval vartype varlen varend-pad (init-head head))

  "Success Returns:  Buffer-Head [continuation pointer]  Environment
Failure Returns:  Buffer-Head [backtrack pointer]  :Fail"

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type symbol varname vartype varend-pad)
	   (type fixnum head tail init-head))

  (cond
    ((typep term 'fixnum)
     (cond ((>= head tail)
	    (setq env :Fail))
	   ((= (the fixnum term) (the fixnum (aref tcp-buffer head)))
	    (setq head (the fixnum (1+ head))))
	   (t (setq env :Fail))))

    ((eq term '=ignored-byte)
     (when (> (setq head (the fixnum (1+ head))) tail)
       (setq head init-head env :Fail)))

    ((keywordp term)
     (cond ((>= head tail)
	    (setq env :Fail))
	   (t (multiple-value-setq (head env)
		  (parse-item term env tcp-buffer head tail)))))

    ((atom term)
     (mishap env tcp-buffer "PARSE-TERM [1] Bad atomic term: ~S" term))

    ;; All terms from this point onward are known to be non-empty LISTs.
    ((eq (setq tag (first term)) '=ignored-bytes)
     (when (> (setq head (the fixnum (+ head (the fixnum (second term)))))
	      tail)
       (setq head init-head env :Fail)))

    ((eq tag '>decode-var)                          ;DICOM Variable.
     (cond
       ((>= head tail)
	(setq env :Fail))

       (t (setq varname (second term)
		vartype (third term)
		varlen (fourth term))

	  (cond
	    ((typep varlen 'fixnum))

	    ((consp varlen)
	     ;; These are references to a variables or functions embedded in
	     ;; function calls -- not TERMs as defined above.
	     (cond
	       ((eq (first varlen) '<lookup-var)
		;; DICOM Variable environmental lookup.
		(setq varlen (item-lookup (second varlen) env t)))

	       ((eq (first varlen) '<funcall)       ;Lisp Function
		(setq varlen (apply (second varlen)
				    (eval-args (cddr varlen) env))))

	       (t (mishap env tcp-buffer "PARSE-TERM [2] Bad VARLEN ~S in:~%~S"
			  varlen term))))

	    (t (mishap env tcp-buffer "PARSE-TERM [3] Bad VARLEN ~S in:~%~S"
		       varlen term)))

	  (unless (and (typep varlen 'fixnum)
		       (>= (the fixnum varlen) 0))
	    (mishap env tcp-buffer "PARSE-TERM [4] Bad VARLEN ~S in:~%~S"
		    varlen term))

	  (cond
	    ((> (the fixnum (+ head varlen)) tail)
	     (mishap env tcp-buffer
		     "PARSE-TERM [5] VARLEN ~S beyond buffer in:~%~S"
		     varlen term))

	    (t (setq varend-pad (fifth term))
	       ;; :Big-Endian or :Little-Endian for FIXNUMs.
	       ;; :No-Pad, :Space-Pad, or :Null-Pad for STRINGs.
	       ;; May be NIL for 1-byte fixnums or :Message structures.
	       (cond
		 ((eq vartype 'fixnum)

		  (cond
		    ((= (the fixnum varlen) 1)
		     ;; VAREND-PAD not required and not checked since Endian
		     ;; status is irrelevant for single-byte FIXNUMs.  Must
		     ;; use NIL as placeholder in term's expression if there
		     ;; are additional arguments.
		     (setq varval (aref tcp-buffer head))
		     (setq head (the fixnum (1+ head))))

		    ((and (= (the fixnum varlen) 2)
			  (eq varend-pad :Big-Endian))
		     (setq varval
			   (logior (ash (the (integer #x00 #xFF)
					  (aref tcp-buffer head)) 8)
				   (the (integer #x00 #xFF)
				     (aref tcp-buffer
					   (the fixnum (1+ head))))))
		     (setq head (the fixnum (+ head 2))))

		    ((and (= (the fixnum varlen) 2)
			  (eq varend-pad :Little-Endian))
		     (setq varval
			   (logior
			     (the (integer #x00 #xFF) (aref tcp-buffer head))
			     (ash (the (integer #x00 #xFF)
				    (aref tcp-buffer
					  (the fixnum (1+ head)))) 8)))
		     (setq head (the fixnum (+ head 2))))

		    ((and (= (the fixnum varlen) 4)
			  (eq varend-pad :Big-Endian))
		     ;; Masks should be #xFF, but using smaller value keeps
		     ;; everything POSITIVE FIXNUM, and no value will exceed
		     ;; 536870911.
		     (setq varval
			   (logior
			     (ash (the (integer #x00 #x1F)
				    (logand #x1F
					    (the (integer #x00 #xFF)
					      (aref tcp-buffer head))))
				  24)
			     (ash (the (integer #x00 #xFF)
				    (aref tcp-buffer (the fixnum (1+ head))))
				  16)
			     (ash (the (integer #x00 #xFF)
				    (aref tcp-buffer (the fixnum (+ head 2))))
				  8)
			     (the (integer #x00 #xFF)
			       (aref tcp-buffer (the fixnum (+ head 3))))))
		     (setq head (the fixnum (+ head 4))))

		    ((and (= (the fixnum varlen) 4)
			  (eq varend-pad :Little-Endian))
		     ;; Masks should be #xFF, but using smaller value keeps
		     ;; everything POSITIVE FIXNUM, and no value will exceed
		     ;; 536870911.
		     (setq varval
			   (logior
			     (ash (the (integer #x00 #x1F)
				    (logand
				      #x1F
				      (the (integer #x00 #xFF)
					(aref tcp-buffer
					      (the fixnum (+ head 3))))))
				  24)
			     (ash (the (integer #x00 #xFF)
				    (aref tcp-buffer (the fixnum (+ head 2))))
				  16)
			     (ash (the (integer #x00 #xFF)
				    (aref tcp-buffer (the fixnum (1+ head))))
				  8)
			     (the (integer #x00 #xFF) (aref tcp-buffer head))))
		     (setq head (the fixnum (+ head 4))))

		    (t (mishap env tcp-buffer
			       "PARSE-TERM [6] Bad Length/Endian in:~%~S"
			       term))))

		 ((and (eq vartype 'string)
		       (or (eq varend-pad :No-Pad)
			   (eq varend-pad :Space-Pad)
			   (eq varend-pad :Null-Pad)))
		  (setq varval (make-string varlen))
		  (do ((idx 0 (the fixnum (1+ idx))))
		      ((= idx (the fixnum varlen)))
		    (declare (type fixnum idx))
		    (setf (aref (the simple-base-string varval) idx)
			  (code-char (aref tcp-buffer head)))
		    (setq head (the fixnum (1+ head))))
		  ;; VARLEN is number of bytes to read from input stream and
		  ;; includes any padding bytes.  Must trim strings AFTER
		  ;; copying bytes and incrementing HEAD VARLEN times.
		  (cond ((eq varend-pad :Null-Pad)
			 (setq varval (string-right-trim '(#\Null) varval)))
			((eq varend-pad :Space-Pad)
			 (setq varval (string-right-trim '(#\Space) varval)))))

		 ;; Structure: ( :Message <Start-Idx> <End-Idx> )
		 ;; Both indices must be within current PDV.
		 ((eq vartype :Message)
		  (setq varval (list :Message
				     head
				     (setq head (the fixnum
						  (+ head varlen))))))

		 (t (mishap env tcp-buffer
			    "PARSE-TERM [7] Bad type ~S in:~%~S"
			    vartype term)))

	       (push (cons varname varval) env))))))

    ((eq tag :Repeat)
     (multiple-value-setq (head env)
	 (parse-repeats (cdr term) env tcp-buffer head tail)))

    (t (mishap env tcp-buffer "PARSE-TERM [8] Bad compound term: ~S" term)))

  (values head env))

;;;-------------------------------------------------------------

(defun parse-item (item env tcp-buffer head tail)

  "Success Returns:  Buffer-Head [continuation pointer]  Environment
Failure Returns:  Buffer-Head [backtrack pointer]  :Fail"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum head tail))

  (let ((rule (get item :Parser-Rule)))
    (cond ((consp rule)
	   (multiple-value-bind (input-cont new-env)
	       (parse-group rule env tcp-buffer head tail)
	     (declare (type fixnum input-cont))
	     (values input-cont new-env)))
	  (t (mishap env tcp-buffer "PARSE-ITEM [1] Bad item: ~S" item)))))

;;;-------------------------------------------------------------

(defun parse-repeats (repeater env tcp-buffer head tail &aux (init-head head)
		      lowlimit highlimit (repeat-code (first repeater))
		      (repeat-item (second repeater)))

  "Success Returns:  Buffer-Head [continuation pointer]  Environment
Failure Returns:  Buffer-Head [backtrack pointer]  :Fail"

  (declare (type list repeater env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum head tail init-head))

  (cond ((typep repeat-code 'fixnum)
	 (setq lowlimit repeat-code highlimit repeat-code))

	((and (consp repeat-code)
	      (typep (setq lowlimit (first repeat-code)) 'fixnum)
	      (or (typep (setq highlimit (second repeat-code)) 'fixnum)
		  (and (eq highlimit :No-Limit)
		       (setq highlimit #.Most-Positive-Fixnum)))))

	(t (mishap env tcp-buffer "PARSE-REPEATS [1] Bad repeat-code: ~S"
		   repeater)))

  (do ((repeat-count 0 (the fixnum (1+ repeat-count))))
      ((= repeat-count (the fixnum highlimit))
       ;; Succeeded in parsing HIGHLIMIT items -- successful return.  If there
       ;; are more such items in the input stream not matched by next element
       ;; in current rule, parser will detect the mismatch when trying to
       ;; parse the next item.
       (values head env))

    (declare (type fixnum repeat-count))

    (multiple-value-bind (input-cont new-env)
	(parse-item repeat-item env tcp-buffer head tail)

      (declare (type fixnum input-cont))

      (cond ((eq new-env :Fail)
	     (cond ((< repeat-count (the fixnum lowlimit))
		    ;; Failure BEFORE parsing LOWLIMIT items -- backtrack.
		    (return (values init-head :Fail)))
		   ;; Failure AFTER parsing LOWLIMIT items -- continue.
		   (t (return (values head env)))))

	    ;; Advance HEAD past bytes parsed successfully.
	    (t (setq head input-cont env new-env))))))

;;;=============================================================
;;; End.
