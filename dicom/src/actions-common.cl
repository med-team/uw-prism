;;;
;;; actions-common
;;;
;;; DICOM Upper-Layer Protocol Action functions common to Client and Server.
;;; Contains functions common to Client and Server.
;;;
;;; 09-May-2001 BobGian DT-02 handles data file writeout [object output
;;;   in general] after object parse, rather than embedding writeout
;;;   functionality inside PARSE-OBJECT.
;;; 07-Mar-2002 BobGian fix DT-02 to pop PDV items off environment stack
;;;   after data is processed, preventing unbounded stack growth.
;;; 15-Mar-2002 BobGian checkpoint environment just before decoding and
;;;   executing command, and restore it when done with command.
;;; 15-Apr-2002 BobGian DT-02 passes all continuations to PARSE-OBJECT,
;;;   without discriminating on type.  Assumed to be C-STORE, any subtype.
;;;   Fix bug in discrimination of Storage-Services provided by Server.
;;; 24-Apr-2002 BobGian *STATUS-MESSAGE* set to "Success" only upon
;;;   successful completion, not as initialization.
;;; 24-Apr-2002 BobGian triggering EVENT-15 sets *STATUS-MESSAGE* rather
;;;   than action function invoked - finer discrimination this way.
;;; 26-Apr-2002 BobGian *STATUS-MESSAGE* set by any abort action function,
;;;   unless set previously by some error-detecting event.
;;; 06-May-2002 BobGian add error message arg to REPORT-ERROR calls.
;;; 26-Jun-2002 BobGian REPORT-ERROR called on errors regardless of log level.
;;; Jul/Aug 2002 BobGian DT-02 does dispatch on SOP Class in C-Store-RQ to
;;;   invoke WRITE-DICOM-OUTPUT for :Image or :Structure-Set data,
;;;   rather than just for :Image data.  Invokes DUMP-DICOM-DATA for all
;;;   non-implemented SOP Classes.
;;; 21-Aug-2002 BobGian DUMP-DICOM-DATA invoked at log level 1 for all C-Store
;;;   operations, at log level 0 for all non-implemented SOP Classes.
;;; 24-Sep-2002 BobGian set special var *DICOM-ALIST* to hold parsed data
;;;   once available - provides access to all error-reporting functions.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 03-Nov-2003 BobGian - Add *STANDARD-OUTPUT* arg to DUMP-DICOM-DATA.
;;; 21-Dec-2003 BobGian: Add arg to PARSE-OBJECT for ignorable slots.
;;; 15-Mar-2005 BobGian: Move WRITE-DICOM-OUTPUT dicom -> prism package.
;;;

(in-package :dicom)

;;;=============================================================

;;; All Action functions must return either an updated environment [CONSP]
;;; resulting from parsing a command or NIL.  Don't accidently let some
;;; random trailing value get returned.

;;;=============================================================
;;; Data-Transfer Actions.
;;; This function is invoked by the receipt of a P-Data-TF PDU containing
;;; a DICOM message [Command or Data-Set].  For server, message can be a
;;; C-Echo-RQ command, C-Store-RQ command, or C-Store dataset.
;;; For client, message can be a response [C-Store-RSP or C-Echo-RSP] from
;;; server to client's sending an RTPlan or an Echo-Verification request.
;;;
;;; This action function receives the current environment and extends it
;;; internally [by parsing messages and datasets].  This extended environment
;;; must be passed back to the caller so that other action functions can see
;;; the new information.  This is the only action function that can return
;;; a non-NIL new environment.

(defun dt-02 (env tcp-buffer tcp-strm &aux new-env tmp)

  "Issue P-Data, decode message, take action"

  (declare (type list env new-env *checkpointed-environment*)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  ;; Pop latest PDV item(s) off environment stack - data already processed,
  ;; so we don't need to accumulate multiple PDV frames in environment.
  (do ((ptr env (cdr ptr)))
      ((not (eq (caar ptr) :P-Data-TF))
       (setq new-env ptr)))

  ;; If environment has not been checkpointed [ie, after association has been
  ;; established and before evaluating a command or data-transfer message]
  ;; checkpoint environment for restoration upon completion of command.
  (let ((checkpointed-env *checkpointed-environment*))
    (unless (consp checkpointed-env)
      (setq *checkpointed-environment* new-env)))

  ;; Give SET-LOOKUP the ENV before PDV is popped off!
  (dolist (pdv (set-lookup env :PDV-Item :P-Data-TF))

    (let ((id (item-lookup 'PC-ID pdv t))
	  (mch (item-lookup 'PDV-MCH pdv t))
	  (msg (item-lookup 'PDV-Message pdv t))
	  (cmd-tag))

      (declare (type list msg)
	       (type symbol cmd-tag)
	       (type fixnum id mch))

      ;; MSG value:  ( :Message <Start-Idx> <End-Idx> )
      ;; Both indices must be within current PDV.
      ;; Message Control Header (MCH) [1 byte]:
      ;;  #b******XY  [* is don't-care bit, X and Y are 2 lowest-order bits]
      ;;  Bit X = 0 -> Message is NOT LAST fragment.
      ;;  Bit X = 1 -> Message is LAST fragment.
      ;;  Bit Y = 0 -> Message is Data-Set.
      ;;  Bit Y = 1 -> Message is a Command.

      (when (>= (the fixnum *log-level*) 3)
	(format t "~%DT-02: P-Data received: ~A, ~A fragment.~%"
		(if (= (logand #x01 mch) #x01) "Command" "Data-Set")
		(if (= (logand #x02 mch) #x02) "Last" "Internal")))

      (cond
	((= (logand #x01 mch) #x01)                 ;Message is a Command
	 (multiple-value-setq (cmd-tag new-env)
	     (parse-message new-env                 ;Environment, PDV popped
			    tcp-buffer             ;Source-Array -- TCP buffer
			    (second msg)            ;Start-Idx of PDV
			    (third msg)))           ;End-Idx of PDV

	 ;; Put PC-ID for this particular PDV-Item into environment as
	 ;; a "global" value so that generator can retrieve it later
	 ;; for constructing response to the command.
	 (push `(PC-ID . ,id) new-env)

	 (cond
	   ((eq cmd-tag :C-Echo-RQ)
	    (cond
	      ((string= (setq tmp (item-lookup 'Echo-SOP-Class-UID-Str
					       new-env t :C-Echo-RQ))
			*Echo-Verification-Service*)
	       (when (>= (the fixnum *log-level*) 3)
		 (format t "~%DT-02: C-Echo-Cmd received.~%"))
	       ;; Sending complete PDU containing C-Echo-RSP message.
	       (send-pdu :C-Echo-RSP new-env tcp-buffer tcp-strm 'PC-ID id)
	       (format t "~%Echo Verification test succeeded.~%"))

	      (t (setq *event* 'event-15)
		 ;; Abort-Source = 2: UL Service-Provider-initiated
		 ;; Abort-Diagnostic = 6: Invalid PDU Parameter Value
		 (setq *args* `(PC-ID ,id Abort-Source 2 Abort-Diagnostic 6))
		 (format t "~%DT-02 [1] ~A~%"
			 (setq *status-message*
			       (format nil "Bad SOP-Class-UID: ~S" tmp)))
		 (report-error new-env tcp-buffer *status-message*))))

	   ((eq cmd-tag :C-Echo-RSP)
	    (format t "~%Echo Verification test succeeded.~%")
	    (format t "~%Echo Message ID: ~S"
		    (item-lookup 'Echo-Msg-ID new-env t :C-Echo-RSP))
	    (format t "~%Echo SOP Class UID: ~S"
		    (item-lookup 'Echo-SOP-Class-UID-Str
				 new-env t :C-Echo-RSP))
	    (format t "~%Echo Verification status: ~S~%"
		    (setq *status-code*
			  (item-lookup 'Echo-Msg-Status
				       new-env t :C-Echo-RSP)))
	    (when (= (the fixnum *status-code*) 0)
	      (setq *status-message* "Success"))
	    (setq *event* 'event-11))

	   ((eq cmd-tag :C-Store-RQ)
	    (let ((storage-service
		    (item-lookup 'Store-SOP-Class-UID-Str
				 new-env t :C-Store-RQ)))
	      (declare (type simple-base-string storage-service))
	      (cond
		((not (member storage-service *Object-Storage-Services*
			      :test #'string=))
		 (setq *event* 'event-15)
		 ;; Abort-Source = 2: UL Service-Provider-initiated
		 ;; Abort-Diagnostic = 6: Invalid PDU Parameter Value
		 (setq *args* `(PC-ID ,id Abort-Source 2 Abort-Diagnostic 6))
		 (format t "~%DT-02 [2] ~A~%"
			 (setq *status-message*
			       (format nil "Bad SOP-Class-UID: ~S"
				       storage-service)))
		 (report-error new-env tcp-buffer *status-message*))

		((= (the fixnum
		      (item-lookup 'DataSet-Type new-env t :C-Store-RQ))
		    #x0101)
		 (setq *event* 'event-15)
		 ;; Abort-Source = 2: UL Service-Provider-initiated
		 ;; Abort-Diagnostic = 6: Invalid PDU Parameter Value
		 (setq *args* `(PC-ID ,id Abort-Source 2 Abort-Diagnostic 6))
		 (format t "~%DT-02 [3] ~A~%"
			 (setq *status-message*
			       "Bad DataSet-Type: #x0101 (no dataset)"))
		 (report-error new-env tcp-buffer *status-message*)))))

	   ((eq cmd-tag :C-Store-RSP)
	    (format t "~%Store Message ID: ~S"
		    (item-lookup 'Store-Msg-ID new-env t :C-Store-RSP))
	    (format t "~%Store SOP Class UID: ~S"
		    (item-lookup 'Store-SOP-Class-UID-Str
				 new-env t :C-Store-RSP))
	    (format t "~%Store SOP Instance UID: ~S"
		    (item-lookup 'Store-SOP-Instance-UID-Str
				 new-env t :C-Store-RSP))
	    (format t "~%C-Store (RTPlan) status: ~S~%"
		    (setq *status-code*
			  (item-lookup 'Store-Msg-Status
				       new-env t :C-Store-RSP)))
	    (when (= (the fixnum *status-code*) 0)
	      (setq *status-message* "Success"))
	    (setq *event* 'event-11))

	   (t (setq *event* 'event-15)
	      ;; Abort-Source = 2: UL Service-Provider-initiated
	      ;; Abort-Diagnostic = 1: Unrecognized PDU
	      (setq *args* `(PC-ID ,id Abort-Source 2 Abort-Diagnostic 1))
	      (format t "~%DT-02 [4] ~A~%"
		      (setq *status-message*
			    (format nil "Unrecognized command: ~S" cmd-tag)))
	      (when (>= (the fixnum *log-level*) 2)
		;; PARSE-MESSAGE already reported the failed parse and dumped
		;; the message in hex, but at this logging level the entire
		;; PDU will get dumped.
		(report-error new-env tcp-buffer *status-message*)))))

	;; Message is a DataSet -- only C-Store datasets handled so far.
	(t (let ((dicom-alist
		   (parse-object new-env            ;Environment, PDV popped
				 tcp-buffer         ;TCP buffer
				 (second msg)       ;Start-Idx of PDV
				 (third msg)        ;End-Idx of PDV
				 (= (logand #x02 mch) #x02) ;Last fragment?
				 *parser-state*     ;Continuation
				 *ignorable-groups-list*))  ;Ignorable slots
		 (so *standard-output*))
	     (declare (type list dicom-alist))
	     (when (consp dicom-alist)
	       (setq *dicom-alist* dicom-alist)     ;Error-reporting handle.
	       (let ((storage-service
		       (item-lookup 'Store-SOP-Class-UID-Str
				    new-env t :C-Store-RQ)))
		 (declare (type simple-base-string storage-service))
		 (cond
		   ;; All server-implemented Image Storage Class SOPs here.
		   ((member storage-service *Image-Storage-Services*
			    :test #'string=)
		    (when (>= (the fixnum *log-level*) 1)
		      (dump-dicom-data dicom-alist so))
		    (pr::write-dicom-output :Image dicom-alist))
		   ;; Handler for Structure-Sets.
		   ((string= storage-service *Structure-Set-Storage-Service*)
		    (when (>= (the fixnum *log-level*) 1)
		      (dump-dicom-data dicom-alist so))
		    (pr::write-dicom-output :Structure-Set dicom-alist))
		   ;; Handler for RT-Plans.  Debugging dumper.
		   ((string= storage-service *RTPlan-Storage-Service*)
		    (dump-dicom-data dicom-alist so))
		   ;; Below is default debug dumper for all unimplemented SOPs.
		   (t (dump-dicom-data dicom-alist so))))
	       (send-pdu :C-Store-RSP new-env tcp-buffer tcp-strm)
	       ;; Completion of command [don't reset environment until done,
	       ;; as indicated by non-null DICOM-ALIST - on pre-completion
	       ;; calls PARSE-OBJECT sets continuation and returns NIL] -
	       ;; restore checkpointed environment and mark environment-saving
	       ;; variable as ready for next save.
	       (setq new-env *checkpointed-environment*)
	       (setq *checkpointed-environment* nil)))))))

  ;; Be sure to return the [possibly] updated environment.
  new-env)

;;;=============================================================
;;; Association Release Actions.

(defun ar-02 (env tcp-buffer tcp-strm)

  "Issue A-Release message"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer tcp-strm))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AR-02: Initiating A-Release.~%"))

  (setq *event* 'event-14)                ;Signal EVENT-14: A-Release Response

  nil)

;;;-------------------------------------------------------------

(defun ar-08 (env tcp-buffer tcp-strm)

  "Issue A-Release message [release collision]"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer tcp-strm))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AR-08: A-Release by Association ~A.~%"
	    (cond ((eq *mode* :Client) "initiator")
		  (t "acceptor"))))

  ;; Client signals EVENT-14 to initiate A-Release.
  (when (eq *mode* :Client)
    (setq *event* 'event-14))

  nil)

;;;=============================================================
;;; Association Abort Actions.
;;; Invoke this function, by signaling EVENT-15, for any detected
;;; inconsistency that requires an abort.  If possible, explain reason by
;;; conveying Abort-Source and Abort-Diagnostic via *ARGS*.
;;; If not possible, reason defaults to "UL Service-User-initiated"
;;; and "Unexpected PDU".

(defun aa-01 (env tcp-buffer tcp-strm)

  "Error detected -- send A-Abort PDU (Service-User-Initiated)"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%" (setq *status-message* "Received Unexpected PDU.")))

  (cond ((consp *args*)
	 (apply #'send-pdu :A-Abort env tcp-buffer tcp-strm *args*)
	 (setq *args* nil))

	;; Abort-Source = 0: UL Service-User-initiated
	;; Abort-Diagnostic = 2: Unexpected PDU
	(t (send-pdu :A-Abort env tcp-buffer tcp-strm
		     'Abort-Source 0 'Abort-Diagnostic 2)
	   nil)))

;;;-------------------------------------------------------------

(defun aa-02 (env tcp-buffer tcp-strm)

  "ARTIM timeout"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%" (setq *status-message* "ARTIM Timeout.")))

  (when (eq *mode* :Client)
    ;; Connection should always be open in states where this function
    ;; is called, so OK for CLOSE-CONNECTION to error out if not.
    (close-connection tcp-strm))

  nil)

;;;-------------------------------------------------------------
;;; A-Abort PDU received.

(defun aa-03 (env tcp-buffer tcp-strm)

  "Issue A-Abort/A-P-Abort message, close connection, leave DUL main loop"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%" (setq *status-message* "Received A-Abort PDU.")))

  (when (eq *mode* :Client)
    (close-connection tcp-strm))

  nil)

;;;-------------------------------------------------------------
;;; Connection-Closed detected.

(defun aa-04 (env tcp-buffer tcp-strm)

  "Issue A-P-Abort message, close connection, leave DUL main loop"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer))

  (when (and (eq *mode* :Client)
	     (streamp tcp-strm))
    (close-connection tcp-strm))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%"
	    (setq *status-message* "Connection-Closed Abort detected.")))

  nil)

;;;-------------------------------------------------------------
;;; PDU received while waiting for connection to close.

(defun aa-06 (env tcp-buffer tcp-strm)

  "Ignore invalid/unhandleable PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer tcp-strm))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%" (setq *status-message* "Received invalid PDU.")))

  nil)

;;;-------------------------------------------------------------
;;; A-Associate-RQ PDU received while waiting for connection to close.

(defun aa-07A (env tcp-buffer tcp-strm)

  "Unexpected PDU received -- Send A-Abort PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%"
	    (setq *status-message*
		  "Rcvd A-Associate-RQ PDU waiting for connection to close.")))

  ;; Abort-Source = 2: UL Service-Provider-initiated
  ;; Abort-Diagnostic = 2: Unexpected PDU
  (send-pdu :A-Abort env tcp-buffer tcp-strm
	    'Abort-Source 2 'Abort-Diagnostic 2)

  nil)

;;;-------------------------------------------------------------
;;; Unrecognized/Invalid PDU received while waiting for connection to close.

(defun aa-07B (env tcp-buffer tcp-strm)

  "Unrecognized PDU received -- Send A-Abort PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%"
	    (setq *status-message*
		  "Rcvd Unrecognized PDU waiting for connection to close.")))

  ;; Abort-Source = 2: UL Service-Provider-initiated
  ;; Abort-Diagnostic = 1: Unrecognized PDU
  (send-pdu :A-Abort env tcp-buffer tcp-strm
	    'Abort-Source 2 'Abort-Diagnostic 1)

  nil)

;;;-------------------------------------------------------------
;;; Like AA-01 [abort reasons defaulted to "Unexpected PDU" unless conveyed
;;; via *ARGS*] except that it prints message too [if logging].

(defun aa-08 (env tcp-buffer tcp-strm &aux (args *args*))

  "Send A-Abort PDU and issue A-P-Abort message"

  (declare (type list env args)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (unless (typep *status-message* 'simple-base-string)
    (format t "~%~A~%"
	    (setq *status-message*
		  (format nil "Sending Abort - reasons: ~S" args))))

  (cond ((consp args)
	 (apply #'send-pdu :A-Abort env tcp-buffer tcp-strm args)
	 (setq *args* nil))

	;; Abort-Source = 2: UL Service-Provider-initiated
	;; Abort-Diagnostic = 2: Unexpected PDU
	(t (send-pdu :A-Abort env tcp-buffer tcp-strm
		     'Abort-Source 2 'Abort-Diagnostic 2)
	   nil)))

;;;=============================================================
;;; End.
