;;;
;;; compiler
;;;
;;; Rule Compiler for State Table and Parsing/Generation Rules.
;;; Contains functions common to Client and Server.
;;;
;;; 02-Mar-2002 BobGian functions embedded in rules moved here from
;;;   "utilities.cl".
;;; Jul/Aug 2002 BobGian rename local var: RTP-COUNT -> REPEAT-COUNT.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================
;;; The run-time versions of rules comprise optimized and compiled
;;; rule bodies stored as properties on the property lists of the symbols
;;; naming the rule item types.

(defun compile-rules (rules rule-type &aux tag-symbol
		      (pdutype-alist *Code/PDUtype-Alist*))

  (declare (type list rules pdutype-alist)
	   (type symbol tag-symbol))

  (dolist (rule rules)
    (setq tag-symbol (first rule))
    (setq rule
	  (mapcan
	      ;; Expander functions return the LIST of atomic terms
	      ;; that a complex term expression expands into.  Must
	      ;; use MAPCAN to append them.
	      #'(lambda (term &aux key output)
		  (setq output
			(cond
			  ((consp term)
			   (cond ((eq (setq key (first term)) '=fixnum-bytes)
				  (fixnum-bytes-expander (second term)
							 (third term)
							 (fourth term)))
				 ((eq key '=string-bytes)
				  (string-bytes-expander (second term)
							 (third term)
							 (fourth term)))
				 ((eq key '=constant-bytes)
				  (constant-bytes-expander (second term)
							   (third term)))
				 (t (list term))))
			  (t (list term))))
		  output)
	    rule))

    ;; We store all rules as a property of the tag-symbol with the property
    ;; named by RULE-TYPE value (:Parser-Rule or :Generator-Rule).
    (setf (get tag-symbol rule-type)
	  (cond ((eq rule-type :Parser-Rule)
		 (cond ((member tag-symbol pdutype-alist :test #'eq :key #'cdr)
			;; For Parse rules for the seven basic PDU types,
			;; we parse the PDU length procedurally but represent
			;; it in the rule [for human readability] by an
			;; "(=IGNORED-BYTES 4)" term.  Slice this plus the
			;; extra =IGNORED-BYTE always present from the run-time
			;; version of the rule.  Also remove the PDU type code
			;; [second element], but leave the type tag [the first
			;; element, which PARSE-GROUP uses to tag the variable
			;; group for this item in environment].
			(cons tag-symbol (cddddr rule)))

		       ;; For parser non-PDU item rules, leave rule as
		       ;; written.  Must include the type tag and type code.
		       ;; This applies to subitem types and message types.
		       (t rule)))

		;; For all generation rules, remove only the type tag.
		(t (cdr rule))))))

;;;-------------------------------------------------------------

(defun fixnum-bytes-expander (dataval datalen dataend)

  (unless (typep dataval 'fixnum)
    ;; Arbitrary Lisp form can be in DATAVAL slot.
    (setq dataval (eval dataval)))

  (unless (typep datalen 'fixnum)
    ;; Arbitrary Lisp form can be in DATALEN slot -- not currently used.
    (setq datalen (eval datalen)))

  (unless (and (typep dataval 'fixnum)
	       (typep datalen 'fixnum)
	       (or (= (the fixnum datalen) 1)
		   ;; For 1 byte, DATAEND = NIL [ie, 3-element term] is OK.
		   (member dataend '(:Big-Endian :Little-Endian) :test #'eq)))
    (error "FIXNUM-BYTES-EXPANDER [1] Bad args: ~S ~S ~S"
	   dataval datalen dataend))

  (cond ((= (the fixnum datalen) 1)
	 (list (logand #x000000FF (the fixnum dataval))))

	((and (= (the fixnum datalen) 2)
	      (eq dataend :Big-Endian))
	 (list (ash (logand #x0000FF00 (the fixnum dataval)) -8)
	       (logand #x000000FF (the fixnum dataval))))

	((and (= (the fixnum datalen) 2)
	      (eq dataend :Little-Endian))
	 (list (logand #x000000FF (the fixnum dataval))
	       (ash (logand #x0000FF00 (the fixnum dataval)) -8)))

	((and (= (the fixnum datalen) 4)
	      (eq dataend :Big-Endian))
	 ;; Largest mask really should be #xFF000000, but using smaller value
	 ;; keeps it a POSITIVE FIXNUM, and no value will exceed 536870911.
	 (list (ash (logand #x1F000000 (the fixnum dataval)) -24)
	       (ash (logand #x00FF0000 (the fixnum dataval)) -16)
	       (ash (logand #x0000FF00 (the fixnum dataval)) -8)
	       (logand #x000000FF (the fixnum dataval))))

	((and (= (the fixnum datalen) 4)
	      (eq dataend :Little-Endian))
	 ;; Largest mask really should be #xFF000000, but using smaller value
	 ;; keeps it a POSITIVE FIXNUM, and no value will exceed 536870911.
	 (list (logand #x000000FF (the fixnum dataval))
	       (ash (logand #x0000FF00 (the fixnum dataval)) -8)
	       (ash (logand #x00FF0000 (the fixnum dataval)) -16)
	       (ash (logand #x1F000000 (the fixnum dataval)) -24)))

	(t (error "FIXNUM-BYTES-EXPANDER [2] Bad values: ~S ~S ~S"
		  dataval datalen dataend))))

;;;-------------------------------------------------------------
;;; :Space-Pad is nowhere used but is included here for completeness.

(defun string-bytes-expander (dataval datalen string-padding &aux (strlen 0))

  (declare (type fixnum strlen))

  (unless (typep dataval 'simple-base-string)
    ;; Arbitrary Lisp form can be in DATAVAL slot.
    (setq dataval (eval dataval)))

  (unless (typep datalen 'fixnum)
    ;; Arbitrary Lisp form can be in DATALEN slot.
    (setq datalen (eval datalen)))

  (unless (and (typep dataval 'simple-base-string)
	       (typep datalen 'fixnum)
	       (member string-padding '(:No-Pad :Null-Pad #+ignore :Space-Pad)
		       :test #'eq)
	       (<= (setq strlen (length (the simple-base-string dataval)))
		   (the fixnum datalen)))
    (error "STRING-BYTES-EXPANDER [1] Bad args: ~S ~S ~S"
	   dataval datalen string-padding))

  (do ((idx (the fixnum (1- strlen)) (the fixnum (1- idx)))
       (output (cond ((eq string-padding :No-Pad)
		      '())
		     ((eq string-padding :Null-Pad)
		      (cond ((oddp strlen)
			     (list 0))
			    (t '())))
		     #+ignore
		     ((and (eq string-padding :Space-Pad)
			   (< strlen (the fixnum datalen)))
		      (make-list (the fixnum (- datalen strlen))
				 :initial-element #.(char-code #\Space)))
		     (t '()))))
      ((< idx 0)
       output)

    (declare (type list output)
	     (type fixnum idx))

    (push (char-code (aref (the simple-base-string dataval) idx)) output)))

;;;-------------------------------------------------------------

(defun constant-bytes-expander (byte-value repeat-count)

  (unless (and (typep byte-value 'fixnum)
	       (typep repeat-count 'fixnum))
    (error "CONSTANT-BYTES-EXPANDER [1] Bad args: ~S ~S"
	   byte-value repeat-count))

  (do ((cnt 0 (the fixnum (1+ cnt)))
       (output '()))
      ((= cnt (the fixnum repeat-count))
       output)

    (declare (type list output)
	     (type fixnum cnt))

    (push (the fixnum byte-value) output)))

;;;-------------------------------------------------------------

(defun compile-states (rules)

  (declare (type list rules))

  (dolist (rule-packet rules)
    (let ((state (first rule-packet))
	  (doc (second rule-packet)))
      (setf (get state 'documentation) doc)
      (dolist (rule (cddr rule-packet))
	(let ((actions (cdr rule)))
	  (dolist (event (car rule))
	    (setf (get state event) actions)))))))

;;;=============================================================
;;; Functions Embedded in Rules for DICOM Message Interpretation.

;;; This function computes an even length [rounding up for odd lengths]
;;; for a string which is to be encoded using :Null-Pad -- all UIDs other
;;; than when used in A-Associate-RQ and A-Associate-AC rules.

(defun even-length (str &aux (len (length str)))

  (declare (type simple-base-string str)
	   (type fixnum len))

  (cond ((oddp len)
	 (the fixnum (1+ len)))
	(t len)))

;;;-------------------------------------------------------------
;;; This function also serves as a variable value predicate embedded
;;; in generation rules.

(defun item-present? (access-chain env)

  (declare (type list access-chain env))

  (cond
    ((null access-chain)
     (mishap env nil "ITEM-PRESENT? [1] Null access-chain."))
    ((null (cdr access-chain))
     (assoc (car access-chain) env :test #'eq))
    (t (assoc (car access-chain) (cdr (item-present? (cdr access-chain) env))
	      :test #'eq))))

;;;=============================================================
;;; End.
