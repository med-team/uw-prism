;;;
;;; utilities
;;;
;;; Functions Embedded in Rules for DICOM Message Interpretation.
;;; Utility functions for Object Parsing, Error Recovery and Logging,
;;; Data/Time, Environmental Printout, PDU Dumping, Debugging, and Testing.
;;; Contains functions common to Client and Server.
;;;
;;; 13-Apr-2001 BobGian fix DUMP-BYTESTREAM to dump max of *MAX-DUMPLEN* bytes.
;;; 23-Apr-2001 BobGian fix DUMP-BYTESTREAM to dump arbitrary region of PDU.
;;; 23-Apr-2001 BobGian simplify and improve error reporting.
;;; 25-Apr-2001 BobGian fix DUMP-BYTESTREAM to dump arbitrary region of PDU.
;;; 09-May-2001 BobGian improve formatting of environment printout.
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 15-Oct-2001 BobGian remove *FILE-MOVE-LIST*.
;;; 23-Jan-2002 BobGian add DUMP-DICOM-DATA as debug printer - independent
;;;   from logging functions in "dicom-rtplan" and Prism package.
;;; 23-Jan-2002 BobGian divide REPORT-ERROR into separate functions, for
;;;   Client and for Server, reporting global vars specialized to each role.
;;; 18-Feb-2002 BobGian change DUMP-DICOM-DATA to write to standard output.
;;; 02-Mar-2002 BobGian functions embedded in rules moved to "compiler.cl".
;;; 16-Apr-2002 BobGian second arg to MISHAP for printing arbitrary list
;;;   structure or dumping TCP-Buffer - passed to REPORT-ERROR.
;;; 04-May-2002 BobGian DUMP-BYTESTREAM dumps all bytes between HEAD and TAIL.
;;; 06-Aug-2002 BobGian *PRINT-PRETTY* -> T in DUMP-DICOM-DATA.
;;; 21-Aug-2002 BobGian *PRINT-ARRAY* -> NIL in DUMP-DICOM-DATA (Oops! :-).
;;; 17-Sep-2002 BobGian:
;;;   *PRINT-ARRAY* -> NIL always except when needed to be T.
;;;   MISHAP takes new 3rd arg (DICOM-ALIST) and passes it to REPORT-ERROR.
;;; 24-Sep-2002 BobGian:
;;;   Remove 3rd arg (DICOM-ALIST) to REPORT-ERROR and MISHAP.  Same
;;;   functionality now obtained via special variable set when data available.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 03-Nov-2003 BobGian New pretty-printer and data formatter written for
;;;    debugging client replaces dumper in server as well.  DUMP-DICOM-DATA
;;;    is used as calling interface.  Actual dumper is DISPLAY-DICOM-DATA.
;;; 09-Nov-2003 BobGian - remove debugging code [for testing parsing routines].
;;; 10-Sep-2004 BobGian - add TERPRI to header in DUMP-DICOM-DATA.
;;;

(in-package :dicom)

;;;=============================================================
;;; Useful Time-Display Utility.

(defun date/time (&aux (universal-time (get-universal-time)))

  (declare (type integer universal-time))

  (multiple-value-bind (seconds minutes hours days months years)
      (decode-universal-time universal-time)

    (declare (type fixnum seconds minutes hours days months years))

    (format nil "~A~D-~A-~D ~A~D:~A~D:~A~D"
	    (if (< days 10) "0" "") days
	    (nth (the fixnum (1- months))
		 '("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul"
		   "Aug" "Sep" "Oct" "Nov" "Dec"))
	    years
	    (if (< hours 10) "0" "") hours
	    (if (< minutes 10) "0" "") minutes
	    (if (< seconds 10) "0" "") seconds)))

;;;=============================================================
;;; Error condition escape.  If a run-time error occurs, MISHAP prints a
;;; message to the log stream [regardless of Logging Level] and then:
;;;  In client, invokes the debugger by calling an untrapped ERROR.
;;;  In server, invokes ERROR, which is trapped by IGNORE-ERRORS in
;;;    wrapper functions.

(defun mishap (env data msg &rest format-args)

  ;; ENV may be NIL or a CONS.  DATA may be NIL, an ARRAY [TCP-Buffer],
  ;; or arbitrary list structure.
  (declare (type list env format-args)
	   (type
	     (or null list (simple-array (unsigned-byte 8) (#.TCP-Bufsize)))
	     data)
	   (type simple-base-string msg))

  (apply #'report-error env data msg format-args)

  (apply #'error msg format-args))

;;;-------------------------------------------------------------
;;; Prints all numbers in base 10.

(defun print-environment (env &aux thing)

  (declare (type list env))

  (format t "~%Environment:")

  (cond
    ((null env)
     (format t "  NIL"))

    ((eq env :Fail)
     (format t "  :Fail"))

    ((consp env)
     (dolist (pair env)
       (cond ((atom pair)
	      (mishap nil nil "PRINT-ENVIRONMENT [1] Bad PAIR ~S in ENV:~%~S"
		      pair env))

	     ((keywordp (setq thing (car pair)))
	      (print-env2 pair 1))

	     ((not (symbolp thing))
	      (mishap nil nil "PRINT-ENVIRONMENT [2] Bad SYMBOL ~S in ENV:~%~S"
		      thing env))

	     (t (format t "~%  Global Var: ~A" thing)
		(format t "~%       Value: ~S" (cdr pair))))))

    (t (mishap nil nil "PRINT-ENVIRONMENT [3] Bad ENV:~%~S" env)))

  (terpri))

;;;-------------------------------------------------------------

(defun print-env2 (env level)

  (declare (type list env)
	   (type fixnum level))

  (terpri)

  (do ((i 0 (the fixnum (1+ i))))
      ((= i level))
    (declare (type fixnum i))
    (format t "  "))

  (format t "Component: ~A" (car env))

  (dolist (object (cdr env))
    (let ((thing (and (consp object) (car object))))

      (cond ((and thing (keywordp thing))
	     (print-env2 object (the fixnum (1+ level))))

	    ((and thing (symbolp thing))
	     (format t "~%  ")
	     (do ((i 0 (the fixnum (1+ i))))
		 ((= i level))
	       (declare (type fixnum i))
	       (format t "  "))
	     (format t "Variable: ~A    Value: ~S" thing (cdr object)))

	    (t (mishap nil nil "PRINT-ENV2 [1] Bad object: ~S" object))))))

;;;-------------------------------------------------------------
;;; Simple and fast dumper, adequate for most purposes.
;;;
;;; PDU is stored in TCP buffer from byte HEAD [inclusive, start] to byte
;;; TAIL [exclusive, end].  This function dumps a PDU in a buffer from its
;;; beginning up to its end.

(defun dump-bytestream (msg tcp-buffer head tail
			&aux (buflen (the fixnum (- tail head))))

  (declare (type simple-base-string msg)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type fixnum head tail buflen))

  (format t "~%Dumping ~A (~D bytes), [Decimal, Hex, Chars]:" msg buflen)

  (do ((bytecount 0 (the fixnum (+ bytecount 20)))
       (ptr head (the fixnum (+ ptr 20))))
      ((>= bytecount buflen))

    (declare (type fixnum bytecount ptr))

    (when (= (the fixnum (mod bytecount 100)) 0)
      (terpri))

    (format t "~%~D~12T" ptr)

    (do ((idx ptr (the fixnum (1+ idx)))
	 (cnt 0 (the fixnum (1+ cnt))))
	((or (= cnt 20)
	     (= idx tail)))

      (declare (type fixnum idx cnt))

      (when (= cnt 10)                              ;Spacer at 10 unit point
	(format t "  "))

      (format t "~2,'0X " (aref tcp-buffer idx)))

    (format t "~77T")

    (do ((idx ptr (the fixnum (1+ idx)))
	 (cnt 0 (the fixnum (1+ cnt)))
	 (ch 0))
	((or (= cnt 20)
	     (= idx tail)))

      (declare (type fixnum idx cnt ch))

      (setq ch (aref tcp-buffer idx))
      (format t "~C" (cond ((<= 32 ch 126)
			    (code-char ch))
			   (t #\.)))))

  (terpri))

;;;-------------------------------------------------------------
;;; Debugging trace -- for dumping unimplemented SOP class
;;; data during development and/or in error situations.

(defun dump-dicom-data (dicom-alist strm)

  (format strm "~%Dicom-Alist:~%~%   (")
  (display-dicom-data dicom-alist 4 120 *group/elemname-alist* strm)
  (format strm ")~%"))

;;;-------------------------------------------------------------

(defun display-dicom-data (data indent-level max-col index-alist strm &aux
			   (indent-level+2 (the fixnum (+ indent-level 2))))

  (declare (type list data index-alist)
	   (type fixnum indent-level max-col indent-level+2))

  (do ((items data (cdr items))
       (indent-string (make-string indent-level :initial-element #\Space))
       (item) (key) (datalist) (text "") (len 0) (col indent-level) (label))
      ((null items))

    (declare (type list items item key datalist label)
	     (type cons index-alist)
	     (type simple-base-string indent-string text)
	     (type fixnum len col))

    (setq item (car items) key (car item) datalist (cdr item))
    (setq label (cdr (assoc key index-alist :test #'equal)))
    (setq text (format nil "(<~A:~A ~A ~S>"
		       (nstring-upcase (format nil "~4,'0X" (car key)))
		       (nstring-upcase (format nil "~4,'0X" (cdr key)))
		       (symbol-name (the symbol (first label)))
		       (second label)))
    (setq col (the fixnum (+ col (length text))))
    (format strm "~A" text)

    (cond ((null datalist)
	   (format strm " <empty>)"))

	  ((consp (car datalist))
	   (dolist (thing datalist)
	     (format strm "~%~A (" indent-string)
	     (display-dicom-data thing
				 indent-level+2
				 max-col
				 index-alist
				 strm)
	     (format strm ")"))
	   (format strm ")"))

	  (t (dolist (thing datalist)
	       (setq text (format nil " ~S" thing)
		     len (length text))
	       (when (> (setq col (the fixnum (+ col len))) max-col)
		 (format strm "~%~A" indent-string)
		 (setq col (the fixnum (+ indent-level len))))
	       (format strm "~A" text))
	     (format strm ")")))

    (when (cdr items)
      (format strm "~%~A" indent-string)
      (setq col indent-level))))

;;;=============================================================
;;; End.
