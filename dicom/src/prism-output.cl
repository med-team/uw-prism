;;;
;;; prism-output
;;;
;;; Functions for writing DICOM objects into Prism filesystem.
;;; Contains functions used in Server only.
;;;
;;; 01-Nov-2000 BobGian change AXIAL-image acceptance test also to print
;;;   value of DICOM slot showing value present [if image rejected].
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 07-Oct-2001 BobGian simplify READ-OBJECT [remove opt args - never used].
;;; 07-Oct-2001 BobGian PUT-IMAGE-SET -> WRITE-IMAGE-SET
;;;   [conflicted with different function of same name in Prism package].
;;; 15-Oct-2001 BobGian remove *FILE-MOVE-LIST*, temporary directory, and file
;;;   moving - all output files now written directly to final directories.
;;; 26-Dec-2001 BobGian change log message for non-Axial image.
;;; 18-Jan-2002 BobGian fix identification of dataset.
;;; 20-Jan-2002 BobGian:
;;;   1. Don't write duplicates - if dataset is identified reliably as
;;;      duplicate [via its UID] then it must be identical to original.
;;;   2. Don't append record to Image Set file until image itself has been
;;;      stored successfully.  This is important since images are no longer
;;;      written to and moved from temporary directory.  Presence of record
;;;      in Image Set file is sole indication of successful image storage.
;;;   3. Rather than caching entire Image Set file only at point of patient
;;;      identification, list of image UIDs is created then [empty for new
;;;      set, but may contain data if some images in current set were sent
;;;      on a previous association].  This list is updated as each image is
;;;      stored during current association, extending duplicate detection to
;;;      multiple identical images received during current association.
;;; 24-Jan-2002 BobGian output directory noted in log file on patient
;;;      identification - valid, since duplicates no longer written.
;;; 19-Mar-2002 BobGian replace own error message printer [which was not
;;;   always reliable] with call to standard DESCRIBE function.
;;; 19-Jun-2002 BobGian begin Structure-Set implementation.
;;; Jul/Aug 2002 BobGian implement Structure-Set C-Store SOP class:
;;;   WRITE-DICOM-OUTPUT does dispatch on C-Store-RQ SOP class (:Image or
;;;     :Structure-Set), calling appropriate output routine.
;;;   GET-PRISM-PATIENT seeks patient name and ID match only for Images.  For
;;;     Structure-Sets it creates a unique index file entry (indexed by name,
;;;     Hosp ID, and timestamp) for each dataset received.  Name printed to
;;;     index file and log is "prettified" version (as in Prism records)
;;;     rather than raw string transmitted in DICOM header.
;;;   PRISM-IMAGE-WRITER (renamed from PRISM-DATA-WRITER) does image output.
;;;     Uses tag 0020:0032 "Image Position Patient (Z)" rather than tag
;;;     0020:1041 "Slice Location (Z)" for image Z coordinate.  Should work
;;;     for both CT and MR images.  Experimental until verified.
;;;   WRITE-IMAGE-SET detects start of new image set transmitted during current
;;;     association by checking A-list of ID and UID for each image.  If ID and
;;;     UID of new image match those of an existing one, new one is declared to
;;;     be a duplicate and ignored.  If ID matches but UID is different, image
;;;     is declared to be first of a new image set.  WRITE-IMAGE-SET increments
;;;     the image-set count (one larger than largest found in image index file)
;;;     and calls itself recursively to begin writing a new image set (rather
;;;     than overwriting existing images as formerly).  The new image set
;;;     number is appended to a saved list of records to be appended to the
;;;     image index file when the association is released.
;;;   New function PRISM-STRUCTURE-WRITER decodes structure-set data and
;;;     writes data in Prism format (via PUT-OBJECT) to file.
;;;   GET-CANONICAL-NAME - new function factored and reused (formerly inlined).
;;;   PUT-OBJECT - new function almost identical to Prism version, differing by
;;;     being special-cased to data objects needed and by including a special
;;;     hook for passing filename for Image files to image-set file slot-value.
;;;   TAB-PRINT also made almost identical to version in Prism system
;;;     [needs to do arbitrary indentation for structure-sets whereas former
;;;     version only did single-level indentation for image-set file].
;;; 06-Aug-2002 BobGian *PRINT-PRETTY* -> T in PUT-OBJECT.
;;; 17-Aug-2002 BobGian tag 0020:0032 does not work for Z coord.
;;;   Reverting back to tag 0020:1041 until this can be figured out.
;;; 20-Aug-2002 BobGian:
;;;   At end of image set (when new set detected here, or at conclusion of
;;;     association in DICOM-SERVER), log number of images stored in each set
;;;     to "image.index" record and to log file.
;;;   PRISM-STRUCTURE-WRITER now logs that it wrote file (and the filename).
;;;   Interchange SERIES <-> STUDY in all tag fields used to identify image
;;;     set number.
;;; 26-Aug-2002 BobGian:
;;;   Series Instance UID (0020:000E) passed to WRITE-IMAGE-SET as definitive
;;;     and mandatory unique identifier of image set.
;;;   Image Position Patient (0020:0032) used as correct slot for image Z
;;;     coordinate - was buggy formerly (accessed X rather than Z coord).
;;;     Slice Location (0020:1041) is not a mandatory data element.
;;; 27-Aug-2002 BobGian:
;;;   READ-OBJECT error messages renumbered.
;;;   READ-OBJECT changed always to abort [num args 4 -> 3].  Default return
;;;     values would cause incorrect operation and would mask problems.
;;;   All slots previously assumed to contain strings representing SINGLE-FLOAT
;;;     values instead can contain strings representing either SINGLE-FLOAT
;;;     or INTEGER values (data type DS for "Decimal String").  Converted
;;;     following slot accessors to read as REAL (INTEGER or SINGLE-FLOAT)
;;;     and coerce to SINGLE-FLOAT as necessary:
;;;         0018:0050  Slice Thickness
;;;         0028:0030  Pixel Spacing
;;;         0020:0032  Image Position Patient
;;;         3006:0050  Contour Data
;;;   Reflected Structure-Set contour Y and Z axes to test matchup.
;;; 30-Aug-2002 BobGian:
;;;   Compute ORIGIN from X,Y,Z coordinates in 0020:0032 "Image Position
;;;     Patient" rather than from pixel spacing and image dimensions.
;;;   Determine Image-Set from correct slot: 0020:000E "Series Instance UID".
;;;     "New set during association" now works exactly as does initial set
;;;     determination - no need for kludges.  Same-ID-different-UID is now
;;;     an error situation (assuming correct image-set identification).
;;; 31-Aug-2002 BobGian:
;;;   Fix error in sign of X,Y coords for image ORIGIN slot.
;;;   Log count of images stored (may differ from ID of image).
;;; 17-Sep-2002 BobGian:
;;;   *PRINT-ARRAY* -> T in PUT-OBJECT.
;;;   DICOM-ALIST passed to MISHAP in WRITE-IMAGE-SET, PRISM-STRUCTURE-WRITER,
;;;     and READ-OBJECT for error reporting.
;;; 23-Sep-2002 BobGian add PAT-POS slot to image - obtained from Dicom
;;;   slot 0018:5100 - and calculate ORIGIN slot components using it.
;;;   It encodes patient positioning as HFS, FFS, HFP, FFP, etc.
;;; 23-Sep-2002 BobGian modify PAT-POS usage to compute axis orientation.
;;; 24-Sep-2002 BobGian:
;;;   Remove 3rd arg (DICOM-ALIST) to MISHAP and passage to it via intermediate
;;;   functions.  Same functionality now obtainable via special variable.
;;; 10-Oct-2002 BobGian fix bug in WRITE-IMAGE-SET: image set number was
;;;   not updating to include sets alread written in current association.
;;; 12-Dec-2002 BobGian temporary fix to PRISM-IMAGE-WRITER to accept
;;;   Decubitus orientations.
;;; 25-Apr-2003 BobGian:
;;;   Correct information and add additional fields to "structure.index" file
;;;     (was writing incorrect organ name).
;;;   Modify structure-set writer to write all structures, each to separate
;;;     file (was writing only first structure).
;;; 08-May-2003 BobGian:
;;;   Modify PRISM-STRUCTURE-WRITER to write dispatch on object type
;;;    (ORGAN, TUMOR, or TARGET), based on info in DICOM stream, and to
;;;    write all objects into single structure-set file.
;;;   Add storage of DISPLAY-COLOR to contours and objects in structure-sets.
;;;   PUT-OBJECT does not bind *PRINT-PRETTY*.  Outer binding used.
;;; 15-May-2003 BobGian:
;;;   Re-order items in "structure.index" file record.
;;;   Add object descriptor and type to "pat-xxx.log" file printout.
;;; 09-Jun-2003 BobGian add separator line at end of "pat-xxx.log" file.
;;; 27-Aug-2002 BobGian remove IRRAD_VOLUME and TREATED_VOLUME as handled
;;;   structure-set import types (not well-defined objects in Prism).
;;; 01-Sep-2003 BobGian write information on structure-sets to background
;;;   log file (used to go to special per-patient log file).
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 03-Mar-2004 BobGian: Change filter for 0008:0008 image type - rejects
;;;   LOCALIZER [GE "Scouts"], accepts and writes images of type ORIGINAL
;;;   PRIMARY AXIAL without complaint, and for any other type the server
;;;   logs the type [which may be experimental] and writes the image.
;;;   Added checks for missing or empty header slots [so PDS will not crash
;;;   when handling experimental data, but still preserving consistency checks
;;;   for expected data types].
;;; 27-Apr-2004 BobGian: Variable split - *STORED-IMAGE-COUNT* ->
;;;     dicom::*STORED-IMAGE-COUNT-PER-SET* [per-image-set count]
;;;     dicom::*STORED-IMAGE-COUNT-CUMULATIVE* [cumulative over association].
;;;   WRITE-IMAGE-SET increments both and resets per-set count only.
;;; 05-Nov-2004 BobGian - Convert to functional dispatch rather than hard-coded
;;;   function calls for greater modularity.  Put this file in PRISM package.
;;; 11-Mar-2005 BobGian - Changed PRISM-STRUCTURE-WRITER so that a structure
;;;   set of any non-recognized type will be treated as of type ORGAN.
;;; 15-Mar-2005 BobGian - fix global symbols in wrong package when this file
;;;   was moved DICOM -> PRISM package.
;;; 24-Aug-2006 I. Kalet change calls to single-float to calls to
;;; coerce instead for ANSI conformance.
;;;

(in-package :prism)

;;;=============================================================

(defun write-dicom-output (obj-type dicom-alist)

  (declare (type (member :Image :Structure-Set) obj-type)
	   (type list dicom-alist))

  ;; Format of each item on DICOM-ALIST:
  ;;
  ;; ( ( <GroupNum> . <ElemNum> )  <value>  <value>  ...  )
  ;;
  ;; ie, CAR is tag pair and CDR is list of values whose length
  ;; equals value multiplicity.

  ;; 0010,0010 is Patient Name; 0010,0020 is Patient ID.
  ;; If name/ID missing, use special value and declare this a NON-MATCH.
  (let ((dicom-pat-name
	  (or (second (assoc '(#x0010 . #x0010) dicom-alist :test #'equal))
	      "*** No Name ***"))
	(dicom-pat-id
	  (or (second (assoc '(#x0010 . #x0020) dicom-alist :test #'equal))
	      "*** No ID ***")))
    (declare (type simple-base-string dicom-pat-name dicom-pat-id))

    ;; Determine which Patient ID to use.  For images, if we find the unique
    ;; match, use real Prism Patient ID and write files to the Image Database.
    ;; If ambiguity arises, construct a new Patient ID and write files to the
    ;; Unmatched-Pat-Image database.  For Structure-Sets, we use only a single
    ;; directory and do not attempt patient identification.
    (cond
      ((eq obj-type :Image)
       (multiple-value-bind (prism-pat-name prism-pat-id image-output-db)
	   ;; For images, GET-PRISM-PATIENT looks in normal and
	   ;; Unmatched-Pat-Image databases because patient might be
	   ;; identified either in Patient Database from a correct
	   ;; identification or in Unmatched-Pat-Image database from storage
	   ;; of a previously ambiguous identification.  It also sets
	   ;; dicom::*CACHED-IMAGE-DB* [and returns it as IMAGE-OUTPUT-DB]
	   ;; as destination of data files.
	   (get-prism-patient dicom-pat-name dicom-pat-id)
	 (declare (type simple-base-string prism-pat-name image-output-db)
		  (type fixnum prism-pat-id))
	 (prism-image-writer prism-pat-name prism-pat-id
			     dicom-alist image-output-db)))
      ((eq obj-type :Structure-Set)
       (prism-structure-writer (get-canonical-name dicom-pat-name)
			       dicom-pat-id dicom-alist
			       dicom::*structure-DB*)))))

;;;-------------------------------------------------------------
;;; When identifying an entry, set variables caching those values so
;;; same can be used on next data transfer for the same patient.

(defun get-prism-patient (dicom-pat-name dicom-pat-id &aux
			  (prism-pat-id 0) (pat-db dicom::*patient-DB*)
			  (pat-idx-filename "patient.index")
			  (prism-pat-name "") (canonical-name ""))

  (declare (type simple-base-string dicom-pat-name dicom-pat-id
		 canonical-name prism-pat-name pat-db pat-idx-filename)
	   (type fixnum prism-pat-id))

  ;; If name/ID match those cached from the previous association, use them.
  ;; Case-sensitive string comparison OK since Server set the variable.
  ;; This branch should be taken on all patient identification attempts
  ;; after the first.
  (when (and (string= dicom-pat-name (or dicom::*cached-dicom-pat-name* ""))
	     (string= dicom-pat-id (or dicom::*cached-dicom-pat-ID* "")))
    (setq prism-pat-id dicom::*cached-prism-pat-ID*)
    (setq prism-pat-name dicom::*cached-prism-pat-name*)
    (when (>= (the fixnum dicom::*log-level*) 1)
      (format t
	      #.(concatenate
		  'string
		  "~%GET-PRISM-PATIENT [1] Found cached entry:"
		  "~%  Patient Name: ~S, Dicom ID: ~S, Prism ID: ~D~%")
	      prism-pat-name dicom-pat-id prism-pat-id))
    (return-from get-prism-patient
      (values prism-pat-name prism-pat-id dicom::*cached-image-DB*)))

  ;; Convert DICOM patient name to canonical form against which to match
  ;; PRISM patient name.  If match found, cache original DICOM name.
  (setq canonical-name (get-canonical-name dicom-pat-name))

  ;; Next, search main Patient Database.  GET-INDEX-LIST returns patient list
  ;; in reverse order -- which is nice, since hits are more likely to occur at
  ;; the end of the patient index file if they occur at all.
  (dolist (pat-info (get-index-list
		      (concatenate 'string pat-db pat-idx-filename)))
    ;; Case-insensitive string comparison used for patient's name.
    ;; If Prism record matches CANONICAL-NAME from DICOM, use Prism's record
    ;; as PRISM-PAT-NAME [for logging] and use CANONICAL-NAME as cached value
    ;; for future comparisons.
    (when (and (match-name (setq prism-pat-name (second pat-info))
			   canonical-name)
	       (match-id (third pat-info) dicom-pat-id))
      (setq prism-pat-id (first pat-info))
      (format t
	      #.(concatenate
		  'string
		  "~%Patient found in \"~A~A\":"
		  "~%  Patient Name: ~S, Dicom ID: ~S, Prism ID: ~D~%")
	      pat-db pat-idx-filename prism-pat-name dicom-pat-id prism-pat-id)
      (setq dicom::*cached-dicom-pat-name* dicom-pat-name)
      (setq dicom::*cached-dicom-pat-ID* dicom-pat-id)
      (setq dicom::*cached-dicom-set-ID* "")
      (setq dicom::*cached-prism-set-ID* 0)
      (return-from get-prism-patient
	(values (setq dicom::*cached-prism-pat-name* prism-pat-name)
		(setq dicom::*cached-prism-pat-ID* prism-pat-id)
		(setq dicom::*cached-image-DB*
		      dicom::*matched-pat-image-DB*)))))

  ;; If not there, search the Unmatched-Pat-Image database.
  (let* ((unmatched-pat-idx-filename
	   (concatenate 'string
			dicom::*unmatched-pat-image-DB* pat-idx-filename))
	 (unmatched-pat-idx-list (get-index-list unmatched-pat-idx-filename)))
    (declare (type simple-base-string unmatched-pat-idx-filename)
	     (type list unmatched-pat-idx-list))
    (do ((new-id 0) (old-id 0)
	 (pts unmatched-pat-idx-list (cdr pts))
	 (pat-info))
	((null pts)
	 ;; If NOT found in Unmatched-Pat-Image database, generate a new entry
	 ;; with next available ID number, add entry to Unmatched-Pat-Image
	 ;; database, and cache and return these values.
	 (setq prism-pat-id (the fixnum (1+ new-id)))
	 (format t
		 #.(concatenate
		     'string
		     "~%Creating entry in Unmatched-Pat-Image DB \"~A~A\":"
		     "~%  Patient Name: ~S, Dicom ID: ~S, Prism ID: ~D~%")
		 dicom::*unmatched-pat-image-DB* pat-idx-filename
		 canonical-name dicom-pat-id prism-pat-id)
	 (push (list prism-pat-id canonical-name dicom-pat-id
		     (dicom::date/time) dicom::*remote-IP-string*)
	       unmatched-pat-idx-list)
	 (let ((*print-pretty* nil))
	   (with-open-file (strm unmatched-pat-idx-filename :direction :Output
				 :element-type 'base-char
				 :if-does-not-exist :Create
				 :if-exists :Supersede)
	     (dolist (item (nreverse unmatched-pat-idx-list))
	       (format strm "~S~%" item))))
	 (setq dicom::*cached-dicom-pat-name* dicom-pat-name)
	 (setq dicom::*cached-dicom-pat-ID* dicom-pat-id)
	 (setq dicom::*cached-dicom-set-ID* "")
	 (setq dicom::*cached-prism-set-ID* 0)
	 (values (setq dicom::*cached-prism-pat-name* canonical-name)
		 (setq dicom::*cached-prism-pat-ID* prism-pat-id)
		 (setq dicom::*cached-image-DB*
		       dicom::*unmatched-pat-image-DB*)))
      (declare (type list pts pat-info)
	       (type fixnum new-id old-id))
      (setq pat-info (car pts)
	    old-id (first pat-info))
      (when (< new-id old-id)                  ;Accumulate max ID seen so far.
	(setq new-id old-id))
      ;; If found in Unmatched-Pat-Image database, cache and return values.
      ;; Case-sensitive string comparisons OK since Server wrote the file.
      (when (and (string= (second pat-info) canonical-name)
		 (string= (third pat-info) dicom-pat-id))
	(setq prism-pat-id old-id)
	(format t
		#.(concatenate
		    'string
		    "~%Patient found in Unmatched-Pat-Image DB \"~A~A\":"
		    "~%  Patient Name: ~S, Dicom ID: ~S, Prism ID: ~D~%")
		dicom::*unmatched-pat-image-DB* pat-idx-filename
		canonical-name dicom-pat-id prism-pat-id)
	(setq dicom::*cached-dicom-pat-name* dicom-pat-name)
	(setq dicom::*cached-dicom-pat-ID* dicom-pat-id)
	(setq dicom::*cached-dicom-set-ID* "")
	(setq dicom::*cached-prism-set-ID* 0)
	(return (values (setq dicom::*cached-prism-pat-name* canonical-name)
			(setq dicom::*cached-prism-pat-ID* prism-pat-id)
			(setq dicom::*cached-image-DB*
			      dicom::*unmatched-pat-image-DB*)))))))

;;;=============================================================
;;; Empty strings are returned as NIL from the association list DICOM-ALIST
;;; rather than as null strings.  That way, ASSOC returns NIL rather than
;;; a null string for empty string slot values, enabling the search to
;;; continue via the OR.
;;;
;;; Server type-checks objects returned by READ-FROM-STRING [in READ-OBJECT]
;;; so that if a READ error occurs or the incorrect type is returned the
;;; server can recover gracefully by calling dicom::MISHAP.

(defun prism-image-writer (prism-pat-name prism-pat-id dicom-alist output-db
			   &aux (img-x-dim 0) (img-y-dim 0)
			   (not-supplied "Not Supplied"))

  (declare (type simple-base-string prism-pat-name output-db not-supplied)
	   (type list dicom-alist)
	   (type fixnum prism-pat-id img-x-dim img-y-dim))

  ;; Value multiplicity = 3 - compare against list.  String "AXIAL" identifies
  ;; what we want.  Others in this list are usually "ORIGINAL" and "PRIMARY",
  ;; although if the image is processed by the scanner the string "SECONDARY"
  ;; can replace "PRIMARY".
  ;; GE "Scouts" are tagged "LOCALIZER".  If present, log reception but ignore
  ;; image.  If any other combination of tags appears, log reception and
  ;; continue [may be part of an experimental data type].
  (let ((im-type (cdr (assoc '(#x0008 . #x0008) dicom-alist :test #'equal))))
    (declare (type list im-type))
    (when (member "LOCALIZER" im-type :test #'string=)
      (format t "~&  Ignoring LOCALIZER image: ~S~%" im-type)
      (return-from prism-image-writer nil))
    (unless (and (member "ORIGINAL" im-type :test #'string=)
		 (member "PRIMARY" im-type :test #'string=)
		 (member "AXIAL" im-type :test #'string=))
      (format t "~&  Non-standard image type: ~S~%" im-type)))

  (setq img-y-dim
	(second (assoc '(#x0028 . #x0010) dicom-alist :test #'equal))   ;Rows
	img-x-dim
	(second (assoc '(#x0028 . #x0011) dicom-alist :test #'equal))) ;Columns

  (let ((im (make-instance 'image-2D)))

    (setf (id im)                                   ;Image Number
	  (read-object
	    (second (assoc '(#x0020 . #x0013) dicom-alist :test #'equal))
	    'fixnum "Image Number"))

    (setf (uid im)                                  ;SOP Instance UID
	  (or (second (assoc '(#x0008 . #x0018) dicom-alist :test #'equal))
	      not-supplied))

    (do ((strings   ;Listed in reverse order here -- reversed by PUSH in loop.
	   (list (second (assoc '(#x0018 . #x1030)  ;Protocol Name
				dicom-alist :test #'equal))
		 (second (assoc '(#x0038 . #x0040)  ;Discharge Diagnosis
				dicom-alist :test #'equal))
		 (second (assoc '(#x0032 . #x1060)  ;Requested Procedure
				dicom-alist :test #'equal))
		 (second (assoc '(#x0018 . #x0039)  ;Therapy Description
				dicom-alist :test #'equal))
		 (second (assoc '(#x0010 . #x21B0) ;Additional Patient History
				dicom-alist :test #'equal))
		 (second (assoc '(#x0008 . #x1080) ;Admitting Diagnosis Descrip
				dicom-alist :test #'equal))
		 (second (assoc '(#x0008 . #x1030)  ;Study Description
				dicom-alist :test #'equal))
		 (second (assoc '(#x0008 . #x103E)  ;Series Description
				dicom-alist :test #'equal))
		 (second (assoc '(#x0020 . #x4000)  ;Image Comments
				dicom-alist :test #'equal)))
	   (cdr strings))
	 (accumulator '()))
	((null strings)
	 (setf (description im)
	       (cond ((consp accumulator)
		      (apply #'concatenate 'string accumulator))
		     (t not-supplied))))
      (let ((item (car strings)))
	(when (and (typep item 'simple-base-string) ;If something to add,
		   ;; and it doesn't duplicate a string already there,
		   (not (member item accumulator :test #'string=)))
	  (when (consp accumulator)    ;but accumulator already has something,
	    (push " " accumulator))         ;first separate them with a space,
	  (push item accumulator))))                ;then add new string.

    (setf (acq-date im)
	  (pretty-date
	    (or (second (assoc '(#x0008 . #x0021)   ;Series Date
			       dicom-alist :test #'equal))
		(second (assoc '(#x0008 . #x0020)   ;Study Date
			       dicom-alist :test #'equal))
		(second (assoc '(#x0008 . #x0022)   ;Acquisition Date
			       dicom-alist :test #'equal))
		(second (assoc '(#x0008 . #x0023)   ;Image Date
			       dicom-alist :test #'equal))
		"00000100")))

    (setf (acq-time im)
	  (pretty-time
	    (or (second (assoc '(#x0008 . #x0031)   ;Series Time
			       dicom-alist :test #'equal))
		(second (assoc '(#x0008 . #x0030)   ;Study Time
			       dicom-alist :test #'equal))
		(second (assoc '(#x0008 . #x0032)   ;Acquisition Time
			       dicom-alist :test #'equal))
		(second (assoc '(#x0008 . #x0033)   ;Image Time
			       dicom-alist :test #'equal))
		"000000.0")))

    (setf (scanner-type im)
	  (or (second (assoc '(#x0008 . #x1090)     ;Manufacturer Model Name
			     dicom-alist :test #'equal))
	      (second (assoc '(#x0008 . #x0070)     ;Manufacturer
			     dicom-alist :test #'equal))
	      not-supplied))

    (setf (hosp-name im)
	  (or (second (assoc '(#x0008 . #x0080)     ;Institution Name
			     dicom-alist :test #'equal))
	      not-supplied))

    (setf (img-type im)
	  (or (second (assoc '(#x0008 . #x0060)     ;Modality
			     dicom-alist :test #'equal))
	      not-supplied))

    ;; Pixel Spacing: " 3.62323e-01\3.62323e-01" Order: Rows(Y),Cols(X)
    ;; Value in slot: ( "...Y..." "...X..." ) as two-element list of strings.
    ;; Value multiplicity = 2: accessing CDR as list of all values.
    (let ((spacing (cdr (assoc '(#x0028 . #x0030) dicom-alist :test #'equal))))
      (declare (type list spacing))
      (when (consp spacing)
	(let ((x-sz (* (coerce img-x-dim 'single-float)
		       (coerce (read-object (second spacing) 'real
					    "Pixel X Spacing")
			       'single-float)
		       0.1))                       ;millimeters -> centimeters
	      (y-sz (* (coerce img-y-dim 'single-float)
		       (coerce (read-object (first spacing) 'real
					    "Pixel Y Spacing")
			       'single-float)
		       0.1)))                      ;millimeters -> centimeters
	  (declare (type single-float x-sz y-sz))
	  (setf (size im) (list x-sz y-sz))
	  (setf (pix-per-cm im) (/ (coerce img-x-dim 'single-float) x-sz)))))

    (let ((image-position                  ;Image Position (Patient) (X, Y, Z)
	    (cdr (assoc '(#x0020 . #x0032) dicom-alist :test #'equal)))
	  (pat-position    ;Patient Position ("HFS", "FFS", "HFP", "FFP", etc)
	    (second (assoc '(#x0018 . #x5100) dicom-alist :test #'equal)))
	  (x-multiplier 0.1) (y-multiplier -0.1) (z-multiplier -0.1))

      (declare (type single-float x-multiplier y-multiplier z-multiplier))

      ;; These multipliers convert millimeters -> centimeters and also
      ;; set axis orientations according to patient position as scanned.
      ;;
      ;; HFS (usual): X+, Y-, Z- rel to Prism coords (no reversals).
      ;; FFS: X+, Y-, Z- rel to Prism coords (no reversals).
      ;; HFP: X-, Y+, Z- rel to Prism coords (reverse X,Y from default).
      ;; FFP: X-, Y+, Z- rel to Prism coords (reverse X,Y from default).
      ;;
      ;; Prism seems to be "machine-centered" regarding Prone/Supine but
      ;; "patient-centered" regarding Feet-First/Head-First.  Supine image
      ;; looks up (increasing Y); Prone image looks down (decreasing Y).  But
      ;; FF vs HF images look the same: Z increasing from head toward toe, and
      ;; X,Y oriented as seen in machine coords looking from toes toward head.
      ;;
      ;; Thus the Dicom -> Prism transformations TAKE INTO ACCOUNT Prone vs
      ;; Supine orientation indication to convert Dicom's patient-centered
      ;; frame to Prism's machine-centered frame but LEAVE IN PLACE Dicom's
      ;; patient-centered axis convention vis-a-vis the HF vs FF orientation.

      (cond
	((or (consp image-position)
	     (typep pat-position 'simple-base-string))
	 (unless (and (typep pat-position 'simple-base-string)
		      (>= (length (the simple-base-string pat-position)) 3)
		      #+ignore
		      (member pat-position '("HFS" "FFS" "HFP" "FFP")
			      :test #'string=))
	   ;; Temporary fix to accept Decubitus orientations.
	   ;; Dicom slot may also contain "HFDR", "FFDR", "HFDL", or "FFDL",
	   ;; but PDS/Prism cannot use these orientations.
	   (dicom::mishap nil nil
			  "PRISM-IMAGE-WRITER [1] Bad PAT-POS slot: ~S"
			  pat-position))
	 (when (char= (aref (the simple-base-string pat-position) 2) #\P)
	   ;; PRONE rather than SUPINE orientation - reverse X and Y axes.
	   (setq x-multiplier (- x-multiplier) y-multiplier (- y-multiplier)))
	 #+ignore
	 ;; Ignore this transformation - preserve Dicom's patient-centeredness
	 ;; vis-a-vis HF vs FF as per comment above.
	 (when (char= (aref (the simple-base-string pat-position) 0) #\F)
	   ;; FEET-FIRST rather than HEAD-FIRST - reverse X and Z axes.
	   (setq x-multiplier (- x-multiplier) z-multiplier (- z-multiplier)))
	 (setf (pat-pos im) pat-position)
	 (setf (origin im)
	       (vector
		 (* (coerce (read-object (first image-position) 'real
					 "Image X coord")
			    'single-float)
		    x-multiplier)
		 (* (coerce (read-object (second image-position) 'real
					 "Image Y coord")
			    'single-float)
		    y-multiplier)
		 (* (coerce (read-object (third image-position) 'real
					 "Image Z coord")
			    'single-float)
		    z-multiplier))))
	(t (setf (pat-pos im) not-supplied)
	   (setf (origin im) (vector 0.0 0.0 0.0)))))

    (setf (range im) 4095)

    (setf (units im) "H + 1024")

    ;; (#x0018 . #x0088) is Spacing Between Slices (mm)
    (let ((spacing (second (assoc '(#x0018 . #x0050)    ;Slice Thickness (Z)
				  dicom-alist :test #'equal))))
      (when (typep spacing 'simple-base-string)
	(setf (thickness im)
	  (* (coerce (read-object spacing 'real "Slice Thickness")
		     'single-float)
	     0.1))))			;millimeters -> centimeters

    (setf (x-orient im) #(1.000 0.000 0.000))

    (setf (y-orient im) #(0.000 -1.000 0.000))

    (setf (pixels im)                               ;Pixel Data
	  (second (assoc '(#x7FE0 . #x0010) dicom-alist :test #'equal)))

    (write-image-set
      prism-pat-name prism-pat-id im
      img-x-dim img-y-dim output-db                 ;Series Instance UID
      (second (assoc '(#x0020 . #x000E) dicom-alist :test #'equal)))))

;;;-------------------------------------------------------------

(defun write-image-set (prism-pat-name prism-pat-id im img-x-dim img-y-dim
			output-db dicom-set-id &aux (prism-set-id 0))

  "WRITE-IMAGE-SET prism-pat-name prism-pat-id im img-x-dim img-y-dim
		   output-db dicom-set-id

appends image IM to image-set for patient whose ID is PRISM-PAT-ID, or
creates a new image-set.  New images and all image-sets go to OUPTUT-DB."

  (declare (type simple-base-string prism-pat-name output-db dicom-set-id)
	   (type fixnum prism-pat-id img-x-dim img-y-dim prism-set-id))

  (cond
    ;; If continuing with an already-identified Image Set, use cached values,
    ;; and conditionally append to already-started Image Set file.
    ((string= dicom-set-id dicom::*cached-dicom-set-ID*)
     (setq prism-set-id dicom::*cached-prism-set-ID*))

    ;; Otherwise identify the Image Set and cache the identification.
    ;; In addition to file itself, must also scan any records pending
    ;; to be written to it at close of current asssociation.
    (t (let ((im-set-record dicom::*current-im-set-record*))
	 (declare (type list im-set-record))
	 (when (consp dicom::*image-ID/UID-alist*)
	   (format t "~&Stored ~D images in this set.~%"
		   dicom::*stored-image-count-per-set*)
	   (setq dicom::*image-ID/UID-alist* nil)
	   ;; If current "image.index" file record [for Image-Set that just
	   ;; finished, since current image is first of next set] is a new one,
	   ;; update it with number of images in that set.  This can happen
	   ;; only if dicom::*NEW-IM-INDEX-RECORDS* is non-NIL.  If that record
	   ;; was already in the "image.index" file, do NOT update it, as this
	   ;; would screw up the comment string in the file.
	   (when (consp im-set-record)
	     (setf (fourth im-set-record)
		   (format nil "Set ~D (~D images): ~A"
			   (third im-set-record)
			   dicom::*stored-image-count-per-set*
			   (fourth im-set-record))))))

       ;; Initialize count of images stored in current set.
       (setq dicom::*stored-image-count-per-set* 0)

       (let ((image-set-idx-filename
	       (concatenate 'string output-db "image.index")))
	 (declare (type simple-base-string image-set-idx-filename))

	 (with-open-file (strm image-set-idx-filename
			       :direction :input
			       :element-type 'base-char
			       :if-does-not-exist nil)
	   ;; If no file, OPEN returns NIL, IMAGE-SET-RECORD gets NIL,
	   ;; and iteration ends immediately.
	   (do ((image-set-record (and strm (read strm nil nil))
				  (and strm (read strm nil nil))))
	       ((null image-set-record)

		;; If new sets have been written, new Image Index records
		;; await appending to index file.  Set PRISM-SET-ID to highest
		;; value so far [used for last set written] before incrementing
		;; to get upcoming set's index number.
		(let ((im-index-records dicom::*new-im-index-records*))
		  (declare (type list im-index-records))
		  (when (consp im-index-records)
		    (setq prism-set-id (third (car im-index-records)))))

		;; Creating new Image Set.  Assign next available index number.
		(setq prism-set-id (the fixnum (1+ prism-set-id)))
		(format t
			#.(concatenate
			    'string
			    "~%Creating Image Set in ~S:~%  Patient Name: ~S,"
			    " Patient ID: ~D, Image-Set ID: ~D~%")
			dicom::*cached-image-DB*
			prism-pat-name prism-pat-id prism-set-id)
		;; Record identifying new image set is cached for writing to
		;; "image.index" file at conclusion of successful association.
		;; Record itself is the CDR of this list.  CAR of it is the
		;; filename to which to write the record.
		;; Image-Set-ID is included in Image Index file because
		;; sometimes description strings for different sets are
		;; identical, making it hard for user to tell them apart.
		(push (setq dicom::*current-im-set-record*
			    (list image-set-idx-filename
				  prism-pat-id prism-set-id
				  (description im)
				  dicom-set-id dicom::*remote-IP-string*))
		      dicom::*new-im-index-records*))

	     ;; DICOM-SET-ID is a string and so is corresponding field of the
	     ;; index file if Server wrote the file.  However, patients scanned
	     ;; before Server was used have this field missing in index file
	     ;; records, yielding NIL as the (FOURTH IMAGE-SET-RECORD) result.
	     ;; Thus the need for (OR (FOURTH IMAGE-SET-RECORD) "") to
	     ;; "stringify" the NIL.  This will work on new cases and will
	     ;; cause first transmissions for old cases to be considered fresh
	     ;; Image Sets [most likely what is intended].  Later, as Server
	     ;; appends a record with all five fields, Image Set entry will
	     ;; match on both the Prism-Pat-ID and the Dicom-Set-ID fields.
	     ;; Note that comparisons on fourth field of IMAGE-SET-RECORD
	     ;; [the DICOM Image Set UID] are done only if the first field
	     ;; [the Prism Patient ID] matches PRISM-PAT-ID, so that Image Set
	     ;; counts FOR THIS PATIENT ONLY get accumulated.
	     (cond
	       ((/= prism-pat-id (the fixnum (first image-set-record))))
	       ((string= (or (fourth image-set-record) "") dicom-set-id)
		;; Identified existing Image Set -- may append or overwrite.
		(setq prism-set-id (second image-set-record))
		(format t
			#.(concatenate
			    'string
			    "~%Appending Image Set in ~S:~%"
			    "  Patient Name: ~S, Patient ID: ~D,"
			    " Image-Set ID: ~D~%")
			dicom::*cached-image-DB*
			prism-pat-name prism-pat-id prism-set-id)
		;; Appending to an existing Image-Set.  Do NOT muck with
		;; description string already written to "image.index" file.
		(setq dicom::*current-im-set-record* nil)
		(return))
	       ;; Accumulate maximum set index so far.
	       (t (setq prism-set-id
			(max prism-set-id
			     (the fixnum (second image-set-record)))))))))

       (setq dicom::*cached-dicom-set-ID* dicom-set-id
	     dicom::*cached-prism-set-ID* prism-set-id)))

  (setf (patient-id im) prism-pat-id)
  (setf (image-set-id im) prism-set-id)

  (let ((pixarray-filename
	  (format nil "pat-~D.image-~D-~D"
		  prism-pat-id prism-set-id (id im)))
	(ID/UID-alist dicom::*image-ID/UID-alist*)
	(image-id (id im)) (image-uid (uid im)) pair
	(im-set-filename
	  (format nil "~Apat-~D.image-set-~D"
		  output-db prism-pat-id prism-set-id)))

    (declare (type simple-base-string pixarray-filename im-set-filename
		   image-uid)
	     (type list ID/UID-alist)
	     (type fixnum image-id))

    (unless (consp ID/UID-alist)
      ;; If no images yet recorded on this association, read and cache Alist
      ;; of any ID/UIDs for images already stored in current Image Set from
      ;; previous association(s).  If no such images stored, Image Set file
      ;; will not exist - cached Alist will be NIL.
      ;; When appending to existing image set, count of images stored will
      ;; include those already written when new appending begins.
      (when (probe-file im-set-filename)
	(with-open-file (strm im-set-filename
			      :direction :Input
			      :element-type 'base-char)
	  (do ((item (read strm nil :EOF) (read strm nil :EOF))
	       (prior-id nil) (prior-uid nil))
	      ((eq item :EOF))
	    (when (eq item 'id)
	      (setq prior-id (read strm nil :EOF)))
	    (when (eq item 'uid)
	      (setq prior-uid (read strm nil :EOF)))
	    ;; When end of record for a given image is reached,
	    ;; store results and reset vars used to accumulate results.
	    (when (and (eq item :End)
		       (typep prior-id 'fixnum)
		       (typep prior-uid 'simple-base-string))
	      (push (cons prior-id prior-uid) ID/UID-alist)
	      (setq prior-id nil prior-uid nil))))
	(setq dicom::*image-ID/UID-alist* ID/UID-alist)))

    ;; Test whether image is a duplicate [image ID/UID pair already on Alist]
    ;; or the beginning of a new Image Set [ID/UID pair on Alist with same ID
    ;; but different UID].  If continuation of current Set, output it to same
    ;; Set.  If duplicate, print message and ignore it.  If new Set, update
    ;; image object and output it to new Set by resetting cached variables and
    ;; calling this function recursively.
    (cond
      ;; Image is new to current Image Set - write to output database.
      ((null (setq pair (assoc image-id ID/UID-alist :test #'equal)))
       ;; Note that image counts are incremented if an image file is actually
       ;; written [ie, not a duplicate], and dicom::*IMAGE-ID/UID-ALIST*
       ;; is extended.  But dicom::*IMAGE-ID/UID-ALIST* also contains records
       ;; for images written previously [ie, current duplicates], gotten from
       ;; the image-set file.
       (format t "~&  Received image ~D: ~S, ~A.~%"
	       (incf (the fixnum dicom::*stored-image-count-per-set*))
	       pixarray-filename (pat-pos im))
       (incf (the fixnum dicom::*stored-image-count-cumulative*))
       (with-open-file (strm (concatenate 'string output-db pixarray-filename)
			     :direction :Output
			     :element-type '(unsigned-byte 8)
			     :if-does-not-exist :Create
			     :if-exists :Supersede)
	 (write-sequence (pixels im) strm))
       ;; Append ID/UID of image just stored to Alist.
       (setq dicom::*image-ID/UID-alist* (cons (cons image-id image-uid)
					       ID/UID-alist))
       ;; Append record to Image Set file.  This operation is done last so
       ;; record will NOT be appended to Image Set file until image file
       ;; itself has been stored successfully.
       (with-open-file (strm im-set-filename :direction :Output
			     :element-type 'base-char
			     :if-does-not-exist :Create :if-exists :Append)
	 ;; *PRINT-ARRAY* must be T because ORIGIN slot value is an array,
	 ;; and we must print its slot values to the Image Set file.
	 (let ((*print-array* t))
	   (put-object im strm 0 pixarray-filename img-x-dim img-y-dim))))

      ;; Duplicate - log that fact, but don't write image.
      ((string= image-uid (cdr pair))
       (format t "~&  Received image: ~S, duplicate.~%" pixarray-filename))

      ;; Same ID but different image UID on same Image-Set: error.
      (t (dicom::mishap nil nil
			"WRITE-IMAGE-SET [1] Bad image set UID: ~S ~S ~S"
			prism-pat-name prism-pat-id dicom-set-id)))))

;;;=============================================================

(defun prism-structure-writer (canonical-name dicom-pat-id dicom-alist
			       output-db &aux (prism-pat-id 0)
			       (structure-idx-filename
				 (concatenate 'string
					      output-db "structure.index"))
			       (structure-list '()) (skipped-item-list '()))

  (declare (type simple-base-string canonical-name dicom-pat-id output-db
		 structure-idx-filename)
	   (type list dicom-alist structure-list skipped-item-list)
	   (type fixnum prism-pat-id))

  ;; This is a debugging hook to detect slot 3006:0045 "Contour Offset Vector",
  ;; which indicates an offset of structure-set contour from the corresponding
  ;; image.  If missing, offset is specified to be zero.  We have yet to see a
  ;; value in this slot, but it might exist for other clients.
  (let ((offset (assoc '(#x3006 . #x0045) dicom-alist :test #'equal)))
    (when (consp offset)
      (format t "~&Offset vector: ~S~%" offset)))

  (when (probe-file structure-idx-filename)
    (with-open-file (strm structure-idx-filename
			  :direction :Input :element-type 'base-char)
      (do ((item (read strm nil :EOF) (read strm nil :EOF)))
	  ((eq item :EOF))
	(let ((idx (first item)))
	  (declare (type fixnum idx))
	  (when (> idx prism-pat-id)
	    (setq prism-pat-id idx))))))

  (do ((ss-roi-sequence                            ;Structure Set ROI Sequence
	 (cdr (assoc '(#x3006 . #x0020) dicom-alist :test #'equal))
	 (cdr ss-roi-sequence))
       (roi-contour-sequence                        ;ROI Contour Sequence
	 (cdr (assoc '(#x3006 . #x0039) dicom-alist :test #'equal))
	 (cdr roi-contour-sequence))
       (observation-sequence                     ;RT ROI Observations Sequence
	 (cdr (assoc '(#x3006 . #x0080) dicom-alist :test #'equal))
	 (cdr observation-sequence))
       (obj-descriptor "") (obj-name "") (obj-type) (obj-itself))
      ((null roi-contour-sequence))

    (declare (type simple-base-string obj-descriptor obj-name)
	     (type list ss-roi-sequence roi-contour-sequence
		   observation-sequence))

    (setq obj-name
	  (or (second (assoc '(#x3006 . #x0026) (car ss-roi-sequence)
			     :test #'equal))        ;ROI Name
	      (second (assoc '(#x3006 . #x0085) (car observation-sequence)
			     :test #'equal))        ;ROI Observation Label
	      "No object name"))

    (setq obj-descriptor                            ;RT ROI Interpreted Type
	  (second (assoc '(#x3006 . #x00A4)
			 (car observation-sequence)
			 :test #'equal)))

    (setq obj-type
	  (or (cdr (assoc obj-descriptor
			  '(("ORGAN" . ORGAN)       ;All Prism-defined types.
			    ("EXTERNAL" . ORGAN)
			    ("AVOIDANCE" . ORGAN)
			    ("GTV" . TUMOR)
			    ("PTV" . TARGET)
			    ("CTV" . TARGET))
			  :test #'STRING=))
	      ;; Default for missing or incorrectly-specified data.
	      'ORGAN))

    (setq obj-itself (make-instance obj-type))

    (setf (name obj-itself) obj-name)

    (let ((contour-list '())                        ;ROI Display Color
	  (roi-color (get-color (cdr (assoc '(#x3006 . #x002A)
					    (car roi-contour-sequence)
					    :test #'equal)))))

      (declare (type list contour-list)
	       (type symbol roi-color))

      ;;Set DISPLAY-COLOR of ORGAN, TUMOR, or TARGET
      (setf (display-color obj-itself) roi-color)

      (dolist (contour-alist                        ;Contour Sequence
		(cdr (assoc '(#x3006 . #x0040) (car roi-contour-sequence)
			    :test #'equal)))
	(declare (type list contour-alist))
	(let ((contour-obj (make-instance 'contour))
	      (contour-data (cdr (assoc '(#x3006 . #x0050) contour-alist
					:test #'equal))))   ;Contour Data
	  (declare (type list contour-data))
	  ;; CONTOUR-DATA: ( X1 Y1 Z1 X2 Y2 Z2 ... Xn Yn Zn )
	  ;; where all Zi should be same value.
	  (unless (= (length contour-data)
		     (the fixnum
		       (* (the fixnum
			    (read-object            ;Number of Contour Points
			      (second (assoc '(#x3006 . #x0046)
					     contour-alist :test #'equal))
			      'fixnum "Number of Contour Points"))
			  3)))
	    (dicom::mishap nil contour-alist
			   "PRISM-STRUCTURE-WRITER [1] Bad vertices list."))

	  ;; Multiplication by 0.1 is for MM -> CM conversion.
	  ;; Have to do equivalent axis orientation business as we do for
	  ;; images using PAT-POS slot.  Have to look up slot value for
	  ;; image associated with current structure-set.
	  (setf (z contour-obj)                  ;Z coordinate of first vertex
	    (* (coerce (read-object (third contour-data) 'real "Z")
		       'single-float)
	       -0.1))
	  (do ((itemlist contour-data (cdddr itemlist))
	       (vert-list '()))
	      ((null itemlist)
	       (setf (vertices contour-obj) (nreverse vert-list)))
	    (push (list (* (coerce (read-object (first itemlist) 'real "X")
				   'single-float)
			   0.1)
			(* (coerce (read-object (second itemlist) 'real "Y")
				   'single-float)
			   -0.1))
		  vert-list))

	  ;; Set DISPLAY-COLOR of CONTOUR.
	  (setf (display-color contour-obj) roi-color)

	  (push contour-obj contour-list)))

      (cond ((consp contour-list)
	     (setf (contours obj-itself) (nreverse contour-list))
	     (push (list obj-itself obj-descriptor obj-type)
		   structure-list))
	    (t (push (list obj-name obj-descriptor) skipped-item-list)))))

  ;; Increment PRISM-PAT-ID since we always write a new Structure-Set.
  (incf (the fixnum prism-pat-id))
  (setq structure-list (nreverse structure-list))

  (let ((plan-name
	  (or
	    ;; Structure Set Label
	    (second (assoc '(#x3006 . #x0004) dicom-alist :test #'equal))
	    ;; Structure Set Name
	    (second (assoc '(#x3006 . #x0002) dicom-alist :test #'equal))
	    "No plan name"))
	(plan-date
	  (pretty-date
	    (or
	      ;; Structure Set Date
	      (second (assoc '(#x3006 . #x0008) dicom-alist :test #'equal))
	      ;; Instance Creation Date
	      (second (assoc '(#x0008 . #x0012) dicom-alist :test #'equal))
	      ;; Series Date
	      (second (assoc '(#x0008 . #x0021) dicom-alist :test #'equal))
	      ;; Study Date
	      (second (assoc '(#x0008 . #x0020) dicom-alist :test #'equal))
	      ;; Acquisition Date
	      (second (assoc '(#x0008 . #x0022) dicom-alist :test #'equal))
	      "00000100")))
	(plan-time
	  (pretty-time
	    (or
	      ;; Structure Set Time
	      (second (assoc '(#x3006 . #x0009) dicom-alist :test #'equal))
	      ;; Instance Creation Time
	      (second (assoc '(#x0008 . #x0013) dicom-alist :test #'equal))
	      ;; Series Time
	      (second (assoc '(#x0008 . #x0031) dicom-alist :test #'equal))
	      ;; Study Time
	      (second (assoc '(#x0008 . #x0030) dicom-alist :test #'equal))
	      ;; Acquisition Time
	      (second (assoc '(#x0008 . #x0032) dicom-alist :test #'equal))
	      "000000.0"))))

    (declare (type simple-base-string plan-name plan-date plan-time))

    ;; Write information on structure-sets to regular log file.
    (format t "~%Writing structure-sets: ~D actual, ~D empty structures,~%"
	    (length structure-list)
	    (length skipped-item-list))

    (format t "~%Patient Name:     ~S~%" canonical-name)
    (format t "Hospital ID:      ~S~%" dicom-pat-id)
    (format t "Structure-set ID: ~D~%" prism-pat-id)
    (format t "Plan Name:        ~S~%" plan-name)
    (format t "Plan Date:        ~S~%" plan-date)
    (format t "Plan Time:        ~S~%~%" plan-time)
    (dolist (item structure-list)
      (format t "Actual structure: ~S, Sent type: ~A, Import type: ~A~%"
	      (name (first item)) (second item) (third item)))
    (when (consp skipped-item-list)
      (dolist (item (nreverse skipped-item-list))
	(format t "Skipped structure: ~S, Sent type: ~A, (no contours)~%"
		(first item) (second item))))
    (format t "~%")

    (when (consp structure-list)
      ;; NB: Write Structure-Set data file before updating index file.
      ;; This prevents Prism access before data is ready.
      ;; Note that the structure index file is written [and therefore, a
      ;; patient index number assigned when file later is read] only if we
      ;; write non-empty structure-sets.  We ignore empty structure-sets;
      ;; we write no structure-set data file, and we make no entry in the
      ;; structure-set index file for them.
      (let ((structure-set-filename
	      (format nil "~Apat-~D.structure-set" output-db prism-pat-id)))
	(declare (type simple-base-string structure-set-filename))
	(format t "Writing structure-data file: ~S (~D structures)~%"
		structure-set-filename (length structure-list))
	(with-open-file (strm structure-set-filename :direction :Output
			      :element-type 'base-char
			      :if-does-not-exist :Create :if-exists :Append)
	  (dolist (item structure-list)
	    (put-object (first item) strm 4))))     ;*PRINT-PRETTY* is T here.
      (let ((*print-pretty* nil))
	(with-open-file (strm structure-idx-filename :direction :Output
			      :element-type 'base-char
			      :if-does-not-exist :Create :if-exists :Append)
	  (format strm "(~D ~S ~S ~S ~S ~S ~S)~%"
		  prism-pat-id
		  canonical-name
		  dicom-pat-id
		  plan-date
		  plan-time
		  plan-name
		  (format nil "~A"
			  (or (mapcar #'(lambda (x)
					  (name (first x)))
				structure-list)
			      "No structures"))))))))

;;;=============================================================
;;; Utility functions used in main functions above.

(defun get-canonical-name (dicom-pat-name)
  (declare (type simple-base-string dicom-pat-name))
  (let ((caret-pos (position #\^ dicom-pat-name :test #'char=)))
    (cond
      ((typep caret-pos 'fixnum)
       (do* ((name
	       (format nil "~A, ~A"
		       (string-capitalize (subseq dicom-pat-name 0 caret-pos))
		       (string-right-trim
			 "^"
			 (string-capitalize
			   (subseq
			     dicom-pat-name
			     (the fixnum (1+ (the fixnum caret-pos))))))))
	     (len (length name))
	     (idx 0 (the fixnum (1+ idx))))
	    ((= idx len)
	     name)
	 (declare (type simple-base-string name)
		  (type fixnum len idx))
	 (when (char= (aref name idx) #\^)
	   (setf (aref name idx) #\Space))))
      (t dicom-pat-name))))

;;;-------------------------------------------------------------

(defun match-name (name1 name2)
  ;; Compares names character-by-character, case-insensitively,
  ;; ignoring non-alphabetic characters.
  (declare (type simple-base-string name1 name2))

  (cond ((string= name2 "*** No Name ***")
	 ;; Special "missing name" tag indicates automatic mismatch.
	 nil)

	(t (do ((limit1 (length name1))
		(limit2 (length name2))
		(p1 0) (p2 0) (ch1) (ch2))
	       ((and (= p1 limit1) (= p2 limit2))
		t)
	     (declare (type fixnum limit1 limit2 p1 p2))
	     (setq ch1 (and (< p1 limit1) (aref name1 p1))
		   ch2 (and (< p2 limit2) (aref name2 p2)))
	     (cond ((and (characterp ch1)
			 (characterp ch2)
			 (char-equal ch1 ch2))
		    (setq p1 (the fixnum (1+ p1)))
		    (setq p2 (the fixnum (1+ p2))))
		   ((and (characterp ch1)
			 (not (alpha-char-p ch1)))
		    (setq p1 (the fixnum (1+ p1))))
		   ((and (characterp ch2)
			 (not (alpha-char-p ch2)))
		    (setq p2 (the fixnum (1+ p2))))
		   (t (return nil)))))))

;;;-------------------------------------------------------------

(defun match-id (id1 id2)
  ;; Compares IDs character-by-character, ignoring non-digit characters.
  (declare (type simple-base-string id1 id2))

  (cond ((string= id2 "*** No ID ***")
	 ;; Special "missing ID" tag indicates automatic mismatch.
	 nil)

	(t (do ((limit1 (length id1))
		(limit2 (length id2))
		(p1 0) (p2 0) (ch1) (ch2))
	       ((and (= p1 limit1) (= p2 limit2))
		t)
	     (declare (type fixnum limit1 limit2 p1 p2))
	     (setq ch1 (and (< p1 limit1) (aref id1 p1))
		   ch2 (and (< p2 limit2) (aref id2 p2)))
	     (cond ((and (characterp ch1)
			 (characterp ch2)
			 (char= ch1 ch2))
		    (setq p1 (the fixnum (1+ p1)))
		    (setq p2 (the fixnum (1+ p2))))
		   ((and (characterp ch1)
			 (not (digit-char-p ch1)))
		    (setq p1 (the fixnum (1+ p1))))
		   ((and (characterp ch2)
			 (not (digit-char-p ch2)))
		    (setq p2 (the fixnum (1+ p2))))
		   (t (return nil)))))))

;;;-------------------------------------------------------------

(defun get-index-list (filename &aux (file-entries '()))

  "GET-INDEX-LIST filename

returns a list of lists, each one containing data about one database entry,
a patient or an image study, from an index file.  The returned list is in
reverse order of the entries in the file."

  (declare (type list file-entries))

  (ignore-errors
    (with-open-file (strm filename :direction :Input
			  :element-type 'base-char
			  :if-does-not-exist nil)
      (when (streamp strm)
	(do ((entry (read strm nil :EOF) (read strm nil :EOF)))
	    ((eq entry :EOF))
	  (push entry file-entries))))

    file-entries))

;;;-------------------------------------------------------------

(defun get-color (rgb-list)

  (declare (type list rgb-list))

  (let ((red? (string/= (first rgb-list) "0"))
	(green? (string/= (second rgb-list) "0"))
	(blue? (string/= (third rgb-list) "0")))

    (declare (type (or null fixnum) red? green? blue?))

    (cond ((and red? green?)
	   (if blue? 'sl:white 'sl:yellow))
	  ((and red? (not green?))
	   (if blue? 'sl:magenta 'sl:red))
	  ((and (not red?) green?)
	   (if blue? 'sl:cyan 'sl:green))
	  (t (if blue? 'sl:blue 'sl:gray)))))

;;;-------------------------------------------------------------
;;; Read an object of type OBJ-TYPE from an arbirary string with
;;; error catching, type-checking, and graceful recovery.

(defun read-object (data-string obj-type situation)

  (declare (type symbol obj-type)
	   (type simple-base-string situation))

  (unless (typep data-string 'simple-base-string)
    (dicom::mishap nil nil
		   #.(concatenate
		       'string
		       "READ-OBJECT [1] Expected ~S and got empty"
		       " slot~%  while reading ~A.  Aborting Association.")
		   obj-type situation))

  (multiple-value-bind (obj-itself msg)
      (ignore-errors
	(read-from-string data-string))

    (cond ((typep msg 'condition)
	   (format t "~%READ-OBJECT [2] Error reading ~A:~%~%"  situation)
	   (describe msg)
	   (dicom::mishap nil nil "Aborting Association."))

	  ((typep obj-itself obj-type)
	   obj-itself)

	  (t (dicom::mishap
	       nil nil
	       #.(concatenate 'string
			      "READ-OBJECT [3] Expected ~S and got ~S, ~S,~%"
			      "  while reading ~A.  Aborting Association.")
	       obj-type
	       (type-of obj-itself)
	       obj-itself
	       situation)))))

;;;-------------------------------------------------------------

(defun put-object (obj-itself strm tab &rest bin-file-data
		   &aux (tab+2 (the fixnum (+ tab 2)))
		   (tab+4 (the fixnum (+ tab 4))))

  "PUT-OBJECT obj-itself strm tab

writes a printed representation of object OBJ-ITSELF to the stream STRM,
in a form suitable to be read in by GET-OBJECT."

  (declare (type list bin-file-data)
	   (type fixnum tab tab+2 tab+4))

  (tab-print (class-name (class-of obj-itself)) strm tab t)

  (dolist (slotname (mapcar #'clos:slot-definition-name
			(clos:class-slots (class-of obj-itself))))
    (when (slot-boundp obj-itself slotname)
      (tab-print slotname strm tab+2 nil)
      (cond ((eq slotname 'pixels)
	     (tab-print bin-file-data strm 0 t))
	    ((eq slotname 'contours)
	     (fresh-line strm)
	     (dolist (obj (slot-value obj-itself slotname))
	       (put-object obj strm tab+4))
	     (tab-print :end strm tab+2 t))
	    (t (tab-print (slot-value obj-itself slotname) strm 0 t)))))

  (tab-print :end strm tab t))                      ; terminates object

;;;-------------------------------------------------------------

(defun tab-print (item strm tab cr?)

  "TAB-PRINT item strm tab? cr?

Given an item, a stream, an indentation value (integer),
and a Return flag (T or NIL), prints the item."

  (declare (type (member nil t) cr?)
	   (type fixnum tab))

  (format strm "~A~S  "
	  (cond ((= tab 0) "")
		(t (make-string tab :initial-element #\Space)))
	  item)

  (when cr? (format strm "~%")))

;;;-------------------------------------------------------------
;;; Convert "19950608" to "Jun 08 1995".

(defun pretty-date (data)
  (declare (type simple-base-string data))
  (format nil "~A ~A ~A"
	  (nth (1- (read-from-string data nil nil :start 4 :end 6))
	       '("Jan" "Feb" "Mar" "Apr" "May" "Jun"
		 "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))
	  (subseq data 6 8)
	  (subseq data 0 4)))

;;;-------------------------------------------------------------
;;; Convert "133132.0" to "13:31:32".

(defun pretty-time (data)
  (declare (type simple-base-string data))
  (format nil "~A:~A:~A"
	  (subseq data 0 2)
	  (subseq data 2 4)
	  (subseq data 4 6)))

;;;=============================================================
;;; End.
