;;;
;;; parser-rules
;;;
;;; Rules for DICOM PDU and Message Interpretation.
;;; Contains declarations common to Client and Server.
;;;
;;; 01-Mar-2002 BobGian change rule for User Information Item in Assoc-RQ
;;;    and Assoc-AC PDUs: :SCP/SCU-Role-Item and :SOP-Class-Ext-Neg-Item
;;;    [optional items] upper limit changed from :No-Limit -> 1.
;;; 23-Apr-2002 BobGian UIDs in A-Assoc-RQ/AC :Null-Pad -> :No-Pad.
;;; 29-Jul-2002 BobGian change rule for User Information Item in Assoc-RQ
;;;    and Assoc-AC PDUs: :SCP/SCU-Role-Item and :SOP-Class-Ext-Neg-Item
;;;    [optional items] upper limit changed from 1 -> :No-Limit.
;;;    :SOP-Class-Ext-Neg-Item is parsed but currently ignored.
;;; Jul/Aug 2002 BobGian comments indicate whether environmental values
;;;   decoded in rules are actually used at present or not.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================
;;; Rules for Parsing Received PDUs.
;;;
;;; Variables commented "Local Env" in <LOOKUP-VAR forms get values from
;;; lookup in local environment of structure being parsed.
;;;
;;; Otherwise, variables get their values from the environment via an access
;;; chain provided as explicit arguments in <LOOKUP-VAR terms.
;;;
;;; The access chain mechanism is implemented but so far all parsing rules
;;; use "Local Env" access only.

(defparameter *Parser-Rule-List*
  `(

    ;;=============================================
    ;; PDU Interpretation Rules.
    ;;=============================================

    ;; A-Associate-RQ PDU rule == COMPLETE PDU.

    (:A-Associate-RQ

      #x01                               ;A-Associate-RQ PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      ;; Protocol Version [2-byte bitstring]
      (>decode-var Protocol-Version fixnum 2 :Big-Endian)

      (=ignored-bytes 2)               ;Reserved field -- not tested [2 bytes]

      ;; Called AE Title [16-byte string] -- Local host accepting association.
      (>decode-var Called-AE-Title string 16 :Space-Pad)

      ;; Calling AE Title [16-byte string] -- Remote host requesting assoc.
      (>decode-var Calling-AE-Title string 16 :Space-Pad)

      (=ignored-bytes 32)             ;Reserved field -- not tested [32 bytes]

      :Application-Context-Item

      ;; 1 or more Presentation Context Items
      (:Repeat (1 :No-Limit) :Presentation-Context-Item-RQ)

      :User-Information-Item)

    ;;---------------------------------------------
    ;; Presentation Context Item rule for Assoc-RQ PDU.

    (:Presentation-Context-Item-RQ

      #x20                        ;Presentation Context Item type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Field Length [2 bytes -- PC-ID to end of last Transfer Syntax Item]
      (>decode-var PCI-Len fixnum 2 :Big-Endian)    ;Not used at present.

      (>decode-var PC-ID fixnum 1)           ;Presentation Context ID [1 byte]

      (=ignored-bytes 3)               ;Reserved field -- not tested [3 bytes]

      :Abstract-Syntax-Item-RQ

      (:Repeat (1 :No-Limit) :Transfer-Syntax-Item))

    ;;---------------------------------------------
    ;; Abstract Syntax Item rule for Assoc-RQ PDU.

    (:Abstract-Syntax-Item-RQ

      #x30                             ;Abstract Syntax Item type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Abstract Syntax Name field length [2 bytes]
      (>decode-var ASN-Len fixnum 2 :Big-Endian)

      ;; Abstract Syntax Name [variable-length byte string]
      (>decode-var ASN-Str
		   string
		   (<lookup-var ASN-Len)            ;Local Env
		   :No-Pad))

    ;;=============================================
    ;; A-Associate-AC PDU rule == COMPLETE PDU.

    (:A-Associate-AC               ;SCU-only normally, SCP in error conditions

      #x02                               ;A-Associate-AC PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      ;; Protocol Version [2-byte bitstring]
      (>decode-var Protocol-Version fixnum 2 :Big-Endian)

      (=ignored-bytes 2)               ;Reserved field -- not tested [2 bytes]

      ;; Called AE Title [16-byte string] -- Remote host being called.
      (>decode-var Called-AE-Title string 16 :Space-Pad)

      ;; Calling AE Title [16-byte string] -- Local host requesting assoc.
      (>decode-var Calling-AE-Title string 16 :Space-Pad)

      (=ignored-bytes 32)             ;Reserved field -- not tested [32 bytes]

      :Application-Context-Item

      ;; 1 or more Presentation Context Items
      (:Repeat (1 :No-Limit) :Presentation-Context-Item-AC)

      :User-Information-Item)

    ;;---------------------------------------------
    ;; Presentation Context Item rule for Assoc-AC PDU.

    (:Presentation-Context-Item-AC ;SCU-only normally, SCP in error conditions

      #x21                        ;Presentation Context Item type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Field Length [2 bytes -- PC-ID to end of last Transfer Syntax Item]
      (>decode-var PCI-Len fixnum 2 :Big-Endian)    ;Not used at present.

      (>decode-var PC-ID fixnum 1)           ;Presentation Context ID [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; 0: Accept
      ;; 1: User-Reject
      ;; 2: Provider-Reject
      ;; 3: Abstract-Syntax Not Supported
      ;; 4: Transfer-Syntax Not Supported
      (>decode-var Result/Reason fixnum 1)

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Transfer Syntax Item is significant only if Result/Reason
      ;; is zero [Acceptance]; it is ignored if Result/Reason is non-zero
      ;; [indicating Rejection].
      :Transfer-Syntax-Item)

    ;;=============================================
    ;; Application Context Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Application-Context-Item

      #x10                         ;Application Context Item type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Application Context Name Length [2 bytes]
      (>decode-var ACN-Len fixnum 2 :Big-Endian)

      ;; Application Context Name [variable length]
      (>decode-var ACN-Str
		   string
		   (<lookup-var ACN-Len)            ;Local Env
		   :No-Pad))

    ;;---------------------------------------------
    ;; Transfer Syntax Item rule for Assoc-RQ and Assoc-AC PDUs.
    ;; May be more than one for Assoc-RQ.

    (:Transfer-Syntax-Item

      #x40                             ;Transfer Syntax Item type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Transfer Syntax Name field length [2 bytes]
      (>decode-var TSN-Len fixnum 2 :Big-Endian)

      ;; Transfer Syntax Name [variable-length byte string]
      (>decode-var TSN-Str
		   string
		   (<lookup-var TSN-Len)            ;Local Env
		   :No-Pad))

    ;;---------------------------------------------
    ;; User Information Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:User-Information-Item

      #x50                            ;User Information Item Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; User Data Item Field Length [2 bytes]
      (>decode-var UII-Len fixnum 2 :Big-Endian)    ;Not used at present.

      :Max-DataField-Len-Item

      :Implementation-Class-UID-Item

      ;; Order of elements here is ambiguous in spec, and various clients
      ;; seem to do it differently.  I list some elements redundantly so
      ;; parse will succeed for several different possible orders.

      ;; Spec says required, but CTN and other clients do this optionally.
      (:Repeat (0 1) :Implementation-Version-Name-Item) ;Optional

      (:Repeat (0 1) :Asynchronous-Ops-Item)        ;Optional

      ;; One per SOP-Class-UID at most.
      ;; Optional in Assoc-RQ -- sent in Assoc-AC only if in Assoc-RQ.
      (:Repeat (0 :No-Limit) :SCP/SCU-Role-Item)

      ;; Spec says required, but CTN and other clients do this optionally.
      (:Repeat (0 1) :Implementation-Version-Name-Item) ;Optional

      ;; One per SOP-Class-UID at most.
      (:Repeat (0 :No-Limit) :SOP-Class-Ext-Neg-Item))  ;Currently ignored.

    ;;---------------------------------------------
    ;; Maximum DataField Length Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Max-DataField-Len-Item

      #x51                         ;Maximum Length Sub-Item field tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Maximum Length Received field length [val = 4, 2 bytes]
      (=fixnum-bytes 4 2 :Big-Endian)

      ;; Maximum Length Received variable.  Zero -> no limit.
      (>decode-var Max-DataField-Len fixnum 4 :Big-Endian))

    ;;---------------------------------------------
    ;; Implementation Class UID Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Implementation-Class-UID-Item

      #x52                         ;Implementation Class UID Item tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Implementation Class UID Item Field Length [2 bytes]
      (>decode-var IC-UID-Len fixnum 2 :Big-Endian) ;Not used, except below.

      ;; Implementation Class UID [variable-len byte string]
      (>decode-var IC-UID-Str                       ;Not used at present.
		   string
		   (<lookup-var IC-UID-Len)         ;Local Env
		   :No-Pad))

    ;;---------------------------------------------
    ;; Asynchronous Operations Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Asynchronous-Ops-Item

      #x53                          ;Asynchronous Operations Item tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Asynchronous Operations Item field length [val = 4, 2 bytes]
      (=fixnum-bytes 4 2 :Big-Endian)

      ;; Max Num Ops Invoked Asynchronously [0 -> unlimited]
      (>decode-var Max-Ops-Invoked fixnum 2 :Big-Endian) ;Not used at present.

      ;; Max Num Ops Performed Asynchronously [0 -> unlimited]
      (>decode-var Max-Ops-Performed fixnum 2 :Big-Endian)) ;Not used.

    ;;---------------------------------------------
    ;; SCP/SCU Role Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:SCP/SCU-Role-Item

      #x54                                     ;SCP/SCU Role Item tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; SCP/SCU Role Item field length [2 bytes]
      (=ignored-bytes 2)      ;Redundant -- subsumed by Role-SOP-Class-UID-Len

      ;; SOP Class UID Item Field Length [2 bytes]
      (>decode-var Role-SOP-Class-UID-Len fixnum 2 :Big-Endian)

      ;; SOP Class UID String [variable-len byte string]
      (>decode-var Role-SOP-Class-UID-Str
		   string
		   (<lookup-var Role-SOP-Class-UID-Len) ;Local Env
		   :No-Pad)

      ;; 0 -> RQ: no SCU, AC: Reject; 1 -> RQ: SCU, AC: Accept
      (>decode-var SCU-Role-Flag fixnum 1)          ;Not used at present.

      ;; 0 -> RQ: no SCP, AC: Reject; 1 -> RQ: SCP, AC: Accept
      (>decode-var SCP-Role-Flag fixnum 1))         ;Not used at present.

    ;;---------------------------------------------
    ;; Implementation Version Name Item rule for Assoc-RQ and Assoc-AC PDUs.

    (:Implementation-Version-Name-Item

      #x55                      ;Implementation Version Name Item tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Implementation Version Name Item Field Length [2 bytes]
      (>decode-var IV-Name-Len fixnum 2 :Big-Endian)  ;Not used, except below.

      ;; Implementation Version Name [variable-len byte string]
      (>decode-var IV-Name-Str                      ;Not used at present.
		   string
		   (<lookup-var IV-Name-Len)        ;Local Env
		   :No-Pad))

    ;;---------------------------------------------
    ;; SOP Class Extended Negotiation Item rule -- Assoc-RQ and Assoc-AC PDUs.

    (:SOP-Class-Ext-Neg-Item                    ;Parsed but currently ignored.

      #x56                   ;SOP Class Extended Negotiation Item tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; Extended Negotiation Item Field Length [2 bytes]
      ;; Not used, except below.
      (>decode-var Ext-Negotiation-Len fixnum 2 :Big-Endian)

      ;; SOP Class UID Item Field Length [2 bytes] Not used, except below.
      (>decode-var EN-SOP-Class-UID-Len fixnum 2 :Big-Endian)

      ;; SOP Class UID String [variable-len byte string]
      (>decode-var EN-SOP-Class-UID-Str             ;Not used at present.
		   string
		   (<lookup-var EN-SOP-Class-UID-Len)   ;Local Env
		   :No-Pad)

      ;; Extended Negotiation data -- varies with SOP class
      (>decode-var Ext-Negotiation-Str              ;Not used at present.
		   string
		   (<funcall -
			     (<lookup-var Ext-Negotiation-Len)  ;Local Env
			     (<lookup-var EN-SOP-Class-UID-Len) ;Local Env
			     2)
		   :No-Pad))

    ;;=============================================
    ;; A-Associate-RJ PDU rule == COMPLETE PDU.

    (:A-Associate-RJ               ;SCU-only normally, SCP in error conditions

      #x03                               ;A-Associate-RJ PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      ;; 1: Rejection-Permanent
      ;; 2: Rejection-Transient
      (>decode-var RJ-Result fixnum 1)

      ;; 1: UL Service-User
      ;; 2: UL Service-Provider [ACSE]
      ;; 3: UL Service-Provider [Presentation Layer]
      (>decode-var RJ-Source fixnum 1)

      ;; If RJ-Source = 1:
      ;;   1: No-Reason-Given
      ;;   2: Application-Context-Name-Not-Supported
      ;;   3: Calling-AE-Title-Not-Recognized
      ;;   4-6: Reserved
      ;;   7: Called-AE-Title-Not-Recognized
      ;;   8-10: Reserved
      ;;
      ;; If RJ-Source = 2:
      ;;   1: No-Reason-Given
      ;;   2: Protocol-Version-Not-Supported
      ;;
      ;; If RJ-Source = 3:
      ;;   0: Reserved
      ;;   1: Temporary-Congestion
      ;;   2: Local-Limit-Exceeded
      ;;   3-7: Reserved
      (>decode-var RJ-Diagnostic fixnum 1))

    ;;=============================================
    ;; P-Data-TF PDU Command/Data-Set DICOM Message rule == COMPLETE PDU.

    (:P-Data-TF

      #x04                                    ;P-Data-TF PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      ;; 1 or more Presentation-Data-Value Items.
      ;; Our system only sends 1 PDV-Item per PDU, but it must be able to parse
      ;; messages from other clients who might send more than one per PDU.
      (:Repeat (1 :No-Limit) :PDV-Item))

    ;;---------------------------------------------
    ;; PDV-Item rule for P-Data-TF PDUs.
    ;; Multiple instances of a :PDV-Item in a single incoming :P-Data-TF PDU
    ;; will result in multiple instances of PDV-Len [used only for parsing],
    ;; PC-ID, and PDV-Message being pushed onto the environment.  Since
    ;; multiple :PDV-Item(s) can appear in a PDU, access with SET retrieval.

    (:PDV-Item

      ;; PDV Length [4 bytes]
      ;; Length is that of PC-ID byte + Control-Header byte + Message length.
      (>decode-var PDV-Len fixnum 4 :Big-Endian)

      (>decode-var PC-ID fixnum 1)           ;Presentation Context ID [1 byte]

      ;; Message Control Header [1 byte]:
      ;;  #b******XY  [* is don't-care bit, X and Y are 2 lowest-order bits]
      ;;  Bit X = 0 -> Message is NOT LAST fragment.
      ;;  Bit X = 1 -> Message is LAST fragment.
      ;;  Bit Y = 0 -> Message is Data-Set.
      ;;  Bit Y = 1 -> Message is a Command.
      (>decode-var PDV-MCH fixnum 1)

      ;; ======================
      ;; DICOM Message: Data-Set
      ;; Variable gets bound to content of message in form
      ;; of a structure:  ( :Message <Start-Idx> <End-Idx> )
      ;; Indices refer to TCP-Buffer -- both must be within current PDV.
      (>decode-var PDV-Message
		   :Message
		   (<funcall -
			     (<lookup-var PDV-Len)  ;Local Env
			     2)))

    ;;=============================================
    ;; A-Release-RQ PDU rule == COMPLETE PDU.

    (:A-Release-RQ

      #x05                                 ;A-Release-RQ PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      (=ignored-bytes 4))              ;Reserved field -- not tested [4 bytes]

    ;;=============================================
    ;; A-Release-RSP PDU rule == COMPLETE PDU.

    (:A-Release-RSP

      #x06                                ;A-Release-RSP PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      (=ignored-bytes 4))              ;Reserved field -- not tested [4 bytes]

    ;;=============================================
    ;; A-Abort PDU rule == COMPLETE PDU.

    (:A-Abort

      #x07                                      ;A-Abort PDU Type tag [1 byte]

      =ignored-byte                     ;Reserved field -- not tested [1 byte]

      (=ignored-bytes 4)             ;PDU Length [4 bytes] parsed procedurally

      (=ignored-bytes 2)               ;Reserved field -- not tested [2 bytes]

      ;; 0: UL Service-User-initiated
      ;; 1: Reserved
      ;; 2: UL Service-Provider-initiated
      (>decode-var Abort-Source fixnum 1)

      ;; If Abort-Source = 0:
      ;;   Not Significant [ignored when received]
      ;;
      ;; If Abort-Source = 2:
      ;;   0: Reason Not Specified
      ;;   1: Unrecognized PDU
      ;;   2: Unexpected PDU
      ;;   3: Reserved
      ;;   4: Unrecognized PDU Parameter
      ;;   5: Unexpected PDU Parameter
      ;;   6: Invalid PDU Parameter Value
      (>decode-var Abort-Diagnostic fixnum 1))

    ;;=============================================
    ;; DICOM Message Interpretation Rules.
    ;;=============================================

    ;; C-Echo-RQ PDV Command/Message rule == MESSAGE ONLY.

    (:C-Echo-RQ

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length

      (>decode-var Group-Len fixnum 4 :Little-Endian)   ;Value (not used)

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      ;; Length Slot
      (>decode-var Echo-SOP-Class-UID-Len fixnum 4 :Little-Endian)

      ;; Value Slot
      (>decode-var Echo-SOP-Class-UID-Str
		   string
		   (<lookup-var Echo-SOP-Class-UID-Len) ;Local Env
		   :Null-Pad)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0030 2 :Little-Endian)       ;Value

      ;;--------- Element 4: Message ID [message being sent]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0110)
      (=fixnum-bytes #x0110 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (>decode-var Echo-Msg-ID fixnum 2 :Little-Endian) ;Value

      ;;--------- Element 5: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0101 2 :Little-Endian))      ;Code for No-Data

    ;;=============================================
    ;; C-Echo-RSP PDV Command/Message rule == MESSAGE ONLY.

    (:C-Echo-RSP

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length

      (>decode-var Group-Len fixnum 4 :Little-Endian)   ;Value (not used)

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      ;; Length Slot
      (>decode-var Echo-SOP-Class-UID-Len fixnum 4 :Little-Endian)

      ;; Value Slot
      (>decode-var Echo-SOP-Class-UID-Str
		   string
		   (<lookup-var Echo-SOP-Class-UID-Len) ;Local Env
		   :Null-Pad)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x8030 2 :Little-Endian)       ;Value

      ;;--------- Element 4: Message ID [message being sent]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0120)
      (=fixnum-bytes #x0120 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (>decode-var Echo-Msg-ID fixnum 2 :Little-Endian) ;Value

      ;;--------- Element 5: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0101 2 :Little-Endian)       ;Code for No-Data

      ;;--------- Element 6: Response Status
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0900)
      (=fixnum-bytes #x0900 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      ;; Status Value: Code for Success is #x0000.
      (>decode-var Echo-Msg-Status fixnum 2 :Little-Endian))

    ;;=============================================
    ;; C-Store-RQ PDV Command/Message rule == MESSAGE ONLY.

    (:C-Store-RQ

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length

      (>decode-var Group-Len fixnum 4 :Little-Endian)   ;Value (not used)

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      (>decode-var Store-SOP-Class-UID-Len fixnum 4 :Little-Endian) ;Length

      (>decode-var Store-SOP-Class-UID-Str          ;Value
		   string
		   (<lookup-var Store-SOP-Class-UID-Len)    ;Local Env
		   :Null-Pad)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0001 2 :Little-Endian)       ;Value

      ;;--------- Element 4: Message ID [message being sent]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0110)
      (=fixnum-bytes #x0110 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (>decode-var Store-Msg-ID fixnum 2 :Little-Endian)    ;Value

      ;;--------- Element 5: Priority
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0700)
      (=fixnum-bytes #x0700 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      ;; #x0002 -> LOW, #x0000 -> MEDIUM, #x0001 -> HIGH
      (>decode-var Store-Priority fixnum 2 :Little-Endian)  ;Value (not used)

      ;;--------- Element 6: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      ;; Value Slot
      ;; Anything not equal to #x0101 -> Data-Present
      (>decode-var DataSet-Type fixnum 2 :Little-Endian)

      ;;--------- Element 7: Affected SOP Instance UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,1000)
      (=fixnum-bytes #x1000 2 :Little-Endian)

      (>decode-var Store-SOP-Instance-UID-Len fixnum 4 :Little-Endian) ;Length

      ;; Value
      (>decode-var Store-SOP-Instance-UID-Str
		   string
		   (<lookup-var Store-SOP-Instance-UID-Len) ;Local Env
		   :Null-Pad)

      ;;--------- Element 8: Move Originator AE Title
      ;; Optional -- required only if C-Store is subservient to a C-Move.
      (:Repeat (0 1) :Move-Originator-AE)

      ;;--------- Element 9: Move Originator Message ID
      ;; Optional -- required only if C-Store is subservient to a C-Move.
      (:Repeat (0 1) :Move-Originator-ID))

    ;;---------------------------------------------
    ;; Move-Originator AE Title subitem rule for C-Store-RQ Message.
    ;; Optional -- required only if C-Store is subservient to a C-Move.

    (:Move-Originator-AE

      ;;--------- Element 8: Move Originator AE Title
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,1030)
      (=fixnum-bytes #x1030 2 :Little-Endian)

      ;; Length (not used at present, except below).
      (>decode-var Move-Orig-AE-Len fixnum 4 :Little-Endian)

      ;; Value
      (>decode-var Move-Orig-AE-Str                 ;Not used at present.
		   string
		   (<lookup-var Move-Orig-AE-Len) ;Local Env (used only here).
		   :Space-Pad))

    ;;---------------------------------------------
    ;; Move-Originator Message ID subitem rule for C-Store-RQ Message.
    ;; Optional -- required only if C-Store is subservient to a C-Move.

    (:Move-Originator-ID

      ;;--------- Element 9: Move Originator Message ID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,1031)
      (=fixnum-bytes #x1031 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (>decode-var Move-Orig-Msg-ID fixnum 2 :Little-Endian)) ;Val (not used).

    ;;=============================================
    ;; C-Store-RSP PDV Command/Message rule == MESSAGE ONLY.

    (:C-Store-RSP

      ;;--------- Element 1: Group Length
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0000)
      (=fixnum-bytes #x0000 2 :Little-Endian)

      (=fixnum-bytes 4 4 :Little-Endian)            ;Length

      (>decode-var Group-Len fixnum 4 :Little-Endian)   ;Value (not used)

      ;;--------- Element 2: Affected SOP Class UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0002)
      (=fixnum-bytes #x0002 2 :Little-Endian)

      (>decode-var Store-SOP-Class-UID-Len fixnum 4 :Little-Endian) ;Length

      (>decode-var Store-SOP-Class-UID-Str          ;Value
		   string
		   (<lookup-var Store-SOP-Class-UID-Len)    ;Local Env
		   :Null-Pad)

      ;;--------- Element 3: Command Field
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0100)
      (=fixnum-bytes #x0100 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x8001 2 :Little-Endian)       ;Value

      ;;--------- Element 4: Message ID [message being responded to]
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0120)
      (=fixnum-bytes #x0120 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (>decode-var Store-Msg-ID fixnum 2 :Little-Endian)    ;Value

      ;;--------- Element 5: Data-Set Type
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0800)
      (=fixnum-bytes #x0800 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      (=fixnum-bytes #x0101 2 :Little-Endian)       ;Code for No-Data

      ;;--------- Element 6: Response Status
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,0900)
      (=fixnum-bytes #x0900 2 :Little-Endian)

      (=fixnum-bytes 2 4 :Little-Endian)            ;Length

      ;; Status Value: Code for Success is #x0000.
      (>decode-var Store-Msg-Status fixnum 2 :Little-Endian)

      ;;--------- Element 7: Affected SOP Instance UID
      (=fixnum-bytes #x0000 2 :Little-Endian)       ;Tag (0000,1000)
      (=fixnum-bytes #x1000 2 :Little-Endian)

      (>decode-var Store-SOP-Instance-UID-Len fixnum 4 :Little-Endian) ;Length

      (>decode-var Store-SOP-Instance-UID-Str       ;Value
		   string
		   (<lookup-var Store-SOP-Instance-UID-Len) ;Local Env
		   :Null-Pad))

    ;;=============================================

    ))

;;;-------------------------------------------------------------
;;; List of all Message types that our system can recognize.

(defparameter *Message-Type-List*
  '(:C-Echo-RQ :C-Echo-RSP :C-Store-RQ :C-Store-RSP))

;;;=============================================================

(eval-when (EVAL LOAD)
  (compile-rules *Parser-Rule-List* :Parser-Rule)
  (setq *Parser-Rule-List* nil))

;;;=============================================================
;;; End.
