;;;
;;; mainloop
;;;
;;; Main Driver Loop for DICOM Message Interpretation and Protocol Actions.
;;; Contains functions common to Client and Server.
;;;
;;; 11-Apr-2001 BobGian convert TCP stream reading/writing code to work
;;;   in ACL Version 6.0 (READ-SEQUENCE, WRITE-SEQUENCE slightly buggy).
;;; 15-Apr-2001 BobGian further hacks to get READ-VECTOR to work correctly.
;;;   Does non-blocking READ.  Code previously assumed blocking READ.
;;; 25-Apr-2001 BobGian fix DUL-MAINLOOP to parse PDUs when multiple PDUs
;;;   come in on a single non-blocking READ with possible read-ahead.
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 18-Aug-2001 BobGian READ-VECTOR -> READ-SEQUENCE.  More portable.
;;; 09-Jan-2001 BobGian modularize system allowing subsystems to be built
;;;   from common code.  DICOM-MAINLOOP takes mode argment to indicate
;;;   :Client or :Server role on per-association basis, and thus it can
;;;   stack role functionality [server can invoke client temporarily].
;;; 24-Jan-2002 BobGian full PDU dump only at log level 4 [full debug mode].
;;; 15-Mar-2002 BobGian TCP stream closed at end of transaction is
;;;   no longer logged as an error.
;;; 16-Mar-2002 BobGian convert READ-SEQUENCE to use blocking READ.
;;;   Non-blocking READ and byte-shifting is too error-prone.
;;;   DUL-MAINLOOP reads and parses incoming PDUs starting at offset zero.
;;; 21-Mar-2002 BobGian SOCKET-RESET error intercepted and interpreted as
;;;   Stream-Closed-by-Remote-Host [some, but not all, clients terminate
;;;   TCP connection this way].  DUL handles this as ordinary Stream-Closed.
;;; 24-Apr-2002 BobGian triggering EVENT-15 sets *STATUS-MESSAGE* rather
;;;   than action function invoked - finer discrimination this way.
;;; 04-May-2002 BobGian implement byte-shifting scheme to allow fragmentation
;;;   on arbitrary [as long as it is EVEN] byte borders within objects and
;;;   Group/Element tag and Length-field headers.  This is done by checking
;;;   continuation from PARSE-OBJECT for length of stored shifted bytes and
;;;   skipping over them in next READ, resetting HEAD pointer to compensate.
;;; 04-May-2002 BobGian add TCP buffer overrun check when reading TCP stream.
;;; 05-May-2002 BobGian if dumping incoming PDU, included leftover bytes
;;;   downshifted by previous PARSE-OBJECT call with bytes in new PDU read.
;;; 06-May-2002 BobGian add error message arg to REPORT-ERROR calls.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================

(defun dicom-mainloop (tcp-buffer tcp-strm new-environment
		       *mode*                       ;Role: :Client or :Server.
		       ;; Client: Signal to contact a Server - EVENT-01.
		       ;; Server: Signal that connection accepted - EVENT-05.
		       *event*
		       &aux
		       (*state* 'state-01)       ;Client or Server start state
		       ;; All internal state variables are bound to initial
		       ;; values on establishment of a new connection [server
		       ;; role] or invocation [client role].
		       (*SOP-class-name* nil) (*parser-state* nil)
		       (*args* nil))

  ;; DICOM-MAINLOOP runs as an infinite loop, terminating when a Next-State
  ;; of NIL is selected by the state-transition table, causing DUL-MAINLOOP
  ;; to return :Return as first value.

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type list new-environment)
	   (type (member :Client :Server) *mode*)
	   (type symbol *event* *state*))

  (do ((iteration 0 (the fixnum (1+ iteration)))
       (pdutype-alist *Code/PDUtype-Alist*)
       (old-environment nil))
      ((eq new-environment :Return))

    (declare (type list pdutype-alist old-environment)
	     (type fixnum iteration))

    (when (>= (the fixnum *log-level*) 2)
      (format t "~%PDS Iteration ~D, State ~A: ~A.~%"
	      iteration *state* (get *state* 'documentation))
      (when (>= (the fixnum *log-level*) 3)
	(unless (eq old-environment new-environment)
	  (setq old-environment new-environment)
	  (print-environment new-environment))))

    (when (eq *mode* :Client)
      (setq tcp-strm *connection-strm*))

    (setq new-environment (dul-mainloop new-environment ; Environment
					tcp-buffer  ; TCP buffer
					tcp-strm    ; TCP stream
					pdutype-alist))))   ; Parser data

;;;-------------------------------------------------------------

(defun dul-mainloop (env tcp-buffer tcp-strm pdutype-alist &aux (head 0)
		     (tail 6) (pdu-end 0) timeout? eof? connection-reset?
		     (log-level *log-level*) (continuation *parser-state*))

  (declare (type list env pdutype-alist)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type (member nil t) timeout? eof? connection-reset?)
	   (type (or null (simple-array t (10))) continuation)
	   (type fixnum head tail pdu-end log-level))

  (when (>= log-level 3)
    (format t "~%Enter DUL-MAINLOOP (~A)~%" *mode*))

  ;; If an event is already signaled on loop entry or by the previous
  ;; action-function, we proceed at once to the State interpreter.
  ;; Otherwise, parse next PDU, reading more TCP input if necessary.
  (unless *event*
    (mp:with-timeout (*artim-timeout* (setq timeout? t))
      ;; Read and decode an incoming PDU.  If PARSE-OBJECT moved unread bytes
      ;; to beginning of TCP buffer, the continuation tells us how many bytes
      ;; to skip over to begin new reading.
      (when (arrayp continuation)
	(setq head (aref (the (simple-array t (10)) continuation) 0))
	(setq tail (the fixnum (+ head 6))))
      (when (>= log-level 3)
	(format t "~%Read PDU bytes ~D -> ~D~%" head tail))
      (unless (< tail #.TCP-Bufsize)
	(mishap env nil "DUL-MAINLOOP [1] Buffer overrun - TAIL: ~S" tail))
      ;; Get first six bytes of PDU.  PDU Type-Code is first byte at HEAD
      ;; [normally zero, but may be greater if unread bytes from prior
      ;; instantiation of PARSE-OBJECT were shifted to front of TCP buffer].
      ;; PDU Length is stored in bytes 2 - 5 [after HEAD], Big-Endian.
      ;; Must parse length before attempting to read rest of PDU because
      ;; we don't yet know how many more bytes to read.
      (unless
	(ignore-errors
	  (cond
	    ((= (read-sequence tcp-buffer tcp-strm :start head :end tail) tail)
	     ;; Masks should be #xFF, but using smaller value keeps everything
	     ;; POSITIVE FIXNUM, and no value will exceed 536870911.  Value
	     ;; stored PDU length field is length of rest of PDU -- add 6 bytes
	     ;; for type-code and length field to get total length.
	     ;; PDU-END points to end of PDU, including offset for any shifted
	     ;; left-over bytes and 6-byte code/length field.
	     (setq pdu-end
		   (the fixnum
		     (+ (logior    ;PDU length not counting code/length field.
			  (ash (the (integer #x00 #x1F)
				 (logand #x1F
					 (the (integer #x00 #xFF)
					   (aref tcp-buffer
						 (the fixnum (+ head 2))))))
			       24)
			  (ash (the (integer #x00 #xFF)
				 (aref tcp-buffer (the fixnum (+ head 3))))
			       16)
			  (ash (the (integer #x00 #xFF)
				 (aref tcp-buffer (the fixnum (+ head 4)))) 8)
			  (the (integer #x00 #xFF)
			    (aref tcp-buffer (the fixnum (+ head 5)))))
			;; TAIL points just past 6 bytes for code/length field.
			tail)))
	     (unless (< pdu-end #.TCP-Bufsize)
	       (mishap env nil "DUL-MAINLOOP [2] Buffer overrun - PDU-END: ~S"
		       pdu-end))
	     (when (>= log-level 3)
	       (format t "~%Read PDU bytes ~D ->" tail))
	     (setq tail (read-sequence tcp-buffer tcp-strm
				       :start tail :end pdu-end))
	     (when (>= log-level 3)
	       (format t " ~D~%" tail))
	     t)
	    ;; Read of less than 6 bytes indicates End-of-File.
	    (t (when (>= log-level 3)
		 (format t "~%EOF on TCP stream signaled.~%"))
	       (setq eof? t))))
	;; Be sure to return non-NIL on success.  NULL return indicates
	;; SOCKET-RESET error [ie, stream closed].
	(setq connection-reset? t)))

    ;; After the READ is done we test for errors, Connection-Closed, or EOF.
    ;; If error happens in either READ-SEQUENCE call, it most likely is a
    ;; SOCKET-RESET error.  This indicates Stream-Closed by Remote Host,
    ;; which DUL handles the same as End-of-File.
    (cond
      (connection-reset?     ;Error case: Stream-Closed exit signals event 17.
	(setq *event* 'event-17)
	(when (>= log-level 1)
	  (format t "~%TCP connection closed (reset) or other error.~%")))

      (timeout?                    ;Timeout during hung READ signals event 18.
	(setq *event* 'event-18)
	(format t "~%~A~%"
		(setq *status-message*
		      (format nil "Timeout after ~D seconds."
			      *artim-timeout*)))
	(report-error env nil))

      ((or eof? (< tail pdu-end))                   ;EOF conveyed from above.
       (when (>= log-level 1)
	 (format t "~%End-of-file on TCP input stream.~%"))
       (setq *event* 'event-17))

      ;; If input is available [TAIL = PDU-END], parse incoming PDU.
      ;; Save TCP buffer bounds for error reporting.
      (t (setq *PDU-tail* pdu-end)
	 (multiple-value-bind (pdutype input-cont new-env)
	     (parse-pdu pdutype-alist env tcp-buffer head pdu-end)
	   (declare (type symbol pdutype)
		    (type list new-env)
		    (type fixnum input-cont))

	   (cond
	     ;; Unrecognized or Invalid PDU or bad PDU length.
	     ((eq pdutype :Fail)
	      (setq *event* 'event-19)
	      ;; Abort-Source = 2: UL Service-Provider-initiated
	      ;; Abort-Diagnostic = 1: Unrecognized/Invalid PDU
	      (setq *args* '(Abort-Source 2 Abort-Diagnostic 1))
	      (format t "~%DUL-MAINLOOP [3] ~A~%"
		      (setq *status-message* "Received malformed PDU."))
	      (report-error env tcp-buffer))

	     ((= input-cont pdu-end)                ;Successful PDU parse
	      (setq env new-env)                    ;Update environment
	      (when (>= log-level 2)
		(format t "~%Decoded PDU type ~A (~D bytes total).~%"
			(get pdutype 'documentation)
			(the fixnum (- pdu-end head))))
	      (cond
		((eq pdutype :A-Associate-AC)
		 ;; A-Associate-AC PDU received on transport connection.
		 (setq *event* 'event-03))

		((eq pdutype :A-Associate-RJ)
		 ;; A-Associate-RJ PDU received on transport connection.
		 (setq *event* 'event-04))

		((eq pdutype :A-Associate-RQ)
		 ;; A-Associate-RQ PDU received on transport connection.
		 (setq *event* 'event-06))

		((eq pdutype :P-Data-TF)
		 ;; P-Data-TF DICOM Message [Command or Data-Set] received.
		 ;; PDV-Message environment variable contains structure as:
		 ;; ( :Message <Start-Idx> <End-Idx> ) with indices refering
		 ;; to TCP-Buffer -- both must be within current PDV.
		 ;; NB: More than one PDV-Item can arrive in a single PDU.
		 ;; Use :Set retrieval to access them.
		 (setq *event* 'event-10))

		((eq pdutype :A-Release-RQ)
		 ;; A-Release-RQ PDU received on open connection.
		 ;; SCU signals EVENT-12A and SCP signals EVENT-12B.
		 (setq *event* (cond ((eq *mode* :Client) 'event-12A)
				     (t 'event-12B))))

		((eq pdutype :A-Release-RSP)
		 ;; A-Release-RSP PDU received on open connection.
		 (setq *event* 'event-13))

		((eq pdutype :A-Abort)
		 ;; A-Abort PDU received on open connection.
		 (setq *event* 'event-16)
		 (format t "~%DUL-MAINLOOP [4] ~A~%"
			 (setq *status-message* "Received A-Abort PDU."))
		 (report-error env tcp-buffer))

		(t (mishap env tcp-buffer "DUL-MAINLOOP [5] Bad PDU type: ~S"
			   pdutype))))

	     ;; Inconsistent length PDU
	     ;; Abort-Source = 2: UL Service-Provider-initiated
	     ;; Abort-Diagnostic = 1: Unrecognized/Invalid PDU
	     (t (setq *args* '(Abort-Source 2 Abort-Diagnostic 1)
		      *event* 'event-15)
		(format t "~%DUL-MAINLOOP [6] ~A~%"
			(setq *status-message*
			      "Received PDU with bad length."))
		(report-error env tcp-buffer "Bad PDU length: ~S ~S"
			      input-cont pdu-end)))))))

  ;; Now run the DUL protocol state machine.
  (let ((actions (get *state* *event*))
	(action-fcn) (next-state))

    (unless (consp actions)
      (mishap env nil "DUL-MAINLOOP [7] No entry for event ~S, state ~S"
	      *event* *state*))

    (setq action-fcn (first actions)
	  next-state (second actions))

    (when (>= log-level 2)
      (format t "~%Event ~A: ~A.~%  Action ~A: ~A.~%  Next-state ~A: ~A.~%"
	      *event*
	      (get *event* 'documentation)
	      (or action-fcn "None")
	      (or (get action-fcn 'excl::%fun-documentation) "Loop exit")
	      (or next-state "None")
	      (cond (next-state (get next-state 'documentation))
		    (t "Leave DUL main loop"))))

    (setq *event* nil)
    ;; Must reset to NIL so it can be tested [and found to be NIL] on next
    ;; cycle UNLESS some action function sets it to a non-NIL value.

    (when action-fcn
      ;; Null ACTION-FCN happens only when NEXT-STATE is also NIL
      ;; and immanent action is termination of DUL-MAINLOOP.
      (let ((new-env (funcall action-fcn env tcp-buffer tcp-strm)))
	;; If updated environment is passed back from command parser
	;; embedded in action function, update ENV.  Otherwise [NIL is
	;; returned if no update] do NOT bash ENV.
	(when (consp new-env)
	  (setq env new-env))))

    (when (>= log-level 3)
      (format t "~%Leave DUL-MAINLOOP (~A)~%" *mode*))

    ;; Non-null next state -> go to it.
    ;;  First return value: ENV, to continue with next iteration.
    ;; NULL next state -> done with this connection.
    ;;  Return value: :Return -> signal for caller to return
    ;;                or Environment if continuing.
    (cond (next-state
	    (setq *state* next-state)
	    env)
	  (t :Return))))

;;;=============================================================

(defun parse-pdu (pdutype-alist env tcp-buffer head tail
		  &aux (pducode 0) pdutype val-1 (val-2 0) (val-3 nil))

  ;; VAL-1 always set; VAL-2 and VAL-3 have default values.

  "Success Returns:  PDU-Type  Input-Stream-Continuation-Pointer  Environment.
Failure Returns:  :Fail  Zero  NIL."

  (declare (type list pdutype-alist env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type symbol val-1)
	   (type fixnum head tail pducode val-2))

  (when (>= (the fixnum *log-level*) 4)
    (when (> head 0)
      (dump-bytestream "Shifted Unread Bytes" tcp-buffer 0 head))
    (dump-bytestream "Incoming PDU" tcp-buffer head tail))

  (setq pducode (aref tcp-buffer head) ;First byte [at HEAD] is PDU type code.
	pdutype (assoc pducode pdutype-alist :test #'=))

  (cond ((consp pdutype)
	 (setq pdutype (cdr pdutype))
	 ;; Each PDUTYPE value a PDU-naming symbol which has a :Parser-Rule
	 ;; property which is the rule for parsing that PDU type.
	 (multiple-value-bind (input-cont new-env)
	     (parse-group (get pdutype :Parser-Rule)
			  env tcp-buffer (the fixnum (+ head 6)) tail)
	   ;; PDU type keyword, PDU code byte, don't-care byte, and
	   ;; PDU-Length fields have already been elided by rule compiler.
	   ;; Skip first 6 input bytes already parsed procedurally.
	   (declare (type fixnum input-cont))
	   ;; PARSE-GROUP returns :Fail [as second value] if parse fails,
	   ;; indicating an improperly formatted PDU was received or that
	   ;; parse rules contain errors.  If parse succeeds, assign all
	   ;; return values here.  If not, return :Fail.
	   (cond ((eq new-env :Fail)
		  (setq val-1 :Fail))
		 (t (setq val-1 pdutype val-2 input-cont val-3 new-env)))))

	(t (setq val-1 :Fail)))

  ;; Return values -- First:  Symbol naming PDU or :Fail.
  ;;                  Second: Continuation pointer in input buffer.
  ;;                          Should point to byte just after PDU end.
  ;;                  Third:  NIL or environment alist.
  (values val-1 val-2 val-3))

;;;=============================================================
;;; End.
