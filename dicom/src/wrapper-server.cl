;;;
;;; wrapper-server
;;;
;;; Server-mode wrappers and functions.
;;; Contains functions used in Server only.
;;;
;;; 21-Dec-2000 BobGian wrap IGNORE-ERRORS around error-prone fcns.
;;;   Include error-recovery options in case those fcns barf.
;;;   Change a few local variable names for consistency.
;;; 26-Dec-2000 BobGian change local variable name [date -> date-string].
;;; 11-Apr-2001 BobGian remove name-server lookup and printing of hostname.
;;;   IP address contains same information and is much faster.
;;; 13-Apr-2001 BobGian add logging of source/target to file move.
;;; 30-Jul-2001 BobGian improve formatting of data sent to log file.
;;; 15-Oct-2001 BobGian remove file moving - outputs now written directly.
;;; 15-Oct-2001 BobGian flush chown [replaced by SGID bit] and chmod
;;;   [replaced by umask in starting shell] mechanisms.
;;; 17-Oct-2001 BobGian cache string variable initialization: "" -> NIL.
;;; 23-Oct-2001 BobGian *PACKAGE* not bound - symbols printed instead
;;;   using "~A" FORMAT directive.
;;; 23-Oct-2001 BobGian *PRINT-PRETTY* -> NIL; no need to indent index files.
;;;   Also, *PRINT-ARRAY* -> T globally.
;;; 08-Jan-2002 BobGian move *STATUS-CODE* and *STATUS-MESSAGE* to proper
;;;   scope for server - bound for each connection, not for life of server.
;;; 11-Jan-2002 BobGian remodularize functionality so full DUL state is
;;;   encapsulated in state of special variables bound on server connection
;;;   acceptance, so PDS can stack state to run client as subsystem of server.
;;; 20-Jan-2002 BobGian *PRINT-PRETTY* -> T for printout of configuration data
;;;   then NIL for rest of operation [index files, logging, etc].
;;; 24-Jan-2002 BobGian *PACKAGE* bound to Dicom package so symbols printed
;;;   to log file will not contain package prefix.
;;; 25-Jan-2002 BobGian DICOM-SERVER must bind *CONNECTION-STRM*.
;;; 19-Mar-2002 BobGian add version-identifying banner printout at startup.
;;; 19-Mar-2002 BobGian replace own error message printer [which was not
;;;   always reliable] with call to standard DESCRIBE function.
;;; 24-Apr-2002 BobGian *STATUS-MESSAGE* initialized to NIL and set
;;;   to appropriate message by any error or to "Success" on success.
;;; 24-Apr-2002 BobGian add optional arg to set logging level, overriding
;;;   value in config file.
;;; 27-Apr-2002 BobGian *PRINT-PRETTY* -> T except in index-file functions.
;;; 04-May-2002 BobGian initialize TCP-Buffer to #\* so unused elements
;;;    can be spotted easily in dump.
;;; Jul/Aug 2002 BobGian:
;;;   DICOM-SERVER: Change names of globals printed to log file at startup.
;;;     Bind all dynamic (special) vars on function entry rather than at top
;;;       level (except of course globals storing configuration data).
;;;     If association completes and images were stored successfully, append
;;;       cached records to "image.index" file.  This prevents Prism users from
;;;       accidently accessing an incomplete image set still being received.
;;; 06-Aug-2002 BobGian *PRINT-PRETTY* -> NIL unless overridden,
;;;   and T during printing of configuration information.
;;; 20-Aug-2002 BobGian:
;;;   On error, close TCP stream first, then log message.
;;;   At end of image set (when new set detected by WRITE-IMAGE-SET, or at
;;;     conclusion of association in DICOM-SERVER), log number of images
;;;     stored in each set to "image.index" record and to log file.
;;; 31-Aug-2002 BobGian log count of images actually stored (w/o duplicates).
;;; 17-Sep-2002 BobGian *PRINT-ARRAY* -> NIL always except when needed to be T.
;;; 24-Sep-2002 BobGian add binding for *DICOM-ALIST*.
;;; 18-Oct-2002 BobGian fix bug whereby image index file was updated only if
;;;   last image set received in association contained images that were stored.
;;;   Correct behavior is to update if ANY images are stored successfully
;;;   during the association, regardless of which image set contained them.
;;; 08-May-2003 BobGian: DICOM-SERVER -> RUN-SERVER (symmetry with RUN-CLIENT).
;;; 30-Jul-2003 I. Kalet move dump-dicom-server here for new cvs code
;;; management scheme.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 08-Nov-2003 BobGian - remove *PRINT-RIGHT-MARGIN* and *PRINT-PRETTY*
;;;   bindings; new data dumper makes them irrelevant.
;;; 09-Nov-2003 BobGian - move startup banner version identification to
;;;   /radonc/prism/start-dicom, which prints date of dicom.dxl file.
;;; 24-Dec-2003 BobGian: Variable *REPORTABLE-VARIABLES* holds list of
;;;   configurable variables whose values are logged at server startup.
;;; 02-Mar-2004 BobGian: Writing non-axial images temporarily as test
;;;   of Computed-Radiography SOP class handling.
;;; 27-Apr-2004 BobGian: Variable split - *STORED-IMAGE-COUNT* ->
;;;     *STORED-IMAGE-COUNT-PER-SET* [per-image-set count]
;;;     *STORED-IMAGE-COUNT-CUMULATIVE* [cumulative count over association].
;;; 18-Apr-2005 I. Kalet incorporate SSL socket support per Tung Le.
;;; 19-Jun-2007 I. Kalet correct misplaced ) in call to make-ssl-server-stream
;;; 24-Jun-2009 I. Kalet move globals here to make files independent
;;; of defsystem.  Move dump-dicom-server to make-prism, not really
;;; part of server.  And, use socket: for symbols in acl-socket package
;;;  4-Oct-2009 I. Kalet use environment variable PDS_CONFIG_DIRECTORY
;;; to locate pds.config file.

;;;=============================================================
;;; Package definition needed to write some received Prism objects

(defpackage :slik
  (:nicknames "SL")
  (:export "BLUE" "CYAN" "GRAY" "GREEN" "MAGENTA" "RED" "WHITE" "YELLOW"))

;;;=============================================================

(in-package :dicom)

;;;=============================================================
;;; Cached information.  Reset on each new connection acceptance.

;;; Variables used to cache directory information decided on each association
;;; acceptance.  Updated on each association acceptance.
(defvar *patient-DB*)                 ;Directory for main "patient.index" file
(defvar *matched-pat-image-DB*)      ;Directory for Matched Patient Image data
(defvar *unmatched-pat-image-DB*)  ;Directory for Unmatched Patient Image data
(defvar *structure-DB*)                      ;Directory for Structure-Set data

;;; Variables used to cache patient information once identification is made
;;; on first association so we needn't repeat the search on each successive
;;; association for the same patient.  Updated on each patient identification.
(defvar *cached-dicom-pat-name*)                    ;String - pat name
(defvar *cached-prism-pat-name*)                    ;String - pat name
(defvar *cached-dicom-pat-ID*)                      ;String - pat hosp ID
(defvar *cached-prism-pat-ID*)                      ;Fixnum - Prism number
(defvar *cached-image-DB*)                          ;String - directory

;;; Cached information describing identification of Image Set.
;;; Updated on each Image Set identification [new patient or old-pt/new-set].
(defvar *cached-dicom-set-ID*)                   ;String - Dicom Image-Set UID
(defvar *cached-prism-set-ID*)                ;Fixnum - Prism Image-Set number

;;; Alist of ID-number/UID-string for images stored in current set, used
;;; to test for duplication of image files and for beginning of new Image Set
;;; on same association.  Reset on each Image Set identification.
(defvar *image-ID/UID-alist*)

;;; Count of images actually stored in current Image-Set during current
;;; Association.  Does not count duplicates.  Reset on each new Image Set
;;; identification.
(defvar *stored-image-count-per-set*)

;;; Count of images actually stored cumulatively in all Image-Sets during
;;; current Association.  Does not count duplicates.
(defvar *stored-image-count-cumulative*)

;;; List of records to be appended to "image.index" file at concluson of
;;; successful association.  Not appending records until then prevents Prism
;;; user from inadvertently accessing an incomplete set while image reception
;;; is still in progress.
(defvar *new-im-index-records*)

;;; Pointer to record in "image.index" file for image-set currently being
;;; written [if a new one], so number of images in set can be written to file.
(defvar *current-im-set-record*)

;;;=============================================================
;;; System Parameters -- not user-configurable.

;;; File name merged with current directory or value of
;;; PDS_CONFIG_DIRECTORY environment variable.  DICOM config file is
;;; used only by server.  Client uses standard Prism configuration file.
(defparameter *config-file* "pds.config")

;;;=============================================================
;;; System Parameters -- Configurable via "pds.config" file in directory
;;; "/radonc/prism" for the server.

(defvar *pds-server-port* 104)
(defvar *qlen* 5)

;; Patient case and index data.
(defvar *patient-database* "/prismdata/cases/")

;; Matched Patient images.
(defvar *matched-pat-image-database* "/prismdata/images/")

;; Unmatched Patient images.
(defvar *unmatched-pat-image-database* "/prismdata/imagedump/")

;; Structure-Sets for all patients.
(defvar *structure-database* "/prismdata/structures/")

;;; Association Requestors from whom to accept proposed associations.
;;; If NIL, associations will be accepted from anybody.  If non-NIL, this is
;;; a list of 2-element lists of strings representing IP addresses [dotted]
;;; and AE-Titles of acceptable clients.
;;;
;;; Optionally, each sublist can also contain four more elements, these being
;;; [respectively] the directory for the main "patient.index" file, for the
;;; "image.index" file, for the "patient.index" file for unmatched patient
;;; names [for images], and for the "structure.index" file.  If not present,
;;; these values default to the values of the four variables just above.
(defvar *remote-entities* '( ))

;;; Server AE names acceptable for Association Requestors to use.
;;; If NIL, associations will be accepted for any name.  If non-NIL,
;;; this is a list of AE names acceptable for client to use.  Server has
;;; one name, but certain clients might be configured differently.
;;;
;;; Each AE name is in form of a list:
;;;   ( <Called-AE-Title> <Pat-Index-Dir> <Matched-Pat-Image-Dir>
;;;     <Unmatched-Pat-Image-Dir> <Structure-Set-Dir> )
;;; The first element is required; the rest are optional,
;;; for overriding default target directories.
(defvar *local-entities* '( ))

(defvar *keepalive-timeout* 7200)                   ;Two hours

;;; List of variables settable in "pds.config" file and whose values are
;;; reported to log file at server startup.
(defvar *reportable-variables*
  '(*pds-server-port*
    *qlen*
    *patient-database*
    *matched-pat-image-database*
    *unmatched-pat-image-database*
    *structure-database*
    *keepalive-timeout*
    *artim-timeout*
    *remote-entities*
    *local-entities*
    *image-storage-services*
    *object-storage-services*
    *all-services*
    *application-context-name*
    *transfer-syntax-name*
    *ignorable-groups-list*))

;;;=============================================================
;;; Main server startup function.  No arguments, to facilitate running as
;;; a stand-alone executable.  All configuration done via parameters set in
;;; "dicom-server.system" and local overrides in config file.
;;; Make sure Standard-Output is redirected to a file in Runtime system.

(defun run-server (&aux accept-strm tcp-strm *connection-strm*
			(*print-array* nil) (*package* (find-package :Dicom))
			;; Initialize to #\* so unused elements can be
			;; spotted easily in dump.
			(tcp-buffer
			 (make-array #.TCP-Bufsize
				     :element-type '(unsigned-byte 8)
				     :initial-element #.(char-code #\*))))
  
  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))
  
  (format t "~&~%Prism DICOM Server ...")
  
  (setf *Implementation-Version-Name* "PDS_1.5.1"
	*Implementation-Class-UID* "1.2.840.113994.100.10.1.1")

  (let* ((config-path (sys:getenv "PDS_CONFIG_DIRECTORY"))
	 (config-file (concatenate 'string config-path *config-file*)))
    ;; Configuration file, if present, must be named and located by this var.
    ;; Need exist only if desired to override default parameter values.
    (declare (type simple-base-string config-file))
    (cond ((probe-file config-file)
	   (format t "~&~%Loading configuration file: ~S~%" config-file)
	   (load config-file :verbose nil))
	  (t (format t "~&~%Configuration file not found: ~S~%" config-file))))

  ;; Check here for erroneous configuration before starting main loop.
  ;; Better to abort during startup than to crash during operation.
  (dolist (dirname (list *patient-database*
			 *matched-pat-image-database*
			 *unmatched-pat-image-database*
			 *structure-database*))
    (unless (probe-file dirname)
      (error "RUN-SERVER [1] Non-existent directory: ~S" dirname)))
  
  (format t "~%Logging at level: ~D, ~A~%" *log-level* (date/time))
  (format t "~%Configuration parameters:~%~%")
  (dolist (sym *reportable-variables*)
    (let ((sym-value (symbol-value sym)))
      (cond ((consp sym-value)
	     (format t "  ~S~34TA list of values:~%" sym)
	     (dolist (val sym-value)
	       (format t "~37T~S~%" val)))
	    (t (format t "  ~S~34T~S~%" sym sym-value)))))

  (format t "~%Prism Dicom Server listening for connections.~%")

  (setq accept-strm (socket:make-socket :connect :Passive
				    :address-family :Internet
				    :type :Stream
				    :format :Binary
				    :reuse-address nil
				    :backlog *qlen*
				    :local-port
				    (if *use-ssl* *ssl-port*
				      *pds-server-port*)))
  
  (unwind-protect

      (do ((connection-count 1 (the fixnum (1+ connection-count)))
	   (remote-IP-addr) (remote-IP-string "") (remote-port 0)
	   (local-IP-addr) (local-IP-string "") (local-port 0) (date-string "")
	   (callers *remote-entities*) (*remote-IP-string* nil nil)
	   (caller nil nil) (*status-message* nil nil) (*status-code* -1 -1)
	   (*patient-DB* nil nil) (*cached-dicom-pat-name* nil nil)
	   (*cached-image-DB* nil nil) (*image-ID/UID-alist* nil nil)
	   (*cached-dicom-pat-ID* nil nil) (*cached-prism-set-ID* nil nil)
	   (*cached-dicom-set-ID* nil nil) (*cached-prism-pat-ID* nil nil)
	   (*cached-prism-pat-name* nil nil) (*matched-pat-image-DB* nil nil)
	   (*unmatched-pat-image-DB* nil nil) (*structure-DB* nil nil)
	   (*checkpointed-environment* nil nil) (*max-datafield-len* nil nil)
	   (*new-im-index-records* nil nil) (*current-im-set-record* nil nil)
	   (*stored-image-count-per-set* 0 0)
	   (*stored-image-count-cumulative* 0 0)
	   (*dicom-alist* nil nil))
	  (( ))

	(declare (type list callers caller *checkpointed-environment*
		       *new-im-index-records* *current-im-set-record*)
		 (type simple-base-string remote-IP-string local-IP-string
		       date-string)
		 (type (or null simple-base-string) *status-message*)
		 (type fixnum connection-count remote-port local-port
		       *stored-image-count-per-set*
		       *stored-image-count-cumulative*
		       *status-code*))

	(prog ( )
	 AWAIT-CONNECTION
	  (mp:with-timeout
	      (*keepalive-timeout*
	       (format t "~&PDS: Live on ~A~%" (date/time))
	       (go AWAIT-CONNECTION))
	    (let ((temp-tcp-strm (socket:accept-connection accept-strm
							   :wait t)))
	      (setq tcp-strm
		(if *use-ssl*
		    (socket:make-ssl-server-stream temp-tcp-strm
						   :certificate *certificate*
						   :key *private-key*)
		  temp-tcp-strm)))))
	(setq date-string (date/time)
	      remote-IP-addr (socket:remote-host tcp-strm)
	      remote-IP-string
	      (or (ignore-errors (socket:ipaddr-to-dotted remote-IP-addr))
		  (format nil "~D" remote-IP-addr))
	      remote-port (socket:remote-port tcp-strm)
	      local-IP-addr (socket:local-host tcp-strm)
	      local-IP-string
	      (or (ignore-errors (socket:ipaddr-to-dotted local-IP-addr))
		  (format nil "~D" local-IP-addr))
	      local-port (socket:local-port tcp-strm))

	;; Refuse connections from unknown clients and log that fact
	;; in order to track intrusion attempts.
	(cond
	 ;; If in "promiscuous" mode or IP address matches ...
	 ((or (null callers)
	      (consp (setq caller (assoc remote-IP-string callers
					 :test #'string=))))
	  
	  ;; CALLER is item [3-list or 7-list] on *REMOTE-ENTITIES* describing
	  ;; client whose IP address we just accepted.  If in "promiscuous"
	  ;; mode, CALLER is not set here and thus must be preset to NIL each
	  ;; iteration.  Usage: to log third element, a client-naming string.
	  
	  ;; Cache information for possible error logging.
	  (setq *remote-IP-string* remote-IP-string)
	  
	  ;; Identify remote client each time it connects so messages to
	  ;; follow can be interpreted as to identity of client.
	  (format t
		  #.(concatenate 'string
		      "~&~%Accepting connection ~D, ~A ...~%"
		      "  Client IP address: ~A, port ~D (~A)~%"
		      "  Server IP address: ~A, port ~D~%")
		  connection-count date-string
		  remote-IP-string remote-port (or (third caller) "Unknown")
		  local-IP-string local-port)

	  (unwind-protect
	      (multiple-value-bind (val msg)
		  (ignore-errors
		   (dicom-mainloop tcp-buffer ;TCP buffer
				   tcp-strm ;TCP stream
				   nil	;Initial Environment
				   :Server ;Role or Mode
				   'event-05)) ;Initial activating Event
		(declare (ignore val))
		(when (typep msg 'condition)
		  ;; If an unexpected error occurs, report error and
		  ;; abandon current connection by closing streams but
		  ;; keep server alive to listen for next connection.
		  (format t "~%RUN-SERVER error:~%~%")
		  (describe msg)))

	    ;; After DICOM-MAINLOOP termination server closes current
	    ;; connection and waits for a new one.
	    (close tcp-strm))

	  (when (or (null *status-message*)
		    (string= *status-message* "Success"))
	    ;; When association completes successfully, log results.
	    (let ((im-index-records *new-im-index-records*))
	      (declare (type list im-index-records))
	      (when (consp *image-ID/UID-alist*)
		;; *IMAGE-ID/UID-ALIST* is non-NIL if images were received.
		;; They may have been ignored [not written] either because
		;; they were non-axial images or because they were duplicates.
		;; Images are counted only if actually written to filesystem
		;; during the association.
		(format t "~&Stored ~D images in this set.~%"
			*stored-image-count-per-set*)
		(format
		 t "~%Stored ~D images cumulatively in this association.~%"
		 *stored-image-count-cumulative*))
	      ;; If any new image sets were stored successfully during this
	      ;; association [not just for its last image set received],
	      ;; append records cached earlier to the "image.index" file now.
	      ;; This prevents Prism from accessing image sets before the
	      ;; association concludes.
	      (when (consp im-index-records)
		;; First element of each record is filename.
		;; CDR of each is actual record to append.
		(let ((im-set-record *current-im-set-record*)
		      (*print-pretty* nil))
		  (declare (type list im-set-record))
		  ;; If current record is a new one, update it with number
		  ;; of images in this set.  If current record was already
		  ;; in "image.index" file, do NOT update some other record
		  ;; which happens to be on the list of new records
		  ;; to append to index file.
		  (when (consp im-set-record)
		    (setf (fourth im-set-record)
		      (format nil "Set ~D (~D images): ~A"
			      (third im-set-record)
			      *stored-image-count-per-set*
			      (fourth im-set-record))))
		  ;; But whether or not current record is new, append any
		  ;; cached new records to file, using filename obtained
		  ;; from any of them [use first - all have same filename].
		  (with-open-file (strm (first (first im-index-records))
				   :direction :Output
				   :element-type 'base-char
				   :if-does-not-exist :Create
				   :if-exists :Append)
		    (dolist (item (nreverse im-index-records))
		      (format strm "~S~%" (cdr item))))))))
	  
	  ;; Extra blank line to offset possible keepalives to follow.
	  (format t "~%Closing connection ~D, ~A.~%~%"
		  connection-count (date/time)))
	 
	 (t (format t
		    #.(concatenate 'string
			"~%Refusing connection ~D, ~A ...~%"
			"  Client IP address: ~A, port ~D~%"
			"  Server IP address: ~A, port ~D~%~%")
		    connection-count date-string remote-IP-string
		    remote-port local-IP-string local-port)
	    (close tcp-strm))))
    
    ;; Close passive socket only when server exits due to some error
    ;; or the receipt of a KILL signal from Unix.
    (close accept-strm)
    (format t "~%Closed ACCEPT socket, ~A, ...~%  ~S~%~%Server exit.~%~%"
	    (date/time) accept-strm))
  
  (values))

;;;=============================================================
;;; End.
