;;;
;;; state-rules
;;;
;;; DICOM Upper-Layer Protocol State Transition Table.
;;; Contains declarations common to Client and Server.
;;;
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :dicom)

;;;=============================================================
;;; DUL State Machine transition table.

(defparameter *State-Rule-List*
  `(
    ;;---------------------------------------------
    (state-01
      "Awaiting establishment of connection"
      ((event-01) ae-01 state-04)                   ;SCU only
      ((event-05) ae-05 state-02))                  ;SCP only

    ;;---------------------------------------------
    (state-02                                       ;SCP only
      "Connection open and awaiting A-Associate-RQ PDU"
      ((event-06) ae-06 state-03)
      ((event-15) aa-01 state-13)                   ;Added as error escape
      ((event-16 event-18) nil nil)
      ((event-17) nil nil)
      ((event-03 event-04 event-10 event-12B event-13 event-19)
       aa-01 state-13))

    ;;---------------------------------------------
    (state-03                                       ;SCP only
      "Awaiting A-Associate response from local process"
      ((event-07) ae-07 state-06)
      ((event-08) ae-08 state-13)                  ;Extra args passed to AE-08
      ((event-15 event-18) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-03 event-04 event-06 event-10 event-12B event-13 event-19)
       aa-08 state-13))

    ;;---------------------------------------------
    (state-04                                       ;SCU only
      "Awaiting connection to complete"
      ((event-02) ae-02 state-05)
      ((event-15 event-18) nil nil)
      ((event-17) aa-04 nil))

    ;;---------------------------------------------
    (state-05                                       ;SCU only
      "Awaiting A-Associate-AC or A-Associate-RJ PDU"
      ((event-03) ae-03 state-06)
      ((event-04) ae-04 nil)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-06 event-10 event-12A event-13 event-19) aa-08 state-13))

    ;;---------------------------------------------
    (state-06
      "Association established and ready for Data Transfer"
      ((event-09) dt-01 state-06)                   ;SCU only
      ((event-10) dt-02 state-06)
      ((event-11) ar-01 state-07)                   ;SCU only
      ((event-12A event-12B) ar-02 state-08)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-13 event-19) aa-08 state-13))

    ;;---------------------------------------------
    (state-07                                       ;SCU only
      "Awaiting A-Release-RSP PDU"
      ;; P-Data-TF PDUs may arrive out of order here.
      ((event-10) ar-06 state-07)
      ((event-12A) ar-08 state-09)
      ((event-12B) ar-08 state-10)     ;If STATE-07 is SCU only, what is this?
      ((event-13) ar-03 nil)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-19) aa-08 state-13))

    ;;---------------------------------------------
    (state-08                                       ;SCP only
      "Awaiting A-Release response from local process"
      ((event-09) ar-07 state-08)                   ;Currently not signaled
      ((event-14) ar-04 state-13)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-10 event-12A event-12B
		 event-13 event-19)
       aa-08 state-13))

    ;;---------------------------------------------
    (state-09                                       ;SCU only
      "Awaiting A-Release response from local process"
      ((event-14) ar-09 state-11)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-10 event-12A event-13 event-19)
       aa-08 state-13))

    ;;---------------------------------------------
    (state-10                                       ;SCP only
      "Awaiting A-Release-RSP PDU"
      ((event-13) ar-10 state-12)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-10 event-12B) aa-08 state-13))

    ;;---------------------------------------------
    (state-11                                       ;SCU only
      "Awaiting A-Release-RSP PDU"
      ((event-13) ar-03 nil)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-10 event-12A event-19)
       aa-08 state-13))

    ;;---------------------------------------------
    (state-12                                       ;SCP only
      "Awaiting A-Release response from local process"
      ((event-14) ar-04 state-13)
      ((event-15) aa-01 state-13)
      ((event-16) aa-03 nil)
      ((event-17) aa-04 nil)
      ((event-18) aa-02 nil)
      ((event-03 event-04 event-06 event-10 event-12B event-13 event-19)
       aa-08 state-13))

    ;;---------------------------------------------
    (state-13
      "Waiting for connection to close"
      ((event-06) aa-07A state-13)
      ;; Event-15 added as error escape
      ((event-15 event-16 event-17 event-18) nil nil)
      ((event-19) aa-07B state-13)
      ((event-03 event-04 event-10 event-12A event-12B event-13)
       aa-06 state-13))

    ;;---------------------------------------------
    ))

;;;=============================================================
;;; Event Documentation.

(eval-when (EVAL LOAD)
  (setf (get 'event-01 'documentation) "A-Associate Request")
  (setf (get 'event-02 'documentation) "Outgoing Connection Opened")
  (setf (get 'event-03 'documentation) "A-Associate-AC PDU Received")
  (setf (get 'event-04 'documentation) "A-Associate-RJ PDU Received")
  (setf (get 'event-05 'documentation) "Incoming Connection Accepted")
  (setf (get 'event-06 'documentation) "A-Associate-RQ PDU Received")
  (setf (get 'event-07 'documentation) "A-Associate response -- ACCEPT")
  (setf (get 'event-08 'documentation) "A-Associate response -- REJECT")
  (setf (get 'event-09 'documentation) "P-Data Request Primitive")
  (setf (get 'event-10 'documentation) "P-Data-TF PDU Received")
  (setf (get 'event-11 'documentation) "A-Release Request Primitive")
  (setf (get 'event-12A 'documentation) "A-Release-RQ PDU Rcvd by SCU")
  (setf (get 'event-12B 'documentation) "A-Release-RQ PDU Rcvd by SCP")
  (setf (get 'event-13 'documentation) "A-Release-RSP PDU Received")
  (setf (get 'event-14 'documentation) "A-Release Response Primitive")
  (setf (get 'event-15 'documentation) "A-Abort Request Primitive")
  (setf (get 'event-16 'documentation) "A-Abort PDU Received")
  (setf (get 'event-17 'documentation) "Connection Closed")
  (setf (get 'event-18 'documentation) "ARTIM Timer Expired")
  (setf (get 'event-19 'documentation) "Unrecognized/Invalid PDU Decoded"))

;;;=============================================================
;;; Protocol Data Unit Documentation.

(eval-when (EVAL LOAD)

  ;; PDUs for Association Negotiation.
  (setf (get :A-Associate-RQ 'documentation) "A-Associate-RQ")
  (setf (get :A-Associate-AC 'documentation) "A-Associate-AC")
  (setf (get :A-Associate-RJ 'documentation) "A-Associate-RJ")

  ;; DICOM Message [Command or Data-Set] Transfer PDU.
  (setf (get :P-Data-TF 'documentation) "P-Data-TF")

  ;; PDUs for Association Release.
  (setf (get :A-Release-RQ 'documentation) "A-Release-RQ")
  (setf (get :A-Release-RSP 'documentation) "A-Release-RSP")

  ;; PDU for Association Abort.
  (setf (get :A-Abort 'documentation) "A-Abort")

  ;; C-Echo Message Handling.
  ;; Echo Request - Complete PDU
  (setf (get :C-Echo-RQ 'documentation) "C-Echo-RQ")
  ;; Echo Response - Complete PDU
  (setf (get :C-Echo-RSP 'documentation) "C-Echo-RSP")

  ;; C-Store Message Handling.
  ;; C-Store Request - Multiple PDUs
  (setf (get :C-Store-RTPlan-RQ 'documentation) "C-Store-RTPlan-RQ")
  ;; PDV Message - Command
  (setf (get :C-Store-RTPlan-Command 'documentation) "C-Store-RTPlan-Command")
  ;; PDV Message - Data
  (setf (get :C-Store-RTPlan-Data 'documentation) "C-Store-RTPlan-Data")
  ;; C-Store Request - Multiple PDUs
  (setf (get :C-Store-RQ 'documentation) "C-Store-RQ")
  ;; C-Store Response - Complete PDU
  (setf (get :C-Store-RSP 'documentation) "C-Store-RSP"))

;;;=============================================================

(eval-when (EVAL LOAD)
  (compile-states *State-Rule-List*)
  (setq *State-Rule-List* nil))

;;;=============================================================
;;; End.
