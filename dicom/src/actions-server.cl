;;;
;;; actions-server
;;;
;;; DICOM Upper-Layer Protocol Action functions for Server only.
;;; Contains functions used in Server only.
;;;
;;; 21-Dec-2000 BobGian wrap IGNORE-ERRORS around error-prone fcns.
;;;   Include error-recovery options in case those fcns barf.
;;;   Change a few local variable names for consistency.
;;; 11-Apr-2001 BobGian change format of *REMOTE-ENTITIES* by eliminating
;;;   hostname - dispatch is done only on IP Address and AE Title.
;;; 13-Apr-2001 BobGian fix bug in AE Title test for association acceptance.
;;; 05-Oct-2001 BobGian add extra arg to items in *REMOTE-ENTITIES* list -
;;;   client name for printing in log file.
;;; 06-Oct-2001 BobGian simplify test for matching IP and AET in Association
;;;   acceptance test.  Only one AE-Title allowed per IP [ie, per SCU].
;;; 23-Jan-2002 BobGian install REPORT-ERROR specialized to Server mode.
;;;  Install stubs: GENERATE-OBJECT and CLOSE-CONNECTION [used by Client only].
;;; 13-Mar-2002 BobGian REPORT-ERROR dumps environment and TCP-BUFFER
;;;  if args supplied.  Start/End indices saved in global vars.
;;; 16-Apr-2002 BobGian extend REPORT-ERROR to print list-structure under
;;;  construction.  This functionality is needed by Client to report PDU
;;;  generation process - added for compatibility to Server.
;;; 16-Apr-2002 BobGian MISHAP called in GENERATE-OBJECT [stub] prints
;;;   list-structure representation of its input if called accidently.
;;; 19-Apr-2002 BobGian second arg to REPORT-ERROR can be used to print
;;;   arbitrary list structure or to dump TCP-Buffer.
;;; 23-Apr-2002 BobGian add *MAX-DATAFIELD-LEN* to REPORT-ERROR.
;;;   Also AE-07 caches max PDU size for all subsequent PDU sends.
;;; 06-May-2002 BobGian optional add error message arg to REPORT-ERROR
;;;   and MISHAP.  Sometimes message in embedded call to ERROR gets lost.
;;; 06-May-2002 BobGian add error message arg to REPORT-ERROR calls.
;;; 10-May-2002 BobGian AE-07 checks *MAX-DATAFIELD-LEN* for maximum value
;;;   and for being EVEN when accepting association.
;;; 30-Jul-2002 BobGian EN-SOP-Class-UID-Str (optional item, not used)
;;;   removed from SOP-class-name disambiguation in AE-06.
;;; 06-Aug-2002 BobGian *PRINT-PRETTY* -> T in REPORT-ERROR.
;;; Jul/Aug 2002 BobGian AE-06 does SOP Class lookup (if present in Assoc-RQ)
;;;   via Role-SOP-Class-UID String - no longer uses External-Negotiation
;;;   SOP-Class-UID (Ext Neg documented as "not supported").
;;;   Labels in REPORT-ERROR used to identify variables improved
;;;   (made more consistent with variable name and function).
;;; 17-Aug-2002 BobGian AE-06 logs (at level 0) AE-Titles and IP-Addresses of
;;;   client and server on Association acceptance (already did on rejection).
;;; 30-Aug-2002 BobGian current Image-Set record written by REPORT-ERROR.
;;; 31-Aug-2002 BobGian count of images stored written by REPORT-ERROR.
;;; 17-Sep-2002 BobGian REPORT-ERROR conditionally dumps Dicom-Alist (3rd arg).
;;; 24-Sep-2002 BobGian:
;;;   Remove 3rd arg (DICOM-ALIST) to REPORT-ERROR and MISHAP.  Same
;;;   functionality now obtained via special variable set when data available.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 08-May-2003 BobGian - REPORT-ERROR no longer binds *PRINT-PRETTY*.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 03-Nov-2003 BobGian - Add *STANDARD-OUTPUT* arg to DUMP-DICOM-DATA.
;;; 02-Mar-2004 BobGian: Fix to output formatting in REPORT-ERROR.
;;; 27-Apr-2004 BobGian: REPORT-ERROR modified - *STORED-IMAGE-COUNT* ->
;;;     *STORED-IMAGE-COUNT-PER-SET* [per-image-set count]
;;;     *STORED-IMAGE-COUNT-CUMULATIVE* [cumulative count over association].
;;; 24-Jun-2009 I. Kalet replace so: with socket: for symbols in
;;; acl-socket package
;;;

(in-package :dicom)

;;;=============================================================

;;; All Action functions must return either an updated environment [CONSP]
;;; resulting from parsing a command or NIL.  Don't accidently let some
;;; random trailing value get returned.

;;;=============================================================
;;; Association Establishment Actions.

(defun ae-05 (env tcp-buffer tcp-strm)

  "Issue CONNECTION OPEN message"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer tcp-strm))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AE-05: Transport Connection open.~%"))

  nil)

;;;-------------------------------------------------------------

(defun ae-06 (env tcp-buffer tcp-strm &aux
	      (applic-context-name *Application-Context-Name*)
	      (calling-IP-addr (socket:remote-host tcp-strm))
	      (calling-IP-string
		(or (ignore-errors (socket:ipaddr-to-dotted calling-IP-addr))
		    (format nil "~D" calling-IP-addr)))
	      (calling-AE-name
		(item-lookup 'Calling-AE-Title env t :A-Associate-RQ))
	      (ACN-name
		(item-lookup 'ACN-Str env t
			     :Application-Context-Item :A-Associate-RQ))
	      (protocol-vn
		(item-lookup 'Protocol-Version env t :A-Associate-RQ))
	      (called-IP-addr (socket:local-host tcp-strm))
	      (called-IP-string
		(or (ignore-errors (socket:ipaddr-to-dotted called-IP-addr))
		    (format nil "~D" called-IP-addr)))
	      (called-AE-name
		(item-lookup 'Called-AE-Title env t :A-Associate-RQ))
	      (SOP-class-name
		(item-lookup 'Role-SOP-Class-UID-Str env nil
			     :SCP/SCU-Role-Item
			     :User-Information-Item
			     :A-Associate-RQ))
	      (callers *remote-entities*) (calleds *local-entities*) called
	      extra-args AE-OK? SOP-OK? (services-list *All-Services*))

  "Stop timer, signal EVENT-07/08 if A-Associate-RQ acceptable or not"

  (declare (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (type list env callers extra-args calleds called services-list)
	   (type simple-base-string calling-IP-string called-IP-string
		 calling-AE-name called-AE-name ACN-name applic-context-name)
	   (type (member nil t) AE-OK?)
	   (type fixnum protocol-vn))

  ;; Cache information for possible error logging.
  (setq *calling-AE-name* calling-AE-name
	*called-AE-name* called-AE-name
	*SOP-class-name* SOP-class-name)

  (cond
    ((and
       ;; Application Context Name correct.
       (string= ACN-name applic-context-name)

       ;; Protocol Version supported includes this version.
       (= (logand #x0001 protocol-vn) #x0001)

       ;; Calling AE Title is OK or Server is in promiscuous mode.
       ;; IP address was checked for acceptability when connection was
       ;; originally accepted by server.  It is used here merely as an
       ;; index to determine which AE Titles are possible matches.
       (cond
	 ((null callers)                   ;Promiscuous mode - accept anybody.
	  (setq AE-OK? t))

	 ;; Not promiscuous - check for matching AE title.
	 ;; There may be different AE-Title entries with same IP address.
	 ;; Must therefore check all entries with matching IP addresses
	 ;; for match on AE-Title too, not just first as would be returned
	 ;; by ASSOC.
	 ((dolist (item callers nil)
	    (when (and (string= (first item) calling-IP-string)
		       (string= (second item) calling-AE-name))
	      ;; Each ITEM is of form:
	      ;; ( <IP-Address> <AE-Title> <Client-Name> <Patient-DB>
	      ;;   <Matched-Pat-Im-DB> <Unmatched-Pat-Im-DB> <Structure-DB> )
	      ;; First three are required and rest are optional.
	      ;; Save optional args [if present] in EXTRA-ARGS [NIL otherwise].
	      (setq extra-args (cdddr item))
	      (setq AE-OK? t)
	      (return t))))

	 ;; Non-promiscuous mode and AE Title didn't pass muster.
	 (t nil))

       ;; Called AE name OK (if discriminating on SCU's use of AE name).
       (or (null calleds)
	   (setq called (assoc called-AE-name calleds :test #'string=)))

       ;; If SOP-Class-UID-Str is not provided [it is optional in
       ;; the :A-Associate-RQ PDU] accept.  If provided, it must
       ;; match one of the services Server is prepared to handle.
       (setq SOP-OK?
	     (or (null SOP-class-name)
		 (member SOP-class-name services-list :test #'string=))))

     (format t
	     #.(concatenate
		 'string
		 "~%Accepting Association, ~A~%  From:     ~S (at ~A)~%"
		 "  To:       ~S (at ~A)~%  Service:  ~S~%")
	     (date/time)
	     calling-AE-name calling-IP-string called-AE-name
	     called-IP-string (or SOP-class-name "Unspecified"))

     ;; Signal EVENT-07: A-Associate Response is ACCEPT.
     (setq *event* 'event-07)

     ;; Decide which database set to use.  These variables are set once at
     ;; association-acceptance time and used each time files are written on
     ;; that association.  Decision is made by taking default value from
     ;; global variables set in configuration file unless overriden for this
     ;; association by extra arguments in *REMOTE-ENTITIES* entry for this
     ;; caller or by extra arguments in *LOCAL-ENTITIES* entry for this called.
     (setq *patient-DB* (or (first extra-args)
			    (second called)
			    *patient-database*))
     (setq *matched-pat-image-DB* (or (second extra-args)
				      (third called)
				      *matched-pat-image-database*))
     (setq *unmatched-pat-image-DB* (or (third extra-args)
					(fourth called)
					*unmatched-pat-image-database*))
     (setq *structure-DB* (or (fourth extra-args)
			      (fifth called)
			      *structure-database*))

     ;; Decide which Presentation Contexts to accept.
     (do ((pcs (set-lookup env :Presentation-Context-Item-RQ :A-Associate-RQ)
	       (cdr pcs))
	  (tsn *Transfer-Syntax-Name*)
	  (pc) (response-list '())
	  (asn-OK?) (tsn-OK?))
	 ((null pcs)
	  (setq *args* (list :Set (nreverse response-list))))

       (declare (type list pcs tsn response-list))

       ;; PCS is a LIST of Presentation Context structures each
       ;; as a Variable-Value-Alist.
       ;;
       ;; PC is the <var-value-alist> [containing substructure] of each
       ;; Presentation Context item in turn.  Ie, it looks like a small
       ;; local environment with values only for one Presentation Context.
       (setq pc (car pcs))

       ;; Check that Abstract Syntax Name for this Presentation
       ;; Context matches one of the SOP names of a service.
       (setq asn-OK? (member
		       (item-lookup 'ASN-Str pc t :Abstract-Syntax-Item-RQ)
		       services-list :test #'string=))

       ;; Check that this Presentation Context contains a Transfer
       ;; Syntax Name matching the NEMA default.
       (setq tsn-OK?
	     (dolist (tsi (set-lookup pc :Transfer-Syntax-Item))
	       (when (string= (item-lookup 'TSN-Str tsi t) tsn)
		 (return t))))

       (push `((PC-ID . ,(item-lookup 'PC-ID pc t))
	       (Result/Reason
		 . ,(cond ((and asn-OK? tsn-OK?)
			   0)                       ;R/R = 0: Acceptance
			  ((not asn-OK?)
			   1)                       ;R/R = 1: User-Rejection
			  (t 4))))     ;R/R = 4: Transfer-Syntax Not Supported
	     response-list)))

    ;; Signal EVENT-08 [Association Rejected] and return
    ;; rejection reasons as data to next action function.
    (t (setq *event* 'event-08)
       (format t
	       #.(concatenate
		   'string
		   "~%Rejecting Association, ~A~%  From:     ~S (at ~A)~%"
		   "  To:       ~S (at ~A)~%  Service:  ~S~%")
	       (date/time)
	       calling-AE-name calling-IP-string called-AE-name
	       called-IP-string (or SOP-class-name "Unspecified"))

       ;; All rejection reasons shown at Log level 0.  Test specifically
       ;; for each rejection condition so that correct reason is reported.
       (cond
	 ((string/= ACN-name applic-context-name)
	  (format t "~&  Reason:   Bad Application-Context-Name.~%")
	  (format t "~&  ACtx-Name: ~S~%  Should be: ~S~%"
		  ACN-name applic-context-name)
	  ;; RJ-Result = 1: Rejection-Permanent
	  ;; RJ-Source = 1: UL Service-User
	  ;; RJ-Diagnostic = 2: Application-Context-Name Not Supported
	  (setq *args* '(RJ-Result 1 RJ-Source 1 RJ-Diagnostic 2)))

	 ((not AE-OK?)
	  (format t "~&  Reason:   Bad Calling AE Title: ~S~%" calling-AE-name)
	  ;; RJ-Result = 1: Rejection-Permanent
	  ;; RJ-Source = 1: UL Service-User
	  ;; RJ-Diagnostic = 3: Calling-AE-Title Not Recognized
	  (setq *args* '(RJ-Result 1 RJ-Source 1 RJ-Diagnostic 3)))

	 ((and (consp calleds)                   ;Discriminating on Called but
	       (null called))                       ;item requested not found.
	  (format t "~&  Reason:   Bad Called AE Title: ~S~%" called-AE-name)
	  ;; RJ-Result = 1: Rejection-Permanent
	  ;; RJ-Source = 1: UL Service-User
	  ;; RJ-Diagnostic = 7: Called-AE-Title Not Recognized
	  (setq *args* '(RJ-Result 1 RJ-Source 1 RJ-Diagnostic 7)))

	 ((/= (logand #x0001 protocol-vn) #x0001)
	  (format t "~&  Reason:   Protocol Version not supported: ~S~%"
		  protocol-vn)
	  ;; RJ-Result = 1: Rejection-Permanent
	  ;; RJ-Source = 2: UL Service-Provider [ACSE]
	  ;; RJ-Diagnostic = 2: Protocol Version Not Supported
	  (setq *args* '(RJ-Result 1 RJ-Source 2 RJ-Diagnostic 2)))

	 ((not SOP-OK?)
	  (format t "~&  Reason:   SOP-Class-UID not supported: ~S~%"
		  SOP-class-name)
	  ;; RJ-Result = 1: Rejection-Permanent
	  ;; RJ-Source = 2: UL Service-Provider [ACSE]
	  ;; RJ-Diagnostic = 1: No Reason Given
	  (setq *args* '(RJ-Result 1 RJ-Source 2 RJ-Diagnostic 1)))

	 ;; This branch should never be taken, but it is here to catch errors
	 ;; that result in failure to trigger any of the above branches.
	 (t (format t "~&  Reason:   Unknown.~%")
	    ;; RJ-Result = 1: Rejection-Permanent
	    ;; RJ-Source = 2: UL Service-Provider [ACSE]
	    ;; RJ-Diagnostic = 1: No Reason Given
	    (setq *args* '(RJ-Result 1 RJ-Source 2 RJ-Diagnostic 1))))

       (when (>= (the fixnum *log-level*) 2)
	 (report-error env tcp-buffer "Association Refused"))))

  nil)

;;;-------------------------------------------------------------

(defun ae-07 (env tcp-buffer tcp-strm)

  "Issue A-Associate-AC message and send associated PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  ;; Client proposed max PDU size - accept it and cache minimum of that value
  ;; and server's own maximum for all remaining PDUs during this association.
  (let ((limit (item-lookup 'Max-DataField-Len env nil
			    :Max-DataField-Len-Item
			    :User-Information-Item
			    :A-Associate-RQ)))
    (cond ((typep limit 'fixnum)
	   ;; Spec requires all P-Data-TF PDUs, and therefore all PDVs,
	   ;; to be of even length.
	   (unless (evenp (the fixnum limit))
	     (mishap env nil "AE-07 [1] Odd datafield length: ~S" limit))
	   (setq *max-datafield-len* (min (the fixnum limit) #.PDU-Bufsize)))
	  (t (setq *max-datafield-len* #.PDU-Bufsize))))

  (when (>= (the fixnum *log-level*) 1)
    (format t "~%AE-07: Server accepts A-Associate-RQ from Client.~%")
    (format t "~&Max PDU size negotiated: ~D~%" *max-datafield-len*))

  (apply #'send-pdu :A-Associate-AC env tcp-buffer tcp-strm *args*)

  (setq *args* nil))

;;;-------------------------------------------------------------

(defun ae-08 (env tcp-buffer tcp-strm)

  "Issue A-Associate-RJ message and send associated PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AE-08: A-Associate rejected.~%"))

  (apply #'send-pdu :A-Associate-RJ env tcp-buffer tcp-strm *args*)

  (setq *args* nil))

;;;=============================================================
;;; Association Release Actions.

(defun ar-04 (env tcp-buffer tcp-strm)

  "Send A-Release-RSP PDU"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer))

  (send-pdu :A-Release-RSP env tcp-buffer tcp-strm)

  nil)

;;;-------------------------------------------------------------

(defun ar-10 (env tcp-buffer tcp-strm)

  "Issue A-Release confirmation message"

  (declare (type list env)
	   (type (simple-array (unsigned-byte 8) (#.TCP-Bufsize)) tcp-buffer)
	   (ignore env tcp-buffer tcp-strm))

  (when (>= (the fixnum *log-level*) 3)
    (format t "~%AR-10: A-Release confirmation.~%"))

  nil)

;;;=============================================================
;;; This version of REPORT-ERROR is specialized to Server functionality.
;;; It includes global vars used only by Server.

(defun report-error (env data &optional msg &rest format-args)

  ;; Reports useful information [previously cached as values of global vars]
  ;; to logging stream in case of run-time errors.

  (declare (type list env format-args)
	   (type (or null simple-base-string) msg)
	   (type
	     (or null list (simple-array (unsigned-byte 8) (#.TCP-Bufsize)))
	     data))

  (format t "~%REPORT-ERROR:~%")
  (when (typep msg 'simple-base-string)
    (apply #'cl:format t msg format-args))

  ;; Date, Time:
  (format t "~&~%  Date/Time:~44T~A~%~%" (date/time))

  ;; Identification of communication entities:
  (format t "~&  Remote IP Address:~44T~S~%" *remote-IP-string*)
  (format t "~&  Calling AE Name:~44T~S~%" *calling-AE-name*)
  (format t "~&  Called AE Name:~44T~S~%" *called-AE-name*)
  (format t "~&  Max PDU Size:~44T~S~%~%"*max-datafield-len*)

  ;; Operation being performed:
  (format t "~&  SOP Class Name:~44T~S~%~%" *SOP-class-name*)

  ;; State of PDU/Object parsers and protocol controller:
  (format t "~&  State:~44T~S (~A)~%" *state* (get *state* 'documentation))
  (format t "~&  Event:~44T~S (~A)~%" *event* (get *event* 'documentation))
  (format t "~&  Arguments:~44T~S~%~%" *args*)

  ;; Next set of expressions are used only by Server.  If the two versions
  ;; of this function are merged [when Server invokes Client functionality
  ;; as subservient system], make this form evaluate conditionally on *MODE*.
  ;;
  ;; Cached database selections:
  (format t "~&  Output Patient DB:~44T~S~%" *patient-DB*)
  (format t "~&  Output Matched Pat Image DB:~44T~S~%" *matched-pat-image-DB*)
  (format t "~&  Output Unmatched Pat Image DB:~44T~S~%"
	  *unmatched-pat-image-DB*)
  (format t "~&  Output Structure DB:~44T~S~%" *structure-DB*)
  ;;
  ;; Cached patient identification information:
  (format t "~&  Cached DICOM Name:~44T~S~%" *cached-dicom-pat-name*)
  (format t "~&  Cached Prism Name:~44T~S~%" *cached-prism-pat-name*)
  (format t "~&  Cached Dicom ID:~44T~S~%" *cached-dicom-pat-ID*)
  (format t "~&  Cached Prism ID:~44T~S~%" *cached-prism-pat-ID*)
  (format t "~&  Cached Image DB:~44T~S~%" *cached-image-DB*)
  ;;
  ;; Cached Image-Set identification information:
  (format t "~&  Cached DICOM Set ID:~44T~S~%" *cached-dicom-set-ID*)
  (format t "~&  Cached Prism Set ID:~44T~S~%~%" *cached-prism-set-ID*)
  ;;
  ;; Cached Image ID/UID information:
  (format t "~&  Images stored in this set:~44T~S~%"
	  *stored-image-count-per-set*)
  (format t "~&  ID/UID of images in current Image-Set:")
  (cond ((consp *image-ID/UID-alist*)
	 (dolist (pair *image-ID/UID-alist*)
	   (format t "~&~44T~S~%" pair)))
	(t (format t "~44TNone.~%")))
  (format t "~&  Images stored in this association:~44T~S~%"
	  *stored-image-count-cumulative*)
  (format t "~%")
  ;;
  ;; Cached records to append to "image.index" file at end of association:
  (format t "~&  Current Image-Set record:~44T~S~%" *current-im-set-record*)
  (format t "~&  New \"image.index\" records:")
  (cond ((consp *new-im-index-records*)
	 (dolist (record *new-im-index-records*)
	   (format t "~&~44T~S~%" record)))
	(t (format t "~44TNone.~%")))
  (format t "~%")
  ;;
  ;; End of Server-Only section.

  ;; Status reports:
  (format t "~&  Status Message:~44T~S~%"
	  (or *status-message* "Unknown error"))
  (format t "~&  Status Code:~44T~S~%" *status-code*)

  ;; State of current Environment:
  (when (consp env)
    (print-environment env))

  (cond ((consp data)
	 ;; State of current list-structure object being constructed:
	 ;; PDU datalist is constructed backwards [items CONSed to front].
	 (format t "~%  Output PDU or raw data:~%  ~S~%" data))

	;; Contents of current TCP buffer:
	((arrayp data)
	 ;; This will dump any shifted bytes from prior PARSE-OBJECT call.
	 ;; New PDU will start at HEAD, which may be non-zero.
	 (dump-bytestream "TCP buffer" data 0 *PDU-tail*)))

  ;; One more Server-Only section.
  (when (consp *dicom-alist*)
    (dump-dicom-data *dicom-alist* *standard-output*)))

;;;=============================================================
;;; Stubs.  For now, these functions are used only by Client, but calls
;;; to them appear [in non-invoked conditional branches] in Common code.

(defun close-connection (tcp-strm)
  (declare (ignore tcp-strm))
  (mishap nil nil "CLOSE-CONNECTION called in Server."))

(defun generate-object (object env output-itemlist)
  (declare (ignore output-itemlist))
  (mishap env object "GENERATE-OBJECT called in Server."))

;;;=============================================================
;;; End.
