;;;
;;;  view-graphics
;;; 
;;; 11-Dec-1992 J. Unger started, from discussions with I. Kalet.
;;; 13-Dec-1992 J. Unger & I. Kalet update to aggreed upon scheme.
;;; 22-Dec-1992 I. Kalet remove refs to width and height in SLIK.
;;; 23-Dec-1992 J. Unger add rectangles-prim code, improve documentation.
;;; 29-Dec-1992 J. Unger make clx:draw's in draw methods conditional on
;;;             there actually being something to draw.
;;; 31-Dec-1992 I. Kalet remove image primitive type - not needed
;;;  4-Jan-1993 J. Unger modify lines-prim to handle multiple sets of 
;;;             connected lines.
;;; 24-Feb-1993 J. Unger move color attribute from subclasses into the
;;;             graphic-primitive base class.
;;; 23-Jul-1993 I. Kalet add method here for lines-prim in pixmap.
;;;  2-Feb-1994 J. Unger fix error in segments-prim documentation.
;;; 10-Mar-1994 I. Kalet change draw method for pixmap into function.
;;; 29-May-1994 I. Kalet change draw-lines-pix to generic function
;;; draw-pix.
;;; 18-Sep-1994 J. Unger add filled attribute to rectangles prim, draw
;;; function.
;;;  8-Oct-1996 I. Kalet remove &rest from draw methods, move
;;;  get-segments-prim and get-rectangles-prim here from
;;;  beam-graphics.
;;; 17-Apr-1998 I. Kalet add draw-pix methods for the rest, and just
;;; have a single generic draw method.
;;; 16-Jul-1998 I. Kalet add a visible attribute and support for it.
;;;

(in-package :prism)

;;;-------------------------------------

(defclass graphic-primitive ()

  ((object :accessor object
           :initarg :object
           :documentation "The graphic object from which this
primitive was generated; eg: an organ, a beam, etc.")

   (color :accessor color
          :initarg :color
          :documentation "The color of the low-level graphic
information, a clx:gcontext.")

   (visible :accessor visible
	    :initform t
	    :documentation "This attribute determines whether the
graphic primitive actually appears in the view or is ignored.")

   )

  (:documentation "A low-level representation of a graphical object.")

  )

;;;-------------------------------------

(defmethod draw ((gp graphic-primitive) (v view))

  "Defers dispatching on prim type to the draw-pix function."

  (if (visible gp) (draw-pix gp (sl:pixmap (picture v)))))

;;;-------------------------------------

(defclass lines-prim (graphic-primitive)

  ((points :accessor points
           :initarg :points
           :documentation "A list of points lists.  Each point list
consists of a series of x y pixel-space pairs, that define the lines
to be drawn for a given connected loop.")

   )

  (:documentation "A low level representation of a list of sequences
of connected line segments.")

  )

;;;-------------------------------------

(defun make-lines-prim (points color &rest other-initargs)

  "MAKE-LINES-PRIM points color &rest other-initargs

Returns a lines-prim graphics primitive, with points, color, and other
initialization attributes appropriately set."

  (apply #'make-instance 'lines-prim 
    :points points :color color other-initargs))

;;;----------------------------------

(defmethod draw-pix ((l lines-prim) px)

  "Draws lines primitive object l into pixmap px."

  (when (points l)
    (dolist (pts (points l))
      (clx:draw-lines px (color l) pts))))

;;;-------------------------------------

(defclass segments-prim (graphic-primitive)

  ((points :accessor points
           :initarg :points
           :documentation "A sequence of the form {x1 y1 x2 y2}*,
where each four successive elements defines the two endpoints of a
line segment.")

   )

  (:documentation "A low level representation of a sequence of
unconnected line segments.")

  )

;;;-------------------------------------

(defun make-segments-prim (points color &rest other-initargs)

  "MAKE-SEGMENTS-PRIM points color &rest other-initargs

Returns a segments-prim graphics primitive, with points, color, and
other initialization attributes appropriately set."

  (apply #'make-instance 'segments-prim 
    :points points :color color other-initargs))

;;;-------------------------------------

(defun get-segments-prim (obj v clr)

  "GET-SEGMENTS-PRIM obj v clr

Creates an empty segments graphic primitive for object obj on view v's
foreground list, with color clr, and returns the created primitive."

  (first (push (make-segments-prim nil clr :object obj)
	       (foreground v))))

;;;-------------------------------------

(defmethod draw-pix ((s segments-prim) px)

  "Draws segments primtive object s into pixmap px."

  (when (points s) (clx:draw-segments px (color s) (points s))))

;;;-------------------------------------

(defclass characters-prim (graphic-primitive)

  ((characters :accessor characters
               :initarg :characters
               :documentation "The characters to be drawn.")

   (x :accessor x
      :initarg :x
      :documentation "The x coordinate of the left baseline position
for the first character drawn.")

   (y :accessor y
      :initarg :y
      :documentation "The y coordinate of the left baseline position
for the first character drawn.")

   )

  (:documentation "A low level representation of a sequence of
characters.")

  )

;;;-------------------------------------

(defun make-characters-prim (characters x y color &rest other-initargs)

  "MAKE-CHARACTERS-PRIM characters x y color &rest other-initargs

Returns a characters-prim graphics primitive, with characters, x, y,
and color attributes appropriately set."

  (apply #'make-instance 'characters-prim
    :characters characters :x x :y y :color color other-initargs))

;;;-------------------------------------

(defmethod draw-pix ((c characters-prim) px)

  "Draws characters object c into pixmap px."

  (when (characters c)
    (clx:draw-glyphs px (color c) (x c) (y c) (characters c))))

;;;-------------------------------------

(defclass rectangles-prim (graphic-primitive)

  ((rectangles :accessor rectangles
               :initarg :rectangles
               :documentation "A list of 4-tuples, each of the form 
(ulc-x ulc-y width height), which define the rectangles to be drawn.")

   (filled :accessor filled
           :initarg :filled
           :documentation "When nil, causes the rectangles to be drawn
in a non-filled fashion.  Otherwise, causes them to be drawn filled.")

   )

  (:default-initargs :filled nil)

  (:documentation "A low level representation of a sequence of
unconnected rectangles.")

  )

;;;-------------------------------------

(defun make-rectangles-prim (rects color &rest other-initargs)

  "MAKE-RECTANGLES-PRIM rects color &rest other-initargs

Returns a rectangles-prim graphics primitive, with rects, color, and
other initialization attributes appropriately set."

  (apply #'make-instance 'rectangles-prim
    :rectangles rects :color color other-initargs))

;;;-------------------------------------

(defun get-rectangles-prim (obj v clr)

  "GET-RECTANGLES-PRIM obj v clr

Creates an empty rectangles graphic primitive for object obj on view
v's foreground list, with color clr, and any future rectangles to be
filled, and returns the created primitive."

  (first (push (make-rectangles-prim nil clr :object obj :filled t) 
	       (foreground v))))

;;;-------------------------------------

(defmethod draw-pix ((r rectangles-prim) px)

  "Draws rectangles primitive object r into pixmap px."

  (when (rectangles r)
    (clx:draw-rectangles px (color r) (rectangles r) (filled r))))

;;;-------------------------------------
;;; End.
