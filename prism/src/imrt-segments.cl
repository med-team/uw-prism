;;;
;;; imrt-segments
;;;
;;; Handle multi-segment IMRT beams composed of ordinary Prism beams
;;; Contains functions used in Client only.
;;;
;;; At this writing (Sep 2001) Prism does not provide a multisegment beam
;;; class.  In the output list of Prism beams sent, some beams are
;;; interpreted as static beams, while other *consecutive sequences* of
;;; ordinary Prism beams are interpreted as dynamic (multisegment) beams.
;;;
;;; 01-Oct-2001  J. Jacky  calc-seg-info,segment-violations from dicom-panel.cl
;;; 08-Nov-2001  BobGian: Add missing IN-PACKAGE form.
;;; 27-Aug-2003 BobGian: Uniformize variable names in preparation
;;;   for adding Dose Monitoring Points.
;;; 03-Oct-2003 BobGian: Change defstruct name and slot names in SEG-REC-...
;;;   to SEGMENT-DESCRIPTOR-... to make DMP code more readable.
;;;   Ditto with a few local variables.
;;;   STATIC, DYNAMIC, SEGMENT (Prism pkg) -> Keyword pkg.
;;; 04-Oct-2003 BobGian: Rewrite CALC-SEG-INFO to add DMPs and
;;;   to improve understandability.
;;; 07-Oct-2003 BobGian: CALC-SEG-INFO input includes DMP list, which is passed
;;;   to new slot in SEGMENT-DESCRIPTOR object (shared with all segs in beam).
;;; 07-Oct-2003 BobGian: Move ADD-SEG-INFO "dicom-panel.cl -> here.
;;; 14-Nov-2003 BobGian: Fix bug: DMP list was not being pooled over all segs
;;;    belonging to a beam.
;;; 24-Nov-2003 BobGian: DMP auto-replication scheme altered.  In function
;;;    ADD-SEG-INFO, the DMP list is scanned, and shared DMPs [contributed to
;;;    by more than one beam] are replicated so that one shared DMP remains
;;;    [whose dose slot values are updated to be cumulative over all beams
;;;    sharing this DMP] and one new DMP is created for each beam in which the
;;;    shared DMP appears and which is contributed to only by that one beam.
;;;    Each auto-replicated DMP gets a name formed by concatenating the point
;;;    name and the beam name.  The newly-created, single-beam DMPs must be
;;;    placed AHEAD of the shared DMPs on the DMP list for each beam, so that
;;;    ASSEMBLE-FRACTION-GROUPS can calculate the correct value for the output
;;;    component 300A:0084 Beam Dose, which is based on dose at the norm point
;;;    DMP from a single beam rather than on the combined dose at the norm
;;;    point from all beams contributing to a DMP shared by multiple beams.
;;;    [See changelog notes, same date, in "dicom-panel" and "dicom-rtplan".]
;;; 25-Nov-2003 BobGian: Finished auto-replication of DMPs by ADD-SEG-INFO.
;;;    Needs additional DMP-CNT arg to allow counting while replicating.
;;; 26-Nov-2003 BobGian: Added DMP name creation via concatenation of Point
;;;    and Beam names for DMPs auto-replicated from a shared DMP but specific
;;;    to each beam separately.  Ditto calculation of their other slot values.
;;; 28-Nov-2003 BobGian: Move DMP defstruct "dicom-rtplan" -> here to
;;;    simplify dependencies.
;;; 04-Dec-2003 BobGian: SEGMENT-DESCRIPTOR-... -> SEGDATA-... (less clutter).
;;; 15-Dec-2003 BobGian: CALC-SEG-INFO needs <OrigBmInst> and <CurrBmInst>.
;;;    Fixed errors in beam segment dose accumulation and DMP auto-replication.
;;; 25-Dec-2002 BobGian: Flushed all "...OTHER-..." slots.  Now allocate a
;;;    separate DMP object for each segment in which the DMP appears, linking
;;;    them through the list in the DMP-SEGLIST slot of each so that dose can
;;;    be accumulated properly across all segments in a single beam.
;;; 28-Dec-2003 BobGian: DMP slots ...-dose -> ...-cGy to emphasize dose units.
;;;    Also auto-replication strategy changed - instead of replicating a shared
;;;    DMP as new non-shared ones, we now allocate a non-shared one each time
;;;    a point is selected for any beam, and then shared ones are created by
;;;    pooling the non-shared ones (by fcn ADD-SEG-INFO).
;;; 30-Dec-2003 BobGian, Mark Phillips: Decided on simplified design which
;;;    factors segment/beam packaging from beam/DMP allocation.  This greatly
;;;    simplifies ADD-SEG-INFO - no auto-replication, but user is free to
;;;    create DMPs with any subset of beams contributing their doses.
;;; 31-Dec-2003 BobGian: ADD-SEG-INFO no longer needs DMP-CNT - no replicating.
;;;    New strategy [per 30-Dec-2003 meeting with Mark P] implemented.
;;; 27-Jan-2004 BobGian began integration of new DMP Panel by Mark Phillips
;;;    with rest of Dicom Panel and interface to Dicom SCU.
;;; 12-Feb-2004 BobGian: ADD-SEG-INFO call chain no longer transfers DMPs to
;;;    ASSEMBLE-DICOM; information passed directly via passback from DMP panel.
;;; 19-Feb-2004 BobGian - introduced uniform naming convention explained below.
;;;    Includes: SEGDATA-... -> PR-BEAM-...
;;; 25-Feb-2004 BobGian - ADD-SEG-INFO -> GENERATE-PBEAM-INFO.
;;;    Also added PBEAM->DBEAM-GROUPER.
;;; 26-Feb-2004 BobGian: Completed DMP integration.
;;; 28-Feb-2004 BobGian: Fixed bug in PBEAM->DBEAM-GROUPER.
;;; 07-Mar-2003 BobGian: Added slots to DI-BEAM and DI-DMP and code to
;;;    PBEAM->DBEAM-GROUPER to track segment doses at each DMP properly,
;;;    to fix incorrect cumulative dose calculation when generating control
;;;    point sequence.
;;;    Removed PR-BEAM-TOTSEGS slot - last seg ascertained by position in list.
;;;    Removed PR-BEAM-SEGNUM slot - indexed via position in list.
;;;    Removed DI-DMP-COORDS slot - computed when needed.
;;; 10-Mar-2004 BobGian: DI-DMP-PRIOR-CGY and DI-DMP-TOTAL-CGY -> FIXNUM.
;;; 02-Apr-2004 BobGian: Updated comment explaining DI-DMP-TOTAL-CGY slot.
;;; 04-Apr-2004 BobGian: Change DI-DMP-TOTAL-CGY to record either computed
;;;    dose or user-textline-typed dose [using DI-DMP-DOSE-TYPE slot to
;;;    indicate which via value :Computed or :User, respectively].
;;; 30-Apr-2004 BobGian: Renamed a few fcn params and local vars to distinguish
;;;    more clearly between Original and Current Prism beam instances.
;;; 17-May-2004 BobGian complete process of extending all instances of Original
;;;    and Current Prism beam instances to include Copied beam instance too,
;;;    to provide copy for comparison with Current beam without mutating
;;;    Original beam instance.
;;; 26-May-2004 BobGian: Move SEGMENT-VIOLATIONS here -> "dicom-panel".
;;; 12-Sep-2004 BobGian: Rename PR-BEAM-CUM-MU slot to PR-BEAM-CUM-MU-INC
;;;    and add PR-BEAM-CUM-MU-EXC to hold cumulative MU for the beam Inclusive
;;;    and Exclusive (respectively) of the current segment.  Needed to provide
;;;    exactly repeating MU values on accumulating segment MU values.
;;; 05-Oct-2004 BobGian fixed a few lines to fit within 80 cols.
;;; 12-Oct-2004 BobGian DI-DMP-TREATED-CGY -> DI-DMP-PRIOR-CGY slot name change
;;;    for better consistency with Dicom-RT standard and Elekta documentation.
;;;    DI-DMP-TOTAL-CGY split into DI-DMP-ACCUM-CGY [part that accumulates dose
;;;    from current beams only] and DI-DMP-TOTAL-CGY [sum of accumulated
;;;    current dose and prior (previously-treated) dose] to fix inconsistency
;;;    between new revised specification and current implementation.
;;;

(in-package :prism)

;;;=============================================================
;;; Uniform naming convention.
;;;
;;; There are two kinds of Beams:
;;;
;;; A "Dicom" beam represents a single segment [in static or non-IMRT case]
;;; or a group of multiple segments [in dynamic IMRT case].  Each individual
;;; segment of such a Dicom beam is represented by what Prism calls an ordinary
;;; beam.  In the DMP source files, a "Prism" beam is synonymous with a
;;; "segment", and a "Dicom" beam is a group of segment beams treated as a
;;; single IMRT beam by the Dicom standard.
;;;
;;; Dose-Monitoring-Points are represented only by per-Dicom-beam structures
;;; encoding information about the DMP viewed as an object related to one or
;;; more Dicom beams.
;;;
;;; There are also three kinds of objects that need to be named consistently -
;;; class instance slot names, DEFSTRUCT accessor slot names, and local
;;; variables bound to the appropriate kinds of objects.
;;;
;;; The following naming convention distinguishes the possible cases
;;; [in some cases the leading/trailing "..." and hyphen might be null]:
;;;
;;;  Prism Beam:
;;;
;;;    Class slots are named PRISM-BEAM-...
;;;    DEFSTRUCT slots are named PR-BEAM-...
;;;    Local variables pointing to contents of either are named ...-P-BM-...
;;;    The Prism BEAM instance object by variables spelled ...-PBI-...
;;;      [for "Prism Beam Instance"].
;;;
;;;    The description "Prism Beam", objects/slots named PRISM-BEAM-...
;;;    or PR-BEAM-..., and local vars named ...-P-BM-... all refer to the
;;;    STRUCTURE described here.
;;;
;;;    The description "Prism Beam Instance", objects/slots named PRISM-BI-...
;;;    or ...-PRISM-BI, and local vars named ...-PBI all refer to the an
;;;    instance of the Prism BEAM object.
;;;
;;;    Prism beam instances [segments of Dicom beams] are one of three types:
;;;     Original - instances of Prism BEAM class objects, containing dose
;;;       information.  Indicated by prefix of "o" or "orig-" in names.
;;;     Copied - instances of copied Prism BEAM class objects, copied so that
;;;       side-effects to them do not mutate the original Prism beam object,
;;;       but not mutated by user.  Names use prefix "copy-".
;;;     Current - instances of copied Prism BEAM class objects, copied to
;;;       avoid side-effects to original Prism beam object.  These ARE
;;;       potentially mutated by user.  Names use prefix "curr-".
;;;     Sometimes other prefixes are used, like "new-", which are explained
;;;       in the context of their use.
;;;
;;;  Dicom Beam:
;;;
;;;    Class slots are named DICOM-BEAM-...
;;;    DEFSTRUCT slots are named DI-BEAM-...
;;;    Local variables pointing to contents of either are named ...-D-BM-...
;;;
;;;  DMP, Dicom beam:
;;;    Class slots are named DICOM-DMP-...
;;;    DEFSTRUCT slots are named DI-DMP-...
;;;    Local variables pointing to contents of either are named ...-D-DMP-...
;;;  [There is no DMP object for Prism beams.]
;;;
;;; Spelling conventions for local variables and slot names versus types
;;; of data objects to which they point:
;;;
;;;  O-BMDATA:
;;;    ( <Btn> <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <New?> <Color> )
;;;
;;;  OUTPUT-ALIST, O-ALIST, GENERATE-PBEAM-INFO in, PBEAM->DBEAM-GROUPER in:
;;;    List of O-BMDATA items.
;;;
;;;  P-BMDATA:
;;;    ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <PR-BEAM-Obj> )
;;;
;;;  P-BM-INFO, GENERATE-PBEAM-INFO output,
;;;    List of P-BMDATA items.
;;;
;;;  DICOM-BEAM-LIST, PBEAM->DBEAM-GROUPER output, D-BMLIST:
;;;    List of DI-BEAM structure instances.
;;;
;;;  BM-PAIR:  ( <CurrBmInst> <New-Bm?> )
;;;
;;;  BM-PAIR-LIST, CALC-SEG-INFO input:
;;;    List of BM-PAIR objects.
;;;
;;;  P-BMLIST, CALC-SEG-INFO output:
;;;    List of PR-BEAM structure instances.
;;;
;;;  PR-BEAM:           Structure defining a segment [a Prism beam as an
;;;                       element of a Dicom beam].
;;;  DI-BEAM:           Structure defining a Dicom beam [a group of segments,
;;;                       each a PR-BEAM instance].
;;;  DI-DMP:            Structure defining a Dicom Dose Monitoring Point.
;;;
;;;  P-BM-OBJ:          Instance of structure defining a segment [PR-BEAM].
;;;  D-BM-OBJ:          Instance of structure defining a DicomBmInst.
;;;  D-DMP-OBJ:         Instance of structure defining a DicomDMP.
;;;
;;;  DICOM-DMP-LIST:    List of <DicomDMP> objects.
;;;  D-DMP-LIST:        List of <DicomDMP> objects.
;;;
;;;  PLAN-ALIST:        List of ( <Btn> <Plan> ) items.
;;;  PRISM-BEAM-ALIST:  List of ( <Btn> <OrigBmInst> ) items.
;;;  D-DMP-ALIST:       List of ( <Btn> <DicomDMP> ) items.
;;;  D-BM-ALIST:        List of ( <Btn> <DicomBmInst> ) items.
;;;  DICOM-ALIST:       Multilevel list passed to Dicom client as output.

;;; Terminology note:
;;;
;;;   "Instance" in reference to a Prism beam, or the abbreviation "...BmInst",
;;;   means an instance of the Prism BEAM class [which represents a "segment"
;;;   of a Dicom beam.
;;;
;;;   The terms "BmObj", "Object", or "structure instance" refer to an instance
;;;   of a structure, not of the Prism BEAM class.

;;;=============================================================

(defstruct pr-beam
  segtype                                     ; :Static, :Dynamic, or :Segment
  dbeam-num                                         ;Fixnum
  seg-mu                                            ;Single-Float
  cum-mu-exc                                        ;Single-Float
  cum-mu-inc                                        ;Single-Float
  tot-mu)                                           ;Single-Float

;;; The PR-BEAM object represents data about the Prism-beam comprising
;;; each segment of a single or multiple-segment Dicom beam.
;;;
;;; The meaning of the fields is:

;;; SEGTYPE [symbol] ->
;;;  :STATIC indicates an ordinary [non-IMRT] beam [Dicom beam contains
;;;     a single Prism beam],
;;;  :DYNAMIC indicates the first segment in an IMRT segment sequence, and
;;;  :SEGMENT indicates other segments in the segment sequence.

;;; DBEAM-NUM [fixnum] - Dicom beam number, order of this static beam or
;;;  segment group in the output list, starting with 1.  All Prism beams that
;;;  belong to the same [static or dynamic] Dicom beam have the same
;;;  DBEAM-NUM [ie, shared by all segments in each Dicom beam].

;;; SEG-MU [single-float] - monitor units for this Prism beam [for this
;;;  segment, if type is :SEGMENT or :DYNAMIC].

;;; CUM-MU-EXC [single-float] - cumulative monitor units for all the
;;;  preceding segments in the current Dicom-beam segment sequence,
;;;  EXCLUSIVE of this segment.

;;; CUM-MU-INC [single-float] - cumulative monitor units for this segment plus
;;;  all the preceding segments in the current Dicom-beam segment sequence,
;;;  INCLUSIVE of this segment.

;;; TOT-MU [single-float] - total monitor units in all segments in the
;;;  current Dicom-beam segment sequence.  If type is :STATIC, TOT-MU = SEG-MU,
;;;  CUM-MU-EXC = 0.0, and TOT-MU = CUM-MU-INC must be true, otherwise
;;;  TOT-MU > SEG-MU should be true for all segments, TOT-MU > CUM-MU-EXC
;;;  should be true for all segments, and TOT-MU > CUM-MU-INC should be true
;;;  for all segments but the last, for which TOT-MU = CUM-MU-INC should hold.
;;;  Shared by all segments in a Dicom beam.

;;;-------------------------------------------------------------
;;; Structure for Dicom beams [grouped segments treated as single Dicom beam].

(defstruct di-beam
  name                                              ;Name of Dicom beam
  opbi-list  ;List of segs [uncopied Orig Prism Beam Instances] in Dicom beam.
  opbi-doses          ;List of dose sublists for each OPBI in this Dicom beam.
  )

;;; OPBI-LIST and OPBI-DOSES are parallel lists.  For each OPBI in OPBI-LIST,
;;; the corresponding element of OPBI-DOSES is the a list of point doses
;;; for that OPBI [segment].  That is, each sublist is a list of doses [actual
;;; dose, not dose/MU] for each point, one sublist for each segment, and list
;;; of doses parallel to list of points.  The slot therefore contains
;;; information about dose/MU at ALL points in the beam.
;;;
;;; Doses here are cGy as SMALL-FLOAT values.
;;;
;;; DI-BEAM-OPBI-DOSES [all for single DI-BEAM]:
;;;  ( ( Pt-1-dose Pt-2-dose ... Pt-N-dose )        <- Seg-1 or OPBI-1
;;;    ( Pt-1-dose Pt-2-dose ... Pt-N-dose )        <- Seg-2 or OPBI-2
;;;    ...
;;;    ( Pt-1-dose Pt-2-dose ... Pt-N-dose ) )      <- Seg-M or OPBI-M

;;;-------------------------------------------------------------
;;; Structure for Dose Monitoring Points - per-Dicom-beam version.

(defstruct di-dmp
  name                                              ;Name of Dicom DMP.

  point                     ;Prism MARK object [with Prism coordinates in cm].

  counter                                          ;Fixnum, incremented index.

  ;; Fixnum, prior or previously-treated dose, cGy, at this DMP.
  ;; This is dose administered prior to or outside the current treatment plan
  ;; and therefore is NOT due to any contribution from beams contributing to
  ;; this DMP.
  ;;
  prior-cGy

  ;; Fixnum, dose accumulated from all beams contributing to this DMP, in cGy.
  ;; Dose is total accumulated and not per fraction or per MU.
  ;;
  ;; The value may be calculated from Prism beam doses [which are then summed]
  ;; or may be typed in via the "Total dose: " textline.
  ;;
  ;; If typed, the amount by which the "Total dose: " textline typed value
  ;; exceeds the current PRIOR dose [the value in the PRIOR-CGY slot, which is
  ;; zero by default] is divided by the number of Dicom beams contributing to
  ;; this DMP to give the per-beam dose at this DMP.  The per-beam dose is then
  ;; divided by the number of segments for that beam to give the per-segment
  ;; dose for the beam.  The dose accumulated at the control point representing
  ;; a segment is the accumulated per-segment dose [divided by number of
  ;; fractions to give dose/fraction].  Typed total dose therefore assumes
  ;; equal contribution of dose to the DMP from each Dicom beam and equal
  ;; contribution from each segment to the total for the beam - it is NOT
  ;; weighted proportionally to Prism's calculated beam doses.
  ;;
  ;; The symbol in the DOSE-TYPE slot indicates which calculation is used
  ;; for the value in this and in the TOTAL-CGY slots.
  ;;
  accum-cGy

  ;; Fixnum, total dose accumulated from all beams contributing to this DMP
  ;; PLUS any non-zero PRIOR dose assigned to this DMP, in cGy.  Dose is total
  ;; accumulated and not per fraction or per MU.
  ;;
  ;; See comments immediately above for the ACCUM-CGY slot for a detailed
  ;; explanation of the meaning and method of calculation of the value
  ;; in this slot.
  ;;
  total-cGy

  ;; Indicates whether ACCUM-CGY and TOTAL-CGY are not yet computed [NIL],
  ;; computed from Prism beam doses [:Computed], or subdivided from value
  ;; typed into "Total dose:" textline [:User].
  ;;
  dose-type                                   ;Symbol [NIL, :Computed, :User].

  ;; DBEAMS and PDOSES are parallel lists.  For each Dicom beam in DBEAMS,
  ;; the corresponding element of PDOSES is a LIST of point doses [actual dose,
  ;; not dose per MU] at the point POINT, one element for each segment in the
  ;; Dicom beam.  Note that this slot's values describe doses at each segment
  ;; and at a SINGLE point, whereas the DI-BEAM-OPBI-DOSES slot values describe
  ;; doses at each segment but for ALL the available points [whether selected
  ;; for DMP duty or not].
  ;;
  ;; Doses here are cGy as SMALL-FLOAT values.
  ;; This is total dose [not dose/MU] over all fractions [not per-fraction].
  ;;
  ;; DI-DMP-PDOSES [all for single DMP]:
  ;;  ( ( OPbi-1-dose OPbi-2-dose ... OPbi-N-dose )         <- D-Beam-1
  ;;    ( OPbi-1-dose OPbi-2-dose ... OPbi-N-dose )         <- D-Beam-2
  ;;    ...
  ;;    ( OPbi-1-dose OPbi-2-dose ... OPbi-N-dose ) )       <- D-Beam-M
  ;;
  dbeams                        ;List of Dicom beams contributing to this DMP.
  ;;
  pdoses        ;Nested list of segment doses per beam and beam doses per DMP.

  )

;;;=============================================================

(defun pbeam->dbeam-grouper (o-alist &aux (segment-accumulator '())
			     (doses-accumulator '()) (outputlist '()))

  "pbeam->dbeam-grouper o-alist

Converts O-ALIST [an assoc list of all Prism beams, not segmented into Dicom
beams] into a list of Dicom beams by grouping the beams and creating a
Dicom-Beam structure for each group."

  ;; Input O-ALIST is list [in reverse order, guaranteed non-empty]
  ;; of objects, each:
  ;;  ( <Button> <OrigBmInst> <CopyBmInst> <CurrBmInst>
  ;;    <Plan> <New-Bm?> <SegColor> )
  ;;  <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
  ;;
  ;; This list contains all Prism beams - that is, all segments for all
  ;; Dicom beams, arranged in Dicom-beam segment order - all segments for
  ;; one Dicom beam followed by all segments for the next, and so forth.
  ;;
  ;; OrigBmInst is uncopied original Prism beam.
  ;; CopyBmInst and CurrBmInst are both copied beams so that changes
  ;; to their collimators will not side-effect real Prism beams.
  ;;
  ;; Output is a list of Dicom beam instances, each containing a list of Prism
  ;; beam instances.  "Prism Beam" passed through in that slot is an uncopied
  ;; Original-Prism-Beam instance.

  (declare (type list o-alist segment-accumulator
		 doses-accumulator outputlist))

  ;; Scanning O-ALIST in reverse order, so all successor-segments first are
  ;; pushed onto SEGMENT-ACCUMULATOR [which will then be in FORWARD order],
  ;; and then when first segment is encountered [sixth of O-BMDATA = T] we
  ;; push it and then push resulting Dicom beam [containing the accumulated
  ;; list of forward-ordered Original Prism beam instances] onto OUTPUTLIST
  ;; [which must be reversed before return].
  ;;
  ;; As we push each SEGMENT we also push its Point-dose-RESULT object
  ;; onto the parallel list DOSES-ACCUMULATOR [also now in forward order].
  ;;
  (dolist (o-bmdata o-alist)
    (let* ((orig-pbi (second o-bmdata))
	   (point-list (points (result orig-pbi)))
	   (opbi-mu (monitor-units orig-pbi)))
      (declare (type list point-list)
	       (type single-float opbi-mu))
      (cond ((sixth o-bmdata)      ;New-Bm? = T -> First segment of Dicom beam
	     (push orig-pbi segment-accumulator)    ;Include first segment
	     (push (mapcar #'(lambda (pt-dose/mu)   ;and doses.
			       (declare (type single-float pt-dose/mu))
			       (* pt-dose/mu opbi-mu))
		     point-list)
		   doses-accumulator)
	     ;; Make Dicom beam and save it in output.
	     ;; Doses here are cGy as SMALL-FLOAT values.
	     (push (make-di-beam :name (string-trim " " (name orig-pbi))
				 :opbi-list segment-accumulator
				 :opbi-doses doses-accumulator)
		   outputlist)
	     (setq segment-accumulator '())         ;Reset for next Dicom beam
	     (setq doses-accumulator '()))
	    ;; For each segment, save Original Prism beam instance and doses.
	    (t (push orig-pbi segment-accumulator)
	       (push (mapcar #'(lambda (pt-dose/mu)
				 (declare (type single-float pt-dose/mu))
				 (* pt-dose/mu opbi-mu))
		       point-list)
		     doses-accumulator)))))

  ;; Input O-ALIST was in reverse order, so PUSHes put OUTPUTLIST back forward.
  outputlist)

;;;-------------------------------------------------------------------------

(defun generate-pbeam-info (o-alist)

  "generate-pbeam-info o-alist

Converts O-ALIST [an assoc list of all Prism beams, not yet segmented into
Dicom beams] to forward-ordered list of one 4-item sublist per Prism beam.
Each output sublist is in form:
  ( OrigBmInst CopyBmInst CurrBmInst Plan Prism-Beam-Object )."

  ;; Input O-ALIST is list [in reverse order, guaranteed non-empty]
  ;; of objects, each:
  ;;  ( <Button> <OrigBmInst> <CopyBmInst> <CurrBmInst>
  ;;    <Plan> <New-Bm?> <SegColor> )
  ;;  <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
  ;;
  ;; OrigBmInst is uncopied original Prism beam.
  ;; CopyBmInst and CurrBmInst are both copied beams so that changes
  ;; to their collimators will not side-effect real Prism beams.
  ;;
  ;; This list contains all Prism beams - that is, all segments for all
  ;; Dicom beams, arranged int Dicom-beam order - all segments for one
  ;; Dicom beam followed by all segments for the next, and so forth.

  (declare (type list o-alist))

  ;; Destructive reversal does not destroy O-ALIST [list in OUTPUT-ALIST
  ;; slot of Dicom-Panel object] because NREVERSE acts on list newly-CONSed
  ;; by MAPCAR.
  (setq o-alist (nreverse (mapcar #'cdr o-alist)))

  ;; O-ALIST is list [now in forward order] of objects, each:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <New-Bm?> <SegColor> )
  ;;  <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
  ;;
  ;; Output is a forward-order list, one entry for each segment, each entry:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <Prism-Beam-Object> )

  (mapcar #'(lambda (b s)
	      (list (first b)                       ; OrigBmInst
		    (second b)                      ; CopyBmInst
		    (third b)                       ; CurrBmInst
		    (fourth b)                      ; Current Plan
		    s))                             ; Prism-Beam-Object
    o-alist
    (calc-seg-info
      (mapcar #'(lambda (b)
		  (list (third b)                   ; CurrBmInst
			(fifth b)))                 ; New-Bm? [T or NIL]
	o-alist))))

;;;-------------------------------------------------------------------------

(defun calc-seg-info (bm-pair-list)

  "calc-seg-info bm-pair-list

 Returns a list of PR-BEAM structure instances, one for each entry
in BM-PAIR-LIST."

  ;; Each entry in BM-PAIR-LIST is a list where the first element is the
  ;; current Prism beam instance, and second element is NIL if that beam is
  ;; a static beam or the initial segment in a segment sequence.  It is T if
  ;; that beam is a subsequent beam in a segment sequence.
  ;;
  ;; The BM-PAIR-LIST list is in FORWARD order [order beam segs were selected].
  ;;
  ;; Each BM-PAIR is:
  ;;  ( <CurrBmInst> <New-Bm?> )
  ;; <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
  ;; and BM-PAIR-LIST is a list of such pairs.
  ;;
  ;; CurrBmInst is copied beam so changes to its collimator will not
  ;; side-effect real Prism beam.

  (declare (type list bm-pair-list))

  (do ((p-bms bm-pair-list (cdr p-bms))
       (bnum 0)              ;order of current segment sequence in output list
       (mu-val 0.0)
       (cum-mu-exc 0.0)               ;cumulative MU excluding current segment
       (cum-mu-inc 0.0)               ;cumulative MU including current segment
       (p-bmlist '())
       (bm-pair) (new-beam?))
      ((null p-bms)
       ;; P-BMLIST is created in reverse order and reversed here for return.
       (nreverse p-bmlist))

    (declare (type list p-bms p-bmlist bm-pair)
	     (type (member nil t) new-beam?)
	     (type single-float mu-val cum-mu-exc cum-mu-inc)
	     (type fixnum bnum))

    (setq bm-pair (car p-bms)
	  mu-val (monitor-units (first bm-pair))
	  new-beam? (second bm-pair))

    (cond (new-beam?                               ;Starting a new Dicom beam.
	    (setq bnum (the fixnum (1+ bnum))
		  cum-mu-exc 0.0
		  cum-mu-inc mu-val))

	  (t (setq cum-mu-exc cum-mu-inc)       ;Adding segment to Dicom beam.
	     (setq cum-mu-inc (+ cum-mu-inc mu-val))

	     ;; Propagate slot values that are shared by all segments to the
	     ;; segments "earlier" in list of segments for this Dicom beam.
	     ;; Since P-BMLIST is currently in reverse order, "earlier" in
	     ;; sublist for a given segment actually occurs toward current
	     ;; tail of the list of items for that Dicom beam.

	     ;; P-BM-OBJ descriptors [BM-PAIR, MU-VAL, etc] are processed in
	     ;; FORWARD order.  P-BMLIST member objects are allocated and
	     ;; processed here in REVERSE order.  P-BMLIST is list of segment
	     ;; objects allocated [temporally] up to but not including the
	     ;; "current" one, which gets created by upcoming MAKE-PR-BEAM.
	     (dolist (p-bm-obj p-bmlist)
	       (setf (pr-beam-tot-mu p-bm-obj) cum-mu-inc)
	       (let ((seg-type (pr-beam-segtype p-bm-obj)))
		 ;; Beginning of sublist for this Dicom beam.
		 ;; Change type to :DYNAMIC.
		 (cond ((eq seg-type :static)
			(setf (pr-beam-segtype p-bm-obj) :dynamic)
			(return))
		       ;; Found beginning of sublist - done.
		       ((eq seg-type :dynamic)
			(return)))))))

    ;; Mark as :STATIC in case this is a singleton-segment beam.  If it is
    ;; first seg of multiseg sequence, this slot will be changed to :DYNAMIC.
    (push (make-pr-beam :segtype (if new-beam? :static :segment)
			:dbeam-num bnum            ;Increments each Dicom beam
			:seg-mu mu-val
			:cum-mu-exc cum-mu-exc
			:cum-mu-inc cum-mu-inc
			:tot-mu cum-mu-inc)
	  p-bmlist)))

;;;=============================================================
;;; End.
