;;;
;;; dose-grids
;;;
;;; Definitions of grid geometry object for the specification of dose
;;; information in Prism.
;;;
;;; 11-Oct-1993 J. Unger created from current implementation report.
;;; 29-Nov-1993 J. Unger remove dim attributes of grid-geometry and add
;;; functions to compute them; add density attribute to grid-geom + event.
;;; 22-Dec-1993 J. Unger add new-color to grid-geometry not-saved method.
;;;  3-Jan-1994 J. Unger change 'density' to 'voxel-size' in code.
;;; 14-Feb-1994 J. Unger remove default-initargs for origin & size of
;;; grid-geometry object, move constants defining fine, med, & coarse
;;; grid granularities here from dose-panels module, set voxel-size
;;; default initarg to coarse grid granularity.
;;; 14-Feb-1994 J. Unger add setf methods for grid-geometry size
;;; attrs to enforce a minimum size; add *minimum-grid-size* constant.
;;; 18-Feb-1994 J. Unger change values of dose grid granularity.
;;; 18-Feb-1994 D. Nguyen add copy-grid-geometry
;;; 18-Apr-1994 I. Kalet split off from dose-objects, change name of
;;; module above, change events to just one, don't announce it here.
;;; 12-May-1994 I. Kalet move globals to prism-globals.
;;; 13-Jun-1994 I. Kalet take out message from copy-dose-grid.
;;; 16-Jun-1994 I. Kalet change color to display-color.
;;; 23-Feb-1995 I. Kalet provide default initargs for origin and size.
;;;  4-Jun-1996 I. Kalet change copy-grid-geometry to method for copy.
;;; 16-Jul-1998 I. Kalet add a default name for grid-geometry object.
;;; 21-Feb-2000 I. Kalet take out rest pars in copy method.
;;; 11-Jun-2001 BobGian replace type-specific arithmetic macros with THE
;;;   declarations in X-DIM, Y-DIM, and Z-DIM.
;;; 15-Mar-2003 BobGian add THE decls - allows TRUNCATE to inline.
;;; 21-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defclass grid-geometry (generic-prism-object)

  ((x-origin :type single-float
	     :accessor x-origin
	     :initarg :x-origin
	     :documentation "The x-coordinate of the grid's origin.")

   (y-origin :type single-float
	     :accessor y-origin
	     :initarg :y-origin
	     :documentation "The y-coordinate of the grid's origin.")

   (z-origin :type single-float
	     :accessor z-origin
	     :initarg :z-origin
	     :documentation "The z-coordinate of the grid's origin.")

   (x-size :type single-float
	   :reader x-size
	   :initarg :x-size
	   :documentation "The size of the grid in the x direction.")

   (y-size :type single-float
	   :reader y-size
	   :initarg :y-size
	   :documentation "The size of the grid in the y direction.")

   (z-size :type single-float
	   :reader z-size
	   :initarg :z-size
	   :documentation "The size of the grid in the z direction.")

   (voxel-size :type single-float
	       :accessor voxel-size
	       :initarg :voxel-size
	       :documentation "The linear measure (ie: length, width,
and height) of a single voxel of the specified grid - voxels are always
regular cubes for now.")

   (display-color :type symbol
		  :accessor display-color
		  :initarg :display-color
		  :documentation "The color of the grid geometry as it
appears in any views.")

   (new-coords :type ev:event
	       :accessor new-coords
	       :initform (ev:make-event)
	       :documentation "Announced when the origin or size
changes, but not by code here.  It must be announced by code that sets
these attributes.  This is to be able to avoid inefficiency due to
multiple updates.")

   (new-voxel-size :type ev:event
		   :accessor new-voxel-size
		   :initform (ev:make-event)
		   :documentation "Announced when the voxel-size changes.")

   (new-color :type ev:event
	      :accessor new-color
	      :initform (ev:make-event)
	      :documentation "Announced when the display-color changes.")

   )

  (:default-initargs :x-origin -10.0 :y-origin -10.0 :z-origin -10.0
		     :x-size 20.0 :y-size 20.0 :z-size 20.0
		     :voxel-size *coarse-grid-size*
		     :name "Dose grid"
		     :display-color 'sl:yellow)

  (:documentation "A grid geometry specifies the origin, size, and dimensions
of a three-dimensional grid of dose samples.")

  )

;;;---------------------------------------------

(defmethod not-saved ((g grid-geometry))

  (append (call-next-method)
	  '(name new-coords new-voxel-size new-color)))

;;;---------------------------------------------

(defmethod (setf x-size) (val (g grid-geometry))

  (setf (slot-value g 'x-size) (max val *minimum-grid-size*)))

;;;---------------------------------------------

(defmethod (setf y-size) (val (g grid-geometry))

  (setf (slot-value g 'y-size) (max val *minimum-grid-size*)))

;;;---------------------------------------------

(defmethod (setf z-size) (val (g grid-geometry))

  (setf (slot-value g 'z-size) (max val *minimum-grid-size*)))

;;;---------------------------------------------

(defmethod (setf voxel-size) :after (val (g grid-geometry))

  (ev:announce g (new-voxel-size g) val))

;;;---------------------------------------------

(defmethod (setf display-color) :after (val (g grid-geometry))

  (ev:announce g (new-color g) val))

;;;-------------------------------------------------------------

(defmethod x-dim ((g grid-geometry))

  (the fixnum (1+ (the fixnum
		    (truncate (/ (the single-float (x-size g))
				 (the single-float (voxel-size g))))))))

;;;-------------------------------------------------------------

(defmethod y-dim ((g grid-geometry))

  (the fixnum (1+ (the fixnum
		    (truncate (/ (the single-float (y-size g))
				 (the single-float (voxel-size g))))))))

;;;-------------------------------------------------------------

(defmethod z-dim ((g grid-geometry))

  (the fixnum (1+ (the fixnum
		    (truncate (/ (the single-float (z-size g))
				 (the single-float (voxel-size g))))))))

;;;---------------------------------------------

(defun make-grid-geometry (&rest initargs)

  "MAKE-GRID-GEOMETRY &rest initargs

Returns a grid-geometry object with specified parameters."

  (apply #'make-instance 'grid-geometry initargs))

;;;---------------------------------------------

(defmethod copy ((g grid-geometry))

  "Copies and returns a grid-geometry object."

  (declare (ignore pars))
  (make-grid-geometry :x-origin (x-origin g)
		      :y-origin (y-origin g)
		      :z-origin (z-origin g)
		      :x-size (x-size g)
		      :y-size (y-size g)
		      :z-size (z-size g)
		      :voxel-size (voxel-size g)
		      :display-color (display-color g)))

;;;---------------------------------------------
