;;;
;;; volume-mediators
;;;
;;; defines mediator for update of contoured volume objects in views
;;;
;;;  3-Sep-1993 I. Kalet split off from volumes module
;;; 14-Aug-2002 J. Sager modify for room-view
;;; 22-Sep-2002 I. Kalet simplify event registrations.
;;; 13-Oct-2002 I. Kalet clear the triangular mesh cache for room-view
;;; when new contour announced.
;;; 25-May-2009 I. Kalet remove ref to room-view
;;;

(in-package :prism)

;;;--------------------------------------

(defclass pstruct-view-mediator (object-view-mediator)

  ()

  (:documentation "This mediator connects a pstruct with a view.")
  )

;;;--------------------------------------

(defmethod initialize-instance :after ((pvm pstruct-view-mediator)
				       &rest initargs)
  (declare (ignore initargs))
  (ev:add-notify pvm (new-color (object pvm))
		   #'update-view)
  (ev:add-notify pvm (new-contours (object pvm))
		 #'update-view))

;;;--------------------------------------

(defmethod destroy :after ((pvm pstruct-view-mediator))

  (ev:remove-notify pvm (new-contours (object pvm)))
  (ev:remove-notify pvm (new-color (object pvm))))

;;;--------------------------------------

(defun make-pstruct-view-mediator (pstruct view)

  (make-instance 'pstruct-view-mediator :object pstruct :view view))

;;;--------------------------------------
