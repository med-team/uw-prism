;;;
;;; clipper
;;;
;;; 22-Jan-1998 BobGian move all polygon clipping code here from file
;;;  "pathlength.cl".  Add declarations for more speed (inlining).  Use FLET
;;;  to declare local functions avoiding necessity of passing large arg lists.
;;; 09-Mar-1998 BobGian minor update with new version of dose-calc code.
;;; 22-May-1998 BobGian:
;;;   - Convert throughout to pass flonum args via Arg-Vec
;;;       (as in COMPUTE-BEAM-DOSE and PATHLENGTH).
;;;   - convert CNODE from DEFSTRUCT to array with named slots.
;;;   - INTERPOLATE-CROSSING: function -> macro (inlined).
;;;   - GRAZER?, PUSHNODE, SINGLE-CROSSPOINT, DUAL-CROSSPOINTS: convert
;;;       internal (FLET) definitions to ordinary function using Arg-Vec
;;;       to pass args are return values(s).
;;; 20-Jul-1998 BobGian optimize a few more array declarations and accessors.
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization).
;;; 08-Feb-2000 BobGian more cosmetic cleanup.
;;; 02-Mar-2000 BobGian add declarations to CLIPBLK-CONTOURS.
;;; 02-Nov-2000 BobGian function name and argument order changes to make
;;;   consistent with new version of dose-calc used in electron code.
;;; 30-May-2001 BobGian:
;;;   Wrap generic arithmetic with THE-declared types.
;;;   Move symbols used only as tags from Prism to Keyword package.
;;;   MOD -> LOGAND in order to enable inlining.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :prism)

;;;=============================================================
;;; Polygon-Clipping code.  Based loosely on Sutherland-Hodgman algorithm
;;; [see COMPUTER GRAPHICS, 2nd Ed in C, Foley, van Dam, Feiner, and Hughes,
;;; pp. 124-127 and 924-937].  There are also comments in file "beam-dose"
;;; relating to polygon clipping and its iterface with the rest of the dose
;;; calculation.

(defmacro interpolate-crossing (ic-1 ic-2 bc-1 bound bc-2)
  ;;
  ;; Parameters:
  ;;    IC-1  - Interpolated Coordinate, Vertex 1
  ;;    IC-2  - Interpolated Coordinate, Vertex 2
  ;;    BC-1  - Bound Coordinate [one defining boundary], Vertex 1
  ;;    BOUND - Boundary [value of Bound Coordinate]
  ;;    BC-2  - Bound Coordinate [one defining boundary], Vertex 2
  ;;
  ;; All args should be compile-time symbols to avoid multiple evaluation.
  ;; All should be declared SINGLE-FLOAT in calling context.
  ;;
  ;; IC can be X or Y coordinate and BC is Y or X coordinate respectively.
  ;; BOUND is value of BC coordinate at crossing boundary.
  ;; Either of Vertex 1 or 2 can be "lower" or "upper" vertex, as long as
  ;; they are consistent between the two.
  ;;
  ;; Value returned is the interpolated value of the IC coordinate.
  ;; Note that BC coordinates need not strictly straddle BOUND - at most
  ;; one of them can EQUAL BOUND.  BOTH CANNOT, or division-by-zero results.
  ;;
  `(cond
     ;;
     #+ignore
     ((= (the single-float ,bc-1) (the single-float ,bc-2))
      ;; Safety - should never occur, but better than crashing machine.
      (error "INTERPOLATE-CROSSING [1] Zero-length crossing."))
     ;;
     ;; Next two cases are to return EXACTLY correct value so testing
     ;; for vertex equality via border node coordinate equality will
     ;; work using internal flonum-equality test.  Fringe benefit: also
     ;; speeds up interpolation of rare border node case.
     ((= (the single-float ,bc-1) (the single-float ,bound))
      (the single-float ,ic-1))
     ((= (the single-float ,bc-2) (the single-float ,bound))
      (the single-float ,ic-2))
     ;;
     ;; Usual case of interpolation for portal boundary strict crossing.
     ;; Due to oddities of flonum arithmetic, we arrange the following
     ;; calculation in either of two mathematically-equivalent ways
     ;; [when using REAL numbers] to get the better FLONUM approximation.
     ;; The idea is to interpolate FROM the point CLOSEST to the boundary.
     ;; In cases where one point is extremely close to the boundary and
     ;; the other is "far" away, this results in smaller roundoff error.
     ;;
     ((< (the single-float
	   (abs (- (the single-float ,bound) (the single-float ,bc-1))))
	 (the single-float
	   (abs (- (the single-float ,bound) (the single-float ,bc-2)))))
      (+ (/ (* (- (the single-float ,ic-2) (the single-float ,ic-1))
	       (- (the single-float ,bound) (the single-float ,bc-1)))
	    (- (the single-float ,bc-2) (the single-float ,bc-1)))
	 (the single-float ,ic-1)))
     ;;
     (t (+ (/ (* (- (the single-float ,ic-1) (the single-float ,ic-2))
		 (- (the single-float ,bound) (the single-float ,bc-2)))
	      (- (the single-float ,bc-1) (the single-float ,bc-2)))
	   (the single-float ,ic-2)))))

;;;-------------------------------------------------------------

(defun clip-blocks (vlist arg-vec &aux (chain nil) (enterlist '()))
  ;;
  ;; CHAIN is Defstruct-chain of CNODEs representing subcontour being built.
  ;; ENTERLIST is list of ENTER nodes not yet absorbed into subcontours.
  ;;
  ;; Takes a list VLIST of vertices [2-lists, X/Y coords in collimator system,
  ;; at isocenter, traversed in either direction] and portal coordinates
  ;; [XCI-, XCI+, YCI-, and YCI+ as single-floats].  Returns LIST of clipped
  ;; subcontours, that is, NIL for none or list of one or more - each as a
  ;; vertex list traversed in CCW direction.
  ;;
  (declare (type list vlist)
	   (type (simple-array single-float (#.Argv-Size)) arg-vec))
  ;;
  ;; Setup ... Make sure VLIST is traversed in CLOCKWISE direction.
  (unless (poly:clockwise-traversal-p vlist)
    (setq vlist (reverse vlist)))
  ;;
  (let ((xci- (aref arg-vec #.Argv-Xci-))
	(xci+ (aref arg-vec #.Argv-Xci+))
	(yci- (aref arg-vec #.Argv-Yci-))
	(yci+ (aref arg-vec #.Argv-Yci+)))
    ;;
    (declare (type single-float xci- xci+ yci- yci+))
    ;;
    ;; A CNODE represents a contour VERTEX almost isomorphically, except for
    ;; two aspects.  Firstly, a vertex which lies exactly on the portal border,
    ;; with both line segments extending into the portal interior, might be
    ;; represented by two nodes.  It will if the vertex is the single point
    ;; of intersection of two subcontours - one node represents the vertex
    ;; in each of the two subcontours [one as a LEAVE node, other as an ENTER].
    ;; If the border point is on a single contour, a single node represents it,
    ;; considered and INSIDE node, and this case is the sole exception that
    ;; vertices on the border are considered OUTSIDE the portal.
    ;;
    ;; Secondly, vertices outside the portal are not represented at all.
    ;; We represent their order in the vertex sequence via the fact that we
    ;; allocate all nodes in the same order as we encounter vertices in a
    ;; clockwise contour traversal.
    ;;
    ;; CHAIN points to an arbitrary entry point in the circular chain of
    ;; nodes representing input contour.  PUSHNODE updates CHAIN to the last
    ;; node allocated.
    ;;
    ;; First vertex traversal, to find all points where segments cross
    ;; portal edges.  Note that ON THE BORDER counts as OUTSIDE.  Thus
    ;; a segment counts as crossing in the ENTER direction when it enters
    ;; the interior from the strict outside or from a border point.  Likewise,
    ;; a segment counts as crossing in the LEAVE direction when it leaves
    ;; the interior to the strict outside or to a border point.  All segments
    ;; lying strictly along a border count as OUTSIDE.  Single exception is
    ;; described in next comment - case of "interior" vertex grazing border.
    ;;
    ;; FWD- means vertex at target end of segment, in direction of traversal.
    ;; BCK- means vertex at other end, at source end of traversal
    ;;
    (let* ((bck-vert (car (last vlist)))
	   (bnx (first bck-vert))
	   (bny (second bck-vert)))
      ;;
      (declare (type single-float bnx bny))
      ;;
      (do ((fwd-verts vlist (cdr fwd-verts))        ;List of verts - CDRed
	   (fwd-vert)                               ;Actual Vertex
	   (fx 0.0) (fy 0.0)                     ;Rotating coords of FWD point
	   (bx bnx fx)                          ;Rotating X coord of BCK point
	   (by bny fy)                          ;Rotating Y coord of BCK point
	   (f-inside?)                             ;FWD point Strictly-Inside?
	   ;;
	   (b-inside?
	     (or (and (< xci- bnx xci+)             ;BCK point Strictly-Inside
		      (< yci- bny yci+))
		 ;;
		 ;; Or BCK-VERT is a GRAZER [see fcn GRAZES?].  Must
		 ;; examine BCK-VERT [last vertex in VLIST] and the
		 ;; vertices which come just before it [next-to-last
		 ;; in VLIST] and just after it [first in VLIST].
		 ;;
		 (let ((fn (car vlist))             ;Vertex AFTER BCK-VERT
		       (bbn (car (last (butlast vlist)))))  ;One BEFORE
		   (setf (aref arg-vec #.Argv-Bx) (first bbn))
		   (setf (aref arg-vec #.Argv-By) (second bbn))
		   (setf (aref arg-vec #.Argv-Cx) bnx)
		   (setf (aref arg-vec #.Argv-Cy) bny)
		   (setf (aref arg-vec #.Argv-Nx) (first fn))
		   (setf (aref arg-vec #.Argv-Ny) (second fn))
		   (grazer? arg-vec)))
	     f-inside?))
	  ;;
	  ((null fwd-verts))
	;;
	(declare (type (member nil t) f-inside? b-inside?)
		 (type single-float bx by fx fy))
	;;
	(setq fwd-vert (car fwd-verts)
	      fx (first fwd-vert)
	      fy (second fwd-vert)
	      f-inside? (and (< xci- fx xci+)
			     (< yci- fy yci+)))
	;;
	(cond
	  ((or f-inside?
	       ;;
	       ;; Either FWD vertex really is INSIDE, or it is a GRAZER.  See
	       ;; function GRAZER? for explanation.  If a grazer, we treat it
	       ;; as an INSIDE node.  Otherwise we treat it as OUTSIDE,
	       ;; inducing a border crossing, resulting in allocation of two
	       ;; nodes [a LEAVE and an ENTER] with identical coordinates, both
	       ;; representing the same vertex, which is a single point of
	       ;; tangency between two otherwise non-intersecting subcontours.
	       ;;
	       (let ((next (or (second fwd-verts)   ;Vertex AFTER current one
			       (car vlist))))
		 ;; FX, FY is current vertex; BX, BY is one just BEFORE it
		 (setf (aref arg-vec #.Argv-Bx) bx)
		 (setf (aref arg-vec #.Argv-By) by)
		 (setf (aref arg-vec #.Argv-Cx) fx)
		 (setf (aref arg-vec #.Argv-Cy) fy)
		 (setf (aref arg-vec #.Argv-Nx) (first next))
		 (setf (aref arg-vec #.Argv-Ny) (second next))
		 (grazer? arg-vec)))
	   ;;
	   ;; Either F-INSIDE? was already true, or we detect special case and
	   ;; treat it so.  Must set F-INSIDE? so current node will be treated
	   ;; correctly on next iteration.
	   (setq f-inside? t)
	   ;;
	   (cond (b-inside?
		   ;; FWD inside, BCK inside: push new FWD node.
		   (setf (aref arg-vec #.Argv-Vx) fx)
		   (setf (aref arg-vec #.Argv-Vy) fy)
		   (setq chain (pushnode arg-vec :Inside chain)))
		 ;;
		 ;; FWD inside, BCK outside: push crossing point and FWD node.
		 (t (setf (aref arg-vec #.Argv-Ix) fx)
		    (setf (aref arg-vec #.Argv-Iy) fy)
		    (setf (aref arg-vec #.Argv-Ox) bx)
		    (setf (aref arg-vec #.Argv-Oy) by)
		    (single-cross arg-vec)
		    ;; XCOORD and YCOORD are in slots 0,1 in ARG-VEC,
		    ;; placed there as return values by SINGLE-CROSS.
		    (setq chain (pushnode arg-vec :Enter chain))
		    (push chain enterlist)
		    (setf (aref arg-vec #.Argv-Vx) fx)
		    (setf (aref arg-vec #.Argv-Vy) fy)
		    (setq chain (pushnode arg-vec :Inside chain)))))
	  ;;
	  (b-inside?
	    ;; FWD outside, BCK inside: push LEAVE node at outgoing crossing.
	    (setf (aref arg-vec #.Argv-Ix) bx)
	    (setf (aref arg-vec #.Argv-Iy) by)
	    (setf (aref arg-vec #.Argv-Ox) fx)
	    (setf (aref arg-vec #.Argv-Oy) fy)
	    (single-cross arg-vec)
	    ;; XCOORD and YCOORD are already in slots 0,1 in ARG-VEC,
	    ;; placed there as return values by SINGLE-CROSS.
	    (setq chain (pushnode arg-vec :Leave chain)))
	  ;;
	  ;; Both FWD point and BCK point are OUTSIDE [strict or border].
	  ;; Intersections are possible but not necessary - see if they
	  ;; occur.  Note that if both ends are OUTSIDE and no portal
	  ;; intersections occur, we don't PUSHNODE anything.
	  (t (setf (aref arg-vec #.Argv-Ix) bx)
	     (setf (aref arg-vec #.Argv-Iy) by)
	     (setf (aref arg-vec #.Argv-Ox) fx)
	     (setf (aref arg-vec #.Argv-Oy) fy)
	     (let ((crossed? (dual-cross arg-vec)))
	       (let ((xe (aref arg-vec #.Argv-Xe))
		     (ye (aref arg-vec #.Argv-Ye))
		     (xl (aref arg-vec #.Argv-Xl))
		     (yl (aref arg-vec #.Argv-Yl)))
		 (declare (type (member nil t) crossed?)
			  (type single-float xe ye xl yl))
		 (when (and crossed?
			    (not (or (and (= xe xl)
					  (or (= xe xci-)
					      (= xe xci+)))
				     (and (= ye yl)
					  (or (= ye yci-)
					      (= ye yci+))))))
		   ;;
		   ;; There must be ZERO or TWO crossings.  If two crossings,
		   ;; either or both might be border points, but each must be
		   ;; either ENTERing or LEAVEing with respect to interior.
		   ;; The NOT filters out line segments which skim along a
		   ;; border or nick a corner without entering the interior.
		   ;;
		   ;; XE and YE are already in slots 0,1 in ARG-VEC,
		   ;; placed there as return values by DUAL-CROSS.
		   (setq chain (pushnode arg-vec :Enter chain))
		   (push chain enterlist)
		   (setf (aref arg-vec #.Argv-Vx) xl)
		   (setf (aref arg-vec #.Argv-Vy) yl)
		   (setq chain (pushnode arg-vec :Leave chain)))))))))
    ;;
    (cond
      ((null chain)
       ;;
       ;; No nodes pushed means all vertices on contour are OUTSIDE portal.
       ;; Either it totally encloses portal, so any point inside portal must
       ;; be enclosed; or it totally excludes portal, so any point inside
       ;; portal must be NOT enclosed.  Use portal center as the testpoint.
       ;;
       (setf (aref arg-vec #.Argv-Enc-X) (* 0.5 (+ xci- xci+)))
       (setf (aref arg-vec #.Argv-Enc-Y) (* 0.5 (+ yci- yci+)))
       (and (encloses? vlist arg-vec)
	    ;;
	    ;; Contour encloses Portal: Return [list of] portal itself - CCW.
	    ;; Otherwise - no enclosed contour - return NIL.
	    ;;
	    (list (list (list xci- yci-)
			(list xci+ yci-)
			(list xci+ yci+)
			(list xci- yci+)))))
      ;;
      ((null enterlist)
       ;;
       ;; No border crossings - contour must be totally INSIDE portal.
       ;; Return list of COUNTER-CLOCKWISE input vertex list.
       (list (reverse vlist)))
      ;;
      ;; Contour is neither totally outside nor totally inside portal - it
      ;; must cross border at least TWICE.  Start a sweep with each ENTER
      ;; node in turn and trace around subcontour it initiates, pushing result
      ;; onto OUTLIST.
      ;;
      ;; Any LEAVE node encountered initiates a search for the nearest [in CW
      ;; direction around portal] ENTER node - which could be the starting
      ;; point [in which case we are done with this subcontour] or might be
      ;; another ENTER node not yet encountered.
      ;;
      ;; When no ENTER nodes remain on ENTERLIST we are finished with the
      ;; entire traversal, and we can return from function with OUTLIST.
      ;;
      (t (do ((starter (car enterlist) (car enterlist))
	      (accumulator '() '())
	      (outlist '()))
	     (( ))
	   ;;
	   ;; Only enter this loop if ENTERLIST is non-empty;
	   ;; thus STARTER must be a valid node [not NIL].
	   (do ((curr starter (svref (the (simple-array t (#.Cnode-Size)) curr)
				     #.Cnode-Next))
		(flag? nil t))
	       ((and flag? (eq curr starter))
		;; Ie, we swept around without finding any LEAVE nodes.
		(error "CLIP-BLOCKS [1] Sweep in infinite loop."))
	     ;;
	     (declare (type (member nil t) flag?))
	     ;;
	     (push (list (svref (the (simple-array t (#.Cnode-Size)) curr)
				#.Cnode-Xci)
			 (svref (the (simple-array t (#.Cnode-Size)) curr)
				#.Cnode-Yci))
		   accumulator)
	     ;;
	     ;; Since we start with an ENTER, we can encounter ONLY nodes of
	     ;; type INSIDE before coming to a LEAVE [which we MUST come to
	     ;; eventually].  At this point we search for the corresponding
	     ;; ENTER point - closing the current subcontour or continuing
	     ;; on it if the ENTER node is other than our starting point.
	     ;;
	     (when (eq (svref (the (simple-array t (#.Cnode-Size)) curr)
			      #.Cnode-Type)
		       :Leave)
	       ;;
	       ;; Find ENTER node nearest-clockwise to LEAVE node.
	       (let ((leavecode
		       (svref (the (simple-array t (#.Cnode-Size)) curr)
			      #.Cnode-Code))
		     (leave-X
		       (svref (the (simple-array t (#.Cnode-Size)) curr)
			      #.Cnode-Xci))
		     (leave-Y
		       (svref (the (simple-array t (#.Cnode-Size)) curr)
			      #.Cnode-Yci))
		     (enternode nil)
		     (entercode 0)
		     (enterdiff 100)    ;"Infinite" so first test will succeed
		     (enter-X 0.0)
		     (enter-Y 0.0)
		     (testcode 0)
		     (testdiff 0)
		     (test-X 0.0)
		     (test-Y 0.0))
		 ;;
		 (declare (type single-float leave-X leave-Y
				enter-X enter-Y test-X test-Y)
			  (type fixnum leavecode entercode testcode
				enterdiff testdiff))
		 ;;
		 ;; Test against all the nodes on ENTERLIST.
		 (dolist (testnode enterlist)
		   (setq testcode (svref (the (simple-array t (#.Cnode-Size))
					   testnode)
					 #.Cnode-Code)
			 ;; (LOGAND x 7) = (MOD x 8), but it inlines.
			 testdiff (logand (the fixnum
					    (- testcode leavecode)) 7)
			 test-X (svref (the (simple-array t (#.Cnode-Size))
					 testnode)
				       #.Cnode-Xci)
			 test-Y (svref (the (simple-array t (#.Cnode-Size))
					 testnode)
				       #.Cnode-Yci))
		   ;;
		   ;; If we go all the way around CW from LEAVE before getting
		   ;; to the test ENTER, the value of TESTDIFF must be 8 rather
		   ;; than 0 to indicate this fact.  This can happen only for
		   ;; non-corner vertices, since at same corner two vertices
		   ;; must be identical, and we rule out this possibility in
		   ;; searching from a LEAVE node to the nearest-clockwise
		   ;; non-identically located ENTER node.
		   (when (and (= testcode leavecode)
			      (case testcode
				(0)                 ;Corner - do nothing
				(1 (< test-Y leave-Y))
				(2)                 ;Corner - do nothing
				(3 (< test-X leave-X))
				(4)                 ;Corner - do nothing
				(5 (> test-Y leave-Y))
				(6)                 ;Corner - do nothing
				(7 (> test-X leave-X))))
		     (setq testdiff 8))
		   ;;
		   ;; Closing ENTER node must represent a vertex distinct from
		   ;; that represented by LEAVE node - thus at least one
		   ;; coordinate value must differ.
		   ;;
		   ;; It is OK for two nodes to have equal-valued coordinates,
		   ;; but this represents a single vertex which belongs to two
		   ;; different subcontours [single-point intersection case].
		   ;; The NODES representing the shared vertex will be
		   ;; allocated to different subcontours by this algorithm.
		   ;; That's why we don't allow equality match here.
		   ;;
		   (when (and (or (/= leave-X test-X)
				  (/= leave-Y test-Y))
			      ;;
			      ;; TESTNODE and ENTERNODE can represent vertices
			      ;; each on a different edge, one on an edge and
			      ;; other in a corner, each in a different corner,
			      ;; both on same edge but with different degrees
			      ;; of rotation [in one we pass no corners going
			      ;; CW from LEAVE to ENTER, in other we pass all
			      ;; 4 corners].  In all such cases, TESTDIFF and
			      ;; ENTERDIFF must differ, and the comparison
			      ;; below selects the smaller.
			      ;;
			      ;; OR ... both vertices can be on the SAME edge
			      ;; with the same degree of rotation [zero or four
			      ;; corners], and thus TESTDIFF = ENTERDIFF and
			      ;; TESTCODE = ENTERCODE.  In this case we must
			      ;; compare coordinate values to determine the
			      ;; minimal point - using different comparisons
			      ;; for each edge!  TESTCODE and ENTERCODE must
			      ;; be ODD in this case, since both vertices are
			      ;; on an EDGE, not at a corner.  [They can't both
			      ;; be at the same corner because then they would
			      ;; would have to be identical, and we can't have
			      ;; multiple identical ENTERing vertices.
			      ;;
			      (or (< testdiff enterdiff)
				  ;; Easy case - different edges/corners.
				  ;;
				  (and (= testdiff enterdiff)
				       ;; Different points on same edge and
				       ;; same degree of CW rotation.
				       ;;
				       (cond
					 ;;
					 ;;Left edge - Y increasing CW.
					 ((= testcode 1)
					  (< test-Y enter-Y))
					 ;;
					 ;;Top edge - X increasing CW.
					 ((= testcode 3)
					  (< test-X enter-X))
					 ;;
					 ;;Right edge - Y decreasing CW.
					 ((= testcode 5)
					  (> test-Y enter-Y))
					 ;;
					 ;;Bottom edge - X decreasing CW.
					 ((= testcode 7) ;Bottom: decreasing X
					  (> test-X enter-X))
					 ;;
					 (t (error "CLIP-BLOCKS [2]"))))))
		     ;;
		     ;; We have found a "better" ENTER node - closer in CW
		     ;; direction to the starting LEAVE node.  Save it and its
		     ;; associated values.
		     (setq enternode testnode
			   entercode testcode
			   enterdiff testdiff
			   enter-X test-X
			   enter-Y test-Y)))
		 ;;
		 (unless enternode
		   ;;
		   ;; Highly unlikely but possible case happened - due to
		   ;; roundoff the only ENTER node pushed happens to have the
		   ;; same coordinates as the LEAVE node from which this search
		   ;; started.  Since only one ENTER node was pushed, there
		   ;; could have been only one LEAVE node too.  Thus contour
		   ;; must just barely clip the border in single point [within
		   ;; roundoff].  Either the contour must surround portal or
		   ;; portal and contour must be disjoint.  Determine which and
		   ;; return immediately.
		   ;;
		   (setf (aref arg-vec #.Argv-Enc-X) (* 0.5 (+ xci- xci+)))
		   (setf (aref arg-vec #.Argv-Enc-Y) (* 0.5 (+ yci- yci+)))
		   (return-from clip-blocks
		     ;;
		     ;; Does contour VLIST enclose portal's midpoint?
		     (and (encloses? vlist arg-vec)
			  ;;
			  ;; If NO, portal/contour are disjoint: return NIL.
			  ;; If YES, contour contains portal: return list
			  ;;   of vertices as a CCW portal traversal.
			  ;;
			  (list (list (list xci- yci-)
				      (list xci+ yci-)
				      (list xci+ yci+)
				      (list xci- yci+))))))
		 ;;
		 ;; Found ENTER node.  See if we have rounded any corners.
		 (when (or (< entercode leavecode) ;On different edges, or ...
			   (= enterdiff 8))   ;same edge, wrap all way around.
		   ;;
		   ;; Wrapped around (XCI- YCI-) corner [and possibly others].
		   ;; Incrementing ENTERCODE by the modulus of 8 allows use of
		   ;; linear rather than modular comparisons in decision tree.
		   (setq entercode (the fixnum (+ entercode 8))))

		 ;; Exhaustive decision tree enumerates all the possibilities.
		 ;; A vertex which IS a portal corner [node has an EVEN CODE]
		 ;; supplies that corner itself.  We only "push corners" here
		 ;; if the subcontour "rounds" a corner, that is, if contour
		 ;; originates BEFORE [not AT] and terminates AFTER [not AT]
		 ;; the corresponding corner.
		 ;;
		 (cond ((= entercode leavecode))
		       ;; ENTER and LEAVE vertices on same edge - do nothing.
		       ;;
		       ((< leavecode 2)
			(when (> entercode 2)
			  (push (list xci- yci+) accumulator))
			(when (> entercode 4)
			  (push (list xci+ yci+) accumulator))
			(when (> entercode 6)
			  (push (list xci+ yci-) accumulator))
			(when (> entercode 8)
			  (push (list xci- yci-) accumulator)))
		       ;;
		       ((< leavecode 4)
			(when (> entercode 4)
			  (push (list xci+ yci+) accumulator))
			(when (> entercode 6)
			  (push (list xci+ yci-) accumulator))
			(when (> entercode 8)
			  (push (list xci- yci-) accumulator))
			(when (> entercode 10)
			  (push (list xci- yci+) accumulator)))
		       ;;
		       ((< leavecode 6)
			(when (> entercode 6)
			  (push (list xci+ yci-) accumulator))
			(when (> entercode 8)
			  (push (list xci- yci-) accumulator))
			(when (> entercode 10)
			  (push (list xci- yci+) accumulator))
			(when (> entercode 12)
			  (push (list xci+ yci+) accumulator)))
		       ;;
		       ;; LEAVECODE must be < 8 since that is the modulus.
		       (t (when (> entercode 8)
			    (push (list xci- yci-) accumulator))
			  (when (> entercode 10)
			    (push (list xci- yci+) accumulator))
			  (when (> entercode 12)
			    (push (list xci+ yci+) accumulator))
			  (when (> entercode 14)
			    (push (list xci+ yci-) accumulator))))
		 ;;
		 ;; Now that any needed corners are pushed, we can take care
		 ;; of the ENTER node.  Note that we wait to delete it from
		 ;; ENTERLIST until NOW, rather than when first encountered
		 ;; in original sweep, because even if already processed into
		 ;; a subcontour we still need to find it [on ENTERLIST] when
		 ;; we encounter the LEAVE node on that subcontour which
		 ;; closes the contour with this ENTER node.
		 ;;
		 (cond
		   ((eq enternode starter)
		    ;;
		    ;; Found starting point - end of current subcontour.
		    (unless (cddr accumulator)
		      ;; Subcontours must have at least 3 nodes - at least
		      ;; one ENTER, same number of LEAVEs, zero or more
		      ;; INSIDE nodes, and zero to 4 corner nodes.
		      (error "CLIP-BLOCKS [3] Degenerate contour."))
		    ;;
		    (push accumulator outlist)
		    (setq enterlist (cdr enterlist))
		    (cond ((null enterlist)
			   (return-from clip-blocks outlist))
			  (t (return))))
		   ;;
		   ;; Found an ENTER node NOT at end of subcontour.
		   ;; Push it and continue traversal from that point.
		   (t (push (list (svref (the (simple-array t (#.Cnode-Size))
					   enternode)
					 #.Cnode-Xci)
				  (svref (the (simple-array t (#.Cnode-Size))
					   enternode)
					 #.Cnode-Yci))
			    accumulator)
		      (setq enterlist (delete enternode enterlist :test #'eq))
		      (setq curr enternode)))))))))))

;;;-------------------------------------------------------------

(defun grazer? (arg-vec)
  ;;
  ;; A GRAZER is a vertex ON the border but treated as INSIDE because the
  ;; polygon interior is between the two segments which intersect at this
  ;; vertex AND both segments traverse the interior of portal.
  ;;
  (declare (type (simple-array single-float (#.Argv-Size)) arg-vec))
  ;;
  (let ((bx (aref arg-vec #.Argv-Bx))
	(by (aref arg-vec #.Argv-By))
	(cx (aref arg-vec #.Argv-Cx))
	(cy (aref arg-vec #.Argv-Cy))
	(nx (aref arg-vec #.Argv-Nx))
	(ny (aref arg-vec #.Argv-Ny))
	(xci- (aref arg-vec #.Argv-Xci-))
	(xci+ (aref arg-vec #.Argv-Xci+))
	(yci- (aref arg-vec #.Argv-Yci-))
	(yci+ (aref arg-vec #.Argv-Yci+)))
    ;;
    ;; BX, BY is vertex just BEFORE current one,
    ;; CX, CY is CURRENT vertex,
    ;; NX, NY is NEXT vertex, just AFTER current one.
    ;;
    (declare (type single-float bx by cx cy nx ny xci- xci+ yci- yci+))
    ;;
    (and (<= xci- cx xci+)              ;Current vertex is on portal boundary,
	 (<= yci- cy yci+)                ;so both CX and CY must be in range.
	 (or (= cx xci-)                  ;Current vertex must lie on at least
	     (= cx xci+)                      ;one of the four boundary edges.
	     (= cy yci-)
	     (= cy yci+))
	 ;;
	 ;; At least ONE of the following conditions must be true, due to
	 ;; OR above, and if ANY is true the failure of its subsidiary
	 ;; conditions will cause its AND to succeed, forcing the outermost
	 ;; OR to succeed, forcing the NOT and hence the entire fcn to FAIL.
	 (not (or (and (= cx xci-)                ;If Current is on left edge,
		       (or (<= bx xci-)           ;other two must be to right.
			   (<= nx xci-)))
		  (and (= cx xci+)               ;If Current is on right edge,
		       (or (>= bx xci+)            ;other two must be to left.
			   (>= nx xci+)))
		  (and (= cy yci-)                 ;If Current is on top edge,
		       (or (<= by yci-)             ;other two must be below.
			   (<= ny yci-)))
		  (and (= cy yci+)              ;If Current is on bottom edge,
		       (or (>= by yci+)             ;other two must be above.
			   (>= ny yci+)))))
	 ;;
	 ;; Now we know both line segments traverse the portal interior.  Now
	 ;; check that they do so in the correct direction - clockwise along
	 ;; the portal edge.  Get the cross-product of two vectors, first the
	 ;; segment approaching the border vertex and second the segment
	 ;; leaving the border vertex.  If this cross-product is negative,
	 ;; implying clockwise rotation [of < 180 degrees] along direction of
	 ;; contour traversal [clockwise], the polygon interior is toward the
	 ;; center of the portal from the current vertex.
	 ;;
	 (< (* (- cx bx)
	       (- ny cy))
	    (* (- nx cx)
	       (- cy by))))))

;;;-------------------------------------------------------------

(defun pushnode (arg-vec node-type chain)
  ;;
  ;; Creates a singly-linked circular chain of all nodes on original
  ;; contour which are inside or border on portal.  CHAIN is
  ;; ptr to last node allocated [or NIL if none yet].  Returns ptr
  ;; to node allocated in this call.
  ;;
  (declare (type (simple-array single-float (#.Argv-Size)) arg-vec))
  ;;
  (let ((xcoord (aref arg-vec #.Argv-Vx))
	(ycoord (aref arg-vec #.Argv-Vy))
	(xci- (aref arg-vec #.Argv-Xci-))
	(xci+ (aref arg-vec #.Argv-Xci+))
	(yci- (aref arg-vec #.Argv-Yci-))
	(yci+ (aref arg-vec #.Argv-Yci+))
	(node (make-array #.Cnode-Size :element-type t)))
    ;;
    (declare (type (simple-array t (#.Cnode-Size)) node)
	     (type single-float xcoord ycoord xci- xci+ yci- yci+))
    ;;
    (unless (and (<= xci- xcoord xci+)
		 (<= yci- ycoord yci+))
      (error "PUSHNODE [1] Vertex outside portal."))
    ;;
    (setf (svref node #.Cnode-Xci) xcoord)
    (setf (svref node #.Cnode-Yci) ycoord)
    (setf (svref node #.Cnode-Type) node-type)
    ;;
    ;; Cache border code for convenience of border-closing search algorithm.
    (setf (svref node #.Cnode-Code)
	  (cond ((eq node-type :Inside)
		 nil)
		((= xcoord xci-)
		 (cond ((= ycoord yci-) 0)
		       ((= ycoord yci+) 2)
		       (t 1)))
		((= xcoord xci+)
		 (cond ((= ycoord yci+) 4)
		       ((= ycoord yci-) 6)
		       (t 5)))
		((= ycoord yci+) 3)
		((= ycoord yci-) 7)
		(t (error "PUSHNODE [2] Border vertex inside portal."))))
    ;;
    ;; Singly-directional and circular linkage.
    (cond ((null chain)
	   ;; First node points to itself.
	   (setf (svref node #.Cnode-Next) node))
	  ;;
	  ;; Otherwise splice in all later nodes with NEXT pointing
	  ;; to node to which last allocated used to point
	  ;; [in forward direction].
	  (t (setf (svref node #.Cnode-Next)
		   (svref (the (simple-array t (#.Cnode-Size)) chain)
			  #.Cnode-Next))
	     (setf (svref (the (simple-array t (#.Cnode-Size)) chain)
			  #.Cnode-Next)
		   node)))
    ;;
    ;; Must return NODE just allocated.
    node))

;;;-------------------------------------------------------------

(defun single-cross (arg-vec &aux (crosspt 0.0))
  ;;
  (declare (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type single-float crosspt))
  ;;
  (let ((ix (aref arg-vec #.Argv-Ix))
	(iy (aref arg-vec #.Argv-Iy))
	(ox (aref arg-vec #.Argv-Ox))
	(oy (aref arg-vec #.Argv-Oy))
	(xci- (aref arg-vec #.Argv-Xci-))
	(xci+ (aref arg-vec #.Argv-Xci+))
	(yci- (aref arg-vec #.Argv-Yci-))
	(yci+ (aref arg-vec #.Argv-Yci+)))
    ;;
    ;; IX, IY - coordinates of point known to be strictly INSIDE portal.
    ;; OX, OY - coordinates of point known to be OUTSIDE portal or
    ;; ON BORDER.
    ;;
    ;; Nota Bene: Inside case is strict; Outside case includes equality.
    ;; Either endpoint can be the initial/terminal endpoint.
    ;;
    (declare (type single-float ix iy ox oy xci- xci+ yci- yci+))
    ;;
    (tagbody
      (when (and (<= ox xci-)                       ;Crossing at XCI-
		 (< xci- ix))
	(setq crosspt (interpolate-crossing iy oy ix xci- ox))
	(when (<= yci- crosspt yci+)
	  (setf (aref arg-vec #.Argv-X) xci-)
	  (setf (aref arg-vec #.Argv-Y) crosspt)
	  (go DONE)))
      ;;
      (when (and (< ix xci+)                        ;Crossing at XCI+
		 (<= xci+ ox))
	(setq crosspt (interpolate-crossing iy oy ix xci+ ox))
	(when (<= yci- crosspt yci+)
	  (setf (aref arg-vec #.Argv-X) xci+)
	  (setf (aref arg-vec #.Argv-Y) crosspt)
	  (go DONE)))
      ;;
      (when (and (<= oy yci-)                       ;Crossing at YCI-
		 (< yci- iy))
	(setq crosspt (interpolate-crossing ix ox iy yci- oy))
	(when (<= xci- crosspt xci+)
	  (setf (aref arg-vec #.Argv-X) crosspt)
	  (setf (aref arg-vec #.Argv-Y) yci-)
	  (go DONE)))
      ;;
      (when (and (< iy yci+)                        ;Crossing at YCI+
		 (<= yci+ oy))
	(setq crosspt (interpolate-crossing ix ox iy yci+ oy))
	(when (<= xci- crosspt xci+)
	  (setf (aref arg-vec #.Argv-X) crosspt)
	  (setf (aref arg-vec #.Argv-Y) yci+)
	  (go DONE)))
      ;;
      (error "SINGLE-CROSS [1] Bad crossing.")
      ;;
      DONE))
  ;;
  ;; Don't allow a boxed flonum to be passed back accidentally.
  nil)

;;;-------------------------------------------------------------

(defun dual-cross (arg-vec &aux (crosspt 0.0) (xe 0.0) (ye 0.0)
		   (xl 0.0) (yl 0.0) (entering? nil) (leaving? nil))
  ;;
  (declare (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type (member nil t) entering? leaving?)
	   (type single-float crosspt xe ye xl yl))
  ;;
  (let ((ix (aref arg-vec #.Argv-Ix))
	(iy (aref arg-vec #.Argv-Iy))
	(ox (aref arg-vec #.Argv-Ox))
	(oy (aref arg-vec #.Argv-Oy))
	(xci- (aref arg-vec #.Argv-Xci-))
	(xci+ (aref arg-vec #.Argv-Xci+))
	(yci- (aref arg-vec #.Argv-Yci-))
	(yci+ (aref arg-vec #.Argv-Yci+)))
    ;;
    ;; IX, IY - coordinates of INITIAL endpoint.
    ;; OX, OY - coordinates of TERMINAL endpoint.
    ;; Both endpoints known to be outside, and either or both endpoints
    ;; can have one or both coordinates equalling one of the border
    ;; values.
    ;;
    ;; Returns 5 values: T/NIL [whether line segment crosses portal],
    ;; X and Y of ENTER crossing and then X and Y of LEAVE crossing.
    ;; If first value is NIL the rest of them are meaningless - a
    ;; default of 0.0 or values left over from flushed edge or corner
    ;; grazings.
    ;;
    (declare (type single-float ix iy ox oy xci- xci+ yci- yci+))
    ;;
    (when (and (<= ix xci-)                         ;Entering at XCI-
	       (< xci- ox))
      (setq crosspt (interpolate-crossing iy oy ix xci- ox))
      (when (<= yci- crosspt yci+)
	(setq xe xci- ye crosspt entering? t)))
    (when (and (<= ox xci-)                         ;Leaving at XCI-
	       (< xci- ix))
      (setq crosspt (interpolate-crossing iy oy ix xci- ox))
      (when (<= yci- crosspt yci+)
	(setq xl xci- yl crosspt leaving? t)))
    (when (and (< ox xci+)                          ;Entering at XCI+
	       (<= xci+ ix))
      (setq crosspt (interpolate-crossing iy oy ix xci+ ox))
      (when (<= yci- crosspt yci+)
	(setq xe xci+ ye crosspt entering? t)))
    (when (and (< ix xci+)                          ;Leaving at XCI+
	       (<= xci+ ox))
      (setq crosspt (interpolate-crossing iy oy ix xci+ ox))
      (when (<= yci- crosspt yci+)
	(setq xl xci+ yl crosspt leaving? t)))
    (when (and (<= iy yci-)                         ;Entering at YCI-
	       (< yci- oy))
      (setq crosspt (interpolate-crossing ix ox iy yci- oy))
      (when (<= xci- crosspt xci+)
	(setq xe crosspt ye yci- entering? t)))
    (when (and (<= oy yci-)                         ;Leaving at YCI-
	       (< yci- iy))
      (setq crosspt (interpolate-crossing ix ox iy yci- oy))
      (when (<= xci- crosspt xci+)
	(setq xl crosspt yl yci- leaving? t)))
    (when (and (< oy yci+)                          ;Entering at YCI+
	       (<= yci+ iy))
      (setq crosspt (interpolate-crossing ix ox iy yci+ oy))
      (when (<= xci- crosspt xci+)
	(setq xe crosspt ye yci+ entering? t)))
    (when (and (< iy yci+)                          ;Leaving at YCI+
	       (<= yci+ oy))
      (setq crosspt (interpolate-crossing ix ox iy yci+ oy))
      (when (<= xci- crosspt xci+)
	(setq xl crosspt yl yci+ leaving? t)))
    ;;
    ;; A "crossing" is legitimate only if there is an ENTER and a LEAVE.
    ;; Corner grazings might cause one without the other, and they are
    ;; not considered legitimate "crossings".
    ;;
    (setf (aref arg-vec #.Argv-Xe) xe)
    (setf (aref arg-vec #.Argv-Ye) ye)
    (setf (aref arg-vec #.Argv-Xl) xl)
    (setf (aref arg-vec #.Argv-Yl) yl)
    ;;
    (and entering? leaving?)))

;;;=============================================================
;;; End.
