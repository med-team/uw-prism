;;;
;;; filmstrip
;;;
;;; The filmstrip displays a list of pixmaps in a horizontal viewing area.
;;; A subset of the pixmap list is displayed in the viewing area, and
;;; arrow buttons on each side provide a means for scrolling forward or
;;; backward through the list.  A refrence frame is displayed to the 
;;; left of the viewing area. 
;;;
;;; 12-Jul-1992 I. Kalet started, and made many modifications.
;;; 16-Feb-1993 J. Unger rewrite from I. Kalet's original code.
;;; 26-Apr-1993 J. Unger revise after extensive discussions.
;;; 29-Apr-1993 J. Unger modify setf index :around method so frame 
;;;   corresponding to selected index moved into viewport.
;;; 07-May-1993 J. Unger modify initialization params to initargs.
;;; 28-May-1993 J. Unger many modifications to operate with easel.
;;; 30-Jun-1993 J. Unger move questions to a different text file.
;;;  2-Jul-1993 I. Kalet remove reference view stuff, move insert-at
;;;  and delete-at to misc module.
;;; 16-Mar-1994 J. Unger add destroy method.
;;; 21-Apr-1994 J. Unger move arrow drawing code to function in misc module
;;; 02-Sep-1994 J. Unger make hilited fs border red.
;;;  9-Jun-1997 I. Kalet delete global params., make width and height
;;;  attributes, use new SLIK arrow button, make button 2 move the
;;;  viewport 5 frames if possible.  Don't make a subclass of
;;;  generic-panel - prepare for moving to SLIK.
;;; 26-Jan-1998 I. Kalet incorporate more data management here instead
;;; of in client modules.  Consolidate and simplify.  Add scale factor
;;; so it does not depend on changes in other components.
;;; 25-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;;  5-Jan-2000 I. Kalet parametrize format of display of frame index
;;; info, keep value in frame-index slot instead of reading back from
;;; readout.  Relax plane match criterion for display.
;;; 30-Jul-2000 I. Kalet put draw-image-pix code inline here, not used
;;; anywhere else.
;;; 24-Oct-2004 A. Simms adjust call to make-instance in make-filmstrip 
;;; to accomodate additional argument keys explicitly.
;;;  3-Jun-2009 I. Kalet use original images instead of mini-images,
;;; use scale-image to resize to frame size.
;;;

(in-package :prism)

;;;-----------------------------------

(defclass filmstrip ()

  ((width :type fixnum
	  :accessor width
	  :initarg :width
	  :documentation "The overall width in pixels of the filmstrip
frame, including the buttons.")

   (height :type fixnum
	   :accessor height
	   :initarg :height
	   :documentation "The overall height in pixels of the
filmstrip frame, including the readouts.")

   (scale :type single-float
	  :accessor scale
	  :initarg :scale
	  :documentation "The number of pixels per unit of model
space, the same in all filmstrip frames.")

   (images :type list
	   :accessor images
	   :initarg :images
	   :documentation "A list of images that will appear in the
background of some frames of the filmstrip, provided by the client
when the filmstrip is created.  Any fs-frame that has an image in it
will stay in the fs-frames list even if there are no foreground data
to display.")

   (index :accessor index
	  :initarg :index
	  :documentation "The index value of the selected frame in the
filmstrip's fs-frame list.")

   (new-index :type ev:event
	      :accessor new-index
	      :initform (ev:make-event)
	      :documentation "Announced when index is updated.")

   (window :type fixnum
	   :accessor window
	   :initarg :window
	   :documentation "The grayscale window width of the images in
the filmstrip background.")

   (level :type fixnum
	  :accessor level
	  :initarg :level
	  :documentation "The grayscale level value or center of the
window of the images in the filmstrip background.")

   (index-format :type string
		 :accessor index-format
		 :initarg :index-format
		 :documentation "The format string used to display the
index value in each frame.")

   ;;--------------------------------------------------------
   ;; from here on this stuff is internal to the filmstrip
   ;;--------------------------------------------------------

   (fr :type sl:frame
       :accessor fr
       :documentation "The SLIK frame that contains the filmstrip.")

   (fs-frames :type list
	      :accessor fs-frames
	      :initform nil
	      :documentation "A list of objects of type fs-frame, each
containing the information for that individual filmstrip frame,
including the picture, the readout and the supporting graphic and
image data.")

   (viewport :accessor viewport
	     :documentation "The available viewing area for the frames
of the filmstrip.")

   (left-frame-no :type fixnum
		  :accessor left-frame-no
		  :initform 0
		  :documentation "An integer that indicates which
frame in the fs-frames list is currently the left-most frame in the
viewport.  When no frames are present it is 0.")

   (left-arrow :type sl:picture
	       :accessor left-arrow
	       :documentation "The button on the left end which when
pressed will scroll the viewing range one frame to the left, i.e., the
pictures move to the right.")

   (right-arrow :type sl:picture
		:accessor right-arrow
		:documentation "The button on the right end which when
pressed will scroll the viewing range one frame to the right, i.e.,
the pictures move to the left.")

   )

  (:default-initargs :scale 5.0 :images nil :index nil
		     :window 500 :level 1024 :index-format "~A")

  (:documentation "The filmstrip shows a scrollable sequence of
pictures and index readouts.  The contents of the pictures are derived
from initialization arguments.  Left and right buttons allow scrolling
through the set of pictures if there are too many to show at once.")

  )

;;;-----------------------------------

(defun make-filmstrip (width height &rest initargs)

  "make-filmstrip width height &rest initargs

Returns a filmstrip with specified overall width, height and other
parameters."

  (apply #'make-instance 'filmstrip
	 :width width :height height :allow-other-keys t initargs))

;;;-----------------------------------

(defmethod destroy ((fs filmstrip))

  "Releases additional X resources used by this panel."

  (dolist (frm (fs-frames fs)) (destroy frm))
  (sl:destroy (left-arrow fs))
  (sl:destroy (right-arrow fs))
  (sl:destroy (viewport fs))
  (sl:destroy (fr fs)))

;;;-----------------------------------

(defclass fs-frame ()

  ((pic :accessor pic
	:initarg :pic
	:documentation "The SLIK picture for this filmstrip frame.")

   (bg-image :accessor bg-image
	     :initarg :bg-image
	     :documentation "The background image pixmap, or a black
pixmap if there is no image in this frame.")

   (fg-prims :type list
	     :accessor fg-prims
	     :initarg :fg-prims
	     :documentation "A list of graphic primitives that are
drawn over the image or black background.  Can be empty.")

   (rdt :accessor rdt
	:initarg :rdt
	:documentation "The SLIK readout at the bottom of the frame,
displaying the index value of the frame.")

   (frame-index :reader frame-index
		:initarg :frame-index
		:documentation "The index value displayed in the frame
readout.")

   )

  (:default-initargs :bg-image nil :fg-prims nil)

  (:documentation "Each displayed frame in the filmstrip has all its
components together in this one data structure, instead of maintaining
separate lists of pictures, lists of pixmaps, etc.")

  )

;;;----------------------------------

(defun make-fs-frame (width height parent index ulc-x fmt-string
		      &rest initargs)

  (apply #'make-instance 'fs-frame
	 :pic (sl:make-picture width width
			       :parent parent
			       :ulc-x ulc-x)
	 :rdt (sl:make-readout width (- height width)
			       :info (format nil fmt-string index)
			       :parent parent
			       :ulc-x ulc-x
			       :ulc-y width)
	 :frame-index index
	 initargs))

;;;----------------------------------

(defmethod destroy ((frm fs-frame))

  (sl:destroy (pic frm))
  (sl:destroy (rdt frm))
  (if (bg-image frm) (clx:free-pixmap (bg-image frm))))

;;;----------------------------------

(defun fs-set-color (obj color-gc fs)

  "fs-set-color obj color-gc fs

updates the color of each of the graphic primitives of the object obj
in the display frames of filmstrip fs with graphic context color-gc."

  (dolist (frm (fs-frames fs))
    (dolist (prim (fg-prims frm))
      (when (eq (object prim) obj)
	(setf (color prim) color-gc)
	(fs-display-frame frm)))))

;;;----------------------------------

(defun fs-add-contour (vol con fs)

  "fs-add-contour vol con fs

Adds the contour con associated with pstruct vol to the filmstrip fs
in the fs-frame whose index is equal to the contour's z level.  If no
such fs-frame exists, creates a new one at that z-level and add to the
fs-frames list."

  (let* ((pic-width *mini-image-size*) ;; default frame width
	 (frm (find (z con) (fs-frames fs)
		    :key #'frame-index :test #'(lambda (a b)
						 (poly:nearly-equal
						  a b *display-epsilon*))))
	 (prim (or (if frm (find vol (fg-prims frm) :key #'object))
		   (make-lines-prim
		    nil (sl:color-gc (display-color vol))
		    :object vol)))
	 (middle (round (/ pic-width 2))))
    (declare (fixnum middle))
    (draw-transverse (vertices con) prim middle middle (scale fs))
    (if frm (push prim (fg-prims frm))
      (let ((win (sl:window (viewport fs))))
	(setq frm (make-fs-frame pic-width
				 (height fs) win
				 (z con) 0
				 (index-format fs)
				 :fg-prims (list prim)))
	(ev:add-notify fs (sl:button-press (pic frm))
		       #'fs-picture-selected)
	(setf (fs-frames fs)
	  (insert frm (fs-frames fs) :key #'frame-index))
	(let* ((pos-newfrm (position frm (fs-frames fs)))
	       (current (left-frame-no fs))
	       (diff (- pos-newfrm current)))
	  (fs-set-viewport fs (if (and (>= diff 0)
				       (< diff (/ (clx:drawable-width win)
						  pic-width)))
				  current
				pos-newfrm)))))
    (fs-display-frame frm)))

;;;----------------------------------

(defun fs-delete-contour (vol z fs)

  "fs-delete-contour vol z fs

Deletes the contour associated with pstruct vol at z from the
filmstrip.  If this contour was the only information to be displayed
at that plane (ie: no other contours or image), then deletes the
entire fs-frame for that plane from the filmstrip."

  (let ((frm (find z (fs-frames fs)
		   :test #'poly:nearly-equal :key #'frame-index)))
    (when frm
      (if (or (find z (images fs) 
		    :test #'(lambda (a b)
			      (poly:nearly-equal a b *display-epsilon*))
		    :key #'(lambda (img) (vz (origin img))))
	      (find vol (fg-prims frm)
		    :key #'object :test-not #'eq))
	  (progn
	    (setf (fg-prims frm) ;; keep frame, delete contour
	      (remove vol (fg-prims frm) :key #'object))
	    (fs-display-frame frm))
	(let ((left-pos (left-frame-no fs)) ;; delete the frame
	      (pos (position frm (fs-frames fs))))
	  (setf (fs-frames fs) (remove frm (fs-frames fs)))
	  (fs-set-viewport fs (if (or (/= pos left-pos)
				      (<= pos (length (fs-frames fs))))
				  left-pos ;; just close up
				(1- pos))) ;; otherwise move over one
	  (ev:remove-notify fs (sl:button-press (pic frm)))
	  (destroy frm))))))

;;;----------------------------------

(defun fs-replace-points (old-pts new-pts index fs)

  "fs-replace-points old-pts new-pts index fs

Replaces the old points in filmstrip fs in frame with z equal to index
with new points at z value index.  If no frame exists at that index
and new-pts is non-nil, create a new frame at that index and add to
the filmstrip.  If there are no new points and the existing frame is
now empty, it is deleted from the filmstrip."

  (let* ((pic-width *mini-image-size*)
	 (middle (round (/ pic-width 2)))
	 (scale (scale fs))
	 (frm (find index (fs-frames fs)
		    :test #'(lambda (a b)
			      (poly:nearly-equal a b *display-epsilon*))
		    :key #'frame-index))
	 (prims (mapcar #'(lambda (pt) ;; make new graphic prims
			    (make-rectangles-prim
			     (list (round (+ middle (* scale (x pt))))
				   (round (- middle (* scale (y pt))))
				   2 2)
			     (sl:color-gc (display-color pt))
			     :object pt))
			new-pts)))
    (when old-pts ;; take off old gp's
      (setf (fg-prims frm)
	(remove-if #'(lambda (obj) (and (typep obj 'mark)
					(find (id obj) old-pts :key #'id)))
		   (fg-prims frm) 
		   :key #'object))
      (if (or new-pts
	      (fg-prims frm)
	      (find index (images fs) 
		    :test #'(lambda (a b)
			      (poly:nearly-equal a b *display-epsilon*))
		    :key #'(lambda (img) (vz (origin img)))))
	  (fs-display-frame frm)
	(let ((left-pos (left-frame-no fs)) ;; delete the frame
	      (pos (position frm (fs-frames fs))))
	  (setf (fs-frames fs) (remove frm (fs-frames fs)))
	  (fs-set-viewport fs (if (or (/= pos left-pos)
				      (<= pos (length (fs-frames fs))))
				  left-pos ;; just close up
				(1- pos))) ;; otherwise move over one
	  (ev:remove-notify fs (sl:button-press (pic frm)))
	  (destroy frm))))
    (when new-pts
      (if frm (setf (fg-prims frm) ;; just add new graphic prims
		(append prims (fg-prims frm)))
	(let ((win (sl:window (viewport fs)))) ;; or make a new frame
	  (setq frm (make-fs-frame pic-width
				   (height fs) win
				   index 0 (index-format fs)
				   :fg-prims prims))
	  (ev:add-notify fs (sl:button-press (pic frm))
			 #'fs-picture-selected)
	  (setf (fs-frames fs)
	    (insert frm (fs-frames fs) :key #'frame-index))
	  (let* ((pos-newfrm (position frm (fs-frames fs)))
		 (current (left-frame-no fs))
		 (diff (- pos-newfrm current)))
	    (fs-set-viewport fs (if (and (>= diff 0)
					 (< diff (/ (clx:drawable-width win)
						    pic-width)))
				    current
				  pos-newfrm)))))
      (fs-display-frame frm))))

;;;-----------------------------------

(defmethod (setf index) :around (new-index (fs filmstrip))

  "Updates the border highlight of the viewport pictures, moves the
highlighted picture into the viewport if it isn't there already, and
announces new-index when index is set."

  ;; unhighlight old picture, highlight new one in filmstrip viewport
  (let* ((old-frm (aif (index fs)
		       (find it (fs-frames fs)
			     :key #'frame-index
			     :test #'(lambda (a b)
				       (poly:nearly-equal
					a b *display-epsilon*)))))
	 (new-frm (find new-index (fs-frames fs)
			:key #'frame-index
			:test #'(lambda (a b)
				  (poly:nearly-equal a b *display-epsilon*)))))
    (when old-frm
      (setf (sl:border-width (pic old-frm)) 1)
      (setf (sl:border-color (pic old-frm)) 'sl:white)
      (sl:erase (pic old-frm))
      (sl:draw-border (pic old-frm)))
    (call-next-method)
    (when new-frm
      (setf (sl:border-width (pic new-frm)) 5)
      (setf (sl:border-color (pic new-frm)) 'sl:red)
      (sl:draw-border (pic new-frm))
      ;; move the highlighted picture into the viewport if needed
      (let* ((win (sl:window (pic new-frm)))
	     (dx (clx:drawable-x win))
	     (pic-width (clx:drawable-width win))
	     (vp-width  (clx:drawable-width (sl:window (viewport fs)))))
	(unless (<= 0 dx (- vp-width pic-width))
	  (fs-set-viewport fs (position new-frm (fs-frames fs))))))
    (sl:flush-output))
  (ev:announce fs (new-index fs) new-index)
  new-index)

;;;-----------------------------------

(defun fs-set-viewport (fs left-pos)

  "fs-set-viewport fs left-pos

Adjusts the x coordinates of all the frame windows so that the frame
in position left-pos in the fs-frames list of filmstrip fs is at the
left end of the filmstrip."

  (setf (left-frame-no fs) left-pos)
  (when (fs-frames fs)
    (let* ((width (sl:width (pic (first (fs-frames fs)))))
	   (x (- (* left-pos width))))
      (dolist (frm (fs-frames fs))
	(setf (clx:drawable-x (sl:window (pic frm))) x)
	(setf (clx:drawable-x (sl:window (rdt frm))) x)
	(incf x width)))))

;;;-----------------------------------

(defun fs-display-frame (frm)

  "fs-display-frame frm

refreshes the window of filmstrip frame frm by copying the background
image pixmap if any, then replaying the graphic primitives and then
exposing the data in the window, as in the usual graphic pipeline."

  (let* ((img-px (bg-image frm))
	 (pic (pic frm))
	 (px (sl:pixmap pic)))
    (if img-px
	(clx:copy-area img-px (sl:color-gc 'sl:white) ;; image pixmap
		       0 0
		       (clx:drawable-width img-px)
		       (clx:drawable-height img-px) 
		       px 0 0)
      (clx:draw-rectangle px (sl:color-gc 'sl:black) ;; or just set to black
			  0 0
			  (clx:drawable-width px)
			  (clx:drawable-height px)
			  t)) ;; fill rectangle
    (mapc #'(lambda (prim) (draw-pix prim px))
	  (fg-prims frm))
    (sl:erase pic)
    (sl:draw-border pic))
  (sl:flush-output))

;;;-----------------------------------

(defun fs-picture-selected (fs pic code x y)

  "fs-picture-selected fs pic code x y

An action function that sets a new index value when a picture in the
filmstrip is selected with the left mouse button and pointer, i.e.,
code is 1."

  (declare (ignore x y))
  (when (= code 1) ;; left button only
    (setf (index fs)
      (frame-index (find pic (fs-frames fs) :key #'pic)))))

;;;-----------------------------------

(defun fs-move-frames (fs nframes)

  "fs-move-frames fs nframes

Shifts the view port, in the filmstrip fs, nframes picture widths to
the left or right, depending on whether nframes is negative or
positive, provided that at least one frame remains in the filmstrip at
the left end of the viewport."

  (let ((new-left-no (+ (left-frame-no fs) nframes)))
    (when (and (>= new-left-no 0) ;; don't go off the left end
	       (< new-left-no (length (fs-frames fs)))) ;; or the right
      (fs-set-viewport fs (setf (left-frame-no fs) new-left-no))
      (sl:flush-output))))

;;;-----------------------------------

(defmethod initialize-instance :after ((fs filmstrip) &rest initargs) 

  "Initializes the user interface for the filmstrip."

  (let* ((pic-size *mini-image-size*) ;; default pixmap size
	 (fsw (width fs))
	 (fsh (height fs))
	 (arrow-wd 50) ;; arrow button width
	 (frm (apply #'sl:make-frame fsw fsh
		     :title "Prism FILMSTRIP" 
		     initargs))
	 (frm-win (sl:window frm))
	 (vp-width (- fsw (* 2 arrow-wd)))
	 (vp (apply #'sl:make-frame vp-width fsh
		    :parent frm-win
		    :ulc-x arrow-wd :ulc-y 0
		    initargs))
	 (left-b (apply #'sl:make-arrow-button arrow-wd fsh :left
			:parent frm-win
			:fg-color 'sl:red
			:ulc-x 0 :ulc-y 0
			initargs))
	 (right-b (apply #'sl:make-arrow-button arrow-wd fsh :right
			 :parent frm-win
			 :fg-color 'sl:red
			 :ulc-x (- fsw arrow-wd) :ulc-y 0
			 initargs)))
    (setf (fr fs) frm
	  (viewport fs) vp
	  (left-arrow fs) left-b
	  (right-arrow fs) right-b)
    ;; create frames for images if present
    (when (images fs)
      (let* ((vp-win (sl:window vp))
	     (ulc-x (- pic-size))
	     (graymap (sl:make-graymap (window fs) (level fs)
				       (range (first (images fs)))))
	     (img-dims (array-dimensions (pixels (first (images fs)))))
	     (mapped-image (make-array img-dims
				       :element-type 'clx:pixel))
	     (scaled-image (make-array (list pic-size pic-size)
				       :element-type 'clx:pixel))
	     (mag (/ pic-size (first img-dims)))
	     (x0 0)
	     (y0 0)
	     )
	(setf (fs-frames fs)
	  (sort (mapcar #'(lambda (img)
			    (make-fs-frame
			     pic-size fsh vp-win (vz (origin img))
			     (incf ulc-x pic-size) (index-format fs)
			     :bg-image ;; transform image to pixmap
			     (let ((px (sl:make-square-pixmap pic-size
							      nil vp-win)))
			       (sl:map-image graymap (pixels img)
					     mapped-image)
			       (scale-image mapped-image scaled-image
					    mag x0 y0)
			       (sl:write-image-clx scaled-image px)
			       px))) ;; must return it from the let
			(images fs))
		#'<
		:key #'frame-index))
 	(setf (index fs) (frame-index (first (fs-frames fs)))
	      (scale fs) (* mag (pix-per-cm (first (images fs))))))
      (mapc #'(lambda (frm)
		(ev:add-notify fs (sl:button-press (pic frm))
			       #'fs-picture-selected)
		(fs-display-frame frm))
	    (fs-frames fs))
      )
    (ev:add-notify fs (sl:button-on left-b)
		   #'(lambda (strip bt)
		       (declare (ignore bt))
		       (fs-move-frames strip -1)))
    (ev:add-notify fs (sl:button-on right-b)
		   #'(lambda (strip bt)
		       (declare (ignore bt))
		       (fs-move-frames strip 1)))
    (ev:add-notify fs (sl:button-2-on left-b)
		   #'(lambda (strip bt)
		       (declare (ignore bt))
		       (fs-move-frames strip -5)))
    (ev:add-notify fs (sl:button-2-on right-b)
		   #'(lambda (strip bt)
		       (declare (ignore bt))
		       (fs-move-frames strip 5)))))

;;;-----------------------------------
;;; End.
