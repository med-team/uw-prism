;;;
;;; beam-block-graphics
;;;
;;; this module contains the draw methods for blocks.
;;;
;;; 30-Sep-1996 I. Kalet created from beam-graphics.
;;; 24-Jan-1997 I. Kalet eliminate reference to geometry package.
;;; Also portal is now the list of vertices, not a contour object.
;;; 10-May-1997 I. Kalet don't move bev-draw-all here - still circular
;;; indirectly through plans and patients that way.
;;; 19-Jan-1998 I. Kalet beam transform is now array, not multiple values.
;;; 23-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;; 23-Oct-1999 I. Kalet preserve declutter information if beam was
;;; already in view - the visible attribute of the graphic prim.
;;; 20-Sep-2002 I. Kalet punt on oblique view and room view.
;;; 25-May-2009 I. Kalet remove ref to room-view completely.
;;;

(in-package :prism)

;;;----------------------------------------------

(defun draw-beam-block (blk v b)

  "Draws beam-block blk of beam b into view v."

  (cond ((and (typep v 'beams-eye-view)
	      (eq b (beam-for v)))
	 (draw-primary-block blk v b))
	((typep v 'oblique-view) nil)
	(t (draw-regular-block blk v b))))

;;;----------------------------------------------

(defun draw-regular-block (blk v b)

  "Draws beam-block blk of beam b into view v.  Handles all cases
except a block of a beam in its own beams-eye-view."

  (let* ((prim (find blk (foreground v) :key #'object))
	 (color (sl:color-gc (display-color blk)))
	 (sad (isodist (if (typep v 'beams-eye-view) (beam-for v)
			 b)))
	 (bt (beam-transform b v)))
    (unless prim
      (setq prim (make-segments-prim nil color :object blk))
      (push prim (foreground v)))
    (setf (color prim) color
	  (points prim) nil)
    (when (vertices blk)
      (draw-portal prim (vertices blk) bt sad v))))

;;;----------------------------------------------

(defun draw-primary-block (blk v b)

  "Draws beam-block blk of primary beam b into beam's eye view v."

  (when (vertices blk)
    ;; start with new gp's each time, to avoid having to look for and
    ;; disambiguate the segments and rectangles prims, which would be
    ;; very complicated.  But first catch the visible attribute of a
    ;; beam graphic prim if present.
    (let ((visible (aif (find blk (foreground v) :key #'object)
			(visible it) t)))
      (setf (foreground v) (remove blk (foreground v) :key #'object))
      (let* ((color (sl:color-gc (display-color blk)))
	     (solid-prim (get-segments-prim blk v color))
	     (marker-prim (get-rectangles-prim blk v color)))
	(setf (visible solid-prim) visible)
	(setf (visible marker-prim) visible)
	(draw-primary-portal solid-prim
			     marker-prim
			     (vertices blk)
			     (* (collimator-angle b) *pi-over-180*)
			     (isodist b)
			     v)))))

;;;----------------------------------------------
;;; End.
