;;;
;;; beam-panels
;;;
;;; The beam panel is defined here because it is too much code to
;;; include along with the beams themselves, in the beams module.
;;;
;;; 18-Sep-1992 I. Kalet created from beams
;;;  1-Oct-1992 I. Kalet fill in some details
;;;  6-Oct-1992 I. Kalet fix some inconsistencies
;;; 13-Oct-1992 I. Kalet add more connections and stuff
;;; 28-Oct-1992 I. Kalet put read-from-string in notify functions
;;; 29-Nov-1992 I. Kalet add sliders, move stuff around
;;; 15-Feb-1993 I. Kalet squeeze margins, add n fractions, add machine
;;; and color actions.
;;; 16-Apr-1993 I. Kalet add collimator control panel creation and
;;; update, adjust size of beam panel to accomodate.
;;; 22-Apr-1993 I. Kalet add wedge menu
;;; 30-Dec-1993 I. Kalet fix error causing wrong font in collimator
;;; panels, when switching beams - didn't set right font in beam panel
;;; frame.
;;; 18-Feb-1994 I. Kalet add copy beam functions and buttons, include
;;; insertion of beam into plan here.
;;; 02-Mar-1994 J. Unger add textlines to edit atten-factor &
;;; arc-size.
;;; 13-May-1994 I. Kalet add error checking to textlines for numbers.
;;; 31-May-1994 I. Kalet add selector panel for blocks.
;;; 03-Jun-1994 J. Unger fixup omitted remove-notify error & make default
;;; atten factor & arc size show up in panel.
;;; 05-Jun-1994 J. Unger add wedge-orientation button & menu to panel.
;;; 05-Jun-1994 J. Unger uncomment call to block selector panel
;;; destroy mthd.
;;; 30-Jun-1994 I. Kalet make range of couch long. slider -50 to 50
;;; 22-Jul-1994 J. Unger make beam panel slightly longer to fit coll panel;
;;; add beam-for param to call to make-collimator-panel.
;;; 27-Jul-1994 J. Unger add block rotation button.
;;; 18-Sep-1994 J. Unger make block's color initially beam's color.
;;; 03-Oct-1994 J. Unger make machine name color yellow.
;;; 15-Jan-1995 I. Kalet use function isodist instead of inline code.
;;;  Access beam-of on panel, not beam-for of wedge.  Pass beam-of to
;;;  make-block-panel.  Include plan-of and patient-of here so can
;;;  eliminate back-pointers.  Pass plan and patient to block, cutout
;;;  and coll. panels too.
;;;  7-Sep-1995 I. Kalet add coerce forms to insure single-floats in
;;;  MU, arc size, etc. and to insure fixnum in ntreats.
;;;  3-Jan-1996 I. Kalet increase couch lateral limits to -50/50.
;;;  4-May-1997 I. Kalet use label instead of title in sliderboxes
;;; 10-Jun-1997 I. Kalet machine returns the object, not the name,
;;; also use color button labels for blocks.
;;; 16-Sep-1997 I. Kalet explicitly provide machine database
;;; parameters as they are no longer optional.
;;; 26-Oct-1997 I. Kalet insure that when changing wedge id, a valid
;;; wedge rotation is set also.
;;; 15-Dec-1998 I. Kalet extend ALL couch linear motion limits to
;;; accomodate CT scans done with patient displaced.
;;; 25-Jan-2000 I. Kalet display blank for wedge rotation when no wedge.
;;; 22-Feb-2000 I. Kalet use copy instead of copy-beam, and explicitly
;;; change gantry etc. in copy beam action functions.
;;; 26-Mar-2000 I. Kalet final mods to copy 180 function - treat CNTS
;;; and SL20 differently.
;;; 28-May-2000 I. Kalet parametrize small font, change labels to
;;; lower case, widen button column.
;;; 26-Nov-2000 I. Kalet move block list to beam-block-panel, like
;;; volume editor, move block rotate button also.
;;;  6-Jan-2002 I. Kalet change beam name textline to three line textbox
;;; 14-Feb-2002 I. Kalet pad or truncate each line of beam name to
;;; exactly 10 characters.
;;; 14-Mar-2002 I. Kalet limit the beam name to three lines.
;;;  7-Feb-2004 I. Kalet parametrize couch lateral and longitudinal
;;; motion limits in prism-globals.
;;; 25-Aug-2004 I. Kalet add listify call to action for new-name of
;;; beam, per suggestion from Balto.
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass beam-panel (generic-panel)

  ((beam-of :initarg :beam-of
	    :accessor beam-of)

   (plan-of :initarg :plan-of
	    :accessor plan-of
	    :documentation "The plan containing the beam.")
   
   (patient-of :initarg :patient-of
	       :accessor patient-of
	       :documentation "The current patient.")
   
   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame containing all the
dials and sliders...")

   (delete-b :accessor delete-b
	     :documentation "The Delete Panel button.")

   (copy-b :accessor copy-b
	   :documentation "The Copy Here button.")

   (copy-90 :accessor copy-90
	    :documentation "The Copy 90 button.")

   (copy-180 :accessor copy-180
	    :documentation "The Copy 180 button.")

   (copy-270 :accessor copy-270
	    :documentation "The Copy 270 button.")

   (mu-box :accessor mu-box
	   :documentation "Textline for monitor units.")

   (nfrac-box :accessor nfrac-box
	      :documentation "Textline for number of fractions.")

   (name-box :accessor name-box
	     :documentation "Textline for Beam name.")

   (atten-box :accessor atten-box
              :documentation "Textline for attenuation factor.")

   (arc-box :accessor arc-box
            :documentation "Textline for arc size.")

   (machine-b :accessor machine-b
	      :documentation "Button for machine selection.")

   (color-b :accessor color-b
	    :documentation "The color selection button.")

   (wedge-sel-b :accessor wedge-sel-b
  	        :documentation "The wedge selection button.")

   (wedge-ang-b :accessor wedge-ang-b
  	        :documentation "The wedge angle button.")

   (toggle-axis-b :accessor toggle-axis-b
                  :documentation "The button to toggle central axis display.")

   (block-btn :accessor block-btn
	      :documentation "The button to make the block editing panel.")

   (block-pan :accessor block-pan
	      :documentation "The block editing subpanel")

   (coll-db :accessor coll-db
	    :documentation "The dialbox for the collimator angle.")

   (gantry-db :accessor gantry-db
	      :documentation "The dialbox for the gantry angle.")

   (couch-db :accessor couch-db
	     :documentation "The dialbox for the couch angle.")

   (couch-lat-sl :accessor couch-lat-sl
		 :documentation "The slider for the couch lateral
motion.")

   (couch-long-sl :accessor couch-long-sl
		  :documentation "The slider for the couch longitudinal
motion.")

   (couch-ht-sl :accessor couch-ht-sl
		:documentation "The slider for the couch height
motion.")

   (coll-pan :accessor coll-pan
	     :documentation "The sub-panel for the collimator jaw
controls.")

   (busy :accessor busy
	 :initform nil
	 :documentation "The busy bit for controlling updates between
beam attributes and beam controls.")

   )

  (:documentation "The beam panel provides the dials and sliders to
control one beam.")

  )

;;;---------------------------------------------

(defun make-beam-panel (a-beam &rest initargs)

  "make-beam-panel a-beam

Returns a beam panel attached to a-beam."

  (apply #'make-instance 'beam-panel :beam-of a-beam initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((bp beam-panel) &rest initargs)

  "This method creates the beam panel and the mediators."

  (let* ((b (beam-of bp))
	 (bpf (symbol-value *small-font*)) ; the value, not the symbol
	 (beam-fr (apply #'sl:make-frame 430 685
			 :title "Prism BEAM Panel"
			 :font bpf initargs))
	 (bp-win (sl:window beam-fr))
	 ;; following code uses the bp-y function defined in
	 ;; prism-objects module - short for button-placement-y
	 (bth 25)			; the button and textline height
	 (btw 130)			; the button and textline width
	 (dx 10)			; left margin
	 (top-y 10)			; where the first button is
	 (mid-y (+ (bp-y top-y bth 5) 75))	; buttons after name textbox
	 (del-b (apply #'sl:make-button btw bth :button-type :momentary
		       :ulc-x dx :ulc-y top-y :label "Delete Panel"
		       :parent bp-win :font bpf initargs))
	 (cpy-b (apply #'sl:make-button btw bth :button-type :momentary 
		       :ulc-x dx :ulc-y (bp-y top-y bth 1)
		       :label "Copy HERE" :parent bp-win
		       :font bpf initargs))
	 (cpy-90 (apply #'sl:make-button btw bth :button-type :momentary 
			:ulc-x dx :ulc-y (bp-y top-y bth 2)
			:label "Copy 90" :parent bp-win
			:font bpf initargs))
	 (cpy-180 (apply #'sl:make-button btw bth :button-type :momentary 
			 :ulc-x dx :ulc-y (bp-y top-y bth 3)
			 :label "Copy 180" :parent bp-win
			 :font bpf initargs))
	 (cpy-270 (apply #'sl:make-button btw bth :button-type :momentary 
			 :ulc-x dx :ulc-y (bp-y top-y bth 4)
			 :label "Copy 270" :parent bp-win
			 :font bpf initargs))
	 (name-t (apply #'sl:make-textbox 85 70 ;; room for three lines
			:scroll nil ;; and no more
			:ulc-x dx :ulc-y (bp-y top-y bth 5)
			:parent bp-win :font sl:courier-bold-12
			initargs))
	 (mach-b (apply #'sl:make-button btw bth
			:ulc-x dx :ulc-y mid-y
			:label (machine-name b)
			:justify :left :fg-color 'sl:yellow
			:parent bp-win :font bpf initargs))
	 (mu-t (apply #'sl:make-textline btw bth :label "MU: "
		      :ulc-x dx :ulc-y (bp-y mid-y bth 1)
		      :parent bp-win :font bpf
		      :numeric t :lower-limit 0.0 :upper-limit 10000.0
		      initargs))
	 (nfrac-t (apply #'sl:make-textline btw bth :label "N Fract: "
			 :ulc-x dx :ulc-y (bp-y mid-y bth 2)
			 :parent bp-win :font bpf
			 :numeric t :lower-limit 1 :upper-limit 200
			 initargs))
	 (col-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y mid-y bth 3)
		       :label "Beam Color"
		       :fg-color (display-color b)
		       :parent bp-win :font bpf initargs))
	 (wdg-sb (apply #'sl:make-button btw bth
		        :ulc-x dx :ulc-y (bp-y mid-y bth 4)
		        :label (wedge-label (id (wedge b)) (machine b))
			:parent bp-win :font bpf initargs))
	 (wdg-ab (apply #'sl:make-button btw bth
		        :ulc-x dx :ulc-y (bp-y mid-y bth 5)
		        :label (format nil "Wdg Rot: ~a"
				       (if (not (zerop (id (wedge b))))
					   (rotation (wedge b))
					 ""))
			:parent bp-win :font bpf initargs))
	 (atten-t (apply #'sl:make-textline btw bth :label "Atten: "
			 :ulc-x dx :ulc-y (bp-y mid-y bth 6)
			 :parent bp-win :font bpf
			 :numeric t :lower-limit 0.0 :upper-limit 1.0
			 initargs))
	 (arc-t (apply #'sl:make-textline btw bth :label "Arc size: "
		       :ulc-x dx :ulc-y (bp-y mid-y bth 7)
		       :parent bp-win :font bpf
		       :numeric t :lower-limit 0.0 :upper-limit 360.0
		       initargs))
         (tog-axis-b (apply #'sl:make-button btw bth 
			    :label (if (display-axis b) 
				       "Axis ON" "Axis OFF")
			    :ulc-x dx :ulc-y (bp-y mid-y bth 8)
			    :parent bp-win :font bpf
			    initargs))
         (blk-bt (apply #'sl:make-button btw bth 
			:label "Beam Blocks"
			:ulc-x dx :ulc-y (bp-y mid-y bth 9)
			:parent bp-win :font bpf
			initargs))
	 (dial-r 35) ;; dial radius
	 (db-y 10) ;; dialbox y pos
	 (dsx (+ 20 btw)) ;; dial and slider left boundary
	 (col-d (apply #'sl:make-dialbox dial-r :title "Collim."
		       :ulc-x dsx :ulc-y db-y
		       :angle (collimator-angle b)
		       :parent bp-win :font bpf initargs))
	 (gty-d (apply #'sl:make-dialbox dial-r :title "Gantry"
		       :ulc-x (+ dsx (sl:width col-d)) :ulc-y db-y
		       :angle (gantry-angle b)
		       :parent bp-win :font bpf initargs))
	 (cch-d (apply #'sl:make-dialbox dial-r :title "Couch"
		       :ulc-x (+ dsx (* 2 (sl:width col-d)))
		       :ulc-y db-y
		       :angle (couch-angle b)
		       :parent bp-win :font bpf initargs))
	 (sw 260)			; slider width
	 (sh 30)			; slider height
	 (cht-s (apply #'sl:make-sliderbox sw sh -75.0 75.0 -50.0
		       :label "Couch HT: "
		       :ulc-x dsx :ulc-y 150
		       :setting (couch-height b)
		       :parent bp-win :font bpf initargs))
	 (clat-s (apply #'sl:make-sliderbox sw sh
			*couch-lat-lower* *couch-lat-upper*
			-50.0
			:label "Couch LAT: "
			:ulc-x dsx :ulc-y 220
			:setting (couch-lateral b)
			:parent bp-win :font bpf initargs))
	 (clng-s (apply #'sl:make-sliderbox sw sh
			*couch-long-lower* *couch-long-upper*
			-100.0 ;; make sure there is room for larger values
			:label "Couch LNG: "
			:ulc-x dsx :ulc-y 290
			:setting (couch-longitudinal b)
			:parent bp-win :font bpf initargs))
	 (col-p (apply #'make-collimator-panel (collimator b)
                       :beam-of b
		       :plan-of (plan-of bp)
		       :patient-of (patient-of bp)
		       :ulc-x dsx :ulc-y 360
		       :parent bp-win :font bpf initargs)))
    (setf (panel-frame bp) beam-fr	; put all the widgets in the slots
	  (delete-b bp) del-b
	  (copy-b bp) cpy-b
	  (copy-90 bp) cpy-90
	  (copy-180 bp) cpy-180
	  (copy-270 bp) cpy-270
	  (mu-box bp) mu-t
	  (sl:info mu-t) (monitor-units b) ; initial contents for MU
	  (nfrac-box bp) nfrac-t
	  (sl:info nfrac-t) (n-treatments b) ; initial contents
	  (name-box bp) name-t
	  ;; initial contents of name textline
	  (sl:info name-t) (listify (name b) 10)
	  (machine-b bp) mach-b
	  (color-b bp) col-b
	  (wedge-sel-b bp) wdg-sb
	  (wedge-ang-b bp) wdg-ab
          (atten-box bp) atten-t
          (sl:info atten-t) (atten-factor b)
          (arc-box bp) arc-t
          (sl:info arc-t) (arc-size b)
	  (toggle-axis-b bp) tog-axis-b
	  (block-btn bp) blk-bt
	  (coll-db bp) col-d
	  (gantry-db bp) gty-d
	  (couch-db bp) cch-d
	  (couch-ht-sl bp) cht-s
	  (couch-long-sl bp) clng-s
	  (couch-lat-sl bp) clat-s
	  (coll-pan bp) col-p)
    (ev:add-notify bp (sl:button-on del-b)
		   #'(lambda (pan a) (declare (ignore a)) (destroy pan)))
    (ev:add-notify b (sl:button-on cpy-b)
		   #'(lambda (bm btn)
		       (declare (ignore btn))
		       (let ((new-beam (copy bm)))
			 (setf (name new-beam)
			   (format nil "~A" (gensym "BEAM-")))
			 (setf (id (wedge new-beam)) 0)
			 (coll:insert-element new-beam
					      (beams (plan-of bp))))))
    (ev:add-notify b (sl:button-on cpy-90)
		   #'(lambda (bm btn)
		       (declare (ignore btn))
		       (let* ((new-beam (copy bm))
			      (blklist (coll:elements (blocks new-beam))))
			 (setf (name new-beam)
			   (format nil "~A" (gensym "BEAM-")))
			 (dolist (blk blklist)
			   (coll:delete-element blk (blocks new-beam)))
			 (setf (gantry-angle new-beam)
			   (mod (+ (gantry-angle bm) 90.0) 360.0))
			 (setf (id (wedge new-beam)) 0)
			 (if (typep (collimator new-beam) 'portal-coll)
			     (setf (vertices (collimator new-beam))
			       ;; back to 10 by 10
			       '((-5.0 -5.0) (5.0 -5.0)
				 (5.0 5.0) (-5.0 5.0))))
			 (coll:insert-element new-beam
					      (beams (plan-of bp))))))
    (ev:add-notify b (sl:button-on cpy-180)
		   #'(lambda (bm btn)
		       (declare (ignore btn))
		       (coll:insert-element (reflected-beam bm)
					    (beams (plan-of bp)))))
    (ev:add-notify b (sl:button-on cpy-270)
		   #'(lambda (bm btn)
		       (declare (ignore btn))
		       (let* ((new-beam (copy bm))
			      (blklist (coll:elements (blocks new-beam))))
			 (setf (name new-beam)
			   (format nil "~A" (gensym "BEAM-")))
			 (dolist (blk blklist)
			   (coll:delete-element blk (blocks new-beam)))
			 (setf (gantry-angle new-beam)
			   (mod (+ (gantry-angle bm) 270.0) 360.0))
			 (setf (id (wedge new-beam)) 0)
			 (if (typep (collimator new-beam) 'portal-coll)
			     (setf (vertices (collimator new-beam))
			       ;; back to 10 by 10
			       '((-5.0 -5.0) (5.0 -5.0)
				 (5.0 5.0) (-5.0 5.0))))
			 (coll:insert-element new-beam
					      (beams (plan-of bp))))))
    (ev:add-notify bp (new-mu b)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (mu-box pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:new-info mu-t)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (monitor-units (beam-of bp))
			   (coerce (read-from-string info) 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-n-treats b)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (nfrac-box pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:new-info nfrac-t)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (n-treatments (beam-of bp))
			   (round (read-from-string info)))
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-name b)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (name-box pan))
			   (listify info 10))
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:new-info name-t)
		   #'(lambda (pan box)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (name (beam-of bp))
			   (apply #'concatenate 'string
				  (mapcar
				   #'(lambda (str)
				       (replace (make-string
						 10 :initial-element
						 #\Space) str))
				   (sl:info box))))
			 (setf (busy pan) nil))))
    (ev:add-notify b (sl:button-on mach-b)
		   #'(lambda (bm btn)
		       (let* ((machines (get-therapy-machine-list
					 *machine-index-directory*))
			      (new-mach (sl:popup-menu machines)))
			 (if new-mach (setf (machine-name bm)
					(nth new-mach machines))))
		       (setf (sl:on btn) nil)))
    (ev:add-notify bp (new-machine b)
		   #'(lambda (pan bm mach)
		       (setf (sl:label (machine-b pan)) mach)
		       (let ((cp (coll-pan pan))
			     (coll (collimator bm))
			     (frm (panel-frame pan)))
			 (unless (eq coll (coll-for cp))
			   (destroy cp)
			   (setf (coll-pan pan)
			     (make-collimator-panel
			      coll
			      :beam-of bm
			      :plan-of (plan-of pan)
			      :patient-of (patient-of pan)
			      :ulc-x dsx :ulc-y 360
			      :parent (sl:window frm)
			      :font (sl:font frm)))))))
    (ev:add-notify b (sl:button-on col-b)
		   #'(lambda (bm btn)
		       (let ((new-col (sl:popup-color-menu)))
			 (if new-col (setf (display-color bm) new-col)))
		       (setf (sl:on btn) nil)))
    (ev:add-notify bp (new-color b)
		   #'(lambda (pan bm col)
		       (declare (ignore bm))
		       (setf (sl:fg-color (color-b pan)) col)))
    (ev:add-notify bp (new-id (wedge b))
		   #'(lambda (pan wdg id)
		       (declare (ignore wdg))
                       (setf (sl:label (wedge-sel-b pan))
			 (wedge-label id (machine (beam-of pan))))))
    (ev:add-notify bp (new-rotation (wedge b))
		   #'(lambda (pan wdg rot)
		       (setf (sl:label (wedge-ang-b pan))
			 (format nil "Wdg Rot: ~a"
				 (if (zerop (id wdg))
				     "" rot)))))
    (ev:add-notify b (sl:button-on wdg-sb)
		   #'(lambda (bm btn)
		       (let* ((mach (machine bm))
			      (namelist (wedge-names mach))
			      (new-wdg-name (sl:popup-menu namelist)))
			 (when new-wdg-name
			   (let ((newid (wedge-id-from-name
					 (nth new-wdg-name namelist)
					 mach)))
			     (setf (id (wedge bm)) newid)
			     ;; set rotation every time so display updates
			     (setf (rotation (wedge bm))
			       (if (= newid 0) 0.0
				 (if (find (rotation (wedge bm))
					   (wedge-rot-angles newid mach))
				     (rotation (wedge bm))
				   (first (wedge-rot-angles newid mach)))))))
			 (setf (sl:on btn) nil))))
    (ev:add-notify b (sl:button-on wdg-ab)
		   #'(lambda (bm btn)
		       (let* ((wdg (wedge bm))
			      (id (id wdg)))
			 (if (zerop id)
			     (sl:acknowledge "Please select a wedge first.")
			   (let* ((angles (wedge-rot-angles id (machine bm)))
				  (pos (sl:popup-menu
					(mapcar #'write-to-string angles)))
				  (ang (when pos (nth pos angles))))
			     (when ang (setf (rotation wdg) ang)))))
		       (setf (sl:on btn) nil)))
    (ev:add-notify bp (new-arc-size b)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (arc-box pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:new-info arc-t)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (arc-size (beam-of bp))
			   (coerce (read-from-string info) 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-atten-factor b)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (atten-box pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:new-info atten-t)
		   #'(lambda (pan a info)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (atten-factor (beam-of bp))
			   (coerce (read-from-string info) 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-gantry-angle b)
		   #'(lambda (pan a ang)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:angle (gantry-db pan)) ang)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:value-changed gty-d)
		   #'(lambda (pan a ang)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (gantry-angle (beam-of bp)) ang)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-coll-angle b)
		   #'(lambda (pan a ang)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:angle (coll-db pan)) ang)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:value-changed col-d)
		   #'(lambda (pan a ang)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (collimator-angle (beam-of bp)) ang)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-couch-angle b)
		   #'(lambda (pan a ang)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:angle (couch-db pan)) ang)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:value-changed cch-d)
		   #'(lambda (pan a ang)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (couch-angle (beam-of bp)) ang)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-couch-ht b)
		   #'(lambda (pan a val)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (couch-ht-sl pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:value-changed cht-s)
		   #'(lambda (pan a val)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (couch-height (beam-of bp)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-couch-long b)
		   #'(lambda (pan a val)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (couch-long-sl pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:value-changed clng-s)
		   #'(lambda (pan a val)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (couch-longitudinal (beam-of bp)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (new-couch-lat b)
		   #'(lambda (pan a val)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (couch-lat-sl pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:value-changed clat-s)
		   #'(lambda (pan a val)
		       (declare (ignore a))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (couch-lateral (beam-of pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify bp (sl:button-on blk-bt)
		   #'(lambda (pan btn)
		       (setf (block-pan pan) (make-block-panel
					      (beam-of pan) (plan-of pan)
					      (patient-of pan)))
		       (ev:add-notify pan (deleted (block-pan pan))
				      #'(lambda (pnl blpnl)
					  (declare (ignore blpnl))
					  (setf (block-pan pnl) nil)
					  (when (not (busy pnl))
					    (setf (busy pnl) t)
					    (setf (sl:on btn) nil)
					    (setf (busy pnl) nil))))))
    (ev:add-notify bp (sl:button-off blk-bt)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (destroy (block-pan pan))
			 (setf (busy pan) nil))))
    (ev:add-notify b (sl:button-on tog-axis-b)
                   #'(lambda (bm btn)
                       (setf (display-axis bm) (not (display-axis bm)))
                       (if (display-axis bm)
			   (setf (sl:label tog-axis-b) "Axis ON")
                         (setf (sl:label tog-axis-b) "Axis OFF"))
                       (setf (sl:on btn) nil)))))

;;;---------------------------------------------

(defmethod destroy :before ((bp beam-panel))

  "Releases X resources used by this panel and its children."

  (destroy (coll-pan bp))
  (sl:destroy (delete-b bp))
  (sl:destroy (copy-b bp))
  (sl:destroy (copy-90 bp))
  (sl:destroy (copy-180 bp))
  (sl:destroy (copy-270 bp))
  (sl:destroy (mu-box bp))
  (sl:destroy (name-box bp))
  (sl:destroy (atten-box bp))
  (sl:destroy (arc-box bp))
  (sl:destroy (nfrac-box bp))
  (sl:destroy (machine-b bp))
  (sl:destroy (color-b bp))
  (sl:destroy (wedge-sel-b bp))
  (sl:destroy (wedge-ang-b bp))
  (sl:destroy (toggle-axis-b bp))
  (if (sl:on (block-btn bp)) (setf (sl:on (block-btn bp)) nil))
  (sl:destroy (block-btn bp))
  (sl:destroy (coll-db bp))
  (sl:destroy (gantry-db bp))
  (sl:destroy (couch-db bp))
  (sl:destroy (couch-lat-sl bp))
  (sl:destroy (couch-long-sl bp))
  (sl:destroy (couch-ht-sl bp))
  (sl:destroy (panel-frame bp))
  (ev:remove-notify bp (new-name (beam-of bp)))
  (ev:remove-notify bp (new-machine (beam-of bp)))
  (ev:remove-notify bp (new-mu (beam-of bp)))
  (ev:remove-notify bp (new-n-treats (beam-of bp)))
  (ev:remove-notify bp (new-gantry-angle (beam-of bp)))
  (ev:remove-notify bp (new-coll-angle (beam-of bp)))
  (ev:remove-notify bp (new-couch-angle (beam-of bp)))
  (ev:remove-notify bp (new-couch-ht (beam-of bp)))
  (ev:remove-notify bp (new-couch-long (beam-of bp)))
  (ev:remove-notify bp (new-couch-lat (beam-of bp)))
  (ev:remove-notify bp (new-color (beam-of bp)))
  (ev:remove-notify bp (new-id (wedge (beam-of bp))))
  (ev:remove-notify bp (new-rotation (wedge (beam-of bp))))
  (ev:remove-notify bp (new-arc-size (beam-of bp)))
  (ev:remove-notify bp (new-atten-factor (beam-of bp))))

;;;---------------------------------------------
;;; End.
