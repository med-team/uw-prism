;;;
;;; anatomy-tree
;;;
;;; 31-Jan-1991 C. Sweeney and B. Lockyear added instances of anatomy-tree
;;;             with class definition, removed load-anatomy-tree function, 
;;;             and added initialize-instance method.
;;; 7-Feb-1991  C. Sweeney fix typo and change names to symbols.
;;; 16-Feb-1991 I. Kalet move the within function to here, from prototypes
;;;             also, correct some spelling errors in node names
;;; 29-Mar-1991 I. Kalet add print-tree
;;; 11-Apr-1991 I. Kalet add within-p ruler function here - used by both
;;;             generate-prototypes and generate-target
;;; 21-Nov-1991 I. Kalet take out use-package
;;; 30-Jul-1993 I. Kalet change package to autoplan.
;;; 22-Mar-1994 J. Unger change 'site to 'pr:site in within-p definition
;;; 24-Mar-1994 J. Unger move anatomy-tree into prism package, change 
;;;             'pr:site back to 'site & pr:tab-print to tab-print.
;;; 28-Mar-1994 J. Unger move within-p to margin-rules.
;;; 12-Jan-1995 I. Kalet move print-tree export to prism-system
;;; 13-Sep-2005 I. Kalet add assertions for Graham inference code.
;;; This module now depends on file-functions (for tab-print) and on
;;; the inference module (with added macros).
;;; 25-Jun-2008 I. Kalet move use-package call to prism defpackage
;;;

(in-package :prism)

;;--------------------------------------
;;; definition of anatomy tree nodes 
;;;--------------------------------------

(defclass anatomy-tree-node ()

  ((name :initarg :name
	 :accessor name)

   (part-of :initarg :part-of
	    :accessor part-of)

   (parts :initarg :parts
	  :accessor parts
	  :documentation "A list of symbols naming daughter nodes")

   )

  (:default-initargs :name nil :part-of nil :parts nil)

  (:documentation "Each instance represents a single anatomic site or
larger anatomic region.  The tree structure implied by the parts and
part-of slots describes anatomic relationships of tumor sites and
groups of tumor sites.")

  )

;;;--------------------------------------

(defmethod initialize-instance :after ((anode anatomy-tree-node)
                                       &key name &allow-other-keys)
  "This method makes the node available by name"
  (set name anode)
  (assert-value 'part-of (name anode) (part-of anode))
  )

;;;-------------------------------------------
;;; rules that establish the subpart relation
;;; which is the transitive closure of part-of
;;;-------------------------------------------

(<- (same ?x ?x))
(<- (subpart ?x ?y) (same ?x ?y))
(<- (subpart ?x ?y) (AND (part-of ?x ?z)
			 (subpart ?z ?y)))

;;;-------------------------------------------
;;; The within relation applies to an entity that has
;;; a site property, which should be asserted separately.
;;;-------------------------------------------

(<- (within ?x ?y) (AND (site ?x ?z)
			(subpart ?z ?y)))

;;;-------------------------------------------
;;; This is a functional version of within that
;;; only applies to anatomy tree nodes.
;;;-------------------------------------------

(defun within (here there)

  "WITHIN here there

takes two anatomy tree node symbols and determines if THERE is equal
to or an ancestor of HERE.  Returns true if so, false otherwise.
Error occurs if HERE or THERE is not a valid anatomy tree node."

  (cond ((null here) nil)
        ((eql here there) t)
        (t (within (slot-value (symbol-value here) 'part-of) there))))

;;;--------------------------------------

(defun print-tree (node &key (indent 0) (stream t))

  "PRINT-TREE node &key (indent 0) (stream t)

Prints the name of node and all its sub-nodes recursively, to the
indicated stream, default is *standard-output*, indenting by the
number of spaces indicated by indent, at each level of subnodes.
Returns nil."

  (when node
    (tab-print (name node) stream indent t)
    (mapc #'(lambda (x) (print-tree (symbol-value x) 
                                    :indent (+ indent 3) :stream stream))
          (parts node))
    t))

;;;----------------------------------------

;;;---- the actual anatomy tree nodes follow ----
  
(make-instance 'anatomy-tree-node  
  :name  'LUNG  
  :part-of  'BODY  
  :parts  NIL  
)
(make-instance 'anatomy-tree-node  
  :name  'THYROID-GLAND
  :part-of  'HEAD-AND-NECK  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'CRANIAL-NERVE-FORAMEN
  :part-of  'BASE-OF-SKULL  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'MASTOID-AIR-CELLS
  :part-of  'BASE-OF-SKULL  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PTERYGOID-PLATES  
  :part-of  'BASE-OF-SKULL  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PTERYGOID-MUSCLE  
  :part-of  'BASE-OF-SKULL  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'BASE-OF-SKULL  
  :part-of  'HEAD-AND-NECK  
  :parts  '(CRANIAL-NERVE-FORAMEN MASTOID-AIR-CELLS
    PTERYGOID-PLATES PTERYGOID-MUSCLE)  
)  
(make-instance 'anatomy-tree-node  
  :name  'BONE  
  :part-of  'HEAD-AND-NECK  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'NECK  
  :part-of  'HEAD-AND-NECK  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'ORBIT  
  :part-of  'HEAD-AND-NECK  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SKIN  
  :part-of  'HEAD-AND-NECK  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'ORBITAL-BODY  
  :part-of  'GLOMUS-BODY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'CAROTID-BODY  
  :part-of  'GLOMUS-BODY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'VAGAL-BODY  
  :part-of  'GLOMUS-BODY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SUPERIOR-LARYNGEAL-GLOMUS-BODY
  :part-of  'GLOMUS-BODY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'TEMPORAL-BONE-GLOMUS-BODY
  :part-of  'GLOMUS-BODY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'GLOMUS-BODY  
  :part-of  'HEAD-AND-NECK  
  :parts  '(ORBITAL-BODY CAROTID-BODY VAGAL-BODY
    SUPERIOR-LARYNGEAL-GLOMUS-BODY TEMPORAL-BONE-GLOMUS-BODY)  
  )
(make-instance 'anatomy-tree-node  
  :name  'INNER-EAR  
  :part-of  'EAR  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PETROMASTOID
  :part-of  'MIDDLE-EAR  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'MIDDLE-EAR  
  :part-of  'EAR  
  :parts  '(PETROMASTOID)  
)  
(make-instance 'anatomy-tree-node  
  :name  'EXTERNAL-AUDITORY-CANAL
  :part-of  'EXTERNAL-EAR  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'AURICLE
  :part-of  'EXTERNAL-EAR  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'EXTERNAL-EAR
  :part-of  'EAR  
  :parts  '(EXTERNAL-AUDITORY-CANAL AURICLE)  
)  
(make-instance 'anatomy-tree-node  
  :name  'EAR  
  :part-of  'HEAD-AND-NECK  
  :parts  '(INNER-EAR MIDDLE-EAR EXTERNAL-EAR)  
)  
(make-instance 'anatomy-tree-node  
  :name  'MINOR-SALIVARY-GLANDS
  :part-of  'SALIVARY-GLANDS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SUBMANDIBULAR
  :part-of  'MAJOR-SALIVARY-GLANDS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SUBLINGUAL-GLANDS
  :part-of  'MAJOR-SALIVARY-GLANDS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PAROTID
  :part-of  'MAJOR-SALIVARY-GLANDS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'MAJOR-SALIVARY-GLANDS
  :part-of  'SALIVARY-GLANDS  
  :parts  '(SUBMANDIBULAR SUBLINGUAL-GLANDS PAROTID)  
)  
(make-instance 'anatomy-tree-node  
  :name  'SALIVARY-GLANDS
  :part-of  'HEAD-AND-NECK  
  :parts  '(MINOR-SALIVARY-GLANDS MAJOR-SALIVARY-GLANDS)  
)  
(make-instance 'anatomy-tree-node  
  :name  'SPHENOIDAL-SINUS
  :part-of  'PARANASAL-SINUSES  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'ETHMOIDAL-SINUS
  :part-of  'PARANASAL-SINUSES  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'FRONTAL-SINUS
  :part-of  'PARANASAL-SINUSES  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'MAXILLARY-SINUS
  :part-of  'PARANASAL-SINUSES  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PARANASAL-SINUSES
  :part-of  'NASAL-FOSSA-AND-SINUSES  
  :parts  '(SPHENOIDAL-SINUS ETHMOIDAL-SINUS FRONTAL-SINUS
    MAXILLARY-SINUS)  
)  
(make-instance 'anatomy-tree-node  
  :name  'NASAL-FOSSA
  :part-of  'NASAL-FOSSA-AND-SINUSES  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'NASAL-FOSSA-AND-SINUSES
  :part-of  'HEAD-AND-NECK  
  :parts  '(PARANASAL-SINUSES NASAL-FOSSA)  
)  
(make-instance 'anatomy-tree-node  
  :name  'AERYTENOIDS
  :part-of  'SUPRAGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'FALSE-CORD
  :part-of  'SUPRAGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'VENTRICLE
  :part-of  'SUPRAGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PHARYNGEAL-EPIGLOTTIC-FOLD
  :part-of  'SUPRAGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'AERYEPIGLOTTIC-FOLD
  :part-of  'SUPRAGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'EPIGLOTTIS
  :part-of  'SUPRAGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SUPRAGLOTTIS
  :part-of  'LARYNX  
  :parts  '(AERYTENOIDS FALSE-CORD VENTRICLE
    PHARYNGEAL-EPIGLOTTIC-FOLD AERYEPIGLOTTIC-FOLD
    EPIGLOTTIS)  
)  
(make-instance 'anatomy-tree-node  
  :name  'TRACHEA
  :part-of  'SUBGLOTTIS  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SUBGLOTTIS
  :part-of  'LARYNX  
  :parts  '(TRACHEA)  
)  
(make-instance 'anatomy-tree-node  
  :name  'GLOTTIS
  :part-of  'LARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'LARYNX
  :part-of  'HEAD-AND-NECK  
  :parts  '(SUPRAGLOTTIS SUBGLOTTIS GLOTTIS)  
)  
(make-instance 'anatomy-tree-node  
  :name  'LOWER-PHARYNGEAL-WALL
  :part-of  'HYPOPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'POSTERICOID
  :part-of  'HYPOPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PYRIFORM-SINUS
  :part-of  'HYPOPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'HYPOPHARYNX
  :part-of  'PHARYNX  
  :parts  '(LOWER-PHARYNGEAL-WALL POSTERICOID PYRIFORM-SINUS)  
)  
(make-instance 'anatomy-tree-node  
  :name  'LATERAL-PHARYNGEAL-WALL
  :part-of  'PHARYNGEAL-WALL  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'POST-PHARYNGEAL-WALL
  :part-of  'PHARYNGEAL-WALL  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'PHARYNGEAL-WALL
  :part-of  'OROPHARYNX  
  :parts  '(LATERAL-PHARYNGEAL-WALL POST-PHARYNGEAL-WALL)  
)  
(make-instance 'anatomy-tree-node  
  :name  'UVULA
  :part-of  'OROPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'BASE-OF-TONGUE
  :part-of  'OROPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'SOFT-PALATE
  :part-of  'OROPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'POSTERIOR-TONSILLAR-PILLAR
  :part-of  'TONSILLAR-PILLAR  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'ANTERIOR-TONSILLAR-PILLAR
  :part-of  'TONSILLAR-PILLAR  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'TONSILLAR-PILLAR
  :part-of  'TONSIL-AND-FOSSA  
  :parts  '(POSTERIOR-TONSILLAR-PILLAR ANTERIOR-TONSILLAR-PILLAR)  
)  
(make-instance 'anatomy-tree-node  
  :name  'TONSILLAR-FOSSA
  :part-of  'TONSIL-AND-FOSSA  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'TONSIL
  :part-of  'TONSIL-AND-FOSSA  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'TONSIL-AND-FOSSA
  :part-of  'OROPHARYNX  
  :parts  '(TONSILLAR-PILLAR TONSILLAR-FOSSA TONSIL)  
)  
(make-instance 'anatomy-tree-node  
  :name  'OROPHARYNX
  :part-of  'PHARYNX  
  :parts  '(PHARYNGEAL-WALL UVULA BASE-OF-TONGUE SOFT-PALATE
    TONSIL-AND-FOSSA)  
)  
(make-instance 'anatomy-tree-node  
  :name  'POSTERIOR-SUPERIOR-WALL
  :part-of  'NASOPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'LATERAL-WALL
  :part-of  'NASOPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'INFERIOR-WALL
  :part-of  'NASOPHARYNX  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'NASOPHARYNX
  :part-of  'PHARYNX  
  :parts  '(POSTERIOR-SUPERIOR-WALL LATERAL-WALL INFERIOR-WALL)  
)  
(make-instance 'anatomy-tree-node  
  :name  'PHARYNX
  :part-of  'HEAD-AND-NECK  
  :parts  '(HYPOPHARYNX OROPHARYNX NASOPHARYNX)  
)  
(make-instance 'anatomy-tree-node  
  :name  'MOBILE-TONGUE
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'RETROMOLAR-TRIGONE
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'LIP
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'HARD-PALATE
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'GINGIVA
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'FLOOR-OF-MOUTH
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'BUCCAL-MUCOSA
  :part-of  'ORAL-CAVITY  
  :parts  NIL  
)  
(make-instance 'anatomy-tree-node  
  :name  'ORAL-CAVITY
  :part-of  'HEAD-AND-NECK  
  :parts  '(MOBILE-TONGUE RETROMOLAR-TRIGONE LIP HARD-PALATE
    GINGIVA FLOOR-OF-MOUTH BUCCAL-MUCOSA)  
)  
(make-instance 'anatomy-tree-node  
  :name  'HEAD-AND-NECK
  :part-of  'BODY 
  :parts  '(THYROID-GLAND BASE-OF-SKULL BONE NECK ORBIT
    SKIN GLOMUS-BODY EAR SALIVARY-GLANDS
    NASAL-FOSSA-AND-SINUSES LARYNX PHARYNX
    ORAL-CAVITY)  
)  
(make-instance 'anatomy-tree-node  
  :name  'BODY
  :part-of  NIL  
  :parts  '(LUNG HEAD-AND-NECK)  
)  

;;;--------------------------------------
