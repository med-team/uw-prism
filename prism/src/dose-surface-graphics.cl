;;;
;;; dose-surface-graphics
;;;
;;; Draw methods for dose-surfaces into views.
;;;
;;; 18-Oct-1993 J. Unger create from earlier prototype.
;;; 22-Oct-1993 J. Unger fix bug in coronal view dose extraction view.
;;; 03-Dec-1993 J. Unger fix bug in draw method for dose surfaces.
;;;  9-Feb-1994 J. Unger modify parameter list of extract-dose-slice
;;;             and calls so it can be used elsewhere.
;;;  8-Apr-1994 I. Kalet split off from dose-graphics
;;; 18-Apr-1994 I. Kalet updated refs to view origin
;;;  5-May-1994 J. Unger changed 'valid' to 'valid-grid'
;;; 16-Jun-1994 I. Kalet changed color in dose surface to display-color
;;; 14-Jul-1994 J. Unger change (or t nil) to (member t nil).
;;; 28-Jul-1994 J. Unger fix bug(s) in update-dose-caches methods.
;;; 31-Aug-1995 I. Kalet change defparameter to defvar for caches.
;;; 19-Sep-1996 I. Kalet remove &rest from draw methods.
;;;  6-Dec-1996 I. Kalet don't generate prims if color is invisible
;;; 13-May-1998 I. Kalet move max-plane-dose here from plots, also
;;; some minor cleanup.
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible.
;;; 25-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;; 22-Oct-2002 I. Kalet add stubs for oblique view and room view.
;;; 25-May-2009 I. Kalet remove stub for room view.
;;;

(in-package :prism)

;;;---------------------------------------------

(defvar *x-dose-unmarked-array* 
  (make-array '(10 10) :element-type '(member t nil))
  "The unmarked array used for sagittal view isodose contour extraction.")

(defvar *y-dose-unmarked-array* 
  (make-array '(10 10) :element-type '(member t nil))
  "The unmarked array used for coronal view isodose contour extraction.")

(defvar *z-dose-unmarked-array* 
  (make-array '(10 10) :element-type '(member t nil))
  "The unmarked array used for transverse view isodose contour extraction.")

(defvar *oblique-dose-unmarked-array* 
  (make-array '(10 10) :element-type '(member t nil))
  "The unmarked array used for oblique view isodose contour extraction.")

(defvar *x-dose-slice-array*
  (make-array '(10 10) :element-type 'single-float)
  "A 2D slice of the 3D dose grid in the sagittal direction.")

(defvar *y-dose-slice-array*
  (make-array '(10 10) :element-type 'single-float)
  "A 2D slice of the 3D dose grid in the coronal direction.")

(defvar *z-dose-slice-array*
  (make-array '(10 10) :element-type 'single-float)
  "A 2D slice of the 3D dose grid in the transverse direction.")

(defvar *oblique-dose-slice-array*
  (make-array '(10 10) :element-type 'single-float)
  "A 2D slice of the 3D dose grid in an oblique direction.")

;;;---------------------------------------------

(defmethod extract-dose-slice (dg dr (v transverse-view) &key slice)

  "Extracts slice, a 2D array of single-floats, from the dose array
within dose result dr.  The plane removed from dr's dose array
corresponds to the intersection of the transverse-view v's plane with
the supplied dose grid dg (a grid geometry) in patient space.  Slice
is a keyword and is optional - if supplied, then that array will be
filled with the resulting slice information.  Otherwise, a new array
will be allocated.  Multiple values are returned -- in order,

  in-bounds slice x-orig y-orig x-size y-size

  where in-bounds is t if the view intersected the grid and nil otherwise,
        slice is the 2D array of float values extracted from a grid plane
        x-orig is the x origin of the slice in patient space 
        y-orig is the y origin of the slice in patient space
        x-size is the x size of the plane
        y-size is the y size of the plane

If in-bounds is nil, all other values retured are undefined.

The slice array is obtained through linear interpolation between the two
nearest grid planes."

  (let* ((pos (view-position v))
         (index  (float (/ (* (- pos (z-origin dg))
			      (1- (z-dim dg)))
			   (z-size dg))))
         (l-ind  (floor index))
         (h-ind  (1+ l-ind))
         (l-fac  (- h-ind index))
         (h-fac  (- 1 l-fac))
         (x-dim  (x-dim dg))
         (y-dim  (y-dim dg))
         (dm     (grid dr)))
    (unless slice 
      (setf slice (make-array (list x-dim y-dim)
			      :element-type 'single-float)))
    (when (= index (1- (z-dim dg))
	     (decf h-ind)))
    (when (<= 0.0 index (1- (z-dim dg)))             
      (dotimes (i x-dim)
        (dotimes (j y-dim)
          (setf (aref slice i j) 
            (+ (* l-fac (aref dm i j l-ind)) 
               (* h-fac (aref dm i j h-ind))))))
      (values
       t slice (x-origin dg) (y-origin dg)
       (x-size dg) (y-size dg)))))

;;;---------------------------------------------

(defmethod extract-dose-slice (dg dr (v coronal-view) &key slice)

  "Extracts slice (a 2D dose array) from the matrix within dr,
according to how the plane of the coronal view v intersects the
supplied grid geometry dg in patient space."

  (let* ((pos (view-position v))
         (index  (float (/ (* (- pos (y-origin dg))
			      (1- (y-dim dg)))
			   (y-size dg))))
         (l-ind  (floor index))
         (h-ind  (1+ l-ind))
         (l-fac  (- h-ind index))
         (h-fac  (- 1 l-fac))
         (x-dim  (x-dim dg))
         (y-dim  (z-dim dg))
         (dm     (grid dr)))
    (unless slice 
      (setf slice (make-array (list x-dim y-dim)
			      :element-type 'single-float)))
    (when (= index (1- (y-dim dg)) (decf h-ind)))
    (when (<= 0.0 index (1- (y-dim dg)))             
      (dotimes (i x-dim)
        (dotimes (j y-dim)
          (setf (aref slice i (- y-dim j 1))
            (+ (* l-fac (aref dm i l-ind j)) 
               (* h-fac (aref dm i h-ind j))))))
      (values
       t slice (x-origin dg) (- (+ (z-origin dg) (z-size dg)))
       (x-size dg) (z-size dg)))))
  
;;;---------------------------------------------

(defmethod extract-dose-slice (dg dr (v sagittal-view) &key slice)

  "Extracts slice (a 2D dose array) from the matrix within dr,
according to how the plane of the sagittal view v intersects the
supplied grid geometry dg in patient space."

  (let* ((pos (view-position v))
         (index (float (/ (* (- pos (x-origin dg))
			     (1- (x-dim dg)))
			  (x-size dg))))
         (l-ind  (floor index))
         (h-ind  (1+ l-ind))
         (l-fac  (- h-ind index))
         (h-fac  (- 1 l-fac))
         (x-dim  (z-dim dg))
         (y-dim  (y-dim dg))
         (dm     (grid dr)))
    (unless slice 
      (setf slice (make-array (list x-dim y-dim)
			      :element-type 'single-float)))
    (when (= index (1- (x-dim dg)) (decf h-ind)))
    (when (<= 0.0 index (1- (x-dim dg)))             
      (dotimes (i x-dim)
        (dotimes (j y-dim)
          (setf (aref slice i j) 
            (+ (* l-fac (aref dm l-ind j i)) 
               (* h-fac (aref dm h-ind j i))))))
      (values
       t slice (z-origin dg) (y-origin dg)
       (z-size dg) (y-size dg)))))
  
;;;---------------------------------------------

(defmethod extract-dose-slice (dg dr (v oblique-view) &key slice)

  "Stub method for now."

  (declare (ignore dg dr slice))
  nil)

;;;---------------------------------------------

(defmethod extract-dose-slice (dg dr (v beams-eye-view) &key slice)

  "Currently, dose planes are not extracted from beam's eye views, so
this method simply returns nil."

  (declare (ignore dg dr slice))
  nil)

;;;---------------------------------------------

(defun max-plane-dose (v grid result)

   "max-plane-dose v grid result

Computes the maximum dose in the plane of view v of given dose result
object and dose grid object.  If the dose result is invalid, or if the
plane of the view does not intersect the volume of space specified by
the dose grid, or if the view is a beam's eye view, 0 is returned."

   ;; Currently, the slice cache specified in the argument list to
   ;; extract-dose-slice is not being preallocated or reused.  If v is
   ;; a beam's eye view, extract-dose-slice will return nil.
   (let* ((in-bounds nil)
	  (slice nil)
	  (max 0))
     (when (valid-grid result)
       (multiple-value-setq 
	   (in-bounds slice) (extract-dose-slice grid result v)))
     (when in-bounds
       (dotimes (i (array-dimension slice 0))
	 (dotimes (j (array-dimension slice 1))
	   (when (< max (aref slice i j))
	     (setq max (aref slice i j))))))
     (round max)))

;;;---------------------------------------------

(defmethod update-dose-caches ((v transverse-view) arr)

  "Checks the dimensions of the dose array arr and ensures that the
appropriate *dose-slice-array* and *dose-unmarked-array* have the same
dimensions; if not, new cache arrays are allocated."

  (let ((x-dim (array-dimension arr 0))
        (y-dim (array-dimension arr 1)))
    (unless (and (= x-dim (array-dimension *z-dose-slice-array* 0))
                 (= y-dim (array-dimension *z-dose-slice-array* 1)))
      (setq *z-dose-slice-array*
        (make-array (list x-dim y-dim) :element-type 'single-float)))
    (unless (and (= x-dim (array-dimension *z-dose-unmarked-array* 0))
                 (= y-dim (array-dimension *z-dose-unmarked-array* 1)))
      (setq *z-dose-unmarked-array*
        (make-array (list x-dim y-dim) :element-type '(member t nil))))
    (values *z-dose-slice-array* *z-dose-unmarked-array*)))

;;;---------------------------------------------

(defmethod update-dose-caches ((v coronal-view) arr)

  "Checks the dimensions of the dose array arr and ensures that the
appropriate *dose-slice-array* and *dose-unmarked-array* have the same
dimensions; if not, new cache arrays are allocated."

  (let ((x-dim (array-dimension arr 0))
        (z-dim (array-dimension arr 2)))
    (unless (and (= x-dim (array-dimension *y-dose-slice-array* 0))
                 (= z-dim (array-dimension *y-dose-slice-array* 1)))
      (setq *y-dose-slice-array*
        (make-array (list x-dim z-dim) :element-type 'single-float)))
    (unless (and (= x-dim (array-dimension *y-dose-unmarked-array* 0))
                 (= z-dim (array-dimension *y-dose-unmarked-array* 1)))
      (setq *y-dose-unmarked-array*
        (make-array (list x-dim z-dim) :element-type '(member t nil))))
    (values *y-dose-slice-array* *y-dose-unmarked-array*)))

;;;---------------------------------------------

(defmethod update-dose-caches ((v sagittal-view) arr)

  "Checks the dimensions of the dose array arr and ensures that the
appropriate *dose-slice-array* and *dose-unmarked-array* have the same
dimensions; if not, new cache arrays are allocated."

  (let ((y-dim (array-dimension arr 1))
        (z-dim (array-dimension arr 2)))
    (unless (and (= z-dim (array-dimension *x-dose-slice-array* 0))
                 (= y-dim (array-dimension *x-dose-slice-array* 1)))
      (setq *x-dose-slice-array*
        (make-array (list z-dim y-dim) :element-type 'single-float)))
    (unless (and (= z-dim (array-dimension *x-dose-unmarked-array* 0))
                 (= y-dim (array-dimension *x-dose-unmarked-array* 1)))
      (setq *x-dose-unmarked-array*
        (make-array (list z-dim y-dim) :element-type '(member t nil))))
    (values *x-dose-slice-array* *x-dose-unmarked-array*)))

;;;---------------------------------------------

(defmethod update-dose-caches ((v oblique-view) arr)

  "Stub method for now."

  (declare (ignore arr))
  nil)

;;;---------------------------------------------

(defmethod update-dose-caches ((v beams-eye-view) arr)

  "Currently, no isodose curves are displayed in beam's eye views, so
this method simply returns nil."

  (declare (ignore arr))
  nil)

;;;---------------------------------------------

(defmethod draw ((ds dose-surface) (v view))

  "This method draws the isodose surface into a view."

  (if (eql (display-color ds) 'sl:invisible)
      (setf (foreground v) (remove ds (foreground v) :key #'object))
    (let ((prim (find ds (foreground v) :key #'object))
	  (color (sl:color-gc (display-color ds))))
      (unless prim 
	(setq prim (make-lines-prim nil color :object ds))
	(push prim (foreground v)))
      (setf (color prim) color
	    (points prim) nil)
      (when (valid-grid (result ds))
	(let ((slice-cache nil)
	      (unmarked-cache nil)
	      (in-bounds nil)
	      (slice nil)
	      (curves nil)
	      (x-orig 0.0)
	      (y-orig 0.0)
	      (x-size 0.0)
	      (y-size 0.0))
	  (multiple-value-setq
	      (slice-cache unmarked-cache)
	    (update-dose-caches v (grid (result ds))))
	  (multiple-value-setq 
	      (in-bounds slice x-orig y-orig x-size y-size)
	    (extract-dose-slice 
	     (dose-grid ds) (result ds) v :slice slice-cache))
	  (when in-bounds
	    (setq curves
	      (get-isodose-curves 
	       slice x-size y-size x-orig y-orig (threshold ds)
	       :unmarked unmarked-cache :complete t)))
	  (dolist (curve curves)
	    (push 
	     (pixel-contour curve (scale v) (x-origin v) (y-origin v))
	     (points prim))))))))

;;;---------------------------------------------
;;; End.
