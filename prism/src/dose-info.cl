;;;
;;; dose-info
;;;
;;; contains class definitions for measured and specified dose data for
;;; instances of therapy machines, e.g. tissue-phantom ratio data for
;;; photons and neutrons.
;;;
;;;  4-Jan-1996 I. Kalet created
;;; 29-Jan-1997 I. Kalet add details for wedge data, add table access
;;;    functions, put globals here.
;;;  1-May-1997 I. Kalet correct error in setup-beamdata
;;;  7-May-1997 BobGian inserted stub proclamations.  Removed 29-Aug-1997.
;;;  5-Jun-1997 I. Kalet machine returns object, not name
;;; 28-Aug-1997 BobGian massaging into form compatible with new dose calc.
;;;   Moved setup-beamdata to beam-dose and inlined it streamline code
;;;   and clarify intent.  Implementing accessors for new dose calc.
;;; 03-Sep-1997 BobGian completed and began testing.
;;; 15-Oct-1997 BobGian implement new wedge-info scheme.
;;; 20-Oct-1997 BobGian remove cal-depth slot - value used as comment only.
;;; 25-Oct-1997 BobGian remodel lookup fcns for wedge-info objects.
;;; 10-Nov-1997 BobGian add "the" declarations for speedup.
;;; 22-Jan-1998 BobGian update to major revision including direct-mapping
;;;   table lookups, inlining (via macro definition) of interpolation and
;;;   specialized multidimensional array access, and array declarations
;;;   to inline array accesses and avoid flonum boxing.  Add new slots
;;;   for mapper-arrays to photon-dose-info class defn.
;;; 09-Mar-1998 BobGian modest upgrade of dose-calc code.
;;; 13-Mar-1998 BobGian fix bug (fencepost error) in interpolate-delta.
;;;   Move excess outputfactor-related code to new file: output-factors
;;;   and excess table-lookup code to new file: table-lookups.  Move
;;;   wedege-info defclass and slot-type method here from therapy-machines.
;;;   Move function for building mapping tables here (from therapy-machines)
;;;   since it depends upon the slots it initializes.
;;; 28-Apr-1998 BobGian move BUILD-MAPPER-TABLES from here to
;;;   therapy-machines to resolve dependency conflict.
;;; 17-Dec-1998 I. Kalet revise electron-dose-info for new data
;;;organization.  Eliminate unnecessary base class.
;;;  2-Feb-1999 I. Kalet add electron-dose-parameters per Paul Cho's specs.
;;; 14-Jun-1999 I. Kalet further mods to electron-dose-info.
;;;  7-Jul-1999 I. Kalet add some interpolation functions for electron data.
;;; 03-Feb-2000 BobGian update type declarations in electron-dose-info defn
;;;   and add multidimensional interpolation functions for electron dosecalc.
;;; 02-Mar-2000 BobGian fix doc string for dd-tables in electron-dose-info;
;;;   fix fencepost errors and optimize in interpolation functions for
;;;   electron dose computation (depth-dose-interp, rof-interp,
;;;   ssd/fs-interp, and recursive-assoc).
;;; 25-Apr-2000 BobGian add slots to photon-dose-info for Irreg.
;;; 26-Apr-2000 BobGian fix conditionalization on optional Irreg slots.
;;;   Remove hvl and calibration-depth slots for Irreg -- tpr0 always used.
;;; 05-May-2000 BobGian add :initarg for all Irreg slots so none unbound.
;;; 30-May-2001 BobGian:
;;;   Wrap generic arithmetic with the-declared types.
;;;   Change type declarations for e0, rp, theta-air, f1, f2, z1, and z2 from
;;;     (simple-array single-float 2) to (simple-array t 2) to reflect their
;;;     true type as created by get-object.
;;; 13-Jun-2001 Ira Kalet add slots to wedge class that are used by
;;; DICOM-RT facility, PDR.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 27-Jun-2004 BobGian - remove all irreg-related slots: SOURCE-DIAMETER,
;;;   COLLIMATOR-CONSTANT, COLLIMATOR-TRANSMISSION, SOURCE-TRAY-DISTANCE,
;;;   PSF-TABLE-VECTOR, PSF-RADIUS-MAPPER, PSF-RADII, PSF-TABLE,
;;;   OAF-TABLE-VECTOR, OAF-RADIUS-MAPPER, OAF-RADII, OAF-TABLE.
;;;

(in-package :prism)

;;;=============================================================
;;; Photon External Beam Dose Calculation data and functions.

(defclass photon-dose-info ()

  ((cal-factor :type single-float
	       :initarg :cal-factor
	       :accessor cal-factor
	       :documentation "The absolute calibration in cGy per MU
at the calibration depth, for a 10 cm square field, usually 1.0")

   (portal-area-coeff :type single-float
		      :initarg :portal-area-coeff
		      :accessor portal-area-coeff
		      :documentation "Coefficient between 0.0 and 1.0
which determines weight of MLC area component of Output-Factor.")

   (of-min-diam :type single-float
		:initarg :of-min-diam
		:accessor of-min-diam
		:documentation "Parameter giving diameter of circular
field with Output-Factor which is minumum for MLC integrated component
when portal area is at least that of this circle.")

   (outputfactor-vector :type (simple-array single-float (3))
			:accessor outputfactor-vector)

   (outputfactor-fss-mapper :type (simple-array t 1)
			    :accessor outputfactor-fss-mapper)

   (outputfactor-fieldsizes :type (simple-array single-float 1)
			    :accessor outputfactor-fieldsizes
			    :documentation
			    "outputfactor-table 1st-index array.")

   (outputfactor-table :type (simple-array single-float 1)
		       :accessor outputfactor-table
		       :documentation "1-D array of relative Output-Factors.")

   (ocr-table-vector :type (simple-array single-float (9))
		     :accessor ocr-table-vector)

   (ocr-fss-mapper :type (simple-array t 1)
		   :accessor ocr-fss-mapper)

   (ocr-fieldsizes :type (simple-array single-float 1)
		   :accessor ocr-fieldsizes
		   :documentation "OCR-TABLE 1st-index array.")

   (ocr-depth-mapper :type (simple-array t 1)
		     :accessor ocr-depth-mapper)

   (ocr-depths :type (simple-array single-float 1)
	       :accessor ocr-depths
	       :documentation "OCR-TABLE 2nd-index array.")

   (ocr-fanline-mapper :type (simple-array t 1)
		       :accessor ocr-fanline-mapper)

   (ocr-fanlines :type (simple-array single-float 1)
		 :accessor ocr-fanlines
		 :documentation "OCR-TABLE 3rd-index array.")

   (ocr-table :type (simple-array t 1)
	      :accessor ocr-table
	      :documentation "3-D array of OCRs")

   (tpr-table-vector :type (simple-array single-float (6))
		     :accessor tpr-table-vector)

   (tpr-fss-mapper :type (simple-array t 1)
		   :accessor tpr-fss-mapper)

   (tpr-fieldsizes :type (simple-array single-float 1)
		   :accessor tpr-fieldsizes
		   :documentation "TPR-TABLE 1st-index array.")

   (tpr-depth-mapper :type (simple-array t 1)
		     :accessor tpr-depth-mapper)

   (tpr-depths :type (simple-array single-float 1)
	       :accessor tpr-depths
	       :documentation "TPR-TABLE 2nd-index array.")

   (tpr-table :type (simple-array t 1)
	      :accessor tpr-table
	      :documentation "2-D array of TPRs")

   (tpr0-table-vector :type (simple-array single-float (3))
		      :accessor tpr0-table-vector)

   (tpr0-depth-mapper :type (simple-array t 1)
		      :accessor tpr0-depth-mapper)

   (tpr0-depths :type (simple-array single-float 1)
		:accessor tpr0-depths
		:documentation "TPR0-TABLE 1st-index array.")

   (tpr0-table :type (simple-array single-float 1)
	       :accessor tpr0-table
	       :documentation "1-D array of zero-field-size TPRs")

   (spr-table-vector :type (simple-array single-float (6))
		     :accessor spr-table-vector)

   (spr-radius-mapper :type (simple-array t 1)
		      :accessor spr-radius-mapper)

   (spr-radii :type (simple-array single-float 1)
	      :accessor spr-radii
	      :documentation "spr-table 1st-index array.")

   (spr-depth-mapper :type (simple-array t 1)
		     :accessor spr-depth-mapper)

   (spr-depths :type (simple-array single-float 1)
	       :accessor spr-depths
	       :documentation "SPR-TABLE 2nd-index array.")

   (spr-table :type (simple-array t 1)
	      :accessor spr-table
	      :documentation "2-D array of SPRs")

   )

  (:documentation "The dose-info class for photon machines.")

  )

;;;=============================================================
;;; These arrays are all general [type T] as created by GET-OBJECT.
;;; They were declared "(simple-array single-float 2)" for later optimization,
;;; but I changed the decls to the types that they really are.

(defclass electron-dose-info ()

  ((airgap :accessor airgap
	   :type single-float
	   :documentation "The air gap in cm for this electron machine,
independent of energy or cone size.")

   (vsad :type list
	 :accessor vsad
	 :documentation "Virtual SAD, depends only on energy.")

   (applic-sizes :type list
		 :accessor applic-sizes
		 :documentation "The applicator sizes in cm at the
cutout level, not the cone-sizes, which are the nominal sizes at
isocenter.  Depends only on cone-size.")

   (rp :type (simple-array t 2)
       :accessor rp
       :documentation "The practical range in cm.  Depends on energy
and cone size.")

   (e0 :type (simple-array t 2)
       :accessor e0
       :documentation "The initial energy of the electron beam in MeV
in the cutout plane.  Depends on energy and cone size.")

   (theta-air :type (simple-array t 2)
	      :accessor theta-air
	      :documentation "The initial angular beam spread in air.
Depends on energy and cone size.")

   (f1 :type (simple-array t 2)
       :accessor f1
       :documentation "Correction factor for the lateral spatial spread
parameter, FMCS, at depth Z1.  Depends on energy and cone size.")

   (f2 :type (simple-array t 2)
       :accessor f2
       :documentation "FMCS factor at depth Z2.  Depends on energy and
cone size.")

   (z1 :type (simple-array t 2)
       :accessor z1
       :documentation "A shallow depth near the surface.  Depends on
energy and cone size.")

   (z2 :type (simple-array t 2)
       :accessor z2
       :documentation "A large depth near the practical range.
Depends on energy and cone size.")

   (depths :type list
	   :accessor depths
	   :documentation "A list of the maximum depths at which the
depth dose data are specified, a function only of energy.")

   (ssd :type list
	:accessor ssd
	:documentation "List of SSD's at which depth dose is provided.")

   (dd-tables :type list
	      :accessor dd-tables
	      :documentation "An array of depth doses by energy,
applicator size, nominal SSD and field size.  The typical SSD's are
100.0, 110.0 and 120.0, and the field sizes, specified in cm at isocenter,
range from some small value up to the applicator size.  Starts at 0.1 cm
depth and does NOT include the value 0.0 for depth zero.")

   (rof-tables :type list
	       :accessor rof-tables
	       :documentation "An array of relative output factors by
energy, applicator size, nominal SSD and field size.")

   )

  (:documentation "The dose-info class for electron machines.
Provides detailed parameters and depth dose tables for electrons, as a
function of energy and applicator size.  The available nominal
energies and cone sizes are specified in the electron-collimator-info
object in the collimator-info slot of the containing therapy machine.")

  )

;;;=============================================================
;;; Interpolation functions for Electron dose computation.

(defun depth-dose-interp (pdd-data energy-value aperture-value ssd-value
			  eff-width eff-length)

  "depth-dose-interp pdd-data energy-value aperture-value
		     ssd-value eff-width eff-length

returns an array of dose vs depth (starting at depth 0.0) for the
specified energy and cone size (as flonums), interpolated from PDD-DATA
for the specified SSD, field width, and field length.  PDD-DATA does NOT
include the 0.0 stored into the returned array zero-th slot."

  ;; For Percent-Depth-Dose, the value interpolated on SSD and fieldsize
  ;; is a list of floating-point numbers, each representing PDD values for
  ;; depths sampled at 0.1 centimeter intervals STARTING AT 0.1 CM.
  ;; SSD/FS-INTER returns such a list (of variable length), once for
  ;; each of EFF-WIDTH and EFF-LENGTH.  We compute the geometric mean
  ;; of the values pairwise in these lists and return a depth-dose array
  ;; of the results.  Size of returned array is ONE GREATER THAN length
  ;; of the smaller of the lists interpolated pairwise, since the 0.0
  ;; stuffed into slot 0 of the array is NOT stored in the data table.

  (declare (type single-float energy-value aperture-value ssd-value
		 eff-width eff-length))

  (do ((dd-values '())
       (width-values (ssd/fs-interp pdd-data energy-value aperture-value
				    ssd-value eff-width)
		     (cdr width-values))
       (length-values (ssd/fs-interp pdd-data energy-value aperture-value
				     ssd-value eff-length)
		      (cdr length-values)))
      ;; Is it necessary to check both, or will both lists always
      ;; have same length?
      ((or (null width-values)
	   (null length-values))
       ;; Returned array DOES include the explicit 0.0 in slot 0.
       (make-array (the fixnum (1+ (length dd-values)))
		   :element-type 'single-float
		   :initial-contents (cons 0.0 (nreverse dd-values))))

    (declare (type list dd-values width-values length-values))

    (push (the (single-float 0.0 *)
	    (sqrt (the (single-float 0.0 *)
		    (* (the single-float (car width-values))
		       (the single-float (car length-values))))))
	  dd-values)))

;;;-------------------------------------------------------------

(defun rof-interp (rof-data energy-value aperture-value ssd-value
		   eff-width eff-length)

  "rof-interp rof-data energy-value aperture-value
	      ssd-value eff-width eff-length

returns an ROF for the specified energy and cone size (as flonums),
interpolated for the specified SSD, field width, and field length."

  ;; For Relative-Output-Factor, the value interpolated on SSD and fieldsize
  ;; is a single floating-point number.  SSD/FS-INTER returns a list of length
  ;; one containing that value, one for each of EFF-WIDTH and EFF-LENGTH.
  ;; We return the geometric mean of these two values.

  (declare (type single-float energy-value aperture-value ssd-value
		 eff-width eff-length))

  (cond ((= eff-width eff-length)
	 (car (ssd/fs-interp rof-data energy-value aperture-value
			     ssd-value eff-width)))
	(t (the (single-float 0.0 *)
	     (sqrt (the (single-float 0.0 *)
		     (* (the single-float
			  (car (ssd/fs-interp rof-data energy-value
					      aperture-value ssd-value
					      eff-width)))
			(the single-float
			  (car (ssd/fs-interp rof-data energy-value
					      aperture-value ssd-value
					      eff-length))))))))))

;;;-------------------------------------------------------------
;;; NB: ASSOCs use #'= as tag comparison operation, and values being
;;; compared are SINGLE-FLOATs.  Be sure values written in data files
;;; are consistent so floating-point comparisons don't go astray.

(defun ssd/fs-interp (nested-alist energy-value aperture-value ssd-value
		      fieldsize &aux sublist1 sublist2 (ssd-frac 0.0))

  "ssd/fs-interp nested-alist energy-value aperture-value
		 ssd-value fieldsize

returns a single-level list representing the values in the nested
association list NESTED-ALIST extracted on discrete but flonum values
ENERGY-VALUE and APERTURE-VALUE and interpolated on continuous (flonum)
values SSD-VALUE and FIELDSIZE."


  (declare (type cons nested-alist)
	   (type list sublist1 sublist2)
	   (type single-float energy-value aperture-value ssd-value
		 fieldsize ssd-frac))

  ;; Program and data tables are designed to work for SSD between 100.0 and
  ;; 120.0, although program accepts SSD down to Electron-SSD-Minlength
  ;; (= 99.5), extrapolating flat, to allow for slight mis-placement
  ;; of isocenter.
  (cond ((<= ssd-value 100.0)
	 (setq sublist1 (cdr (assoc 100.0 nested-alist :test #'=))))
	((< ssd-value 110.0)
	 (setq sublist1 (cdr (assoc 100.0 nested-alist :test #'=))
	       sublist2 (cdr (assoc 110.0 nested-alist :test #'=))
	       ssd-frac (* 0.1 (- ssd-value 100.0))))
	((= ssd-value 110.0)
	 (setq sublist1 (cdr (assoc 110.0 nested-alist :test #'=))))
	((< ssd-value 120.0)
	 (setq sublist1 (cdr (assoc 110.0 nested-alist :test #'=))
	       sublist2 (cdr (assoc 120.0 nested-alist :test #'=))
	       ssd-frac (* 0.1 (- ssd-value 110.0))))
	(t (setq sublist1 (cdr (assoc 120.0 nested-alist :test #'=)))))

  (setq sublist1 (recursive-assoc energy-value aperture-value
				  fieldsize sublist1))

  (cond ((consp sublist2)
	 (list-interpolate ssd-frac
			   sublist1
			   (recursive-assoc energy-value aperture-value
					    fieldsize sublist2)))
	(t sublist1)))

;;;-------------------------------------------------------------
;;; NB: ASSOCs use #'= as tag comparison operation, and values being
;;; compared are SINGLE-FLOATs.  Be sure values written in data files
;;; are consistent so floating-point comparisons don't go astray.

(defun recursive-assoc (energy-value aperture-value fieldsize sublist)

  "recursive-assoc energy-value aperture-value fieldsize sublist

does a recursive ASSOC lookup in nested ALISTs SUBLIST based on
discrete but flonum values ENERGY-VALUE and APERTURE-VALUE, then
does an ASSOC-like continuous lookup on flonum-valued FIELDSIZE,
interpolating between nearest values if exact match fails, and
returning edge value if lookup falls off either side.  Object
returned is list of values in CDR of the innermost ALIST."

  (declare (type cons sublist)
	   (type single-float energy-value aperture-value fieldsize))

  (setq sublist (cdr (assoc aperture-value
			    (cdr (assoc energy-value sublist :test #'=))
			    :test #'=)))

  (do ((fs-sublists sublist (cdr fs-sublists))
       (old-fieldsize-tag 0.0 new-fieldsize-tag)
       (new-fieldsize-tag 0.0)
       (old-fs-sublist nil new-fs-sublist)
       (new-fs-sublist))
      ((null fs-sublists)
       ;; Ran off end - FIELDSIZE is larger than largest stored tag -
       ;; return sublist corresponding to largest stored tag.
       (cdr old-fs-sublist))

    (declare (type list fs-sublists old-fs-sublist new-fs-sublist)
	     (type single-float old-fieldsize-tag new-fieldsize-tag))

    (setq new-fs-sublist (car fs-sublists)
	  new-fieldsize-tag (car new-fs-sublist))

    (cond
      ((< fieldsize new-fieldsize-tag)
       (cond
	 ((eq fs-sublists sublist)
	  ;; FIELDSIZE smaller than smallest tag - return sublist
	  ;; for smallest stored fieldsize value.
	  (return (cdr new-fs-sublist)))
	 ;; FIELDSIZE is between two tags - interpolate.
	 (t (return
	      (list-interpolate (/ (- fieldsize old-fieldsize-tag)
				   (- new-fieldsize-tag old-fieldsize-tag))
				(cdr old-fs-sublist)
				(cdr new-fs-sublist))))))
      ((= fieldsize new-fieldsize-tag)
       ;; Exact match - return corresponding fieldsize sublist.
       (return (cdr new-fs-sublist))))))

;;;-------------------------------------------------------------

(defun list-interpolate (fraction sublist1 sublist2)

  "list-interpolate fraction sublist1 sublist2

takes a fraction (between 0.0 and 1.0) and returns a list of values, each
interpolated that fractional amount between the values which are corresponding
elements of the lists SUBLIST1 and SUBLIST2.  If list inputs differ in length,
output list contains as many elements as does the shorter input list."

  (declare (type cons sublist1 sublist2)
	   (type (single-float 0.0 1.0) fraction))

  (mapcar #'(lambda (x-val y-val)
	      (declare (type single-float x-val y-val))
	      (+ (* fraction y-val)
		 (* (- 1.0 fraction) x-val)))
    sublist1
    sublist2))

;;;=============================================================

(defclass wedge-info ()

  ((name :type string
	 :initarg :name
	 :accessor name
	 :documentation "A unique short string identifying which
particular wedge this data set describes.")

   (id :initarg :id
       :accessor id
       :documentation "A unique id, that is put in the id slot in the
wedge object to identify which wedge is in use.  Wedge id's are
numbers in many data files, but the numeric value has no significance
as a number, except that a wedge id of 0 in a beam means no wedge in
the beam.")

   (accessory-code :initarg :accessory-code
		   :accessor accessory-code
		   :documentation "This is for external wedges that are
transmitted as elements in a DICOM-RT data transfer.")

   (fitment-code :initarg :fitment-code
		 :accessor fitment-code
		 :documentation "This is for external wedges that are
transmitted as elements in a DICOM-RT data transfer.")

   (comments :type list
	     :initarg :comments
	     :accessor comments
	     :documentation "A list of strings of comments about the
current data set.  Could be used to note details about changes in the
data.")

   (rot-angles :type list
	       :initarg :rot-angles
	       :accessor rot-angles
	       :documentation "A list of angles, some subset of 0.0,
90.0, 180.0, 270.0, each angle a single-float.  The angles are valid
wedge rotation angles for this machine in the prism coordinate
system.")

   (caf-depth-coef :type single-float
		   :accessor caf-depth-coef
		   :documentation "Coef of Depth term in Alina's formula")

   (caf-fs-coef :type single-float
		:accessor caf-fs-coef
		:documentation "Coef of Field-Size term in Alina's formula")

   (caf-constant :type single-float
		 :accessor caf-constant
		 :documentation "Constant term in Alina's formula")

   (profile-table-vector :type (simple-array single-float (6))
			 :accessor profile-table-vector)

   (profile-depth-mapper :type (simple-array t 1)
			 :accessor profile-depth-mapper)

   (profile-depths :type (simple-array single-float 1)
		   :accessor profile-depths
		   :documentation "PROFILE-TABLE 1st-index array.")

   (profile-position-mapper :type (simple-array t 1)
			    :accessor profile-position-mapper)

   (profile-positions :type (simple-array single-float 1)
		      :accessor profile-positions
		      :documentation "PROFILE-TABLE 2nd-index array.")

   (profile-table :type (simple-array t 1)
		  :accessor profile-table
		  :documentation "2-D array of Wedge Profiles as a
function of Depth and Position but not Field-Size.")

   )

  (:default-initargs :comments nil :rot-angles '(0.0))

  (:documentation "A wedge-info object describes a particular wedge.
The slots of a wedge object should not be updated by Prism planning
code but should be updated by a separate machine data management
program.")

  )

;;;--------------------------------------------------

(defmethod slot-type ((obj wedge-info) slotname)

  (declare (ignore slotname))
  :simple)

;;;=============================================================
;;; End.
