;;;
;;; brachy-coord-panels
;;;
;;; replaces ortho-film-entry as it contains all the little subpanels
;;; for each kind of coordinate entry mode, that appear on the brachy
;;; panel, in the new arrangement of everything on one brachy panel.
;;;
;;;  1-Aug-2002 I. Kalet created from code earlier (temporarily)
;;; located in brachy-panels.
;;; 12-Aug-2002 I. Kalet add actions for AP and Right Lat buttons in
;;; ortho-coord-panel
;;; 19-Sep-2002 I. Kalet fix error in digitizer prompt box code, widen
;;; some boxes, narrow others, change label from "Next" to "Enter"
;;; 13-Oct-2002 I. Kalet add support for line sources
;;; 29-Jan-2003 I. Kalet create common parent class, coord-panel, add
;;; events to allow synch of current and end-source.
;;; 31-Jan-2005 A. Simms add :allow-other-keys to make-coord-entry-panel
;;;

(in-package :prism)

;;;---------------------------------------------
;;; generic coordinate entry subpanel
;;;---------------------------------------------

(defclass coord-panel ()

  ((defaults-panel :accessor defaults-panel
                   :initarg :defaults-panel
		   :documentation "The brachy-update-panel containing
the defaults for new sources.")

   (panel-frame :accessor panel-frame
		:documentation "The frame for this panel")

   (line-sources :accessor line-sources
		 :initarg :line-sources
		 :documentation "The collection containing all the
line sources")

   (seeds :accessor seeds
	  :initarg :seeds
	  :documentation "The collection containing all the seeds.")

   (entry-mode :accessor entry-mode
	       :initarg :entry-mode
	       :documentation "The entry mode, a symbol specifying the
coordinate entry mode currently active, either seeds or line-sources.")

   (end-source :accessor end-source
	       :initarg :end-source
	       :initform 1
	       :documentation "Last source number to enter.")
   (end-tln :accessor end-tln)
   (new-end :accessor new-end
	    :initform (ev:make-event)
	    :documentation "Announced when end source no. is changed,
	       so can preserve current and end across entry mode changes")

   (current :accessor current
	    :initarg :current
	    :initform 1
	    :documentation "Source number of source being entered.")
   (current-tln :accessor current-tln)
   (new-current :accessor new-current
		:initform (ev:make-event)
		:documentation "Announced when current source no. is changed,
	       so can preserve current and end across entry mode changes")

   (next-btn :accessor next-btn)
   )
  )

;;;---------------------------------------------

(defmethod (setf end-source) :after (new-id (pnl coord-panel))
  (ev:announce pnl (new-end pnl) new-id))

(defmethod (setf current) :after (new-id (pnl coord-panel))
  (ev:announce pnl (new-current pnl) new-id))

;;;---------------------------------------------

(defmethod destroy ((pan coord-panel))

  ;; remove notifies not needed
  (sl:destroy (end-tln pan))
  (sl:destroy (current-tln pan))
  (sl:destroy (next-btn pan))
  (sl:destroy (panel-frame pan)))

;;;---------------------------------------------
;;; X, Y, Z coordinate entry subpanel
;;;---------------------------------------------

(defclass xyz-coord-panel (coord-panel)

  ((x :accessor x :initform 0.0)
   (x-tln :accessor x-tln)
   (y :accessor y :initform 0.0)
   (y-tln :accessor y-tln)
   (z :accessor z :initform 0.0)
   (z-tln :accessor z-tln)
   ;; for line sources need end 2
   (x2 :accessor x2 :initform 0.0)
   (x2-tln :accessor x2-tln)
   (y2 :accessor y2 :initform 0.0)
   (y2-tln :accessor y2-tln)
   (z2 :accessor z2 :initform 0.0)
   (z2-tln :accessor z2-tln)
   )
  )

;;;---------------------------------------------

(defmethod make-coord-entry-panel (mode (method (eql 'xyz))
				   source-data-panel line-coll seed-coll
				   &rest initargs)

  (apply #'make-instance 'xyz-coord-panel
	 :defaults-panel source-data-panel
	 :line-sources line-coll :seeds seed-coll
	 :entry-mode mode
	 :allow-other-keys t
	 initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((pan xyz-coord-panel) &rest initargs)

  (let* ((fr (apply #'sl:make-frame 260 95 :ulc-x 185 :ulc-y 45 initargs))
	 (bpf (symbol-value *small-font*))
	 (btw 80)
	 (bth 25)
	 (dx 5)
	 (dx2 90)
	 (dx3 175)
	 (top-y 5)
	 (win (sl:window fr))
	 (line-mode (eql (entry-mode pan)
			 'line-sources)) ;; boolean for convenience
	 (curr-tl (sl:make-textline btw bth
				    :ulc-x dx :ulc-y top-y
				    :font bpf :parent win
				    :numeric t :lower-limit 1 :upper-limit 1000
				    :label "Curr: "))
	 (end-tl (sl:make-textline btw bth
				   :ulc-x dx :ulc-y (bp-y top-y bth 1)
				   :font bpf :parent win
				   :numeric t :lower-limit 1 :upper-limit 1000
				   :label "End: "))
	 (next-b (sl:make-button btw bth
				 :ulc-x dx :ulc-y (bp-y top-y bth 2)
				 :font bpf :parent win
				 :button-type :momentary
				 :label "Enter"))
	 (x-tl (sl:make-textline btw bth
				 :ulc-x dx2 :ulc-y top-y
				 :font bpf :parent win
				 :numeric t
				 :lower-limit -100.0
				 :upper-limit 100.0
				 :label "X: "))
	 (y-tl (sl:make-textline btw bth
				 :ulc-x dx2 :ulc-y (bp-y top-y bth 1)
				 :font bpf :parent win
				 :numeric t
				 :lower-limit -100.0
				 :upper-limit 100.0
				 :label "Y: "))
	 (z-tl (sl:make-textline btw bth
				 :ulc-x dx2 :ulc-y (bp-y top-y bth 2)
				 :font bpf :parent win
				 :numeric t
				 :lower-limit -100.0
				 :upper-limit 100.0
				 :label "Z: "))
	 (x2-tl (if line-mode
		    (sl:make-textline btw bth
				      :ulc-x dx3 :ulc-y top-y
				      :font bpf :parent win
				      :numeric t
				      :lower-limit -100.0
				      :upper-limit 100.0
				      :label "X2: ")))
	 (y2-tl (if line-mode
		    (sl:make-textline btw bth
				      :ulc-x dx3 :ulc-y (bp-y top-y bth 1)
				      :font bpf :parent win
				      :numeric t
				      :lower-limit -100.0
				      :upper-limit 100.0
				      :label "Y2: ")))
	 (z2-tl (if line-mode
		    (sl:make-textline btw bth
				      :ulc-x dx3 :ulc-y (bp-y top-y bth 2)
				      :font bpf :parent win
				      :numeric t
				      :lower-limit -100.0
				      :upper-limit 100.0
				      :label "Z2: ")))
	 )
    (setf (panel-frame pan) fr
	  (end-tln pan) end-tl
	  (current-tln pan) curr-tl
	  (x-tln pan) x-tl
	  (y-tln pan) y-tl
	  (z-tln pan) z-tl
	  (x2-tln pan) x2-tl
	  (y2-tln pan) y2-tl
	  (z2-tln pan) z2-tl
	  (next-btn pan) next-b)
    (setf (sl:info end-tl) (end-source pan)
	  (sl:info curr-tl) (current pan)
	  (sl:info x-tl) (x pan)
	  (sl:info y-tl) (y pan)
	  (sl:info z-tl) (z pan))
    (when line-mode
      (setf (sl:info x2-tl) (x2 pan)
	    (sl:info y2-tl) (y2 pan)
	    (sl:info z2-tl) (z2 pan)))
    (ev:add-notify pan (sl:new-info end-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (end-source pnl)
			 (round (read-from-string info)))))
    (ev:add-notify pan (sl:new-info curr-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (current pnl)
			 (round (read-from-string info)))))
    (ev:add-notify pan (sl:new-info x-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (x pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info y-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (y pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info z-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (z pnl)
			 (coerce (read-from-string info) 'single-float))))
    (when line-mode
      (ev:add-notify pan (sl:new-info x2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (x2 pnl)
			   (coerce (read-from-string info) 'single-float))))
      (ev:add-notify pan (sl:new-info y2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (y2 pnl)
			   (coerce (read-from-string info) 'single-float))))
      (ev:add-notify pan (sl:new-info z2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (z2 pnl)
			   (coerce (read-from-string info) 'single-float)))))
    (ev:add-notify pan (sl:button-on next-b)
		   #'(lambda (pnl btn)
		       (declare (ignore btn))
		       (let* ((line-mode (eql (entry-mode pnl) 'line-sources))
			      (coll (if line-mode (line-sources pnl)
				      (seeds pnl)))
			      (oldsrc (find (current pnl) (coll:elements coll)
					    :key #'id))
			      (newxyz (list (x pnl) (y pnl) (z pnl)))
			      (newxyz2 (if line-mode
					   (list (x2 pnl) (y2 pnl) (z2 pnl)))))
			 (if oldsrc
			     (if line-mode
				 (setf (end-1 oldsrc) newxyz
				       (end-2 oldsrc) newxyz2)
			       (setf (location oldsrc) newxyz))
			   (let ((defaults (defaults-panel pnl)))
			     (coll:insert-element
			      (if line-mode (make-line-source
					     ""
					     :id (current pnl)
					     :source-type (src-type defaults)
					     :activity (src-strength defaults)
					     :treat-time (app-time defaults)
					     :end-1 newxyz :end-2 newxyz2)
				(make-seed ""
					   :id (current pnl)
					   :source-type (src-type defaults)
					   :activity (src-strength defaults)
					   :treat-time (app-time defaults)
					   :location newxyz))
			      coll))))
		       (unless (= (current pnl) (end-source pnl))
			 (incf (current pnl))
			 (setf (sl:info (current-tln pan)) (current pnl)))
		       ))
    ))

;;;---------------------------------------------

(defmethod destroy :before ((pan xyz-coord-panel))

  ;; remove notifies not needed

  (sl:destroy (x-tln pan))
  (sl:destroy (y-tln pan))
  (sl:destroy (z-tln pan))
  (when (eql (entry-mode pan) 'line-sources)
    (sl:destroy (x2-tln pan))
    (sl:destroy (y2-tln pan))
    (sl:destroy (z2-tln pan))))

;;;---------------------------------------------
;;; ortho-coordinate entry subpanel
;;;---------------------------------------------

(defclass ortho-coord-panel (coord-panel)

  ((digitizing :accessor digitizing
	       :initform nil
	       :documentation "True if digitizer in use.")
   (digitizer-btn :accessor digitizer-btn)

   (ap-flag :accessor ap-flag
	    :initform t
	    :documentation "True if using AP rather than PA view.")
   (ap-button :accessor ap-button)

   (lat-flag :accessor lat-flag
	     :initform t
	     :documentation "True if using right lateral film.")
   (lat-button :accessor lat-button)

   (ap-mag :accessor ap-mag
	   :initform 1.0
	   :documentation "The AP or PA film magnification factor.")
   (ap-tln :accessor ap-tln)

   (lat-mag :accessor lat-mag
	    :initform 1.0
	    :documentation "The lateral film magnification factor.")
   (lat-tln :accessor lat-tln)

   (x-ap :accessor x-ap :initform 0.0)
   (x-ap-tln :accessor x-ap-tln)
   (y-ap :accessor y-ap :initform 0.0)
   (y-ap-tln :accessor y-ap-tln)
   (x-lat :accessor x-lat :initform 0.0)
   (x-lat-tln :accessor x-lat-tln)
   (y-lat :accessor y-lat :initform 0.0)
   (y-lat-tln :accessor y-lat-tln)

   ;; for line sources need end 2
   (x2-ap :accessor x2-ap :initform 0.0)
   (x2-ap-tln :accessor x2-ap-tln)
   (y2-ap :accessor y2-ap :initform 0.0)
   (y2-ap-tln :accessor y2-ap-tln)
   (x2-lat :accessor x2-lat :initform 0.0)
   (x2-lat-tln :accessor x2-lat-tln)
   (y2-lat :accessor y2-lat :initform 0.0)
   (y2-lat-tln :accessor y2-lat-tln)
   )
  )

;;;---------------------------------------------

(defmethod make-coord-entry-panel (mode (method (eql 'ortho-film))
				   source-data-panel line-coll seed-coll
				   &rest initargs)

  (apply #'make-instance 'ortho-coord-panel
	 :defaults-panel source-data-panel
	 :line-sources line-coll :seeds seed-coll
	 :entry-mode mode
	 initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((pan ortho-coord-panel)
				       &rest initargs)

  (let* ((fr (apply #'sl:make-frame 345 125 :ulc-x 185 :ulc-y 45 initargs))
	 (bpf (symbol-value *small-font*))
	 (btw-s 70)
	 (btw-m 80)
	 (btw-l 90)
	 (bth 25)
	 (dx 5)
	 (dx2 80)
	 (dx3 175)
	 (dx4 260)
	 (top-y 5)
	 (win (sl:window fr))
	 (line-mode (eql (entry-mode pan)
			 'line-sources)) ;; boolean for convenience
	 (curr-tl (sl:make-textline btw-s bth
				    :ulc-x dx :ulc-y top-y
				    :font bpf :parent win
				    :numeric t :lower-limit 1 :upper-limit 1000
				    :label "Curr: "))
	 (end-tl (sl:make-textline btw-s bth
				   :ulc-x dx :ulc-y (bp-y top-y bth 1)
				   :font bpf :parent win
				   :numeric t :lower-limit 1 :upper-limit 1000
				   :label "End: "))
	 (next-b (sl:make-button btw-s bth
				 :ulc-x dx :ulc-y (bp-y top-y bth 2)
				 :font bpf :parent win
				 :button-type :momentary
				 :label "Enter"))
	 (dig-b (sl:make-button btw-s bth
				:ulc-x dx :ulc-y (bp-y top-y bth 3)
				:font bpf :parent win
				:label "Digitizer"))
	 (ap-b (sl:make-button btw-l bth
			       :ulc-x dx2 :ulc-y top-y
			       :font bpf :parent win
			       :button-type :momentary
			       :label "AP"))
	 (ap-mag-tl (sl:make-textline btw-l bth
				      :ulc-x dx2 :ulc-y (bp-y top-y bth 1)
				      :font bpf :parent win
				      :numeric t
				      :lower-limit 0.5
				      :upper-limit 5.0
				      :label "AP Mag: "))
	 (lat-b (sl:make-button btw-l bth
			       :ulc-x dx2 :ulc-y (bp-y top-y bth 2)
			       :font bpf :parent win
			       :button-type :momentary
			       :label "Right Lat"))
	 (lat-mag-tl (sl:make-textline btw-l bth
				      :ulc-x dx2 :ulc-y (bp-y top-y bth 3)
				      :font bpf :parent win
				      :numeric t
				      :lower-limit 0.5
				      :upper-limit 5.0
				      :label "Lat Mag: "))
	 (ap-x-tl (sl:make-textline btw-m bth
				    :ulc-x dx3 :ulc-y top-y
				    :font bpf :parent win
				    :numeric t
				    :lower-limit -100.0
				    :upper-limit 100.0
				    :label "AP X: "))
	 (ap-y-tl (sl:make-textline btw-m bth
				    :ulc-x dx3 :ulc-y (bp-y top-y bth 1)
				    :font bpf :parent win
				    :numeric t
				    :lower-limit -100.0
				    :upper-limit 100.0
				    :label "AP Y: "))
	 (lat-x-tl (sl:make-textline btw-m bth
				     :ulc-x dx3 :ulc-y (bp-y top-y bth 2)
				     :font bpf :parent win
				     :numeric t
				     :lower-limit -100.0
				     :upper-limit 100.0
				     :label "Lat X: "))
	 (lat-y-tl (sl:make-textline btw-m bth
				     :ulc-x dx3 :ulc-y (bp-y top-y bth 3)
				     :font bpf :parent win
				     :numeric t
				     :lower-limit -100.0
				     :upper-limit 100.0
				     :label "Lat Y: "))
	 ;; when entry-mode is line-sources add x2, y2 etc.
	 (ap-x2-tl (if line-mode
		       (sl:make-textline btw-m bth
					 :ulc-x dx4 :ulc-y top-y
					 :font bpf :parent win
					 :numeric t
					 :lower-limit -100.0
					 :upper-limit 100.0
					 :label "AP X2: ")))
	 (ap-y2-tl (if line-mode
		       (sl:make-textline btw-m bth
					 :ulc-x dx4 :ulc-y (bp-y top-y bth 1)
					 :font bpf :parent win
					 :numeric t
					 :lower-limit -100.0
					 :upper-limit 100.0
					 :label "AP Y2: ")))
	 (lat-x2-tl (if line-mode
			(sl:make-textline btw-m bth
					  :ulc-x dx4 :ulc-y (bp-y top-y bth 2)
					  :font bpf :parent win
					  :numeric t
					  :lower-limit -100.0
					  :upper-limit 100.0
					  :label "Lat X2: ")))
	 (lat-y2-tl (if line-mode
			(sl:make-textline btw-m bth
					  :ulc-x dx4 :ulc-y (bp-y top-y bth 3)
					  :font bpf :parent win
					  :numeric t
					  :lower-limit -100.0
					  :upper-limit 100.0
					  :label "Lat Y2: ")))
	 )
    (setf (panel-frame pan) fr
	  (end-tln pan) end-tl
	  (current-tln pan) curr-tl
	  (ap-button pan) ap-b
	  (lat-button pan) lat-b
	  (ap-tln pan) ap-mag-tl
	  (lat-tln pan) lat-mag-tl
	  (x-ap-tln pan) ap-x-tl
	  (y-ap-tln pan) ap-y-tl
	  (x-lat-tln pan) lat-x-tl
	  (y-lat-tln pan) lat-y-tl
	  (x2-ap-tln pan) ap-x2-tl
	  (y2-ap-tln pan) ap-y2-tl
	  (x2-lat-tln pan) lat-x2-tl
	  (y2-lat-tln pan) lat-y2-tl
	  (next-btn pan) next-b
	  (digitizer-btn pan) dig-b)
    (setf (sl:info end-tl) (end-source pan)
	  (sl:info curr-tl) (current pan)
	  (sl:info ap-mag-tl) (ap-mag pan)
	  (sl:info lat-mag-tl) (lat-mag pan)
	  (sl:info ap-x-tl) (x-ap pan)
	  (sl:info ap-y-tl) (y-ap pan)
	  (sl:info lat-x-tl) (x-lat pan)
	  (sl:info lat-y-tl) (y-lat pan))
    (when line-mode
      (setf (sl:info ap-x2-tl) (x2-ap pan)
	    (sl:info ap-y2-tl) (y2-ap pan)
	    (sl:info lat-x2-tl) (x2-lat pan)
	    (sl:info lat-y2-tl) (y2-lat pan)))
    (ev:add-notify pan (sl:new-info end-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (end-source pnl)
			 (round (read-from-string info)))))
    (ev:add-notify pan (sl:new-info curr-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (current pnl)
			 (round (read-from-string info)))))
    (ev:add-notify pan (sl:button-on ap-b)
		   #'(lambda (pnl btn)
		       (setf (ap-flag pnl) (not (ap-flag pnl)))
		       (setf (sl:label btn) (if (ap-flag pnl) "AP" "PA"))))
    (ev:add-notify pan (sl:button-on lat-b)
		   #'(lambda (pnl btn)
		       (setf (lat-flag pnl) (not (lat-flag pnl)))
		       (setf (sl:label btn)
			 (if (lat-flag pnl) "Right Lat" "Left Lat"))))
    (ev:add-notify pan (sl:new-info ap-mag-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (ap-mag pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info lat-mag-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (lat-mag pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info ap-x-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (x-ap pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info ap-y-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (y-ap pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info lat-x-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (x-lat pnl)
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify pan (sl:new-info lat-y-tl)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (y-lat pnl)
			 (coerce (read-from-string info) 'single-float))))
    (when line-mode
      (ev:add-notify pan (sl:new-info ap-x2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (x2-ap pnl)
			   (coerce (read-from-string info) 'single-float))))
      (ev:add-notify pan (sl:new-info ap-y2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (y2-ap pnl)
			   (coerce (read-from-string info) 'single-float))))
      (ev:add-notify pan (sl:new-info lat-x2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (x2-lat pnl)
			   (coerce (read-from-string info) 'single-float))))
      (ev:add-notify pan (sl:new-info lat-y2-tl)
		     #'(lambda (pnl tln info)
			 (declare (ignore tln))
			 (setf (y2-lat pnl)
			   (coerce (read-from-string info) 'single-float)))))
    (ev:add-notify pan (sl:button-on next-b)
		   #'(lambda (pnl btn)
		       (declare (ignore btn))
		       (let* ((line-mode (eql (entry-mode pnl) 'line-sources))
			      (coll (if line-mode (line-sources pnl)
				      (seeds pnl)))
			      (oldsrc (find (current pnl) (coll:elements coll)
					    :key #'id))
			      (newxyz (xyz-from-ortho pnl))
			      (newxyz2 (xyz2-from-ortho pnl)))

			 (if oldsrc
			     (if line-mode
				 (setf (end-1 oldsrc) newxyz
				       (end-2 oldsrc) newxyz2)
			       (setf (location oldsrc) newxyz))
			   (let ((defaults (defaults-panel pnl)))
			     (coll:insert-element
			      (if line-mode
				  (make-line-source
				   ""
				   :id (current pnl)
				   :source-type (src-type defaults)
				   :activity (src-strength defaults)
				   :treat-time (app-time defaults)
				   :end-1 newxyz
				   :end-2 newxyz2
				   :ap-flag (ap-flag pnl)
				   :ap-mag (ap-mag pnl)
				   :lat-flag (lat-flag pnl)
				   :lat-mag (lat-mag pnl)
				   :raw-ap-coords (list (x-ap pnl)
							(y-ap pnl)
							(x2-ap pnl)
							(y2-ap pnl))
				   :raw-lat-coords (list (x-lat pnl)
							 (y-lat pnl)
							 (x2-lat pnl)
							 (y2-lat pnl)))
				(make-seed ""
					   :id (current pnl)
					   :source-type (src-type defaults)
					   :activity (src-strength defaults)
					   :treat-time (app-time defaults)
					   :location newxyz
					   :ap-flag (ap-flag pnl)
					   :ap-mag (ap-mag pnl)
					   :lat-flag (lat-flag pnl)
					   :lat-mag (lat-mag pnl)
					   :raw-ap-coords (list (x-ap pnl)
								(y-ap pnl))
					   :raw-lat-coords (list (x-lat pnl)
								 (y-lat pnl))))
			      coll))))
		       (unless (= (current pnl) (end-source pnl))
			 (incf (current pnl))
			 (setf (sl:info (current-tln pan)) (current pnl)))
		       ))
    (ev:add-notify pan (sl:button-on dig-b)
		   #'(lambda (pnl btn)
		       (if (digitizer-present)
			   (brachy-ortho-digitize pnl)
			 (sl:acknowledge "Digitzer not available"))
		       (setf (sl:on btn) nil)
		       ))
    ))

;;;---------------------------------------------

(defun xyz-from-ortho (panel)

  "returns demagnified x,y and averaged z from ortho film coords."

  (let* ((ap-mag (ap-mag panel))
	 (lat-mag (lat-mag panel))
	 (x (/ (x-ap panel) ap-mag))
	 (y (/ (y-lat panel) lat-mag))
	 ;; remember that z+ is toward the feet
	 (z-ap (- (/ (y-ap panel) ap-mag)))
	 (z-lat (/ (x-lat panel) lat-mag)))
    (unless (ap-flag panel) (setq x (- x)))
    (unless (lat-flag panel) (setq z-lat (- z-lat)))
    (list x y (/ (+ z-ap z-lat) 2.0))))

;;;---------------------------------------------

(defun xyz2-from-ortho (panel)

  "returns demagnified x2,y2 and averaged z2 from ortho film coords."

  (let* ((ap-mag (ap-mag panel))
	 (lat-mag (lat-mag panel))
	 (x (/ (x2-ap panel) ap-mag))
	 (y (/ (y2-lat panel) lat-mag))
	 ;; remember that z+ is toward the feet
	 (z-ap (- (/ (y2-ap panel) ap-mag)))
	 (z-lat (/ (x2-lat panel) lat-mag)))
    (unless (ap-flag panel) (setq x (- x)))
    (unless (lat-flag panel) (setq z-lat (- z-lat)))
    (list x y (/ (+ z-ap z-lat) 2.0))))

;;;---------------------------------------------

(defun brachy-ortho-digitize (panel)

  (sl:push-event-level)
  (digit-calibrate)
  (let ((prompt-box (sl:make-textbox 300 60
				     :title "Digitizer directions"))
	(start-no (current panel)) ;; so can reset it for Lat film
	(line-mode (eql (entry-mode panel) 'line-sources))
	state x0 y0 x y)
    
    ;; create raw ap window

    (loop
      (setf (sl:info prompt-box)
	(list (format nil "Place the ~A film on the digitizer"
		      (if (ap-flag panel) "AP" "PA"))
	      "Please digitize the origin"))
      (multiple-value-setq (state x0 y0) (digitize-point))
      (when (eql state :point) (return)))
    (loop
      (setf (sl:info prompt-box)
	(list (format nil "Digitize Source ~A" (current panel))))
      (multiple-value-setq (state x y) (digitize-point))
      (case state
	(:point
	 (setf (x-ap panel) (- x x0) (y-ap panel) (- y y0))
	 (setf (sl:info (x-ap-tln panel)) (x-ap panel)
	       (sl:info (y-ap-tln panel)) (y-ap panel))
	 (if (not line-mode)
	     (let ((oldsrc (find (current panel)
				 (coll:elements (seeds panel))
				 :key #'id)))
	       (if oldsrc
		   (setf (x-lat panel)
		     (or (first (raw-lat-coords oldsrc)) 0.0)
		     (y-lat panel)
		     (or (second (raw-lat-coords oldsrc)) 0.0)
		     
		     ;; update panel display?
		     
		     (location oldsrc) (xyz-from-ortho panel)
		     (ap-flag oldsrc) (ap-flag panel)
		     (ap-mag oldsrc) (ap-mag panel)
		     (raw-ap-coords oldsrc) (list (x-ap panel)
						  (y-ap panel)))
		 (let ((defaults (defaults-panel panel)))
		   (coll:insert-element ;; check for entry-mode here
		    (make-seed ""
			       :id (current panel)
			       :source-type (src-type defaults)
			       :activity (src-strength defaults)
			       :treat-time (app-time defaults)
			       :location (xyz-from-ortho panel)
			       ;; just set AP stuff here
			       :ap-flag (ap-flag panel)
			       :ap-mag (ap-mag panel)
			       :raw-ap-coords (list (x-ap panel)
						    (y-ap panel)))
		    (seeds panel))))
	       ;; (draw-raw-source src (ap-view mp) :ap))
	       )
	   ;; augment for line sources - digitize end 2 also
	   (loop
	     (setf (sl:info prompt-box)
	       (list (format nil "Digitize Source ~A, End 2" (current panel))))
	     (multiple-value-setq (state x y) (digitize-point))
	     (if (eql state :point)
		 (progn
		   (setf (x2-ap panel) (- x x0) (y2-ap panel) (- y y0))
		   (setf (sl:info (x2-ap-tln panel)) (x2-ap panel)
			 (sl:info (y2-ap-tln panel)) (y2-ap panel))
		   (let ((oldsrc (find (current panel)
				       (coll:elements (line-sources panel))
				       :key #'id)))
		     (if oldsrc
			 (setf (x-lat panel)
			   (or (first (raw-lat-coords oldsrc)) 0.0)
			   (y-lat panel)
			   (or (second (raw-lat-coords oldsrc)) 0.0)
			   (x2-lat panel)
			   (or (third (raw-lat-coords oldsrc)) 0.0)
			   (y2-lat panel)
			   (or (fourth (raw-lat-coords oldsrc)) 0.0)
			       
			   ;; update panel display?
		     
			   (end-1 oldsrc) (xyz-from-ortho panel)
			   (end-2 oldsrc) (xyz2-from-ortho panel)
			   (ap-flag oldsrc) (ap-flag panel)
			   (ap-mag oldsrc) (ap-mag panel)
			   (raw-ap-coords oldsrc) (list (x-ap panel)
							(y-ap panel)
							(x2-ap panel)
							(y2-ap panel)))
		       (let ((defaults (defaults-panel panel)))
			 (coll:insert-element ;; check for entry-mode here
			  (make-line-source ""
					    :id (current panel)
					    :source-type (src-type defaults)
					    :activity (src-strength defaults)
					    :treat-time (app-time defaults)
					    :end-1 (xyz-from-ortho panel)
					    :end-2 (xyz2-from-ortho panel)
					    ;; just set AP stuff here
					    :ap-flag (ap-flag panel)
					    :ap-mag (ap-mag panel)
					    :raw-ap-coords
					    (list (x-ap panel)
						  (y-ap panel)
						  (x2-ap panel)
						  (y2-ap panel)))
			  (line-sources panel)))))
		   (return)))))
	 (if (= (current panel) (end-source panel))
	     (return)
	   (progn
	     (incf (current panel))
	     (setf (sl:info (current-tln panel)) (current panel)))))
	(:done (return))
	))

    ;; remove ap window??

    ;; reset current and repeat for lat film
    (setf (current panel) start-no
	  (sl:info (current-tln panel)) (current panel))

    ;; create raw lateral window or reuse ap window

    (loop
      (setf (sl:info prompt-box)
	(list (format nil "Place the ~A film on the digitizer"
		      (if (lat-flag panel) "Right lateral" "Left lateral"))
	      "Please digitize the origin"))
      (multiple-value-setq (state x0 y0) (digitize-point))
      (when (eql state :point) (return)))
    (loop
      (setf (sl:info prompt-box)
	(list (format nil "Digitize Source ~A" (current panel))))
      (multiple-value-setq (state x y) (digitize-point))
      (case state
	(:point
	 (setf (x-lat panel) (- x x0) (y-lat panel) (- y y0))
	 (setf (sl:info (x-lat-tln panel)) (x-lat panel)
	       (sl:info (y-lat-tln panel)) (y-lat panel))
	 (if (not line-mode)
	     (let ((oldsrc (find (current panel)
				 (coll:elements (seeds panel))
				 :key #'id)))
	       (if oldsrc
		   (setf (x-ap panel) (or (first (raw-ap-coords oldsrc)) 0.0)
			 (y-ap panel) (or (second (raw-ap-coords oldsrc)) 0.0)

			 ;; update panel display?

			 (location oldsrc) (xyz-from-ortho panel)
			 (lat-flag oldsrc) (lat-flag panel)
			 (lat-mag oldsrc) (lat-mag panel)
			 (raw-lat-coords oldsrc) (list (x-lat panel)
						       (y-lat panel)))
		 (let ((defaults (defaults-panel panel)))
		   (coll:insert-element
		    (make-seed ""
			       :id (current panel)
			       :source-type (src-type defaults)
			       :activity (src-strength defaults)
			       :treat-time (app-time defaults)
			       :location (xyz-from-ortho panel)
			       ;; just set Lateral stuff here
			       :lat-flag (lat-flag panel)
			       :lat-mag (lat-mag panel)
			       :raw-lat-coords (list (x-lat panel)
						     (y-lat panel)))
		    (seeds panel))))

	       ;; (draw-raw-source src (lat-view mp) :lat))
	       )
	   ;; line sources - digitize end 2 also
	   (loop
	     (setf (sl:info prompt-box)
	       (list (format nil "Digitize Source ~A, End 2" (current panel))))
	     (multiple-value-setq (state x y) (digitize-point))
	     (if (eql state :point)
		 (progn
		   (setf (x2-lat panel) (- x x0) (y2-lat panel) (- y y0))
		   (setf (sl:info (x2-lat-tln panel)) (x2-lat panel)
			 (sl:info (y2-lat-tln panel)) (y2-lat panel))
		   (let ((oldsrc (find (current panel)
				       (coll:elements (line-sources panel))
				       :key #'id)))
		     (if oldsrc
			 (setf (x-ap panel)
			   (or (first (raw-ap-coords oldsrc)) 0.0)
			   (y-ap panel)
			   (or (second (raw-ap-coords oldsrc)) 0.0)
			   (x2-ap panel)
			   (or (third (raw-ap-coords oldsrc)) 0.0)
			   (y2-ap panel)
			   (or (fourth (raw-ap-coords oldsrc)) 0.0)
			       
			   ;; update panel display?
		     
			   (end-1 oldsrc) (xyz-from-ortho panel)
			   (end-2 oldsrc) (xyz2-from-ortho panel)
			   (lat-flag oldsrc) (lat-flag panel)
			   (lat-mag oldsrc) (lat-mag panel)
			   (raw-lat-coords oldsrc) (list (x-lat panel)
							 (y-lat panel)
							 (x2-lat panel)
							 (y2-lat panel)))
		       (let ((defaults (defaults-panel panel)))
			 (coll:insert-element
			  (make-line-source ""
					    :id (current panel)
					    :source-type (src-type defaults)
					    :activity (src-strength defaults)
					    :treat-time (app-time defaults)
					    :end-1 (xyz-from-ortho panel)
					    :end-2 (xyz2-from-ortho panel)
					    ;; just set LAT stuff here
					    :lat-flag (lat-flag panel)
					    :lat-mag (lat-mag panel)
					    :raw-lat-coords
					    (list (x-lat panel)
						  (y-lat panel)
						  (x2-lat panel)
						  (y2-lat panel)))
			  (line-sources panel)))))
		   (return)))))

	 (if (= (current panel) (end-source panel))
	     (return)
	   (progn
	     (incf (current panel))
	     (setf (sl:info (current-tln panel)) (current panel)))))
	(:done (return))
	))
    (sl:destroy prompt-box))
  (sl:pop-event-level))

;;;---------------------------------------------

(defmethod destroy :before ((pan ortho-coord-panel))

  (sl:destroy (ap-button pan))
  (sl:destroy (lat-button pan))
  (sl:destroy (ap-tln pan))
  (sl:destroy (lat-tln pan))
  (sl:destroy (x-ap-tln pan))
  (sl:destroy (y-ap-tln pan))
  (sl:destroy (x-lat-tln pan))
  (sl:destroy (y-lat-tln pan))
  ;; destroy end-2 stuff if entry-mode is line-sources
  (when (eql (entry-mode pan) 'line-sources)
    (sl:destroy (x2-ap-tln pan))
    (sl:destroy (y2-ap-tln pan))
    (sl:destroy (x2-lat-tln pan))
    (sl:destroy (y2-lat-tln pan)))
  (sl:destroy (digitizer-btn pan)))

;;;---------------------------------------------

(defmethod make-coord-entry-panel (mode (method t)
				   source-data-panel line-coll seed-coll
				   &rest initargs)

  (declare (ignore mode source-data-panel line-coll seed-coll))
  (format t "~%No method for entry type ~A~%" method)
  nil)

;;;---------------------------------------------
;;; End.
