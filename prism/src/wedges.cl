;;;
;;;  wedges
;;;
;;;  Definitions of wedge object and related code.
;;;
;;; 24-Jun-1994 J. Unger extract out of beam.
;;;  5-Sep-1994 J. Unger add init-inst so other init keywords can be 
;;;  supplied (and ignored).
;;; 15-Jan-1995 I. Kalet move copy functions here from beams.  Don't
;;; monkey with beam and plan stuff here in setf methods, just
;;; announce the events.  Make beam-for slotname :ignored so don't
;;; have to edit old data files.
;;; 11-Sep-1995 I. Kalet delete display-color, never used.  DON'T
;;; round wedge rotation in copy-wedge-rotation, it must stay
;;; single-float.
;;; 26-Oct-1997 I. Kalet add default for rotation also, since beam
;;; panel will insure valid values when changing wedge id.
;;; 30-Jan-2000 I. Kalet delete copy-wedge and copy-wedge-rotation, no
;;; longer needed.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defclass wedge (generic-prism-object)

  ((id :type fixnum
       :initarg :id
       :accessor id
       :documentation "The wedge id, as known to the dose computation
program.")

   (rotation :type single-float
             :initarg :rotation
             :accessor rotation
             :documentation "The wedge rotation angle (currently available
only on machines with multileaf collimators), not the steepness of the
wedge profile.")

   (new-id :type ev:event
	   :accessor new-id
	   :initform (ev:make-event)
           :documentation "Announced when the wedge's ID changes.")

   (new-rotation :type ev:event
	         :accessor new-rotation
	         :initform (ev:make-event)
                 :documentation "Announced when the wedge's rotation changes.")

  )
  (:default-initargs :id 0 :rotation 0.0)

  (:documentation "A wedge is contained in a beam.")
  )

;;;---------------------------------------------

(defmethod slot-type ((object wedge) slotname)

  (case slotname
    ((beam-for display-color) :ignore)
    (otherwise :simple)))

;;;---------------------------------------------

(defmethod not-saved ((object wedge))

  (append (call-next-method) '(new-rotation new-id name)))

;;;---------------------------------------------

(defun make-wedge (&rest initargs)

  "make-wedge &rest initargs

Returns a wedge object with the specified initialization args."

  (apply #'make-instance 'wedge initargs))

;;;---------------------------------------------

(defmethod (setf id) :after (new-id (w wedge))

  (ev:announce w (new-id w) new-id))

;;;---------------------------------------------

(defmethod (setf rotation) :after (new-rot (w wedge))

  (ev:announce w (new-rotation w) new-rot))

;;;---------------------------------------------
