;;;
;;; medical-images
;;;
;;; These are the CLOS (class) definitions for the medical images in
;;; radiation treatment planning, including 2-D and 3-D images.
;;;
;;;  9-May-1992 I. Kalet from earlier prism code
;;; 14-Jul-1992 I. Kalet add get-transverse-image and draw functions
;;; 31-Jul-1992 I. Kalet merge in additions from Jon Unger's imagedefs
;;;  9-Aug-1992 I. Kalet use defs from geometry system
;;;  7-Sep-1992 I. Kalet make window and level methods modify, not
;;;  replace, default methods
;;; 13-Nov-1992 I. Kalet/J. Unger move window/level to view, add sl:
;;; prefix where needed
;;; 13-Dec-1992 I. Kalet change image-displayed to background-displayed
;;; 31-Dec-1992 I. Kalet eliminate draw method for SLIK picture, draw
;;; for views writes to background pixmap.
;;;  2-Mar-1993 I. Kalet add method for bin-array-pathname
;;; 27-Apr-1993 J. Unger fix minor bug in downsize-image.
;;; 28-Apr-1993 J. Unger/I. Kalet break drawing of images up into two
;;; methods, one for views and one for clx:pixmaps.
;;;  3-May-1993 I. Kalet move some code here from image-manager, make
;;;  into generic functions instead of using typecase for dispatch.
;;; 12-May-1993 J. Unger add reader method for pix-per-cm -- computes
;;;  from image size and dimensions of pixels array if necessary.
;;; 28-Dec-1993 I. Kalet change downsize-image to resize-image
;;;  7-Jan-1994 I. Kalet add code to generate-image-from-set to resize
;;;  image to view size.
;;; 21-Jan-1994 I. Kalet add some declarations in an attempt to
;;; further optimize resize-image.
;;; 10-Mar-1994 I. Kalet change method draw for pixmap into function
;;; draw-image-pix
;;; 23-May-1994 J. Unger optimize some image manipulation code.
;;;  8-Jun-1994 I. Kalet set ID attribute in resize-image.
;;;  8-Jan-1995 I. Kalet remove proclaim form.
;;; 18-Feb-1996 I. Kalet in draw-image-pix let map-image-to-clx put
;;; the data in the pixmap, hiding the details from this module.
;;; 19-Sep-1996 I. Kalet remove &rest from draw method.
;;; 21-Jan-1997 I. Kalet remove references to geometry package, define
;;; origin, x-orient and y-orient as vectors, use accessors in misc.
;;;  1-Mar-1997 I. Kalet update calls to NEARLY- functions.
;;; 03-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx.
;;; 21-Jan-1998 I. Kalet some optimization mods to make-coronal-image
;;; and make-sagittal-image.
;;; 19-Jun-1998 I. Kalet put method for generate-image-from-set for
;;; beams-eye-views here, with the others.
;;;  6-Jul-1998 I. Kalet fill in details of drr parameters, add
;;; make-3d-image.
;;; 15-Jul-1998 I. Kalet add binarray-filename method and slots for it
;;; to use.
;;; 11-Aug-1998 C. Wilcox finish details of drr parameters and related
;;; functions
;;; 12-Aug-1998 I. Kalet in generate-image-from-set check if images in
;;; set before attempting to generate an image from the set
;;; 28-Sep-1998 I. Kalet set origin in DRR image properly, as it is
;;; used in the PostScript hard copy code.
;;; 15-Feb-1999 I. Kalet center generated coronal and sagittal images
;;; in nominal view area.
;;; 25-Feb-1999 I. Kalet cosmetic fixes in resize-image-pixels
;;; 10-Apr-1999 C. Wilcox minor changes to generate-image-from-set
;;;  for bev's to support background processing and DRR's
;;; 19-Nov-1999 BobGian add UID slot to IMAGE object for DICOM.
;;;  5-Jan-2000 I. Kalet relax z match criterion for transverse images
;;; in find-transverse-image.
;;; 17-Jul-2000 I. Kalet add support for OpenGL image magnification
;;; and display.
;;; 30-Jul-2000 I. Kalet split off draw methods and other view related
;;; stuff to separate file, image-graphics, move draw-image-pix to
;;; inline code in filmstrip, since used only there.
;;;  6-Aug-2000 I. Kalet move get-transverse-image back here, not
;;; related to views.
;;; 18-Sep-2002 BobGian add PAT-POS (default "HFS") slot to IMAGE class
;;;  for describing patient position as scanned (Head-First Supine, etc)
;;; 26-Jun-2005 I. Kalet change single-float calls to coerce.
;;;  3-Jan-2009 I. Kalet add new procedure scale-image that does what
;;; resize-image-pixels did but way faster in the general case, to
;;; replace the use of OpenGL for images.
;;;  1-Jun-2009 I. Kalet remove resize-image-pixels and resize-image
;;;

(in-package :prism)

;;;--------------------------------------------

(defclass image ()

  ((id :accessor id
       :initarg :id)

   (uid :type string
	:accessor uid
	:initarg :uid)

   (patient-id :type fixnum
	       :accessor patient-id
	       :initarg :patient-id
	       :documentation "The patient id of the patient this
image belongs to.")

   (image-set-id :type fixnum
		 :accessor image-set-id
		 :initarg :image-set-id
		 :documentation "The image set id of the primary image
set the image belongs to, can also be changed in order to make it part
of another image set.")

   (pat-pos :type string
	    :accessor pat-pos
	    :initarg :pat-pos
	    :documentation "String, one of \"HFP\", \"HFS\", \"FFP\", \"FFS\"
describing patient position as scanned (Head/Feet-First Prone/Supine, etc).
Also legal but not used in Prism are \"HFDR\", \"HFDL\", \"FFDR\", \"FFDL\"
for Head/Feet-first Decubitus Right/Left.")

   (description :type string
		:accessor description
		:initarg :description)

   (acq-date :type string
	     :accessor acq-date
	     :initarg :acq-date)

   (acq-time :type string
	     :accessor acq-time
	     :initarg :acq-time)

   (scanner-type :type string
		 :accessor scanner-type             ;; GE9800, SOMATOM-DR, etc
		 :initarg :scanner-type)

   (hosp-name :type string
	      :accessor hosp-name
	      :initarg :hosp-name)

   (img-type :type string
	     :accessor img-type                     ;; CT, NMR, PET, etc
	     :initarg :img-type)

   (origin :type (vector single-float 3)
	   :accessor origin
	   :initarg :origin
	   :documentation "Origin refers to the location in patient
space of the corner of the image as defined by the point at pixel
array reference 0 0 or voxel array reference 0 0 0 -- see the pixels
and voxels slot in the respective image-2D and image-3D subclasses.")

   (size :type list                         ;; of two or three elements, x y z
	 :accessor size
	 :initarg :size
	 :documentation "The size slot refers to the overall size of
the image in each dimension, measured in centimeters in patient
space.")

   (range :type fixnum
	  :accessor range
	  :initarg :range
	  :documentation "Range refers to the maximum pixel/voxel
value possible for this type of image.")

   (units :type string
	  :accessor units
	  :initarg :units)                          ;; eg: Hounsfield numbers

   )

  (:default-initargs :id 0 :uid "" :patient-id 0 :image-set-id 0
		     :pat-pos "HFS" :description ""
		     :acq-date "missing" :acq-time "missing"
		     :scanner-type "GE9800Q" :hosp-name "UWMC"
		     :img-type "X-ray CT" :range 4095 :units "H - 1024")

  (:documentation "The basis for all kinds of geometric studies upon
patients, including 2-D images, 3-D images, 2-D image sets, like a
series of CT slices, and 3-D image sets.  The information here defines
all the parameters relevant to the moment of study itself and to
parameters found in all images.")

  )

;;;--------------------------------------------

(defmethod bin-array-pathname ((im image))

  "returns the directory pathname for the image data binary files and
index files."

  *image-database*)

;;;--------------------------------------------

(defmethod bin-array-filename ((im image) slotname)

  "returns the filename for the image data binary file to which to
write image im."

  (declare (ignore slotname))
  (format nil "pat-~D.image-~D-~D"
	  (patient-id im) (image-set-id im) (id im)))

;;;--------------------------------------------

(defclass image-2D (image)

  ((thickness :type single-float
	      :accessor thickness
	      :initarg :thickness)

   (x-orient :type (vector single-float 3)
	     :accessor x-orient
	     :initarg :x-orient
	     :documentation "The x-orient and y-orient slots are
vectors in patient space that define the orientation of the X and Y
axes of the image respectively, relative to the patient coordinate
system.")

   (y-orient :type (vector single-float 3)
	     :accessor y-orient
	     :initarg :y-orient
	     :documentation "See x-orient.")

   (pix-per-cm :type single-float
	       :accessor pix-per-cm
	       :initarg :pix-per-cm)

   (pixels :type (simple-array (unsigned-byte 16) 2)
	   :accessor pixels
	   :initarg :pixels
	   :documentation "Pixels is the array of image data itself.
The value at each index of the array refers to a sample taken from the
center of the region indexed, and values for images with non-zero
thickness refer to points mid-way through the image's thickness.  The
origin of the pixels array is in the upper left hand corner, and the
array is stored in row-major order so values are indexed as row,
column pairs, i.e., the dimensions are y, x.")

   )

  (:documentation "An image-2D depicts some 2-D slice, cross section
or projected view of a patient's anatomy and is typically a single CT
image, an interpolated cross section of a volume, or the result of ray
tracing through a volume from an eyepoint to a viewing plane.")

  )

;;;--------------------------------------------

(defmethod pix-per-cm :before ((img image-2D))

  "Computes the pixels per centimeters if not already set."

  (unless (slot-boundp img 'pix-per-cm)
    (setf (slot-value img 'pix-per-cm)
	  (float (/ (array-dimension (pixels img) 1) (first (size img)))))))

;;;--------------------------------------------

(defmethod slot-type ((object image-2D) slotname)

  (case slotname
    (pixels :bin-array)
    (otherwise (call-next-method))))

;;;--------------------------------------------

(defun scale-image (old new mag x0 y0)
  
  "Does pan and zoom of array data in old to generate new, using mag
  as the magnification from old to new, and x0 and y0 are the array
  coordinates in old of the 0,0 pixel in new.  The arrays are assumed
  to be of type unsigned-byte 32, or clx:pixel."

  (let* ((old-dim (array-dimension old 0))
	 (new-dim (array-dimension new 0))
	 (delta (/ 1.0 mag))
	 (old-dim-flt (coerce old-dim 'single-float))
	 (xstart (- x0 delta))
	 (x xstart) ;; initial value not really used
	 (y (- y0 delta)) ;; this is ystart also
	 (yint 0)
	 )
    (declare (type (simple-array (unsigned-byte 32) 2) old new)
	     (type fixnum x0 y0 old-dim new-dim yint)
	     (type single-float mag delta old-dim-flt xstart x y))
    (dotimes (j new-dim)
      (declare (type fixnum j))
      (incf y delta)
      (setq yint (round y)
	    x xstart)
      (dotimes (i new-dim)
	(declare (type fixnum i))
	(incf x delta)
	(setf (aref new j i)
	  (if (or (< x 0.0)
		  (< yint 0)
		  (> x old-dim-flt)
		  (> yint old-dim))
	      0
	    (aref old yint (the fixnum (round x)))))))
    new))

;;;--------------------------------------------

(defun find-transverse-image (z images epsilon)

  "find-transverse-image z images

Scans images, a list of image-2D's, for an image whose z-coordinate
is nearly equal to the z parameter, and returns such an image, if one
exists, or nil if no such image exists."

  (find z images
	:test #'(lambda (a b) (poly:nearly-equal a b epsilon))
	:key #'(lambda (img) (vz (origin img)))))

;;;--------------------------------------------

(defun make-coronal-image (y images)

  "make-coronal-image y images

If y lies within the y-extent of images, a list of image-2D's, this
routine computes and returns an image-2D whose pixels are a reformatting
of images at the sagittal plane determined by y.  If y lies outside
the y-extent of images, nil is returned."

  (let ((fi (first images)))
    (when (poly:nearly-increasing
	    0.0 (- (vy (origin fi)) y) (second (size fi)))
      (let* ((new-pix (make-array (array-dimensions (pixels fi))
				  :element-type '(unsigned-byte 16)
				  :initial-element 0))
	     (zlist (mapcar #'(lambda (img) (- (vz (origin img))
					       (/ (thickness img) 2.0)))
		      images))
	     (top-z (* 0.5 (+ (apply #'min zlist) (apply #'max zlist)
			      (- (second (size fi)))))))
	(declare (single-float top-z)
		 (type (simple-array (unsigned-byte 16) 2) new-pix))
	(dolist (img images)
	  (let* ((ppcm (pix-per-cm img))
		 (img-pix (pixels img))
		 (new-row (round (* ppcm (- (vz (origin img))
					    (/ (thickness img) 2.0)
					    top-z))))
		 (pixels-thick (truncate (1+ (* ppcm (thickness img)))))
		 (img-dim-x (array-dimension img-pix 0))
		 (img-dim-y (array-dimension img-pix 1))
		 (img-row 0))
	    (declare (single-float ppcm)
		     (type (simple-array (unsigned-byte 16) 2) img-pix)
		     (fixnum pixels-thick img-dim-x
			     img-dim-y new-row img-row))
	    (when (< -1 new-row img-dim-y)
	      (setq img-row (round (* ppcm (- (vy (origin img)) y))))
	      (dotimes (i pixels-thick)             ;; row replication
		(when (< new-row img-dim-y)
		  (dotimes (new-col img-dim-x)      ;; pixels in the row
		    (declare (fixnum new-col))
		    (setf (aref new-pix new-row new-col)
			  (aref img-pix img-row new-col))))
		(incf new-row)))))
	(make-instance 'image-2D
	  :id 1                                     ;; arbitrary
	  :description "Prism coronal image"
	  :acq-date (acq-date fi)
	  :acq-time (acq-time fi)
	  :scanner-type (scanner-type fi)
	  :hosp-name (hosp-name fi)
	  :img-type (img-type fi)
	  :origin (vector (vx (origin fi)) y top-z)
	  :size (size fi)
	  :range (range fi)
	  :units (units fi)
	  :thickness 1.0
	  :x-orient (vector 1.0 0.0 0.0)
	  :y-orient (vector 0.0 0.0 1.0)
	  :pix-per-cm (pix-per-cm fi)
	  :pixels new-pix)))))

;;;--------------------------------------------

(defun make-sagittal-image (x images)

  "make-sagittal-image x images

If x lies within the x-extent of images, a list of image-2D's, this
routine computes and returns an image-2D whose pixels are a reformatting
of images at the sagittal plane determined by x.  If x lies outside
the x-extent of images, nil is returned."

  (let ((fi (first images)))
    (when (poly:nearly-increasing
	    0.0 (- x (vx (origin fi))) (first (size fi)))
      (let* ((new-pix (make-array (array-dimensions (pixels fi))
				  :element-type '(unsigned-byte 16)
				  :initial-element 0))
	     (zlist (mapcar #'(lambda (img) (- (vz (origin img))
					       (/ (thickness img) 2.0)))
		      images))
	     (top-z (* 0.5 (+ (apply #'min zlist) (apply #'max zlist)
			      (- (second (size fi)))))))
	(declare (single-float top-z)
		 (type (simple-array (unsigned-byte 16) 2) new-pix))
	(dolist (img images)
	  (let* ((ppcm (pix-per-cm img))
		 (img-pix (pixels img))
		 (new-col (round (* ppcm (- (vz (origin img))
					    (/ (thickness img) 2.0)
					    top-z))))
		 (pixels-thick (truncate (1+ (* ppcm (thickness img)))))
		 (img-dim-x (array-dimension img-pix 0))
		 (img-dim-y (array-dimension img-pix 1))
		 (img-col 0))
	    (declare (single-float ppcm)
		     (type (simple-array (unsigned-byte 16) 2) img-pix)
		     (fixnum pixels-thick img-dim-x
			     img-dim-y new-col img-col))
	    (when (< -1 new-col img-dim-x)
	      (setq img-col (round (* ppcm (- x (vx (origin img))))))
	      (dotimes (i pixels-thick)             ;; column replication
		(when (< new-col img-dim-x)
		  (dotimes (new-row img-dim-y)
		    (declare (fixnum new-row))
		    (setf (aref new-pix new-row new-col)
			  (aref img-pix new-row img-col))))
		(incf new-col)))))
	(make-instance 'image-2D
	  :id 2                                     ;; arbitrary
	  :description "Prism sagittal image"
	  :acq-date (acq-date fi)
	  :acq-time (acq-time fi)
	  :scanner-type (scanner-type fi)
	  :hosp-name (hosp-name fi)
	  :img-type (img-type fi)
	  :origin (vector x (vy (origin fi)) top-z)
	  :size (size fi)
	  :range (range fi)
	  :units (units fi)
	  :thickness 1.0
	  :x-orient (vector 1.0 0.0 0.0)
	  :y-orient (vector 0.0 0.0 1.0)
	  :pix-per-cm (pix-per-cm fi)
	  :pixels new-pix)))))

;;;--------------------------------------------

(defun make-3d-image (images)

  "make-3d-image z-size images

returns a 3D array and a list of z values from images, a list of
image-2d, in which the number of pixels in the z direction is z-size."

  (let* ((z-list nil)
	 (count 0)
	 (prev 0.0)
	 (z-array (make-array (+ 1 (length images))
			      :element-type 'single-float))
	 (3dimage (make-array (length images))))
    ;; create a list to sort the images by z value
    ;; without side-effecting images
    (dolist (i images)
      (setf z-list (cons (list (aref (origin i) 2) count) z-list))
      (incf count 1))
    (setq z-list (sort z-list #'< :key #'car))
    ;; build the sorted arrays for images and corresponding z-values
    (setf count 0)
    (setf prev (first (first z-list)))
    (dolist (e z-list)
      (setf (aref 3dimage count)
	    (pixels (nth (second e) images)))
      (setf (aref z-array count)
	    (coerce (/ (+ prev (first e)) 2.0) 'single-float))
      (setf prev (first e))
      (incf count 1))
    (setf (aref z-array count) (coerce prev 'single-float))
    (values 3dimage z-array)))

;;;--------------------------------------------

(defclass image-3d (image)

  ((voxels :type (simple-array (unsigned-byte 16) 3)
	   :accessor voxels
	   :initarg :voxels
	   :documentation "Voxels is the array of intensities itself
The value at each index of the array refers to a sample taken from the
center of the region indexed.  The origin of the voxels array is in
the upper left back corner and the array is stored in row, then plane
major order, so values are indexed as plane, row, column triples, i.e.
the dimensions are ordered z, y, x.")

   )

  (:documentation "An image-3D depicts some 3-D rectangular solid
region of a patient's anatomy.")

  )

;;;--------------------------------------------

(defmethod slot-type ((object image-3d) slotname)

  (case slotname
    (voxels :bin-array)
    (otherwise (call-next-method))))

;;;--------------------------------------------

(defun get-transverse-image (im3d z)

  "get-transverse-image im3d z

returns an image-2d instance that corresponds to the transverse image
at z through the image-3d im3d."

  (let ((vox (voxels im3d))
	(org (origin im3d))
	(size (size im3d)))
    (make-instance 'image-2d
      :description (format nil "Image at z = ~A" z)
      :acq-date (acq-date im3d)
      :acq-time (acq-time im3d)
      :scanner-type (scanner-type im3d)
      :hosp-name (hosp-name im3d)
      :img-type (img-type im3d)
      :origin (vector (vx org) (vy org) 0.0)
      :size (list (first size) (second size))
      :range (range im3d)
      :units (units im3d)
      :thickness (/ (float (array-dimension vox 2)) (third size))
      :x-orient (vector 1.0 0.0 0.0)
      :y-orient (vector 0.0 1.0 0.0)
      :pix-per-cm (/ (float (array-dimension vox 1)) (second size))
      :pixels (sl:get-z-array vox (vz org) (third size) z))))

;;;--------------------------------------------
;;; End.
