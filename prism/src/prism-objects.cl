;;;
;;; prism-objects
;;;
;;; This is the code that defines generic named prism objects that are
;;; created and manipulated by the various panels.  This includes
;;; objects such as patients, plans, organs, and beams, but not
;;; contours, which are not named and do not appear in lists.
;;;
;;; 16-Sep-1992 I. Kalet created from code in selector-panels
;;; 15-Oct-1992 I. Kalet add default draw method
;;; 29-Dec-1992 I. Kalet add <CR> in default draw method message
;;; 15-Feb-1993 I. Kalet add the bp-y function here - used by several
;;; panels
;;; 19-Sep-1996 I. Kalet remove &rest from draw method
;;; 10-Jun-1997 I. Kalet add default method for display-color and
;;; new-color
;;;  1-Feb-2003 I. Kalet move default method for name here from view-panels
;;;

(in-package :prism)

;;;---------------------------------------

(defclass generic-prism-object ()

  ((name :type string
	 :accessor name
	 :initarg :name
	 :documentation "The name string for each instance of an
object, e.g., patient name, or plan name.")

   (new-name :type ev:event
	     :accessor new-name
	     :initform (ev:make-event)
	     :documentation "Announced when the name attribute is
updated.")

   )

  (:default-initargs :name "Generic Prism object.")

  (:documentation "This is the basic prism object definition for
objects that will have names and be created and deleted via selector
panels, and with their own editing panels.")

  )

;;;---------------------------------------

(defmethod (setf name) :after (text (obj generic-prism-object))

  (ev:announce obj (new-name obj) text))

;;;---------------------------------------

(defmethod not-saved ((object generic-prism-object))

  '(new-name))

;;;--------------------------------------

(defmethod name ((obj t))

  "default name for anything"

  "no name")

;;;---------------------------------------

(defmethod draw ((obj t) (v t))

  "DRAW (obj t) (v t)

This is a default or stub method so we can build and use the various
functions without crashing on not yet implemented draw calls."

  (format t "No DRAW method for class ~A in ~A~%"
	  (class-name (class-of obj))
	  (class-name (class-of v))))

;;;---------------------------------------

(defmethod display-color ((obj t))

  "stub method for reference by selector panels code or other code."

  (format t "No DISPLAY-COLOR method for class ~A~%"
	  (class-name (class-of obj))))

;;;---------------------------------------

(defmethod new-color ((obj t))

  "stub method for reference by selector panels code or other code."

  (format t "No NEW-COLOR method for class ~A~%"
	  (class-name (class-of obj))))

;;;---------------------------------------

(defclass generic-panel ()

  ((deleted :type ev:event
	    :accessor deleted
	    :initform (ev:make-event)
	    :documentation "Announced when the panel is deleted.")

   )

  (:documentation "This is the basic prism panel definition, for
panels that edit various classes of prism objects.")

  )

;;;---------------------------------------

(defmethod destroy ((p generic-panel))

  "Panels need a destroy method to be called by the
button-panel-mediator when their button is deselected."

  (ev:announce p (deleted p)))

;;;---------------------------------------

(defun bp-y (start-y button-height n)

  "BP-Y start-y button-height n

allows a 5 pixel spacing and computes the ulc-y pixels for the nth
button or textline in a panel left side button stack.  The first
button is button 0, which is at start-y."

  (+ start-y (* n (+ button-height 5))))

;;;---------------------------------------
