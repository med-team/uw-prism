;;;
;;; brachy-specs-panels
;;;
;;; Definitions of special control panels for source type and other
;;; non-coordinate parameters of line sources and seeds.
;;;
;;; 27-Feb-2000 I. Kalet split off from brachy-panels.
;;; 27-Apr-2000 I. Kalet add actions for source activity and
;;; treat-time updates.  Add source strength units display.  Add
;;; update when any activity or treatment time changes.  Display 2
;;; decimal places for coords.
;;;  8-May-2000 I. Kalet split gamma into dose rate constant and
;;; anisotropy factor, also add protocol label.
;;; 11-May-2000 I. Kalet change limits on activity, application time,
;;; and parametrize.
;;; 28-May-2000 I. Kalet parametrize small font.
;;;  1-Apr-2002 I. Kalet big overhaul to make a nice interface
;;;  5-May-2002 I. Kalet allow for button-off events
;;; 24-Jul-2002 I. Kalet fix mishandling of Z columns, also added more
;;; required event registrations.
;;; 29-Jul-2002 I. Kalet make initial source range in update panel 0
;;; to 0 to prevent accidental modification or deletion.
;;; 12-Aug-2002 I. Kalet add delta-Z column for seeds.
;;;  6-Oct-2002 I. Kalet add line source support back in.
;;; 11-Feb-2003 I. Kalet update units readout when source type changes.
;;;  2-Nov-2003 I. Kalet remove #. reader macro to allow compile
;;; without load.
;;;  1-Dec-2003 I. Kalet use backquote in array initialization instead
;;; of quote, to allow eval of parameters.
;;;  5-Jan-2005 A. Simms add :allow-other-keys t to make-brachy-update-panel
;;;

(in-package :prism)

;;;---------------------------------------------

(defvar *brachy-rows* 6)

;;;---------------------------------------------

(defvar *brachy-specs-row-heights*
    (make-list (+ *brachy-rows* 1) :initial-element 25))

;;;---------------------------------------------

(defvar *brachy-specs-col-widths* '(50 60 210 60 50 30
				    60 70 70 70 70))

;;;---------------------------------------------

(defvar *brachy-specs-cells*
    (make-array (list (+ *brachy-rows* 1) 11)
		:initial-contents
		`(((:label "Go To:")
		   (:number nil 1 1000)
		   (:label "Source type")
		   (:label "Strength")
		   nil ;; room for labels
		   (:label "Perm")
		   (:label "App. time")
		   (:label "Act. len") ;; initially line sources
		   (:label "Phys. len") ;; initially line sources
		   (:label "Comp. len") ;; initially line sources
		   (:label "")) ;; initially line sources
		  ;; six rows of sources, with arrows in first and last
		  ((:up-arrow nil nil nil :fg-color sl:red)
		   (:button "" nil nil :border-width 0) ;; src no.
		   (:button "") ;; source type - by menu
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*) ;; source strength
		   (:readout "" nil nil :border-width 0) ;; src strength units
		   (:button "P") ;; permanent checkbox
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*) ;; application time
		   (:readout "" nil nil :border-width 0) ;; active len or x
		   (:readout "" nil nil :border-width 0) ;; phys. len or y
		   (:readout "" nil nil :border-width 0) ;; comp. len or z
		   (:readout "" nil nil :border-width 0)) ;; blank or delta-z
		  (nil ;; as above without arrow
		   (:button "" nil nil :border-width 0)
		   (:button "")
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*)
		   (:readout "" nil nil :border-width 0)
		   (:button "P") ;; permanent checkbox
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0))
		  (nil
		   (:button "" nil nil :border-width 0)
		   (:button "")
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*)
		   (:readout "" nil nil :border-width 0)
		   (:button "P") ;; permanent checkbox
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0))
		  (nil
		   (:button "" nil nil :border-width 0)
		   (:button "")
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*)
		   (:readout "" nil nil :border-width 0)
		   (:button "P") ;; permanent checkbox
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0))
		  (nil
		   (:button "" nil nil :border-width 0)
		   (:button "")
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*)
		   (:readout "" nil nil :border-width 0)
		   (:button "P") ;; permanent checkbox
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0))
		  ((:down-arrow nil nil nil :fg-color sl:red)
		   (:button "" nil nil :border-width 0)
		   (:button "")
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*)
		   (:readout "" nil nil :border-width 0)
		   (:button "P") ;; permanent checkbox
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)
		   (:readout "" nil nil :border-width 0)))))

;;;---------------------------------------------

(defclass brachy-specs-panel ()

  ((src-coll :accessor src-coll
	     :initarg :src-coll
	     :documentation "The collection of sources for this panel,
from the plan.")

   (panel-frame :accessor panel-frame
		:documentation "The frame for this panel")

   (src-pos :type fixnum
	    :accessor src-pos
	    :initform 0
	    :documentation "The position in the source list of the
source in the first row of the source panel spreadsheet.")

   (busy :accessor busy
	 :initform nil
	 :documentation "Used to prevent infinite loop in permanent button")

   ))

;;;---------------------------------------------

(defun make-brachy-specs-panel (sources window x y)

  (make-instance 'brachy-specs-panel
    :src-coll sources
    :parent window :ulc-x x :ulc-y y
    :font (symbol-value *small-font*)
    :allow-other-keys t))

;;;---------------------------------------------

(defmethod initialize-instance :after ((srcpan brachy-specs-panel)
				       &rest initargs)

  (let ((pan-fr (apply #'sl:make-spreadsheet
		       *brachy-specs-row-heights*
		       *brachy-specs-col-widths*
		       *brachy-specs-cells*
		       initargs)))
    (setf (panel-frame srcpan) pan-fr)
    (brachy-specs-refresh srcpan)
    (dolist (src (coll:elements (src-coll srcpan)))
      (ev:add-notify srcpan (new-source-type src)
		     #'(lambda (pan source newid)
			 (let ((srctab (source-data newid)))
			   (update-brachy-spec-cell
			    pan source
			    (format nil "~A ~A C=~4,2F A=~4,2F"
				    (src-type srctab) (protocol srctab)
				    (dose-rate-const srctab)
				    (anisotropy-fn srctab))
			    "~A" 2)
			   (update-brachy-spec-cell
			    pan source (activity-units srctab) "~A" 4))))
      (ev:add-notify srcpan (new-activity src)
		     #'(lambda (pan source newact)
			 (update-brachy-spec-cell
			  pan source newact "~6,3F" 3)))
      (ev:add-notify srcpan (new-treat-time src)
		     #'(lambda (pan source newtime)
			 (update-brachy-spec-cell
			  pan source newtime "~6,1F" 6)))
      (ev:add-notify srcpan (new-location src)
		     #'(lambda (pan source newloc)
			 (if (> (actlen (source-data (source-type source))) 0)
			     (update-brachy-spec-cell
			      pan source (source-length source) "~6,2F" 9)
			   (progn
			     (update-brachy-spec-cell
			      pan source (first newloc) "~6,2F" 7)
			     (update-brachy-spec-cell
			      pan source (second newloc) "~6,2F" 8)
			     (update-brachy-spec-cell
			      pan source (third newloc) "~6,2F" 9)
			     ;; column 10 blank for now
			     )))))
    (ev:add-notify srcpan (coll:inserted (src-coll srcpan))
		   #'(lambda (pan coll src)
		       (declare (ignore coll))
		       (brachy-specs-refresh pan)
		       (ev:add-notify pan (new-source-type src)
				      #'(lambda (pnl source newid)
					  (let ((srctab (source-data newid)))
					    (update-brachy-spec-cell
					     pnl source
					     (format nil
						     "~A ~A C=~4,2F A=~4,2F"
						     (src-type srctab)
						     (protocol srctab)
						     (dose-rate-const srctab)
						     (anisotropy-fn srctab))
					     "~A" 2)
					    (update-brachy-spec-cell
					     pan source
					     (activity-units srctab)
					     "~A" 4))))
		       (ev:add-notify pan (new-activity src)
				      #'(lambda (pnl source newact)
					  (update-brachy-spec-cell
					   pnl source newact "~6,3F" 3)))
		       (ev:add-notify pan (new-treat-time src)
				      #'(lambda (pnl source newtime)
					  (update-brachy-spec-cell
					   pnl source newtime "~6,1F" 6)))
		       (ev:add-notify pan (new-location src)
				      #'(lambda (pnl source newloc)
					  (if (> (actlen
						  (source-data
						   (source-type source))) 0)
					      (update-brachy-spec-cell
					       pan source
					       (source-length source)
					       "~6,2F" 9)
					    (progn
					      (update-brachy-spec-cell
					       pnl source (first newloc)
					       "~6,2F" 7)
					      (update-brachy-spec-cell
					       pnl source (second newloc)
					       "~6,2F" 8)
					      (update-brachy-spec-cell
					       pnl source (third newloc)
					       "~6,2F" 9)
					      ;; column 10 blank for now
					      ))))
		       ))
    (ev:add-notify srcpan (coll:deleted (src-coll srcpan))
		   #'(lambda (pan coll src)
		       (declare (ignore coll))
		       (ev:remove-notify pan (new-source-type src))
		       (ev:remove-notify pan (new-activity src))
		       (ev:remove-notify pan (new-treat-time src))
		       (ev:remove-notify pan (new-location src))
		       (brachy-specs-refresh pan)
		       ))
    (ev:add-notify srcpan (sl:user-input pan-fr)
		   #'(lambda (pan sheet i j info)
		       (let* ((srcs (coll:elements (src-coll pan)))
			      (lastrow (min (- (length srcs) (src-pos pan))
					    *brachy-rows*)))
			 (cond ((and (= i 1) (= j 0)) ;; up arrow
				(src-scroll pan (case info
						  (1 -1)
						  (2 -10))))
			       ((and (= i *brachy-rows*) (= j 0)) ;; down arrow
				(src-scroll pan (case info
						  (1 1)
						  (2 10))))
			       ((and (= i 0) (= j 1)) ;; "Go To" textline
				(aif (position info srcs :key #'id)
				     (src-scroll pan (- it (src-pos pan)))
				   (sl:acknowledge "No such source number")))
			       ((<= i lastrow)
				(let* ((src (nth (+ i -1 (src-pos pan)) srcs))
				       (srctab (source-data
						(source-type src))))
				  (case j
				    (1 (when (= info 1)
					 (aif (sl:popup-color-menu)
					      (progn
						(setf (display-color src) it
						      (sl:fg-color
						       (sl:cell-object
							sheet i j)) it)))
					 (sl:set-button sheet i j nil)))
				    (2 (when (= info 1)
					 (let ((srclist
						(source-menu
						 (not (zerop
						       (actlen srctab))))))
					   (aif (sl:popup-menu
						 (mapcar #'second srclist))
						(setf (source-type src)
						  (first (nth it srclist)))))
					 (sl:set-button sheet i j nil)))
				    (3 (setf (activity src)
					 (coerce info 'single-float)))
				    ;; action for P button
				    (5 (unless (busy pan)
					 (setf (busy pan) t)
					 (case info
					   (0 (setf (permanent src) nil))
					   (1 (setf (permanent src) t)))
					 (setf (busy pan) nil)))
				    (6 (if (permanent src)
					   (sl:acknowledge
					    (list
					     "Cannot change treatment time"
					     "for permanent implant source"))
					 (setf (treat-time src)
					   (coerce info 'single-float)))))))
			       (t (sl:acknowledge "That cell is empty")
				  (if (or (= j 1) (= j 2) (= j 5))
				      (if (= info 1)
					  (sl:set-button sheet i j nil))
				    (sl:erase-contents sheet i j)))))
		       (unless (busy pan)
			 (setf (busy pan) t)
			 (brachy-specs-refresh pan)
			 (setf (busy pan) nil))
		       ))))

;;;---------------------------------------------

(defun brachy-specs-refresh (panel)

  (let ((sheet (panel-frame panel)))
    (dotimes (row *brachy-rows*)
      (sl:set-contents sheet (+ row 1) 1 "") ;; source number button
      (sl:set-contents sheet (+ row 1) 2 "") ;; source type button
      (sl:erase-contents sheet (+ row 1) 3)
      (sl:erase-contents sheet (+ row 1) 6)
      (sl:erase-contents sheet (+ row 1) 7)
      (sl:erase-contents sheet (+ row 1) 8)
      (sl:erase-contents sheet (+ row 1) 9)
      (sl:erase-contents sheet (+ row 1) 10))
    (let ((row 0)
	  (pos (src-pos panel)))
      (dolist (src (nthcdr pos (coll:elements (src-coll panel))))
	(if (<= (incf row) *brachy-rows*) ;; don't go past the bottom!
	    (let ((srctab (source-data (source-type src))))
	      (setf (sl:fg-color (sl:cell-object sheet row 1))
		(display-color src))
	      (sl:set-contents sheet row 1
			       (format nil "~3@A" (id src)))
	      (sl:set-contents sheet row 2
			       (format nil "~A ~A C=~4,2F A=~4,2F"
				       (src-type srctab) (protocol srctab)
				       (dose-rate-const srctab)
				       (anisotropy-fn srctab)))
	      (sl:set-contents sheet row 3
			       (format nil "~6,3F" (activity src)))
	      (sl:set-contents sheet row 4 (activity-units srctab))
	      (unless (busy panel)
		(setf (busy panel) t)
		(sl:set-button sheet row 5 (permanent src))
		(setf (busy panel) nil))
	      (sl:set-contents sheet row 6
			       (format nil "~6,1F" (treat-time src)))
	      (sl:set-contents sheet row 7
			       (format nil "~6,2F"
				       (if (> (actlen srctab) 0)
					   (actlen srctab)
					 (first (location src)))))
	      (sl:set-contents sheet row 8
			       (format nil "~6,2F"
				       (if (> (actlen srctab) 0)
					   (physlen srctab)
					 (second (location src)))))
	      (sl:set-contents sheet row 9
			       (format nil "~6,2F"
				       (if (> (actlen srctab) 0)
					   (source-length src)
					 (third (location src)))))
	      ;; column 10 is currently left blank
	      ))))))

;;;---------------------------------------------

(defun src-scroll (panel amt)

  (when amt ;; could be nil - see case above
    (let ((tmp (+ (src-pos panel) amt))
	  (srclist (coll:elements (src-coll panel))))
      (when (and (>= tmp 0) (< tmp (length srclist)))
	(setf (src-pos panel) tmp)
	(brachy-specs-refresh panel)))))

;;;---------------------------------------------

(defun update-brachy-spec-cell (panel source info format-str column)

  "update-brachy-spec-cell panel source info format-str column

updates the display in the brachy-specs-panel panel if this source is
currently visible.  If it is not within the range of the displayed
rows, nothing is done."

  (let ((panel-pos (src-pos panel))
	(srcpos (position source (coll:elements (src-coll panel)))))
    (when (and (>= srcpos panel-pos) (< srcpos (+ panel-pos *brachy-rows*)))
      (sl:set-contents (panel-frame panel)
		       (+ srcpos (- panel-pos) 1) column
		       (format nil format-str info)))))

;;;---------------------------------------------

(defmethod destroy ((pan brachy-specs-panel))

  (dolist (src (coll:elements (src-coll pan)))
    (ev:remove-notify pan (new-source-type src))
    (ev:remove-notify pan (new-activity src))
    (ev:remove-notify pan (new-treat-time src))
    (ev:remove-notify pan (new-location src)))
  (ev:remove-notify pan (coll:inserted (src-coll pan)))
  (ev:remove-notify pan (coll:deleted (src-coll pan)))
  (sl:destroy (panel-frame pan)))

;;;---------------------------------------------
;;; update controls
;;;---------------------------------------------

(defvar *brachy-update-row-heights* '(25 25 25 25 25))

;;;---------------------------------------------

(defvar *brachy-update-col-widths* '(70 240 50 50))

;;;---------------------------------------------

(defvar *brachy-update-cells*
    (make-array '(5 4)
		:initial-contents
		`(((:button "Change" nil nil :button-type :momentary)
		   (:button "")
		   nil
		   nil
		   )
		  ((:button "Change" nil nil :button-type :momentary)
		   (:label "Source strength:")
		   (:number nil ,*brachy-activity-min*
		    ,*brachy-activity-max*) ;; source strength
		   (:readout "" nil nil :border-width 0) ;; src strength units
		   )
		  ((:button "Change" nil nil :button-type :momentary)
		   (:label "Application time:")
		   (:number nil ,*brachy-app-time-min*
		    ,*brachy-app-time-max*) ;; application time
		   (:label "Hours")
		   )
		  ((:button "Delete" nil nil :button-type :momentary)
		   (:label "Source number range:")
		   (:readout "First:" nil nil :border-width 0)
		   (:number nil 1 1000) ;; start source
		   )
		  (nil
		   nil
		   (:readout "Last:" nil nil :border-width 0)
		   (:number nil 1 1000) ;; end source
		   )
		  )))

;;;---------------------------------------------

(defclass brachy-update-panel ()

  ((src-coll :accessor src-coll
	     :initarg :src-coll
	     :documentation "The collection of sources for this panel,
from the plan.")

   (line-mode :accessor line-mode
	      :initarg :line-mode
	      :initform nil
	      :documentation "t if src-coll will contain line sources,
	      nil if seeds")

   (panel-frame :accessor panel-frame
		:documentation "The frame for this panel")

   (src-type :accessor src-type
	     :documentation "The currently selected source type")

   (src-strength :accessor src-strength
		 :initform 1.0
		 :documentation "The currently specified source strength.")

   (app-time :accessor app-time
	     :initform 1.0
	     :documentation "The currently specified application time
	     in hours.")

   (first-src :accessor first-src
	      :initform 0
	      :documentation "The first source to modify or delete.")

   (last-src :accessor last-src
	     :initform 0
	     :documentation "The last source to modify or delete.")

   ))

;;;---------------------------------------------

(defun make-brachy-update-panel (sources line-mode window x y)

  (make-instance 'brachy-update-panel
    :src-coll sources :line-mode line-mode
    :parent window :ulc-x x :ulc-y y
    :font (symbol-value *small-font*)
    :allow-other-keys t))

;;;---------------------------------------------

(defmethod initialize-instance :after ((srcpan brachy-update-panel)
				       &rest initargs)

  (let* ((pan-fr (apply #'sl:make-spreadsheet
			*brachy-update-row-heights*
			*brachy-update-col-widths*
			*brachy-update-cells*
			initargs))
	 (default-src-type (first (first (source-menu (line-mode srcpan)))))
	 (default-table (source-data default-src-type)))
    (setf (panel-frame srcpan) pan-fr)
    (setf (src-type srcpan) default-src-type)
    (sl:set-contents pan-fr 0 1
		     (format nil "~A ~A C=~4,2F A=~4,2F"
			     (src-type default-table)
			     (protocol default-table)
			     (dose-rate-const default-table)
			     (anisotropy-fn default-table)))
    (sl:set-contents pan-fr 1 2 (src-strength srcpan))
    (sl:set-contents pan-fr 1 3 (activity-units default-table))
    (sl:set-contents pan-fr 2 2 (app-time srcpan))
    (ev:add-notify srcpan (sl:user-input pan-fr)
		   #'(lambda (pan sheet i j info)
		       (let ((srcs (coll:elements (src-coll pan))))
			 (cond (;; update source type
				(and (= i 0) (= j 0) (= info 1))
				(dolist (src srcs)
				  (if (<= (first-src pan) (id src)
					  (last-src pan))
				      (setf (source-type src)
					(src-type pan)))))
			       (;; select source type
				(and (= i 0) (= j 1) (= info 1))
				(let ((srclist (source-menu (line-mode
							     pan))))
				  (aif (sl:popup-menu
					(mapcar #'second srclist))
				       (let* ((srcid (first (nth it srclist)))
					      (srctab (source-data srcid)))
					 (setf (src-type pan) srcid)
					 (sl:set-contents
					  sheet i j
					  (format nil "~A ~A C=~4,2F A=~4,2F"
						  (src-type srctab)
						  (protocol srctab)
						  (dose-rate-const srctab)
						  (anisotropy-fn srctab)))
					 ;; also update source
					 ;; strength units in i=1,j=3
					 (sl:set-contents
					  sheet 1 3 (activity-units srctab))
					 )))
				(sl:set-button sheet i j nil))
			       (;; update source strength
				(and (= i 1) (= j 0) (= info 1))
				(dolist (src srcs)
				  (if (<= (first-src pan) (id src)
					  (last-src pan))
				      (setf (activity src)
					(src-strength pan)))))
			       ((and (= i 1) (= j 2))
				(setf (src-strength pan)
				  (coerce info 'single-float)))
			       (;; update applic. time
				(and (= i 2) (= j 0) (= info 1))
				(dolist (src srcs)
				  (if (<= (first-src pan) (id src)
					  (last-src pan))
				      (setf (treat-time src)
					(app-time pan)))))
			       ((and (= i 2) (= j 2))
				(setf (app-time pan)
				  (coerce info 'single-float)))
			       (;; delete sources
				(and (= i 3) (= j 0) (= info 1))
				(dolist (src srcs)
				  (if (<= (first-src pan)
					  (id src)
					  (last-src pan))
				      (coll:delete-element
				       src (src-coll pan)))))
			       ((and (= i 3) (= j 3))
				(setf (first-src pan)
				  (coerce info 'single-float)))
			       ((and (= i 4) (= j 3))
				(setf (last-src pan)
				  (coerce info 'single-float)))
			       ))))
    ))

;;;---------------------------------------------

(defmethod (setf line-mode) :after (mode (pan brachy-update-panel))

  (let* ((default-src-type (first (first (source-menu mode))))
	 (default-table (source-data default-src-type))
	 (pan-fr (panel-frame pan)))
    (setf (src-type pan) default-src-type)
    (sl:set-contents pan-fr 0 1
		     (format nil "~A ~A C=~4,2F A=~4,2F"
			     (src-type default-table)
			     (protocol default-table)
			     (dose-rate-const default-table)
			     (anisotropy-fn default-table)))
    (sl:set-contents pan-fr 1 3 (activity-units default-table))))

;;;---------------------------------------------

(defmethod destroy ((pan brachy-update-panel))

  (sl:destroy (panel-frame pan)))

;;;---------------------------------------------
;;; End.
