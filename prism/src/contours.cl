;;;
;;; contours
;;;
;;; Defines polylines and contours and their methods
;;;
;;;  7-Sep-1992 I. Kalet taken initially from old volumes module
;;; 23-Jul-1993 I. Kalet move make-contour here from easel
;;;  3-Sep-1993 I. Kalet move draw methods to contour-graphics, marks
;;;  to points module
;;; 16-Jun-1994 I. Kalet change float to single-float
;;;  2-Jul-1997 BobGian modify :documentation string for CONTOUR class from
;;;      "the vertices must not all be collinear" to
;;;      "no three adjacent vertices can be collinear".  Testing for this
;;;      will also catch the "must enclose non-zero area" requirement.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass polyline ()

  ((z :type single-float 
      :initarg :z
      :accessor z) ; z coord. of plane of definition

   (vertices :type list
	     :initarg :vertices
	     :accessor vertices
	     :documentation "A list of 2-d coordinate pairs")

   (display-color :type symbol
		  :initarg :display-color
		  :accessor display-color)

   )

  (:default-initargs :vertices nil :display-color 'sl:magenta)

  (:documentation "Polylines represent any unconstrained curve in the
plane, like a clipped isodose contour or a physician's signature.")

  )

;;;--------------------------------------

(defclass contour (polyline)

  ()

  (:documentation "Contours are always part of some object, the type
of which determines the definition plane.  The vertices are a list of
coordinate pairs because there is nothing about points that would make
it worth having a list of point instances instead.  Structurally, a
contour is the same as a polyline but the implicit difference between
them is that contours are non-self-intersecting, must enclose non-zero
area, no three adjacent vertices can be collinear, and no vertices are
duplicated. It is also understood that the last point is connected to
the first, though it is not explicitly repeated in the vertices
list.")

  )

;;;--------------------------------------

(defun make-contour (&rest initargs)

  "MAKE-CONTOUR &rest initargs

Returns a contour with specified parameters."

  (apply #'make-instance 'contour initargs)
  )

;;;--------------------------------------
