;;;
;;; patients
;;;
;;; The Prism patient class and associated functions.
;;;
;;;  1-Aug-1992 I. Kalet created from rtp-objects
;;; 30-Nov-1992 I. Kalet cache table-position in plan when created,
;;; update plans when t-p is updated, set llc-anatomy and urc-anatomy
;;; to 0.0's if there is no anatomy.
;;; 16-Dec-1992 I.Kalet/J. Unger pass anatomy, tumors and targets sets
;;; to plans when they are created so the stuff can be displayed in
;;; views, also add images to plans when creating.
;;; 31-Dec-1992 I. Kalet let plan create image-view-manager, add
;;; action function to create anatomy managers when plan added to
;;; plans set.
;;;  1-Mar-1993 I. Kalet split off patient-panels separate module,
;;; delete history pertaining to panels.
;;; 11-Apr-1993 I. Kalet create new image-managers here when adding
;;; plans to plan set.  No organ sets etc. in plans so don't forward
;;;  3-Aug-1993 I. Kalet eliminate new-hospid, because hospid not
;;;  editable in Prism patient panel.  Add auto update of date-entered
;;;  when contours etc. change.  Don't save name, hospital id since
;;;  they are gotten from patient index file. Add new-date event.
;;; 18-Oct-1993 J. Unger cache organs and marks in plan when created.
;;; 20-Oct-1993 J. Unger initialize a plan's dose specification
;;; manager when plan is added to patient's plan collection.
;;; 26-Oct-1993 I. Kalet add mini-image-set cache for performance.
;;; 29-Oct-1993 J. Unger set dose-grid, result, and name attributes of
;;; each of a plan's dose surfaces to the plan's dose-grid, the plan's
;;; sum-dose, and the dose surface's threshold respectively, when the
;;; plan is inserted into the patient's collection of plans.
;;; 05-Nov-1993 J. Unger add code to set a plan's patient-id and
;;; case-id attributes when it is added to the patient's collection of
;;; plans.  Also add patient-id and case-id to patient's not-saved
;;; method, and add a setf :after method for case-id to set the
;;; case-id of each plan when patient's case-id changes.
;;;  3-Jan-1994 I. Kalet plans and beams now have back-pointers, so
;;;  don't forward stuff to plans, just set back-pointer to patient.
;;; 27-May-1994 J. Unger set default immob dev from *immob-device* list
;;; 02-Jun-1994 J. Unger update case when points change, comments, or
;;; the name of the case changes.
;;; 21-Jun-1994 I. Kalet declare date-entered to be slot type
;;; :timestamp and remove setf method for name.
;;; 30-Jun-1994 I. Kalet always set a new table-position to the origin.
;;; 29-Aug-1994 J. Unger minor adj to patient class def to fix bug
;;; involving 'old' info in comments box when new patient is selected.
;;; 07-Sep-1994 J. Unger add registration for points' new-name and
;;; new-loc events so patient timestamp can update.
;;; 12-Mar-1995 I. Kalet modify for patient-plan-mediators, add
;;; new-image-set event (back-pointers eliminated somewhere in here).
;;; 21-Jan-1997 I. Kalet eliminate table-position, urc-anat and
;;; llc-anat attributes and methods, leave latter two as functions.
;;; Also eliminate refs. to geometry package.  Use vector accessor
;;; macros from misc.
;;; 25-Jun-1997 I. Kalet merge patient-plan-mediators back into this
;;; module, keep the set here, not in a "manager" in patient panel.
;;;  3-Oct-1997 BobGian inline-expand LO-HI-COMPARE and fix result in
;;;   LLC-ANAT and URC-ANAT to make it cleaner, safer, and faster.
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization).
;;;  1-Jun-2009 I. Kalet remove mini-images and resize-image call, not
;;; precomputing these any more.
;;;

(in-package :prism)

;;;------------------------------------------

(defclass patient (generic-prism-object)

  ((patient-id :type fixnum
	       :initarg :patient-id
	       :accessor patient-id
	       :documentation "The system assigned patient id number.
Note that several cases may belong to the same patient, so there may
be several cases with the same patient id.")

   (case-id :type fixnum
	    :initarg :case-id
	    :accessor case-id
	    :documentation "A case id is also assigned by the system,
one case id per set of anatomy.")

   (hospital-id :type string
		:initarg :hospital-id
		:accessor hospital-id)

   (comments :type list
	     :initarg :comments
	     :accessor comments)

   (new-comments :type ev:event
		 :initform (ev:make-event)
		 :accessor new-comments
		 :documentation "Announced when the comments are updated.")

   (date-entered :type string
		 :initform (date-time-string)
		 :accessor date-entered
		 :documentation "The date entered is updated when a case
is modified, i.e., contours are changed or added, organs added, etc.")

   (new-date :type ev:event
	     :initform (ev:make-event)
	     :accessor new-date
	     :documentation "Announced when date-entered is updated.")

   (immob-device :type symbol
		 :initarg :immob-device
		 :accessor immob-device
		 :documentation "The immobilization device or method
used for this case, if any.")

   (new-immob-dev :type ev:event
		  :initform (ev:make-event)
		  :accessor new-immob-dev
		  :documentation "Announced when the immob. device is
changed.")

   (anatomy :accessor anatomy
	    :initform (coll:make-collection)
	    :documentation "A set of organs.") 

   (findings :accessor findings
	     :initform (coll:make-collection)
	     :documentation "A set of tumor instances for now.")

   (targets :accessor targets
	    :initform (coll:make-collection)
	    :documentation "A set of planning target volumes.")

   (points :accessor points
	   :initform (coll:make-collection)
	   :documentation "A set of marks in the patient volume.")

   (plans :accessor plans
	  :initform (coll:make-collection)
	  :documentation "A set of plans for this patient case.  There may
be (and usually are) more than one plan for a given patient and anatomy.")

   (image-set-id :type fixnum
		 :initarg :image-set-id
		 :accessor image-set-id
		 :documentation "The identifier for the data
describing the CT-scans.  Not a filename.  Assigned by the system.")

   (image-set :initarg :image-set
	      :accessor image-set
	      :documentation "The set of CT-scans from which the
anatomy was drawn, if any.")

   (new-image-set :type ev:event
		  :initform (ev:make-event)
		  :accessor new-image-set
		  :documentation "Announced when an image set is read
in or set from somewhere.")

   (pat-plan-mediator-set :accessor pat-plan-mediator-set
			  :initform (coll:make-collection)
			  :documentation "The set of patient-plan mediators")

   )

  (:default-initargs :name "" :patient-id 0 :case-id 0 :hospital-id ""
		     :comments (list "") 
		     :immob-device (second (first *immob-devices*))
		     :image-set nil :image-set-id 0)

  (:documentation "This is the information that describes the
patient's condition and anatomy, separately from the treatment plan,
which is the method of treating the condition.  Also, there may be
more than one instance of a patient object for a particular patient,
because the anatomy and prescription may change, thus rtp computations
will be different.")

  )

;;;------------------------------------------

(defmethod slot-type ((object patient) slotname)

  (case slotname
    ((anatomy findings targets points plans) :collection)
    (date-entered :timestamp)
    ((table-position urc-anat llc-anat) :ignore)
    (otherwise :simple)))

;;;------------------------------------------

(defmethod not-saved ((object patient))

  (append (call-next-method)
	  '(name hospital-id
		 new-date new-comments new-immob-dev
		 patient-id case-id
		 image-set new-image-set
		 pat-plan-mediator-set)))

;;;------------------------------------------

(defun reset-case-id (pat &rest ignored)

  "reset-case-id pat &rest ignored

sets case-id to 0 and updates the date entered attribute."

  (declare (ignore ignored))
  (setf (case-id pat) 0)
  (setf (date-entered pat) (date-time-string)))

;;;----------------------------------

(defclass patient-plan-mediator ()

  ((the-patient :accessor the-patient
		:initarg :the-patient
		:documentation "The patient case this mediator connects to.")

   (the-plan :accessor the-plan
	     :initarg :the-plan
	     :documentation "The plan this mediator connects to.")

   (org-vm :accessor org-vm
	   :documentation "The organs-views-manager.")

   (tum-vm :accessor tum-vm
	   :documentation "The tumors-views-manager.")

   (tar-vm :accessor tar-vm
	   :documentation "The targets-views-manager.")

   (pts-vm :accessor pts-vm
	   :documentation "The points-views-manager.")

   (im-vm :accessor im-vm
	  :documentation "The image-view manager")

   (dsm :accessor dsm
	:documentation "The plan's dose-specification manager.")

   ))

;;;----------------------------------

(defmethod initialize-instance :after ((ppm patient-plan-mediator)
				       &rest initargs)

  (declare (ignore initargs))
  (let ((pat (the-patient ppm))
	(pln (the-plan ppm)))
    (setf (im-vm ppm)                               ;; nil if no image set yet
	  (if (image-set pat) (make-image-view-manager
				(image-set pat) (plan-views pln))))
    (setf (pts-vm ppm) (make-object-view-manager
			 (points pat) (plan-views pln)
			 #'make-point-view-mediator))
    (setf (org-vm ppm) (make-object-view-manager
			 (anatomy pat) (plan-views pln)
			 #'make-pstruct-view-mediator))
    (setf (tum-vm ppm) (make-object-view-manager
			 (findings pat) (plan-views pln)
			 #'make-pstruct-view-mediator))
    (setf (tar-vm ppm) (make-object-view-manager
			 (targets pat) (plan-views pln)
			 #'make-pstruct-view-mediator))
    (setf (dsm ppm) (make-dose-specification-manager
		      :organs (anatomy pat)
		      :grid (dose-grid pln)
		      :beams (beams pln)
		      :seeds (seeds pln)
		      :line-sources (line-sources pln)
		      :points (points pat)))
    (ev:add-notify ppm (new-image-set pat)
      #'(lambda (med pt)
	  (setf (im-vm med)
		(make-image-view-manager
		  (image-set pt)
		  (plan-views (the-plan med))))))))

;;;--------------------------------------

(defmethod destroy ((ppm patient-plan-mediator))

  "destroys the individual mediator managers."

  (ev:remove-notify ppm (new-image-set (the-patient ppm)))
  (destroy (org-vm ppm))
  (destroy (tum-vm ppm))
  (destroy (tar-vm ppm))
  (destroy (dsm ppm)))

;;;------------------------------------------

(defmethod initialize-instance :after ((p patient) &rest initargs)

  "Arranges for the patient's case-id (and date-entered attrib) to get
updated when a significant change to one of the lists of pstructs or points
occurs.  Also creates the patient-plan-manager."

  (declare (ignore initargs))
  (ev:add-notify p (coll:inserted (anatomy p))
    #'(lambda (pat ann org)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:add-notify pat (update-case org)
	  #'reset-case-id)))
  (ev:add-notify p (coll:deleted (anatomy p))
    #'(lambda (pat ann org)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:remove-notify pat (update-case org))))
  (ev:add-notify p (coll:inserted (findings p))
    #'(lambda (pat ann tum)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:add-notify pat (update-case tum)
	  #'reset-case-id)))
  (ev:add-notify p (coll:deleted (findings p))
    #'(lambda (pat ann tum)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:remove-notify pat (update-case tum))))
  (ev:add-notify p (coll:inserted (targets p))
    #'(lambda (pat ann targ)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:add-notify pat (update-case targ)
	  #'reset-case-id)))
  (ev:add-notify p (coll:deleted (targets p))
    #'(lambda (pat ann targ)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:remove-notify pat (update-case targ))))
  (ev:add-notify p (coll:inserted (points p))
    #'(lambda (pat ann pt)
	(declare (ignore ann))
	(reset-case-id pat)
	;; register with this point's new-loc & new-name events
	(ev:add-notify pat (new-name pt)
	  #'(lambda (pat ann nm)
	      (declare (ignore ann nm))
	      (reset-case-id pat)))
	(ev:add-notify pat (new-loc pt)
	  #'(lambda (pat ann nm)
	      (declare (ignore ann nm))
	      (reset-case-id pat)))))
  (ev:add-notify p (coll:deleted (points p))
    #'(lambda (pat ann pt)
	(declare (ignore ann))
	(reset-case-id pat)
	(ev:remove-notify pat (new-name pt))
	(ev:remove-notify pat (new-loc pt))))
  (dolist (pl (coll:elements (plans p)))
    (coll:insert-element (make-instance 'patient-plan-mediator
			   :the-patient p
			   :the-plan pl)
			 (pat-plan-mediator-set p)))
  (ev:add-notify p (coll:inserted (plans p))
    #'(lambda (pat pln-set pln)
	(declare (ignore pln-set))
	(coll:insert-element (make-instance
			       'patient-plan-mediator
			       :the-patient pat
			       :the-plan pln)
			     (pat-plan-mediator-set pat))))
  (ev:add-notify p (coll:deleted (plans p))
    #'(lambda (pat pln-set pln)
	(declare (ignore pln-set))
	(let* ((ppm-set (pat-plan-mediator-set pat))
	       (ppm (find pln (coll:elements ppm-set)
			  :key #'the-plan)))
	  (coll:delete-element ppm ppm-set)
	  (destroy ppm)
	  (destroy pln)))))

;;;------------------------------------------

(defmethod (setf date-entered) :after (text (p patient))

  (ev:announce p (new-date p) text))

;;;------------------------------------------

(defmethod (setf comments) :after (text (p patient))

  (reset-case-id p)
  (ev:announce p (new-comments p) text))

;;;------------------------------------------

(defmethod (setf immob-device) :after (text (p patient))

  (ev:announce p (new-immob-dev p) text))

;;;------------------------------------------

(defun llc-anat (pat)

  "llc-anat pat

Computes the extreme lower limits of the contours of the objects in
the anatomy slot of the patient case PAT and returns them as a three
element list, the lowest X value, the lowest Y value and lowest Z
value in order."

  (let ((min-x #.most-positive-single-float)
	(min-y #.most-positive-single-float)
	(min-z #.most-positive-single-float))
    (declare (single-float min-x min-y min-z))
    (dolist (org (coll:elements (anatomy pat)))
      (dolist (cont (contours org))
	(dolist (vert (vertices cont))
	  (let ((x (first vert))
		(y (second vert)))
	    (declare (single-float x y))
	    (when (< x min-x)
	      (setq min-x x))
	    (when (< y min-y)
	      (setq min-y y))))
	(let ((z (z cont)))
	  (declare (single-float z))
	  (when (< z min-z)
	    (setq min-z z)))))
    (list min-x min-y min-z)))

;;;------------------------------------------

(defun urc-anat (pat)

  "urc-anat pat

Computes the extreme upper limits of the contours of the objects in
the anatomy slot of the patient case PAT and returns them as a three
element list, the highest X value, the highest Y value and highest Z
value in order."

  (let ((max-x #.most-negative-single-float)
	(max-y #.most-negative-single-float)
	(max-z #.most-negative-single-float))
    (declare (single-float max-x max-y max-z))
    (dolist (org (coll:elements (anatomy pat)))
      (dolist (cont (contours org))
	(dolist (vert (vertices cont))
	  (let ((x (first vert))
		(y (second vert)))
	    (declare (single-float x y))
	    (when (> x max-x)
	      (setq max-x x))
	    (when (> y max-y)
	      (setq max-y y))))
	(let ((z (z cont)))
	  (declare (single-float z))
	  (when (> z max-z)
	    (setq max-z z)))))
    (list max-x max-y max-z)))

;;;---------------------------------------------

(defmethod (setf image-set) :after (imgs (p patient))

  "Just announces the new image set."

  (declare (ignore imgs))
  (ev:announce p (new-image-set p)))

;;;---------------------------------------------
;;; End.
