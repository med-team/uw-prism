;;;
;;; selector-panels
;;;
;;; the Prism code for composing a scrolling-list, an Add button and a
;;; bunch of objects, e.g. organs, into a component of a panel, e.g.
;;; the patient panel or the plan panel.
;;;
;;; Requirements:
;;;
;;; 1. The object must have a name attribute, a text string, with an
;;; accessor called name, and a new-name event which is announced when
;;; the name attribute is updated (so the button text in the
;;; scrolling-list can update too).
;;;
;;; 2. The panel must have a deleted event and a destroy method,
;;; referring to symbols in the prism package.
;;;
;;; These are satisfied if the object class is a subclass of
;;; generic-prism-object and the panel class is a subclass of
;;; generic-prism-panel, defined in the prism-objects module.
;;;
;;; 3. The object-fn in make-selector-panel is a function that
;;; constructs a new instance of the object class.  Its only parameter
;;; is a string for the name.
;;;
;;; 4. The panel-fn is a function that makes a new panel instance of
;;; the right type for the object class.  Its only parameter is the
;;; object for which it is to be made.
;;;
;;; An additional keyword argument may be supplied to the constructor
;;; function make-selector-panel, :use-color, if the object class has
;;; a display-color attribute, whose value is a SLIK color symbol, and
;;; an event named new-color, that announces the new color of the
;;; object.  If the use-color argument to make-selector-panel is true,
;;; the objects mediator registers with new-color and keeps the
;;; foreground color of the button consistent with the object color.
;;;
;;; 29-May-1992 I. Kalet started
;;;  9-Jun-1992 I. Kalet add generic-panel so this file can be loaded
;;;  before the rest of the application code.
;;;  7-Jul-1992 I. Kalet moved make-new-button to SLIK, renamed
;;;  make-list-button, change be: to ev: and behavior to event
;;;  8-Aug-1992 I. Kalet change action function for Add button to make
;;;  and insert an object, not a button.  Delete prompt-for-string.
;;; 18-Aug-1992 I. Kalet add destroy method to reclaim X resources,
;;; and unregister with object set.  Also, add code to create buttons
;;; for objects in initially non-empty object set (at last).
;;; 22-Aug-1992 I. Kalet fix up generic-panel class so it can be used
;;; by stub code.
;;; 16-Sep-1992 I. Kalet move generic object and panel code to
;;; prism-objects module.
;;; 02-Jan-1993 I. Kalet make destroy method remove notification for
;;; new-name event in all the objects.  Also, remove notification for
;;; new-name of object being deleted from object set, i.e., don't
;;; assume the object is destroyed.
;;;  6-Aug-1993 I. Kalet now that delete button for scrolling lists is
;;;  finally implemented, need to enable it here.
;;;  3-Sep-1993 I. Kalet correct error discovered by Kevin Sullivan,
;;;  omitted registration and deregistration for button insertion and
;;;  deletion.
;;; 23-Jun-1997 I. Kalet add use-color keyword parameter, add search
;;; functions button-for and object-for, add radio keyword to make
;;; radio-selector-panel.
;;; 22-Mar-1999 I. Kalet add a popup-list-sort function, that can be
;;; used to reorder the objects in the object set, and correspondingly
;;; the buttons in the button set, without destroying or deleting the
;;; objects, the buttons or the relationships.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 26-Nov-2000 I. Kalet cosmetics to popup-list-sort
;;;  2-Dec-2000 I. Kalet move select-1 here from volume-editor.
;;; 26-Dec-2001 I. Kalet change popup-list-sort to move the remaining
;;; objects from the original list to the new list even if the user
;;; did not move them.
;;;

(in-package :prism)

;;;----------------------------------

(defclass selector-panel ()

  ((objects ;; :type coll:collection
	    :accessor objects
	    :initarg :objects
	    :initform (coll:make-collection) ; usually supplied as initarg
	    :documentation "The set of actual objects, e.g., organs,
that are being selected and added and deleted.")

   (panels ;; :type coll:collection
	   :accessor panels
	   :initform (coll:make-collection) ; initially, no panels
	   :documentation "The set of panels, one for each selected
object, for editing the object's attributes.")

   (scroll-list ;; :type sl:scrolling-list
		:accessor scroll-list
		:documentation "The SLIK scrolling-list widget
containing the buttons for the organs.")

   (objects-mediator :accessor objects-mediator
		     :documentation "A mediator to connect the object
set and the selection list.  Created by initialization of selector-panel.")

   (panels-mediator :accessor panels-mediator
		    :documentation "A mediator to connect the panel set
and the selection list.  Created by initialization of selector-panel.")

   (add-button ;; :type sl:button
	       :accessor add-button
	       :documentation "The SLIK button the user presses to add
a new instance of the object.")

   (selector-frame ;; :type sl:frame
		   :accessor selector-frame
		   :documentation "The SLIK frame containing the
scrolling-list and the Add button.")

   )

  (:documentation "The selector-panel class provides the higher level
machinery to provide creation, selection, deselection and deletion of
various sets of objects that are in the Prism patient model, such as
organs, plans, beams, views, etc.")

  )

;;;---------------------------------------

(defclass objects-mediator ()

  ((objects ;; :type coll:collection
	    :accessor objects
	    :initarg :objects
	    :documentation "A reference to the object set.")

   (scroll-list ;; :type sl:scrolling-list
		:accessor scroll-list
		:initarg :scroll-list
		:documentation "A reference to the scrolling list.")

   (use-color :accessor use-color
	      :initarg :use-color
	      :documentation "A boolean, if true, the mediator should
make the button fg-color track the value provided by the object's
new-color event announcement.")

   (button-object-relation ;; :type coll:relation
			   :accessor button-object-relation
			   :initform (coll:make-relation)
			   :documentation "The relation connecting the
button set with the object set.  Referenced here and in the
panels-mediator.")

   (busy :accessor busy
	 :initform nil
	 :documentation "A flag for handling circularity.")

   )

  (:documentation "The mediator that connects the object set and the
scrolling list button set.")

  )

;;;---------------------------------------

(defclass panels-mediator ()

  ((panels ;; :type coll:collection
	   :accessor panels
	   :initarg :panels
	   :documentation "A reference to the panel set.")

   (scroll-list ;; :type sl:scrolling-list
		:accessor scroll-list
		:initarg :scroll-list
		:documentation "A reference to the scrolling list.")

   (button-panel-relation ;; :type coll:relation
			  :accessor button-panel-relation
			  :initform (coll:make-relation)
			  :documentation "The relation connecting the
selected button set with the panel set.")

   (button-object-relation ;; :type coll:relation
			   :accessor button-object-relation
			   :initarg :button-object-relation
			   :documentation "The relation connecting the
button set with the object set.  Referenced here as well as in the
objects-mediator class.")

   (busy :accessor busy
	 :initform nil
	 :documentation "A flag for handling circularity.")

   )

  (:documentation "The mediator that connects the panel set and the
scrolling list button set.")

  )

;;;---------------------------------------

(defmethod button-for (obj (med objects-mediator))

  (first (coll:projection obj (coll:inverse-relation
			       (button-object-relation med)))))

;;;---------------------------------------

(defmethod button-for (obj (pan selector-panel))

  "button-for obj (pan selector-panel)

returns the button in the selector panel pan corresponding to the
object obj, or nil if not found."

  (button-for obj (objects-mediator pan)))

;;;---------------------------------------

(defmethod object-for (btn (med objects-mediator))

  (first (coll:projection btn (button-object-relation med))))

;;;---------------------------------------

(defmethod object-for (btn (med panels-mediator))

  (first (coll:projection btn (button-object-relation med))))

;;;---------------------------------------

(defmethod object-for (btn (pan selector-panel))

  "object-for btn (pan selector-panel)

returns the object in the selector panel pan corresponding to the
button btn, or nil if not found."

  (object-for btn (objects-mediator pan)))

;;;---------------------------------------

(defmethod initialize-instance :after ((sp selector-panel)
				       &rest other-initargs
				       &key width height
				       object-fn panel-fn
				       use-color radio
				       &allow-other-keys)

  "This method creates the panel with the scrolling list and Add
button."

  (let* ((sf (apply 'sl:make-frame width height other-initargs))
	 (win (sl:window sf))
	 (fh (sl:font-height (sl:font sf)))
	 (bh (+ fh 10)) ; this is for the Add button
	 (scr (apply (if radio 'sl:make-radio-scrolling-list
		       'sl:make-scrolling-list)
		     width (- height bh 20) ;; leave room for Add button
		     :parent win
		     :ulc-x 0 :ulc-y (+ bh 20)
		     :enable-delete t
		     other-initargs))
	 (b (apply 'sl:make-button (- width 20) bh ; a little margin
		   :parent win
		   :button-type :momentary
		   :ulc-x 10 :ulc-y 10	; based on margins above
		   other-initargs)))	; should contain a :label parameter
    (setf (selector-frame sp) sf
	  (scroll-list sp) scr
	  (add-button sp) b)
    (setf (objects-mediator sp) (make-instance 'objects-mediator
				  :objects (objects sp)
				  :scroll-list scr
				  :object-fn object-fn
				  :use-color use-color)) ;; pass through
    (setf (panels-mediator sp) (make-instance 'panels-mediator
				 :panels (panels sp)
				 :scroll-list scr
				 :panel-fn panel-fn
				 ;; and we need a reference to the
				 ;; newly created button-object
				 ;; relation in the other mediator
				 :button-object-relation
				 (button-object-relation
				  (objects-mediator sp))))
    (ev:add-notify sp (sl:button-on b)
		   #'(lambda (pan bt) ;; action for Add button
		       (let ((obj (funcall object-fn ""))) ; no name yet
			 (coll:insert-element obj (objects pan))
			 (sl:select-button (button-for obj pan)
					   (scroll-list pan)))
		       ;; do the following in case the button-release
		       ;; X event got discarded by the object-fn
		       (setf (sl:on bt) nil)))))

;;;---------------------------------------

(defun make-selector-panel (width height label object-set
			    object-fn panel-fn
			    &rest other-initargs)

  "make-selector-panel width height label object-set object-fn panel-fn
                       &rest other-initargs

returns an instance of a selector-panel, with objects in the provided
object-set, and buttons for each.  The :use-color and :radio
parameters are in the other-initargs, if provided."

  (apply 'make-instance 'selector-panel
	 :width width :height height
	 :objects object-set
	 :label label
	 :object-fn object-fn :panel-fn panel-fn
	 other-initargs))

;;;---------------------------------------

(defmethod initialize-instance :after ((om objects-mediator)
				       &rest initargs
				       &key object-fn &allow-other-keys)

  "Sets up the initial relation between the object set and the scroll
list."

  (declare (ignore initargs))

  ;; add buttons to scroll list for objects initially in object set
  (let ((scr (scroll-list om))
	(obj-list (coll:elements (objects om))))
    (setf (busy om) t) ;; don't create more objects indirectly...
    (dolist (obj obj-list)
      (let ((b (sl:make-list-button scr (name obj))))
	(ev:add-notify b (new-name obj)
		       #'(lambda (bt ob nm)
			   (declare (ignore ob))
			   (setf (sl:label bt) nm)))
	(when (use-color om)
	  (setf (sl:fg-color b) (display-color obj))
	  (ev:add-notify b (new-color obj)
			 #'(lambda (bt ob col)
			     (declare (ignore ob))
			     (setf (sl:fg-color bt) col))))
	(sl:insert-button b scr)
	(coll:insert-element (list b obj) (button-object-relation om))))
    (setf (busy om) nil))

  ;; register with object set
  (ev:add-notify om (coll:inserted (objects om))
		 #'(lambda (omed oset obj)
		     (declare (ignore oset))
		     (when (not (busy omed))
		       (setf (busy omed) t)
		       (let* ((scr (scroll-list omed))
			      ;; we assume there is a name reader
			      ;; function for the new object
			      (b (sl:make-list-button scr (name obj))))
			 ;; when object name changes update the
			 ;; button label in the scrolling-list
			 (ev:add-notify b (new-name obj)
					#'(lambda (l a nm)
					    (declare (ignore a))
					    (setf (sl:label l) nm)))
			 (when (use-color omed)
			   (setf (sl:fg-color b) (display-color obj))
			   (ev:add-notify b (new-color obj)
					  #'(lambda (bt ob col)
					      (declare (ignore ob))
					      (setf (sl:fg-color bt) col))))
			 (sl:insert-button b scr)
			 (coll:insert-element (list b obj)
					      (button-object-relation omed)))
		       (setf (busy omed) nil))))
  (ev:add-notify om (coll:deleted (objects om))
		 #'(lambda (omed oset obj)
		     (declare (ignore oset))
		     (when (not (busy omed))
		       (setf (busy omed) t)
		       (let ((b (button-for obj omed)))
			 (ev:remove-notify b (new-name obj))
			 (if (use-color omed)
			     (ev:remove-notify b (new-color obj)))
			 (sl:delete-button b (scroll-list omed))
			 (coll:delete-element (list b obj)
					      (button-object-relation omed)))
		       (setf (busy omed) nil))))

  ;; register with scroll list
  (ev:add-notify om (sl:inserted (scroll-list om))
		 #'(lambda (omed sc b)
		     (declare (ignore sc))
		     (when (not (busy omed))
		       (setf (busy omed) t)
		       (let ((obj (funcall object-fn (sl:label b))))
			 (coll:insert-element obj (objects omed))
			 ;; when object name changes update the
			 ;; button label in the scrolling-list
			 (ev:add-notify b (new-name obj)
					#'(lambda (bt ob nm)
					    (declare (ignore ob))
					    (setf (sl:label bt) nm)))
			 (when (use-color omed)
			   (setf (sl:fg-color b) (display-color obj))
			   (ev:add-notify b (new-color obj)
					  #'(lambda (bt ob col)
					      (declare (ignore ob))
					      (setf (sl:fg-color bt) col))))
			 (coll:insert-element (list b obj)
					      (button-object-relation omed)))
		       (setf (busy omed) nil))))
  (ev:add-notify om (sl:deleted (scroll-list om))
		 #'(lambda (omed sc b)
		     (declare (ignore sc))
		     (when (not (busy omed))
		       (setf (busy omed) t)
		       (let ((obj (object-for b omed)))
			 (ev:remove-notify b (new-name obj))
			 (if (use-color omed)
			     (ev:remove-notify b (new-color obj)))
			 (coll:delete-element obj (objects omed))
			 (coll:delete-element (list b obj)
					      (button-object-relation omed)))
		       (setf (busy omed) nil)))))

;;;---------------------------------------

(defmethod initialize-instance :after ((pm panels-mediator)
				       &rest initargs
				       &key panel-fn &allow-other-keys)

  "Sets up the initial relation between the panel set and the scroll
list."

  (declare (ignore initargs))

  ;; register with panel set: note that we do not register with
  ;; (inserted (panels pm)) because this should not happen outside the
  ;; mediator, though there is no neat way to enforce it.
  (ev:add-notify pm (coll:deleted (panels pm))
		 #'(lambda (pm a pan)
		     (declare (ignore a))
		     (when (not (busy pm))
		       (setf (busy pm) t)
		       (let ((b (first (coll:projection
					pan (coll:inverse-relation
					     (button-panel-relation pm))))))
			 (sl:deselect-button b (scroll-list pm))
			 (coll:delete-element (list b pan)
					      (button-panel-relation pm)))
		       (setf (busy pm) nil))))

  ;; register with scroll list
  (ev:add-notify pm (sl:selected (scroll-list pm))
		 #'(lambda (pm sc b)
		     (declare (ignore sc))
		     (when (not (busy pm))
		       (setf (busy pm) t)
		       (let* ((obj (object-for b pm))
			      (p (funcall panel-fn obj)))
			 (coll:insert-element p (panels pm))
			 (coll:insert-element (list b p)
					      (button-panel-relation pm))
			 (ev:add-notify pm (deleted p)
					#'(lambda (pm pan)
					    (coll:delete-element
					     pan (panels pm)))))
		       (setf (busy pm) nil))))
  (ev:add-notify pm (sl:deselected (scroll-list pm))
		 #'(lambda (pm a b)
		     (declare (ignore a))
		     (when (not (busy pm))
		       (setf (busy pm) t)
		       (let ((pan (first (coll:projection
					  b (button-panel-relation pm)))))
			 (coll:delete-element (list b pan)
					      (button-panel-relation pm))
			 (destroy pan))
		       (setf (busy pm) nil)))))

;;;----------------------------------------

(defmethod destroy ((sp selector-panel))

  "Deselects all the buttons to remove the panels, then destroys the
components."

  (sl:destroy (add-button sp))
  (let* ((scr (scroll-list sp))
	 (om (objects-mediator sp))
	 (objs (objects om)))
    (mapc #'(lambda (b)
	      (sl:deselect-button b scr)
	      (let ((ob (object-for b om)))
		(ev:remove-notify b (new-name ob))
		(if (use-color om)
		    (ev:remove-notify b (new-color ob)))))
	  (sl:buttons scr))
    ;; unregister from the scrolling list before destroying it
    (ev:remove-notify om (sl:inserted scr))
    (ev:remove-notify om (sl:deleted scr))
    (sl:destroy scr)
    (ev:remove-notify om (coll:inserted objs))
    (ev:remove-notify om (coll:deleted objs)))
  (sl:destroy (selector-frame sp)))

;;;------------------------------------------

(defun select-1 (sel-pan)

  "a helper function that turns on the first button in the selector
panel sel-pan and returns t if there are any, otherwise returns nil"

  (let* ((scr-list (scroll-list sel-pan))
	 (btn-list (sl:buttons scr-list)))
    (when btn-list
      (sl:select-button (first btn-list) scr-list)
      (return-from select-1 t))))

;;;------------------------------------------

(defun popup-listsort (panel)

  "popup-listsort panel

Provides an interactive panel for reordering the objects in the object
set of selector-panel panel, and also reordering the corresponding
buttons in the scrolling list of the selector-panel."

  (sl:push-event-level)
  (let* ((ppf (symbol-value *small-font*))
	 (bth 25) ;; button and textline height for small font
	 (btw 120) ;; regular button and textline width
	 (dx 10) ;; left margin
	 (top-y 10)
	 (scr-ht 210) ;; the height of the scrolling lists
	 (width (+ dx btw 10 btw 10))
	 (height (+ top-y bth 10 scr-ht 10 bth 10))
	 (sortpanel (sl:make-frame width height
				   :title "List Sort Panel"))
	 (pp-win (sl:window sortpanel))
	 (old-rdt (sl:make-readout btw bth :font ppf
				   :info "Old list"
				   :ulc-x dx :ulc-y top-y
				   :parent pp-win))
	 (new-rdt (sl:make-readout btw bth :font ppf
				   :info "New list"
				   :ulc-x (+ dx btw 10) :ulc-y top-y
				   :parent pp-win))
	 (old-scr (sl:make-scrolling-list btw scr-ht :font ppf
					  :ulc-x dx
					  :ulc-y (+ top-y bth 10)
					  :parent pp-win))
	 (new-scr (sl:make-scrolling-list btw scr-ht :font ppf
					  :ulc-x (+ dx btw 10)
					  :ulc-y (+ top-y bth 10)
					  :parent pp-win))
	 (sbw (+ 10 (clx:text-width ppf "Accept")))
	 (left-x (round (/ (- width (* 2 sbw) 10) 2)))
	 (accept-b (sl:make-exit-button sbw bth :font ppf
					:label "Accept" :parent pp-win
					:ulc-x left-x
					:ulc-y (- height bth 10)
					:bg-color 'sl:green))
	 (cancel-b (sl:make-exit-button sbw bth :font ppf
					:label "Cancel" :parent pp-win
					:ulc-x (+ left-x sbw 10)
					:ulc-y (- height bth 10)))
	 (obj-set (objects panel))
	 (scr (scroll-list panel))
	 (old-oblist (copy-list (coll:elements obj-set)))
	 new-oblist)
    (mapc #'(lambda (bm)
	      (sl:make-and-insert-list-button old-scr (name bm)))
	  old-oblist)
    ;; when buttons are pressed, move objects and
    ;; buttons from one list to the other.
    (ev:add-notify old-oblist (sl:selected old-scr)
		   #'(lambda (oblist oscr bt)
		       (declare (ignore oblist))
		       (let* ((index (position bt (sl:buttons oscr)))
			      (obj (nth index old-oblist))
			      (btlabel (sl:label bt)))
			 (setf new-oblist (append new-oblist (list obj))
			       old-oblist (remove obj old-oblist))
			 (sl:delete-button bt oscr)
			 (sl:make-and-insert-list-button new-scr btlabel))))
    (ev:add-notify new-oblist (sl:selected new-scr)
		   #'(lambda (oblist nscr bt)
		       (declare (ignore oblist))
		       (let* ((index (position bt (sl:buttons nscr)))
			      (obj (nth index new-oblist))
			      (btlabel (sl:label bt)))
			 (setf old-oblist (append old-oblist (list obj))
			       new-oblist (remove obj new-oblist))
			 (sl:delete-button bt nscr)
			 (sl:make-and-insert-list-button old-scr btlabel))))
    (ev:add-notify obj-set (sl:button-on accept-b)
		   #'(lambda (coll btn)
		       (declare (ignore btn))
		       (let ((tmplist (append new-oblist old-oblist)))
			 ;; replace old list in coll with new list
			 ;; of same objects, and replace list of
			 ;; buttons in scrolling list with the same
			 ;; buttons in new order.
			 (progn
			   (setf (coll:elements coll) tmplist)
			   (sl:reorder-buttons
			    scr
			    (mapcar #'(lambda (ob)
					(find (name ob) (sl:buttons scr)
					      :key #'sl:label))
				    tmplist))))))
    (sl:process-events)
    (sl:destroy old-rdt)
    (sl:destroy new-rdt)
    (sl:destroy old-scr)
    (sl:destroy new-scr)
    (sl:destroy accept-b)
    (sl:destroy cancel-b)
    (sl:destroy sortpanel))
  (sl:pop-event-level))

;;;----------------------------------------
;;; End.
