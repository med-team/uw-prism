;;;
;;; brachy-dose
;;;
;;; Functions that implement the brachytherapy dose computation.
;;;
;;;  2-Jan-1996 I. Kalet created
;;;  7-Mar-1997 I. Kalet finally start implementation
;;; 24-Mar-1997 I. Kalet ongoing work...
;;;  7-May-1997 BobGian inlined (SQR x) to (* x x) where arg is symbol;
;;;    left as call to inlined fcn SQR where arg is a form.
;;; 30-Oct-1997 BobGian COMPUTE-xxx-DOSE fcns return T on success.
;;;  1-Feb-2000 I. Kalet create local variables for efficiency, use
;;; first, second, third to access source end coordinates etc., use
;;; flet for local dose comp. expressions.
;;; 27-Feb-2000 I. Kalet add type declarations.
;;;  8-May-2000 I. Kalet gamma factor now split into dose rate
;;; constant and anisotropy factor.
;;; 19-Jun-2000 I. Kalet handle points close to source exactly as in
;;; UWPLAN.
;;; 20-Jun-2000 I. Kalet protect against user entering zero length
;;; line sources.
;;; 24-Jul-2002 I. Kalet make flat cutoff for distances less than
;;; *brachy-min-dist* parameter.  Add terminal output of progress.
;;; 18-Sep-2002 I. Kalet return nil when no pts or gg are provided,
;;; and be sure to return non-nil when successful.
;;; 27-Dec-2002 I. Kalet add two more terms to polynomial tissue
;;; correction.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defvar *brachy-min-dist* 0.2 "The minimum distance for computing a
reasonable dose.  For less than this, the value at this distance is used.")

;;;--------------------------------------------------

(defun compute-line-dose (src pts gg)

  "compute-line-dose src pts gg

Computes the doses to points or dose array (but NOT both) for line
source src and puts them in the results object of src.  Returns
results on success, nil if no pts or gg."

  (let* ((src-table (source-data (source-type src)))
	 (gamma (* (dose-rate-const src-table) (anisotropy-fn src-table)))
	 (active-length (actlen src-table))
	 (range (poly-range src-table))
	 (mu-w (mu-water src-table))
	 (a0 (a0 src-table))
	 (a1 (a1 src-table))
	 (a2 (a2 src-table))
	 (a3 (a3 src-table))
	 (a4 (a4 src-table))
	 (a5 (a5 src-table))
	 (src-end1 (end-1 src))
	 (src-end2 (end-2 src))
	 (xs1 (first src-end1))
	 (ys1 (second src-end1))
	 (zs1 (third src-end1))
	 (xs2 (first src-end2))
	 (ys2 (second src-end2))
	 (zs2 (third src-end2))
	 (xs (/ (+ xs1 xs2) 2.0))
	 (ys (/ (+ ys1 ys2) 2.0))
	 (zs (/ (+ zs1 zs2) 2.0))
	 (dxs (- xs2 xs1))
	 (dys (- ys2 ys1))
	 (dzs (- zs2 zs1))
	 (source-length (sqrt (+ (* dxs dxs) (* dys dys) (* dzs dzs)))))
    (declare (single-float xs ys zs gamma active-length dxs dys dzs
			   source-length a0 a1 a2 a3 a4 a5 range mu-w))
    (if (> source-length 0.0)
	(flet ((line-dose (x y z)
		 (let* ((rx (- x xs))
			(ry (- y ys))
			(rz (- z zs))
			(r2 (+ (* rx rx) (* ry ry) (* rz rz)))
			(r (sqrt r2))
			(reduced-r (/ r active-length))
			)
		   (declare (single-float rx ry rz r2 r reduced-r))
		   ;; for points close to the center of a source, set dose
		   ;; rate same as at cutoff distance
		   (if (< r *brachy-min-dist*)
		       (setq r *brachy-min-dist*
			     r2 (* r r)
			     reduced-r (/ r active-length)))
		   (/ (* gamma (sievert reduced-r
					;; this is cos theta
					(/ (abs (+ (* rx dxs)
						   (* ry dys)
						   (* rz dzs)))
					   (* r source-length))
					(sievert-table src-table))
			 (tisscorr r range mu-w a0 a1 a2 a3 a4 a5))
		      r2))))
	  (if pts
	      (setf (points (result src)) ;; also returns success
		(mapcar #'(lambda (pt) (line-dose (x pt) (y pt) (z pt)))
			pts))
	    (if gg
		(let* ((nx (x-dim gg))
		       (ny (y-dim gg))
		       (nz (z-dim gg))
		       (x-step (/ (x-size gg) (1- nx)))
		       (y-step (/ (y-size gg) (1- ny)))
		       (z-step (/ (z-size gg) (1- nz)))
		       ;; if gg already has a dose array present use it
		       (dose-array (or (grid (result src))
				       (make-array (list nx ny nz)
						   :element-type 'single-float
						   :initial-element 0.0))))
		  (do ((i 0 (1+ i))
		       (x (x-origin gg) (incf x x-step)))
		      ((= i nx))
		    (do ((j 0 (1+ j))
			 (y (y-origin gg) (incf y y-step)))
			((= j ny))
		      (do ((k 0 (1+ k))
			   (z (z-origin gg) (incf z z-step)))
			  ((= k nz))
			(setf (aref dose-array i j k) (line-dose x y z)))))
		  t) ;; return success for grid
	      nil))) ;; neither points nor grid, so return failure
      (progn (format t "~%*** zero length source - cannot compute dose!~%")
	     nil))))

;;;--------------------------------------

(defun compute-seed-dose (src pts gg)

  "compute-seed-dose src pts gg

Computes the doses to points or dose array (but NOT both) for seed src
and puts them in the results object of src.  Returns doses if
successful, nil otherwise."

  (let* ((src-table (source-data (source-type src)))
	 (gamma (* (dose-rate-const src-table) (anisotropy-fn src-table)))
	 (range (poly-range src-table))
	 (mu-w (mu-water src-table))
	 (a0 (a0 src-table))
	 (a1 (a1 src-table))
	 (a2 (a2 src-table))
	 (a3 (a3 src-table))
	 (a4 (a4 src-table))
	 (a5 (a5 src-table))
	 (src-loc (location src))
	 (xs (first src-loc))
	 (ys (second src-loc))
	 (zs (third src-loc)))
    (declare (single-float gamma range mu-w a0 a1 a2 a3 a4 a5 xs ys zs))
    (format t "~%Computing dose for source ~A...~%" (id src))
    (flet ((seed-dose (x y z)
	     (let* ((r2 (+ (sqr (- x xs)) (sqr (- y ys)) (sqr (- z zs))))
		    (r (sqrt r2)))
	       (declare (single-float r r2))
	       ;; for distances less than cutoff distance set dose rate
	       ;; to same as at cutoff distance
	       (if (< r *brachy-min-dist*)
		   (setq r *brachy-min-dist* r2 (* r r)))
	       (/ (* gamma (tisscorr r range mu-w a0 a1 a2 a3 a4 a5))
		  r2))))
      (if pts
	  (setf (points (result src)) ;; also returns success
	    (mapcar #'(lambda (pt)
			(seed-dose (x pt) (y pt) (z pt)))
		    pts))
	(if gg
	    (let* ((nx (x-dim gg))
		   (ny (y-dim gg))
		   (nz (z-dim gg))
		   (x-step (/ (x-size gg) (1- nx)))
		   (y-step (/ (y-size gg) (1- ny)))
		   (z-step (/ (z-size gg) (1- nz)))
		   (dose-array (or (grid (result src)) ; if present use it,
				   (make-array (list nx ny nz) ; or make one
					       :element-type 'single-float
					       :initial-element 0.0))))
	      (do ((i 0 (1+ i))
		   (x (x-origin gg) (incf x x-step)))
		  ((= i nx))
		(do ((j 0 (1+ j))
		     (y (y-origin gg) (incf y y-step)))
		    ((= j ny))
		  (do ((k 0 (1+ k))
		       (z (z-origin gg) (incf z z-step)))
		      ((= k nz))
		    (setf (aref dose-array i j k) (seed-dose x y z)))))
	      t) ;; return success for grid
	  nil))))) ;; neither pts or grid, so return failure

;;;--------------------------------------------------

(defun tisscorr (dist range mu-w a0 a1 a2 a3 a4 a5)

  (declare (single-float dist range mu-w a0 a1 a2 a3 a4 a5))
  (if (> dist range) (exp (- (* mu-w dist))) ;; exponential absorbtion
    (let* ((dist2 (* dist dist))
	   (dist3 (* dist dist2))) ;; Meisberger polynomial
      (declare (single-float dist2))
      (+ a0 (* a1 dist) (* a2 dist2) (* a3 dist3)
	 (* a4 dist2 dist2) (* a5 dist2 dist3)))))

;;;--------------------------------------------------
;;; End.
