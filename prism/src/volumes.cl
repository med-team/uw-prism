;;;
;;; volumes
;;;
;;; The classes and methods for volume objects, including anatomy,
;;; tumors and targets.
;;;
;;; 10-Aug-1992 I. Kalet created from old rtp-objects
;;;  7-Sep-1992 I. Kalet change some methods to :before to supplement
;;;  default methods, also move contour stuff to contours module
;;; 16-Sep-1992 I. Kalet name, new-name now in prism-objects
;;;  1-Mar-1993 I. Kalet remove make-easel definition - defined in
;;;  easel
;;; 31-Jul-1993 I. Kalet add new-contours and new-color events
;;;  3-Sep-1993 I. Kalet split draw methods to volume-graphics, and
;;;  mediator to volume-mediators
;;; 15-Oct-1993 I. Kalet remove unnecessary slot-type methods, add
;;; default initargs for tumors and targets
;;; 25-Oct-1993 I. Kalet add default initarg for density
;;; 22-Mar-1994 J. Unger enhance tumor def for PTVT.
;;; 28-Mar-1994 J. Unger add announcements when tumor attribs change.
;;; 30-Mar-1994 J. Unger misc mods & enhancements to tumor attribs.
;;;  2-Jun-1994 J. Unger add some announcements for setf obj
;;; attributes.
;;; 16-Jun-1994 I. Kalet change float to single-float, density can be
;;; nil, default target-type is "unspecified".
;;; 11-Sep-1995 I. Kalet DON'T SAVE new-m-stage - inadvertently
;;; omitted from not-saved method for tumors.
;;; 23-Jun-1997 I. Kalet add default initarg for tolerance dose.
;;; 19-Oct-1998 C. Wilcox changed the calculation of thickness
;;; for the physical volume calculation
;;; 25-Feb-1999 I. Kalet put find-center-vol here, moved from coll-panels
;;;  1-Apr-1999 I. Kalet add physical-volume, dose-histogram to
;;; not-saved method for pstruct
;;; 13-Aug-2002 J. Sager add 3d-display slot to pstruct and event 
;;; new-3d-display
;;; 13-Oct-2002 I. Kalet add mesh slot to pstruct to hold triangulated
;;; mesh generated from contours, remove new-3d-display event
;;; 30-Oct-2002 I. Kalet don't save 3d-display!
;;;  4-Aug-2005 E. Webster cumulative changes to improve OpenGL rendering
;;; 25-May-2009 I. Kalet remove room-view support
;;; 26-Jun-2009 I. Kalet and remove :3d-display default initarg in
;;; target class.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass pstruct (generic-prism-object)

  ((contours :initarg :contours
	     :accessor contours
	     :documentation "A list of contours representing the
surface of the volume.")

   (new-contours :type ev:event
		 :initform (ev:make-event)
		 :accessor new-contours
		 :documentation "Announced when a contour is added,
replaced or altered.  Must be done by external code, since the
contours are not a collection but a simple list.")

   (physical-volume :type single-float
		    :initarg :physical-volume
		    :reader physical-volume
		    :documentation "The total volume enclosed by the
surface defined by the contours.")

   (dose-histogram :initarg :dose-histogram
		   :accessor dose-histogram)

   (display-color :initarg :display-color
		  :accessor display-color)

   (new-color :type ev:event
	      :initform (ev:make-event)
	      :accessor new-color
	      :documentation "Announced by setf method when
display-color is updated.")

   (update-case :type ev:event
                :initform (ev:make-event)
                :accessor update-case
                :documentation "An event that gets announced 
whenever any pstruct attribute changes that justifies resetting the 
pstruct's containing case id and timestamp.")
   
   )

  (:default-initargs :name "" :contours nil :display-color 'sl:white)

  (:documentation "A pstruct is any kind of 3-d geometric structure
pertaining to the case, either an organ, with density to be used in
the dose computation, or an organ with no density, but whose dose
histogram should be known, or a target, whose dose should be
analyzed.")

  )

;;;--------------------------------------

(defmethod slot-type ((obj pstruct) slotname)

  (case slotname
	(contours :object-list)
	(otherwise :simple)))

;;;--------------------------------------

(defmethod not-saved ((obj pstruct))

  (append (call-next-method)
	  '(update-case new-color new-contours physical-volume
	    dose-histogram)))

;;;--------------------------------------

(defmethod (setf name) :after (nm (obj pstruct))

  (declare (ignore nm))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defmethod (setf display-color) :after (col (obj pstruct))

  (ev:announce obj (new-color obj) col))

;;;--------------------------------------

(defmethod physical-volume :before ((pstr pstruct))

  "Returns the physical volume of a pstruct, initially by computing
the area of each contour (polygon) and multiplying by the thickness of
each slice, then cached in the pstruct.  The pstruct must have
contours, and each is considered closed."

  ;; Method for computing the area of a polygon (contour): 
  ;; Sums area of each triangle determined by a fixed reference point
  ;; (in this case, the origin (0,0) and each side of polygon). Area K
  ;; of each triangle is computed the formula:
  ;;        K = | V1 x V2 | / 2.0
  ;; (see any text, for example Anton H, Elementary Linear Algebra ed.,
  ;; John Wiley and Sons 1981, p. 113.)   For the details and drawings
  ;; showing the point, V1 and V2, see the discussion of IRREG-style
  ;; scatter summation PLAN-32 User's Manual, Appendix A.  In fact,
  ;; this code is lifted right out of sector_sum.

;;  (unless (slot-boundp pstr 'physical-volume)
    (setf (slot-value pstr 'physical-volume)
      (let* ((conts (sort (copy-list (contours pstr))
			  #'(lambda (x y) (< (z x) (z y)))))
	     (zs (mapcar #'(lambda (x) (z x)) conts))
	     (prev-z (car zs))
	     (curr-z (car zs))
	     (next-z (car zs))
	     verts
	     first-vert
	     second-vert
	     (slab-thickness 0.0)
	     (cross 0.0)
	     (volume 0.0)
	     (area 0.0))
	(dolist (cont conts volume)
	  (setf prev-z curr-z)
	  (setf curr-z next-z)
	  (setf zs (cdr zs))
	  (when zs (setf next-z (car zs)))
	  (setf slab-thickness (/ (abs (- prev-z next-z)) 2))
	  (setf verts (vertices cont))
	  (setf first-vert (car verts))
	  (setf area 0.0)
	  (dolist (vert (append (cdr verts) (list first-vert))) 
	    (setf second-vert first-vert)
	    (setf first-vert vert)
	    ;; cross := end_2_y * end_1_x - end_2_x * end_1_y
	    (setf cross (- (* (cadr second-vert)
			      (car first-vert))
			   (* (car second-vert)
			      (cadr first-vert))))
	    (setf area (+ area cross)))
	  (setf volume (+ volume (* slab-thickness
				    (float (/ (abs area) 2.0)))))))))

;;;--------------------------------------

(defun bounding-box (vol)

  "bounding-box vol

Return the maximum and minimum cordinates of a pstruct vol."
  (let* ((clist (contours vol))
	 (pts (apply #'append (mapcar #'vertices clist)))
	 (x-list (mapcar #'first pts))
	 (y-list (mapcar #'second pts))
	 (max-x (apply #'max x-list))
	 (min-x (apply #'min x-list))
	 (max-y (apply #'max y-list))
	 (min-y (apply #'min y-list))
	 (z-list (mapcar #'z clist))
	 (max-z (apply #'max z-list))
	 (min-z (apply #'min z-list)))
    (list (list min-x min-y min-z)
	  (list max-x max-y max-z))))

;;;--------------------------------------

(defun find-center-vol (vol)

  "find-center-vol vol

Returns the center coordinates and maximum diameter of pstruct vol."

  (let* ((extremes (bounding-box vol))
	 (minpt (first extremes))
	 (maxpt (second extremes)))
    (values (list (* 0.5 (+ (first maxpt) (first minpt)))
		  (* 0.5 (+ (second maxpt) (second minpt)))
		  (* 0.5 (+ (third maxpt) (third minpt))))
	    (max (abs (- (first maxpt) (first minpt)))
		 (abs (- (second maxpt) (second minpt)))
		 (abs (- (third maxpt) (third minpt)))))))

;;;--------------------------------------

(defclass organ (pstruct)

  ((tolerance-dose :type single-float
		   :initarg :tolerance-dose
		   :accessor tolerance-dose
		   :documentation "The accepted value for radiation
tolerance for this organ type, in rads.")

   (density :initarg :density
	    :accessor density
	    :documentation "The density to be used in the dose
computation for inhomogeneity corrections.  It can be nil or a number,
so the type is not specified here.  If nil, the organ is not used in
the dose computation for inhomogeneity corrections.")

   (new-density :type ev:event
		:initform (ev:make-event)
		:accessor new-density
		:documentation "Announced when the density is
updated.")

   (organ-name :initarg :organ-name
	       :reader organ-name
	       :documentation "One of the known organ names.")

   )

  (:default-initargs :tolerance-dose 0.0 :density nil
		     :display-color 'sl:green)

  (:documentation "This class includes both organs that represent
inhomogeneities and organs for which there is a tolerance dose not to
be exceeded.  Some organs are of both types.")

  )

;;;--------------------------------------

(defmethod not-saved ((obj organ))

  (append (call-next-method)
	  '(new-density)))

;;;--------------------------------------

(defmethod (setf density) :after (den (obj organ))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-density obj) den))

;;;--------------------------------------

(defmethod (setf tolerance-dose) :after (tol (obj organ))

  (declare (ignore tol))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defun make-organ (org-name &rest initargs)

  (apply #'make-instance 'organ
	 :name (if (equal org-name "")
		   (format nil "~A" (gensym "ORGAN-"))
		 org-name)
	 initargs))

;;;--------------------------------------

(defclass tumor (pstruct)

  ((t-stage :type symbol
            :initarg :t-stage
	    :accessor t-stage
            :documentation "The tumor's t-stage - one of 't1, 't2,
't3, t4, or nil if unspecified.")

   (new-t-stage :type ev:event
                :initform (ev:make-event)
                :accessor new-t-stage
                :documentation "Announced when the tumor's t-stage
changes.")

   (m-stage :type symbol
            :initarg :m-stage
            :accessor m-stage
            :documentation "The tumor's m-stage.")

   (new-m-stage :type ev:event
                :initform (ev:make-event)
                :accessor new-m-stage
                :documentation "Announced when the tumor's m-stage
changes.")

   (n-stage :type symbol
            :initarg :n-stage
	    :accessor n-stage
            :documentation "The tumor's n-stage - one of 'n0, 'n1,
'n2, 'n3, or nil if unspecified.")

   (new-n-stage :type ev:event
                :initform (ev:make-event)
                :accessor new-n-stage
                :documentation "Announced when the tumor's n-stage
changes.")

   (cell-type :type symbol
              :initarg :cell-type
	      :accessor cell-type
              :documentation "One of a list of numerous cell types, or
nil if unspecified.")

   (new-cell-type :type ev:event
                  :initform (ev:make-event)
                  :accessor new-cell-type
                  :documentation "Announced when the tumor's cell-type
 changes.")

   (site :type symbol
         :initarg :site
	 :accessor site
	 :documentation "One of the known tumor sites, a symbol, as
determined by the anatomy tree.")

   (new-site :type ev:event
             :initform (ev:make-event)
             :accessor new-site
             :documentation "Announced when the tumor's site changes.")

   (region :type symbol
           :initarg :region
	   :accessor region
           :documentation "For lung tumors, a region of the lung.  Nil
if unspecified or for other tumor sites, or one of 'hilum, 'upper-lobe,
'lower-lobe, or 'mediastinum.")

   (new-region :type ev:event
               :initform (ev:make-event)
               :accessor new-region
               :documentation "Announced when the tumor's region
changes.")

   (side :type symbol
         :initarg :side
	 :accessor side
         :documentation "For lung tumors, the side of the lung that
the tumor is on.  Nil if unspecified or for other tumor sites, or one
of 'left or 'right.")

   (new-side :type ev:event
             :initform (ev:make-event)
             :accessor new-side
             :documentation "Announced when the tumor's side changes.")

   (fixed :type symbol
          :initarg :fixed
	  :accessor fixed
          :documentation "For lung tumors, an indication of whether
the tumor is fixed to the chest wall or not.  Nil if unspecified of
for other tumor sites, or one of 'yes or 'no.")

   (new-fixed :type ev:event
              :initform (ev:make-event)
              :accessor new-fixed
              :documentation "Announced when the tumor's fixed
attribute changes.")

   (pulm-risk :type symbol
              :initarg :pulm-risk
 	      :accessor pulm-risk
              :documentation "For lung tumors, the tumor's pulmonary 
risk.  Nil if unspecified or for other tumor sites, or one of 'high
or 'low.")

   (new-pulm-risk :type ev:event
                  :initform (ev:make-event)
                  :accessor new-pulm-risk
              :documentation "Announced when the tumor's pulmonary
risk changes.")

   (grade :initarg :grade
          :accessor grade
          :documentation "The tumor's grade")

   (new-grade :type ev:event
              :initform (ev:make-event)
              :accessor new-grade
              :documentation "Announced when the tumor's grade
changes.")

   )

  (:default-initargs :t-stage nil :n-stage nil :m-stage nil
		     :cell-type nil :site 'body :region nil
		     :side nil :fixed nil :pulm-risk nil
		     :grade nil :display-color 'sl:cyan)

  (:documentation "There may be more than one tumor volume for a
patient.")

  )

;;;--------------------------------------

(defmethod not-saved ((obj tumor))

  (append (call-next-method)
	  '(new-t-stage new-m-stage new-n-stage new-cell-type new-site
	    new-region new-side new-fixed new-pulm-risk new-grade)))

;;;--------------------------------------

(defmethod (setf t-stage) :after (t-stg (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-t-stage obj) t-stg))

;;;--------------------------------------

(defmethod (setf n-stage) :after (n-stg (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-n-stage obj) n-stg))

;;;--------------------------------------

(defmethod (setf cell-type) :after (new-ct (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-cell-type obj) new-ct))

;;;--------------------------------------

(defmethod (setf site) :after (new-s (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-site obj) new-s))

;;;--------------------------------------

(defmethod (setf region) :after (new-r (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-region obj) new-r))

;;;--------------------------------------

(defmethod (setf side) :after (new-s (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-side obj) new-s))

;;;--------------------------------------

(defmethod (setf fixed) :after (new-f (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-fixed obj) new-f))

;;;--------------------------------------

(defmethod (setf pulm-risk) :after (new-pr (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-pulm-risk obj) new-pr))

;;;--------------------------------------

(defmethod (setf grade) :after (new-gr (obj tumor))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-grade obj) new-gr))

;;;--------------------------------------

(defun make-tumor (tumor-name &rest initargs)

  (apply #'make-instance 'tumor
	 :name (if (equal tumor-name "")
		   (format nil "~A" (gensym "TUMOR-"))
		 tumor-name)
	 initargs))

;;;--------------------------------------

(defclass target (pstruct)

  ((site :initarg :site
	 :accessor site
	 :documentation "One of the known tumor sites")

   (required-dose :type single-float 
		  :initarg :required-dose
		  :accessor required-dose)

   (region :initarg :region
	   :accessor region)

   (target-type :initarg :target-type
		:accessor target-type
		:documentation "One of either initial or boost")

   (nodes :initarg :nodes
	  :accessor nodes
	  :documentation "Nodes to treat")

   (average-size :type single-float 
		 :initarg :average-size
		 :accessor average-size)

   (how-derived :initarg :how-derived
		:accessor how-derived)

   )

  (:default-initargs :site 'body :required-dose 0.0
		     :region nil :target-type "unspecified"
		     :how-derived "Manual"
		     :display-color 'sl:blue)

  (:documentation "There may be more than one target volume for a
patient, e.g., the boost volume and the large volume.  Also, the tumor
volume and the target volume are different.")

  )

;;;--------------------------------------

(defmethod (setf site) :after (new-s (obj target))

  (ev:announce obj (update-case obj))
  (ev:announce obj (new-site obj) new-s))

;;;--------------------------------------

(defmethod (setf required-dose) :after (new-dos (obj target))

  (declare (ignore new-dos))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defmethod (setf region) :after (new-reg (obj target))

  (declare (ignore new-reg))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defmethod (setf target-type) :after (new-type (obj target))

  (declare (ignore new-type))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defmethod (setf nodes) :after (new-nodes (obj target))

  (declare (ignore new-nodes))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defmethod (setf average-size) :after (new-size (obj target))

  (declare (ignore new-size))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defmethod (setf how-derived) :after (new-deriv (obj target))

  (declare (ignore new-deriv))
  (ev:announce obj (update-case obj)))

;;;--------------------------------------

(defun make-target (target-name &rest initargs)

  (apply #'make-instance 'target
	 :name (if (equal target-name "")
		   (format nil "~A" (gensym "TARGET-"))
		 target-name)
	 initargs))

;;;--------------------------------------
