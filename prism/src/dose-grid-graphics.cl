;;;
;;; dose-grid-graphics
;;;
;;; Draw methods for grid-geometries into views.
;;;
;;; 18-Oct-1993 J. Unger create from earlier prototype.
;;;  5-Nov-1993 J. Unger add draw methods for dose grid into views.
;;;  8-Apr-1994 I. Kalet split off from dose-graphics
;;; 18-Apr-1994 I. Kalet change refs to view origin
;;; 16-Jun-1994 I. Kalet change color in grid geom. to display-color
;;;  4-Sep-1995 I. Kalet change open coding to calls to pix-x, pix-y,
;;;  and add declarations
;;; 19-Sep-1996 I. Kalet remove &rest from draw methods
;;;  5-Dec-1996 I. Kalet don't generate primitives if color is invisible
;;; 03-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx .
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible.
;;; 25-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;; 25-Sep-2002 I. Kalet add (stub) support for room-view.
;;; 25-May-2009 I. Kalet remove support for room view.
;;;

(in-package :prism)

;;;---------------------------------------------

(defmethod draw ((dg grid-geometry) (v view))

  "draw (dg grid-geometry) (v view)

This is a no-op for views that are not explicitly specified in
draw methods elsewhere."

  )

;;;---------------------------------------------

(defun compute-grid-geometry-graphics (y-up g-xorig g-yorig g-xsize g-ysize
                                       g-xdim g-ydim v-xorig v-yorig v-ppcm)

  "compute-grid-geometry-graphics y-up g-xorig g-yorig g-xsize g-ysize
                                  g-xdim g-ydim v-xorig v-yorig v-ppcm

Computes a sequence of {x1 y1 x2 y2}* terms, suitable for inclusion
in a segments-prim, which represent the four corners of a grid-geometry
as it appears in an orthogonal view.  Parameters:

  y-up             t if yaxis points up in the view; nil otherwise
  g-xorig g-yorig  origin of grid geom as it appears in view
  g-xsize g-ysize  size of grid geom as it appears in view
  g-xdim  g-ydim   dimensions of specified grid
  v-xorig v-yorig  origin of patient space axes in view (in pixels)
  v-ppcm           scale of view"

  (let* ((g-dx (float (/ g-xsize (1- g-xdim))))
         (g-dy (float (/ g-ysize (1- g-ydim))))
         (g-xorig-t (pix-x g-xorig v-xorig v-ppcm))
         (g-yorig-t (if y-up (pix-y g-yorig v-yorig v-ppcm)
                      (pix-x g-yorig v-yorig v-ppcm)))
         (g-xsize-t (round (* g-xsize v-ppcm)))
         (g-ysize-t (round (* g-ysize v-ppcm)))
         (g-dx-t    (round (* g-dx v-ppcm)))
         (g-dy-t    (round (* g-dy v-ppcm)))
         (x-llc  g-xorig-t)
         (y-llc  g-yorig-t)
         (x-lrc  (+ g-xorig-t g-xsize-t))
         (y-lrc  g-yorig-t)
         (x-ulc  g-xorig-t)
         (y-ulc  (- g-yorig-t g-ysize-t))
         (x-urc  (+ g-xorig-t g-xsize-t))
         (y-urc  (- g-yorig-t g-ysize-t)))
    (declare (fixnum v-xorig v-yorig g-xorig-t g-yorig-t g-xsize-t
		     g-ysize-t g-dx-t g-dy-t x-llc y-llc x-lrc y-lrc
		     x-ulc y-ulc x-urc y-urc)
	     (single-float g-xorig g-yorig g-xsize g-ysize v-ppcm g-dx
			   g-dy))
    (list x-llc y-llc x-llc (- y-llc g-dy-t)
	  x-llc y-llc (+ x-llc g-dx-t) y-llc
	  x-lrc y-lrc x-lrc (- y-lrc g-dy-t)
	  x-lrc y-lrc (- x-lrc g-dx-t) y-lrc
	  x-ulc y-ulc x-ulc (+ y-ulc g-dy-t)
	  x-ulc y-ulc (+ x-ulc g-dx-t) y-ulc
	  x-urc y-urc x-urc (+ y-urc g-dy-t)
	  x-urc y-urc (- x-urc g-dx-t) y-urc)))

;;;---------------------------------------------

(defmethod draw ((dg grid-geometry) (v transverse-view))

  "draw (dg grid-geometry) (v transverse-view)

This method draws the dose grid into a transverse view."

  (if (eql (display-color dg) 'sl:invisible)
      (setf (foreground v) (remove dg (foreground v) :key #'object))
    (let ((prim (find dg (foreground v) :key #'object))
	  (color (sl:color-gc (display-color dg))))
      (unless prim 
	(setq prim (make-segments-prim nil color :object dg))
	(push prim (foreground v)))
      (setf (color prim) color
	    (points prim) nil)
      (when 
	  (and (poly:nearly-increasing 
		(z-origin dg) (view-position v)
		(+ (z-origin dg) (z-size dg)))
	       (plusp (x-size dg))
	       (plusp (y-size dg))
	       (plusp (z-size dg)))
	(setf (points prim)
	  (compute-grid-geometry-graphics
	   t
	   (x-origin dg) (y-origin dg) 
	   (x-size dg) (y-size dg) 
	   (x-dim dg) (y-dim dg) 
	   (x-origin v) (y-origin v)
	   (scale v)))))))

;;;---------------------------------------------

(defmethod draw ((dg grid-geometry) (v coronal-view))

  "draw (dg grid-geometry) (v coronal-view)

This method draws the dose grid into a coronal view."

  (if (eql (display-color dg) 'sl:invisible)
      (setf (foreground v) (remove dg (foreground v) :key #'object))
    (let ((prim (find dg (foreground v) :key #'object))
	  (color (sl:color-gc (display-color dg))))
      (unless prim 
	(setq prim (make-segments-prim nil color :object dg))
	(push prim (foreground v)))
      (setf (color prim) color
	    (points prim) nil)
      (when 
	  (and (poly:nearly-increasing 
		(y-origin dg) (view-position v)
		(+ (y-origin dg) (y-size dg)))
	       (plusp (x-size dg))
	       (plusp (y-size dg))
	       (plusp (z-size dg)))
	(setf (points prim)
	  (compute-grid-geometry-graphics
	   nil
	   (x-origin dg) (+ (z-origin dg) (z-size dg))
	   (x-size dg) (z-size dg) 
	   (x-dim dg) (z-dim dg) 
	   (x-origin v) (y-origin v)
	   (scale v)))))))

;;;---------------------------------------------

(defmethod draw ((dg grid-geometry) (v sagittal-view))

  "draw (dg grid-geometry) (v sagittal-view)

This method draws the dose grid into a sagittal view."

  (if (eql (display-color dg) 'sl:invisible)
      (setf (foreground v) (remove dg (foreground v) :key #'object))
    (let ((prim (find dg (foreground v) :key #'object))
	  (color (sl:color-gc (display-color dg))))
      (unless prim 
	(setq prim (make-segments-prim nil color :object dg))
	(push prim (foreground v)))
      (setf (color prim) color
	    (points prim) nil)
      (when 
	  (and (poly:nearly-increasing 
		(x-origin dg) (view-position v) (+ (x-origin dg) (x-size dg)))
	       (plusp (x-size dg))
	       (plusp (y-size dg))
	       (plusp (z-size dg)))
	(setf (points prim)
	  (compute-grid-geometry-graphics
	   t
	   (z-origin dg) (y-origin dg) 
	   (z-size dg) (y-size dg) 
	   (z-dim dg) (y-dim dg) 
	   (x-origin v) (y-origin v)
	   (scale v)))))))

;;;---------------------------------------------
;;; End.
