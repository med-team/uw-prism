;;;
;;; dose-surface-panels
;;;
;;; Implements the dose-surface-panel with the dose surface controls
;;;
;;;  9-Jun-1997 I. Kalet recreated for revised plan panel.
;;; 18-Jun-2000 I. Kalet make sliderbox initial upper limit adapt to
;;; max dose in dose array.
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass dose-surface-panel (generic-panel)

  ((fr :type sl:frame
       :accessor fr
       :documentation "The SLIK frame that contains the dose surface
panel.")

   (dose-surface :type dose-surface
                 :accessor dose-surface
                 :initarg :dose-surface
                 :documentation "The dose surface for this panel.")

   (del-pnl-btn :accessor del-pnl-btn
                :documentation "The delete panel button for this panel.")

   (thresh-sbox :accessor thresh-sbox
		:documentation "The sliderbox for modifying the dose
surface's threshold value.")

   (color-btn :accessor color-btn
               :documentation "The button for selecting the dose
surface color.")

   )

  (:documentation "The dose surface panel controls a single dose
surface, and in this incarnation it has a sliderbox as well as a color
button.")

  )

;;;---------------------------------------------

(defmethod initialize-instance :after ((dsp dose-surface-panel)
				       &rest initargs)

  "Initializes the user interface for the dose surface panel."

  (let* ((frm (apply #'sl:make-frame 300 130
		     :title "Prism DOSE SURFACE Panel" initargs))
         (frm-win (sl:window frm))
	 (max (if (valid-grid (result (dose-surface dsp)))
		  (let ((max-dose -1.0)
			(dose-arr (grid (result (dose-surface dsp)))))
		    (dotimes (i (array-dimension dose-arr 0))
		      (dotimes (j (array-dimension dose-arr 1))
			(dotimes (k (array-dimension dose-arr 2))
			  (when (< max-dose (aref dose-arr i j k))
			    (setq max-dose (aref dose-arr i j k))))))
		    (coerce (round (+ max-dose 100.0)) 'single-float))))
	 (del-pnl-b (apply #'sl:make-button 130 30
			   :parent frm-win
			   :ulc-x 10 :ulc-y 10
			   :label "Del Panel"
			   :button-type :momentary
                           initargs))
         (color-b (apply #'sl:make-button 130 30
			 :parent frm-win
			 :ulc-x 160 :ulc-y 10
			 :label "Surface color"
			 :button-type :momentary
			 initargs))
         (thresh-sb (apply #'sl:make-adjustable-sliderbox
			   270 30 0.0 (or max 9999.9) 99999.9
			   :parent frm-win
			   :setting (threshold (dose-surface dsp))
			   :ulc-x 10 :ulc-y 50
			   initargs)))
    (setf (fr dsp) frm
	  (del-pnl-btn dsp) del-pnl-b
	  (thresh-sbox dsp) thresh-sb
	  (color-btn dsp) color-b)
    (setf (sl:fg-color color-b) (display-color (dose-surface dsp)))
    (ev:add-notify dsp (sl:button-on del-pnl-b)
		   #'(lambda (dsp a)
		       (declare (ignore a))
		       (destroy dsp)))
    (ev:add-notify dsp (sl:value-changed thresh-sb)
		   #'(lambda (dsp bx new-val)
		       (declare (ignore bx))
		       (setf (threshold (dose-surface dsp)) new-val)))
    (ev:add-notify dsp (sl:button-on color-b)
		   #'(lambda (dsp bt)
		       (let ((new-col (sl:popup-color-menu)))
			 (when new-col
			   (setf (display-color (dose-surface dsp)) new-col)
			   (setf (sl:fg-color bt) new-col)))
		       (setf (sl:on bt) nil)))))

;;;---------------------------------------------

(defun make-dose-surface-panel (ds &rest initargs)

  "make-dose-surface-panel ds &rest initargs

Creates and returns a dose-surface panel for dose surface ds."

  (apply #'make-instance 'dose-surface-panel
	 :dose-surface ds initargs))

;;;---------------------------------------------

(defmethod destroy :before ((dsp dose-surface-panel))

  "Releases X resources used by this panel and its children."

  (sl:destroy (del-pnl-btn dsp))
  (sl:destroy (color-btn dsp))
  (sl:destroy (thresh-sbox dsp))
  (sl:destroy (fr dsp)))

;;;---------------------------------------------
;;; End.
