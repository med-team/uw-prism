;;;
;;; table-lookups
;;;
;;; This module contains code related to fast table lookup using mapping
;;; vectors (an idea first suggested by Steve Sutlief).  It is generic in
;;; that it is not specialized to a particular application in PRISM.
;;;
;;; 13-Mar-1998 BobGian created from code in therapy-machines and dose-info.
;;; 22-May-1998 BobGian rearrange order of defns to improve logical flow.
;;; 22-Jun-1998 BobGian bug-fix in BUILD-MAPPER - fencepost error in mapper
;;;   array allocation and initialization.
;;; 26-Jun-1998 BobGian modification to macro INTERPOLATE-DELTA; reimplement
;;;   mapping-vector table-lookups to handle fencepost cases correctly;
;;;   also fix bug in 2D-LOOKUP-INT (array indices swapped).
;;; 03-Jul-1998 BobGian fix indentation to fit within 80 cols; lowercase.
;;; 20-Jul-1998 BobGian optimize a few more array declarations and accessors.
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization, right margin).
;;; 08-Feb-2000 BobGian more cosmetic cleanup.
;;; 02-Mar-2000 BobGian case normalization in CONVERT-ARRAY.
;;; 30-May-2001 BobGian wrap generic arithmetic with THE-declared types
;;;   and add type decls to cause inlining of ROUND function.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 15-Mar-2003 BobGian add THE decls - allows TRUNCATE to inline.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 25-Jun-2004 BobGian - add documentation to clarify array types.
;;; 29-Jun-2004 BobGian: BUILD-MAPPER -> "therapy-machines.cl" (simplifies
;;;   dependencies).
;;;

(in-package :prism)

;;;=============================================================
;;; Macros used in Table-Lookup functions.

(defmacro 2d-aref (arr idx1 idx2)

  `(aref (the (simple-array single-float 1)
	   (svref (the (simple-array t 1) ,arr) (the fixnum ,idx1)))
	 (the fixnum ,idx2)))

;;;-------------------------------------------------------------

(defmacro 3d-aref (arr idx1 idx2 idx3)

  `(aref (the (simple-array single-float 1)
	   (svref (the (simple-array t 1)
		    (svref (the (simple-array t 1) ,arr) (the fixnum ,idx1)))
		  (the fixnum ,idx2)))
	 (the fixnum ,idx3)))

;;;-------------------------------------------------------------
;;; Linear Interpolation function.  Bi- or Tri-linear interpolation is done
;;; by using this function to interpolate the outputs from interpolating on
;;; other dimensions, as implemented in the xxx-LOOKUP functions above.
;;; New, memory-minimalizing version.

(defmacro interpolate-delta (input1 arg input2 value1 value2)

  ;; Interpolates between VALUE1 and VALUE2 according to fractional distance
  ;; ARG is between INPUT1 and INPUT2.  Must have INPUT1 < INPUT2 and
  ;; INPUT1 <= ARG.

  ;; INPUT1, ARG, and INPUT2 must be compile-time SYMBOLS to avoid
  ;; multiple evaluation.  They must also be declared SINGLE-FLOAT in the
  ;; containing form.  VALUE1 and VALUE2 are always compile-time FORMS,
  ;; but they are evaluated once only so no problem.

  `(/ (+ (* (the single-float ,value2)
	    (- (the single-float ,arg)
	       (the single-float ,input1)))
	 (* (the single-float ,value1)
	    (- (the single-float ,input2)
	       (the single-float ,arg))))
      (- (the single-float ,input2)
	 (the single-float ,input1))))

;;;-------------------------------------------------------------
;;; Slot 0 for input/output; 1,2 for mapper parameters.

(defmacro 1d-lookup (mapping-vector arg1 input1 mapper1 output)

  ;; MAPPING-VECTOR: Small-Float array
  ;; ARG1:           Small-Float number
  ;; INPUT1:         Small-Float array
  ;; MAPPER1:        Type-T array [fixnum contents]
  ;; OUTPUT:         Small-Float array [single-level table]
  ;;
  ;; Values in INPUT1 array must be monotonic increasing.
  ;; Ditto fixnum values in MAPPER1.

  `(progn
     (setf (aref (the (simple-array single-float (3)) ,mapping-vector) 0)
	   (the single-float ,arg1))
     (1d-lookup-int ,mapping-vector ,input1 ,mapper1 ,output)
     (aref (the (simple-array single-float (3)) ,mapping-vector) 0)))

;;;-------------------------------------------------------------
;;; Slots 0,1 for input/output; 2,3,4,5 for mapper parameters.

(defmacro 2d-lookup (mapping-vector arg1 arg2 input1 input2
		     mapper1 mapper2 output)

  ;; MAPPING-VECTOR: Small-Float array
  ;; ARG1:           Small-Float number
  ;; ARG2:           Small-Float number
  ;; INPUT1:         Small-Float array
  ;; INPUT2:         Small-Float array
  ;; MAPPER1:        Type-T array [fixnum contents]
  ;; MAPPER2:        Type-T array [fixnum contents]
  ;; OUTPUT:         Type-T array [double-level table]
  ;;
  ;; Values in INPUT1 and INPUT2 arrays must be monotonic increasing.
  ;; Ditto fixnum values in MAPPER1 and MAPPER2.

  `(progn
     (setf (aref (the (simple-array single-float (6)) ,mapping-vector) 0)
	   (the single-float ,arg1))
     (setf (aref (the (simple-array single-float (6)) ,mapping-vector) 1)
	   (the single-float ,arg2))
     (2d-lookup-int ,mapping-vector ,input1 ,input2 ,mapper1 ,mapper2 ,output)
     (aref (the (simple-array single-float (6)) ,mapping-vector) 0)))

;;;-------------------------------------------------------------
;;; Slots 0,1,2 for input/output; 3,4,5,6,7,8 for mapper parameters.

(defmacro 3d-lookup (mapping-vector arg1 arg2 arg3 input1 input2 input3
		     mapper1 mapper2 mapper3 output)

  ;; MAPPING-VECTOR: Small-Float array
  ;; ARG1:           Small-Float number
  ;; ARG2:           Small-Float number
  ;; ARG3:           Small-Float number
  ;; INPUT1:         Small-Float array
  ;; INPUT2:         Small-Float array
  ;; INPUT3:         Small-Float array
  ;; MAPPER1:        Type-T array [fixnum contents]
  ;; MAPPER2:        Type-T array [fixnum contents]
  ;; MAPPER3:        Type-T array [fixnum contents]
  ;; OUTPUT:         Type-T array [triple-level table]
  ;;
  ;; Values in INPUT1, INPUT2, and INPUT3 arrays must be monotonic increasing.
  ;; Ditto fixnum values in MAPPER1, MAPPER2, and MAPPER3.

  `(progn
     (setf (aref (the (simple-array single-float (9)) ,mapping-vector) 0)
	   (the single-float ,arg1))
     (setf (aref (the (simple-array single-float (9)) ,mapping-vector) 1)
	   (the single-float ,arg2))
     (setf (aref (the (simple-array single-float (9)) ,mapping-vector) 2)
	   (the single-float ,arg3))
     (3d-lookup-int ,mapping-vector ,input1 ,input2 ,input3
		    ,mapper1 ,mapper2 ,mapper3 ,output)
     (aref (the (simple-array single-float (9)) ,mapping-vector) 0)))

;;;=============================================================
;;; Functions for construction of mapping-vectors and associated tables.

(defun convert-array (in-array &aux (dim1 0) (dim2 0) (dim3 0)
		      (dims (array-dimensions in-array))
		      (dimnum (length dims)))

  (declare (type (simple-array t *) in-array)
	   (type fixnum dim1 dim2 dim3 dimnum))

  (cond ((= dimnum 1)
	 (setq dim1 (first dims))
	 (do ((arr1 (make-array dim1 :element-type 'single-float))
	      (val 0.0)
	      (idx1 0 (the fixnum (1+ idx1))))
	     ((= idx1 dim1)
	      arr1)
	   (declare (type (simple-array single-float 1) arr1)
		    (type fixnum idx1))
	   (setq val (svref (the (simple-array t 1) in-array) idx1))
	   (unless (typep val 'single-float)
	     (error "CONVERT-ARRAY [1] Bad data in input: ~S at ~D" val idx1))
	   (setf (aref arr1 idx1) (the single-float val))))

	((= dimnum 2)
	 (setq dim1 (first dims)
	       dim2 (second dims))
	 (do ((arr1 (make-array dim1 :element-type t))
	      (val 0.0)
	      (idx1 0 (the fixnum (1+ idx1))))
	     ((= idx1 dim1)
	      arr1)
	   (declare (type (simple-array t 1) arr1)
		    (type fixnum idx1))
	   (do ((arr2 (make-array dim2 :element-type 'single-float))
		(idx2 0 (the fixnum (1+ idx2))))
	       ((= idx2 dim2)
		(setf (svref arr1 idx1) arr2))
	     (declare (type (simple-array single-float 1) arr2)
		      (type fixnum idx2))
	     (setq val (aref (the (simple-array t 2) in-array) idx1 idx2))
	     (unless (typep val 'single-float)
	       (error "CONVERT-ARRAY [2] Bad data in input: ~S at ~D,~D"
		      val idx1 idx2))
	     (setf (aref arr2 idx2) (the single-float val)))))

	(t (setq dim1 (first dims)
		 dim2 (second dims)
		 dim3 (third dims))
	   (do ((arr1 (make-array dim1 :element-type t))
		(val 0.0)
		(idx1 0 (the fixnum (1+ idx1))))
	       ((= idx1 dim1)
		arr1)
	     (declare (type (simple-array t 1) arr1)
		      (type fixnum idx1))
	     (do ((arr2 (make-array dim2 :element-type t))
		  (idx2 0 (the fixnum (1+ idx2))))
		 ((= idx2 dim2)
		  (setf (svref arr1 idx1) arr2))
	       (declare (type (simple-array t 1) arr2)
			(type fixnum idx2))
	       (do ((arr3 (make-array dim3 :element-type 'single-float))
		    (idx3 0 (the fixnum (1+ idx3))))
		   ((= idx3 dim3)
		    (setf (svref arr2 idx2) arr3))
		 (declare (type (simple-array single-float 1) arr3)
			  (type fixnum idx3))
		 (setq val (aref (the (simple-array t 3) in-array)
				 idx1 idx2 idx3))
		 (unless (typep val 'single-float)
		   (error "CONVERT-ARRAY [3] Bad data in input: ~S at ~D,~D,~D"
			  val idx1 idx2 idx3))
		 (setf (aref arr3 idx3) (the single-float val))))))))

;;;=============================================================
;;; Fast table lookup using mapping-vectors.
;;; Slot 0 for input/output; 1,2 for mapper parameters.

(defun 1d-lookup-int (mapping-vector input1 mapper1 output
		      &aux (maxindex 0) (idx1- 0) (idx1= 0) (idx1+ 0)
		      (val1- 0.0) (val1= 0.0) (val1+ 0.0))

  ;; MAPPING-VECTOR: Small-Float array
  ;; INPUT1:         Small-Float array
  ;; MAPPER1:        Type-T array [fixnum contents]
  ;; OUTPUT:         Small-Float array [single-level table]
  ;;
  ;; Values in INPUT1 array must be monotonic increasing.
  ;; Ditto fixnum values in MAPPER1.

  (declare (type (simple-array single-float 1) input1 output)
	   (type (simple-array single-float (3)) mapping-vector)
	   (type (simple-array t 1) mapper1)
	   (type single-float val1- val1= val1+)
	   (type fixnum maxindex idx1- idx1= idx1+))

  (let ((arg1 (aref mapping-vector 0)))
    (declare (type single-float arg1))
    (setf (aref mapping-vector 0)
	  (cond
	    ((<= arg1 (the single-float (aref input1 0)))
	     (aref output 0))

	    ((>= arg1
		 (the single-float
		   (aref input1
			 (setq maxindex
			       (the fixnum (1- (array-total-size input1)))))))
	     (aref output maxindex))

	    (t (setq idx1=
		     (svref mapper1
			    (the fixnum
			      (round (the single-float
				       (* (the single-float
					    (aref mapping-vector 1))
					  (- arg1
					     (the single-float
					       (aref mapping-vector 2)))))))))

	       (cond ((= idx1= 0)
		      (setq idx1- 0
			    idx1+ 1
			    val1- (aref input1 0)
			    val1+ (aref input1 1)))

		     ((= idx1= maxindex)
		      (setq idx1- (the fixnum (1- maxindex))
			    idx1+ maxindex
			    val1- (aref input1 idx1-)
			    val1+ (aref input1 idx1+)))

		     (t (setq idx1- (the fixnum (1- idx1=))
			      idx1+ (the fixnum (1+ idx1=))
			      val1- (aref input1 idx1-)
			      val1= (aref input1 idx1=)
			      val1+ (aref input1 idx1+))
			(cond ((< arg1 val1-)
			       (error "1D-LOOKUP-INT [1]"))
			      ((= arg1 val1-)
			       (setq idx1+ idx1-))
			      ((< arg1 val1=)
			       (setq idx1+ idx1=
				     val1+ val1=))
			      ((= arg1 val1=)
			       (setq idx1- (setq idx1+ idx1=)))
			      ((< arg1 val1+)
			       (setq idx1- idx1=
				     val1- val1=))
			      ((= arg1 val1+)
			       (setq idx1- idx1+))
			      (t (error "1D-LOOKUP-INT [2]")))))

	       (cond ((= idx1- idx1+)
		      (aref output idx1-))
		     (t (interpolate-delta val1- arg1 val1+
					   (aref output idx1-)
					   (aref output idx1+))))))))

  nil)

;;;-------------------------------------------------------------
;;; Slots 0,1 for input/output; 2,3,4,5 for mapper parameters.

(defun 2d-lookup-int (mapping-vector input1 input2 mapper1 mapper2 output
		      &aux (maxindex 0) (idx1- 0) (idx1= 0) (idx1+ 0)
		      (idx2- 0) (idx2= 0) (idx2+ 0) (val1- 0.0) (val1= 0.0)
		      (val1+ 0.0) (val2- 0.0) (val2= 0.0) (val2+ 0.0))

  ;; MAPPING-VECTOR: Small-Float array
  ;; INPUT1:         Small-Float array
  ;; INPUT2:         Small-Float array
  ;; MAPPER1:        Type-T array [fixnum contents]
  ;; MAPPER2:        Type-T array [fixnum contents]
  ;; OUTPUT:         Type-T array [double-level table]
  ;;
  ;; Values in INPUT1 and INPUT2 arrays must be monotonic increasing.
  ;; Ditto fixnum values in MAPPER1 and MAPPER2.

  (declare (type (simple-array single-float 1) input1 input2)
	   (type (simple-array single-float (6)) mapping-vector)
	   (type (simple-array t 1) mapper1 mapper2 output)
	   (type single-float val1- val1= val1+ val2- val2= val2+)
	   (type fixnum maxindex idx1- idx1= idx1+ idx2- idx2= idx2+))

  (let ((arg1 (aref mapping-vector 0))
	(arg2 (aref mapping-vector 1)))
    (declare (type single-float arg1 arg2))

    (cond
      ((<= arg1 (the single-float (aref input1 0)))
       (setq idx1- (setq idx1+ 0)))

      ((>= arg1
	   (the single-float
	     (aref input1
		   (setq maxindex
			 (the fixnum (1- (array-total-size input1)))))))
       (setq idx1- (setq idx1+ maxindex)))

      (t (setq idx1= (svref mapper1
			    (the fixnum
			      (round (the single-float
				       (* (the single-float
					    (aref mapping-vector 2))
					  (- arg1
					     (the single-float
					       (aref mapping-vector 4)))))))))

	 (cond ((= idx1= 0)
		(setq idx1- 0
		      idx1+ 1
		      val1- (aref input1 0)
		      val1+ (aref input1 1)))

	       ((= idx1= maxindex)
		(setq idx1- (the fixnum (1- maxindex))
		      idx1+ maxindex
		      val1- (aref input1 idx1-)
		      val1+ (aref input1 idx1+)))

	       (t (setq idx1- (the fixnum (1- idx1=))
			idx1+ (the fixnum (1+ idx1=))
			val1- (aref input1 idx1-)
			val1= (aref input1 idx1=)
			val1+ (aref input1 idx1+))
		  (cond ((< arg1 val1-)
			 (error "2D-LOOKUP-INT [1]"))
			((= arg1 val1-)
			 (setq idx1+ idx1-))
			((< arg1 val1=)
			 (setq idx1+ idx1=
			       val1+ val1=))
			((= arg1 val1=)
			 (setq idx1- (setq idx1+ idx1=)))
			((< arg1 val1+)
			 (setq idx1- idx1=
			       val1- val1=))
			((= arg1 val1+)
			 (setq idx1- idx1+))
			(t (error "2D-LOOKUP-INT [2]")))))))

    (cond
      ((<= arg2 (the single-float (aref input2 0)))
       (setq idx2- (setq idx2+ 0)))

      ((>= arg2
	   (the single-float
	     (aref input2
		   (setq maxindex
			 (the fixnum (1- (array-total-size input2)))))))
       (setq idx2- (setq idx2+ maxindex)))

      (t (setq idx2= (svref mapper2
			    (the fixnum
			      (round (the single-float
				       (* (the single-float
					    (aref mapping-vector 3))
					  (- arg2
					     (the single-float
					       (aref mapping-vector 5)))))))))

	 (cond ((= idx2= 0)
		(setq idx2- 0
		      idx2+ 1
		      val2- (aref input2 0)
		      val2+ (aref input2 1)))

	       ((= idx2= maxindex)
		(setq idx2- (the fixnum (1- maxindex))
		      idx2+ maxindex
		      val2- (aref input2 idx2-)
		      val2+ (aref input2 idx2+)))

	       (t (setq idx2- (the fixnum (1- idx2=))
			idx2+ (the fixnum (1+ idx2=))
			val2- (aref input2 idx2-)
			val2= (aref input2 idx2=)
			val2+ (aref input2 idx2+))
		  (cond ((< arg2 val2-)
			 (error "2D-LOOKUP-INT [3]"))
			((= arg2 val2-)
			 (setq idx2+ idx2-))
			((< arg2 val2=)
			 (setq idx2+ idx2=
			       val2+ val2=))
			((= arg2 val2=)
			 (setq idx2- (setq idx2+ idx2=)))
			((< arg2 val2+)
			 (setq idx2- idx2=
			       val2- val2=))
			((= arg2 val2+)
			 (setq idx2- idx2+))
			(t (error "2D-LOOKUP-INT [4]")))))))

    (setf (aref mapping-vector 0)
	  (cond
	    ((and (= idx1- idx1+) (= idx2- idx2+))
	     (2d-aref output idx1+ idx2+))

	    ((= idx1- idx1+)
	     (let ((plane1 (svref output idx1+)))
	       (declare (type (simple-array single-float 1) plane1))
	       (interpolate-delta val2- arg2 val2+
				  (aref plane1 idx2-)
				  (aref plane1 idx2+))))

	    ((= idx2- idx2+)
	     (interpolate-delta val1- arg1 val1+
				(2d-aref output idx1- idx2+)
				(2d-aref output idx1+ idx2+)))

	    (t (let ((plane1- (svref output idx1-))
		     (plane1+ (svref output idx1+)))
		 (declare (type (simple-array single-float 1) plane1- plane1+))
		 (interpolate-delta
		   val2- arg2 val2+
		   (interpolate-delta val1- arg1 val1+
				      (aref plane1- idx2-)
				      (aref plane1+ idx2-))
		   (interpolate-delta val1- arg1 val1+
				      (aref plane1- idx2+)
				      (aref plane1+ idx2+))))))))

  nil)

;;;-------------------------------------------------------------
;;; Slots 0,1,2 for input/output; 3,4,5,6,7,8 for mapper parameters.

(defun 3d-lookup-int (mapping-vector input1 input2 input3 mapper1 mapper2
		      mapper3 output &aux (maxindex 0) (idx1- 0) (idx1= 0)
		      (idx1+ 0) (idx2- 0) (idx2= 0) (idx2+ 0) (idx3- 0)
		      (idx3= 0) (idx3+ 0) (val1- 0.0) (val1= 0.0) (val1+ 0.0)
		      (val2- 0.0) (val2= 0.0) (val2+ 0.0) (val3- 0.0)
		      (val3= 0.0) (val3+ 0.0))

  ;; MAPPING-VECTOR: Small-Float array
  ;; INPUT1:         Small-Float array
  ;; INPUT2:         Small-Float array
  ;; INPUT3:         Small-Float array
  ;; MAPPER1:        Type-T array [fixnum contents]
  ;; MAPPER2:        Type-T array [fixnum contents]
  ;; MAPPER3:        Type-T array [fixnum contents]
  ;; OUTPUT:         Type-T array [triple-level table]
  ;;
  ;; Values in INPUT1, INPUT2, and INPUT3 arrays must be monotonic increasing.
  ;; Ditto fixnum values in MAPPER1, MAPPER2, and MAPPER3.

  (declare (type (simple-array single-float 1) input1 input2 input3)
	   (type (simple-array single-float (9)) mapping-vector)
	   (type (simple-array t 1) mapper1 mapper2 mapper3 output)
	   (type single-float val1- val1= val1+ val2- val2= val2+
		 val3- val3= val3+)
	   (type fixnum maxindex idx1- idx1= idx1+ idx2- idx2= idx2+
		 idx3- idx3= idx3+))

  (let ((arg1 (aref mapping-vector 0))
	(arg2 (aref mapping-vector 1))
	(arg3 (aref mapping-vector 2)))
    (declare (type single-float arg1 arg2 arg3))

    (cond
      ((<= arg1 (the single-float (aref input1 0)))
       (setq idx1- (setq idx1+ 0)))

      ((>= arg1
	   (the single-float
	     (aref input1
		   (setq maxindex
			 (the fixnum (1- (array-total-size input1)))))))
       (setq idx1- (setq idx1+ maxindex)))

      (t (setq idx1= (svref mapper1
			    (the fixnum
			      (round (the single-float
				       (* (the single-float
					    (aref mapping-vector 3))
					  (- arg1
					     (the single-float
					       (aref mapping-vector 6)))))))))

	 (cond ((= idx1= 0)
		(setq idx1- 0
		      idx1+ 1
		      val1- (aref input1 0)
		      val1+ (aref input1 1)))
	       ;;
	       ((= idx1= maxindex)
		(setq idx1- (the fixnum (1- maxindex))
		      idx1+ maxindex
		      val1- (aref input1 idx1-)
		      val1+ (aref input1 idx1+)))
	       ;;
	       (t (setq idx1- (the fixnum (1- idx1=))
			idx1+ (the fixnum (1+ idx1=))
			val1- (aref input1 idx1-)
			val1= (aref input1 idx1=)
			val1+ (aref input1 idx1+))
		  (cond ((< arg1 val1-)
			 (error "3D-LOOKUP-INT [1]"))
			((= arg1 val1-)
			 (setq idx1+ idx1-))
			((< arg1 val1=)
			 (setq idx1+ idx1=
			       val1+ val1=))
			((= arg1 val1=)
			 (setq idx1- (setq idx1+ idx1=)))
			((< arg1 val1+)
			 (setq idx1- idx1=
			       val1- val1=))
			((= arg1 val1+)
			 (setq idx1- idx1+))
			(t (error "3D-LOOKUP-INT [2]")))))))

    (cond
      ((<= arg2 (the single-float (aref input2 0)))
       (setq idx2- (setq idx2+ 0)))

      ((>= arg2
	   (the single-float
	     (aref input2
		   (setq maxindex (the fixnum
				    (1- (array-total-size input2)))))))
       (setq idx2- (setq idx2+ maxindex)))

      (t (setq idx2= (svref mapper2
			    (the fixnum
			      (round (the single-float
				       (* (the single-float
					    (aref mapping-vector 4))
					  (- arg2
					     (the single-float
					       (aref mapping-vector 7)))))))))

	 (cond ((= idx2= 0)
		(setq idx2- 0
		      idx2+ 1
		      val2- (aref input2 0)
		      val2+ (aref input2 1)))

	       ((= idx2= maxindex)
		(setq idx2- (the fixnum (1- maxindex))
		      idx2+ maxindex
		      val2- (aref input2 idx2-)
		      val2+ (aref input2 idx2+)))

	       (t (setq idx2- (the fixnum (1- idx2=))
			idx2+ (the fixnum (1+ idx2=))
			val2- (aref input2 idx2-)
			val2= (aref input2 idx2=)
			val2+ (aref input2 idx2+))
		  (cond ((< arg2 val2-)
			 (error "3D-LOOKUP-INT [3]"))
			((= arg2 val2-)
			 (setq idx2+ idx2-))
			((< arg2 val2=)
			 (setq idx2+ idx2=
			       val2+ val2=))
			((= arg2 val2=)
			 (setq idx2- (setq idx2+ idx2=)))
			((< arg2 val2+)
			 (setq idx2- idx2=
			       val2- val2=))
			((= arg2 val2+)
			 (setq idx2- idx2+))
			(t (error "3D-LOOKUP-INT [4]")))))))

    (cond
      ((<= arg3 (the single-float (aref input3 0)))
       (setq idx3- (setq idx3+ 0)))

      ((>= arg3
	   (the single-float
	     (aref input3
		   (setq maxindex (the fixnum
				    (1- (array-total-size input3)))))))
       (setq idx3- (setq idx3+ maxindex)))

      (t (setq idx3= (svref mapper3
			    (the fixnum
			      (round (the single-float
				       (* (the single-float
					    (aref mapping-vector 5))
					  (- arg3
					     (the single-float
					       (aref mapping-vector 8)))))))))

	 (cond ((= idx3= 0)
		(setq idx3- 0
		      idx3+ 1
		      val3- (aref input3 0)
		      val3+ (aref input3 1)))

	       ((= idx3= maxindex)
		(setq idx3- (the fixnum (1- maxindex))
		      idx3+ maxindex
		      val3- (aref input3 idx3-)
		      val3+ (aref input3 idx3+)))

	       (t (setq idx3- (the fixnum (1- idx3=))
			idx3+ (the fixnum (1+ idx3=))
			val3- (aref input3 idx3-)
			val3= (aref input3 idx3=)
			val3+ (aref input3 idx3+))
		  (cond ((< arg3 val3-)
			 (error "3D-LOOKUP-INT [5]"))
			((= arg3 val3-)
			 (setq idx3+ idx3-))
			((< arg3 val3=)
			 (setq idx3+ idx3=
			       val3+ val3=))
			((= arg3 val3=)
			 (setq idx3- (setq idx3+ idx3=)))
			((< arg3 val3+)
			 (setq idx3- idx3=
			       val3- val3=))
			((= arg3 val3+)
			 (setq idx3- idx3+))
			(t (error "3D-LOOKUP-INT [6]")))))))

    (setf (aref mapping-vector 0)
	  (cond
	    ((and (= idx1- idx1+) (= idx2- idx2+) (= idx3- idx3+))
	     (3d-aref output idx1+ idx2+ idx3+))

	    ((and (= idx2- idx2+) (= idx3- idx3+))
	     (interpolate-delta val1- arg1 val1+
				(3d-aref output idx1- idx2+ idx3+)
				(3d-aref output idx1+ idx2+ idx3+)))

	    ((and (= idx1- idx1+) (= idx3- idx3+))
	     (let ((plane1 (svref output idx1+)))
	       (declare (type (simple-array t 1) plane1))
	       (interpolate-delta val2- arg2 val2+
				  (2d-aref plane1 idx2- idx3+)
				  (2d-aref plane1 idx2+ idx3+))))

	    ((and (= idx1- idx1+) (= idx2- idx2+))
	     (let ((plane2 (svref (the (simple-array t 1) (svref output idx1+))
				  idx2+)))
	       (declare (type (simple-array single-float 1) plane2))
	       (interpolate-delta val3- arg3 val3+
				  (aref plane2 idx3-)
				  (aref plane2 idx3+))))

	    ((= idx1- idx1+)
	     (let* ((plane1 (svref output idx1+))
		    (plane2- (svref plane1 idx2-))
		    (plane2+ (svref plane1 idx2+)))
	       (declare (type (simple-array t 1) plane1)
			(type (simple-array single-float 1) plane2- plane2+))
	       (interpolate-delta val3- arg3 val3+
				  (interpolate-delta val2- arg2 val2+
						     (aref plane2- idx3-)
						     (aref plane2+ idx3-))
				  (interpolate-delta val2- arg2 val2+
						     (aref plane2- idx3+)
						     (aref plane2+ idx3+)))))

	    ((= idx2- idx2+)
	     (let ((plane2-
		     (svref (the (simple-array t 1) (svref output idx1-))
			    idx2+))
		   (plane2+
		     (svref (the (simple-array t 1) (svref output idx1+))
			    idx2+)))
	       (declare (type (simple-array single-float 1) plane2- plane2+))
	       (interpolate-delta val3- arg3 val3+
				  (interpolate-delta val1- arg1 val1+
						     (aref plane2- idx3-)
						     (aref plane2+ idx3-))
				  (interpolate-delta val1- arg1 val1+
						     (aref plane2- idx3+)
						     (aref plane2+ idx3+)))))

	    ((= idx3- idx3+)
	     (let ((plane1- (svref output idx1-))
		   (plane1+ (svref output idx1+)))
	       (declare (type (simple-array t 1) plane1- plane1+))
	       (interpolate-delta
		 val2- arg2 val2+
		 (interpolate-delta val1- arg1 val1+
				    (2d-aref plane1- idx2- idx3+)
				    (2d-aref plane1+ idx2- idx3+))
		 (interpolate-delta val1- arg1 val1+
				    (2d-aref plane1- idx2+ idx3+)
				    (2d-aref plane1+ idx2+ idx3+)))))

	    (t (let ((plane1- (svref output idx1-))
		     (plane1+ (svref output idx1+)))
		 (declare (type (simple-array t 1) plane1- plane1+))
		 (let ((plane2-- (svref plane1- idx2-))
		       (plane2-+ (svref plane1- idx2+))
		       (plane2+- (svref plane1+ idx2-))
		       (plane2++ (svref plane1+ idx2+)))
		   (declare (type (simple-array single-float 1)
				  plane2-- plane2-+ plane2+- plane2++))
		   (interpolate-delta
		     val3- arg3 val3+
		     (interpolate-delta
		       val2- arg2 val2+
		       (interpolate-delta val1- arg1 val1+
					  (aref plane2-- idx3-)
					  (aref plane2+- idx3-))
		       (interpolate-delta val1- arg1 val1+
					  (aref plane2-+ idx3-)
					  (aref plane2++ idx3-)))
		     (interpolate-delta
		       val2- arg2 val2+
		       (interpolate-delta val1- arg1 val1+
					  (aref plane2-- idx3+)
					  (aref plane2+- idx3+))
		       (interpolate-delta val1- arg1 val1+
					  (aref plane2-+ idx3+)
					  (aref plane2++ idx3+))))))))))

  nil)

;;;=============================================================
;;; End.
