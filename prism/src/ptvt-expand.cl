;;;
;;;  ptvt-expand
;;; 
;;;  The ptvt volume expansion panel, used to get some additional info
;;;  from the user in order to generate a target from a tumor by using
;;;  a built-in version of the Planning Taret Volume Tool.
;;;
;;;  27-Apr-1994  J. Unger created.
;;;  04-May-1994  J. Unger split off linear expansion code to separate module.
;;;  31-May-1994  J. Unger update to current spec.
;;;  06-Jun-1994  J. Unger add some lung-specific preprocessing before target
;;;  generation (likely to be temporary - this should be handled by rules).
;;;  8-Jul-1994 J. Unger have only tumors w/ 2 or more contours in list.
;;; 13-Sep-2005 I. Kalet call new combined function target-volume, no
;;; separate functions for initial and boost.
;;;

(in-package :prism)

;;;---------------------------------------

(defparameter *ptv-offset* 10 "Distance between components of PTV editor.")
(defparameter *ptv-button-width* 175 "Width of a button on PTV editor")
(defparameter *ptv-button-height* 30 "Height of a button on PTV editor")
(defparameter *ptv-scroll-width* *ptv-button-width*
  "Width of the PTV editor scrolling lists.")
(defparameter *ptv-small-scroll-height* (* 2 *ptv-button-height*)
  "Height of the small PTV editor scrolling list.")
(defparameter *ptv-large-scroll-height* (* 6 *ptv-button-height*)
  "Height of the large PTV editor scrolling list.")
(defparameter *ptv-width* (+ (* 3 *ptv-offset*) 
                             (* 2 *ptv-scroll-width*))
  "Width of the PTV editor")
(defparameter *ptv-height* (+ (* 2 *ptv-offset*) 
                               *ptv-button-height*
                               *ptv-large-scroll-height*)
  "Height of the PTV editor")

;;;---------------------------------------

(defun make-ptv-expanded-target (immob-dev organs all-tumors)

  "MAKE-PTV-EXPANDED-TARGET immob-dev organs all-tumors

Returns a target instance whose contours are determined by automatic
generation using the Planning Target Volume Tool.  The patient's
immobilization device, list of organs, and list of tumors are supplied
to a special purpose panel.  Only the tumors that have at least two
contours are selected as candidates for target volume generation.  The
user selectes a tumor to use for target volume generation, a set of
critical organs, and a patient outline from the panel, which runs at a
nested event processing level."

  (sl:push-event-level)
  (let* ((frm (sl:make-frame *ptv-width* *ptv-height*
			     :title "PRISM PTV Expansion Editor"))
         (frm-win (sl:window frm))
         (accept-b (sl:make-exit-button 
		    *ptv-scroll-width* *ptv-button-height*
		    :parent frm-win 
		    :ulc-x *ptv-offset*
		    :ulc-y (+ (* 3 *ptv-offset*)
			      (* 2 *ptv-button-height*)
			      *ptv-small-scroll-height*)
		    :label "Accept"
		    :bg-color 'sl:blue))
         (tumor-r (sl:make-readout
		   *ptv-scroll-width* *ptv-button-height*
		   :parent frm-win
		   :ulc-x *ptv-offset*
		   :ulc-y *ptv-offset*
		   :label "Sel Tumor:"
		   :border-width 0))
         (crit-r (sl:make-readout
		  *ptv-scroll-width* *ptv-button-height*
		  :parent frm-win
		  :ulc-x  (+ (* 2 *ptv-offset*) *ptv-scroll-width*)
		  :ulc-y  *ptv-offset*
		  :label "Sel Crit Structs:"
		  :border-width 0))
         (tumor-s (sl:make-radio-scrolling-list 
		   *ptv-scroll-width* *ptv-small-scroll-height*
		   :parent frm-win
		   :ulc-x *ptv-offset*
		   :ulc-y (+ *ptv-offset* *ptv-button-height*)))
         (crit-s (sl:make-scrolling-list 
		  *ptv-scroll-width* *ptv-large-scroll-height*
		  :parent frm-win
		  :ulc-x (+ (* 2 *ptv-offset*) *ptv-scroll-width*)
		  :ulc-y (+ *ptv-offset* *ptv-button-height*)))
         (tumors (remove-if #'(lambda (tum)
                                (> 2 (length (contours tum))))
			    (coll:elements all-tumors)))
         (tumor-btns nil)
         (crit-btns nil)
         (tumor nil)
         (crit-structs nil)
	 )
    (dolist (item tumors)
      (let ((btn (sl:make-list-button tumor-s (name item))))
	(push btn tumor-btns)
	(sl:insert-button btn tumor-s)))
    (setq tumor-btns (reverse tumor-btns))
    (sl:select-button (first tumor-btns) tumor-s)
    (setq tumor (first tumors))
    (dolist (item (coll:elements organs))
      (let ((btn (sl:make-list-button crit-s (name item))))
	(push btn crit-btns)
	(sl:insert-button btn crit-s)))
    (setq crit-btns (reverse crit-btns))
    (sl:process-events)
    (setq tumor 
      (nth (position (find-if #'sl:on tumor-btns) tumor-btns) tumors))
    (dolist (btn crit-btns)
      (when (sl:on btn)
	(push (nth (position btn crit-btns) (coll:elements organs))
	      crit-structs)))
    (sl:destroy crit-s)
    (sl:destroy tumor-s)
    (sl:destroy crit-r)
    (sl:destroy tumor-r)
    (sl:destroy accept-b)
    (sl:destroy frm)
    (sl:pop-event-level)
    (sl:acknowledge
     (append 
      (list "Will generate a target from these parameters:  "
            ""
            (format nil "Immob dev: ~a" immob-dev)
            ""
            (format nil "Tumor name: ~a" (name tumor))
            (format nil "Tumor site: ~a" (site tumor))
            (format nil "Tumor t-stage ~a" (t-stage tumor))
            (format nil "Tumor n-stage ~a" (n-stage tumor))
            (format nil "Tumor cell-type ~a" (cell-type tumor))
            (format nil "Tumor region ~a" (region tumor))
            (format nil "Tumor side ~a" (side tumor))
            (format nil "Tumor fixed? ~a" (fixed tumor))
            (format nil "Tumor pulm risk: ~a" (pulm-risk tumor))
            ""
            "Critical structures:")
      (mapcar #'(lambda (cs) 
                  (format nil "   ~a" (name cs)))
	      crit-structs)))
    ;; the rule base expects lung cell types to be small-cell or
    ;; non-small-cell, so change cell-type if it isn't small-cell here.
    (when (and (equal (site tumor) 'lung)
	       (find (cell-type tumor) 
		     '(adenocarcinoma large-cell squamous-cell unclassified)))
      (setf (cell-type tumor) 'non-small-cell))
    (target-volume tumor immob-dev crit-structs)))

;;;---------------------------------------
;;; End.
