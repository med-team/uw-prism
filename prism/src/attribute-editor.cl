;;;
;;; attribute-editor
;;;
;;; The attribute-editor provides a facility for editing textual and 
;;; other non-graphical attributes of an organ, tumor, target, or
;;; other pstruct.  The base class and default constructor function
;;; are provided as well as specific classes and constructors for
;;; organs, tumors, and targets.
;;;
;;; 26-May-1993 J. Unger created.
;;; 16-Aug-1993 I. Kalet allow other keys in initialize-instance
;;; 18-Oct-1993 I. Kalet add code for tumors and targets, move name
;;; and color here
;;;  2-Dec-1993 I. Kalet change side to symbol
;;; 22-Mar-1994 J. Unger make tumor-attr-editor match ptvt specs.
;;; 27-May-1994 J. Unger change 'other to 'body in site list.
;;; 31-May-1994 I. Kalet make button width parameterized.
;;; 27-Jun-1994 I. Kalet add Density on/off button in organ editor
;;; 05-Jul-1994 J. Unger add remove-notify for new-density event of
;;; organ attrib editor to fix bug.
;;; 06-Jul-1994 J. Unger minor fix to init of use/ignore in comp
;;; button.
;;;  8-Jan-1995 I. Kalet initialize density to nil as specified in the
;;;  implementation report.  Destroy "use in comp" button in organ.
;;;  8-Oct-1996 I. Kalet make req. dose textline in target numeric.
;;; 24-Jun-1997 I. Kalet squeeze tumor editor vertically to fit the
;;; medium easel size, change global pars to local vars in let forms,
;;; don't initialize organ density or tolerance dose - they are now
;;; guaranteed to be bound, delete Name: label in name textline.
;;; 10-Mar-1998 I. Kalet coerce density,  and tolerance dose to single
;;; float on input.
;;; 12-Apr-2000 I. Kalet use smaller font everywhere.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 26-Apr-2004 I. Kalet add editor panel for points
;;; 17-May-2004 I. Kalet take out unnecessary local var in
;;; initialize-instance method of point editor.
;;; 25-Aug-2005 I. Kalet finish up point attribute editor
;;;

(in-package :prism)

;;;-----------------------------------

(defclass attribute-editor (generic-panel)

  ((width :type fixnum
	  :accessor width
	  :initarg :width
	  :documentation "The width in pixels to make the frame")

   (height :type fixnum
	   :accessor height
	   :initarg :height
	   :documentation "The height in pixels to make the frame")

   (button-width :type fixnum
		 :accessor button-width
		 :initarg :button-width
		 :documentation "The width in pixels of an attribute
editor button or textline.")

   (fr :type sl:frame
       :accessor fr
       :documentation "The SLIK frame that contains the attribute
editor.")

   (object :type pstruct
           :accessor object
           :initarg :object
           :documentation "The object to be edited by this attribute
editor.")

   (name-tln ;; :type sl:textline
             :accessor name-tln
	     :documentation "The SLIK textline displaying the name of
the pstruct being edited.")

   (color-btn ;; :type sl:button
	      :accessor color-btn
	      :documentation "The color button for the pstruct being
edited by the easel.")

   )

  (:default-initargs :width 150 :button-width 140 :height 75)

  (:documentation "An attribute editor provides a facility for editing
the textual and other non-graphical attributes of an object descended
from the pstruct class.")

  )

;;;-----------------------------------

(defmethod initialize-instance :after ((ae attribute-editor) 
				       &rest initargs)

  "Initializes the user interface for the basic attribute editor."

  (let* ((frm (apply #'sl:make-frame (width ae) (height ae)
		     :title "Prism Attribute Editor"
		     initargs))
         (frm-win (sl:window frm))
	 (obj (object ae))
	 (dx 5)
	 (bth 25)
	 (btw (- (width ae) (* 2 dx)))
	 (att-f (symbol-value *small-font*)) ;; the value, not the symbol
         (name-t (apply #'sl:make-textline btw bth
			:parent frm-win :font att-f
			:ulc-x dx :ulc-y dx
			initargs))
         (color-b (apply #'sl:make-button btw bth
			 :parent frm-win :font att-f
			 :ulc-x dx :ulc-y (bp-y dx bth 1)
			 :label "Color"
			 :button-type :momentary
			 initargs)))
    (setf (fr ae) frm
	  (name-tln ae) name-t
	  (color-btn ae) color-b
	  (button-width ae) btw
	  (sl:info name-t) (name obj))
    (ev:add-notify obj (sl:new-info name-t)
		   #'(lambda (ob tl new-info)
		       (declare (ignore tl))       
		       (setf (name ob) new-info)))
    (ev:add-notify obj (sl:button-on color-b)
		   #'(lambda (ob bt)
		       ;; maybe handle invisible differently ??
		       (setf (display-color ob)
			 (or (sl:popup-color-menu) (display-color ob)))
                       (setf (sl:fg-color bt) (display-color ob))
		       ;; popup-color-menu leaves it on
		       (setf (sl:on bt) nil)))
    (setf (sl:fg-color color-b) (display-color obj))))

;;;-----------------------------------

(defmethod make-attribute-editor ((pstr pstruct) &rest initargs)

  "make-attribute-editor ((pstr pstruct) &rest initargs

Returns the default attribute-editor with specified parameters."

  (apply #'make-instance 'attribute-editor
	 :object pstr :allow-other-keys t
	 initargs))

;;;-----------------------------------

(defmethod destroy :before ((ae attribute-editor))

  "Releases X resources used by this panel."

  (sl:destroy (color-btn ae))
  (sl:destroy (name-tln ae))
  (sl:destroy (fr ae)))

;;;-----------------------------------

(defclass organ-attribute-editor (attribute-editor)

  ((density-button :accessor density-button
		   :documentation "The button that turns on/off the
density attribute")

   (density-tln ;; :type sl:textline
		:accessor density-tln
		:documentation "The density textline.")

   (tol-dose-tln ;; :type sl:textline
                 :accessor tol-dose-tln
                 :documentation "The tolerance dose textline.")

   )

  (:default-initargs :height 155)

  (:documentation "The subclass of attribute editor specific to
organs.")

  )

;;;-----------------------------------

(defmethod initialize-instance :after ((ae organ-attribute-editor) 
				       &rest initargs)

  "Initializes the user interface for the organ attribute editor."

  (let* ((frm (fr ae))
         (frm-win (sl:window frm))
	 (obj (object ae))
	 (dx 5)
	 (bth 25)
	 (btw (button-width ae))
	 (att-f (symbol-value *small-font*)) ;; the value, not the symbol
	 (den-btn (apply #'sl:make-button btw bth
			 :parent frm-win :font att-f
			 :ulc-x dx :ulc-y (bp-y dx bth 2)
			 :label (if (density obj) 
				    "Use in comp." 
                                  "Ignore in comp.")
			 :button-type :momentary
			 initargs))
         (den-t (apply #'sl:make-textline btw bth
		       :parent frm-win :font att-f
		       :ulc-x dx :ulc-y (bp-y dx bth 3)
		       :label "Den: "
		       :numeric t :lower-limit 0.0 :upper-limit 20.0
		       :info (if (density obj)
				 (write-to-string (density obj))
			       "None")
		       initargs))
         (tol-t (apply #'sl:make-textline btw bth
		       :parent frm-win :font att-f
		       :ulc-x dx :ulc-y (bp-y dx bth 4)
		       :label "Tol: "
		       :numeric t :lower-limit 0.0 :upper-limit 10000.0
		       :info (write-to-string (tolerance-dose obj))
		       initargs)))
    (setf (density-button ae) den-btn
	  (density-tln ae) den-t
	  (tol-dose-tln ae) tol-t)
    (ev:add-notify ae (sl:button-on den-btn)
		   #'(lambda (aed btn)
		       (if (density (object aed))
			   (progn
			     (setf (density (object aed)) nil)
			     (setf (sl:label btn) "Ignore in comp."))
			 (progn
			   (setf (density (object aed)) 1.0)
			   (setf (sl:label btn) "Use in comp.")))))
    (ev:add-notify ae (sl:new-info tol-t)
		   #'(lambda (aed a new-info)
		       (declare (ignore a))
		       (setf (tolerance-dose (object aed))
			 (coerce (read-from-string new-info)
				 'single-float))))
    (ev:add-notify ae (sl:new-info den-t)
		   #'(lambda (aed tl new-info)
		       (if (density (object aed))
			   (setf (density (object aed))
			     (coerce (read-from-string new-info)
				     'single-float))
			 (setf (sl:info tl) "None"))))
    (ev:add-notify ae (new-density obj)
		   #'(lambda (aed org new-den)
		       (declare (ignore org))
		       (setf (sl:info (density-tln aed))
			 (if new-den new-den "None"))))))

;;;-----------------------------------

(defmethod make-attribute-editor ((org organ) &rest initargs)

  "make-attribute-editor (org organ) &rest initargs

Returns an organ-specific attribute-editor with specified parameters."

  (apply #'make-instance 'organ-attribute-editor
	 :object org :allow-other-keys t
	 initargs))

;;;-----------------------------------

(defmethod destroy :before ((ae organ-attribute-editor))

  "Releases additional X resources used by this panel."

  (ev:remove-notify ae (new-density (object ae)))
  (sl:destroy (density-button ae))
  (sl:destroy (density-tln ae))
  (sl:destroy (tol-dose-tln ae)))

;;;-----------------------------------

(defclass tumor-attribute-editor (attribute-editor)

  ((site-btn :accessor site-btn
             :documentation "The site button.")

   (t-stage-btn :accessor t-stage-btn
	        :documentation "The T-stage button.")

   (n-stage-btn :accessor n-stage-btn
	        :documentation "The N-stage button.")

   (cell-type-btn :accessor cell-type-btn
		  :documentation "The cell type button.")

   (region-btn :accessor region-btn
               :documentation "The region button.")

   (side-btn :accessor side-btn
	     :documentation "The side button.")

   (fixed-btn :accessor fixed-btn
	      :documentation "The fixed? button.")

   (pulm-risk-btn :accessor pulm-risk-btn
	          :documentation "The pulmonary risk button.")

   )

  (:default-initargs :height 305)

  (:documentation "The subclass of attribute editor specific to
tumors.")

  )

;;;-----------------------------------

(defmethod initialize-instance :after ((ae tumor-attribute-editor) 
				       &rest initargs)

  "Initializes the user interface for the tumor attribute editor."

  (let* ((frm (fr ae))
         (frm-win (sl:window frm))
	 (obj (object ae))
	 (dx 5)
	 (bth 25)
	 (att-f (symbol-value *small-font*)) ;; the value, not the symbol
	 (btw (button-width ae))
	 (site-b (apply #'sl:make-button btw bth
			:parent frm-win :font att-f
			:ulc-x dx :ulc-y (bp-y dx bth 2)
			:label (format nil "Site: ~a" (site obj))
			initargs))
	 (t-stage-b (apply #'sl:make-button btw bth
			   :parent frm-win :font att-f
			   :ulc-x dx :ulc-y (bp-y dx bth 3)
			   :label (format nil "T-Stage: ~a"
					  (t-stage obj))
			   initargs))
         (n-stage-b (apply #'sl:make-button btw bth
			   :parent frm-win :font att-f
			   :ulc-x dx :ulc-y (bp-y dx bth 4)
			   :label (format nil "N-Stage ~a"
					  (n-stage obj))
			   initargs))
         (cell-b (apply #'sl:make-button btw bth
			:parent frm-win :font att-f
			:ulc-x dx :ulc-y (bp-y dx bth 5)
			:label (format nil "Cell type: ~a"
				       (cell-type obj))
			initargs))
         (region-b (apply #'sl:make-button btw bth
			  :parent frm-win :font att-f
			  :ulc-x dx :ulc-y (bp-y dx bth 6)
			  :label (format nil "Region: ~a"
					 (region obj))
			  initargs))
         (side-b (apply #'sl:make-button btw bth
			:parent frm-win :font att-f
			:ulc-x dx :ulc-y (bp-y dx bth 7)
			:label (format nil "Side: ~a"
				       (side obj))
			initargs))
         (fixed-b (apply #'sl:make-button btw bth
			 :parent frm-win :font att-f
			 :ulc-x dx :ulc-y (bp-y dx bth 8)
			 :label (format nil "Fixed?: ~a"
					(fixed obj))
			 initargs))
         (pulm-b (apply #'sl:make-button btw bth
			:parent frm-win :font att-f
			:ulc-x dx :ulc-y (bp-y dx bth 9)
			:label (format nil "Pulm Risk: ~a"
				       (pulm-risk obj))
			initargs)))
    (setf (site-btn ae) site-b
	  (t-stage-btn ae) t-stage-b
	  (n-stage-btn ae) n-stage-b
	  (cell-type-btn ae) cell-b
	  (region-btn ae) region-b
	  (side-btn ae) side-b
          (fixed-btn ae) fixed-b
          (pulm-risk-btn ae) pulm-b)
    (ev:add-notify obj (sl:button-on site-b) 
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu '("Lung"
							 "Nasopharynx"
							 "Body"))))
			 (when selection
			   (setf (site ob) (case selection
					     (0 'lung)
					     (1 'nasopharynx)
					     (2 'body)))
			   (setf (sl:label bt)
			     (format nil "Site: ~a" (site ob))))
			 (setf (sl:on bt) nil)))) ;; popup-menu leaves it on
    (ev:add-notify obj (sl:button-on t-stage-b)
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu
					 '("T1" "T2" "T3" "T4"))))
			 (when selection
			   (setf (t-stage ob)
			     (case selection (0 'T1) (1 'T2) (2 'T3) (3 'T4)))
			   (setf (sl:label bt)
			     (format nil "T-Stage: ~a" (t-stage ob))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify obj (sl:button-on n-stage-b)
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu
					 '("N0" "N1" "N2" "N3"))))
			 (when selection
			   (setf (n-stage ob)
			     (case selection (0 'N0) (1 'N1) (2 'N2) (3 'N3)))
			   (setf (sl:label bt)
			     (format nil "N-Stage: ~a" (n-stage ob))))
			 (setf (sl:on bt) nil))))

    ;; Currently, grade not needed by ptvt.  May bring back later, though.
    ;;
    ;; (ev:add-notify obj (sl:button-on grade-b)
    ;;		   #'(lambda (ob bt)
    ;;		       (let ((selection (sl:popup-menu '("Grade I"
    ;;							 "Grade II"
    ;;							 "Grade III"
    ;;							 "Grade IV"))))
    ;;			 (when selection
    ;;			   (setf (grade ob) (case selection
    ;;					      (0 'I) (1 'II)
    ;;					      (2 'III) (3 'IV)))
    ;;			   (setf (sl:label bt)
    ;;			     (format nil "Grade ~A" (grade ob))))
    ;;			 ;; popup-menu leaves it on
    ;;			 (setf (sl:on bt) nil))))

    (ev:add-notify obj (sl:button-on cell-b)
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu 
					 '("Squamous Cell"
					   "Lymphoepithelioma"
					   "Small Cell"
					   "Large Cell"
					   "Adenocarcinoma"
					   "Unclassified"))))
			 (when selection
			   (setf (cell-type ob) (case selection 
						  (0 'squamous-cell)
						  (1 'lymphoepithelioma)
						  (2 'small-cell)
						  (3 'large-cell)
						  (4 'adenocarcinoma)
						  (5 'unclassified)))
			   (setf (sl:label bt)
			     (format nil "Cell type: ~a"
				     (cell-type ob))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify obj (sl:button-on region-b) 
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu '("Hilum"
							 "Upper Lobe" 
							 "Lower Lobe"
							 "Mediastinum"))))
			 (when selection
			   (setf (region ob) (case selection
					       (0 'hilum)
					       (1 'upper-lobe)
					       (2 'lower-lobe)
					       (3 'mediastinum)))
			   (setf (sl:label bt)
			     (format nil "Region: ~a" (region ob))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify obj (sl:button-on side-b)
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu '("Left" "Right"))))
			 (when selection
			   (setf (side ob) (case selection
					     (0 'left) (1 'right)))
			   (setf (sl:label bt) 
			     (format nil "Side: ~a" (side ob))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify obj (sl:button-on fixed-b)
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu '("Yes" "No"))))
			 (when selection
			   (setf (fixed ob) (case selection
					      (0 'yes) (1 'no)))
			   (setf (sl:label bt)
			     (format nil "Fixed?: ~a" (fixed ob))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify obj (sl:button-on pulm-b)
		   #'(lambda (ob bt)
		       (let ((selection (sl:popup-menu '("High" "Low"))))
			 (when selection
			   (setf (pulm-risk ob) (case selection
						  (0 'high) (1 'low)))
			   (setf (sl:label bt)
			     (format nil "Pulm Risk: ~a"
				     (pulm-risk ob))))
			 (setf (sl:on bt) nil))))))

;;;-----------------------------------

(defmethod make-attribute-editor ((tum tumor) &rest initargs)

  "make-attribute-editor (tum tumor) &rest initargs

Returns a tumor attribute-editor with specified parameters."

  (apply #'make-instance 'tumor-attribute-editor
	 :object tum :allow-other-keys t
	 initargs))

;;;-----------------------------------

(defmethod destroy :before ((ae tumor-attribute-editor))

  "Releases additional X resources used by this panel."

  (sl:destroy (site-btn ae))
  (sl:destroy (t-stage-btn ae))
  (sl:destroy (n-stage-btn ae))
  (sl:destroy (cell-type-btn ae))
  (sl:destroy (region-btn ae))
  (sl:destroy (side-btn ae))
  (sl:destroy (fixed-btn ae))
  (sl:destroy (pulm-risk-btn ae)))

;;;-----------------------------------

(defclass target-attribute-editor (attribute-editor)

  ((site-btn :accessor site-btn
             :documentation "The site button.")

   (req-dose-tln :accessor req-dose-tln
                 :documentation "The required dose textline.")

   (region-btn :accessor region-btn
               :documentation "The region button.")

   (target-type-btn :accessor target-type-btn
		    :documentation "The target type button.")

   (nodes-btn :accessor nodes-btn
              :documentation "The nodes button.")

   )

  (:default-initargs :height 215)

  (:documentation "The subclass of attribute editor specific to
targets.")

  )

;;;-----------------------------------

(defun not-impl (obj btn)

  (declare (ignore obj))
  (sl:acknowledge "Feature not implemented")
  (setf (sl:on btn) nil))

;;;-----------------------------------

(defmethod initialize-instance :after ((ae target-attribute-editor) 
				       &rest initargs)

  "Initializes the user interface for the target attribute editor."

  (let* ((frm (fr ae))
         (frm-win (sl:window frm))
	 (obj (object ae))
	 (dx 5)
	 (bth 25)
	 (btw (button-width ae))
	 (att-f (symbol-value *small-font*)) ;; the value, not the symbol
         (site-b (apply #'sl:make-button btw bth
			:parent frm-win :font att-f
			:ulc-x dx :ulc-y (bp-y dx bth 2)
			:label "Site"
			initargs))
         (req-dose-t (apply #'sl:make-textline btw bth
			    :parent frm-win :font att-f
			    :ulc-x dx :ulc-y (bp-y dx bth 3)
			    :label "PD: "
			    :numeric t
			    :lower-limit 0.0 :upper-limit 20000.0
			    :info (write-to-string (required-dose obj))
			    initargs))
         (region-b (apply #'sl:make-button btw bth
			  :parent frm-win :font att-f
			  :ulc-x dx :ulc-y (bp-y dx bth 4)
			  :label "Region"
			  initargs))
         (targ-type-b (apply #'sl:make-button btw bth
			     :parent frm-win :font att-f
			     :ulc-x dx :ulc-y (bp-y dx bth 5)
			     :label (if (target-type obj)
					(target-type obj)
				      "Targ. type")
			     initargs))
         (nodes-b (apply #'sl:make-button btw bth
			 :parent frm-win :font att-f
			 :ulc-x dx :ulc-y (bp-y dx bth 6)
			 :label "Nodes"
			 initargs)))
    (setf (site-btn ae) site-b
	  (req-dose-tln ae) req-dose-t
	  (region-btn ae) region-b
	  (target-type-btn ae) targ-type-b
	  (nodes-btn ae) nodes-b)
    (ev:add-notify obj (sl:button-on site-b) #'not-impl)
    (ev:add-notify obj (sl:new-info req-dose-t)
		   #'(lambda (ob tln dose)
		       (declare (ignore tln))
		       (setf (required-dose ob)
			 (coerce (read-from-string dose)
				 'single-float))))
    (ev:add-notify obj (sl:button-on region-b) #'not-impl)
    (ev:add-notify obj (sl:button-on targ-type-b)
		   #'(lambda (ob btn)
		       (let ((selection (sl:popup-menu '("Initial"
							 "Boost"))))
			 (when selection
			   (setf (target-type ob) (case selection
						    (0 "Initial")
						    (1 "Boost")))
			   (setf (sl:label btn) (target-type ob)))
			 (setf (sl:on btn) nil))))
    (ev:add-notify obj (sl:button-on nodes-b) #'not-impl)))

;;;-----------------------------------

(defmethod make-attribute-editor ((targ target) &rest initargs)

  "make-attribute-editor (targ target) &rest initargs

Returns a target attribute-editor with specified parameters."

  (apply #'make-instance 'target-attribute-editor
	 :object targ :allow-other-keys t
	 initargs))

;;;-----------------------------------

(defmethod destroy :before ((ae target-attribute-editor))

  "Releases additional X resources used by this panel."

  (sl:destroy (site-btn ae))
  (sl:destroy (req-dose-tln ae))
  (sl:destroy (region-btn ae))
  (sl:destroy (target-type-btn ae))
  (sl:destroy (nodes-btn ae)))

;;;-----------------------------------

(defclass point-attribute-editor (attribute-editor)

  ((id-rdt :accessor id-rdt
	   :documentation "The ID readout")

   (x-tln :accessor x-tln
	  :documentation "The X coordinate textline.")

   (y-tln :accessor y-tln
	  :documentation "The Y coordinate textline.")

   )

  (:default-initargs :height 155)

  (:documentation "The subclass of attribute editor specific to
points.")

  )

;;;-----------------------------------

(defmethod initialize-instance :after ((ae point-attribute-editor) 
				       &rest initargs)

  "Initializes the user interface for the point attribute editor,
allows the user to interactively modify the point vertex pv's
attributes, or to displace the mark by a fixed amount in the x & y
directions."

  (let* ((frm (fr ae))
         (frm-win (sl:window frm))
	 (obj (object ae))
	 (dx 5)
	 (tlw (- (width ae) (* 2 dx)))
	 (tlh 25)
         (x-loc (fix-float (x obj) 2))
         (y-loc (fix-float (y obj) 2))
         (num-rdt (sl:make-readout tlw tlh :parent frm-win
				   :label "ID: "
				   :ulc-x dx
				   :ulc-y (bp-y dx tlh 2)))
	 (x-tln (sl:make-textline tlw tlh :parent frm-win
				  :label "X loc: "
				  :numeric t
				  :lower-limit -999.9
				  :upper-limit 999.9
				  :ulc-x dx
				  :ulc-y (bp-y dx tlh 3)))
         (y-tln (sl:make-textline tlw tlh :parent frm-win
				  :label "Y loc: "
				  :numeric t
				  :lower-limit -999.9
				  :upper-limit 999.9
				  :ulc-x dx
				  :ulc-y (bp-y dx tlh 4)))
	 )
    (setf (sl:info num-rdt) (id obj))
    (setf (sl:info x-tln) x-loc)
    (setf (sl:info y-tln) y-loc)
    (setf (id-rdt ae) num-rdt
	  (x-tln ae) x-tln
	  (y-tln ae) y-tln)
    (ev:add-notify ae (sl:new-info x-tln)
		   #'(lambda (aed tln info)
		       (declare (ignore tln))
		       (setf (x (object aed))
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify ae (sl:new-info y-tln)
		   #'(lambda (aed tln info)
		       (declare (ignore tln))
		       (setf (y (object aed))
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify ae (new-loc obj)
		   #'(lambda (aed pt loc)
		       (declare (ignore pt))
		       (setf (sl:info (x-tln aed)) (first loc)
			     (sl:info (y-tln aed)) (second loc))))
    ))

;;;-----------------------------------

(defmethod make-attribute-editor ((pt mark) &rest initargs)

  "make-attribute-editor (pt mark) &rest initargs

Returns a point attribute-editor with specified parameters."

  (apply #'make-instance 'point-attribute-editor
	 :object pt :allow-other-keys t
	 initargs))

;;;-----------------------------------

(defmethod destroy :before ((ae point-attribute-editor))

  "Releases additional X resources used by this panel."

  (sl:destroy (x-tln ae))
  (sl:destroy (y-tln ae))
  (sl:destroy (id-rdt ae))
  ;; remove-notify for above add-notify on point since it persists
  (ev:remove-notify ae (new-loc (object ae))))

;;;-----------------------------------
;;; End.
