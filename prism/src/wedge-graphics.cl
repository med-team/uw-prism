;;;
;;; wedge-graphics
;;;
;;; this module contains the draw methods for wedges.
;;;
;;; 20-Sep-1996 I. Kalet created from beam-graphics.
;;;  1-Mar-1997 I. Kalet update calls to NEARLY- functions
;;; 26-Jun-1997 BobGian specialize CLIP to CLIP-FIXNUM in KEEP-IN-VIEW.
;;; 03-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx .
;;; 12-Aug-1997 BobGian CLIP-FIXNUM -> CLIP in KEEP-IN-VIEW (no more
;;;   need for separate versions of clipping code specialized on data type).
;;; 25-Aug-1997 BobGian cosmetics in DRAW-WEDGE [abolishes compiler
;;; warning].
;;; 20-Jan-1998 I. Kalet beam transforms now array, not multiple
;;; values, also add some declarations, eliminate some local vars.
;;;

(in-package :prism)

;;;----------------------------------------------

(defconstant *wedge-base-length* 2.0 "The length, model space, of the
base of the triangle depicting the wedge in views.")

(defconstant *wedge-height* 5.0 "The height, in model space, of the
wedge triangle as it would appear in a view if the vector from wedge
heel to wedge toe were parallel to the plane of the view.")

;;;----------------------------------------------

(defun draw-wedge (prim bt sad w-rot scale x-origin y-origin width height)

  "DRAW-WEDGE prim bt sad w-rot scale x-origin y-origin width height

Draws an icon representing a wedge into graphic primitive prim, based
upon beam transform bt, source to axis distance sad, wedge-rotation
w-rot, and the provided view plane scale, x-origin, and y-origin.  The
width and height parameters are the width and height of the picture
into which the wedge is to be drawn."

  ;; We transform two points from collimator space to view space - 
  ;; a point at the toe of the wedge, a point in the center of the 
  ;; of the wedge.  These points define the wedge directional vector.

  (declare (single-float sad w-rot)
	   (type (simple-array single-float (12)) bt))
  (let* ((x-vec (cond ((= w-rot 90.0) (- *wedge-height*))
		      ((= w-rot 270.0) *wedge-height*)
		      (t 0.0)))
         (y-vec (cond ((= w-rot 0.0) *wedge-height*)
		      ((= w-rot 180.0) (- *wedge-height*))
		      (t 0.0)))
	 (r02 (aref bt 2))
	 (r03 (aref bt 3))
	 (r12 (aref bt 6))
	 (r13 (aref bt 7))
	 (toe-x (+ (* (aref bt 0) x-vec)
		   (* (aref bt 1) y-vec)
		   (* r02 sad) r03))
	 (toe-y (+ (* (aref bt 4) x-vec)
		   (* (aref bt 5) y-vec)
		   (* r12 sad) r13)) 
	 (src-x (+ (* r02 sad) r03))
	 (src-y (+ (* r12 sad) r13)))
    (declare (single-float x-vec y-vec r02 r03 r12 r13))
    (setf (points prim)
      (if (and (poly:nearly-equal toe-x src-x 0.1)
	       (poly:nearly-equal toe-y src-y 0.1))
	  ;; vector tip/tail coincide - cannot determine wedge gradient
	  (append
	   (draw-indecisive-icon toe-x toe-y src-x src-y r03 r13 
				 scale x-origin y-origin
				 (* 0.5 *wedge-base-length*)
				 width height)
	   (points prim))
	;; vector tip and tail do not coincide
	(append
	 (draw-triangle-icon toe-x toe-y src-x src-y r03 r13
			     scale x-origin y-origin
			     *wedge-base-length* width height)
	 (points prim))))))

;;;----------------------------------------------

(defun draw-bev-wedge (prim b-ptl blk-outs col-ang sid
                       w-rot v-pos scl x-orig y-orig width height)

  "DRAW-BEV-WEDGE prim b-ptl blk-outs col-ang sid 
                       w-rot v-pos scl x-orig y-orig width height

Draws a wedge icon into graphics primitive prim for a beam's eye view
based upon wedge rotation w-rot, source-to-isocenter distance sid, and
collimator angle col-ang.  The wedge icon is drawn outside of the beam
portal and any block outlines (b-ptl & blk-outs resp).  The bev scale,
x-origin, y-origin, and view-position are also supplied."

  ;; place the wedge icon just to the outside of the beam portal 
  ;; and block outlines in the bev.

  (declare (single-float col-ang sid w-rot v-pos))
  (let* ((x-vec (cond ((= w-rot 90.0) (- *wedge-height*))
		      ((= w-rot 270.0) *wedge-height*)
		      (t 0.0)))
         (y-vec (cond ((= w-rot 0.0) *wedge-height*)
		      ((= w-rot 180.0) (- *wedge-height*))
		      (t 0.0)))
         (bdr-gap 4.0)
         (toe-x 0.0) (toe-y 0.0)
         (ctr-x 0.0) (ctr-y 0.0)
         (sin-c (sin col-ang))
         (cos-c (cos col-ang))
         (fac (/ (- sid v-pos) sid)) ;; assume wedge at isoctr
         (ptl-list (apply #'append (cons b-ptl blk-outs))))
    (declare (single-float x-vec y-vec bdr-gap toe-x toe-y
			   ctr-x ctr-y sin-c cos-c fac))
    (cond
     ((= w-rot 0.0)
      (setq toe-x (+ x-vec
		     (apply #'min (mapcar #'first ptl-list))
		     (- bdr-gap))
	    toe-y y-vec
	    ctr-x toe-x
	    ctr-y 0.0))
     ((= w-rot 90.0)
      (setq toe-x x-vec
	    toe-y (+ y-vec
		     (apply #'min (mapcar #'second ptl-list))
		     (- bdr-gap))
	    ctr-x 0.0
	    ctr-y toe-y))
     ((= w-rot 180.0)
      (setq toe-x (+ x-vec
		     (apply #'max (mapcar #'first ptl-list))
		     bdr-gap)
	    toe-y y-vec
	    ctr-x toe-x
	    ctr-y 0.0))
     ((= w-rot 270.0)
      (setq toe-x x-vec
	    toe-y (+ y-vec
		     (apply #'max (mapcar #'second ptl-list))
		     bdr-gap)
	    ctr-x 0.0
	    ctr-y toe-y)))
    ;; convert to view space
    (setf (points prim) 
      (append (draw-triangle-icon
	       (* fac (- (* toe-x cos-c) (* toe-y sin-c)))
	       (* fac (+ (* toe-x sin-c) (* toe-y cos-c)))
	       (* fac (- (* ctr-x cos-c) (* ctr-y sin-c)))
	       (* fac (+ (* ctr-x sin-c) (* ctr-y cos-c)))
	       0.0 0.0 scl x-orig y-orig 
	       *wedge-base-length* width height)
	      (points prim)))))

;;;----------------------------------------------

(defun draw-indecisive-icon (pt-x pt-y xc yc xi yi scl x-orig y-orig bl w h)

  "DRAW-INDECISIVE-ICON pt-x pt-y xc yc xi yi scl x-orig y-orig bl w h

Draws a square with an X inside of it, centered at pt, with sides of
length bl, signifying that a triangle cannot be drawn since there is
no basis for determining the triangle's height or orientation.  The
scl, x-orig, and y-orig parameters are the scale and origin of the
plane into which the square is drawn.  Draws the icon at the edge of
the picture along the line determined by (xc yc) and (xi yi) if it
would otherwise run outside of it.  Returns a list of the form {x1 y1
x2 y2}* suitable for passing to clx:draw-segments."

  (declare (single-float pt-x pt-y xc yc xi yi scl bl)
	   (fixnum x-orig y-orig w h))
  (let* ((xt (pix-x pt-x x-orig scl))
         (yt (pix-y pt-y y-orig scl))
         (x-ctr (pix-x xc x-orig scl))
         (y-ctr (pix-y yc y-orig scl))
         (x-iso (pix-x xi x-orig scl))
         (y-iso (pix-y yi y-orig scl))
         (b (round (* scl bl)))
         (xl (- xt b))  
         (yl (- yt b))
         (xh (+ xt b))  
         (yh (+ yt b))
         (result (list 
		  (list xl yl) (list xl yh) (list xh yh) (list xh yl)
		  (list xl yl) (list xh yh) (list xh yl) (list xl yh))))
    (declare (fixnum xt yt b))
    (keep-in-view result x-iso y-iso x-ctr y-ctr w h)))

;;;----------------------------------------------

(defun draw-triangle-icon (xt yt xc yc xi yi scl x-orig y-orig bl w h)

  "DRAW-TRIANGLE-ICON xt yt xc yc xi yi scl x-orig y-orig bl w h

Draws an isosoles triangle with tip at (xt yt) and (xc yc) in the
center of the triangle, using the supplied scale and origin
parameters.  Assumes that (xt yt) does not equal (xc yc), otherwise,
the orientation of the triangle of the plane cannot be determined.
The length of the base is bl.  The w and h parameters are the
width and height, respectively of the picture into which the icon will
be drawn.  If the icon would otherwise be drawn off the picture, it is
drawn at the intersection the edge of the screen with the ray from (xc
yc), understood to be the projection of the beam source into the
plane, to (xi yi), understood to be the projection of the beam
isocenter into the plane.  Returns a list of the form {x1 y1 x2 y2}*
suitable for passing to clx:draw-segments."

  (declare (single-float xt yt xc yc bl))
  (let* ((rlen (/ 1.0 (distance xt yt xc yc)))
	 (dx (* rlen (- xc xt))) 
	 (dy (* rlen (- yc yt)))
	 (xb (- (* 2 xc) xt))
	 (yb (- (* 2 yc) yt))
	 (x-tip  (pix-x xt x-orig scl))
	 (y-tip  (pix-y yt y-orig scl))
         (x-ctr  (pix-x xc x-orig scl))
         (y-ctr  (pix-y yc y-orig scl))
         (x-iso  (pix-x xi x-orig scl))
         (y-iso  (pix-y yi y-orig scl))
	 (x-edg1 (pix-x (- xb (* bl dy)) x-orig scl))
	 (x-edg2 (pix-x (+ xb (* bl dy)) x-orig scl))
	 (y-edg1 (pix-y (+ yb (* bl dx)) y-orig scl))
	 (y-edg2 (pix-y (- yb (* bl dx)) y-orig scl)))
    (declare (single-float rlen dx dy xb yb))
    (keep-in-view (list (list x-ctr y-ctr)
			(list x-tip y-tip)
			(list x-edg1 y-edg1)
			(list x-edg2 y-edg2)
			(list x-tip y-tip))
		  x-iso y-iso x-ctr y-ctr w h)))

;;;----------------------------------------------

(defun keep-in-view (pts xi yi xc yc w h)

  "KEEP-IN-VIEW pts xi yi xc yc w h

Ensures that pts, a list of the form {(x y)}*, is inside the region
bounded by (0,0) and (w,h).  If the pts are outside this region, they
are moved as necessary along the line determined by (xi yi) and (xc
yc) to bring them back into the region.  A list of the form {x1 y1 x2
y2}* is returned, suitable for passing to clx:draw-segments."

  (declare (fixnum xi yi xc yc w h))
  (let* ((x-pts (mapcar #'first pts))
         (y-pts (mapcar #'second pts))
	 (min-x (apply #'min x-pts)) 
         (max-x (apply #'max x-pts))
	 (min-y (apply #'min y-pts)) 
         (max-y (apply #'max y-pts))
         (x1 xc)
         (y1 yc)
         (x2 (- (* 2 xi) xc))
         (y2 (- (* 2 yi) yc))
         (bdr-gap 5)
         (xl-bdr (+ bdr-gap (- xc min-x)))
         (yl-bdr (+ bdr-gap (- yc min-y)))
         (xh-bdr (- (+ w xc) (+ bdr-gap max-x)))
         (yh-bdr (- (+ h yc) (+ bdr-gap max-y))))
    (declare (fixnum min-x max-x min-y max-y x1 y1 x2 y2 
                     bdr-gap xl-bdr yl-bdr xh-bdr yh-bdr))
    (when (clip x1 y1 x2 y2 xl-bdr yl-bdr xh-bdr yh-bdr)
      (let ((dx (- x1 xc))
            (dy (- y1 yc)))
	(declare (fixnum dx dy))
	(dolist (pt pts)
	  (incf (first pt) dx)
	  (incf (second pt) dy))))
    (do* ((ptr pts (rest ptr))
          (segs nil))
	((null (rest ptr)) segs)
      (push (second (second ptr)) segs)
      (push (first (second ptr)) segs)
      (push (second (first ptr)) segs)
      (push (first (first ptr)) segs))))

;;;----------------------------------------------
;;; End.
