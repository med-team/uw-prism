;;;
;;; dose-grid-mediators
;;;
;;; The class definitions and functions for mediators involved 
;;; with the management of grid geometry information.
;;;
;;; 15-Oct-1993 J. Unger created from design report and earlier prototypes.
;;; 20-Oct-1993 J. Unger add dose-specification-manager.
;;;  5-Nov-1993 J. Unger add grid-view-mediator.
;;; 29-Nov-1993 J. Unger change occurrences new-dim to new-density, add
;;; new-color add- & remove-notifies to grid-view-mediator code.
;;;  3-Jan-1994 J. Unger change 'density' to 'voxel-size' in code.
;;;  8-Apr-1994 I. Kalet split off from dose-mediators
;;; 18-Apr-1994 I. Kalet add code for corner grab boxes, change ref to
;;; view origin to new names
;;; 25-Apr-1994 I. Kalet move pix-x and pix-y to contour-graphics,
;;; change color to gcontext instead of symbol, don't display in bev.
;;; 22-May-1994 I. Kalet ignore grab box motion unless button 1 down
;;; 16-Jun-1994 I. Kalet change color in dose grid to display-color
;;;  3-Sep-1995 I. Kalet use cm-x and cm-y where appropriate.  Coerce
;;;  single-float in a few spots also.
;;;  9-Oct-1996 I. Kalet make calls to draw conform to signature,
;;; don't use apply and &rest parameters, in draw or in lambda for
;;; update-view call.
;;; 25-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;;  2-Oct-2002 I. Kalet add support for other view types - just ignore
;;; for now
;;;

(in-package :prism)

;;;--------------------------------------

(defclass grid-view-mediator (object-view-mediator)

  ((ulc :accessor ulc
	:initform nil
	:documentation "The grab box for the upper left corner.")

   (llc :accessor llc
	:documentation "The grab box for the lower left corner.")

   (urc :accessor urc
	:documentation "The grab box for the upper right corner.")

   (lrc :accessor lrc
	:documentation "The grab box for the lower right corner.")

   )

  (:documentation "This mediator connects a grid geometry with a view.
It also maintains the grid corner grab boxes in the view")
  )

;;;--------------------------------------

(defmethod grid-box-xy ((vw transverse-view) grid)

  "returns four values, x-left, x-right, y-upper and y-lower pixel
coordinates of the grid grab boxes in view vw."

  (let* ((xl (x-origin grid))
	 (yl (y-origin grid))
	 (xr (+ xl (x-size grid)))
	 (yu (+ yl (y-size grid)))
	 (x0 (x-origin vw))
	 (y0 (y-origin vw))
	 (ppcm (scale vw)))
    (values (pix-x xl x0 ppcm)
	    (pix-x xr x0 ppcm)
	    (pix-y yu y0 ppcm)
	    (pix-y yl y0 ppcm))))

;;;--------------------------------------

(defmethod grid-box-xy ((vw coronal-view) grid)

  "returns four values, x-left, x-right, y-upper and y-lower pixel
coordinates of the grid grab boxes in view vw."

  (let* ((xl (x-origin grid))
	 (yu (- (z-origin grid)))
	 (xr (+ xl (x-size grid)))
	 (yl (- yu (z-size grid)))
	 (x0 (x-origin vw))
	 (y0 (y-origin vw))
	 (ppcm (scale vw)))
    (values (pix-x xl x0 ppcm)
	    (pix-x xr x0 ppcm)
	    (pix-y yu y0 ppcm)
	    (pix-y yl y0 ppcm))))


;;;--------------------------------------

(defmethod grid-box-xy ((vw sagittal-view) grid)

  "returns four values, x-left, x-right, y-upper and y-lower pixel
coordinates of the grid grab boxes in view vw."

  (let* ((xl (z-origin grid))
	 (yl (y-origin grid))
	 (xr (+ xl (z-size grid)))
	 (yu (+ yl (y-size grid)))
	 (x0 (x-origin vw))
	 (y0 (y-origin vw))
	 (ppcm (scale vw)))
    (values (pix-x xl x0 ppcm)
	    (pix-x xr x0 ppcm)
	    (pix-y yu y0 ppcm)
	    (pix-y yl y0 ppcm))))

;;;--------------------------------------

(defmethod update-grid ((vw transverse-view) gg xl xr yu yl)

  (let ((x0 (x-origin vw))
	(y0 (y-origin vw))
	(ppcm (scale vw)))
    (setf (x-origin gg) (cm-x xl x0 ppcm)
	  (y-origin gg) (cm-y yl y0 ppcm)
	  (x-size gg) (coerce (/ (- xr xl) ppcm) 'single-float)
	  (y-size gg) (coerce (/ (- yl yu) ppcm) 'single-float))))

;;;--------------------------------------

(defmethod update-grid ((vw coronal-view) gg xl xr yu yl)

  (let ((x0 (x-origin vw))
	(y0 (y-origin vw))
	(ppcm (scale vw)))
    (setf (x-origin gg) (cm-x xl x0 ppcm)
	  (z-origin gg) (cm-x yu y0 ppcm) ;; z is like x here
	  (x-size gg) (coerce (/ (- xr xl) ppcm) 'single-float)
	  (z-size gg) (coerce (/ (- yl yu) ppcm) 'single-float))))

;;;--------------------------------------

(defmethod update-grid ((vw sagittal-view) gg xl xr yu yl)

  (let ((x0 (x-origin vw))
	(y0 (y-origin vw))
	(ppcm (scale vw)))
    (setf (z-origin gg) (cm-x xl x0 ppcm) ;; z is like x here too
	  (y-origin gg) (cm-y yl y0 ppcm)
	  (z-size gg) (coerce (/ (- xr xl) ppcm) 'single-float)
	  (y-size gg) (coerce (/ (- yl yu) ppcm) 'single-float))))

;;;--------------------------------------

(defmethod view-intersects-grid ((vw transverse-view) gg)

  (let ((z (view-position vw))
	(z0 (z-origin gg)))
    (and (>= z z0)
	 (<= z (+ z0 (z-size gg))))))

;;;--------------------------------------

(defmethod view-intersects-grid ((vw coronal-view) gg)

  (let ((y (view-position vw))
	(y0 (y-origin gg)))
    (and (>= y y0)
	 (<= y (+ y0 (y-size gg))))))

;;;--------------------------------------

(defmethod view-intersects-grid ((vw sagittal-view) gg)

  (let ((x (view-position vw))
	(x0 (x-origin gg)))
    (and (>= x x0)
	 (<= x (+ x0 (x-size gg))))))

;;;--------------------------------------

(defmethod view-intersects-grid ((vw view) gg)

  "default for all other views - always return nil!"

  (declare (ignore gg))
  nil)

;;;--------------------------------------

(defun add-grid-boxes (gvm)

  "adds four grab boxes in the view in gvm at the grid corners of the
grid in gvm, if the view intersects the grid."

  (let* ((vw (view gvm))
	 (gg (object gvm))
	 (col (sl:color-gc (display-color gg))))
    (when (view-intersects-grid vw gg) ;; check for intersection...
      (multiple-value-bind (xl xr yu yl) (grid-box-xy vw gg)
	(setf (ulc gvm) (sl:make-square gg xl yu :color col)
	      (llc gvm) (sl:make-square gg xl yl :color col)
	      (urc gvm) (sl:make-square gg xr yu :color col)
	      (lrc gvm) (sl:make-square gg xr yl :color col)))
      (sl:add-pickable-obj
       (list (ulc gvm) (llc gvm) (urc gvm) (lrc gvm))
       (picture vw))
      ;; register with the grab boxes - just set origin and size of
      ;; grid and the actions for those will update the rest
      (ev:add-notify gvm (sl:motion (ulc gvm))
		     #'(lambda (med gb x y state)
			 (declare (ignore gb))
			 (when (member :button-1
				       (clx:make-state-keys state))
			   (let* ((grid (object med))
				  (v (view med)))
			     (multiple-value-bind (xla xra yua yla)
				 (grid-box-xy v grid)
			       (declare (ignore xla yua))
			       (update-grid v grid x xra y yla))))))
      (ev:add-notify gvm (sl:motion (llc gvm))
		     #'(lambda (med gb x y state)
			 (declare (ignore gb))
			 (when (member :button-1
				       (clx:make-state-keys state))
			   (let* ((grid (object med))
				  (v (view med)))
			     (multiple-value-bind (xla xra yua yla)
				 (grid-box-xy v grid)
			       (declare (ignore xla yla))
			       (update-grid v grid x xra yua y))))))
      (ev:add-notify gvm (sl:motion (urc gvm))
		     #'(lambda (med gb x y state)
			 (declare (ignore gb))
			 (when (member :button-1
				       (clx:make-state-keys state))
			   (let* ((grid (object med))
				  (v (view med)))
			     (multiple-value-bind (xla xra yua yla)
				 (grid-box-xy v grid)
			       (declare (ignore xra yua))
			       (update-grid v grid xla x y yla))))))
      (ev:add-notify gvm (sl:motion (lrc gvm))
		     #'(lambda (med gb x y state)
			 (declare (ignore gb))
			 (when (member :button-1
				       (clx:make-state-keys state))
			   (let* ((grid (object med))
				  (v (view med)))
			     (multiple-value-bind (xla xra yua yla)
				 (grid-box-xy v grid)
			       (declare (ignore xra yla))
			       (update-grid v grid xla x yua y))))))
      )))

;;;--------------------------------------

(defun update-grid-boxes (gvm)

  "updates the grab boxes if present and still intersecting.  Adds
them if not present and intersecting.  Deletes them if not
intersecting and present."

  (let ((gg (object gvm))
	(vw (view gvm)))
    (if (view-intersects-grid vw gg)
	(if (ulc gvm) ;; already have them, so update them
	    (multiple-value-bind (xl xr yu yl)
		(grid-box-xy vw gg)
	      (setf (sl:x-center (ulc gvm)) xl
		    (sl:y-center (ulc gvm)) yu
		    (sl:x-center (llc gvm)) xl
		    (sl:y-center (llc gvm)) yl
		    (sl:x-center (urc gvm)) xr
		    (sl:y-center (urc gvm)) yu
		    (sl:x-center (lrc gvm)) xr
		    (sl:y-center (lrc gvm)) yl))
	  (add-grid-boxes gvm)) ;; otherwise add them
      (if (ulc gvm) ;; if present, remove them, otherwise nothing
	  (progn (sl:remove-pickable-objs gg (picture vw))
		 (setf (ulc gvm) nil)))))) ;; only need to set the first one

;;;--------------------------------------

(defmethod update-grid :after ((vw view) gg xl xr yu yl)

  "insures only one graphic update."

  (declare (ignore xl xr yu yl))
  (ev:announce gg (new-coords gg)))

;;;--------------------------------------

(defmethod initialize-instance :after ((gvm grid-view-mediator)
                                        &rest initargs)
  (declare (ignore initargs))
  (let* ((gg (object gvm))
	 (vw (view gvm)))
    (add-grid-boxes gvm) ;; create the grab boxes
    (ev:add-notify gvm (new-coords gg)
		   #'(lambda (med grid &rest pars)
		       (declare (ignore pars))
		       (update-grid-boxes med)
		       (update-view med grid)))
    (ev:add-notify gvm (new-voxel-size gg) #'update-view)
    (ev:add-notify gvm (new-color gg)
		   #'(lambda (med grid &rest pars)
		       (declare (ignore pars))
		       (if (ulc med)
			   (let ((col (sl:color-gc
				       (display-color (object med)))))
			     (setf (sl:color (ulc med)) col
				   (sl:color (llc med)) col
				   (sl:color (urc med)) col
				   (sl:color (lrc med)) col)))
		       (update-view med grid)))

    ;; this supercedes the generic object-view-mediator add-notify
    ;; but the generic remove-notify is still ok.
    (ev:add-notify gvm (refresh-fg vw)
		   #'(lambda (med v)
		       (update-grid-boxes med) ;; this is additional
		       (draw (object med) v)))))

;;;--------------------------------------

(defmethod destroy :after ((gvm grid-view-mediator))

  (let ((obj (object gvm))
	(vw (view gvm)))
    (sl:remove-pickable-objs obj (picture vw))
    (ev:remove-notify gvm (new-coords obj))
    (ev:remove-notify gvm (new-voxel-size obj))
    (ev:remove-notify gvm (new-color obj))))

;;;--------------------------------------

(defun make-grid-view-mediator (gg v)

  "MAKE-GRID-VIEW-MEDIATOR gg v

Creates and returns a grid-view-mediator between grid-geometry gg and 
view v."

  (make-instance 'grid-view-mediator :object gg :view v))

;;;--------------------------------------
;;; End.
