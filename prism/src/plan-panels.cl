;;;
;;; plan-panels
;;;
;;; Implements the plan panel with all the dose display stuff
;;;
;;; 15-Jan-1992 J. Unger enhance call to interactive-make-view to handle 
;;; interactive creation of beam's eye views.
;;; 15-Feb-1993 I. Kalet add table-position to interactive-make-view
;;; call, add time-stamp display, rearrange and parametrize buttons.
;;; 30-Aug-1993 I. Kalet change button and textline placement, add
;;; dose panel button, ckpt button.
;;;  5-Sep-1993 I. Kalet split off from plans module
;;; 15-Oct-1993 J. Unger hook up dose panel.
;;; 18-Oct-1993 J. Unger simplify dose-panel initialization code.
;;; 19-Oct-1993 J. Unger change name of store plan button to 'archive'.
;;; 21-Oct-1993 J. Unger add code for deletion of dose-panel.
;;; 29-Oct-1993 I. Kalet add code for save plan and checkpoint plan.
;;;  3-Nov-1993 J. Unger finish adding code for save & checkpoint plan.
;;; 22-Dec-1993 J. Unger fix bug to allow dose info to get saved with
;;; checkpointed plans.
;;;  3-Jan-1994 I. Kalet plans have reference to patient, not
;;;  forwarded data.
;;; 11-Feb-1994 J. Unger implement Print Chart operation.
;;; 22-Apr-1994 J. Unger add point-dose panel button and action functions.
;;; 05-May-1994 J. Unger make point-dose panel go away properly.
;;; 13-May-1994 I. Kalet only make point dose panel if there are
;;; points and beams.
;;; 16-May-1994 J. Unger add support for comments textbox.
;;; 18-May-1994 I. Kalet embed comments textbox right on the plan panel.
;;;  8-Jun-1994 J. Unger elim *save-plan-dose* mechanism for saving
;;;  dose info.
;;; 13-Jun-1994 I. Kalet fix copy plan button so it retains new plan.
;;; 08-Jul-1994 J. Unger put temporary reminders into plan archive & chekpt
;;;  operations to remind user not to cross save cases and plans.
;;; 12-Jan-1995 I. Kalet get table-position from patient, not plan.
;;; Pass plan and patient to make-beam-panel, make-view-panel, etc.
;;; 27-Apr-1995 I. Kalet turn timestamp border red when timestamp
;;; changes, turn white when archive or checkpoint is successful.
;;;  2-Jun-1996 I. Kalet big revision to add brachy support.
;;; 21-Jan-1997 I. Kalet eliminate table-position.
;;;  9-Jun-1997 I. Kalet move stuff here from dose-panels, reorganize
;;;  according to spec., including colored button labels for beams and
;;;  dose levels.
;;;  2-May-1998 I. Kalet new make-chart-panel function.
;;; 21-Mar-1999 I. Kalet add beam sorting popup panel, called here,
;;; but general function implemented in selector-panels.
;;; 19-Mar-2000 I. Kalet revision of support for brachy, and new chart
;;; code using PostScript.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 14-Oct-2001 I. Kalet adapt copy plan function to new semantics of
;;; the copy method - exact copy, modify by caller afterward.
;;;  4-Jan-2002 I. Kalet make comments textbox slightly higher to
;;; accomodate the bottom line.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass plan-panel (generic-panel)

  ((the-plan :accessor the-plan
	     :initarg :the-plan
	     :documentation "The plan that this panel edits.")
   
   (patient-of :accessor patient-of
	       :initarg :patient-of
	       :documentation "The current patient.")

   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame containing all the
panel stuff.")

   (name-box :accessor name-box
	     :documentation "The textline for the plan name.")

   (plan-by-box :accessor plan-by-box
		:documentation "The textline for the planner's name.")

   (timestamp-box :accessor timestamp-box
		   :documentation "Displays the time stamp.")

   (comments-box :accessor comments-box
                 :documentation "The plan's comments box.")

   (delete-b :accessor delete-b
	     :documentation "The Delete Panel button.")

   (comments-btn :accessor comments-btn
		 :documentation "The Accept button for accepting the
text in the comments box, i.e., making an update to the plan comments
slot.")

   (copy-b :accessor copy-b
	   :documentation "The Copy Plan button.")

   (save-b :accessor save-b
	   :documentation "The Archive Plan button.")

   (ckpt-b :accessor ckpt-b
	   :documentation "The Checkpoint button")

   (brachy-b :accessor brachy-b
	     :documentation "The Brachy Sources button")

   (brachy-panel :accessor brachy-panel
		 :initform nil
		 :documentation "The plan panel's brachytherapy
sources panel.")

   (point-b :accessor point-b
            :documentation "The Point Dose Panel button.")

   (point-dose-panel :accessor point-dose-panel
                     :initform nil
                     :documentation "The plan panel's point dose panel.")

   (sort-beams-btn :accessor sort-beams-btn
		   :documentation "The button for the beam sorting and
linking subpanel.")

   (print-b :accessor print-b
	    :documentation "The Print Chart button")

   (compute-btn :accessor compute-btn
                :documentation "The Compute Dose button.")

   (write-dose-btn :accessor write-dose-btn
                   :documentation "The Write Dose button brings up a 
menu of three dose files to send a valid dose dist.")

   (beam-selector :accessor beam-selector
		  :documentation "The selector panel listing the beams
in the plan.")

   (dose-selector :accessor dose-selector
		  :documentation "The selector panel listing the dose
levels in the plan.")

   (view-selector :accessor view-selector
		  :documentation "The selector panel listing the views
for this plan.")

   (grid-size-btn :accessor grid-size-btn
                  :documentation "The grid size button.")

   (grid-color-btn :accessor grid-color-btn
                   :documentation "The dose grid color button.")

   (max-dos-rdt :accessor max-dos-rdt
                :documentation "The maximum dose readout.")

   (max-coord-rdt :accessor max-coord-rdt
		  :documentation "The maximum dose coordinates readout.")

   (busy :accessor busy
	 :initform nil
	 :documentation "The mediator busy flag for updates.")

   )

  )

;;;------------------------------------------

(defun update-max-dose-display (pp)

  "update-max-dose-display pp

Updates the 'max dose' and 'coords' textfields of the dose panel pp
when pp's plan's sum-result becomes valid."

  (let* ((max-dose -1.0)
         (max-i -1)
         (max-j -1)
         (max-k -1)
         (dose-arr (grid (sum-dose (the-plan pp))))
         (x-dim (array-dimension dose-arr 0))
         (y-dim (array-dimension dose-arr 1))
         (z-dim (array-dimension dose-arr 2))
         (dose-grid (dose-grid (the-plan pp))))
    (declare (single-float max-dose))
    (declare (fixnum max-i max-j max-k x-dim y-dim z-dim))
    (dotimes (i x-dim)
      (dotimes (j y-dim)
        (dotimes (k z-dim)
          (when (< max-dose (aref dose-arr i j k))
            (setq 
		max-dose (aref dose-arr i j k)
		max-i i
		max-j j
		max-k k)))))
    (setf (sl:info (max-dos-rdt pp)) 
      (write-to-string (fix-float max-dose 2)))
    (setf (sl:info (max-coord-rdt pp))
      (concatenate 'string
        (write-to-string (fix-float
			  (+ (x-origin dose-grid) 
			     (* (x-size dose-grid) (/ max-i (1- x-dim))))
			  2))
        ", "
        (write-to-string (fix-float
			  (+ (y-origin dose-grid) 
			     (* (y-size dose-grid) (/ max-j (1- y-dim))))
			  2))
        ", "
        (write-to-string (fix-float
			  (+ (z-origin dose-grid) 
			     (* (z-size dose-grid) (/ max-k (1- z-dim))))
			  2))))))

;;;------------------------------------------

(defun write-dose-info (plan pat)

  "write-dose-info plan pat

Writes a plan's dose information (with some other identification) out
to a dose file in the user's checkpoint directory."

  (if (valid-grid (sum-dose plan))
      (let* ((filelist '("dose1" "dose2" "dose3"))
	     (choice (sl:popup-menu filelist :title "Filename"))
	     (filename (when choice (nth choice filelist)))
	     (pathname (when choice (merge-pathnames *local-database*
						     filename))))
	(if choice
	    (if (probe-file *local-database*)
		(with-open-file (stream pathname 
				 :direction :output
				 :if-exists :supersede
				 :if-does-not-exist :create)
		  (format stream "PRISM:PATIENT-ID  ~a ~%"
			  (patient-id pat))
		  (format stream "PRISM:CASE-ID  ~a ~%"
			  (case-id pat))
		  (format stream "PRISM:NAME  ~a ~%"
			  (name plan))
		  (format stream "PRISM:TIME-STAMP  ~a ~%"
			  (time-stamp plan))
		  (put-object (dose-grid plan) stream)
		  (put-object (sum-dose plan) stream)
		  (sl:acknowledge "Dose file written."))
	      (sl:acknowledge "Unable to find checkpoint database."))
	  (sl:acknowledge "No choice made -- no dose file written.")))
    (sl:acknowledge 
     "You must compute a dose grid before saving dose information.")))

;;;------------------------------------------

(defun make-plan-panel (pln pat &rest initargs)

  "make-plan-panel pln pat &rest initargs

returns an instance of a plan panel for the plan pln and patient pat."

  (apply #'make-instance 'plan-panel
	 :the-plan pln :patient-of pat initargs))

;;;------------------------------------------

(defmethod initialize-instance :after ((pp plan-panel) &rest initargs)

  (let* ((p (the-plan pp))
	 (pat (patient-of pp))
	 (ppf (symbol-value *small-font*))
	 (pan-fr (apply #'sl:make-frame 585 370
			:title "Prism PLAN Panel" initargs))
	 (pp-win (sl:window pan-fr))
	 (bth 25) ;; button and textline height for small font
	 (btw 120) ;; regular button and textline width
	 (sbw 20) ;; small button width
	 (dx 10) ;; left margin
	 (top-y 10) ;; dosim, plan id and timestamp are at top
	 (dy (+ top-y bth 95)) ;; where the middle stuff starts
	 (sp-wd (+ btw 20)) ;; the width of the selector panels
	 (sp-ht 205) ;; the height of the selector panels
	 ;; readouts, textlines and textbox at top
	 (plan-by-t (apply #'sl:make-textline 200 bth
			   :font ppf
			   :label "DS: "
			   :ulc-x dx :ulc-y top-y
			   :parent pp-win initargs))
	 (name-t (apply #'sl:make-textline 195 bth
			:font ppf :label "Plan ID: "
			:ulc-x (+ dx 205) :ulc-y top-y
			:parent pp-win initargs))
	 (ts-box (apply #'sl:make-readout 165 bth
			:font ppf :ulc-x 415 :ulc-y top-y
			:parent pp-win initargs))
	 (com-box (apply #'sl:make-textbox 440 85
			 :ulc-x (+ dx btw 10)
			 :ulc-y (bp-y top-y bth 1)
			 :info (comments p)
			 :parent pp-win initargs))
	 (del-b (apply #'sl:make-button btw bth :button-type :momentary
		       :ulc-x dx :ulc-y (bp-y top-y bth 1)
		       :font ppf
		       :label "Delete Panel"
		       :parent pp-win initargs))
	 (cmt-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y top-y bth 2)
		       :font ppf
		       :label "Accept cmts"
		       :parent pp-win initargs))
	 (cpy-b (apply #'sl:make-button btw bth :button-type :momentary 
		       :ulc-x dx :ulc-y (bp-y top-y bth 3)
		       :font ppf
		       :label "Copy Plan"
		       :parent pp-win initargs))
	 (sv-b (apply #'sl:make-button btw bth :button-type :momentary
		      :ulc-x dx :ulc-y (bp-y top-y bth 4)
		      :font ppf
		      :label "Archive"
		      :parent pp-win initargs))
	 (ckp-b (apply #'sl:make-button btw bth :button-type :momentary
		       :ulc-x dx :ulc-y (bp-y top-y bth 5)
		       :font ppf
		       :label "Checkpt"
		       :parent pp-win initargs))
	 (bra-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y top-y bth 6)
		       :font ppf
		       :label "Brachy sources"
		       :parent pp-win initargs))
	 (pt-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y top-y bth 7)
		       :font ppf
		       :label "Point doses"
		       :parent pp-win initargs))
	 (bsrt-b (apply #'sl:make-button btw bth :button-type :momentary
			:ulc-x dx :ulc-y (bp-y top-y bth 8)
			:font ppf
			:label "Sort beams"
			:parent pp-win initargs))
	 (prt-b (apply #'sl:make-button btw bth :button-type :momentary
		       :ulc-x dx :ulc-y (bp-y top-y bth 9)
		       :font ppf
		       :label "Print Chart"
		       :parent pp-win initargs))
         (comp-b (apply #'sl:make-button btw bth :button-type :momentary
			:ulc-x dx :ulc-y (bp-y top-y bth 10)
			:font ppf
			:label "Compute"
			:parent pp-win initargs))
         (wrtd-b (apply #'sl:make-button btw bth :button-type :momentary
			:ulc-x dx :ulc-y (bp-y top-y bth 11)
			:font ppf
			:label "Write Dose"
			:parent pp-win initargs))
         (gsize-b (apply #'sl:make-button (- btw sbw 10) bth
			 :ulc-x (+ dx btw 5)
			 :ulc-y (bp-y top-y bth 11)
			 :font ppf
			 :parent pp-win initargs))
         (gcolor-b (apply #'sl:make-button sbw bth
			  :ulc-x (+ dx btw (- btw sbw))
			  :ulc-y (bp-y top-y bth 11)
			  :bg-color (display-color (dose-grid p))
			  :parent pp-win initargs))
         (max-dos-r (apply #'sl:make-readout (+ btw 20) bth
			   :ulc-x (+ dx (* 2 btw) 5)
			   :ulc-y (bp-y top-y bth 11)
			   :font ppf
			   :label "Max Dose: "
			   :parent pp-win initargs))
         (max-crd-r (apply #'sl:make-readout 180 bth
			   :ulc-x (+ dx (* 3 btw) 30)
			   :ulc-y (bp-y top-y bth 11)
			   :font ppf
			   :label "At: "
			   :parent pp-win initargs)))
    (setf (panel-frame pp) pan-fr ;; put all the widgets in the slots
	  ;; info initialized here so that it won't be centered
	  (plan-by-box pp) plan-by-t
	  (sl:info plan-by-t) (plan-by p)
	  (name-box pp) name-t
	  (sl:info name-t) (name p)
	  (timestamp-box pp) ts-box
	  (sl:info ts-box) (time-stamp p)
          (comments-box pp) com-box
	  (delete-b pp) del-b
	  (comments-btn pp) cmt-b
	  (copy-b pp) cpy-b
	  (save-b pp) sv-b
	  (ckpt-b pp) ckp-b
	  (brachy-b pp) bra-b
	  (point-b pp) pt-b
	  (sort-beams-btn pp) bsrt-b
	  (print-b pp) prt-b
	  (compute-btn pp) comp-b
	  (write-dose-btn pp) wrtd-b
	  (grid-size-btn pp) gsize-b
	  (grid-color-btn pp) gcolor-b
	  (max-dos-rdt pp) max-dos-r
	  (max-coord-rdt pp) max-crd-r)
    (ev:add-notify pp (sl:new-info plan-by-t)
		   #'(lambda (pan pl info)
		       (declare (ignore pl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (plan-by (the-plan pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify pp (new-plan-by p)
		   #'(lambda (pan pl info)
		       (declare (ignore pl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (plan-by-box pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify pp (sl:new-info name-t)
		   #'(lambda (pan tl info)
		       (declare (ignore tl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (name (the-plan pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify pp (new-name p)
		   #'(lambda (pan pl info)
		       (declare (ignore pl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (name-box pan)) info)
			 (setf (busy pan) nil))))
    (ev:add-notify pp (new-time-stamp p)
		   #'(lambda (pan pl new-ts)
		       (declare (ignore pl))
		       (setf (sl:info (timestamp-box pan)) new-ts)
		       (setf (sl:border-color (timestamp-box pan))
			 'sl:red)))
    (ev:add-notify pp (sl:new-info com-box)
		   #'(lambda (pan box)
		       (declare (ignore box))
		       (unless (sl:on (comments-btn pan))
			 (setf (sl:on (comments-btn pan)) t))))
    (ev:add-notify pp (sl:button-on del-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (destroy pan)))
    (ev:add-notify pp (sl:button-off cmt-b)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
		       (setf (comments (the-plan pan))
			 (sl:info (comments-box pan)))))
    (ev:add-notify pp (sl:button-on cpy-b)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
		       (let ((temp-plan (copy (the-plan pan))))
			 (setf (name temp-plan)
			   (format nil "~A" (gensym "PLAN-")))
			 (coll:insert-element temp-plan
					      (plans (patient-of pan))))))
    (ev:add-notify pp (sl:button-on sv-b)
		   #'(lambda (pan btn)
		       (let ((pat (patient-of pan)))
			 (if (zerop (case-id pat))
			     (sl:acknowledge 
			      '("Plan not archived: belongs to new case."
				"Archive case from Patient panel instead."))
			   (if (sl:confirm 
				'("Reminder - only archive this plan if"
				  "current case came from archive."))
			       (if (put-plan-data 
				    (patient-id pat) (case-id pat)
				    (the-plan pan) *patient-database*)
				   (progn
				     (sl:acknowledge
				      "Plan successfully archived")
				     (setf (sl:border-color
					    (timestamp-box pan))
				       'sl:white))
				 (sl:acknowledge "Archive failed"))
			     (sl:acknowledge "Archive aborted."))))
		       (setf (sl:on btn) nil)))
    (ev:add-notify pp (sl:button-on ckp-b)
		   #'(lambda (pan btn)
		       (let ((pat (patient-of pan)))
			 (if (zerop (case-id pat))
			     (sl:acknowledge
			      '("Plan not checkpointed: belongs to new case."
				"Checkpoint case from Patient panel instead."))
			   (if (sl:confirm 
				'("Reminder - only checkpoint this plan if"
				  "current case came from checkpt database"))
			       (if (put-plan-data 
				    (patient-id pat) (case-id pat)
				    (the-plan pan) *local-database*)
				   (progn
				     (sl:acknowledge "Plan saved locally")
				     (setf (sl:border-color
					    (timestamp-box pan))
				       'sl:white))
				 (sl:acknowledge "Checkpoint failed"))
			     (sl:acknowledge "Checkpoint aborted."))))
		       (setf (sl:on btn) nil)))
    (ev:add-notify pp (sl:button-on bra-b) 
                   #'(lambda (pan bb)
		       (declare (ignore bb))
		       (let* ((pln (the-plan pan))
			      (bra-p (make-brachy-panel 
				       :line-sources (line-sources pln)
				       :seeds (seeds pln)
				       :points (points (patient-of pan)))))
			 (setf (brachy-panel pan) bra-p)
			 (ev:add-notify pan (deleted bra-p)
					#'(lambda (ppl bp)
					    (ev:remove-notify ppl
							      (deleted bp))
					    (setf (brachy-panel ppl) nil)
					    (when (not (busy ppl))
					      (setf (busy ppl) t)
					      (setf (sl:on (brachy-b ppl))
						nil)
					      (setf (busy ppl) nil)))))))
    (ev:add-notify pp (sl:button-off bra-b)
                   #'(lambda (pan btn)
                       (declare (ignore btn))
                       (when (not (busy pan))
                         (setf (busy pan) t)
                         (destroy (brachy-panel pan))
                         (setf (busy pan) nil))))
    (ev:add-notify pp (sl:button-on pt-b) 
                   #'(lambda (pan pb) ;; first check for points and beams
		       (if (and (coll:elements (points (patient-of pan)))
				(coll:elements (beams (the-plan pan))))
			   (let ((pdp (make-point-dose-panel 
				       :plan (the-plan pan)
				       :pat (patient-of pan))))
			     (setf (point-dose-panel pan) pdp)
			     (ev:add-notify pan (deleted pdp)
					    #'(lambda (ppl pdp)
						(ev:remove-notify
						 ppl (deleted pdp))
						(setf (point-dose-panel ppl)
						  nil)
						(when (not (busy ppl))
						  (setf (busy ppl) t)
						  (setf (sl:on
							 (point-b ppl))
						    nil)
						  (setf (busy ppl)
						    nil)))))
			 (progn
			   (sl:acknowledge "Points or beams missing")
			   (setf (busy pan) t)
			   (setf (sl:on pb) nil)
			   (setf (busy pan) nil)))))
    (ev:add-notify pp (sl:button-off pt-b)
                   #'(lambda (pan btn)
                       (declare (ignore btn))
                       (when (not (busy pan))
                         (setf (busy pan) t)
                         (destroy (point-dose-panel pan))
                         (setf (busy pan) nil))))
    (ev:add-notify pp (sl:button-on prt-b)
		   #'(lambda (pan btn) 
                       (declare (ignore btn))
		       (chart-panel 'main (patient-of pan) (the-plan pan))
		       (setf (sl:on (print-b pan)) nil)))
    (ev:add-notify pp (sl:button-on bsrt-b)
		   #'(lambda (pan btn)
		       (popup-listsort (beam-selector pan))
		       (setf (sl:on btn) nil)))
    (ev:add-notify pp (sl:button-on comp-b)
		   #'(lambda (pan bt)
		       (compute-dose-grid (the-plan pan) (patient-of pan))
		       (setf (sl:on bt) nil)))
    (ev:add-notify pp (sl:button-on wrtd-b)
		   #'(lambda (pan bt)
		       (write-dose-info (the-plan pan) (patient-of pan))
		       (setf (sl:on bt) nil)))
    ;; set up the coarseness label
    (let ((vs (voxel-size (dose-grid p))))
      (cond
       ((= vs *fine-grid-size*)   
	(setf (sl:label gsize-b) "Fine Grid"))
       ((= vs *medium-grid-size*) 
	(setf (sl:label gsize-b) "Medium Grid"))
       ((= vs *coarse-grid-size*) 
	(setf (sl:label gsize-b) "Coarse Grid"))
       (t (setf (sl:label gsize-b) (format nil "~A" vs)))))
    (ev:add-notify p (sl:button-on gsize-b)
		   #'(lambda (pl bt)
		       (let ((item (sl:popup-menu 
				    '("Coarse Grid"
				      "Medium Grid"
				      "Fine Grid"))))
			 (when item
			   (case item 
			     (0 (setf (voxel-size (dose-grid pl))
				  *coarse-grid-size*)
				(setf (sl:label bt) "Coarse Grid"))
			     (1 (setf (voxel-size (dose-grid pl))
				  *medium-grid-size*)
				(setf (sl:label bt) "Medium Grid"))
			     (2 (setf (voxel-size (dose-grid pl))
				  *fine-grid-size*)
				(setf (sl:label bt) "Fine Grid"))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify p (sl:button-on gcolor-b)
		   #'(lambda (pln bt)
		       (let ((new-col (sl:popup-color-menu)))
			 (when new-col
			   (setf (display-color (dose-grid pln)) new-col)
			   (setf (sl:bg-color bt) new-col)))
		       (setf (sl:on bt) nil)))
    ;; initialize the max-dose display
    (when (valid-grid (sum-dose p)) (update-max-dose-display pp))
    (ev:add-notify pp (grid-status-changed (sum-dose p))
		   #'(lambda (pan sd v)
		       (declare (ignore v))
		       (if (valid-grid sd)
			   (update-max-dose-display pan)
			 (setf (sl:info max-dos-r) ""
			       (sl:info max-crd-r) ""))))
    (setf (beam-selector pp)
      (make-selector-panel sp-wd sp-ht
			   "Add a beam" (beams p)
			   'make-beam
			   #'(lambda (bm)
			       (make-beam-panel bm :plan-of p
						:patient-of pat))
			   :parent pp-win :font ppf
			   :ulc-x (+ dx btw 10)
			   :ulc-y dy
			   :use-color t))
    (setf (dose-selector pp)
      (make-selector-panel sp-wd sp-ht
			   "Add dose level" (dose-surfaces p)
			   #'(lambda (name)
			       (declare (ignore name))
			       (make-dose-surface :dose-grid (dose-grid p)
						  :result (sum-dose p)))
			   'make-dose-surface-panel
			   :parent pp-win :font ppf
			   :ulc-x (+ dx (* 2 btw) 40)
			   :ulc-y dy
			   :use-color t))
    (setf (view-selector pp)
      (make-selector-panel sp-wd sp-ht
			   "Add a view" (plan-views p)
			   #'(lambda (name)
			       (interactive-make-view
				name
				:beams (coll:elements (beams p))))
			   #'(lambda (vw)
			       (make-view-panel vw :plan-of p
						:patient-of pat))
			   :parent pp-win :font ppf
			   :ulc-x (+ dx (* 2 btw) 50 sp-wd)
			   :ulc-y dy))))

;;;------------------------------------------

(defmethod destroy :before ((pp plan-panel))

  "releases X resources used by this panel and its children."

  (ev:remove-notify pp (new-name (the-plan pp)))
  (ev:remove-notify pp (new-plan-by (the-plan pp)))
  (ev:remove-notify pp (new-time-stamp (the-plan pp)))
  (ev:remove-notify pp (grid-status-changed (sum-dose (the-plan pp))))
  (when (point-dose-panel pp) (destroy (point-dose-panel pp)))
  (when (brachy-panel pp) (destroy (brachy-panel pp)))
  (sl:destroy (plan-by-box pp))
  (sl:destroy (name-box pp))
  (sl:destroy (timestamp-box pp))
  (sl:destroy (comments-box pp))
  (sl:destroy (delete-b pp))
  (sl:destroy (comments-btn pp))
  (sl:destroy (copy-b pp))
  (sl:destroy (save-b pp))
  (sl:destroy (ckpt-b pp))
  (sl:destroy (brachy-b pp))
  (sl:destroy (point-b pp))
  (sl:destroy (sort-beams-btn pp))
  (sl:destroy (print-b pp))
  (sl:destroy (compute-btn pp))
  (sl:destroy (write-dose-btn pp))
  (sl:destroy (grid-size-btn pp))
  (sl:destroy (grid-color-btn pp))
  (sl:destroy (max-dos-rdt pp))
  (sl:destroy (max-coord-rdt pp))
  (destroy (beam-selector pp))
  (destroy (dose-selector pp))
  (destroy (view-selector pp))
  (sl:destroy (panel-frame pp)))

;;;------------------------------------------
;;; End.
