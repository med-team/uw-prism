;;;
;;; volume-editor
;;;
;;; The volume-editor is a drawing panel used to create and
;;; modify collections of organs, tumors and targets, plane by plane.
;;; It also handles points or marks that are not part of a contour, a
;;; task formerly handled separately by a 3d-point-editor.  The common
;;; code was formerly in a separate module called the easel.  The
;;; revision history of both is merged here.
;;;
;;; 10-Jul-1992 I. Kalet start and make many changes
;;;  2-Jul-1993 I. Kalet eliminate filmstrip ref view.
;;; 28-Jul-1993 J. Unger add name textline, make empty easel have at
;;; least one fs frame, redraw other contours in ce for new-scale.
;;; 31-Jul-1993 I. Kalet announce new-contours when updating the
;;; contours list in the pstruct being edited.
;;;  2-Aug-1993 J. Unger change color of contour in ce when organ
;;;  color is changed by user.  Fix CCNP bug (blew up if no contour to
;;;  copy), modify color-btn add-notify to change color of ce contour.
;;; 14-Oct-1993 I. Kalet move name and color to attribute editor, also
;;; cosmetic fixes in code
;;; 26-Oct-1993 I. Kalet coerce numeric data from z textline to float,
;;; and use mini-image-set cache from patient instead of computing
;;; them here.
;;; 28-Dec-1993 I. Kalet efficiency mods - eliminate ce-image-cache,
;;; other extra steps. Add support for larger contour editor with images.
;;; 10-Mar-1994 I. Kalet fix problem with regenerating NIL for deleted
;;; contours, change call to draw for pixmaps into function call to
;;; either draw-lines-pix or draw-image-pix
;;; 17-Mar-1994 I. Kalet fix CCNP to ignore current plane
;;; 12-May-1994 I. Kalet update raw image in contour editor to reflect
;;;  gray scale, not really raw image data.
;;; 17-May-1994 I. Kalet add new-org parameter to new-origin action fn.
;;; 29-May-1994 I. Kalet split off from old easel to eliminate
;;; redundant code with point editor.
;;; 30-May-1994 I. Kalet retain common code in easel, put rest into
;;; volume-editor, to eliminate redundancy with 3d-point-editor, add
;;; new-z event to control circularity at this level.  Make
;;; draw-lines-pix into a generic function draw-pix.  Set new pan-zoom
;;; flag, not the image slot in the planar editor.
;;;  2-Jun-1994 J. Unger add update-case announcement w/ new-contours.
;;;  5-Jun-1994 J. Unger announce new-contours if the 'delete contour' 
;;; button is pressed.
;;;  8-Jun-1994 I. Kalet take out grayscale mapping of data for
;;;  autocontour for now, also move attribute editor and buttons down,
;;;  add slice no. register.
;;;  8-Jan-1995 I. Kalet destroy slice-no textline too.
;;; 13-Jan-1995 I. Kalet made background pixmap here, so free it here.
;;; 10-Sep-1995 I. Kalet make *esl-default-pe-scale* a float, not fix
;;; 21-Jan-1997 I. Kalet eliminate refs to geometry package, use
;;; macros in misc instead.
;;;  2-Mar-1997 I. Kalet update calls to NEARLY- functions, change
;;;  title bar to read "Prism volume editor".  Change back to making a
;;;  mapped raw image for autocontour in the contour editor, instead
;;;  of unmapped.
;;; 22-Jun-1997 I. Kalet reduce globals usage, move CCNP button to easel
;;; from volume-editor, other mods to allow changing volume selected
;;; within the volume editor.  Make scale in PE default, no global,
;;; get value from PE when needed (this means, make the PE before
;;; using the scale value).
;;; 24-Jun-1997 I. Kalet don't use global params, turn on Accept
;;; button on Delete Contour, move organ, tumor and target selector
;;; panels here from patient panel, add an immob-dev slot for PTV, add
;;; a method for planar-editor-vertices, to be used in CCNP action,
;;; register CCNP action here, needs contours of selected volume, make
;;; sure filmstrip updates when pstruct added or deleted.
;;;  3-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx.
;;; 14-Jan-1998 I. Kalet move a bunch of filmstrip stuff to there, use
;;;  simpler interface to filmstrip, fix copy from nearest plane code.
;;; 27-Jan-1998 I. Kalet modifications for new organization of
;;;  filmstrip, new names for adding, deleting contours, etc.  Also,
;;;  fix up copy contour from nearest plane.
;;;  4-Jun-1998 I. Kalet make local-make-target default to manual if
;;;  the user presses the Cancel button.
;;; 25-Jun-1998 I. Kalet move free-pixmap for PE background to :after
;;; method for destroy.  Also protect Copy NP from crash when there
;;; are no contours to copy.
;;; 11-Mar-1999 I. Kalet adjustments to accomodate sliderboxes for
;;; window and level controls instead of textlines.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;;  5-Jan-2000 I. Kalet use new index-format parameter in
;;; make-filmstrip, relax plane match criterion for display.
;;;  2-Apr-2000 I. Kalet reset slice-no textline to blank when invalid
;;; slice number is entered.
;;; 12-Apr-2000 I. Kalet incorporate some of Lee Zeman's work on
;;; extended automatic contour generation.  Shrink button height to
;;; make room for more controls.
;;;  9-May-2000 I. Kalet continuing work on extended autocontour.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 27-Jun-2000 I. Kalet parametrize format of display of z values
;;; 20-Jul-2000 I. Kalet use OpenGL to display images, instead of CLX,
;;; don't disable pan and zoom in planar editor, modify setf pe-image
;;; to set new slots for contour-editor to handle magnified image display
;;;  2-Dec-2000 I. Kalet move select-1 to selector-panels
;;; 28-Jan-2001 I. Kalet correct coding error in use of textlines for
;;; zplus and zminus limits, in auto-extend subpanel and
;;; generate-extended-contours.
;;; 11-Mar-2001 I. Kalet set initial values of zplus and zminus in
;;; auto-extend panel based on extrema of image Z values.
;;;  6-Jan-2002 I. Kalet In CCNP, use nearest from misc instead of
;;; nearest-z from easel (gone).
;;; 14-Feb-2002 I. Kalet extend allowed slice numbers to 500
;;; 22-Feb-2004 I. Kalet merge point editor functionality here, and
;;; re-merge with easel instead of separate panel and modules.  Add
;;; point sorting, like beam sorting.  Move auto-extend-panel code to
;;; separate module.
;;;----------------- merged revision history from easel --------------
;;; 26-Oct-1993 I. Kalet coerce numeric data from z textline to float,
;;; and use mini-image-set cache from patient instead of computing
;;; them here.
;;; 28-Dec-1993 I. Kalet efficiency mods - eliminate ce-image-cache,
;;; other extra steps. Add support for larger contour editor with images.
;;; 10-Mar-1994 I. Kalet fix problem with regenerating NIL for deleted
;;; contours, change call to draw for pixmaps into function call to
;;; either draw-lines-pix or draw-image-pix
;;; 17-Mar-1994 I. Kalet fix CCNP to ignore current plane
;;; 12-May-1994 I. Kalet update raw image in contour editor to reflect
;;;  gray scale, not really raw image data.
;;; 17-May-1994 I. Kalet add new-org parameter to new-origin action fn.
;;; 30-May-1994 I. Kalet retain common code, put rest into
;;; volume-editor, to eliminate redundancy with 3d-point-editor, add
;;; new-z event to control circularity at this level.  Make
;;; draw-lines-pix into a generic function draw-pix.  Set new pan-zoom
;;; flag, not the image slot in the planar editor.
;;; 22-Jun-1997 I. Kalet reduce globals usage, move CCNP button here
;;; from volume-editor, other mods to allow changing volume selected
;;; within the volume editor.  Make scale in PE default, no global,
;;; get value from PE when needed (this means, make the PE before
;;; using the scale value).
;;; 14-Feb-2002 I. Kalet extend allowed slice numbers to 500
;;;-------------------------------------------------------------------
;;; 17-May-2004 I. Kalet continuing overhaul.  Move update-pstruct to
;;; autovolume to remove circularity.  Add fimlstrip as input to
;;; make-auto-extend-panel call
;;; 24-Jan-2005 I. Kalet change make-contour-editor to
;;; make-planar-editor, and other changes for the overhaul.
;;; 12-May-2005 I. Kalet finish (setf volume), start on (setf point)
;;; 25-Aug-2005 I. Kalet continue to finish up loose ends.
;;; 22-Jun-2007 I. Kalet add action for point selected in planar
;;; editor - just select the corresponding button in the point selector
;;;  3-Jan-2009 I. Kalet modify write-pe-background to use
;;; write-image-clx instead of write-image-gl, remove gl-buffer and
;;; add a slot for a scratch array for computing pan and zoom.
;;;  1-Jun-2009 I. Kalet use original images, not mini-images, in filmstrip
;;; 18-Jun-2009 I. Kalet clean up interface to auto-extend-panel
;;;

(in-package :prism)

;;;----------------------------------

(defclass volume-editor (generic-panel)

  ((bg-vols :type list
	    :accessor bg-vols
	    :initarg :bg-vols
	    :documentation "All the pstructs appearing in the
background of the point editor and filmstrip.")

   (images :type list 
           :accessor images
	   :initarg :images
           :documentation "The image study, a list of image-2D's, to
serve as backgrounds for the planar editor drawing region and the
filmstrip frames.")

   (window :type fixnum
           :accessor window
           :initarg :window
           :documentation "The grayscale window width of the image in
the planar editor's background.")

   (level :type fixnum
          :accessor level
          :initarg :level
          :documentation "The grayscale level value or center of the
window of the image in the planar editor's background.")

   (pe :type planar-editor
       :accessor pe
       :documentation "The planar editor associated with this easel.")

   (fs :type filmstrip
       :accessor fs
       :documentation "The filmstrip associated with this easel.")

   (z :type single-float
      :accessor z
      :initform 0.0 ;; need an initial value here, does not matter what
      :documentation "The z coordinate in real space to which the
easel is set.")

   (new-z :type ev:event
	  :accessor new-z
	  :initform (ev:make-event)
	  :documentation "Announced when the z attribute changes.")

   (z-tln ;; :type sl:textline
          :accessor z-tln
	  :documentation "The SLIK textline displaying the z value for
the current editing plane.")

   (slice-no :accessor slice-no
	     :documentation "The textline displaying the CT image
slice number of the currently displayed image.")

   (busy :type (member t nil)
	 :accessor busy
	 :initform nil
	 :documentation "The flag to control circularity among z
value, content of z textline, slice-no and index in filmstrip.")

   (pe-image :type image-2D
	     :accessor pe-image
	     :initform nil ;; can't start out with slot unbound
	     :documentation "The image to appear in the background of
the planar editor at the specified z level.  Could be a different size
than the images in the list.")

   (image-cache :accessor image-cache
		:initform nil
		:documentation "A pixel array produced by mapping the
current background image, if any, using the current window and level
values.  Saves a lot of computing when changing scale or background
data and recomputing the image is not needed.")

   (scaled-image :accessor scaled-image
		 :documentation "A pixel array used to compute a
temporary scaled image before writing it to the background clx pixmap.")

   (pe-volume-prims :type list
		    :accessor pe-volume-prims
		    :documentation "A list of graphic primitives
corresponding to the background contours to appear along with the
image in the planar editor background at the specified z level.")

   (pe-point-prims :type list
		   :accessor pe-point-prims
		   :documentation "A list of graphic primitives
corresponding to the background points to appear along with the image
and contours in the planar editor background at the specified z level.")

   (fr :type sl:frame
       :accessor fr
       :documentation "The SLIK frame that contains the easel.")

   (del-pnl-btn ;; :type sl:button
		:accessor del-pnl-btn
		:documentation "The delete panel button for the easel
panel.")

   (window-control :accessor window-control
		   :documentation "The textline that displays and sets
the window for the current editing plane.")

   (level-control :accessor level-control
		  :documentation "The textline that displays and sets
the level for the current editing plane.")

   (cpy-pts-btn ;; :type sl:button
		:accessor cpy-pts-btn
		:documentation "The copy points from nearest plane
button.")

   (immob-dev :accessor immob-dev
	      :initarg :immob-dev
	      :documentation "A copy of the immob-dev slot in the
patient, for use in the PTV tool, and kept consistent by a little one
way event registration when the panel is created.")

   (organ-coll :accessor organ-coll
	       :initarg :organ-coll
	       :documentation "The collection of anatomic volumes from
the patient case.")

   (tumor-coll :accessor tumor-coll
	       :initarg :tumor-coll
	       :documentation "The collection of tumor volumes from
the patient case.")

   (target-coll :accessor target-coll
		:initarg :target-coll
		:documentation "The collection of target volumes from
the patient case.")

   (point-coll :accessor point-coll
	       :initarg :point-coll
	       :documentation "The collection of points from the
	       patient case.")

   (volume :type pstruct
           :accessor volume
	   :initarg :volume
	   :initform nil
	   :documentation "The contoured volume currently being
edited.  It is represented by data in real space, i.e., coordinates in
cm, not in pixels.")

   (point :accessor point
	  :initarg :point
	  :initform nil
	  :documentation "The point that is the current editor focus,
	  selected from the points selector panel.")

   (del-con-btn	:accessor del-con-btn
		:documentation "The delete contour button.")

   (extend-btn :accessor extend-btn
	       :documentation "The button that puts up the auto-extend
subpanel for doing a whole series of autocontouring.")

   (auto-extend-subpanel :accessor auto-extend-subpanel
			 :initform nil
			 :documentation "The subpanel that provides
entry of data for extended autocontouring.")

   (organ-selector :accessor organ-selector
		   :documentation "The selector panel listing the
organs in the patient.")

   (tumor-selector :accessor tumor-selector
		   :documentation "The selector panel listing the
tumor volumes in the patient.")

   (target-selector :accessor target-selector
		    :documentation "The selector panel listing the
target volumes in the patient.")

   (point-selector :accessor point-selector
		   :documentation "The selector panel listing the
		   points in the patient.")

   )

  (:default-initargs :window 500 :level 1024 :bg-vols nil)

  (:documentation "The volume editor includes everything needed to
create and edit all the contoured volumes and points of interest in a
patient, from an image study.")

  )

;;;----------------------------------

(defun write-pe-background (vol-ed)

  "write-pe-background vol-ed

Renders the current image or writes a black pixmap into the planar
editor background, and draws the graphics primitives from the other
volumes and the points on top."

  (let* ((im (pe-image vol-ed))
	 (pe (pe vol-ed))
	 (px (background pe)))
    (if (image-cache vol-ed)
	(let* ((im-ppcm (pix-per-cm im))
	       (mag (/ (scale pe) im-ppcm))
	       (im-x0 (- (round (* (vx (origin im)) im-ppcm))))
	       (im-y0 (round (* (vy (origin im)) im-ppcm)))
	       (x0 (- im-x0 (/ (x-origin pe) mag)))
	       (y0 (- im-y0 (/ (y-origin pe) mag))))
	  (scale-image (image-cache vol-ed)
		       (scaled-image vol-ed) ;; scratch array to avoid gc
		       mag x0 y0)
	  (sl:write-image-clx (scaled-image vol-ed) px))
      (clx:draw-rectangle px (sl:color-gc 'sl:black) 0 0 
			  (clx:drawable-width px)
			  (clx:drawable-height px) t))
    (dolist (prim (pe-volume-prims vol-ed)) (draw-pix prim px))
    (if (and (volume vol-ed) (not (point vol-ed)))
	(dolist (prim (pe-point-prims vol-ed)) (draw-pix prim px)))))

;;;----------------------------------

(defun compute-volume-prims (vol-ed)

  "compute-volume-prims vol-ed

Computes the graphic primitives for the other-pstructs to be drawn in
the planar editor background."

  (let* ((pe (pe vol-ed))
	 (xorig (x-origin pe))
	 (yorig (y-origin pe))
	 (ppcm (scale pe))
	 (z (z vol-ed)))
    (declare (fixnum xorig yorig) (single-float ppcm z))
    (setf (pe-volume-prims vol-ed)
      (mapcar #'(lambda (vol) 
		  (let ((prim  (make-lines-prim
				nil (sl:color-gc (display-color vol))
				:object vol))) 
		    (dolist (con (contours vol) prim)
		      (when (poly:nearly-equal (z con) z
					       *display-epsilon*)
			(draw-transverse (vertices con)
					 prim xorig yorig ppcm)))))
	      (bg-vols vol-ed)))))

;;;----------------------------------

(defun compute-point-prims (vol-ed)

  "compute-point-prims vol-ed

computes the graphic primitives for the points to be drawn in the
planar editor background."

  (let* ((pe (pe vol-ed))
	 (xorig (x-origin pe))
	 (yorig (y-origin pe))
	 (ppcm (scale pe))
	 (z (z vol-ed)))
    (declare (fixnum xorig yorig) (single-float ppcm z))
    (setf (pe-point-prims vol-ed)
      (apply #'append
	     (mapcar #'(lambda (pt)
			 (if (poly:nearly-equal (z pt) z
						*display-epsilon*)
			     (let* ((color(sl:color-gc (display-color pt)))
				    (s-prim (make-segments-prim
					     nil color :object pt))
				    (c-prim (make-characters-prim
					     (write-to-string (id pt))
					     nil nil color :object pt)))
			       (multiple-value-bind
				   (hatchmarks x-anchor y-anchor)
				   (pixel-point (x pt) (y pt)
						ppcm xorig yorig)
				 (setf (points s-prim) hatchmarks)
				 (setf (x c-prim) x-anchor)
				 (setf (y c-prim) y-anchor))
			       (list s-prim c-prim))))
		     (coll:elements (point-coll vol-ed)))))))

;;;----------------------------------

(defmethod (setf z) :after (new-z (vol-ed volume-editor))

  "Updates the background image and graphics, also updates the z
  textline, the slice no. textline, and the contour editor vertices
  from the current volume."

  (setf (pe-image vol-ed) (find new-z (images vol-ed) 
			     :key #'(lambda (img) (vz (origin img)))
			     :test #'(lambda (a b)
				       (poly:nearly-equal
					a b *display-epsilon*))))
  (compute-volume-prims vol-ed)
  (compute-point-prims vol-ed)
  (write-pe-background vol-ed)
  (setf (sl:info (z-tln vol-ed)) (format nil *display-format* new-z))
  (setf (sl:info (slice-no vol-ed)) ;; put up slice number
    (if (pe-image vol-ed) (id (pe-image vol-ed)) "")) ;; or blank
  (ev:announce vol-ed (new-z vol-ed) new-z)
  (setf (vertices (pe vol-ed))
    (if (volume vol-ed) (aif (find new-z (contours (volume vol-ed)) 
				:key #'z
				:test #'(lambda (a b)
					  (poly:nearly-equal
					   a b *display-epsilon*)))
			  (vertices it))
      (remove new-z (coll:elements (point-coll vol-ed))
	      :key #'z 
	      :test-not #'(lambda (a b)
			    (poly:nearly-equal
			     a b *display-epsilon*))))))

;;;----------------------------------

(defmethod (setf volume) :before (new-vol (vol-ed volume-editor))

  "Disconnects the old volume, if necessary."

  (let ((old-vol (volume vol-ed)))
    (when old-vol
      (unless new-vol
	(setf (contour-mode (pe vol-ed)) nil))
      (ev:remove-notify vol-ed (new-color old-vol))
      ;; Find old-vol in organs, tumors or targets and deselect
      ;; it, if it is in a different collection.  Within the same
      ;; collection, the radio selector panel already does it.
      ;; This will destroy the old attribute editor for that volume.
      (unless (eq (type-of old-vol) (type-of new-vol))
	(let ((sp (typecase old-vol
		    (organ (organ-selector vol-ed))
		    (tumor (tumor-selector vol-ed))
		    (target (target-selector vol-ed)))))
	  (sl:deselect-button (button-for old-vol sp)
			      (scroll-list sp)))))))

;;;----------------------------------

(defmethod (setf volume) :after (new-vol (vol-ed volume-editor))

  "Updates the planar editor background and vertices, and connections
to the new volume if there is one, after the old one has been
deselected.  The selector panel creates and places the attribute editor."

  (setf (bg-vols vol-ed)
    (remove new-vol (append (coll:elements (organ-coll vol-ed))
			    (coll:elements (tumor-coll vol-ed))
			    (coll:elements (target-coll vol-ed)))))
  (compute-volume-prims vol-ed)
  (compute-point-prims vol-ed)
  (if new-vol
      (progn
	(write-pe-background vol-ed)
	(setf (color (pe vol-ed)) (sl:color-gc (display-color new-vol)))
	(ev:add-notify vol-ed (new-color new-vol)
		       #'(lambda (eas vol col)
			   (let ((col-gc (sl:color-gc col)))
			     (setf (color (pe eas)) col-gc)
			     (fs-set-color vol col-gc (fs eas)))))
	(let ((temp-con (find (z vol-ed) (contours new-vol)
			      :key #'z
			      :test #'(lambda (a b)
					(poly:nearly-equal
					 a b *display-epsilon*)))))
	  (setf (vertices (pe vol-ed))
	    (if temp-con (vertices temp-con) nil))))
    (setf (vertices (pe vol-ed)) nil)))

;;;----------------------------------

(defmethod (setf point) :before (new-pt (vol-ed volume-editor))

  "Disconnects the old point, if necessary."

  (let ((old-pt (point vol-ed)))
    (when old-pt
      (ev:remove-notify vol-ed (new-color old-pt))
      (when (not new-pt)
	;; going from point to volume, so need to...
	(setf (contour-mode (pe vol-ed)) t)
	(sl:deselect-button (button-for old-pt
					(point-selector vol-ed))
			    (scroll-list (point-selector vol-ed)))))))

;;;----------------------------------

(defmethod (setf point) :after (new-pt (vol-ed volume-editor))

  "Updates the planar editor background and vertices, and connections
to the new point, if there is one, after the old one has been
deselected.  The selector panel creates and places the attribute editor."

  (compute-point-prims vol-ed)
  (if new-pt
      (progn
	(write-pe-background vol-ed)
	(setf (color (pe vol-ed)) (sl:color-gc (display-color new-pt)))
	(ev:add-notify vol-ed (new-color new-pt)
		       #'(lambda (eas pt col)
			   (let ((col-gc (sl:color-gc col)))
			     (setf (color (pe eas)) col-gc)
			     (fs-set-color pt col-gc (fs eas)))))
	(setf (vertices (pe vol-ed))
	  (remove (z vol-ed) (coll:elements (point-coll vol-ed))
		  :key #'z 
		  :test-not #'(lambda (a b)
				(poly:nearly-equal
				 a b *display-epsilon*)))))
    (setf (vertices (pe vol-ed)) nil)))

;;;----------------------------------

(defmethod (setf pe-image) :after (new-image (vol-ed volume-editor))

  "sets the image cache to the gray mapped version of the new image,
or nil, and updates the contour editor with the new image data"

  (setf (image-cache vol-ed)
    (if new-image (sl:map-image (sl:make-graymap (window vol-ed)
						 (level vol-ed)
						 (range new-image))
				(pixels new-image))
      nil))
  (let ((pe (pe vol-ed)))
    (if new-image
	(progn
	  (setf (image pe)
	    ;; without mapping, just (pixels new-image) here, make the
	    ;; type of the image slot in the contour-editor
	    ;; (unsigned-byte 16) and comment out the (setf window) and
	    ;; (setf level) after methods below.
	    ;; With mapping the image slot in the contour editor should
	    ;; be (unsigned-byte 8) and the setf methods are needed.
	    (sl:map-raw-image (pixels new-image)
			      (window vol-ed)
			      (level vol-ed)
			      (range new-image)
			      (image (pe vol-ed))))
	  (setf (img-x0 pe) (- (round (* (vx (origin new-image))
					 (pix-per-cm new-image))))
		(img-y0 pe) (round (* (vy (origin new-image))
				      (pix-per-cm new-image)))
		(img-ppcm pe) (pix-per-cm new-image)))
      (setf (image pe) nil))))

;;;----------------------------------
;;; the following function should also handle updating the filmstrip images
;;;----------------------------------

(defun window-level-update (vol-ed)

  "updates the image cache and the displayed image in the planar editor"

  (let ((im (pe-image vol-ed)))
    (when im
      (setf (image-cache vol-ed) ;; used for display
	(sl:map-image (sl:make-graymap (window vol-ed) (level vol-ed)
				       (range im))
		      (pixels im)))
      (write-pe-background vol-ed)
      (display-planar-editor (pe vol-ed))
      (setf (image (pe vol-ed)) ;; used by autocontour
	(sl:map-raw-image (pixels im) (window vol-ed) (level vol-ed)
			  (range im) (image (pe vol-ed)))))))

;;;----------------------------------
  
(defmethod (setf window) :after (new-window (vol-ed volume-editor))

  (declare (ignore new-window))
  (window-level-update vol-ed))

(defmethod (setf level) :after (new-level (vol-ed volume-editor))

  (declare (ignore new-level))
  (window-level-update vol-ed))

;;;----------------------------------

(defun update-points (pt-pan new-points)

  "update-points pt-pan new-points

Updates the points in the point collection at the current z value in
the point editor panel pt-pan (old points), from the list of
new-points which were returned from editing.  Each old point for which
there is a point in new-points with the same ID attribute, is updated
from the new one.  Each old point with no corresponding point on
new-points (same ID), is deleted from the point collection.  Each
point in new-points with no corresponding old point of the same ID, is
added to the point collection.  This function also maintains the
scrolling list of buttons in the panel."

  (let* ((point-coll (point-coll pt-pan))
	 (old-points (remove (z pt-pan) (coll:elements point-coll)
			     :key #'z
			     :test-not #'(lambda (a b)
					   (poly:nearly-equal
					    a b *display-epsilon*)))))
    (dolist (old old-points)
      (let ((new (find (id old) new-points :key #'id)))
	;; don't assign values over if they haven't changed - minimizes
	;; screen refreshes & dose pt invalidation triggered by new-loc
	;; announcements
	(if new
	    (progn
	      (unless (poly:nearly-equal (x old) (x new))
		(setf (x old) (x new)))
	      (unless (poly:nearly-equal (y old) (y new))
		(setf (y old) (y new)))
	      (unless (poly:nearly-equal (z old) (z new))
		(setf (z old) (z new)))
	      (unless (string= (name old) (name new))
		(setf (name old) (name new)))
	      (unless (eq (display-color old) (display-color new))
		(setf (display-color old) (display-color new))))
	  (coll:delete-element old point-coll))))
    (dolist (new new-points)
      (unless (find (id new) old-points :key #'id)
	(coll:insert-element new point-coll)))))

;;;----------------------------------

(defmethod initialize-instance :after ((vol-ed volume-editor)
				       &rest initargs
				       &key width &allow-other-keys)

  "Initializes the user interface for the volume editor panel."

  (let* ((img-size *easel-size*)
         (btw 150) ;; button width
	 (bth 25) ;; button height
	 (smf (symbol-value *small-font*)) ;; the value, not the symbol
	 (dx 5) ;; margin and spacing
	 (fsh (+ *mini-image-size* bth)) ;; filmstrip height
	 (frm (apply #'sl:make-frame
		     ;; allow width to be supplied, if not, use default
		     (if width width (+ *easel-size* btw (* 2 dx)))
		     ;; in height, allow for planar editor controls
		     (+ *easel-size* fsh bth (* 2 dx))
		     :font smf :title "Prism Volume Editor" initargs))
         (frm-win (sl:window frm))
	 (frm-width (sl:width frm))
         (start-y (+ fsh dx))
         (del-pnl-b (apply #'sl:make-button (- (/ btw 2) 3) bth
			   :parent frm-win :font smf
			   :ulc-x dx :ulc-y start-y
			   :label "Del Pan"
			   :button-type :momentary
			   initargs))
         (z-t (apply #'sl:make-textline (- (/ btw 2) 2) bth
		     :parent frm-win :font smf
		     :ulc-x (+ dx (/ btw 2) 2) :ulc-y start-y
		     :label "Z: "
		     :numeric t :lower-limit -100.0 :upper-limit 100.0
		     initargs))
         (slice-t (apply #'sl:make-textline btw bth
			 :parent frm-win :font smf
			 :ulc-x dx :ulc-y (bp-y start-y bth 1)
			 :label "Slice no: "
			 :numeric t :lower-limit 0 :upper-limit 500
			 initargs))
         (win-sl (apply #'sl:make-sliderbox
			btw bth 1.0 2047.0 9999.0
			:parent frm-win :font smf
			:label "Win: "
			:ulc-x (- dx 5) :ulc-y (- (bp-y start-y bth 2) 5)
			:border-width 0 :display-limits nil
			initargs))
	 (lev-sl (apply #'sl:make-sliderbox
			btw bth 1.0 4095.0 9999.0
			:parent frm-win :font smf
			:label "Lev: "
			:ulc-x (- dx 5) :ulc-y (- (bp-y start-y bth 4) 5)
			:border-width 0 :display-limits nil
			initargs))
	 (del-con-b (apply #'sl:make-button (- (/ btw 2) 3) bth
			   :parent frm-win :font smf
			   :ulc-x dx :ulc-y (bp-y start-y bth 6)
			   :label "Del Cont" :button-type :momentary
			   initargs))
         (cpy-b (apply #'sl:make-button (- (/ btw 2) 2) bth
		       :parent frm-win :font smf
		       :ulc-x (+ dx (/ btw 2) 2) :ulc-y (bp-y start-y bth 6)
		       :label "Copy NP" :button-type :momentary
		       initargs))
         (extend-con-b (apply #'sl:make-button btw bth
			      :parent frm-win :font smf
			      :ulc-x dx :ulc-y (bp-y start-y bth 7)
			      :label "Extended Auto Mode"
			      initargs))
	 (sp-width 150) ;; size parameters for selector panels
	 (sp-height 175)) ;; ...not the same everywhere
    (setf (fr vol-ed) frm
	  (del-pnl-btn vol-ed) del-pnl-b
	  (z-tln vol-ed) z-t
	  (slice-no vol-ed) slice-t
	  (window-control vol-ed) win-sl
	  (level-control vol-ed) lev-sl
	  (del-con-btn vol-ed) del-con-b
	  (cpy-pts-btn vol-ed) cpy-b
	  (extend-btn vol-ed) extend-con-b)
    (setf (fs vol-ed)
      (make-filmstrip (clx:drawable-width frm-win) fsh
		      :parent frm-win
		      :images (images vol-ed)
		      :window (window vol-ed)
		      :level (level vol-ed)
		      :index-format *display-format*))
    (ev:add-notify vol-ed (sl:button-on del-pnl-b)
		   #'(lambda (vol-ed a)
		       (declare (ignore a))
		       (destroy vol-ed)))
    (ev:add-notify vol-ed (new-index (fs vol-ed))
		   #'(lambda (vol-ed a fs-z)
		       (declare (ignore a))
		       (unless (busy vol-ed)
			 (setf (busy vol-ed) t)
			 (setf (z vol-ed) fs-z)
			 (setf (busy vol-ed) nil))))
    (ev:add-notify vol-ed (new-z vol-ed)
		   #'(lambda (vol-ed a zz)
		       (declare (ignore a))
		       (unless (busy vol-ed)
			 (setf (busy vol-ed) t)
			 (setf (index (fs vol-ed)) zz)
			 (setf (busy vol-ed) nil))))
    (ev:add-notify vol-ed (sl:new-info z-t)
		   #'(lambda (vol-ed a info)
		       (declare (ignore a))
		       (setf (z vol-ed) (coerce (read-from-string info)
					     'single-float))))
    (ev:add-notify vol-ed (sl:new-info slice-t)
		   #'(lambda (vol-ed a info)
		       (declare (ignore a))
		       (let* ((sn (read-from-string info))
			      (im (find sn (images vol-ed) :key #'id)))
			 (if im (setf (z vol-ed) (vz (origin im)))
			   (progn
			     (sl:acknowledge "No such slice number")
			     (setf (sl:info (slice-no vol-ed)) ""))))))
    (setf (sl:setting (window-control vol-ed))
      (coerce (window vol-ed) 'single-float))
    (ev:add-notify vol-ed (sl:value-changed (window-control vol-ed))
		   #'(lambda (pan wc win)
		       (declare (ignore wc))
		       (setf (window pan) (round win))))
    (setf (sl:setting (level-control vol-ed))
      (coerce (level vol-ed) 'single-float))
    (ev:add-notify vol-ed (sl:value-changed (level-control vol-ed))
		   #'(lambda (pan lc lev)
		       (declare (ignore lc))
		       (setf (level pan) (round lev))))
    ;; make up local functions for use with selector panels
    (flet ((make-vol-panel (vol) ;; works for all three types
	     ;; order here is important - remove old, set new
	     (if (point vol-ed) (setf (point vol-ed) nil))
	     (setf (volume vol-ed) vol)
	     (make-attribute-editor vol
				    :parent frm-win :font smf
				    :width btw
				    :ulc-x dx :ulc-y 485))
	   (make-point-panel (pt)
	     ;; order here is important - remove old, set new
	     (if (volume vol-ed) (setf (volume vol-ed) nil))
	     (setf (point vol-ed) pt)
	     (if (not (poly:nearly-equal (z pt) (z vol-ed)))
		 (setf (z vol-ed) (z pt)))
	     (make-attribute-editor pt
				    :parent frm-win :font smf
				    :width btw
				    :ulc-x dx :ulc-y 485))
	   (local-make-target (name) ;; easier to read if here
	     (let ((choice 0) 
		   (tumors (coll:elements (tumor-coll vol-ed))))
	       (when (and tumors
			  (some #'(lambda (tum)
				    (> (length (contours tum)) 1)) 
				tumors))
		 (setq choice
		   (sl:popup-menu '("Manual editing with easel"
				    "Tri-Linear expansion"
				    "Planning Target Volume Tool")
				  :title "Target initialization")))
	       (case choice
		 ((0 nil) (make-target name))
		 (1 (make-lin-expanded-target (tumor-coll vol-ed)))
		 (2 (make-ptv-expanded-target (immob-dev vol-ed)
					      (organ-coll vol-ed)
					      (tumor-coll vol-ed))))))
	   (local-make-point (name)
	     (prog1 (make-point name :x 0.0 :y 0.0 :z (z vol-ed)
				:id (next-mark-id (pe vol-ed)))
	       (incf (next-mark-id (pe vol-ed))))))
      (setf (organ-selector vol-ed)
	(make-selector-panel sp-width sp-height
			     "Add an organ" (organ-coll vol-ed)
			     #'make-organ
			     #'make-vol-panel
			     :parent frm-win
			     :use-color t :radio t
			     :ulc-x (- frm-width dx sp-width)
			     :ulc-y start-y))
      (setf (tumor-selector vol-ed)
	(make-selector-panel sp-width 125
			     "Add a tumor" (tumor-coll vol-ed)
			     #'make-tumor
			     #'make-vol-panel
			     :parent frm-win
			     :use-color t :radio t
			     :ulc-x (- frm-width dx sp-width)
			     :ulc-y (+ start-y dx sp-height)))
      (setf (target-selector vol-ed)
	(make-selector-panel sp-width 125
			     "Add a target" (target-coll vol-ed)
			     #'local-make-target ;; see above
			     #'make-vol-panel
			     :parent frm-win
			     :use-color t :radio t
			     :ulc-x (- frm-width dx sp-width)
			     :ulc-y (+ start-y (* 2 dx) sp-height 125)))
      (setf (point-selector vol-ed)
	(make-selector-panel sp-width sp-height
			     "Add a point" (point-coll vol-ed)
			     #'local-make-point
			     #'make-point-panel
			     :parent frm-win
			     :use-color t :radio t
			     :ulc-x (- frm-width dx sp-width)
			     :ulc-y (+ start-y (* 3 dx) sp-height 250))))
    (flet ((add-pstr (pan coll str)
	     (declare (ignore coll))
	     (push str (bg-vols pan))
	     (dolist (con (contours str))
	       (fs-add-contour str con (fs pan))))
	   (rem-pstr (pan coll str)
	     (declare (ignore coll))
	     (setf (bg-vols pan) (remove str (bg-vols pan)))
	     (compute-volume-prims pan)
	     (write-pe-background pan)
	     (display-planar-editor (pe vol-ed))
	     (dolist (con (contours str))
	       (fs-delete-contour str (z con) (fs pan))))
	   (add-fs-pt (pan coll pt)
	     (declare (ignore pan coll pt))
	     ;; to be done
	     )
	   (rem-fs-pt (pan coll pt)
	     (declare (ignore pan coll pt))
	     ;; to be done
	     ))
      (ev:add-notify vol-ed (coll:inserted (organ-coll vol-ed)) #'add-pstr)
      (ev:add-notify vol-ed (coll:inserted (tumor-coll vol-ed)) #'add-pstr)
      (ev:add-notify vol-ed (coll:inserted (target-coll vol-ed)) #'add-pstr)
      (ev:add-notify vol-ed (coll:inserted (point-coll vol-ed)) #'add-fs-pt)
      (ev:add-notify vol-ed (coll:deleted (organ-coll vol-ed)) #'rem-pstr)
      (ev:add-notify vol-ed (coll:deleted (tumor-coll vol-ed)) #'rem-pstr)
      (ev:add-notify vol-ed (coll:deleted (target-coll vol-ed)) #'rem-pstr)
      (ev:add-notify vol-ed (coll:deleted (point-coll vol-ed)) #'rem-fs-pt))
    (setf (pe vol-ed)
      (make-planar-editor ;; just take default color and scale at first
       :parent frm-win
       :ulc-x (+ btw (* 2 dx)) :ulc-y fsh
       :image nil ;; this will get set when the easel z is set
       :background (sl:make-square-pixmap img-size t frm-win)
       :x-origin (round (/ img-size 2))
       :y-origin (round (/ img-size 2))
       :next-mark-id (1+ (aif (coll:elements (point-coll vol-ed))
			      (apply #'max (mapcar #'id it))
			      0))
       ))
    (setf (scaled-image vol-ed) (make-array (list img-size img-size)
					    :element-type
					    '(unsigned-byte 32)))
    ;; add volumes to filmstrip
    (dolist (vol (append (coll:elements (organ-coll vol-ed))
			 (coll:elements (tumor-coll vol-ed))
			 (coll:elements (target-coll vol-ed))))
      (dolist (con (contours vol))
	(fs-add-contour vol con (fs vol-ed))))
    ;; add points to filmstrip
    (dolist (pt (coll:elements (point-coll vol-ed)))
      (fs-replace-points nil (list pt) (z pt) (fs vol-ed)))
    (ev:add-notify vol-ed (sl:button-on (cpy-pts-btn vol-ed))
		   #'(lambda (vol-ed1 bt)
		       (aif (nearest (z vol-ed1)
				     (mapcar #'z
					     (if (volume vol-ed1)
						 (contours (volume vol-ed1))
					       (coll:elements
						(point-coll vol-ed1))))
				     *display-epsilon*)
			    (progn
			      (setf (vertices (pe vol-ed1))
				(if (volume vol-ed1)
				    ;; make fresh lists for copied contour
				    (copy-tree
				     (vertices (find it (contours
							 (volume vol-ed1))
						     :test #'= :key #'z)))
				  (append ;; add from nearest, not replace
				   (vertices (pe vol-ed1))
				   (remove nil
					   (mapcar
					    #'(lambda (pt)
						(if (poly:nearly-equal
						     it (z pt)
						     *display-epsilon*)
						    (prog1 ;; need new one!
							(make-point
							 ""
							 :x (x pt)
							 :y (y pt)
							 :z (z vol-ed1)
							 :id (next-mark-id
							      (pe vol-ed1)))
						      (incf (next-mark-id
							     (pe vol-ed1))))))
					    (coll:elements
					     (point-coll vol-ed1)))))))
			      (unless (sl:on (accept-btn (pe vol-ed1)))
				(setf (sl:on (accept-btn (pe vol-ed1))) t)))
			    (sl:acknowledge "No nearest contour or points"))
		       (if (sl:on bt) (setf (sl:on bt) nil))))
    (ev:add-notify vol-ed (sl:button-on del-con-b)
		   #'(lambda (vol-ed1 bt)
		       (declare (ignore bt))
		       (when (volume vol-ed1) ;; no action for points
			 (setf (vertices (pe vol-ed1)) nil) ;; do this first
			 (update-pstruct (volume vol-ed1) nil (z vol-ed1))
			 (fs-delete-contour (volume vol-ed1)
					    (z vol-ed1) (fs vol-ed1)))))
    (ev:add-notify vol-ed (sl:button-on (extend-btn vol-ed))
		   #'(lambda (vol-ed1 bt)
		       (if (eql (edit-mode (pe vol-ed1)) :automatic)
			   (setf (auto-extend-subpanel vol-ed1)
			     (make-auto-extend-panel
			      vol-ed1
			      5 (bp-y (+ (height (fs vol-ed1)) 5) bth 8)))
			 (progn
			   (sl:acknowledge 
			    '("Multi-slice drawing possible" 
			      "only in Automatic mode"))
			   (setf (sl:on bt) nil)))))
    (ev:add-notify vol-ed (sl:button-off (extend-btn vol-ed))
		   #'(lambda (vol-ed1 bt)
		       (declare (ignore bt))
		       (when (auto-extend-subpanel vol-ed1)
			 (destroy (auto-extend-subpanel vol-ed1))
			 (setf (auto-extend-subpanel vol-ed1) nil))))
    (ev:add-notify vol-ed (new-vertices (pe vol-ed))
		   #'(lambda (vol-ed1 a new-verts)
		       (declare (ignore a))
		       (if (volume vol-ed1)
			   (progn
			     (update-pstruct (volume vol-ed1) new-verts
					     (z vol-ed1))
			     (fs-delete-contour (volume vol-ed1)
						(z vol-ed1) (fs vol-ed1))
			     (fs-add-contour (volume vol-ed1)
					     (make-contour :z (z vol-ed1)
							   :vertices
							   new-verts)
					     (fs vol-ed1))
			     (when (sl:on (extend-btn vol-ed1))
			       (generate-extended-contours
				(auto-extend-subpanel vol-ed1) new-verts)))
			 (progn ;; add z coords to new points
			   (dolist (pt new-verts) (setf (z pt) (z vol-ed1)))
			   (fs-replace-points ;; do this before update
			    (remove (z vol-ed1)
				    (coll:elements (point-coll vol-ed1))
				    :key #'z
				    :test-not #'(lambda (a b)
						  (poly:nearly-equal
						   a b *display-epsilon*)))
			    new-verts (z vol-ed1) (fs vol-ed1))
			   (update-points vol-ed1 new-verts)))))
    ;; select first avail pstruct or make a new organ to edit
    (cond ((select-1 (organ-selector vol-ed)))
	  ((select-1 (tumor-selector vol-ed)))
	  ((select-1 (target-selector vol-ed)))
	  (t (let ((sel-pan (organ-selector vol-ed)))
	       (setf (sl:on (add-button sel-pan)) t) ;; adds a new organ
	       (setf (sl:on (add-button sel-pan)) nil))))
    (ev:add-notify vol-ed (new-scale (pe vol-ed))
		   #'(lambda (vol-ed1 a new-sc)
		       (declare (ignore a new-sc))
		       (compute-volume-prims vol-ed1)
		       (compute-point-prims vol-ed1)
		       (write-pe-background vol-ed1)))
    (ev:add-notify vol-ed (new-origin (pe vol-ed))
		   #'(lambda (vol-ed1 a new-org)
		       (declare (ignore a new-org))
		       (compute-volume-prims vol-ed1)
		       (compute-point-prims vol-ed1)
		       (write-pe-background vol-ed1)))
    (ev:add-notify vol-ed (pt-selected (pe vol-ed))
		   #'(lambda (vol-ed1 pl-ed pt)
		       (declare (ignore pl-ed))
		       ;; select the point pt in the selector panel
		       (let ((selpan (point-selector vol-ed1)))
			 (sl:select-button
			  (button-for pt selpan)
			  (scroll-list selpan)))))
    (setf (z vol-ed) (or (index (fs vol-ed)) 0.0))
    (sl:flush-output)))

;;;----------------------------------

(defun make-volume-editor (&rest initargs)
 
  "make-volume-editor &rest initargs

Returns a volume editor with the specified parameters."

  (apply #'make-instance 'volume-editor initargs))

;;;----------------------------------

(defmethod destroy :before ((vol-ed volume-editor))

  "Releases X resources used by this panel and its children."

  (ev:remove-notify vol-ed (coll:inserted (organ-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:inserted (tumor-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:inserted (target-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:inserted (point-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:deleted (organ-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:deleted (tumor-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:deleted (target-coll vol-ed)))
  (ev:remove-notify vol-ed (coll:deleted (point-coll vol-ed)))
  (if (volume vol-ed)
      (ev:remove-notify vol-ed (new-color (volume vol-ed))))
  (if (point vol-ed)
      (ev:remove-notify vol-ed (new-color (point vol-ed))))

  ;; possibly more here - check

  (if (sl:on (extend-btn vol-ed)) (setf (sl:on (extend-btn vol-ed)) nil))
  (sl:destroy (extend-btn vol-ed))
  (sl:destroy (del-con-btn vol-ed))
  (destroy (organ-selector vol-ed))
  (destroy (tumor-selector vol-ed))
  (destroy (target-selector vol-ed))
  (destroy (point-selector vol-ed))
  (sl:destroy (del-pnl-btn vol-ed))
  (sl:destroy (z-tln vol-ed))
  (sl:destroy (slice-no vol-ed))
  (sl:destroy (window-control vol-ed))
  (sl:destroy (level-control vol-ed))
  (sl:destroy (cpy-pts-btn vol-ed))
  (destroy (fs vol-ed))
  (destroy (pe vol-ed))
  (sl:destroy (fr vol-ed)))

;;;----------------------------------

(defmethod destroy :after ((vol-ed volume-editor))

  (clx:free-pixmap (background (pe vol-ed)))) ;; made here, free here

;;;----------------------------------
;;; End.
