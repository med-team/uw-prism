;;;
;;; quadtree
;;;
;;; This module provides the quadtree representation of a two
;;; dimensional data structure.
;;;
;;; 13-Jun-1998 P. Cho
;;; 28-Mar-1999 I. Kalet cosmetic and other fixes.
;;; 03-Feb-2000 BobGian rename NODE -> QNODE for clarity; cosmetic fixes.
;;;   Avoid two global vars by passing/returning info to/from functions.
;;; 02-Mar-2000 BobGian rename arg in MERGE-NODES for clarity.
;;; 02-Nov-2000 BobGian function name and argument order changes to make
;;;   consistent with new version of dose-calc used in electron code -
;;;   quadtree function now takes EFLIST and ARG-VEC and calls ENCLOSES?
;;;   explicitly rather than taking closure as enclose-testing function.
;;; 30-May-2001 BobGian:
;;;   Wrap generic arithmetic with THE-declared types.
;;;   Move DEFSTRUCTs for QNODE and TILE to "dosecomp-decls".
;;; 03-Jan-2003 BobGian change structures to arrays (inlined accessors
;;;   and new declarations).
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :prism)

;;;=============================================================
;;; QUADTREE: generates quadtree structure by subdividing the
;;;           root node until unit node size is reached, then
;;;           merges nodes that are inside the electron field
;;;=============================================================

(defun quadtree (root current-node-size eflist arg-vec
		 &aux (dim (qnode-dimension root))
		 (dim/2 (* 0.5 dim)) (dim/4 (* 0.25 dim)))

  "quadtree root current-node-size eflist arg-vec

generates quadtree structure by subdividing the root node until unit node
size is reached, then merges nodes that are inside the boundary EFLIST."

  (declare (type (simple-array t (8)) root)
	   (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type list eflist)
	   (type single-float dim dim/2 dim/4)
	   (type fixnum current-node-size))

  (cond ((> current-node-size 1)         ; If not leaf, divide root node by 4.
	 (setq current-node-size (ash current-node-size -1))

	 ;;Divide parent node by 4
	 ;;1 of Four
	 (setf (qnode-child1 root)
	       (make-qnode (+ (the single-float (qnode-xpos root)) dim/4)
			   (+ (the single-float (qnode-ypos root)) dim/4)
			   dim/2))
	 (quadtree (qnode-child1 root) current-node-size eflist arg-vec)

	 ;;2 of Four
	 (setf (qnode-child2 root)
	       (make-qnode (+ (the single-float (qnode-xpos root)) dim/4)
			   (- (the single-float (qnode-ypos root)) dim/4)
			   dim/2))
	 (quadtree (qnode-child2 root) current-node-size eflist arg-vec)

	 ;;3 of Four
	 (setf (qnode-child3 root)
	       (make-qnode (- (the single-float (qnode-xpos root)) dim/4)
			   (+ (the single-float (qnode-ypos root)) dim/4)
			   dim/2))
	 (quadtree (qnode-child3 root) current-node-size eflist arg-vec)

	 ;;4 of Four
	 (setf (qnode-child4 root)
	       (make-qnode (- (the single-float (qnode-xpos root)) dim/4)
			   (- (the single-float (qnode-ypos root)) dim/4)
			   dim/2))
	 (quadtree (qnode-child4 root) current-node-size eflist arg-vec)

	 (merge-qnodes root))                     ; Merge siblings if possible

	;; Leaf node - assign status according to enclosure test.
	(t (setf (aref arg-vec #.Argv-Enc-X) (qnode-xpos root))
	   (setf (aref arg-vec #.Argv-Enc-Y) (qnode-ypos root))
	   (setf (qnode-status root)
		 (if (encloses? eflist arg-vec)
		     :Inside :Outside)))))

;;;-------------------------------------------------------------

(defun count-qnodes (tree)

  "count-qnodes tree

Returns the number of nodes with status inside."

  (if tree
      (the fixnum
	(+ (if (eq (qnode-status tree) :Inside) 1 0)
	   (the fixnum
	     (+ (the fixnum
		  (+ (the fixnum (count-qnodes (qnode-child1 tree)))
		     (the fixnum (count-qnodes (qnode-child2 tree)))))
		(the fixnum
		  (+ (the fixnum (count-qnodes (qnode-child3 tree)))
		     (the fixnum (count-qnodes (qnode-child4 tree)))))))))
      0))

;;;-------------------------------------------------------------

(defun traverse-tree (root tiles nquad &aux child)

  "traverse-tree root tiles nquad

Traverse tree root and store information in an array tiles.
Note: Merged nodes are represented by square tiles of different
sizes.  Tile-dimension is the half-width of the square tile.
Returns NQUAD."

  (declare (type (simple-array t (8)) root)
	   (type (simple-array t 1) tiles)
	   (type fixnum nquad))

  (when (eq (qnode-status root) :Inside)
    (setf (aref tiles nquad)
	  (make-tile (qnode-xpos root)
		     (qnode-ypos root)
		     (* 0.5 (the single-float (qnode-dimension root)))))
    (setq nquad (the fixnum (1+ nquad))))

  ;; Recurse if children exist.
  (when (setq child (qnode-child1 root))
    (setq nquad (traverse-tree child tiles nquad)))
  (when (setq child (qnode-child2 root))
    (setq nquad (traverse-tree child tiles nquad)))
  (when (setq child (qnode-child3 root))
    (setq nquad (traverse-tree child tiles nquad)))
  (when (setq child (qnode-child4 root))
    (setq nquad (traverse-tree child tiles nquad)))

  nquad)

;;;-------------------------------------------------------------

(defun merge-qnodes (parent)

  "merge-qnodes parent

merge nodes that are inside the region, e.g. if all four children are
inside, their node will be assigned inside and the children
removed."

  (cond ((and (eq (qnode-status (qnode-child1 parent)) :Inside)
	      (eq (qnode-status (qnode-child2 parent)) :Inside)
	      (eq (qnode-status (qnode-child3 parent)) :Inside)
	      (eq (qnode-status (qnode-child4 parent)) :Inside))
	 (setf (qnode-status parent) :Inside)
	 (setf (qnode-child1 parent) nil)
	 (setf (qnode-child2 parent) nil)
	 (setf (qnode-child3 parent) nil)
	 (setf (qnode-child4 parent) nil))

	;;If all four children are Outside, their parent will be
	;; Outside and their children are removed.
	((and (eq (qnode-status (qnode-child1 parent)) :Outside)
	      (eq (qnode-status (qnode-child2 parent)) :Outside)
	      (eq (qnode-status (qnode-child3 parent)) :Outside)
	      (eq (qnode-status (qnode-child4 parent)) :Outside))
	 (setf (qnode-status parent) :Outside)
	 (setf (qnode-child1 parent) nil)
	 (setf (qnode-child2 parent) nil)
	 (setf (qnode-child3 parent) nil)
	 (setf (qnode-child4 parent) nil))

	;;If the children cannot be merged, leave them alone.
	;; Assign their parent :Cantmerge.
	(t (setf (qnode-status parent) :Cantmerge))))

;;;=============================================================
;;; End.
