;;;
;;; volume-graphics
;;;
;;; defines draw methods and other stuff for drawing contoured volumes
;;; in views.
;;;
;;; 29-Nov-1992 J. Unger modify draw method to pass pstruct colors,
;;; modify draw method to accept optional color arg and to draw a
;;; segment between last and first point of contour.
;;; 13-Dec-1992 J. Unger modify draw method for pstructs into views to
;;; pass parent parameter, also to operate on view's foreground
;;; display list.
;;; 22-Dec-1992 J. Unger add code to draw contours into
;;; sagittal/coronal views.
;;; 29-Dec-1992 J. Unger break the draw method for pstructs into views
;;; into a separate method for each view; modify to do graphics
;;; primitive management cleanly, modify contour draw methods to work
;;; in conjunction with new draw method for pstructs into views.
;;; 04-Jan-1993 J. Unger modify draw method for pstructs into
;;; transverse views to correctly handle pstructs with multiple
;;; contours with the same z attribute, fix sign bug in
;;; contour/coronal view draw method, modify draw method for contours
;;; into transverse views to correctly handle the possibility of
;;; multiple contours from the same 'contour source' (eg: pstruct)
;;; with the same z attribute.
;;; 13-Jan-1993 J. Unger add draw method for pstructs into beam's eye
;;; views, add draw method for contours into beam's eye views and
;;; supporting code, move bev cache recomputation code to views
;;; module.
;;; 11-Feb-1993 J. Unger optimize drawing of contours into bev's.
;;; 15-Feb-1993 I. Kalet always set color in primitives whether new or
;;; old, get src-to-isocenter info from therapy machine.
;;; 25-Mar-1993 J. Unger move draw method for contours into beams eye
;;; views into beams-eye-views module, to break up a dependency cycle.
;;;  3-Sep-1993 I. Kalet split off from volumes module, separated from
;;;  contours module and move draw method for contour in bev here from
;;;  bev module.
;;;  1-Apr-1994 I. Kalet move bev method to new bev-graphics module
;;; 22-Apr-1994 I. Kalet change refs to view origin to new ones.
;;; 19-Sep-1996 I. Kalet merge draw methods for contours into methods
;;; for pstruct, to eliminate :prim keyword parameter to draw
;;; function.
;;;  6-Dec-1996 I. Kalet don't generate prims for color invisible
;;;  3-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx .
;;; 11-Jan-1998 I. Kalet add draw-transverse back here, use here too.
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;;  5-Jan-2000 I. Kalet relax z match criterion for transverse views.
;;; 13-Oct-2002 I. Kalet add draw method for room view.
;;; 25-May-2009 I. Kalet remove draw method for room view.
;;;

(in-package :prism)

;;;--------------------------------------

(defun draw-transverse (vertices prim x-origin y-origin scale)

  "draw-transverse vertices prim x-origin y-origin scale

Draws vertices into graphics primitive prim (which must be a lines
prim), based upon a transverse drawing plane with origin and scale as
provided."

  (declare (fixnum x-origin y-origin) (single-float scale))
  (let ((pts (pixel-contour vertices scale x-origin y-origin)))
    (push (nconc pts (list (first pts) (second pts)))
	  (points prim))))

;;;--------------------------------------

(defmethod draw ((pstr pstruct) (tv transverse-view))

  "draw (pstr pstruct) (tv transverse-view)

This method draws all the contours in the pstruct into a transverse
view.  Only those whose z is close to the z of the view are drawn."

  (if (eql (display-color pstr) 'sl:invisible)
      (setf (foreground tv) (remove pstr (foreground tv) :key #'object))
    (let ((prim (find pstr (foreground tv) :key #'object))
	  (color (sl:color-gc (display-color pstr)))
	  (pos (view-position tv))
	  (scale (scale tv))
	  (x0 (x-origin tv))
	  (y0 (y-origin tv)))
      (declare (fixnum x0 y0) (single-float pos scale))
      (unless prim
	(setq prim (make-lines-prim nil color :object pstr))
	(push prim (foreground tv)))
      (setf (color prim) color
	    (points prim) nil)
      (dolist (con (contours pstr))
	(when (poly:nearly-equal (z con) pos *display-epsilon*)
	  (draw-transverse (vertices con) prim x0 y0 scale))))))

;;;--------------------------------------

(defmethod draw ((pstr pstruct) (sv sagittal-view))

  "draw (pstr pstruct) (sv sagittal-view)

This method draws all the contours in the pstruct into a sagittal
view.  At each point where a contour intersects the view, a small box
is drawn in the view."

  (if (eql (display-color pstr) 'sl:invisible)
      (setf (foreground sv) (remove pstr (foreground sv) :key #'object))
    (let ((prim (find pstr (foreground sv) :key #'object))
	  (color (sl:color-gc (display-color pstr)))
	  (pos (view-position sv))
	  (xorig (x-origin sv)) 
	  (yorig (y-origin sv))
	  (scale (scale sv)))
      (declare (fixnum xorig yorig) (single-float pos scale))
      (unless prim 
	(setq prim (make-rectangles-prim nil color :object pstr))
	(push prim (foreground sv)))
      (setf (color prim) color
	    (rectangles prim) nil)
      (dolist (con (contours pstr))
	(when (vertices con)
	  (let ((rects  nil))
	    ;; Here, we check each line segment in the contour,
	    ;; determined by points (x1,y1) and (x2,y2), to determine
	    ;; whether the plane of this sagittal view cuts the
	    ;; segment.  If so, then determine the coordinates of the
	    ;; point of intersection (x,y) via linear interpolation,
	    ;; and map that point into pixel coordinates (xpix, ypix)
	    ;; of the view plane.
	    (mapl #'(lambda (verts)
		      (when (rest verts)
			(let ((x1 (first (first verts)))
			      (y1 (second (first verts)))
			      (x2 (first (second verts)))
			      (y2 (second (second verts))))
			  (declare (single-float x1 y1 x2 y2))
			  (when (and (not (poly:nearly-equal x1 x2))
				     (or (poly:nearly-increasing x1 pos x2) 
					 (poly:nearly-decreasing x1 pos x2)))
			    (let* ((x (z con))
				   (y (- y2 (* (- x2 pos)
					       (/ (- y2 y1) (- x2 x1)))))
				   (xpix (pix-x x xorig scale))
				   (ypix (pix-y y yorig scale)))
			      (declare (single-float x y)
				       (fixnum xpix ypix))
			      (setq rects 
				(nconc (list (- xpix 2) (- ypix 2) 4 4)
				       rects)))))))
		  (append (vertices con) (list (first (vertices con)))))
	    (setf (rectangles prim) (append rects (rectangles prim)))))))))

;;;--------------------------------------

(defmethod draw ((pstr pstruct) (cv coronal-view))

  "draw (pstr pstruct) (cv coronal-view)

This method draws all the contours in the pstruct into a coronal view."

  (if (eql (display-color pstr) 'sl:invisible)
      (setf (foreground cv) (remove pstr (foreground cv) :key #'object))
    (let ((prim (find pstr (foreground cv) :key #'object))
	  (color (sl:color-gc (display-color pstr)))
	  (pos (view-position cv))
	  (xorig (x-origin cv))
	  (yorig (y-origin cv))
	  (scale (scale cv)))
      (declare (fixnum xorig yorig) (single-float pos scale))
      (unless prim 
	(setq prim (make-rectangles-prim nil color :object pstr))
	(push prim (foreground cv)))
      (setf (color prim) color
	    (rectangles prim) nil)
      (dolist (con (contours pstr))
	(when (vertices con)
	  (let ((rects nil))
	    ;; Here, we check each line segment in the contour,
	    ;; determined by points (x1,y1) and (x2,y2), to determine
	    ;; whether the plane of this coronal view cuts the
	    ;; segment.  If so, then determine the coordinates of the
	    ;; point of intersection (x,y) via linear interpolation,
	    ;; and map that point into pixel coordinates (xpix, ypix)
	    ;; of the view plane.
	    (mapl #'(lambda (verts)
		      (when (rest verts)
			(let ((x1 (first (first verts)))
			      (y1 (second (first verts)))
			      (x2 (first (second verts)))
			      (y2 (second (second verts))))
			  (declare (single-float x1 y1 x2 y2))
			  (when (and (not (poly:nearly-equal y1 y2))  
				     (or (poly:nearly-increasing y1 pos y2) 
					 (poly:nearly-decreasing y1 pos y2)))
			    (let* ((y (z con))
				   (x (+ x1 (* (- pos y1)
					       (/ (- x2 x1) (- y2 y1)))))
				   (xpix (pix-x x xorig scale))
				   ;; here ypix transforms like x, not y
				   (ypix (pix-x y yorig scale)))
			      (declare (single-float x y)
				       (fixnum xpix ypix))
			      (setq rects 
				(nconc (list (- xpix 2) (- ypix 2) 4 4)
				       rects)))))))
		  (append (vertices con) (list (first (vertices con)))))
	    (setf (rectangles prim) (append rects (rectangles prim)))))))))

;;;--------------------------------------
;;; End.
