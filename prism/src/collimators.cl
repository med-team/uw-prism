;;;
;;; collimators
;;;
;;; Definitions of collimators for radiation beams, and their methods.
;;;
;;; 18-Jan-1993 I. Kalet separated from beams module to prevent cycle
;;; 11-Apr-1993 I. Kalet add events for collimator setting updates
;;; 16-Apr-1993 I. Kalet add collimator panels
;;; 27-Apr-1993 I. Kalet take out unnecessary function
;;; 31-Jul-1993 I. Kalet add not-saved methods
;;; 18-Aug-1993 I. Kalet fix error in replace-coll
;;;  5-Sep-1993 I. Kalet move panel code to coll-panels
;;; 30-Dec-1993 I. Kalet add full support for multileaf collimators
;;; 18-May-1994 I. Kalet add beam-for to multileaf collimator, add
;;; beam parameter to replace-coll, finally move beam-blocks out.
;;;  3-Jun-1994 J. Unger redefine attributes of combination-coll, edit
;;; code involving combination-coll.
;;; 23-Jun-1994 I. Kalet change floats to single-floats.
;;; 05-Aug-1994 J. Unger add cnts-coll class definition.
;;; 11-Aug-1994 J. Unger make slight mods to replace-coll.
;;; 24-Aug-1994 J. Unger add leaf-settings attr to cnts-coll.
;;; 29-Aug-1994 J. Unger make cnts-coll's leaf-settings and all coll's
;;; names not saved.
;;; 11-Jan-1995 I. Kalet put copy-coll methods here, not in beams.
;;;  Make replace-coll a generic function.  Eliminate back pointers to
;;;  beam-for everywhere.  Put slot-type ignore for beam-for.
;;;  1-Sep-1995 I. Kalet small optimizations in portal methods
;;;  5-Jan-1996 I. Kalet add portal-coll, electron-coll, srs-coll
;;;  4-Feb-1997 I. Kalet add methods for coll-length, coll-width,
;;; change portal methods to return only vertices, not a contour obj.
;;; 21-May-1997 I. Kalet move replace-coll to separate module, to make
;;;  adding collimator types easier.
;;; 21-Jun-1997 BobGian add x-inf-coord, y-sup-coord, etc, methods to
;;;  return portal edges (jaw coordinates) independently of jaw type
;;;  for symmetric-jaw, variable-jaw, and combination collimators.
;;;  Reasons: (1) no consing (as the portal method does),
;;;           (2) naming uniformity (same name used for all coll types).
;;; 21-Jun-1997 BobGian make (* 2.0 (coerce pi 'single-float))
;;;  be read-time-evaluated via sharpsign-dot, and reverse -> nreverse,
;;;  both efficiency hacks, in (defmethod portal ((coll srs-coll)).
;;; 25-Aug-1997 BobGian changed #.(expression (coerce pi 'single-float))
;;;                          to #.(coerce (expression pi))
;;;  that is, do math in double-precision first and then coerce to
;;;  single-float at end, all inside read-time computation.
;;;  1-Sep-1997 BobGian simplified coll-width and coll-length methods for
;;;  multileaf collimators - no need for absolute value.
;;;  3-Sep-1997 BobGian changed x-inf-coord, x-sup-coord ("x" is "x" or "y"),
;;;  four separate methods, to single method coll-coords for each collimator
;;;  type.  Method for mlcs returns portal vertices.  Used only in beam-dose.
;;; 13-Mar-1998 BobGian remove coll-width and coll-length methods for mlc
;;;  since the information needed to compute bounding box on an mlc is not
;;;  available to the collimator object.
;;; 18-Dec-1998 I. Kalet add energy to electron collimator, really
;;; belongs to beam, but fits better here.
;;;  9-Feb-2000 BobGian add new-energy to not-saved for electron-coll.
;;; 13-Feb-2000 I. Kalet copy-coll for portal collimators (mlc and
;;; electrons) copies or reflects portal vertices only for gantry delta
;;; 0 or 180, otherwise just resets to 10 by 10 square.
;;; 22-Feb-2000 I. Kalet replace copy-coll with just copy methods,
;;; that return new instances of collimator objects.  Defer the
;;; reflection etc. to the place where needed.
;;; 10-Sep-2000 I. Kalet remove support for obsolete srs collimator.
;;; Modify multileaf collimator to include diaphragm jaws and make leaf
;;; settings canonical instead of portal contour.
;;; 11-Jun-2001 BobGian replace type-specific arithmetic macros
;;;   with THE declarations.
;;; 19-Jun-2001 I. Kalet add to not-saved method for multileaf-coll.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defmacro counter-clockwise-rectangle (left bottom right top)

  `(list (list (the single-float ,left) (the single-float ,bottom))
	 (list (the single-float ,right) (the single-float ,bottom))
	 (list (the single-float ,right) (the single-float ,top))
	 (list (the single-float ,left) (the single-float ,top))))

;;;---------------------------------------------

(defclass collimator (generic-prism-object)

  ((new-coll-set :type ev:event
		 :accessor new-coll-set
		 :initform (ev:make-event)
		 :documentation "Announced when any of the collimator
settings changes, so some entities need only register with this event,
not with all the individual collimator settings.")

   )

  (:documentation "The base collimator class just provides this simple
forwarding event.")

  )

;;;---------------------------------------------

(defmethod not-saved ((coll collimator))

  (append (call-next-method) '(name new-coll-set)))

;;;---------------------------------------------

(defclass symmetric-jaw-coll (collimator)

  ((x :type single-float
      :accessor x
      :initarg :x)

   (y :type single-float
      :accessor y
      :initarg :y)

   (new-coll-x :type ev:event
	       :accessor new-coll-x
	       :initform (ev:make-event)
	       :documentation "Announced when x is updated.")

   (new-coll-y :type ev:event
	       :accessor new-coll-y
	       :initform (ev:make-event)
	       :documentation "Announced when y is updated.")

   )

  (:default-initargs :x 10.0 :y 10.0)

  (:documentation "A symmetric jaw collimator system")

  )

;;;---------------------------------------------

(defmethod not-saved ((coll symmetric-jaw-coll))

  (append (call-next-method) '(new-coll-x new-coll-y)))

;;;---------------------------------------------

(defmethod coll-width ((coll symmetric-jaw-coll))

  (x coll))

(defmethod coll-length ((coll symmetric-jaw-coll))

  (y coll))

;;;---------------------------------------------

(defmethod coll-coords ((coll symmetric-jaw-coll))
  ;;
  ;; Returns first value nil and all 4 jaw coordinates.  First return
  ;; value means this is not an mlc [or other portal collimator subtype]
  ;; and therefore we can use blocking with this collimator type.
  ;;
  (let ((xval (* 0.5 (the single-float (x coll))))
	(yval (* 0.5 (the single-float (y coll)))))
    (declare (single-float xval yval))
    (values nil
	    (- xval)
	    xval
	    (- yval)
	    yval)))

;;;---------------------------------------------

(defmethod (setf x) :after (new-x (coll symmetric-jaw-coll))

  (ev:announce coll (new-coll-x coll) new-x)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf y) :after (new-y (coll symmetric-jaw-coll))

  (ev:announce coll (new-coll-y coll) new-y)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod portal ((coll symmetric-jaw-coll))

  "Creates a simple rectangular CCW contour centered on the isocenter,
with width x, and height y."

  (let ((xval (* 0.5 (the single-float (x coll))))
	(yval (* 0.5 (the single-float (y coll)))))
    (declare (single-float xval yval))
    (counter-clockwise-rectangle (- xval) (- yval) xval yval)))

;;;---------------------------------------------

(defmethod copy ((old-col symmetric-jaw-coll))

  (make-instance 'symmetric-jaw-coll :x (x old-col) :y (y old-col)))

;;---------------------------------------------

(defclass variable-jaw-coll (collimator)

  ((x-sup :type single-float
	  :accessor x-sup
	  :initarg :x-sup)

   (y-sup :type single-float
	  :accessor y-sup
	  :initarg :y-sup)

   (x-inf :type single-float
	  :accessor x-inf
	  :initarg :x-inf)

   (y-inf :type single-float
	  :accessor y-inf
	  :initarg :y-inf)

   (new-coll-x-sup :type ev:event
		   :accessor new-coll-x-sup
		   :initform (ev:make-event)
		   :documentation "Announced when x-sup is updated.")

   (new-coll-y-sup :type ev:event
		   :accessor new-coll-y-sup
		   :initform (ev:make-event)
		   :documentation "Announced when y-sup is updated.")

   (new-coll-x-inf :type ev:event
		   :accessor new-coll-x-inf
		   :initform (ev:make-event)
		   :documentation "Announced when x-inf is updated.")

   (new-coll-y-inf :type ev:event
		   :accessor new-coll-y-inf
		   :initform (ev:make-event)
		   :documentation "Announced when y-inf is updated.")

   )

  (:default-initargs :x-sup 5.0 :y-sup 5.0 :x-inf 5.0 :y-inf 5.0)

  (:documentation "A collimator with independently movable jaws")
  )

;;;---------------------------------------------

(defmethod not-saved ((coll variable-jaw-coll))

  (append (call-next-method)
	  '(new-coll-x-sup new-coll-y-sup
			   new-coll-x-inf new-coll-y-inf)))

;;;---------------------------------------------

(defmethod coll-width ((coll variable-jaw-coll))

  (+ (the single-float (x-sup coll)) (the single-float (x-inf coll))))

(defmethod coll-length ((coll variable-jaw-coll))

  (+ (the single-float (y-sup coll)) (the single-float (y-inf coll))))

;;;---------------------------------------------

(defmethod coll-coords ((coll variable-jaw-coll))

  "Returns multiple values, nil and all 4 jaw coordinates.  First
return value means this is NOT an MLC [or other portal collimator
subtype] and therefore we CAN use blocking with this collimator type."

  (values nil
	  (- (the single-float (x-inf coll)))
	  (x-sup coll)
	  (- (the single-float (y-inf coll)))
	  (y-sup coll)))

;;;---------------------------------------------

(defmethod (setf x-sup) :after (new-x (coll variable-jaw-coll))

  (ev:announce coll (new-coll-x-sup coll) new-x)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf y-sup) :after (new-y (coll variable-jaw-coll))

  (ev:announce coll (new-coll-y-sup coll) new-y)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf x-inf) :after (new-x (coll variable-jaw-coll))

  (ev:announce coll (new-coll-x-inf coll) new-x)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf y-inf) :after (new-y (coll variable-jaw-coll))

  (ev:announce coll (new-coll-y-inf coll) new-y)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod portal ((coll variable-jaw-coll))

  "Creates a simple rectangular CCW contour with width the difference of
x-sup and x-inf, and height the difference of y-sup and y-inf."

  (counter-clockwise-rectangle (- (the single-float (x-inf coll)))
			       (- (the single-float (y-inf coll)))
			       (x-sup coll)
			       (y-sup coll)))

;;;---------------------------------------------

(defmethod copy ((old-col variable-jaw-coll))

  (make-instance 'variable-jaw-coll
    :x-inf (x-inf old-col) :x-sup (x-sup old-col)
    :y-inf (y-inf old-col) :y-sup (y-sup old-col)))

;;;---------------------------------------------

(defclass cnts-coll (variable-jaw-coll)

  ((leaf-settings :accessor leaf-settings
		  :initarg :leaf-settings
		  :initform nil
		  :documentation "A list of numbers corresponding to
the leaf settings that will best match the portal contour -- a cache
used in the code that writes a cnts-collimator field to the neutron
file.")

   )

  (:documentation "A cnts-coll is a variable-jaw collimator with leaf
settings.")

  )

;;;---------------------------------------------

(defmethod not-saved ((coll cnts-coll))

  (append (call-next-method) '(leaf-settings)))

;;;---------------------------------------------

(defmethod slot-type ((object cnts-coll) slotname)

  (case slotname
    (beam-for :ignore)
    (otherwise (call-next-method))))

;;;---------------------------------------------

(defmethod copy ((old-col cnts-coll))

  (make-instance 'cnts-coll
    :leaf-settings (leaf-settings old-col)
    :x-inf (x-inf old-col) :x-sup (x-sup old-col)
    :y-inf (y-inf old-col) :y-sup (y-sup old-col)))

;;;---------------------------------------------

(defclass combination-coll (collimator)

  ((x-inf :type single-float
	  :accessor x-inf
	  :initarg :x-inf)

   (x-sup :type single-float
	  :accessor x-sup
	  :initarg :x-sup)

   (y :type single-float
      :accessor y
      :initarg :y)

   (new-coll-x-inf :type ev:event
		   :accessor new-coll-x-inf
		   :initform (ev:make-event)
		   :documentation "Announced when x-inf is updated.")

   (new-coll-x-sup :type ev:event
		   :accessor new-coll-x-sup
		   :initform (ev:make-event)
		   :documentation "Announced when x-sup is updated.")

   (new-coll-y :type ev:event
	       :accessor new-coll-y
	       :initform (ev:make-event)
	       :documentation "Announced when y is updated.")

   )

  (:default-initargs :x-sup 5.0 :x-inf 5.0 :y 10.0)

  (:documentation "A collimator with only one set of independently
movable jaws.")
  )

;;;---------------------------------------------

(defmethod not-saved ((coll combination-coll))

  (append (call-next-method)
	  '(new-coll-x-inf new-coll-x-sup new-coll-y)))

;;;---------------------------------------------

(defmethod coll-width ((coll combination-coll))

  (+ (the single-float (x-sup coll)) (the single-float (x-inf coll))))

(defmethod coll-length ((coll combination-coll))

  (y coll))

;;;---------------------------------------------

(defmethod coll-coords ((coll combination-coll))

  "Returns first value NIL and all 4 jaw coordinates.  First return
value means this is NOT an MLC [or other portal collimator subtype]
and therefore we CAN use blocking with this collimator type."

  (let ((yval (* 0.5 (the single-float (y coll)))))
    (declare (single-float yval))
    (values nil
	    (- (the single-float (x-inf coll)))
	    (x-sup coll)
	    (- yval)
	    yval)))

;;;---------------------------------------------

(defmethod (setf x-inf) :after (new-x-inf (coll combination-coll))

  (ev:announce coll (new-coll-x-inf coll) new-x-inf)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf x-sup) :after (new-x-sup (coll combination-coll))

  (ev:announce coll (new-coll-x-sup coll) new-x-sup)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf y) :after (new-y (coll combination-coll))

  (ev:announce coll (new-coll-y coll) new-y)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod portal ((coll combination-coll))

  "Creates a simple rectangular CCW contour with width the difference of
x-sup and x-inf, and height y."

  (let ((bottom (* 0.5 (the single-float (y coll)))))
    (declare (single-float bottom))
    (counter-clockwise-rectangle (- (the single-float (x-inf coll)))
				 (- bottom)
				 (x-sup coll)
				 bottom)))

;;;---------------------------------------------

(defmethod copy ((old-col combination-coll))

  (make-instance 'combination-coll
    :x-inf (x-inf old-col) :x-sup (x-sup old-col)
    :y (y old-col)))

;;;---------------------------------------------

(defclass portal-coll (collimator contour)

  ()

  (:default-initargs :z 0.0 :vertices '((-5.0 -5.0) ;; 10 by 10
					(5.0 -5.0)
					(5.0 5.0)
					(-5.0 5.0)))

  (:documentation "A collimator that includes a portal, e.g. multileaf
or electron cone with cutout. It includes slots from class contour to
define the portal contour.  It therefore also will inherit methods for
drawing contours.  The contour is always at z = 0.0 .")

  )

;;;---------------------------------------------

(defmethod coll-coords ((coll portal-coll))

  "Returns portal vertex list and four dummy jaw coordinates.  First
return value [must be non-nil by spec] indicates that this is an mlc
[or other portal collimator subtype] and we don't use blocking."

  (values (vertices coll) 0.0 0.0 0.0 0.0))

;;;---------------------------------------------

(defmethod (setf vertices) :after (new-verts (coll portal-coll))

  (declare (ignore new-verts))
  (ev:announce coll (new-coll-set coll)))

;;;----------------------------------------------

(defmethod portal ((coll portal-coll))

  "Returns the multivertex polygon contained in COLL as a vertex-list."

  (vertices coll))

;;;---------------------------------------------

(defmethod copy ((old-col portal-coll))

  (make-instance (class-of old-col)
    :z (z old-col)
    :vertices (mapcar #'(lambda (pt) (list (first pt) (second pt)))
		(vertices old-col))))

;;;---------------------------------------------

(defclass multileaf-coll (portal-coll)

  ((leaf-settings :accessor leaf-settings
		  :initarg :leaf-settings
		  :documentation "A list of numbers corresponding to
the leaf settings that will best match the portal contour, or those
chosen by the dosimetrist.")

   (x1 :type single-float
       :accessor x1
       :documentation "DICOM X1 leaves, open in -x direction")

   (x2 :type single-float
       :accessor x2
       :documentation "DICOM X2 leaves, open in +x direction")

   (y1 :type single-float
       :accessor y1
       :documentation "DICOM Y1 leaves, open in -y direction")

   (y2 :type single-float
       :accessor y2
       :documentation "DICOM Y2 leaves, open in +y direction")

   )

  ;; No init for jaws - used only in DICOM panel.
  (:default-initargs :leaf-settings nil)

  (:documentation "A multileaf collimator. It includes slots from
class contour to define the portal contour.  It therefore also will
inherit methods for drawing contours.  The contour is always at z = 0.0 .")

  )

;;;---------------------------------------------

(defmethod slot-type ((object multileaf-coll) slotname)

  (case slotname
    (beam-for :ignore)                     ;; required from past history at UW
    (otherwise :simple)))

;;;----------------------------------------------

;; The leaf-settings attribute for an mlc can probably be removed
;; from the system.  There are numerous instances of this attribute
;; in existing files, however, so deleting this attribute will require
;; a sweep through existing data files.

(defmethod not-saved ((coll multileaf-coll))

  (append (call-next-method) '(leaf-settings x1 y1 x2 y2)))

;;;---------------------------------------------
;;; coll-width and coll-length methods for multileaf-col were here, but
;;; were deleted since they can only return the correct bounding box
;;; approximation to the MLC portal by knowing the collimator angle,
;;; which is not available to the collimator object.
;;;---------------------------------------------

(defclass electron-coll (portal-coll)

  ((energy :type single-float
	   :accessor energy
	   :initarg :energy
	   :documentation "A single electron machine includes a range
	   of energies, and the one selected is recorded here.")

   (new-energy :type ev:event
	       :accessor new-energy
	       :initform (ev:make-event)
	       :documentation "Announced when energy is changed.")

   (cone-size :type single-float
	      :accessor cone-size
	      :initarg :cone-size
	      :documentation "An electron collimator is a square cone
with possibly a metal cutout fastened to it.")

   (new-cone-size :type ev:event
		  :accessor new-cone-size
		  :initform (ev:make-event)
		  :documentation "Announced when cone size is changed.")

   )

  (:default-initargs :energy 10.0 :cone-size 10.0)

  (:documentation "This collimator models the use of the electron
beam.  A single electron machine has a series of energies, so we
include energy selection with the electron collimator.")

  )

;;;---------------------------------------------

(defmethod not-saved ((coll electron-coll))

  (append (call-next-method) '(new-energy new-cone-size)))

;;;---------------------------------------------

(defmethod coll-width ((coll electron-coll))

  (cone-size coll))

(defmethod coll-length ((coll electron-coll))

  (cone-size coll))

;;;---------------------------------------------

(defmethod (setf energy) :after (new-e (coll electron-coll))

  (ev:announce coll (new-energy coll) new-e)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod (setf cone-size) :after (new-size (coll electron-coll))

  (ev:announce coll (new-cone-size coll) new-size)
  (ev:announce coll (new-coll-set coll)))

;;;---------------------------------------------

(defmethod copy ((old-col electron-coll))

  (let ((new-col (call-next-method)))
    (setf (energy new-col) (energy old-col))
    (setf (cone-size new-col) (cone-size old-col))
    new-col))

;;;---------------------------------------------
;;; End.
