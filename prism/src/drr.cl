;;;
;;; drr
;;;
;;; code for computing digitally reconstructed radiographs
;;;
;;; xx-Jul-1998 C. Wilcox wrote, based on Jon Unger's code from 1992.
;;; 12-Aug-1998 I. Kalet make image quality setting a global instead
;;; of prompting, also reformat some code for readability.
;;; 03-Apr-1999 C. Wilcox created progressive version of DRR's with
;;;  support for pausing, restarting, and canceling. 
;;; 11-Jul-2000 I. Kalet map-image-to-clx now split into two functions.
;;; 10-Sep-2000 I. Kalet image display now handled by OpenGL, not here,
;;; also eliminate multiresolution scheme, not useful after all.
;;; 13-Dec-2000 I. Kalet handle incremental display update by cached
;;; function in view, not by indirect kludge.
;;; 26-Jun-2005 I. Kalet change single-float calls to coerce
;;; 25-Jun-2008 I. Kalet take out erroneous declarations
;;;

(in-package :prism)

;;;----------------------------------------------

(defconstant **epsilon** (* 5 least-positive-single-float)) ;; single-float

(defvar *drr-rows-per-time-slice* 10
  "determines how long the drr runs before processing accumulated X events")

;;;----------------------------------------------

(defun vec-cross (v1 v2 &optional v3)
  (declare (type (simple-array single-float (3)) v1 v2 v3)
	   ;; (:explain :calls :types :variables :boxing)
	   )
  (if (not v3) (setf v3 (make-array 3 :element-type 'single-float)))
  (setf (aref v3 0)
    (- (* (aref v1 1) (aref v2 2)) (* (aref v1 2) (aref v2 1))))
  (setf (aref v3 1)
    (- (* (aref v1 2) (aref v2 0)) (* (aref v1 0) (aref v2 2))))
  (setf (aref v3 2)
    (- (* (aref v1 0) (aref v2 1)) (* (aref v1 1) (aref v2 0))))
  v3)

;;;----------------------------------------------

(defun vec-scale (s v &optional v3)

  (declare (type (simple-array single-float (3)) v v3)
	   (type single-float s))
  (if (not v3) (setf v3 (make-array 3 :element-type 'single-float)))
  (setf (aref v3 0) (* s (aref v 0)))
  (setf (aref v3 1) (* s (aref v 1)))
  (setf (aref v3 2) (* s (aref v 2)))
  v3)

;;;----------------------------------------------

(defun vec-diff (v1 v2 &optional v3)

  (declare (type (simple-array single-float (3)) v1 v2 v3))
  (if (not v3) (setf v3 (make-array 3 :element-type 'single-float)))
  (setf (aref v3 0) (- (aref v1 0) (aref v2 0)))
  (setf (aref v3 1) (- (aref v1 1) (aref v2 1)))
  (setf (aref v3 2) (- (aref v1 2) (aref v2 2)))
  v3)

;;;----------------------------------------------

(defun vec-sum (v1 v2 &optional v3)

  (declare (type (simple-array single-float (3)) v1 v2 v3))
  (if (not v3) (setf v3 (make-array 3 :element-type 'single-float)))
  (setf (aref v3 0) (+ (aref v1 0) (aref v2 0)))
  (setf (aref v3 1) (+ (aref v1 1) (aref v2 1)))
  (setf (aref v3 2) (+ (aref v1 2) (aref v2 2)))
  v3)

;;;----------------------------------------------

(defun vec-mag (v)

  (declare (type (simple-array single-float (3)) v))
  (sqrt (+ (* (aref v 0) (aref v 0))
	   (+ (* (aref v 1) (aref v 1))
	      (* (aref v 2) (aref v 2))))))

;;;----------------------------------------------

(defun vec-normalize (v &optional v3)

  (declare (type (simple-array single-float (3)) v v3))
  (if (not v3) (setf v3 (make-array 3 :element-type 'single-float)))
  (let* ((mag (vec-mag v)))
    (declare (type single-float mag))
    (cond ((> **epsilon** mag) 
	   (error "Can not normalize a zero length array"))
	  (t (vec-scale (/ mag) v v3)))))

;;;--------------------
;; Return the index for the largest array element that is
;; less than or equal to value

(defun array-search (arr value)

  (declare (type single-float value)
	   ;; one-dimension and unknown length
	   (type (simple-array single-float 1) arr))
  (let* ((low 0)
	 (high (array-dimension arr 0))
	 (mid 0))
    (declare (integer low high mid))
    (loop
      (when (>= low high) 
	(return (if (>= 0 high) 0 (- high 1))))
      (setf mid (truncate (+ low high) 2))
      (cond
       ((<= (aref arr mid) value) (setf low (+ 1 mid)))
       (t (setf high mid))))))

;;;--------------------

(defun find-voxel (point voxel-width zarray return-array)

  ;; ASSUMPTION:  voxel-width is non-zero!
  ;; ASSUMPTION:  min-corner is <0,0,0>

  (declare (type (simple-array single-float (3)) point voxel-width)
	   (type (simple-array single-float) zarray)
	   (type (simple-array (unsigned-byte 16) (3)) return-array))
  (setf (aref return-array 0) 
    (max 0 (floor (aref point 0) (aref voxel-width 0))))
  (setf (aref return-array 1)
    (max 0 (floor (aref point 1) (aref voxel-width 1))))
  (setf (aref return-array 2)
    (max 0 (array-search zarray (aref point 2)))))

;;;--------------------

(defun ray-box-intersect (ray-start ray-direction box-width)

  "This function takes three parameters:
      ray-start: a point in space where a ray starts
      ray-direction: a vector pointing in the direction of
                     the ray
      box-width: three coords representing the size of the box
   This function returns a two element list:
      first: the length along the ray to to first intersection point
      second: the length along the ray to the second intersection
              point
   If there is no intersection, then we will return nil."

  (declare (type (simple-array single-float (3))
		 ray-start ray-direction box-width))
  (let* ((tnear most-negative-single-float)
	 (tfar  most-positive-single-float)
	 (t1         0.0)
	 (t2         0.0)
	 (temp       0.0))
    (declare (type single-float tnear tfar t1 t2 temp))
    (dotimes (i 3)
      (cond
       ((< (abs (aref ray-direction i)) **epsilon**)
	(when (or (< (aref ray-start i) 0.0) 
		  (> (aref ray-start i) (aref box-width i)))
	  (return-from ray-box-intersect nil)))
       (t
	;; distance to first slab
	(setf t1 (/ (- 0.0 (aref ray-start i)) (aref ray-direction i)))
	;; distance to second slab
	(setf t2 (/ (- (aref box-width i) (aref ray-start i))
		    (aref ray-direction i)))
	;; ensure that t1 < t2
	(when (> t1 t2)
	  (setf temp t1)
	  (setf t1 t2)
	  (setf t2 temp))
	;; update near and far
	(when (> t1 tnear)
	  (setf tnear t1))
	(when (< t2 tfar)
	  (setf tfar t2))
	;; if we miss or the box is behind the eye
	;; then bail out and return nil
	(when (or (> tnear tfar) (< tfar 0.0))
	  (return-from ray-box-intersect nil)))))
    (list tnear tfar)))

;;;--------------------

(defun density-sum (eye pixPt voxels coord-dist voxel-widths zarray)

  "This is where the drr raytrace is calculated for each pixel in the image."

  ;; assume that min < max for all 3 coordinates
  ;; assume that min corner of voxel array is <0,0,0>
  ;; calculate a normalized vector from eye to pixPt

  (declare (type (simple-array single-float (3))
                 eye pixPt coord-dist voxel-widths)
           (type (simple-array single-float 1) zarray)
	   ;; an array of 2d arrays of unsigned-byte 16's
           (type (simple-array (simple-array (unsigned-byte 16) 2) 1)
                 voxels))
  (let* ((ray (vec-normalize (vec-diff pixPt eye)))
         (bounds (ray-box-intersect eye ray coord-dist))
         (voxdim (make-array 3 :element-type '(unsigned-byte 16)
                             :initial-contents
                             (list (- (array-dimension (aref voxels 0) 0) 1)
                                   (- (array-dimension (aref voxels 0) 1) 1)
                                   (- (array-dimension voxels 0) 1))))
         (tmin 0.0)
         (tmax 0.0)
         (next-t 0.0)
         (current-t 0.0)
         (next-axis 0) ;; fixnum
         (current-voxel (make-array 3 :element-type '(unsigned-byte 16)))
         (ray-sign  (make-array 3 :element-type 'fixnum))
         (next-index-val  (make-array 3 :element-type 'fixnum))
         (next-ts (make-array 3 :element-type 'single-float))
         (next-plane-val (make-array 3 :element-type 'single-float))
         (delta-t (make-array 3 :element-type 'single-float))
         (delta-zt (make-array (- (array-dimension zarray 0) 1)
                               :element-type 'single-float))
         (total-density 0.0))
    (declare (type single-float tmin tmax next-t current-t total-density)
             (fixnum next-axis)
	     (type (simple-array (unsigned-byte 16) (3))
		   voxdim current-voxel)
             (type (simple-array fixnum (3))
                   ray-sign next-index-val)
             (type (simple-array single-float (3))
                   ray next-ts next-plane-val delta-t)
             (type (simple-array single-float 1) delta-zt))
    ;; if the ray does not intersect the voxel array, return 0
    (when (not bounds)
      (return-from density-sum 0.0))
    ;; set some values now that bounds != nil
    (setf tmin (first bounds))
    (setf tmax (second bounds))
    (setf current-t tmin)
    ;; create the delta-z array
    (dotimes (i (array-dimension delta-zt 0))
      (setf (aref delta-zt i)
        (abs (/ (- (aref zarray (+ i 1)) (aref zarray i)) (aref ray 2)))))
    (find-voxel (vec-sum (vec-scale (+ tmin **epsilon**) ray) eye) 
		voxel-widths zarray current-voxel)
    ;; do a max bounds check on the current-voxel
    (dotimes (i 3)
      (when (> (aref current-voxel i) (aref voxdim i))
        (setf (aref current-voxel i) (aref voxdim i))))
    (dotimes (i 3)
      ;; set whether the ray is moving positive, negative, or neither
      (setf (aref ray-sign i)
        (cond
         ((> (aref ray i) 0.0) 1)
         ((< (aref ray i) 0.0) -1)
         (t 0)))
      ;; set next index value that is going to be crossed in each direction
      (setf (aref next-index-val i)
        (if (= (aref ray-sign i) -1)
            (aref current-voxel i)
          (+ 1 (aref current-voxel i))))
      ;; only used for finding the first set of next-ts
      ;; this is the world coordinates value for the next plane that
      ;; will be crossed
      (setf (aref next-plane-val i)
        (if (= i 2)
            (aref zarray (aref next-index-val 2))
          (* (aref next-index-val i) (aref voxel-widths i))))
      (setf (aref delta-t i)
        (if (= (aref ray-sign i) 0)
            100000.0
          (abs (/ (aref voxel-widths i) (aref ray i)))))
      (setf (aref next-ts i)
        (if (= (aref ray-sign i) 0) 
            100000.0
          (/ (- (aref next-plane-val i) (aref eye i)) (aref ray i)))))
    ;; select the next axis that the ray will cross
    (setf next-axis 0)
    ;; choose the next axis to cross in the voxel array
    (when (< (aref next-ts 1) (aref next-ts next-axis))
      (setf next-axis 1))
    (when (< (aref next-ts 2) (aref next-ts next-axis))
      (setf next-axis 2))
    ;; update the next value of t for crossing a voxel
    (setf next-t (aref next-ts next-axis))
    ;; increment the next t for the axis that was chosen
    (incf (aref next-ts next-axis)
          (if (= next-axis 2)
	      ;; do something smart for z-axis @@
              (aref delta-zt 
                    (floor
                     (min (- (array-dimension delta-zt 0) 1)
                          (max 0
                               (+ (aref current-voxel 2) (aref ray-sign 2))))))
	    ;; if it is the x or y axis then add a delta
            (aref delta-t next-axis)))
    ;; take care of precision issue
    (decf tmax **epsilon**)

    ;; ***** This is where the action is *****
    (do nil ( (or (> next-t tmax) (< next-t 0.0)) . nil)
      ;; increment the density value
      (incf total-density 
            (* (aref (aref voxels (aref current-voxel 2)) 
		     ;; flip the 'sign' for y since y points
		     ;; down in image coordinates (in slice data)
                     (- (aref voxdim 1) (aref current-voxel 1))
                     (aref current-voxel 0))
               (- next-t current-t)))
      ;; update current-t
      (setf current-t next-t)
      ;; update current-voxel with bounds check
      (setf (aref current-voxel next-axis)
	(max 0 (min (aref voxdim next-axis)
		    (+ (aref current-voxel next-axis)
		       (aref ray-sign next-axis)))))
      ;; choose the next axis to cross
      (setf next-axis 0)
      (when (< (aref next-ts 1) (aref next-ts 0))
        (setf next-axis 1))
      (when (< (aref next-ts 2) (aref next-ts next-axis))
        (setf next-axis 2))
      ;; assign the next value for t based on the chosen axis
      (setf next-t (aref next-ts next-axis))
      ;; increment the next t for the axis that was chosen
      (incf (aref next-ts next-axis)
            (if (= next-axis 2)
		;; do something smart for z-axis @@
                (aref delta-zt 
                      (floor
                       (min (- (array-dimension delta-zt 0) 1)
                            (max 0
                                 (+ (aref current-voxel 2)
                                    (aref ray-sign 2))))))
	      ;; if it is the x or y axis then add a delta
              (aref delta-t next-axis)))
      ) ;; *** end of do loop ***
    ;; final increment of the density value (use tmax instead of next-t)
    (incf total-density 
          (* (aref (aref voxels (aref current-voxel 2)) 
                   (- (aref voxdim 1) (aref current-voxel 1))
                   (aref current-voxel 0))
             (- tmax current-t)))
    ;; return the total density
    total-density))

;;;--------------------

(defun drr (corner1 corner2 zarray eyePt centerPt topPt
	    x-pixels y-pixels voxels bev)

  "Calculates the drr:
      corner1 = patient coordinates of one corner of the voxel grid
      corner2 = patient coord's of opposing corner of the voxel grid
      zarray = patient space z coord's for each 'slice' of the voxel array
      eyePt   = patient coord's for the origin of projection
      centerPt = patient coord's for the center of the
                 projection plane
      topPt = patient coord's for the top middle coord of
                 the projection plane
      x-pixels = number of horizontal pixels in the final image
      y-pixels = number of vertical pixels in the final image
      voxels = the array of 2d arrays of voxel data
      bev = the beams-eye-view that we are generating a drr for

   returns a 2d array of (unsigned-byte 16) whose
      dimensionality corresponds to x-pixels & y-pixels"

  ;; Declare the types for the input parameters
  (declare (type (simple-array single-float (3)) eyePt centerPt topPt)
	   (type (unsigned-byte 16) x-pixels y-pixels)
	   ;; an array of 2d arrays of unsigned-byte 16's
	   (type (simple-array (simple-array (unsigned-byte 16) 2) 1)
		 voxels))
  ;; Setup local variables
  (let* ((voxdim (make-array 3 :element-type 'fixnum
			     :initial-contents
			     (list (array-dimension (aref voxels 0) 0)
				   (array-dimension (aref voxels 0) 1)
				   (array-dimension voxels 0))))
	 (voxmin (make-array 3 :element-type 'single-float
			     :initial-contents 
			     (list
			      (coerce (min (first corner1)
					   (first corner2)) 'single-float)
			      (coerce (min (second corner1)
					   (second corner2)) 'single-float)
			      (coerce (aref zarray 0) 'single-float))))
	 (voxmax (make-array 3 :element-type 'single-float
			     :initial-contents
			     (list
			      (coerce (max (first corner1)
					   (first corner2)) 'single-float)
			      (coerce (max (second corner1)
					   (second corner2)) 'single-float)
			      ;; last element of the zarray
			      (coerce (aref zarray
					    (- (array-dimension zarray 0)
					       1)) 'single-float))))
  	 (up-v (vec-diff topPt centerPt))
	 (normal-v (vec-diff eyePt centerPt))
	 (screen-height (* 2.0 (vec-mag up-v)))
	 ;; use pixel ratio to find screen-width
	 (screen-width  (* screen-height (/ x-pixels y-pixels)))
	 (right-v (vec-cross up-v normal-v))
	 (top-left-pt (make-array 3 :element-type 'single-float))
	 (density-map (make-array (list x-pixels y-pixels)
				  :element-type 'single-float
				  :initial-element 0.0))
	 (return-map (make-array (list x-pixels y-pixels)
				 :element-type '(unsigned-byte 16)
				 :initial-element 0))
	 (voxel-array-widths (vec-diff voxmax voxmin))
	 (voxel-widths (vec-diff voxmax voxmin)))
    (declare (type (simple-array single-float (3))
		   voxmin voxmax up-v normal-v right-v
		   top-left-pt voxel-widths)
	     (type (simple-array fixnum (3)) voxdim)
	     (type single-float screen-height screen-width)
	     (type (simple-array single-float 2) density-map)
	     (type (simple-array (unsigned-byte 16) 2) return-map))
    ;; calculate the widths for each voxel
    (dotimes (i 3)
      (setf (aref voxel-widths i) 
	(/ (aref voxel-widths i) 
	   (coerce (aref voxdim i) 'single-float))))
    ;; guarantee that the image plane is perpendicular 
    ;; to the viewing direction
    (setf up-v (vec-normalize (vec-cross normal-v right-v)))
    (setf right-v (vec-normalize right-v))
    (setf top-left-pt
      (vec-sum 
       (vec-sum centerPt (vec-scale (* screen-width -0.5) right-v))
       (vec-scale  (* screen-width 0.5) up-v)))
    ;; pre-scale the right and up vectors to make the pixel to
    ;; patient coordinate transformation faster
    (setf right-v (vec-scale (/ screen-width (- x-pixels 1)) right-v))
    (setf up-v    (vec-scale (/ screen-height (- y-pixels 1)) up-v))
    ;; for speed optimization translate so that the min corner of
    ;; the voxel array is <0,0,0> in world coords
    (setf eyePt (vec-diff eyePt voxmin))
    (setf top-left-pt (vec-diff top-left-pt voxmin))
    (dotimes (i (array-dimension zarray 0))
      (decf (aref zarray i) (aref voxmin 2)))
    (let ((valfunc 
	   #'(lambda (x y)
	       (density-sum eyePt
			    (vec-sum (vec-sum top-left-pt
					      (vec-scale (* -1.0 y) up-v))
				     (vec-scale (* 1.0 x) right-v))
			    voxels
			    voxel-array-widths
			    voxel-widths
			    zarray))))
      (setf (drr-args bev)
	;; pixels, drr floats, density function, initial row, initial maxval
	(vector return-map density-map valfunc 0 0.0))
      (setf (drr-state bev) 'running)
      (setf (sl:fg-color (image-button bev)) 'sl:green)
      (drr-bg bev))
    return-map))

;;;-----------------------------------------------
;; this wrapper is needed to identify the drr
;;   background function so that we can remove
;;   it from the background queue when necessary

(defun drr-bg (bev)

  (when (eq 'running (drr-state bev))
    (progressive-fill (drr-args bev))
    (let* ((drr-args (drr-args bev))
	   (pixels (aref drr-args 0))
	   (vals (aref drr-args 1))
	   (next-row (aref drr-args 3))
	   (maxp (aref drr-args 4)))
      (update-pixels vals pixels next-row (if (> maxp 0.0)
					      (/ 2000.0 maxp)
					    1.0))
      (format t "DRR completed up to row ~A~%" next-row)
      (funcall (display-func bev) bev)
      (cond ((< next-row (array-dimension vals 1)) ;; continue
	     (sl:enqueue-bg-event (list 'drr-bg bev)))
	    (t ;; cleanup, do not requeue, cache result
	     (setf (sl:fg-color (image-button bev)) 'sl:red)
	     (setf (drr-state bev) 'stopped)
	     (format t "DRR done!~%"))))))

;;;-----------------------------------------------

(defun update-pixels (vals pixels next-row scale)

  "Copy current progressive state of the floating point vals
to the image-pixels."

  (declare (type (simple-array single-float 2) vals)
	   (type (simple-array (unsigned-byte 16) 2) pixels)
	   (type single-float scale))
  ;; assume that dimension of pixels is = dim of vals
  (dotimes (y next-row)
    (declare (type (unsigned-byte 16) y))
    (dotimes (x (array-dimension vals 0))
      (declare (type (unsigned-byte 16) x))
      (setf (aref pixels y x)
	(max 0 (min 4000 (floor (* scale (aref vals y x))))))))
  nil)

;;;-----------------------------------------------

(defun progressive-fill (drr-args)

  "computes a bunch of rows of DRR data according to the standard
increment or how many rows are left, if fewer."

  (let ((vals (aref drr-args 1))
	(valfunc (aref drr-args 2))
	(next-row (aref drr-args 3))
	(maxval (aref drr-args 4))
	(tempf 0.0))
    (dotimes (delta-y (min *drr-rows-per-time-slice*
			   (- (array-dimension vals 1) next-row)))
      (dotimes (x (array-dimension vals 0))
	(setq tempf (funcall valfunc x next-row))
	(if (> tempf maxval) (setq maxval tempf))
	(setf (aref vals next-row x) tempf))
      (incf next-row))
    (setf (aref drr-args 3) next-row)
    (setf (aref drr-args 4) maxval)
    drr-args))

;;;-----------------------------------------------
;;; End.
