;;;
;;; import-structure-sets
;;;
;;; 6 May 03 M Phillips
;;; A panel for importing structure sets into the Prism current patient
;;; case from user-specified files. This was originally the file
;;; IMPORT-ANATOMY.  It has been modified to work with enhancements to
;;; the DICOM server in handling structure sets.  It is more hard-wired
;;; for directories and what is in each file, than was the old version.
;;;
;;; 15-May-2003 BobGian provide branch for "structure.index" file not found.
;;; 30-May-2003 BobGian add mechanism to lock structure Z-coodinates to
;;;   Z-coordinate of nearest image slice.  Image set must be pre-loaded.
;;; 06-Jun-2003 BobGian: ORIGIN slot of IMAGE is declared to be
;;;   (VECTOR SINGLE-FLOAT 3) but it is (SIMPLE-ARRAY T 3) instead, as created
;;;   by GET-ALL-OBJECTS.  Type-casting error in Z-coord justification fcn.
;;; 12-Jun-2003 BobGian parameterize structure-set directory as value
;;;   of variable *structure-database*.  Also, IMPORT-STRUCTURE-SET removed;
;;;   MAKE-IMPORT-STRUCTURE-SET-PANEL called directly from *SPECIAL-TOOLS*.
;;; 22-Aug-2003 BobGian add 180-degree rotation to structure-set contour vertex
;;;   set when structure-set corresponds to image from a patient scanned prone
;;;   (that is, PAT-POS of image is "HFP" or "FFP").
;;; 13-Nov-2009 I. Kalet Modify initialization method for
;;; import-structure-set-panel to allow for importing structure sets
;;; without having a corresponding image set.  There was no reason to
;;; require it, though it is important to match the Z values when an
;;; image set is present.  This was initially in a patch file,
;;; struct-import-patch, loaded at run time, which is now no longer needed.
;;;

(in-package :Prism)

;;;------------------------------------------------------

(defclass import-structure-set-panel (generic-panel)

  ((patient-of :accessor patient-of
	       :initarg :patient-of
	       :documentation "The patient - needed for list of plans")
   (panel-frame :accessor panel-frame
		:documentation "Slik frame for this panel")
   (cancel-but :accessor cancel-but
	       :documentation "The cancel panel button.")
   (accept-but :accessor accept-but
	       :documentation "The accept button for the function.")
   (namelist-but :accessor namelist-but
		 :documentation "Allows user to select file using a menu.")
   (pat-name :accessor pat-name
	     :documentation "Readout with name of patient if obtained from
			     namelist")
   (objects-scr :accessor objects-scr
		:documentation "Scrolling list that contains all the
			       objects available to import.")
   (objects-title :accessor objects-title)
   )
  (:documentation "Panel for importing anatomy data from an external
	       non-Prism data file.")
  )

;;;-----------------------------------------------------------

(defmethod initialize-instance :after ((issp import-structure-set-panel)
				       &rest initargs)

  (let* ((btw 100)
	 (bth 25)
	 (isspfr (apply #'sl:make-frame 375 375
			:title "INSERT STRUCTURE SET PANEL"
			:bg-color 'sl:gray initargs))
	 (win (sl:window isspfr))
	 (cancel-b (apply #'sl:make-button btw bth
			  :label "CANCEL" :parent win
			  :ulc-x 225 :ulc-y 345
			  :bg-color 'sl:red
			  :fg-color 'sl:black
			  initargs))
	 (accept-b (apply #'sl:make-button btw bth
			  :label "ACCEPT" :parent win
			  :ulc-x 15 :ulc-y 345
			  :bg-color 'sl:green
			  :fg-color 'sl:black
			  initargs))
	 (namelist-but (apply #'sl:make-button (+ 30 (* 2 btw)) bth
			      :label "AVAILABLE STRUCTURE SETS"
			      :parent win
			      :ulc-x 15 :ulc-y  15
			      initargs))
	 (pat-name (apply #'sl:make-readout 310 bth
			  :bg-color 'sl:yellow :fg-color 'sl:black
			  :label "Patient name: "
			  :ulc-x 15 :ulc-y 50 :parent win initargs))
	 (objects-title (apply #'sl:make-readout 200 bth
			       :ulc-x 15 :ulc-y 100
			       :info "AVAILABLE OBJECTS"
			       :parent win initargs))
	 (objects-scr (apply #'sl:make-scrolling-list 200 200
			     :ulc-x 15 :ulc-y 125
			     :parent win initargs))
	 (s-file (concatenate 'string *structure-database* "structure.index"))
	 (objects-alist '())             ; association list for scrolling list
	 (selected-list '())            ; list for structures selected by user
	 (selected-name nil)       ; patient name read from structure set file
	 (selected-file nil))

    (setf (panel-frame issp) isspfr
	  (cancel-but issp) cancel-b
	  (accept-but issp) accept-b
	  (namelist-but issp) namelist-but
	  (objects-title issp) objects-title
	  (objects-scr issp) objects-scr
	  (pat-name issp) pat-name)

    (ev:add-notify issp (sl:button-on cancel-b)
      #'(lambda (panel button)
	  (declare (ignore button))
	  (destroy panel)))

    (ev:add-notify issp (sl:button-on accept-b)
      #'(lambda (panel button)
	  (cond
	    ((= 0 (patient-id (patient-of panel)))
	     (sl:acknowledge "Patient needs to be selected.")
	     (setf (sl:on button) nil))
	    ((null selected-list)
	     (sl:acknowledge "Please select at least one structure.")
	     (setf (sl:on button) nil))
	    ((sl:confirm
	       (format nil
		       "Prism patient is [ ~A ].  Selected patient is [ ~A ]."
		       (name (patient-of panel)) selected-name))
	     (let* ((pat (patient-of panel))
		    (images (image-set pat)))
	       (cond ((consp images)
		      (let* ((pat-position (pat-pos (first images)))
			     (prone? (or (string= pat-position "HFP")
					 (string= pat-position "FFP")))
			     (z-coords
			       (mapcar #'(lambda (im)
					   (aref (origin im) 2))
				 images)))
			(dolist (struct selected-list)
			  (justify-coordinates struct z-coords prone?)
			  (cond ((typep struct 'organ)
				 (coll:insert-element struct (anatomy pat)))
				((typep struct 'tumor)
				 (coll:insert-element struct (findings pat)))
				((typep struct 'target)
				 (coll:insert-element struct (targets pat)))))
			(destroy panel)))
		     (t (if (sl:confirm '("No image set loaded."
					  "Are you sure you want to proceed?"))
			    (progn
			      (dolist (struct selected-list)
				(cond ((typep struct 'organ)
				       (coll:insert-element
					struct (anatomy pat)))
				      ((typep struct 'tumor)
				       (coll:insert-element
					struct (findings pat)))
				      ((typep struct 'target)
				       (coll:insert-element
					struct (targets pat)))))
			      (destroy panel))
			  (setf (sl:on button) nil))))))
	    (t (setf (sl:on button) nil)))))

    (ev:add-notify issp (sl:button-on namelist-but)
      #'(lambda (panel button)
	  ;clean up buttons made by previous selection if present
	  (dolist (o objects-alist)
	    (sl:delete-button (cdr o) objects-scr))
	  (setq objects-alist '())
	  (setq selected-name nil)
	  (cond
	    ((null (image-set (patient-of panel)))
	     (sl:acknowledge "No image set loaded."))
	    ((probe-file s-file)
	     ; read in structures.index and put them in scrolling list
	     (let ((input-list '())
		   (selected-index nil))
	       (with-open-file (in s-file :direction :input)
		 (loop
		   (let ((s (read in nil nil)))
		     (cond (s (push s input-list))
			   (t (return))))))
	       (setq selected-index
		     (sl:popup-scroll-menu
		       (mapcar
			   #'(lambda (x) (format nil "~A" x))
			 (mapcar #'cdr input-list))
		       700 200))
	       (when selected-index
		 (setq selected-file
		       (format nil
			       "~Apat-~D.structure-set"
			       *structure-database*
			       (car (nth selected-index input-list))))
		 (dolist (obj (get-all-objects selected-file))
		   (cond
		     ((null obj)
		      (sl:acknowledge "Error in object.  It is NIL."))
		     ((null (contours obj))
		      (sl:acknowledge "No contours in object."))
		     ((or (typep obj 'organ)
			  (typep obj 'tumor)
			  (typep obj 'target))
		      (setq selected-name
			    (second (nth selected-index input-list)))
		      (setf (sl:info (pat-name issp)) selected-name)
		      (let ((btn (sl:make-list-button
				   objects-scr
				   (format nil "~A" (name obj)))))
			(sl:insert-button btn objects-scr)
			(setq objects-alist (acons obj btn objects-alist)))
		      ))))))
	    (t (sl:acknowledge "No structure-sets found.")))
	  (setf (sl:on button) nil)))

    (ev:add-notify issp (sl:selected objects-scr)
      #'(lambda (issp objects-scr btn)
	  (declare (ignore issp objects-scr))
	  (let ((object (first (rassoc btn objects-alist))))
	    (setq selected-list (append selected-list (list object)))
	    (format t "~%Selected-list: ~S" selected-list))))

    (ev:add-notify issp (sl:deselected objects-scr)
      #'(lambda (issp objects-scr btn)
	  (declare (ignore issp objects-scr))
	  (let ((object (first (rassoc btn objects-alist))))
	    (format t "~%Object: ~S" object)
	    (setq selected-list (remove object selected-list))
	    (format t "~%New selected-list: ~S" selected-list))))))

;;;-----------------------------------------------------------

(defun justify-coordinates (obj z-coords prone?)

  ;; OBJ is an ORGAN, TUMOR, or TARGET object.
  ;; Z-COORDS is a LIST of Z-coordinates [each a FLONUM] representing
  ;; the Z-coordinate of an image in the patient's image-set.
  ;; PRONE? is T or NIL indicating Prone or Supine, respectively.

  ;; We destructively modify the coordinates of the object [that is,
  ;; the Z of the CONTOUR in the CONTOURS slot of OBJ, and the X and Y
  ;; coords of the contour vertices if PRONE? is T] here.
  ;; That is OK since the object was newly-created via GET-ALL-OBJECTS.

  (dolist (contour-obj (contours obj))
    (do ((structure-z-val (z contour-obj))
	 (coordlist z-coords (cdr coordlist))
	 (image-z-val 0.0)
	 (this-difference 0.0)
	 (best-difference 1000000.0)
	 (best-struc-z-val 0.0))
	((null coordlist)
	 (setf (z contour-obj) best-struc-z-val)

	 ;; If image is oriented prone, rotate contour vertices by 180 degrees
	 ;; by multiplying by -1.0 each of the X and Y coordinates.
	 (when prone?
	   (dolist (vert (vertices contour-obj))
	     ;; Iterate through the two [X and Y] coordinates of each vertex.
	     (do ((coords vert (cdr coords)))
		 ((null coords))
	       (setf (car coords)
		     (* -1.0 (car coords)))))))

      (declare (type list coordlist)
	       (type single-float structure-z-val image-z-val
		     this-difference best-difference best-struc-z-val))

      ;; Find Z coordinate in image nearest Z coordinate in structure-set,
      ;; and assign that coordinate as new Z coordinate for structure-set.
      (setq image-z-val (car coordlist))
      (when (< (setq this-difference (abs (- structure-z-val image-z-val)))
	       best-difference)
	(setq best-struc-z-val image-z-val)
	(setq best-difference this-difference)))))

;;;-----------------------------------------------------------

(defmethod destroy :before ((issp import-structure-set-panel))

  (sl:destroy (cancel-but issp))
  (sl:destroy (accept-but issp))
  (sl:destroy (objects-title issp))
  (sl:destroy (objects-scr issp))
  (sl:destroy (namelist-but issp))
  (sl:destroy (pat-name issp))
  (sl:destroy (panel-frame issp)))

;;;-----------------------------------------------------------

(defun make-import-structure-set-panel (pat &rest initargs)

  (apply #'make-instance 'import-structure-set-panel :patient-of pat initargs))

;;;--------------------------------------------------------------------
;;; End.
