;;;
;;; point-mediators
;;;
;;; Defines mediator for update of points in views
;;;
;;; 25-Apr-1994 J. Unger create.
;;; 22-May-1994 J. Unger condense new-x, new-y, & new-z point events into
;;; a single new-loc event.
;;; 30-Aug-1994 J. Unger add registration for changed display-color
;;;

(in-package :prism)

;;;--------------------------------------

(defclass point-view-mediator (object-view-mediator)

  ()

  (:documentation "This mediator connects a point with a view.")
)

;;;--------------------------------------

(defmethod initialize-instance :after ((pvm point-view-mediator)
                                       &rest initargs)

  (declare (ignore initargs))

  (ev:add-notify pvm (new-loc (object pvm)) #'update-view)
  (ev:add-notify pvm (new-id (object pvm)) #'update-view)
  (ev:add-notify pvm (new-color (object pvm)) #'update-view))

;;;--------------------------------------

(defmethod destroy :after ((pvm point-view-mediator))

  (ev:remove-notify pvm (new-loc (object pvm)))
  (ev:remove-notify pvm (new-id (object pvm)))
  (ev:remove-notify pvm (new-color (object pvm))))

;;;--------------------------------------

(defun make-point-view-mediator (point view)

  (make-instance 'point-view-mediator :object point :view view))

;;;--------------------------------------
