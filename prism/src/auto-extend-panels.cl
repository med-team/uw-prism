;;;
;;; auto-extend-panels
;;;
;;; the little panel that sets the parameters for the extended
;;; autocontour functions in autovolume
;;;
;;; 22-Feb-2004 I. Kalet split off from volume-editor module
;;; 22-Apr-2004 I. Kalet mods to eliminate circular dependencies
;;; 17-May-2004 I. Kalet further fixes to allow direct update of filmstrip
;;; 18-Jun-2009 I. Kalet mods to simplify interface with parent volume
;;; editor panel and autovolume functions.
;;; 17-Jul-2011 I. Kalet add missing pe arg to generate-internal call,
;;; must have been dropped in the reorg.
;;;

(in-package :prism)

;;;----------------------------------

(defclass auto-extend-panel ()

  ((volume-editor :accessor volume-editor
		  :initarg :volume-editor
		  :documentation "The volume editor in which this
		  subpanel appears")

   (ulc-x :accessor ulc-x
	  :initarg :ulc-x
	  :documentation "The upper left corner x coordinate of this
	  window in its parent.")

   (ucl-y :accessor ulc-y
	  :initarg :ulc-y
	  :documentation "The upper left corner y coordinate of this
	  window in its parent.")

   (zplus :accessor zplus
	  :documentation "Z+")

   (zminus :accessor zminus
	   :documentation "Z-")

   (mode :accessor mode
	 :initform :replace
	 :documentation "Mode for handling existing contour: a keyword
symbol, one of :replace, :stop, :skip, :use")

   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame for this subpanel.")

   (zplus-tln :accessor zplus-tln
	      :documentation "The textline for entering the max z value.")

   (zminus-tln :accessor zminus-tln
	       :documentation "The textline for entering the min z value.")

   (mode-btn :accessor mode-btn
	     :documentation "The button that pops up the menu for the
mode to handle existing contours when encountered.")

   (clear-btn :accessor clear-btn
	      :documentation "The button that removes the generated
contours to try again.")

   (extern-btn :accessor extern-btn
	       :documentation "The button that toggles the type of
	       object being contoured, either skin or other.")

   )

  )

;;;----------------------------------

(defun make-auto-extend-panel (vol-ed ulc-x ulc-y)

  (make-instance 'auto-extend-panel 
    :volume-editor vol-ed :ulc-x ulc-x :ulc-y ulc-y))

;;;----------------------------------

(defmethod initialize-instance :after ((pan auto-extend-panel)
				       &rest initargs)

  (let* ((images (images (volume-editor pan)))
	 (btw 150)
	 (bth 25)
	 (frm (sl:make-frame btw (* 3 (+ bth 5))
			     :parent (sl:window (fr (volume-editor pan)))
			     :border-width 0
			     :ulc-x (ulc-x pan)
			     :ulc-y (ulc-y pan)))
	 (frm-win (sl:window frm))
	 (smf (symbol-value *small-font*)) ;; the value, not the symbol
         (zp-tln (apply #'sl:make-textline (- (/ btw 2) 3) bth
			:parent frm-win :font smf
			:ulc-x (+ (/ btw 2) 2) :ulc-y 0
			:label "Z+ "
			:numeric t
			:lower-limit (min-image-z-coord images)
			:upper-limit (max-image-z-coord images)
			initargs))
         (zm-tln (apply #'sl:make-textline (- (/ btw 2) 2) bth
			:parent frm-win :font smf
			:ulc-x 0 :ulc-y 0
			:label "Z- "
			:numeric t
			:lower-limit (min-image-z-coord images)
			:upper-limit (max-image-z-coord images)
			initargs))
         (mode-b (apply #'sl:make-button btw bth
			:parent frm-win :font smf
			:ulc-x 0 :ulc-y (bp-y 0 bth 1)
			:label "Mode: Replace"
			initargs))
	 (clr-b (apply #'sl:make-button (- (/ btw 2) 3) bth
		       :parent frm-win :font smf
		       :ulc-x 0 :ulc-y (bp-y 0 bth 2)
		       :label "Clear" :button-type :momentary
		       initargs))
         (ext-b (apply #'sl:make-button (- (/ btw 2) 2) bth
		       :parent frm-win :font smf
		       :ulc-x (+ (/ btw 2) 2) :ulc-y (bp-y 0 bth 2)
		       :label "External" :button-type :hold
		       initargs))
	 )
    (setf (panel-frame pan) frm
	  (zplus-tln pan) zp-tln
	  (zminus-tln pan) zm-tln
	  (mode-btn pan) mode-b
	  (clear-btn pan) clr-b
	  (extern-btn pan) ext-b)
    (setf (sl:info zp-tln) (max-image-z-coord images)
	  (sl:info zm-tln) (min-image-z-coord images)
	  (zplus pan) (max-image-z-coord images)
	  (zminus pan) (min-image-z-coord images))
    ;; Add events to allow adjustment of characteristics.
    ;; and reaction to users. The changing state of buttons and 
    ;; boxes serves two functions: firstly, they allow the user to
    ;; see what options are chosen, and the panel is passed to 
    ;; the contour-extending routine and used to determine 
    ;; which processes are used for autocontouring.
    ;; The external contour button can switch back and forth between
    ;; three modes: "External", "Vertebrae", and "Internal" These modes 
    ;; all use roughly the same code, but with slightly different inputs 
    ;; and paramaters in places.
    (ev:add-notify pan (sl:new-info zp-tln)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (zplus pnl) (read-from-string info))))
    (ev:add-notify pan (sl:new-info zm-tln)
		   #'(lambda (pnl tln info)
		       (declare (ignore tln))
		       (setf (zminus pnl) (read-from-string info))))
    (ev:add-notify pan (sl:button-on ext-b)
		   #'(lambda (pnl bt)
		       (declare (ignore pnl))
		       (let ((selection (sl:popup-menu '("External"
							 "Inner Organs" 
							 "Vertebrae" ))))
			 (when selection
			   (setf (sl:label bt)
			     (case selection
			       (0 "External")
			       (1 "Inner Organs")
			       (2 "Vertebrae")))))
		       (setf (sl:on bt) nil)))
    ;;the 'clear' button clears ALL contours for the current organ.
    ;;in case the user wishes to auto-generate the entire group.
    (ev:add-notify pan (sl:button-on clr-b)
		   #'(lambda (pnl bt)
		       (if (sl:confirm 
			    (list
			     "Are you sure you want to clear"
			     (format nil "all contours associated with ~A?"
				     (name (volume (volume-editor pnl))))))
			   ;; delete each contour, and then each
			   ;; filmstrip contour. pstruct updated.
			   (dolist (cont (contours
					  (volume (volume-editor pnl))))
			     (fs-delete-contour (volume (volume-editor pnl))
						(z cont)
						(fs (volume-editor pnl)))
			     (setf (contours (volume (volume-editor pnl)))
			       (remove cont
				       (contours (volume
						  (volume-editor pnl)))))
			     (update-pstruct (volume (volume-editor pnl))
					     nil
					     (z cont))))
		       (setf (sl:on bt) nil)))
    (ev:add-notify pan (sl:button-on mode-b)
		   #'(lambda (pnl bt)
		       (declare (ignore pnl))
		       (let ((selection (sl:popup-menu
					 '("Replace" "Stop" "Use" "Ignore"))))
			 (when selection
			   (setf (sl:label bt)
			     (case selection
			       (0 "Mode: Replace")
			       (1 "Mode: Stop")
			       (2 "Mode: Use")
			       (3 "Mode: Ignore")))))
		       (setf (sl:on bt) nil))))
  nil)

;;;----------------------------------

(defmethod destroy ((pan auto-extend-panel))

  (sl:destroy (zplus-tln pan))
  (sl:destroy (zminus-tln pan))
  (sl:destroy (mode-btn pan))
  (sl:destroy (clear-btn pan))
  (sl:destroy (extern-btn pan))
  (sl:destroy (panel-frame pan)))

;;;----------------------------------

(defun generate-extended-contours (pan new-verts)
    
  "generate-extended-contours pan new-verts

Based on user button selection in the auto-extend-panel, selects
and calls the appropriate contour extension routine (external, vertebral,
internal)."

  (let ((min (zminus pan))
	(max (zplus pan))
	(mode (sl:label (extern-btn pan)))
	(ve (volume-editor pan)))
    (cond ((equal mode "External")
	   (generate-externals (window ve) (level ve) (fs ve)
			       (volume ve) (images ve)
			       min max))
	  ((equal mode "Vertebrae")
	   (generate-vertebrae (window ve) (level ve) (fs ve)
			       (volume ve) (images ve)
			       min max))
	  (t (generate-internal (window ve) (level ve) (z ve) (fs ve)
				(pe ve) (volume ve) (images ve)
				min max new-verts)))))

;;;----------------------------------

(defun max-image-z-coord (images)

  "max-image-z-coord images

Returns the z-coordinate of the image with the largest one."

  (let ((max 0))
    (dolist (img images max)
      (if (> (elt (origin img) 2) max) 
	  (setq max (elt (origin img) 2))))))

;;;----------------------------------

(defun min-image-z-coord (images)

  "min-image-z-coord images

Returns the z-coordinate of the image with the smallest z coordinate."

  (let ((min 100))
    (dolist (img images min)
      (if (< (elt (origin img) 2) min) 
	  (setq min (elt (origin img) 2))))))

;;;----------------------------------
;;; End.
