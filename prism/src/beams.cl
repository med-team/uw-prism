;;;
;;; beams
;;;
;;; Definitions of radiation beams or treatment fields, and their
;;; methods.
;;;
;;;  2-Sep-1992 I. Kalet created from old rtp-objects
;;;  7-Sep-1992 I. Kalet make collimator classes, and make collimator
;;;  part of beam, instead of a lot of beam subclasses
;;; 18-Sep-1992 I. Kalet beam panels code moved to beam-panels module
;;; 29-Nov-1992 I. Kalet add a table-position slot to cache this info
;;; 29-Dec-1992 I. Kalet add accessor to blocks slot, change machine
;;; slot type from symbol to string.
;;; 16-Feb-1993 I. Kalet add initialization of machine slot, and
;;; new-machine event, don't save new-machine
;;; 22-Apr-1993 I. Kalet add code for installing different types of
;;; collimators as needed, add new-wedge event and wedge setf method
;;; 24-Aug-1993 J. Unger change tray-factor to atten-factor, add
;;; several more attributes for additional quantities output from 
;;; dose computation program.  [TRAY-FACTOR later added back.]
;;; 11-Oct-1993 J. Unger replace dose-array attribute with dose-result.
;;; 19-Oct-1993 J. Unger fix not-saved method for beams; remove old
;;; attrs, make result invalid when relevant attributes of a beam change.
;;; 26-Oct-1993 I. Kalet change attribute name from dose-result to
;;; result, don't invalidate result when color changes.
;;; 22-Dec-1993 J. Unger make inclusion of result attribute in not-saved
;;; method conditional on value of *save-plan-dose*.
;;;  3-Jan-1994 I. Kalet make table-position a method, not a slot, put
;;;  in pointer to plan instead of keeping a copy of table-position.
;;; 18-Feb-1994 I. Kalet implement copy-beam and make more modular
;;;  5-May-1994 J. Unger split 'valid' into 'valid-points' and 'valid-grid'.
;;; 16-May-1994 I. Kalet add beam-for initialization in collimator.
;;;  1-Jun-1994 I. Kalet make blocks a collection, set beam-for of
;;; each block when inserted into the blocks collection.
;;;  2-Jun-1994 J. Unger add announcement for change to atten-factor.
;;;  3-Jun-1994 J. Unger implement rest of copy-beam operations.
;;;  5-Jun-1994 J. Unger add new-wedge-orient announcement. Also delete the
;;; beam's wedge &reset wedge angle if the machine changes.
;;; 23-Jun-1994 I. Kalet change a lot of floats to single-floats etc.
;;; also move copy-block to beam-blocks.
;;; 24-Jun-1994 J. Unger change wedge to an object.
;;; 05-Jul-1994 J. Unger add code to invalidate dose result if any collimator 
;;; attributes change.
;;; 07-Jul-1994 J. Unger change color to display-color in copy-wedge.
;;; 07-Jul-1994 J. Unger modify copy-beam to take keyword copy-name param.
;;; 12-Jul-1994 J. Unger update plan timestamp if collim attrs change.
;;; 05-Aug-1994 J. Unger make type check in (setf machine) :after method 
;;; more specific.
;;; 24-Aug-1994 J. Unger fix bug in copy-wedge, adj init-inst so a wedge
;;; supplied at initialization time is not overwritten.
;;; 26-Aug-1994 J. Unger fix bug in copy-wedge-rotation.
;;; 29-Aug-1994 J. Unger fix *another* bug in copy-wedge.
;;; 04-Sep-1994 J. Unger move/copy components of (setf collimator) to 
;;; (setf plan-of) and (setf machine) methods.  Also move creation of 
;;; wedge from init-inst to make-beam.
;;; 03-Oct-1994 J. Unger add display-axis & axis-changed attributes.
;;; 04-Oct-1994 J. Unger add keyword to copy-beam to ignore parent plan.
;;; 19-Oct-1994 J. Unger add add-notifies to invalidate beam's dose results
;;; & update plan timestamp when a block's vertices or transmission changes.
;;; Also update plan timestamp when block's name changes.
;;; 07-Nov-1994 J. Unger fix unintentional timestamp update in copy beam.
;;; 22-Jan-1995 I. Kalet put isodist function here and use it lots of
;;;  places.  Move copy-wedge and copy-wedge-rotation to wedges, and
;;;  reparametrize.  Move copy-coll to collimators and reparametrize.
;;;  Remove beam-block method for invalidate-etc. - not needed.  The
;;;  beam should be the target of the announcement, not the block.
;;;  Handle wedge updates here with event notification, not setf
;;;  methods in the wedge module.  Add update-plan event instead of
;;;  operating on plan-of in setf methods.  Don't set beam-for in
;;;  blocks or collimators - no longer a block or collimator attrib.
;;;  Take out table-position - no longer needed.  Take out plan-of,
;;;  not needed.  Put new-coll-set registration in plans module.
;;; 11-Sep-1995 I. Kalet don't pass initargs on to the make-wedge
;;; call, since there should be no relevant parameters in the call to
;;; make-beam.  The new beam will have no wedge.  It can be set afterward.
;;;  5-Jan-1996 I. Kalet delete blocks when new machine has a
;;;  multileaf collimator or is an electron beam, per V1.1 spec.
;;; 15-Jan-1997 I. Kalet add cal-factor function.
;;;  3-Mar-1997 I. Kalet delete blocks when changing collimator type
;;;  to srs as well as the portal collim. types, multileaf and electron.
;;;  5-Jun-1997 I. Kalet in therapy-machine, collimator is now
;;;  collimator-type, make machine return the machine, machine-name
;;;  returns and updates the string in the machine slot.
;;; 29-Aug-1997 BobGian commented-out CAL-FACTOR function - nowhere
;;; used.
;;; 16-Sep-1997 I. Kalet explicitly provide machine database
;;; parameters, as they are no longer optional.
;;; 26-Oct-1997 I. Kalet modify for new wedge id semantics and therapy
;;; machine structure for wedge-info.  No need for fancy wedge
;;; rotation setting, let default be used in make-beam, or value from
;;; file when using get-object in reading in case data.  Similarly
;;; when changing machine.
;;; 19-Dec-1999 I. Kalet copy-block now takes keyword parameter :copy-name
;;; 30-Jan-2000 I. Kalet always delete wedge when copying a beam
;;; 22-Feb-2000 I. Kalet change copy-beam to just copy, defer policies to
;;; places where needed.
;;; 29-Mar-2000 I. Kalet implement copy 180 as reflected-beam function,
;;; treat CNTS and SL20 differently.
;;; 11-Jul-2000 I. Kalet correct misplaced parentheses error in block
;;; reflection code in reflect-beam.
;;; 13-Dec-2000 I. Kalet add drr-cache, so need not recompute pixels
;;; for views, MLC panel and block panel.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defclass beam (generic-prism-object)

  ((machine :type string
	    :initarg :machine
	    :initform (first (get-therapy-machine-list
			      *machine-index-directory*))
	    :accessor machine-name
	    :documentation "The unique string naming the type of
machine used for this beam, e.g., Clinac 2500 6MV, obtained from the
machine database by therapy machine access functions.  The
machine-name accessor returns and updates the string, and the machine
function returns the machine corresponding to the name.")

   (new-machine :type ev:event
		:accessor new-machine
		:initform (ev:make-event))

   (gantry-angle :type single-float
		 :accessor gantry-angle
		 :initarg :gantry-angle)

   (new-gantry-angle :type ev:event
		     :accessor new-gantry-angle
		     :initform (ev:make-event))

   (arc-size :type single-float 
	     :initarg :arc-size
	     :accessor arc-size)

   (new-arc-size :type ev:event
		 :accessor new-arc-size
		 :initform (ev:make-event))

   (collimator :initarg :collimator
	       :accessor collimator
	       :documentation "The collimator is an object, an
instance of one of the various types of collimators that can be on a
machine.  This is an alternate to making a series of beam subclasses
with different types of collimators.")

   (collimator-angle :type single-float
		     :initarg :collimator-angle
		     :accessor collimator-angle)

   (new-coll-angle :type ev:event
		   :accessor new-coll-angle
		   :initform (ev:make-event))

   (monitor-units :type single-float 
		  :initarg :monitor-units
		  :accessor monitor-units)

   (new-mu :type ev:event
	   :accessor new-mu
	   :initform (ev:make-event))

   (n-treatments :type integer 
		 :initarg :n-treatments
		 :accessor n-treatments)

   (new-n-treats :type ev:event
		 :accessor new-n-treats
		 :initform (ev:make-event))

   (couch-lateral :type single-float
		  :initarg :couch-lateral
		  :accessor couch-lateral)

   (new-couch-lat :type ev:event
		  :accessor new-couch-lat
		  :initform (ev:make-event))

   (couch-longitudinal :type single-float 
		       :initarg :couch-longitudinal
		       :accessor couch-longitudinal)

   (new-couch-long :type ev:event
		   :accessor new-couch-long
		   :initform (ev:make-event))

   (couch-height :type single-float
		 :initarg :couch-height
		 :accessor couch-height)

   (new-couch-ht :type ev:event
		 :accessor new-couch-ht
		 :initform (ev:make-event))

   (couch-angle :type single-float
		:initarg :couch-angle
		:accessor couch-angle)

   (new-couch-angle :type ev:event
		    :accessor new-couch-angle
		    :initform (ev:make-event))

   (wedge :type wedge
          :initarg :wedge
	  :initform (make-wedge)
          :accessor wedge
          :documentation "The beam's wedge object.")

   (atten-factor :type single-float 
		 :initarg :atten-factor
		 :accessor atten-factor
                 :documentation "A factor between 0.0 and 1.0 which is
is used in dose computation to attenuate the strength of the beam.")

   (new-atten-factor :type ev:event
		     :accessor new-atten-factor
		     :initform (ev:make-event)
		     :documentation "Announced when the attenuation factor
changes.")

   (blocks :accessor blocks
	   :initform (coll:make-collection)
	   :documentation "A collection of beam-block objects")

   (display-color :initarg :display-color
		  :accessor display-color)

   (new-color :type ev:event
	      :accessor new-color
	      :initform (ev:make-event))
   
   (update-plan :type ev:event
		:accessor update-plan
		:initform (ev:make-event))

   (result :type dose-result
	   :initarg :result
	   :accessor result
	   :initform (make-dose-result)
	   :documentation "The result of computing dose from this beam
is stored in the beam's result.")

   (display-axis :type (member t nil)
                 :initarg :display-axis
                 :accessor display-axis
                 :documentation "A boolean attribute which, when non-nil,
causes the beam's central axis and tic marks to appear in a view when the
beam's isocenter lies in the plane of the view.")

  (axis-changed :type ev:event
                :accessor axis-changed
                :initform (ev:make-event))

  (drr-cache :accessor drr-cache
	     :initform nil
	     :documentation "A place to cache the pixel array for a
DRR for this beam, so it can appear in several places without
recomputing.")

  )

  (:default-initargs :gantry-angle 0.0
		     :arc-size 0.0 
		     :collimator-angle 0.0
		     :monitor-units 100.0
		     :n-treatments 1
		     :couch-lateral 0.0
		     :couch-longitudinal 0.0
		     :couch-height 0.0
		     :couch-angle 0.0
		     :atten-factor 1.0
		     :display-color 'sl:red
                     :display-axis t)

  (:documentation "Class beam defines the generic treatment parameters
for a beam, without specifics about the collimator type.  The wedge
slot identifies which wedge object is in use from the set available
for the machine in question.  If arc-size is 0 the beam is a fixed
field, otherwise gantry-angle specifies the starting point of the
moving field.")

  )

;;;---------------------------------------------

(defmethod machine ((bm beam))

  "returns the actual machine, not the string naming it."

  (get-therapy-machine (machine-name bm)
		       *therapy-machine-database*
		       *machine-index-directory*))

;;;---------------------------------------------

(defmethod slot-type ((object beam) slotname)

  (case slotname
	(blocks :collection)
	((collimator result wedge) :object)
	(plan-of :ignore)
	(otherwise :simple)))

(defmethod not-saved ((object beam)) 

  (append (call-next-method)
	  '(new-machine new-gantry-angle new-arc-size new-coll-angle
	    new-mu new-n-treats new-couch-lat new-couch-long
	    new-couch-ht new-couch-angle new-color axis-changed
	    new-atten-factor result update-plan drr-cache)))

;;;---------------------------------------------

(defmethod invalidate-results ((bm beam) &rest ignored)

  "invalidate-results (bm beam) &rest ignored

An action function that invalidates a beam's dose results and
announces update-plan event.  Called in response to various changes to
beam attributes, and attributes of a beam's component objects."

  (declare (ignore ignored))
  (setf (valid-grid (result bm)) nil)
  (setf (valid-points (result bm)) nil)
  (ev:announce bm (update-plan bm)))

;;;---------------------------------------------

(defmethod initialize-instance :after ((b beam) &rest initargs)

  "this method makes sure that addition of a block to beam b
invalidates b's dose results, and registers changes in the block's
parameters."

  (declare (ignore initargs))
  (ev:add-notify b (coll:inserted (blocks b))
  		 #'(lambda (bm blkset blk)
		     (declare (ignore blkset))
                     (invalidate-results bm)
                     (ev:add-notify bm (new-vertices blk)
				    #'invalidate-results)
                     (ev:add-notify bm (new-transmission blk)
				    #'invalidate-results)
                     (ev:add-notify bm (new-name blk)
				    #'(lambda (bm ann val)
					(declare (ignore ann val))
					(ev:announce bm (update-plan bm))))))
  (ev:add-notify b (coll:deleted (blocks b))
  		 #'(lambda (bm blkset blk)
                     (declare (ignore blkset))
                     (invalidate-results bm)
                     (ev:remove-notify bm (new-vertices blk))
                     (ev:remove-notify bm (new-transmission blk))
                     (ev:remove-notify bm (new-name blk)))))

;;;---------------------------------------------

(defun make-beam (beam-name &rest initargs)

  "make-beam beam-name &rest initargs

returns a new instance of a beam, with specified parameters, but no
wedge and no blocks."

  (let ((b (apply #'make-instance 'beam
		  :name (if (equal beam-name "")
			    (format nil "~A" (gensym "BEAM-"))
			  beam-name)
		  initargs)))
    (setf (collimator b) ;; after method does event registration
      (apply #'make-instance
	     (collimator-type (machine b))
	     :allow-other-keys t
	     initargs))
    (ev:add-notify b (new-id (wedge b)) ;; wedge is initialized above
		   #'invalidate-results)
    (ev:add-notify b (new-rotation (wedge b))
		   #'invalidate-results)
    b))

;;;---------------------------------------------

(defmethod (setf name) :after (new-name (bm beam))

  (declare (ignore new-name))
  (ev:announce bm (update-plan bm)))

;;;---------------------------------------------

(defmethod (setf machine-name) :after (new-mach (b beam))

  "If collimator type has changed, create new collimator and set
reasonable values based on old.  Also set wedge ID to 0."

  ;; invalidating dose here covers when the collimator type changes,
  ;; so we don't have to repeat this in a (setf collimator) method.
  (invalidate-results b)
  (setf (drr-cache b) nil)
  (let ((new-coll-type (collimator-type (machine b)))
	(old-coll (collimator b)))
    (unless (eql (type-of old-coll) new-coll-type)
      (setf (collimator b) (replace-coll old-coll new-coll-type))
      (typecase (collimator b)
	(portal-coll
	 (dolist (blk (coll:elements (blocks b)))
	   (coll:delete-element blk (blocks b))))))
    (setf (id (wedge b)) 0))
  (ev:announce b (new-machine b) new-mach))

;;;---------------------------------------------

(defmethod (setf gantry-angle) :after (new-angle (b beam))

  (invalidate-results b)
  (setf (drr-cache b) nil)
  (ev:announce b (new-gantry-angle b) new-angle))

;;;---------------------------------------------

(defmethod (setf arc-size) :after (new-size (b beam))

  (invalidate-results b)
  (ev:announce b (new-arc-size b) new-size))

;;;---------------------------------------------

(defmethod (setf collimator-angle) :after (new-angle (b beam))

  (invalidate-results b)
  (ev:announce b (new-coll-angle b) new-angle))

;;;---------------------------------------------

(defmethod (setf collimator) :after (new-coll (b beam))

  (ev:add-notify b (new-coll-set new-coll)
		 #'invalidate-results))

;;;---------------------------------------------

(defmethod (setf monitor-units) :after (new-units (b beam))

  (ev:announce b (update-plan b))
  (ev:announce b (new-mu b) new-units))

;;;---------------------------------------------

(defmethod (setf n-treatments) :after (new-n (b beam))

  (ev:announce b (update-plan b))
  (ev:announce b (new-n-treats b) new-n))

;;;---------------------------------------------

(defmethod (setf couch-lateral) :after (new-value (b beam))

  (setf (drr-cache b) nil)
  (invalidate-results b)
  (ev:announce b (new-couch-lat b) new-value))

;;;---------------------------------------------

(defmethod (setf couch-longitudinal) :after (new-value (b beam))

  (setf (drr-cache b) nil)
  (invalidate-results b)
  (ev:announce b (new-couch-long b) new-value))

;;;---------------------------------------------

(defmethod (setf couch-height) :after (new-value (b beam))

  (setf (drr-cache b) nil)
  (invalidate-results b)
  (ev:announce b (new-couch-ht b) new-value))

;;;---------------------------------------------

(defmethod (setf couch-angle) :after (new-angle (b beam))

  (setf (drr-cache b) nil)
  (invalidate-results b)
  (ev:announce b (new-couch-angle b) new-angle))

;;;---------------------------------------------

(defmethod (setf display-color) :after (new-c (b beam))

  (ev:announce b (new-color b) new-c))

;;;---------------------------------------------

(defmethod (setf display-axis) :after (new-val (b beam))

  (ev:announce b (axis-changed b) new-val))

;;;---------------------------------------------

(defmethod (setf atten-factor) :after (new-val (b beam))

  (invalidate-results b)
  (ev:announce b (new-atten-factor b) new-val))

;;;---------------------------------------------

(defun isodist (bm)

  "isodist bm

The source to axis distance of the beam bm."

  (cal-distance (machine bm)))

;;;---------------------------------------------

(defmethod copy ((bm beam))

  "copy bm 

creates a copy of the beam bm, including the blocks and the wedge.  If
a more complex copy protocol, i.e., reflecting the blocks, is desired,
explicitly modify the new, copied, beam afterward."

  (let ((new-b (make-beam (name bm)
			  :machine (machine-name bm)
			  :gantry-angle (gantry-angle bm)
			  :arc-size (arc-size bm)
			  :collimator-angle (collimator-angle bm)
			  :monitor-units (monitor-units bm)
			  :n-treatments (n-treatments bm)
			  :couch-lateral (couch-lateral bm)
			  :couch-longitudinal (couch-longitudinal bm)
			  :couch-height (couch-height bm)
			  :couch-angle (couch-angle bm)
			  :atten-factor (atten-factor bm))))
    (setf (collimator new-b) (copy (collimator bm)))
    (setf (id (wedge new-b)) (id (wedge bm)))
    (setf (rotation (wedge new-b)) (rotation (wedge bm)))
    (dolist (blk (coll:elements (blocks bm)))
      (coll:insert-element (copy blk) (blocks new-b)))
    new-b))

;;;---------------------------------------------

(defun reflected-beam (bm)

  "reflected-beam bm

returns a copy of bm, reflected 180 degrees, with a rather complex
protocol concerning collimator rotation and settings."

  (let* ((new-beam (copy bm))
	 (blklist (coll:elements (blocks new-beam)))
	 (col-angle (collimator-angle new-beam))
	 (reflect-y nil))
    (setf (name new-beam) (format nil "~A" (gensym "BEAM-")))
    (setf (gantry-angle new-beam)
      (mod (+ (gantry-angle bm) 180.0) 360.0))
    ;; this is the hairy part
    (typecase (collimator new-beam)
      (symmetric-jaw-coll (if (and (member col-angle '(90.0 270.0))
				   (zerop (id (wedge new-beam))))
			      (setf reflect-y t)
			    (unless (zerop col-angle)
			      (setf (collimator-angle new-beam)
				(- 360.0 col-angle)))))
      (combination-coll (if (and (member col-angle '(90.0 270.0))
				 (zerop (id (wedge new-beam))))
			    (setf reflect-y t)
			  (let ((tmp (x-inf (collimator new-beam))))
			    (setf (x-inf (collimator new-beam))
			      (x-sup (collimator new-beam))
			      (x-sup (collimator new-beam)) tmp)
			    (unless (zerop col-angle)
			      (setf (collimator-angle new-beam)
				(- 360.0 col-angle))))))
      (variable-jaw-coll (if (and (member col-angle '(90.0 270.0))
				  (zerop (id (wedge new-beam))))
			     (let ((tmp (y-inf (collimator new-beam))))
			       (setf (y-inf (collimator new-beam))
				 (y-sup (collimator new-beam))
				 (y-sup (collimator new-beam)) tmp
				 reflect-y t))
			   (let ((tmp (x-inf (collimator new-beam))))
			     (setf (x-inf (collimator new-beam))
			       (x-sup (collimator new-beam))
			       (x-sup (collimator new-beam)) tmp)
			     (unless (zerop col-angle)
			       (setf (collimator-angle new-beam)
				 (- 360.0 col-angle))))))
      (cnts-coll (if (member col-angle '(90.0 270.0))
		     (let ((tmp (y-inf (collimator new-beam))))
		       (setf (y-inf (collimator new-beam))
			 (y-sup (collimator new-beam))
			 (y-sup (collimator new-beam)) tmp
			 reflect-y t))
		   (let ((tmp (x-inf (collimator new-beam))))
		     (setf (x-inf (collimator new-beam))
		       (x-sup (collimator new-beam))
		       (x-sup (collimator new-beam)) tmp)
		     (unless (zerop col-angle)
		       (setf (collimator-angle new-beam)
			 (- 360.0 col-angle))))))
      (multileaf-coll (cond ((member col-angle '(90.0 270.0))
			     (if (= (id (wedge new-beam)) 5) ;; internal wedge
				 (setf (collimator-angle new-beam)
				   (- 360.0 col-angle))))
			    ((member col-angle '(0.0 180.0))
			     (unless (member (id (wedge new-beam)) '(0 5))
			       (setf (collimator-angle new-beam)
				 (mod (+ (collimator-angle new-beam) 180)
				      360))))
			    (t (setf (collimator-angle new-beam)
				 (- 360.0 col-angle))))
		      (setf (vertices (collimator new-beam))
			(mapcar #'(lambda (pt)
				    (list (- (first pt)) (second pt)))
				(vertices (collimator new-beam)))))
      (electron-coll (setf (vertices (collimator new-beam))
		       (mapcar #'(lambda (pt)
				   (list (- (first pt)) (second pt)))
			       (vertices (collimator new-beam))))))
    (setf (id (wedge new-beam)) 0)
    ;; reflect either the x or y coordinates of the block vertices
    (dolist (blk blklist)
      (setf (vertices blk)
	(if reflect-y
	    (mapcar #'(lambda (pt) (list (first pt) (- (second pt))))
		    (vertices blk))
	  (mapcar #'(lambda (pt) (list (- (first pt)) (second pt)))
		  (vertices blk)))))
    new-beam))

;;;---------------------------------------------
;;; End.
