;;;
;;; points
;;;
;;; defines points of interest in a patient case
;;;
;;;  3-Sep-1993 I. Kalet created from contours module
;;; 25-Apr-1994 J. Unger enhance definition - add events & id attribute,
;;;             events, setf methods, etc.
;;; 19-May-1994 J. Unger adj make-point to allow for additional initargs,
;;;             move assignment of id to 2d-point editor.
;;; 22-May-1994 J. Unger condense new-x, new-y, & new-z events into one.
;;; 25-May-1994 J. Unger take name out of not-saved list.
;;; 30-Aug-1994 J. Unger add new-color announcement.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass mark (generic-prism-object)

  ((x :type single-float 
      :accessor x
      :initarg :x
      :documentation "The mark's x coordinate.")

   (y :type single-float
      :accessor y 
      :initarg :y
      :documentation "The mark's y coordinate.")

   (z :type single-float 
      :accessor z
      :initarg :z
      :documentation "The mark's z coordinate.")

   (new-loc :type ev:event
	    :accessor new-loc
	    :initform (ev:make-event)
	    :documentation "Announced when the mark's x, y, or z attribute 
changes.")

   (id :type fixnum
       :initarg :id
       :reader id
       :documentation "The id is a read-only (for now) attribute that is
assigned when a point is created.  The id for the first point created
is 1, and increases by 1 for successively created points.")

   (new-id :type ev:event
          :accessor new-id
          :initform (ev:make-event)
          :documentation "Announced when the mark's id attribute changes.")

   (display-color :initarg :display-color
		  :initform 'sl:yellow
		  :accessor display-color)

   (new-color :type ev:event
	      :initform (ev:make-event)
	      :accessor new-color
	      :documentation "Announced by setf method when
display-color is updated.")

   )

  (:documentation "A mark is a point within the patient that marks
some anatomic landmark or a point where the dose needs to be known.")

  )

;;;--------------------------------------

(defmethod not-saved ((pt mark))

   (append (call-next-method)
    '(new-loc new-id new-color)))

;;;--------------------------------------

(defmethod (setf x) :after (val (pt mark))

  (declare (ignore val))
  (ev:announce pt (new-loc pt) (list (x pt) (y pt) (z pt))))

;;;--------------------------------------

(defmethod (setf y) :after (val (pt mark))

  (declare (ignore val))
  (ev:announce pt (new-loc pt) (list (x pt) (y pt) (z pt))))

;;;--------------------------------------

(defmethod (setf z) :after (val (pt mark))

  (declare (ignore val))
  (ev:announce pt (new-loc pt) (list (x pt) (y pt) (z pt))))

;;;--------------------------------------

(defmethod (setf display-color) :after (clr (pt mark))

  (ev:announce pt (new-color pt) clr))

;;;--------------------------------------

(defun make-point (point-name &rest initargs)

  "MAKE-POINT point-name &rest initargs

Returns a mark object with specified parameters."

  (apply #'make-instance 'mark 
            :name (if (equal point-name "")
                        (format nil "~A" (gensym "POINT-"))
                        point-name)
    initargs)
  )

;;;--------------------------------------
