;;;
;;; dicom-rtplan
;;;
;;; Support for Dicom RT Plan and related modules.
;;; Contains functions used in Client only.
;;;
;;; 29-Jun-2000  J. Jacky  Separate out from dicom-panel.cl
;;;  2-Aug-2000  J. Jacky  Finish first version with stub send-dicom
;;;    Aug-2000   BobGian  Fill in send-dicom, add delistify-leaves,
;;;                         cm to mm conversion, other minor revisions
;;; 30-Aug-2000  J. Jacky  Add beam type, radiation type, machine name,
;;;                         beam energy
;;;                        Put tags in ascending tag number order
;;; 31-Aug-2000  J. Jacky  More tag order fixes
;;;                        Add several missing type 1 and 2 attributes
;;;  1-Sep-2000  J. Jacky  More minor fixes
;;;                        delistify-leaves: rearrange leaf order also
;;;  5-Sep-2000  J. Jacky  Prism = IEC1217 so don't scale gan-,couch-,coll-rot
;;;                        delistify-leaves: also reverse y-ordering of leaves
;;;  6-Sep-2000  J. Jacky  Y diaphragms(coll-ymin,ymax) cover all closed leaves
;;;  8-Sep-2000  J. Jacky  assemble-beams:use cached leaf-settings,don't recalc
;;; 11-Sep-2000  J. Jacky  dicom-date-time: fix single-digit-hour bug
;;;                        Calc coll-xmin etc. from new x1 etc. collimator
;;;  8-Nov-2000  J. Jacky  Always include Wedge Position Seq. #x0116 in ctrl pt
;;;                         so can simplify list construction, omit appends
;;;                        Cumulative Meterset Weight #x0134 is always 100
;;;                         in static field
;;;                        Final Cum. Meterset Weight #x010E is always 100
;;;                        Absolute monitor units specified only in
;;;                         Beam Meterset #x0086
;;; 13-Nov-2000  J. Jacky  In Wedge Pos. Seq., put Pos,Num in ascending order
;;;                        Nest Wedge Pos. Seq. one level deeper using (list )
;;;  1-Dec-2000  J. Jacky  Rename Dicom.Log to ~/dicom/log/dicom.dat
;;;                        Rename *dicom-log-file* to *dicom-data-file*
;;;                        Rename log-dicom to log-dicom-data
;;;                        New log-dicom-transfer
;;;  8-Dec-2000  J. Jacky  send-dicom: collect, return RUN-CLIENT status, msgs
;;; 19-Jun-2001  J. Jacky  send-dicom: handle :send-enabled feature
;;; 21-Jun-2001  J. Jacky  use Prism machine ident not name for machine-name B2
;;;                        use Prism machine name for beam description C3
;;; 21-Jun-2001   BobGian  remove :send-enabled feature for "production" vers.
;;; 27-Jul-2001  J. Jacky  read-from-string with :start to get case-id
;;;                        Revise description items so RTD display more helpful
;;; 31-Aug-2001   BobGian  SEND-DICOM and RUN-CLIENT return status message
;;;                        as single string rather than as list of strings.
;;; 14-Sep-2001   J. Jacky assemble-fractions,-beams: handle segment info
;;; 18-Sep-2001   J. Jacky assemble-fractions,-beams: use new beam-rec,seg-rec
;;; 19-Sep-2001   J. Jacky assemble-fractions: preprocess out segments first
;;;                        replace beam-rec with list
;;; 24-Sep-2001   J. Jacky replace assemble-beams w/new assemble-beam-sequence
;;; 28-Sep-2001   J. Jacky remove r-mu-per-frac, not useful for segmented beams
;;;  1-Oct-2001   J. Jacky assemble-control-point: include wedge, collim at cp1
;;; 26-Oct-2001   J. Jacky assemble-beam: represent ext. wedge as shadow tray
;;;  2-Nov-2001   J. Jacky assemble-beam: represent ext. blocks as shadow tray
;;;  5-Dec-2001   J. Jacky assemble-dicom: dicom-pat-id argument
;;; 10-Dec-2001   J. Jacky log-dicom-transfer: log new dicom-pat-id
;;; 23-Jan-2002    BobGian *dicom-data-file* -> *pdr-data-file* (maybe temp).
;;; 18-Feb-2002    BobGian dicom::*dicom-log-dir* -> Prism pkg.
;;;  9-Apr-2002    J.Jacky assemble-control-point: fix round error in cp0-mu
;;;  9-May-2002    BobGian arg to RUN-CLIENT changed from :C-Store-RTPlan
;;;                        to :C-Store-RTPlan-RQ (consistency w frag version).
;;; 19-Jun-2002    J.Jacky assemble beam: make 300A,00C4 "STATIC" not "DYNAMIC"
;;; 16-Sep-2002    J.Jacky Remove client-ae-title argument from RUN-CLIENT
;;; 27-Aug-2003    BobGian add Dose-Monitoring Points.
;;; 08-Sep-2003    BobGian ASSEMBLE-FRACTIONS -> ASSEMBLE-FRACTION-GROUPS.
;;; 19-Sep-2003    BobGian DMP carries coords in Dicom convention [in mm,
;;;                        rounded to fixed precision], so these are used to
;;;                        construct data rather than rounding here.
;;; 03-Oct-2003 BobGian: Change defstruct name and slot names in SEG-REC-...
;;;   to SEGMENT-DESCRIPTOR-... to make DMP code more readable.
;;;   Ditto with a few local variables.
;;;   STATIC, DYNAMIC, SEGMENT (Prism pkg) -> Keyword pkg.
;;; 20-Oct-2003 BobGian: Move DMP defstruct "dicom-panel" -> here
;;;   to simplify dependencies.
;;; 30-Oct-2003 BobGian: dicom::DUMP-DICOM-DATA, new pretty-printer and data
;;;   formatter, replaces old LOG-DICOM-DATA to enable finer-grained
;;;   debugging.  Same dumper is used for Server, so code is moved to
;;;   "utilities.cl" (file common to both).  LOG-DICOM-DATA serves
;;;   as driver to send dicom::DUMP-DICOM-DATA output to file.
;;;   Also much debugging to finish and correct implementation of DMPs.
;;; 14-Nov-2003 BobGian: Modularize function ASSEMBLE-BEAM-SEQUENCE.
;;;   More testing and debugging.
;;; 18-Nov-2003 BobGian:
;;;   1: Fixed a few stubborn bugs involving pooling of DMPs over all segments
;;;      in a beam.  Tests producing dumped output now match desired [from
;;;      Dicom spec] completely.  Ready for testing against real Elekta server.
;;;   2: If slot is empty, tag not sent either [for DMPs only - some tags and
;;;      empty slots are required for other items].
;;;   3: Implemented and optimized uniquization of DMPs.
;;;   4: DMP auto-replication [one per beam + one for group of shared beams]
;;;      added - must be in post-processing [while generating data stream,
;;;      after all user-interface operations completed].
;;;   5: ASSEMBLE-BEAM-SEQUENCE modularized/factored for better clarity.
;;; 19-Nov-2003 BobGian: Correct implementation of auto-replication of DMPs
;;;   to get one for per-beam use and one for use shared by common beams.
;;; 21-Nov-2003 BobGian:
;;;   1: Plan name, slots 300A:0002 and 300A:0003 (formerly timestamp) changed.
;;;      If Prism plan name fits in 16 chars, 300A:0002 holds it and 300A:0003
;;;      is present but empty.  If plan name fits in 64 chars, 300A:0002 is a
;;;      dummy stub "RT-Plan" while 300A:0003 holds Prism plan name.  Ditto if
;;;      plan name > 64 chars except plan name is truncated to 64 chars.  This
;;;      is because 300A:0002 is limited to 16 and 300A:0003 to 64 chars.
;;;   2: Rounding after cGy -> Gy removed because cGy inputs already are
;;;      represented as fixnum values anyway.
;;; 24-Nov-2003 BobGian:
;;;   1: DMP auto-replication scheme altered.  DMP slots renamed.
;;;      TOTAL-DOSE, DAILY-DOSE, and PRIOR-DOSE are accumulated values for
;;;      a single beam only.  If same DMP is selected for another beam too
;;;      [ie, becomes shared between beams], existing values of these slots
;;;      are pushed onto stacks held in slots OTHER-xxx-DOSES and current slots
;;;      are cleared any used for current-beam-only values.  When data are
;;;      passed from Dicom panel to the Dicom interface, ADD-SEG-INFO expands
;;;      and auto-replicates shared DMPs automatically, using the current and
;;;      the OTHER-xxx-DOSES slot values.  [See changelog notes, same date, in
;;;      files "dicom-panel" and "imrt-segments".]
;;;   2: Added NAME slot to DMP; holds POINT name unless DMP is shared by more
;;;      than one beam, in which case each auto-replicated DMP gets a name
;;;      formed by concatenating point name and beam name.
;;; 26-Nov-2003 BobGian:
;;;   1: DMP auto-replication scheme polished, esp dose coefficient portion.
;;;   2: Plan name [see 21-Nov-2003, #1] modified to put full plan name in
;;;      slot 300A:0002 and leave out 300A:0003 if name fits.  Otherwise, put
;;;      first 16 chars in 300A:0002 and rest in 300A:0003 [truncated to 64
;;;      chars total if necessary].
;;;   3: Fixed LOG-DICOM-TRANSFER - wrong tag group number for plan timestamp.
;;;   4: Slot 300A:0004 Plan Description truncated to 1024 chars if necessary.
;;;   5: Slot 300A:00C2 Beam Name truncated to 64 chars if necessary.
;;;      300A:0016 DMP Name limited to 64 chars, checked when DMP created.
;;;      0010:0010 and 0010:1000 limited to 64 chars but checked elsewhere.
;;; 28-Nov-2003 BobGian: Move DMP defstruct here -> "imrt-segments" to
;;;   simplify dependencies.
;;; 01-Dec-2003 BobGian:
;;;   1: Fix slots where dose may contribute to single DMP from multiple beams
;;;      (300A:001A, 300A:0027).
;;;   2: Fix slots where dose at DMP is calculated as proportion of dose to
;;;      result object (300A:0084, 300A:010C).
;;; 04-Dec-2003 BobGian: SEGMENT-DESCRIPTOR-... -> SEGDATA-... (less clutter).
;;; 25-Dec-2003 BobGian: Flushed all "...OTHER-..." slots.  Now allocate a
;;;    separate DMP object for each segment in which the DMP appears, linking
;;;    them through the list in the DMP-SEGLIST slot of each so that dose can
;;;    be accumulated properly across all segments in a single beam.
;;; 28-Dec-2003 BobGian: DMP slots ...-dose -> ...-cGy to emphasize dose units.
;;; 27-Jan-2004 BobGian began integration of new DMP Panel by Mark Phillips
;;;    with rest of Dicom Panel and interface to Dicom SCU.
;;; 19-Feb-2004 BobGian: Modfied norm-point mechanism to use a fictitious
;;;    norm point with dose 1.0 Gray and coordinates (0.0 0.0 0.0)
;;;    [coordinates are ignored by Elekta].
;;; 19-Feb-2004 BobGian - introduced uniform naming convention explained
;;;    in file "imrt-segments".  This includes:
;;;    SEGDATA-... -> PR-BEAM-...
;;; 26-Feb-2004 BobGian completed DMP integration.
;;; 27-Feb-2004 BobGian added constraint checking on DMPs: every DMP receives
;;;    dose, every DMP is in some beam, and every beam has at least one DMP.
;;;    Constraints checked when Send-Dicom button is pressed.
;;; 01-Mar-2004 BobGian more constraint-checking on beams/DMPs about to be
;;;    sent out of ASSEMBLE-DICOM to new fcn CHECK-BEAM-CONSTRAINTS called
;;;    on Send-Beams button press immediately before ASSEMBLE-DICOM.
;;; 07-Mar-2003 BobGian: Fixed bug in CUM-DOSE-DATA to track segment doses at
;;;    each DMP properly when accumulating doses for control point sequence.
;;;    Added NFRAC arg to CUM-MU-DATA and CUM-DOSE-DATA.  Removed one
;;;    superfluous argument from each.
;;; 10-Mar-2004 BobGian: DI-DMP-PRIOR-CGY and DI-DMP-TOTAL-CGY changed
;;;    to type FIXNUM to better accord with rounding conventions used by
;;;    Elekta RT-Desktop: integral centigray doses.
;;; 04-Apr-2004 BobGian: Change DI-DMP-TOTAL-CGY to record EITHER computed
;;;    dose or user-textline-typed dose [using DI-DMP-DOSE-TYPE to indicate
;;;    via value :Computed or :User, respectively], with per-control-point
;;;    DMP segment dose calculated accordingly: from Prism segment doses for
;;;    :Computed and from DI-DMP-TOTAL-CGY divided equally per fraction
;;;    and per contributing Dicom beam for :User dose-types.  Changes to mode
;;;    of segment-dose calculation are in CUM-DOSE-DATA.
;;; 08-Apr-2004 BobGian: Simplified logic for [TRAY-]ACCESSORY-CODE.
;;; 29-Apr-2004 BobGian - To fix MU and dose roundoff problems:
;;;   1. Add 300A:00B3 to Beams Module (Primary Dosimeter Unit, Value: "MU")
;;;   2. Convert 300A:0086 "Beam Meterset" integer -> float.
;;;   3. Convert 300A:010E "Final Cumulative Meterset Weight" integer -> float.
;;;   4. Convert 300A:0134 "Cumulative Meterset Weight" integer -> float.
;;;  Also STRING-TRIMmed leading/trailing spaces in 300A:00C2 "Beam Name".
;;; 17-May-2004 BobGian complete process of extending all instances of Original
;;;    and Current Prism beam instances to include Copied beam instance too,
;;;    to provide copy for comparison with Current beam without mutating
;;;    Original beam instance.
;;; 01-Jul-2004 BobGian: Energy sent at all control points so that it can vary
;;;    from segment to segment.  This is legal in DICOM standard and is
;;;    accepted by Elekta.  No harm comes from sending it every segment even
;;;    in cases where it does not vary.
;;; 15-Jul-2004 BobGian: SEND-DICOM prints MACH-NAME, MACH-ID, and MACH-IDENT
;;;    to background window using "~S" rather than "~A".
;;; 07-Sep-2004 BobGian: Prepend plan timestamp to 300A:0004 (Plan descrip).
;;; 10-Sep-2004 BobGian: Pass DMP list to LOG-DICOM-DATA via SEND-DICOM.
;;;    LOG-DICOM-DATA now prints information about each DMP, including
;;;    DMP number, DMP name, name of original Prism point, Prior-cGy,
;;;    Total-cGy, and dose type (computed by Prism or typed by user).
;;;    This is followed by formatted dump of data-stream sent to server.
;;; 12-Sep-2004 BobGian: Modify CUM-MU-DATA to use renamed and new slots
;;;    PR-BEAM-CUM-MU-INC and PR-BEAM-CUM-MU-EXC, allowing EXACT computation
;;;    of MU on accumulating segment MU values without roundoff accumulation
;;;    between control points, which was triggering a bug in Elekta server.
;;; 19-Sep-2004 BobGian: Add to CHECK-BEAM-CONSTRAINTS check that each
;;;    radiating segment [ie, all segments for us] has at least 1.0 MU.
;;;    This is an Elekta-specific constraint.
;;; 05-Oct-2004 BobGian fixed a few lines to fit within 80 cols.
;;; 12-Oct-2004 BobGian "treated" -> "prior" in log and in description of
;;;    "previously-treated" dose and DI-DMP-TREATED-CGY -> DI-DMP-PRIOR-CGY
;;;    slot name change, for better consistency with Dicom-RT standard and
;;;    Elekta documentation.  DI-DMP-TOTAL-CGY -> DI-DMP-ACCUM-CGY and
;;;    DI-DMP-TOTAL-CGY to fix inconsistency between spec and implementation.
;;;    Modify computation of DMP dose to get Total-cGy = Prior-cGy + Accum-cGy.
;;; 14-Oct-2004 BobGian modify LOG-DICOM-DATA to print Prior, Accum, and Total
;;;    dose for each DMP [was Prior and incorrect Total before].
;;; 04-Nov-2004 BobGian add default error message for *STATUS-ALIST*.
;;; 17-Feb-2005 A. Simms replace Allegro getenv with misc.cl wrapper getenv.
;;; 26-Jun-2005 I. Kalet replace single-float call with coerce
;;;  6-Jul-2007 I. Kalet replace remaining single-float calls missed prev.
;;;

(in-package :prism)

;;;=============================================================

(defun check-beam-constraints (p-bm-info d-dmp-list)

  "check-beam-constraints p-bm-info d-dmp-list

checks DMP/Beam constraints.  Returns T -> no constraints are violated
or some are but user chooses to proceed anyway, NIL -> violations and user
chooses not to continue."

  ;; P-BM-INFO is a list, in forward order, each entry being:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <Prism-Beam-Object> )
  ;; with one entry for each segment - constructed by GENERATE-PBEAM-INFO.

  (dolist (d-dmp-obj d-dmp-list)
    ;; Calculate total dose at DMP, if user has not previously
    ;; set it from textline or calculated it via "Calc-Dose" button.
    ;; If computed here, set DI-DMP-DOSE-TYPE to :Computed.
    (unless (di-dmp-dose-type d-dmp-obj)
      (let ((accum-dose 0.0))
	(declare (type single-float accum-dose))
	;; For each DMP, iterate over all the Dicom-Beams
	;; contributing to that DMP and all the segment [ORIG-PBI] doses
	;; making up the segments of each Dicom Beam.
	(dolist (doselist (di-dmp-pdoses d-dmp-obj))
	  (do ((seg-doses doselist (cdr seg-doses)))
	      ((null seg-doses))
	    (incf accum-dose (the single-float (car seg-doses)))))
	;; Now add Prior dose to Accum dose to get Total dose.
	(setf (di-dmp-total-cGy d-dmp-obj)
	      (+ (the fixnum (di-dmp-prior-cGy d-dmp-obj))
		 (setf (di-dmp-accum-cGy d-dmp-obj) (round accum-dose))))
	(setf (di-dmp-dose-type d-dmp-obj) :Computed))))

  ;; Verify that constraints are satisfied:
  ;;  1. Every DMP receives some non-zero dose.
  ;;  2. Every DMP is contributed to by at least one beam.
  ;;  3. Every beam contributes to at least one DMP.
  (let ((violated-constraints '()))
    (declare (type list violated-constraints))

    (dolist (d-dmp-obj d-dmp-list)
      ;; DI-DMP-ACCUM-CGY must be a valid [computed or typed] dose here.
      (unless (> (the fixnum (di-dmp-accum-cGy d-dmp-obj)) 0)
	(push (format nil "DMP ~S receives no dose." (di-dmp-name d-dmp-obj))
	      violated-constraints))
      (unless (consp (di-dmp-dbeams d-dmp-obj))
	(push (format nil "DMP ~S is in no beams." (di-dmp-name d-dmp-obj))
	      violated-constraints)))

    (dolist (p-bmdata p-bm-info)

      ;; Check that each radiating segment has at least 1.0 MU per fraction.
      ;; This is an Elekta-specific constraint.
      ;; First expression is Total MU for given segment [Prism beam].
      ;; Second expression is number of fractions for this segment.
      (let ((MU/frac (/ (the single-float (pr-beam-tot-mu (fifth p-bmdata)))
			(coerce (n-treatments (third p-bmdata))
				'single-float))))
	(declare (type single-float MU/frac))
	(when (< MU/frac 1.0)
	  (push (format nil "Segment ~S has insufficient MU: ~F."
			(name (first p-bmdata))
			MU/frac)
		violated-constraints)))

      ;; ORIG-PBIs in list checked against ORIG-PBIs in DI-BEAM-OPBI-LIST slot.
      ;; Each ORIG-PBI here is an Original-Prism-Beam instance, the first
      ;; segment in a Dicom beam [ie, its SEGTYPE is :STATIC or :DYNAMIC].
      (unless (eq (pr-beam-segtype (fifth p-bmdata)) :segment)
	(let ((orig-pbi (first p-bmdata)))
	  (block beam-hits-DMP
	    (dolist (d-dmp-obj d-dmp-list)
	      (dolist (d-bm-obj (di-dmp-dbeams d-dmp-obj))
		(when (member orig-pbi (di-beam-opbi-list d-bm-obj) :test #'eq)
		  (return-from beam-hits-DMP))))
	    (push (format nil "Beam ~S hits no DMP(s)." (name orig-pbi))
		  violated-constraints)))))

    (or (null violated-constraints)
	(sl:confirm `("Violated constraints:"
		      ""
		      ,@(nreverse violated-constraints)
		      ""
		      "Continue?"
		      ""
		      "PROCEED -> Yes, do transmission"
		      "CANCEL -> No, return to panel")))))

;;;-------------------------------------------------------------

(defun assemble-dicom (cur-pat p-bm-info dicom-pat-id d-dmp-list)

  "assemble-dicom cur-pat p-bm-info dicom-pat-id d-dmp-list

Returns DICOM-ALIST, a Dicom-RT Plan for patient CUR-PAT, which can be
processed by SEND-DICOM and LOG-DICOM-DATA."

  ;; The RT Plan is an association list.  The car of each
  ;; list element is the Dicom tag, the cdr is the data itself.  When the
  ;; tag indicates a sequence (DICOM VR SQ), the cdr is the entire
  ;; sequence, another association list of Dicom tags and data, which can
  ;; contain more sequences etc.

  ;; The Dicom tags *MUST* appear in exactly the order they are coded here
  ;; (the Dicom standard requires tags within each nesting level to
  ;; appear in ascending numeric order)

  ;; P-BM-INFO is a list, in forward order, each entry being:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <Prism-Beam-Object> )
  ;; with one entry for each segment - constructed by GENERATE-PBEAM-INFO.
  ;;
  ;; This list contains all Prism beams - that is, all segments for all Dicom
  ;; beams, grouped into Dicom beams in order - all segments for one Dicom
  ;; beam followed by all segs for the next, and so forth.

  (declare (type list p-bm-info d-dmp-list))

  ;; Attributes values - some type coercions too (fixnum -> string)
  (let* ((prism-pat-id (format nil "~D" (patient-id cur-pat)))
	 (case-id-string (format nil "~D" (case-id cur-pat)))
	 (pln (fourth (first p-bm-info)))           ; just first beam's plan
	 (plan-name (name pln))
	 (small-plan-name "") (big-plan-name "")
	 (plan-timestamp (time-stamp pln)))

    (declare (type simple-base-string prism-pat-id case-id-string
		   plan-name small-plan-name big-plan-name plan-timestamp))

    (cond ((<= (length plan-name) 16)
	   (setq small-plan-name plan-name))
	  (t (setq small-plan-name (subseq plan-name 0 16)
		   big-plan-name (subseq plan-name 16))
	     (when (> (length big-plan-name) 64)
	       (setq big-plan-name (subseq big-plan-name 0 64)))))

    (multiple-value-bind (plan-date plan-time)
	(dicom-date-time plan-timestamp)

      ;; Association list of attribute tag-value pairs.
      ;; Dicom std says items must appear in order of ascending tag value
      ;; (within each nesting level).
      ;; This is NOT the order items appear in Dicom std or Elekta statement.

      `(((#x0008 . #x0016) "1.2.840.10008.5.1.4.1.1.481.5")
	((#x0008 . #x0018) "9.9.9.9")

	;; Study module, items with low tag values
	((#x0008 . #x0020))
	((#x0008 . #x0030))
	((#x0008 . #x0050))

	;; Series module, item with low tag values
	((#x0008 . #x0060) "RTPLAN")

	;; General Equipment module, not used by Elekta but required
	((#x0008 . #x0070))

	;; Study module again
	((#x0008 . #x0090))

	;; RT general plan module
	;; Dicom operator is Prism user who transfered plan
	((#x0008 . #x1070)
	 ,(progn (getenv "USER")))

	;; Patient module
	((#x0010 . #x0010) ,(name cur-pat))         ;64 chars max.
	((#x0010 . #x0020) ,dicom-pat-id)
	((#x0010 . #x0030))
	((#x0010 . #x0040))
	((#x0010 . #x1000)                          ;64 chars max.
	 ,(format nil "~A ~A ~A"
		  prism-pat-id case-id-string
		  (let ((h-id (hospital-id cur-pat)))
		    (declare (type simple-base-string h-id))
		    (if (> (length h-id) 0) h-id "99-99-99-99"))))

	;; Study module, not used by Elekta but required
	((#x0020 . #x000D) "9.9.9.9")

	;; Series module
	((#x0020 . #x000E) "9.9.9.9")

	;; Study module
	((#x0020 . #x0010))

	;; Series module, not used by Elekta but required
	((#x0020 . #x0011))

	;; RT General Plan module
	((#x300A . #x0002) ,small-plan-name)        ;0 -> 16 chars, Required

	,@(and (> (length big-plan-name) 0)
	       `(((#x300A . #x0003) ,big-plan-name))) ;0 -> 64 chars, Optional

	((#x300A . #x0004)                  ;Plan description, 1024 chars max.
	 ;; Possible need to truncate to length 1024 since COMMENTS
	 ;; fields here can be of arbitrary length.
	 ,(let ((descrip
		  (format
		    nil
		    "~A~%~A~%DS: ~A  Prism patient: ~A case: ~A~{~%~A~}~{~%~A~}"
		    plan-timestamp
		    plan-name (plan-by pln)
		    prism-pat-id case-id-string
		    (comments pln) (comments cur-pat))))
	    (declare (type simple-base-string descrip))
	    (cond ((<= (length descrip) 1024) descrip)
		  (t (subseq descrip 0 1024)))))

	((#x300A . #x0006) ,plan-date)
	((#x300A . #x0007) ,plan-time)
	((#x300A . #x000C) "PATIENT")

	;; RT Prescription module - optional [present if DMPs available]
	;; Transmit all DMPs, no matter in which Dicom beam they appear.
	,@(and
	    (consp d-dmp-list)
	    `(((#x300A . #x0010)
	       ,@(mapcar
		     #'(lambda (d-dmp-obj &aux (pt (di-dmp-point d-dmp-obj)))
			 `(((#x300A . #x0012) ,(di-dmp-counter d-dmp-obj))
			   ;;
			   ((#x300A . #x0014) "COORDINATES")
			   ;;
			   ;; 64-character-maximum-length string here.
			   ((#x300A . #x0016) ,(di-dmp-name d-dmp-obj))
			   ;;
			   ((#x300A . #x0018)
			    ,(* 10.0 (the single-float (x pt)))
			    ,(* 10.0 (the single-float (y pt)))
			    ,(* 10.0 (the single-float (z pt))))
			   ;;
			   ((#x300A . #x001A)       ;FLOAT, in Gy.
			    ,(* 0.01                ;cGY -> Gy
				(coerce
				  (the fixnum
				    (di-dmp-prior-cGy d-dmp-obj))
				  'single-float)))
			   ;;
			   ((#x300A . #x0020) "TARGET")
			   ;;
			   ;; DI-DMP-TOTAL-CGY must be a valid
			   ;; [computed or typed] dose here.
			   ((#x300A . #x0027)       ;FLOAT, in Gy.
			    ,(* 0.01                ;cGY -> Gy
				(coerce
				  (the fixnum
				    (di-dmp-total-cGy d-dmp-obj))
				  'single-float)))))
		   ;;
		   d-dmp-list))))

	;; RT Fraction Scheme module - prescribed MU, N of fractions here
	((#x300A . #x0070)
	 ,@(assemble-fraction-groups p-bm-info))

	;; RT Beams module
	((#x300A . #x00B0)
	 ,@(assemble-beam-sequence p-bm-info d-dmp-list))))))

;;;-------------------------------------------------------------

(defun assemble-fraction-groups (p-bm-info &aux p-bm-seq (seq-num 0))

  "assemble-fraction-groups p-bm-info

Assemble Dicom-RT Fraction Group Sequence portion of DICOM-ALIST."

  (declare (type list p-bm-info)
	   (type fixnum seq-num))

  ;; P-BM-INFO is a list, in forward order, each entry being:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <Prism-Beam-Object> )
  ;; with one entry for each segment.

  ;; Within each fraction group, all beams have the same number of fractions.
  ;; In each fraction group, there is a sequence of beam number/MU pairs.

  (setq

    ;; P-BM-SEQ [Prism-beam sequence] is a list of
    ;;  ( <Seq-Number> <Num-Fractions> <Total-MU-per-frac> )
    ;; for each Prism beam.
    p-bm-seq (mapcar
		 #'(lambda (p-bmdata)               ;Each item in P-BM-INFO
		     (let ((nfrac (n-treatments (third p-bmdata)))
			   (p-bm-obj (fifth p-bmdata)))
		       (declare (type fixnum nfrac))
		       (list (setq seq-num (the fixnum (1+ seq-num)))
			     nfrac
			     (/ (the single-float (pr-beam-tot-mu p-bm-obj))
				(coerce nfrac 'single-float)))))
	       ;; Input is list of items as in P-BM-INFO except only for
	       ;; :STATIC and :DYNAMIC beams - not for subsequent :SEGMENTs.
	       (remove-if #'(lambda (p-bmdata)
			      (eq (pr-beam-segtype (fifth p-bmdata)) :segment))
			  p-bm-info)))

  ;; Dicom Fraction Group Sequence with tags.
  (let ((frac-seq '())
	(idx 0))

    (declare (type list frac-seq)
	     (type fixnum idx))

    ;; FRAC-SEQ [fraction sequence] is list of distinct N in P-BM-SEQ.
    ;; This loop uniquizes the list of NUM-FRACTIONS for each item in P-BM-SEQ,
    ;; while preserving the order of the items in the list being uniquized.
    ;; cl:REMOVE-DUPLICATES does NOT guarantee to preserve order.
    (dolist (frac-num (mapcar #'second p-bm-seq))
      (unless (member frac-num frac-seq :test #'=)
	(push frac-num frac-seq)))
    (setq frac-seq (nreverse frac-seq))

    (mapcar
	#'(lambda (frac-num frac-group)
	    `(((#x300A . #x0071)                    ;Fraction Group Number
	       ,(setq idx (the fixnum (1+ idx))))
	      ((#x300A . #x0078) ,frac-num)       ;Number of Fractions Planned
	      ((#x300A . #x0080) ,(length frac-group))  ;Number of Dicom beams
	      ((#x300A . #x00A0) 0)       ;Number of Brachy Application Setups
	      ((#x300C . #x0004)               ;Referenced Dicom beam Sequence
	       ,@(mapcar
		     #'(lambda (frac-item)
			 ;; FRAC-ITEM:
			 ;;  ( <Seq-Num> <Num-Fractions> <Total-MU-per-frac> )
			 ;; Coord sign conventions ignored here.
			 ;; Dicom requires slot, but Elekta ignores it.
			 ;; Fictitious DicomDMP used as normalization point -
			 ;; its arbitrary coordinates and dose [of 1.0 Gray]
			 ;; are; used as norm-point values to represent all
			 ;; DicomDMPs.  Assumption is that every Dicom beam
			 ;; will have at least one DicomDMP in it.  If not,
			 ;; the first two slots here optionally can be missing.
			 `(
			   ;; Beam Dose Specification Point - optional.
			   ;; Present if DicomDMPs are available.
			   ((#x300A . #x0082) 0.0 0.0 0.0)
			   ;; Beam Dose - optional, present if DMPs available.
			   ((#x300A . #x0084) 1.0)  ;Gray.
			   ;; Beam Meterset, absolute MU per fraction.
			   ((#x300A . #x0086) ,(third frac-item))   ;Total-MU
			   ;; Beam number [cross-reference index].
			   ((#x300C . #x0006) ,(first frac-item))))
		   frac-group))))
      frac-seq
      ;; Second arg is elements of P-BM-SEQ grouped by N from FRAC-SEQ.
      (mapcar #'(lambda (frac)
		  (declare (type fixnum frac))
		  (remove-if-not
		    #'(lambda (data)
			(= frac (the fixnum (second data))))
		    p-bm-seq))
	frac-seq))))

;;;-------------------------------------------------------------

(defun assemble-beam-sequence (p-bm-info d-dmp-list)

  "assemble-beam-sequence p-bm-info d-dmp-list

Assemble Dicom-RT Beam Sequence portion of DICOM-ALIST."

  ;; P-BM-INFO is a list, in forward order, each entry being:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <Prism-Beam-Object> )
  ;; with one entry for each segment.

  (declare (type list p-bm-info d-dmp-list))

  (do ((p-bm-attrs nil)                        ; list of Prism-beam attributes
       (dbeam-sequence nil)                         ; Dicom beam sequence
       (nfrac 0)                               ; num fractions in current beam
       (cps nil)                                    ; control point sequence
       (mach) (wedge-id 0) (wedge-name "") (curr-coll)
       (p-bms p-bm-info)            ;P-BMS gets CDRed at end of this function.
       (first-seg? nil nil)         ;Tracks segment starting a new Dicom beam.
       #+:Ignore
       (energy-change? nil nil)        ;Energy changes during this Dicom beam.
       (cp-index 0)
       (p-bmdata) (orig-pbi) (p-bm-obj))
      ((null p-bms)
       (nreverse dbeam-sequence))
    (declare (type list p-bm-attrs dbeam-sequence cps p-bms p-bmdata)
	     (type simple-base-string wedge-name)
	     (type (member nil t) first-seg? #+:Ignore energy-change?)
	     (type fixnum nfrac wedge-id cp-index))
    (setq p-bmdata (car p-bms))                     ;Each item in P-BM-INFO
    (setq orig-pbi (first p-bmdata))             ;Original Prism beam instance
    (setq nfrac (n-treatments orig-pbi))
    (setq mach (machine orig-pbi))
    (setq wedge-id (id (wedge orig-pbi)))
    (setq wedge-name (wedge-label wedge-id mach))
    ;; NB: Current-Prism-Beam [rather than Original] instance used for
    ;; collimator since collimator attributes can be altered in Dicom Panel.
    ;; Collimator attributes are ONLY ones that can be so edited.  All other
    ;; beam attributes [including especially all dose-calc results] come from
    ;; the Original-Prism-beam instance.
    (setq curr-coll (collimator (third p-bmdata)))
    (setq p-bm-obj (fifth p-bmdata))            ;Prism-Beam structure instance
    (unless (eq (pr-beam-segtype p-bm-obj) :segment)
      (setq first-seg? t)
      (setq cp-index 0)
      (setq cps '())
      (setq
	p-bm-attrs
	(let ((mach-name (name mach))
	      (rad-type (case (particle mach)
			  ((photon) "PHOTON")
			  ((electron) "ELECTRON")
			  ((neutron) "NEUTRON")
			  ((otherwise "UNKNOWN"))))
	      (ext-wdg? (not (or (zerop wedge-id)
				 (string= wedge-name "Fixed Wedge"))))
	      (ext-blks? (coll:elements (blocks orig-pbi)))
	      (attrs nil))          ;accumulated list of Dicom beam attributes

	  (setq
	    attrs
	    `(
	      ;; (0008,1090) is Dicom Manufacturer Model Name
	      ;; but we use it here for Prism machine name so we can
	      ;; use (300A,003C) Beam Description for other stuff
	      ((#x0008 . #x1090) ,mach-name)        ;64 char max.
	      ((#x300A . #x00B2) ,(car (ident mach)))
	      ((#x300A . #x00B3) "MU")              ;Primary Dosimeter Unit

	      ;; Beam limiting device seq
	      ((#x300A . #x00B6)
	       (((#x300A . #x00B8) "ASYMX")
		((#x300A . #x00BC) 1))
	       (((#x300A . #x00B8) "ASYMY")
		((#x300A . #x00BC) 1))
	       (((#x300A . #x00B8) "MLCX")
		((#x300A . #x00BC) 40)))

	      ;; Dicom Beam number, name, description, type, radiation type
	      ((#x300A . #x00C0)
	       ,(pr-beam-dbeam-num p-bm-obj))

	      ((#x300A . #x00C2)                    ;Beam name, 64 chars max.
	       ,(let ((str (string-trim " " (name orig-pbi))))
		  (declare (type simple-base-string str))
		  (cond ((<= (length str) 64) str)
			(t (subseq str 0 64)))))

	      ((#x300A . #x00C3)
	       ;; Field length limited to 1024 chars here.
	       ,(format
		  nil
		  "Machine:  ~A~%Modality: ~A~%Energy:   ~A MV~%Wedge:    ~A"
		  mach-name
		  rad-type
		  (energy mach)
		  wedge-name))

	      ;; Our dynamic beams must be called "STATIC" not
	      ;; "DYNAMIC" in Dicom.  Step-and-shoot is "STATIC".
	      ;; See Andrew Long's email 14-Jun-2002
	      ((#x300A . #x00C4)
	       ,(case (pr-beam-segtype p-bm-obj)
		  (:static "STATIC")
		  (:dynamic "STATIC")               ; sic - see comment
		  (otherwise "UNKNOWN")))           ; RTD will reject
	      ((#x300A . #x00C6) ,rad-type)

	      ;; Wedge seq, Elekta internal wedge *only*.
	      ;; External wedges are represented as shadow trays.
	      ;; If we include wedge seq, must also include
	      ;; wedge position seq in each control point.
	      ;; Can say wedge is out at each control point.
	      ((#x300A . #x00D0) 1)
	      ((#x300A . #x00D1)
	       (((#x300A . #x00D2) 1)
		((#x300A . #x00D3) "MOTORIZED")
		((#x300A . #x00D5))          ; D5,6,8 are required but ignored
		((#x300A . #x00D6))         ; must be present but can be empty
		((#x300A . #x00D8))))

	      ;; Compensators, boli: there are none
	      ((#x300A . #x00E0) 0)
	      ((#x300A . #x00ED) 0)

	      ;; Represent external wedges or blocks as shadow tray,
	      ;; so number of blocks is 1
	      ((#x300A . #x00F0) ,(if (or ext-wdg? ext-blks?) 1 0))))

	  ;; Purpose of this block sequence is just to identify the tray.
	  (when (or ext-wdg? ext-blks?)
	    (setq attrs
		  (nconc
		    attrs
		    `(((#x300A . #x00F4)            ; Block Sequence
		       (((#x300A . #x00E1))         ; 2C, required but ignored
			((#x300A . #x00F5)          ; Shadow Tray
			 ,(cond (ext-wdg?
				  (accessory-code
				    (find wedge-id (wedges mach)
					  :key #'ID)))
				(t (tray-accessory-code mach))))
			((#x300A . #x00F6))         ; 2C, required but ignored
			((#x300A . #x00F8)
			 ,(if ext-wdg? "EXTERNAL WEDGE" "EXTERNAL BLOCKS"))
			((#x300A . #x00FA))         ; 2C, required but ignored
			((#x300A . #x00FC) 1)      ; So RTD will say "Block 1"
			((#x300A . #x00FE)
			 ,(if ext-wdg? wedge-name ""))
			((#x300A . #x0100))         ; 2C, required but ignored
			((#x300A . #x0102))         ; 2C, required but ignored
			((#x300A . #x0104))         ; 2C, required but ignored
			((#x300A . #x0106))         ; 2C, required but ignored
			))))))

	  ;; Total MU per fraction - absolute MU, not percent, for now.
	  ;; Transfered as FLOAT rather than INTEGER as formerly.
	  (nconc attrs `(((#x300A . #x010E)
			  ,(/ (the single-float (pr-beam-tot-mu p-bm-obj))
			      (coerce nfrac 'single-float))))))))

    ;; Even-numbered control points indicate start of beam segment.
    ;; Control point for first segment contains everything.
    ;; Control points for successive segments contain only those components
    ;; that change over course of segments.
    (push
      `(((#x300A . #x0112) ,cp-index)
	((#x300A . #x0114) ,(energy mach))
	,(wedge-data wedge-name)
	,(leaf/diaphragm-data curr-coll)
	,@(and first-seg? (gantry/coll/couch-data orig-pbi))
	,(cum-mu-data p-bm-obj nfrac t)
	,@(and (consp d-dmp-list)
	       ;; Referenced Dose Reference Sequence - present if DMPs are.
	       (cum-dose-data d-dmp-list orig-pbi nfrac t)))
      cps)

    (setq cp-index (the fixnum (1+ cp-index)))

    ;; Odd-numbered control points indicate end of beam segment.
    ;; Contains all components that change over course of segments.
    (push
      `(((#x300A . #x0112) ,cp-index)
	((#x300A . #x0114) ,(energy mach))
	,(wedge-data wedge-name)
	,(leaf/diaphragm-data curr-coll)
	,(cum-mu-data p-bm-obj nfrac nil)
	,@(and (consp d-dmp-list)
	       ;; Referenced Dose Reference Sequence - present if DMPs are.
	       (cum-dose-data d-dmp-list orig-pbi nfrac nil)))
      cps)

    (setq cp-index (the fixnum (1+ cp-index)))
    (when (or (null (setq p-bms (cdr p-bms)))  ;Now doing last P-BMDATA object
	      ;; Or next P-BMDATA object is NOT a successor segment.
	      (not (eq (pr-beam-segtype (fifth (car p-bms))) :segment)))
      ;; Which implies this P-BMDATA obj is the LAST segment of a Dicom beam.
      ;; 300A:0110 and 0111 are N control points and control point seq,
      ;;  the last two elements in the P-BM-ATTRS list, just NCONC to end.
      (push (nconc p-bm-attrs
		   `(((#x300A . #x0110) ,(length cps))
		     ((#x300A . #x0111) . ,(nreverse cps))))
	    dbeam-sequence))))

;;;-------------------------------------------------------------

(defun cum-mu-data (p-bm-obj nfrac even?)

  (declare (type (member nil t) even?)
	   (type fixnum nfrac))

  `((#x300A . #x0134)
    ,(/ (the single-float
	  (cond (even?
		  (pr-beam-cum-mu-exc p-bm-obj))
		(t (pr-beam-cum-mu-inc p-bm-obj))))
	(coerce nfrac 'single-float))))

;;;-------------------------------------------------------------
;;; D-DMP-LIST is guaranteed non-empty here.

(defun cum-dose-data (d-dmp-list orig-pbi nfrac even?
		      &aux (dmp-doses '()) (dmp-counters '()))

  (declare (type list d-dmp-list dmp-doses dmp-counters)
	   (type (member nil t) even?)
	   (type fixnum nfrac))

  ;; Iterate over all DMPs contributed to by current beam.
  ;; D-DMP-LIST contains ALL DMPs.  Must filter so as so accumulate dose
  ;; only from those contributed to by the beam in question.
  ;; These accumulated beam/segment doses do NOT include any Prior dose,
  ;; which is why Prior-cGy is subtracted from Total-cGy here.
  ;; Doses here are from DI-DMP-PDOSES, which are cGy as SMALL-FLOAT values.
  (dolist (d-dmp-obj d-dmp-list)
    (let* ((d-bmlist (di-dmp-dbeams d-dmp-obj))
	   (user-dose? (eq (di-dmp-dose-type d-dmp-obj) :User))
	   (per-beam-dose (/ (coerce
			       (- (the fixnum (di-dmp-total-cgy d-dmp-obj))
				  (the fixnum (di-dmp-prior-cgy d-dmp-obj)))
			       'single-float)
			     (coerce (the fixnum (length d-bmlist))
				     'single-float))))
      ;; D-BMLIST checked above to be non-empty.
      (declare (type list d-bmlist)
	       (type single-float per-beam-dose)
	       (type (member nil t) user-dose?))
      (do ((d-bms d-bmlist (cdr d-bms))
	   (doselist (di-dmp-pdoses d-dmp-obj) (cdr doselist))
	   (dbeam-seglist))
	  ((null d-bms))
	(declare (type list d-bms doselist dbeam-seglist))
	;; All ORIG-PBIs must be [uncopied] Original-Prism-Beam instances.
	;; Only generate data points if current ORIG-PBI segment is in the
	;; list for the DicomBmInst contributing to the current DMP point.
	(when (member orig-pbi
		      (setq dbeam-seglist (di-beam-opbi-list (car d-bms)))
		      :test #'eq)
	  ;; Cumulative dose [actual, not per-MU] in Gray PER FRACTION at DMP
	  ;; due to all segments up to but EXCLUDING current ORIG-PBI [segment
	  ;; at current control-point pair] for EVEN control point and up to
	  ;; and INCLUDING current ORIG-PBI for ODD control point.
	  (do ((orig-pbi-list dbeam-seglist (cdr orig-pbi-list))
	       (seg-doses (car doselist) (cdr seg-doses))
	       (per-seg-dose (/ per-beam-dose (length dbeam-seglist)))
	       (this-seg-dose 0.0) (accum-dose 0.0))
	      ((null seg-doses)
	       (error "CUM-DOSE-DATA [1] Ran off end."))
	    (declare (type list orig-pbi-list seg-doses)
		     (type single-float per-seg-dose this-seg-dose accum-dose))
	    ;; ORIG-PBI-LIST: list of uncopied original Prism beam instances.
	    ;; SEG-DOSES: list of Prism segment doses [total, not per-frac].
	    (setq this-seg-dose (cond (user-dose? per-seg-dose)
				      (t (the single-float (car seg-doses)))))
	    (cond ((eq orig-pbi (car orig-pbi-list))
		   (unless even?
		     (incf accum-dose this-seg-dose))
		   (push (di-dmp-counter d-dmp-obj) dmp-counters)
		   (push (/ (* 0.01 accum-dose)     ;cGy -> Gray
			    (coerce nfrac 'single-float))   ;Per FRACTION
			 dmp-doses)
		   (return))
		  (t (incf accum-dose this-seg-dose))))
	  ;; ORIG-PBI can be in only one Dicom beam.  Therefore, once we have
	  ;; found its beam and accumulated doses across the beam's segment
	  ;; list, we are done with this DMP.  There may be more DMPs
	  ;; contributed to by this ORIG-PBI [that is, other DMPs contributed
	  ;; to by this dicom beam], so we must continue scan.
	  (return)))))

  ;; We use a fictitious DMP as the normalization point and an arbitrary
  ;; dose of 1.0 Gray as the norm-point value to represent all DMPs.
  `(((#x300C . #x0050)
     ,@(mapcar
	   #'(lambda (dmp-dose dmp-counter)
	       `(((#x300A . #x010C)
		  ;; Cumulative Dose Reference Coefficient
		  ;; Number computed here [a single-float ratio] times
		  ;; norm-point dose in Gy gives accumulated dose [Gy]
		  ;; for current control point pair at current DMP due to
		  ;; all Prism beams in this Dicom beam.
		  ,dmp-dose)
		 ((#x300C . #x0051)
		  ;; Referenced Dose Reference Number [cross-ref index].
		  ,dmp-counter)))
	 (nreverse dmp-doses)
	 (nreverse dmp-counters)))))

;;;-------------------------------------------------------------

(defun wedge-data (wedge-name)

  (declare (type simple-base-string wedge-name))

  `((#x300A . #x0116)
    (((#x300A . #x0118)
      ,(if (string= wedge-name "Fixed Wedge") "IN" "OUT"))
     ((#x300C . #x00C0) 1))))

;;;-------------------------------------------------------------

(defun leaf/diaphragm-data (curr-coll)

  ;; CURR-COLL is [copied and possibly mutated] from <CurrBmInst> since that
  ;; collimator contains fitted-to-portal and adjusted-to-flagpole leaves and
  ;; backup diaphragms [possibly modified by user].

  `((#x300A . #x011A)
    ;; diaphragms
    (((#x300A . #x00B8) "ASYMX")
     ((#x300A . #x011C)
      ,(* 10.0 (the single-float (x1 curr-coll)))
      ,(* 10.0 (the single-float (x2 curr-coll)))))
    (((#x300A . #x00B8) "ASYMY")
     ((#x300A . #x011C)
      ,(* 10.0 (the single-float (y1 curr-coll)))
      ,(* 10.0 (the single-float (y2 curr-coll)))))
    ;; leaves
    (((#x300A . #x00B8) "MLCX")
     ((#x300A . #x011C)
      ,@(delistify-leaves (leaf-settings curr-coll))))))

;;;-------------------------------------------------------------

(defun delistify-leaves (leaf-pos &aux (x1-bank '()) (x2-bank '()))

  "delistify-leaves leaf-pos

converts Prism-format leaf positions (list of N pairs, coords in CM)
to Dicom-format (list of 2N scalars, coords in MM), also rearranges order"

  ;; Rearrange order from pairs of opposed leaves,
  ;; to all the X1 (-x) leaves, then all the X2 (+x) leaves.
  ;; Furthermore must rearrange order of leaves in each bank, because
  ;; edge-list passed to COMPUTE-MLC is arranged from +y end to -y end,
  ;; but Dicom wants leaves sorted from -y to +y

  (dolist (pair leaf-pos)
    (push (* 10.0 (the single-float (first pair))) x1-bank)
    (push (* 10.0 (the single-float (second pair))) x2-bank))

  (nconc x1-bank x2-bank))      ; no nreverse needed, we want to reverse order

;;;-------------------------------------------------------------

(defun gantry/coll/couch-data (orig-pbi)

  `(((#x300A . #x011E) ,(gantry-angle orig-pbi))
    ((#x300A . #x011F) "NONE")
    ((#x300A . #x0120) ,(collimator-angle orig-pbi))
    ((#x300A . #x0121) "NONE")
    ((#x300A . #x0122) ,(couch-angle orig-pbi))
    ((#x300A . #x0123) "NONE")
    ((#x300A . #x0125) 0.0)                         ;table top eccentric angle
    ((#x300A . #x0126) "NONE")
    ((#x300A . #x0128))         ;Table linear motions all type 2C, leave empty
    ((#x300A . #x0129))
    ((#x300A . #x012A))))

;;;=============================================================
;;; Invoke Dicom SCU.

(defun send-dicom (dicom-alist d-dmp-list)

  "send-dicom dicom-alist d-dmp-list

Send contents of DICOM-ALIST to a server using DICOM-RT.  This code
calls our client (SCU) to communicate with the server (SCP)."

  (declare (type list dicom-alist d-dmp-list))

  (log-dicom-data dicom-alist d-dmp-list)

  (let* ((beam-sequence
	   (cdr (assoc '(#x300A . #x00B0) dicom-alist :test #'equal)))
	 (mach-id
	   (second (assoc '(#x300A . #x00B2) (car beam-sequence)
			  :test #'equal)))
	 (mach-name
	   (second (assoc '(#x0008 . #x1090) (car beam-sequence)
			  :test #'equal)))
	 (mach-ident
	   (ident (get-therapy-machine mach-name
				       *therapy-machine-database*
				       *machine-index-directory*))))
    ;; Trace printout.
    (format t "~&~%Send-Dicom: ~S ~S ~S" mach-name mach-id mach-ident)

    ;; error checking - does ident field correctly identify Dicom server?
    (cond ((and (consp mach-ident)
		(= (length mach-ident) 5))
	   ;; no error - ident field does identify a Dicom server
	   (let ((iden (first mach-ident))
		 (server-ae-title (second mach-ident))
		 (server-ip (third mach-ident))
		 (server-port (fourth mach-ident)))

	     ;; Trace printout.
	     (format t "~%ID: ~A, Server: ~A, IP: ~A, Port: ~A."
		     iden server-ae-title server-ip server-port)

	     (multiple-value-bind (status msg)

		 (cond ((sl:confirm
			  '("Send plan to accelerator?"
			    ""
			    "PROCEED -> Yes, do transmission"
			    "CANCEL -> No, testing dump only"))
			(dicom:run-client :C-Store-RTPlan-RQ
					  server-ip
					  server-port
					  server-ae-title
					  dicom-alist   ;RTPlan data as AList
					  "9.9.9.9"))
		       (t (values -1 "Testing - no transfer attempted")))

	       (log-dicom-transfer dicom-alist status msg)
	       (values status msg))))

	  (t (let ((status -1)
		   (msg (format nil "No Dicom server for ~A." mach-name)))
	       (log-dicom-transfer dicom-alist status msg)
	       (values status msg))))))

;;;-------------------------------------------------------------

(defun log-dicom-data (dicom-alist d-dmp-list)

  "log-dicom-data dicom-alist

Pretty-prints contents of DICOM-ALIST (with self-identifying tags) to
file specified by *PDR-DATA-FILE*."

  (with-open-file (strm *pdr-data-file* :direction :output
			:if-exists :supersede :if-does-not-exist :create)

    ;; Log information about each dose-monitoring point ...
    (format strm "~%Dose-Monitoring-Points:~%~%")
    (dolist (d-dmp-obj d-dmp-list)
      (format
	strm
	"~4,' D: ~S (~S), ~D cGy (prior), ~D cGy (accum), ~D cGy (total), ~A.~%"
	(di-dmp-counter d-dmp-obj)
	(di-dmp-name d-dmp-obj)
	(name (di-dmp-point d-dmp-obj))
	(di-dmp-prior-cGy d-dmp-obj)
	(di-dmp-accum-cGy d-dmp-obj)
	(di-dmp-total-cGy d-dmp-obj)
	(cond ((eq (di-dmp-dose-type d-dmp-obj) :Computed)
	       "computed by Prism")
	      (t "typed by User"))))

    ;; Log formatted dump of information sent by client to server ...
    (dicom::dump-dicom-data dicom-alist strm)))

;;;-------------------------------------------------------------

(defun log-dicom-transfer (dicom-alist status msg)

  "log-dicom-transfer dicom-alist status msg

Write Dicom transfer log file with transfer attempt status and message,
also date, time, Prism user, patient, Prism case, plan timestamp, field name"

  (let ((fname (concatenate 'string
			    *dicom-log-dir* "dicom" (lex-timestamp) ".log")))

    (with-open-file (fp fname :direction :output :if-exists :append
			:if-does-not-exist :create) ; should not exist
      (cond ((< status 0)
	     (format fp "~A CSTORE status (none) ~A~%"  (timestamp) msg))
	    (t (format fp "~A CSTORE status #x~4,'0X ~A~%"
		       (timestamp)
		       status
		       (or (cdr (assoc status *status-alist* :test #'=))
			   "Unknown error"))))

      ;; Get data for log entries from DICOM-ALIST using assoc and Dicom tags
      ;; unhack the hack where we store several ID's in one Dicom string
      (let* ((pat-case-ids
	       (second (assoc '(#x0010 . #x1000) dicom-alist :test #'equal)))
	     (case-id-string
	       (subseq pat-case-ids
		       (1+ (position #\Space pat-case-ids :test #'eq)))))
	(format fp "~5,D ~30,A ~11,A  case ~2,D, transfer by ~A~%"
		(read-from-string pat-case-ids)     ;Prism Patient ID
		(second (assoc '(#x0010 . #x0010)   ;Patient Name
			       dicom-alist :test #'equal))
		(read-from-string                   ;Prism Hospital ID
		  (subseq case-id-string
			  (1+ (position #\Space case-id-string :test #'eq))))
		(read-from-string case-id-string)   ;Case ID number
		(second (assoc '(#x0008 . #x1070)   ;Prism User
			       dicom-alist :test #'equal)))
	(format fp "Transferred as ~A~%"
		(second (assoc '(#x0010 . #x0020)   ;Dicom Patient ID
			       dicom-alist :test #'equal)))
	(dolist (p-bm-seq (cdr (assoc '(#x300A . #x00B0)    ;Beam Sequence
				      dicom-alist :test #'equal)))
	  (format fp "~2,D ~16,A ~6,A ~16,A ~20,A ~A~%"
		  (second (assoc '(#x300A . #x00C0) ;Beam Number
				 p-bm-seq :test #'equal))
		  (second (assoc '(#x300A . #x00C2) ;Beam Name
				 p-bm-seq :test #'equal))
		  (second (assoc '(#x300A . #x00B2) ;Machine ID
				 p-bm-seq :test #'equal))
		  (second (assoc '(#x300A . #x0002) ;Plan Name
				 dicom-alist :test #'equal))
		  (second (assoc '(#x300A . #x0006) ;Plan Date
				 dicom-alist :test #'equal))
		  (second (assoc '(#x300A . #x0007) ;Plan Time
				 dicom-alist :test #'equal))))))))

;;;=============================================================

(defun timestamp ()

  "timestamp

Return date and time in DD-MMM-YYYY HH:MM:SS format"

  (multiple-value-bind (sec min hr day mo yr) (get-decoded-time)
    (let ((month (nth (1- mo) '("Jan" "Feb" "Mar" "Apr" "May" "Jun"
				"Jul" "Aug" "Sep" "Oct" "Nov" "Dec"))))
      (format nil "~2,'0D-~A-~A ~2,'0D:~2,'0D:~2,'0D"
	      day month yr hr min sec))))

;;;-------------------------------------------------------------

(defun lex-timestamp ()

  "lex-timestamp

Return date and time in YYYY-MMDD-HHMMSS format so lex. order = chron. order"

  (multiple-value-bind (sec min hr day mo yr) (get-decoded-time)
    (format nil "~A-~2,'0D~2,'0D-~2,'0D~2,'0D~2,'0D" yr mo day hr min sec)))

;;;-------------------------------------------------------------

(defun dicom-date-time (prism-timestamp)

  "dicom-date-time prism-timestamp

Converts prism-timestamp, a string like '5-Mar-1995 15:47:34' or
'24-Mar-1995 13:52:33', to Dicom format date and time.
Returns two values: Dicom DA format date, like '19950305' or '19950324',
and Dicom TM format time, like '154734'."


  (let* ((hindex (position #\- prism-timestamp :test #'eq)) ; Find first hyphen
	 (daynum (subseq prism-timestamp 0 hindex)) ; Day, 1 or 2 chars
	 (mts (subseq prism-timestamp (1+ hindex) (length prism-timestamp)))
	 (sindex (position #\Space mts :test #'eq)) ; Find first space
	 (cindex (position #\: mts :test #'eq))     ; Find first colon
	 (hrnum (subseq mts (1+ sindex) cindex))
	 (mss (subseq mts (1+ cindex) (length mts))))

    (values (concatenate                            ;Date
	      'string
	      (subseq mts 4 8)                      ;Year
	      (format nil "~2,'0D"                  ;Month
		      (1+ (position (subseq mts 0 3)
				    '("Jan" "Feb" "Mar" "Apr" "May" "Jun"
				      "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")
				    :test #'string-equal)))
	      (if (= (length daynum) 2)             ;Day - 2 chars
		  daynum
		  (concatenate 'string "0" daynum)))

	    (concatenate                            ;Time
	      'string
	      (if (= (length hrnum) 2)              ;Day - 2 chars
		  hrnum
		  (concatenate 'string "0" hrnum))
	      (subseq mss 0 2)                      ;Minutes
	      (subseq mss 3 5)))))                  ;Seconds

;;;=============================================================
;;; End.
