;;;
;;; brachy-graphics
;;;
;;; defines draw methods for line sources and seeds in views
;;;
;;;  3-Jun-1996 I. Kalet started with stub draw method.
;;; 24-Aug-1997 I. Kalet wrote basic methods for cross sectional
;;; views.
;;; 13-Oct-1997 I. Kalet add stub methods for beam's eye views
;;; 31-Mar-1998 I. Kalet combine into one method for all ortho views,
;;; for line-sources and seeds, and use view-x,y to distinguish.
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible.
;;; 23-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;; 26-Mar-2000 I. Kalet add support for drawing raw source data from
;;; films into AP and Lateral view displays.
;;; 30-Jul-2002 I. Kalet add methods for view-, view-y for oblique views
;;;

(in-package :prism)

;;;--------------------------------------

(defmethod view-x ((vw transverse-view) vec)

  "returns the item in list vec that corresponds to the x coordinate
in a transverse view."

  (first vec))

;;;--------------------------------------

(defmethod view-y ((vw transverse-view) vec)

  "returns the item in list vec that corresponds to the y coordinate
in a transverse view."

  (second vec))

;;;--------------------------------------

(defmethod view-x ((vw coronal-view) vec)

  "returns the item in list vec that corresponds to the x coordinate
in a coronal view."

  (first vec))

;;;--------------------------------------

(defmethod view-y ((vw coronal-view) vec)

  "returns the item in list vec that corresponds to the y coordinate
in a coronal view."

  (- (third vec)))

;;;--------------------------------------

(defmethod view-x ((vw sagittal-view) vec)

  "returns the item in list vec that corresponds to the x coordinate
in a sagittal view."

  (third vec))

;;;--------------------------------------

(defmethod view-y ((vw sagittal-view) vec)

  "returns the item in list vec that corresponds to the y coordinate
in a sagittal view."

  (second vec))

;;;--------------------------------------

(defmethod view-x ((vw oblique-view) vec)

  "returns the transformed view x coordinate in an oblique view."

  (let* ((x (first vec))
	 (z (third vec))
	 (azi-rad (* (azimuth vw) *pi-over-180*))
	 (sin1 (sin azi-rad))
	 (cos1 (cos azi-rad)))
    (- (* x cos1) (* z sin1))))

;;;--------------------------------------

(defmethod view-y ((vw oblique-view) vec)

  "returns the transformed view y coordinate in an oblique view."

  (let* ((x (first vec))
	 (y (second vec))
	 (z (third vec))
	 (azi-rad (* (azimuth vw) *pi-over-180*))
	 (alt-rad (* (altitude vw) *pi-over-180*))
	 (sin1 (sin azi-rad))
	 (cos1 (cos azi-rad))
	 (sin2 (sin alt-rad))
	 (cos2 (cos alt-rad)))
    (- (* y cos2) (* (+ (* x sin1) (* z cos1)) sin2))))

;;;--------------------------------------

(defmethod draw ((ls line-source) (vw view))

  "draw (ls line-source) (vw view)

generates graphic primitives for line sources in views.  The
differences among the views are in the view-x and view-y generic
functions."

  (unless (typep vw 'beams-eye-view)
    (if (eql (display-color ls) 'sl:invisible)
	(setf (foreground vw) (remove ls (foreground vw) :key #'object))
      (let ((prim (find ls (foreground vw) :key #'object))
	    (color (sl:color-gc (display-color ls)))
	    (scale (scale vw))
	    (x0 (x-origin vw))
	    (y0 (y-origin vw))
	    (x1 (view-x vw (end-1 ls)))
	    (y1 (view-y vw (end-1 ls)))
	    (x2 (view-x vw (end-2 ls)))
	    (y2 (view-y vw (end-2 ls))))
	(unless prim
	  (setq prim (make-segments-prim nil color :object ls))
	  (push prim (foreground vw)))
	(setf (color prim) color
	      (points prim) (pixel-segments (list (list x1 y1 x2 y2))
					    scale x0 y0))))))

;;;--------------------------------------

(defmethod draw ((sd seed) (vw view))

  "draw (sd seed) (vw view)

generates graphic primitives for seeds in views.  For each, draws a +
icon ten pixels long.  The differences among the views are in the
view-x and view-y generic functions."

  (unless (typep vw 'beams-eye-view)
    (if (eql (display-color sd) 'sl:invisible)
	(setf (foreground vw) (remove sd (foreground vw) :key #'object))
      (let ((prim (find sd (foreground vw) :key #'object))
	    (color (sl:color-gc (display-color sd)))
	    (scale (scale vw))
	    (x0 (x-origin vw))
	    (y0 (y-origin vw))
	    (pt (list (view-x vw (location sd))
		      (view-y vw (location sd)))))
	(unless prim
	  (setq prim (make-segments-prim nil color :object sd))
	  (push prim (foreground vw)))
	(setf (color prim) color
	      (points prim) (draw-plus-icon pt scale x0 y0 5))))))

;;;--------------------------------------

(defun draw-all-raw-sources (line-data seed-data ap-vw lat-vw)

  (let ((line-sources (coll:elements line-data))
	(seeds (coll:elements seed-data)))
    (when ap-vw
      (setf (foreground ap-vw) nil)
      (dolist (line line-sources) (draw-raw-source line ap-vw :ap))
      (dolist (seed seeds) (draw-raw-source seed ap-vw :ap)))
    (when lat-vw
      (setf (foreground lat-vw) nil)
      (dolist (line line-sources) (draw-raw-source line lat-vw :lat))
      (dolist (seed seeds) (draw-raw-source seed lat-vw :lat)))))

;;;--------------------------------------

(defun draw-raw-source (src vw which)

  ;; this function just recomputes or creates a graphic-prim for one
  ;; raw source, src.

  (let ((raw-coords (case which
		      (:ap (raw-ap-coords src))
		      (:lat (raw-lat-coords src))))
	(prim (find src (foreground vw) :key #'object))
	(color (sl:color-gc (display-color src)))
	(scale (scale vw))
	(x0 (x-origin vw))
	(y0 (y-origin vw)))
    (when raw-coords
      (unless prim
	(setq prim (make-segments-prim nil color :object src))
	(push prim (foreground vw)))
      (setf (color prim) color)
      (setf (points prim)
	(if (= (length raw-coords) 4)
	    (pixel-segments (list raw-coords) scale x0 y0)
	  (draw-plus-icon raw-coords scale x0 y0 5))))))

;;;--------------------------------------
;;; End.
