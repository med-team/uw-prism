;;;
;;; digitizer
;;;
;;; The lisp interface for the GP-8 sonic digitizer.
;;;
;;; 15-Mar-1994 J. Unger created.
;;; 01-May-1994 J. Unger add gp8-calibrate and gp8-digitize functions,
;;;   transliterated from UWPLAN source file GP8.PAS
;;; 10-Jun-1994 I. Kalet reorganize and add a lot.
;;; 27-Jun-1994 I. Kalet insure that *gp8-xorigin* is nil when the
;;; digitizer is initialized.
;;; 11-Jul-1994 J. Unger work on getting digitizer dialog boxes to display
;;; (not finished).
;;; 11-Aug-1994 J. Unger add os-wait call to gp8-close
;;;  8-Jan-1995 I. Kalet parametrize and change names to digit-
;;;  instead of gp8- in variables and functions.  Create digitizer
;;;  class and instance to keep global data.
;;; 12-Mar-1995 I. Kalet add global variables to prism-globals and use
;;; them here - this is easier to customize than initargs or other
;;; schemes.
;;; 13-Aug-1995 I. Kalet change stream to different name for
;;; compliance with ANSI standard.
;;; 24-Dec-1998 I. Kalet run-subprocess for stty does not need output
;;; redirect.  Since wait is now default, don't need os-wait on close.
;;; 13-Aug-2000 I. Kalet move digitizer-specific globals to here, as
;;; they are really digitizer internals.
;;;

(in-package :prism)

;;;--------------------------------------

(defvar *digitizer* nil "The instance of digitizer in use in the
current prism session")

;; these values are for the Science Accessories GP9

(defvar *digit-x-start* 0 "The position of the first digit of the raw
x coordinate data in the digitizer input string.")

(defvar *digit-x-size* 5 "The length of the substring containing the
raw x coordinate data.")

(defvar *digit-y-start* 5 "The position of the first digit of the raw
y coordinate data in the digitizer input string.")

(defvar *digit-y-size* 5 "The length of the substring containing the
raw y coordinate data.")

(defvar *digit-exp-x-origin* 0 "The expected raw value for the x
coordinate of the lower left calibration point.") ;; units are mm.

(defvar *digit-exp-y-origin* -2328 "The expected raw value for the y
coordinate of the lower left calibration point.")

(defvar *digit-exp-x-full* 2295 "The expected raw value for the x
coordinate of the upper right calibration point.")

(defvar *digit-exp-y-full* -38 "The expected raw value for the y
coordinate of the upper right calibration point.")

(defvar *digit-exp-x-size* 58.0 "The width in cm of the digitizing
area on the plastic overlay pattern.")

(defvar *digit-exp-y-size* 58.0 "The full height in cm of the
digitizing area on the plastic overlay pattern.")

(defvar *digit-calib-tol* 30 "The tolerance for calibration values.")

(defvar *digit-boxdepth* -2131 "The raw y coordinate of the top of the
menu boxes")

(defvar *digit-boxwidth* 394 "The raw horizontal width of a single
menu box.")

;;;--------------------------------------

(defclass digitizer ()

  ((device :type string
	   :initarg :device
	   :accessor device
	   :documentation "The device file name of the digitizer")

   (digit-stream :type stream
		 :initarg :digit-stream
		 :accessor digit-stream
		 :documentation "The stream that is opened to the
digitizer")

   (x-origin :accessor x-origin
	     :initform nil ;; to insure initial calibration
	     :documentation "The raw value for the x coordinate of the
lower left calibration point.")

   (y-origin :accessor y-origin
	     :documentation "The raw value for the y coordinate of the
lower left calibration point.")

   (x-scale :type single-float
	    :accessor x-scale
	    :documentation "The x scale factor in cm per count.")

   (y-scale :type single-float
	    :accessor y-scale
	    :documentation "The y scale factor in cm per count.")
   
   )
  
  (:documentation "The digitizer object is a frame for storing data
about the particular instance of digitizer in use at a Prism site.")

  )

;;;--------------------------------------

(defun digit-initialize (digit-dev)

  "DIGIT-INITIALIZE digit-dev

Initializes the input stream for the digitizer, using device filename
digit-dev."

  (setf *digitizer* (make-instance 'digitizer
		      :device digit-dev
		      :digit-stream (open digit-dev)))
  (run-subprocess (format nil "stty 9600 cooked < ~a" digit-dev)))

;;;--------------------------------------

(defun digitizer-present ()

  (if *digitizer* t nil))

;;;--------------------------------------

(defun digit-close ()

  "DIGIT-CLOSE

Closes the stream to the digitizer which was set up in digit-initialize."

  (close (digit-stream *digitizer*))
  (setf *digitizer* nil))

;;;--------------------------------------

(defun digit-raw-point ()

  "DIGIT-RAW-POINT 

Takes no parameters.  Reads a point from the sonic digitizer stream
Returns two values, the x and y coordinates of that point in digitizer
coordinates. This function will not return until the digitizer pen is
sparked."

  (let* ((xs *digit-x-start*)
	 (xe (+ xs *digit-x-size*))
	 (ys *digit-y-start*)
	 (ye (+ ys *digit-y-size*))
	 (coords (read-line (digit-stream *digitizer*))))
    (values
     (read-from-string (subseq coords xs xe))
     (read-from-string (subseq coords ys ye)))))

;;;--------------------------------------

(defun digit-calibrate (&optional force-recalibration)

  "DIGIT-CALIBRATE &optional force-recalibration

If the global calibration values are not yet set or if
force-recalibration is t, prompts the user to spark the lower left and
upper right corners of the digitizer, via a SLIK readout.  Sets the
four global quantities: *digit-xorigin*, *digit-yorigin* (the integer
value returned by the digitizer when it is sparked at the marked
origin point in the lower left corner of the tabled), and
*digit-xscale*, *digit-yscale* (real calibration constants, expressed in
digitizer units/cm.)"

  (when (or force-recalibration (not (x-origin *digitizer*)))
    (let ((xorg *digit-exp-x-origin*)
	  (yorg *digit-exp-y-origin*)
	  (xfull *digit-exp-x-full*)
	  (yfull *digit-exp-y-full*)
	  (tol *digit-calib-tol*)
	  (xcal *digit-exp-x-size*)
	  (ycal *digit-exp-x-size*)
	  xraw yraw
	  (rdt (sl:make-readout 400 40 :title "Digitizer calibration")))
      (loop ;; promp until a reasonable origin point is digitized
        (setf (sl:info rdt) "Enter the lower left calibration point")
	(multiple-value-setq (xraw yraw) (digit-raw-point))
	(when (and (<= (- xorg tol) xraw (+ xorg tol))     
		   (<= (- yorg tol) yraw (+ yorg tol)))
	  (setf (x-origin *digitizer*) xraw)
	  (setf (y-origin *digitizer*) yraw)
	  (return)))
      (loop ;; prompt until a reasonable full span point is digitized
        (setf (sl:info rdt) "Enter the upper right calibration point")
	(multiple-value-setq (xraw yraw) (digit-raw-point))
	(when (and (<= (- xfull tol) xraw (+ xfull tol))
		   (<= (- yfull tol) yraw (+ yfull tol)))
	  (setf (x-scale *digitizer*)
	    (float (/ (- xraw (x-origin *digitizer*)) xcal)))
	  (setf (y-scale *digitizer*)
	    (float (/ (- yraw (y-origin *digitizer*)) ycal)))
	  (return)))
      (sl:destroy rdt))))

;;;--------------------------------------

(defun digit-reset ()

  "DIGIT-RESET

insures that digit-calibrate will compute new calibration values."

  (setf (x-origin *digitizer*) nil))

;;;--------------------------------------

(defun digitize-point ()

  "DIGITIZE-POINT

Obtains from the digitizer the (x,y) coordinates of a single point,
offset by the origin values entered in digit-calibrate and scaled by
the scale factors determined there.  A 3-element values form is
returned, consisting of the status, the x coordinate, and the y
coordinate.  The status is one of :point, :delete-last, :delete-all,
:close-contour, or :done, indicating where on the digitizer the pen
was sparked.  The returned x and y coordinates are in centimeters."

  (let (xraw yraw)
    (multiple-value-setq (xraw yraw) (digit-raw-point))
    (values
     (if (> yraw *digit-boxdepth*) :point
       (case (truncate (- xraw (x-origin *digitizer*))
		       *digit-boxwidth*)
	 (0 :delete-last)
	 (1 :delete-all)
	 (2 :close-contour)
	 (3 :done)))
     (float (/ (- xraw (x-origin *digitizer*))
	       (x-scale *digitizer*)))
     (float (/ (- yraw (y-origin *digitizer*))
	       (y-scale *digitizer*))))))

;;;--------------------------------------

(defun digitize-contour (verts update-fn mag x0 y0)

  "DIGITIZE-CONTOUR verts update-fn mag x0 y0

Edits the vertex list verts (a list of (x y) pairs) with points
acquired from the digitizer.  Update-fn is called after each time the
digitizer pen is sparked (it may update the display with the new
contour segment, for example).  Mag is the digitizer film
magnification factor, i.e., the amount the digitizer film is
magnified.  The origin parameters specify an application defined
origin relative to the lower left calibration point, and the
coordinates returned are relative to that application origin.  returns
the verts list when the 'Done' box is sparked on the digitizer."

  (do ((xcm nil)
       (ycm nil)
       (status nil))
      ((eq status :done) verts)
    (multiple-value-setq (status xcm ycm) (digitize-point))
    (case status
      (:point (push (list (/ (- xcm x0) mag)
			  (/ (- ycm y0) mag))
		    verts))
      (:delete-last (setf verts (rest verts)))
      (:delete-all (setf verts nil)))
    (funcall update-fn verts)))

;;;--------------------------------------
