;;;
;;; spots
;;;
;;; Functions which implement spot routines.
;;;
;;; ??-Jul-1990 B. Lockyear created file.
;;; ??-Nov-1991 C. Sweeney added average-dose slot,
;;;                        added physical-volume slot,
;;;                        changed surrounding-volume to surrounding-pstruct
;;;                          for clarity,
;;;                        changed underbars to hyphens,
;;;                        added pstruct-form slot and reader method,
;;;                        added placement-info slot and placement-info
;;;                          function, also placement-info structure,
;;;                        fixed low and high-dose-region? to return the
;;;                          right thing for organs and targets.
;;; 09-Feb-1994 D. Nguyen adapted file for autoplan package.
;;;

(in-package :prism)

;;;--------------------------------------------------
;;;
;;; FINAL SPOT OBJECT CLASS RETURNED BY SCANNER...
;;;

(defclass spot ()
  
  ((peak-dose :type float 
	      :initarg :peak-dose 
	      :accessor peak-dose)

   (limit :type float 
	  :initarg :limit 
	  :accessor limit)

   (average-dose :type float 
		 :initarg :average-dose 
		 :accessor average-dose)

   (dist-to-target :type float 
		   :accessor dist-to-target)

   (physical-volume :type float 
		    :accessor physical-volume)

   (voxel-count :type float 
		:initarg :voxel-count 
		:accessor voxel-count)

   (center :initarg :center 
	   :accessor center)

   (surrounding-pstruct :initarg :surrounding-pstruct 
			:accessor surrounding-pstruct)

   (all-segs :initarg :all-segs 
	     :accessor all-segs
	     :documentation "All segments of the spot.")

   (placement-info :initarg :placement-info 
		   :accessor placement-info
		   :documentation "Holds an assoc list of (beam
placement-info).")

   (pstruct-form :initarg :pstruct-form 
		 :accessor pstruct-form
		 :documentation "Holds the pstruct form of the spot,
contours and all.")))

;;;
;;; Methods
;;;

(defmethod center ((spot spot))

  (let ((c (slot-value spot 'center)))
    (values (first c) (second c) (third c))))

(defmethod pstruct-form ((spot spot))

  "Reader function which derives a pstruct from the segments of a spot."

  (let (color curr-z next-z min-x max-x 
	curr-y next-y left-pts right-pts conts)
    (if (slot-boundp spot 'pstruct-form)
	(slot-value spot 'pstruct-form)
      (setf (slot-value spot 'pstruct-form)
	(progn
	  ;; sort segs into increasing z and within that, increasing y values
	  (setf (all-segs spot)
	    (sort (all-segs spot) 
		  #'(lambda (seg1 seg2)
		      (cond ((equal (round (seg-z seg1))
				    (round (seg-z seg2)))
			     (when (< (seg-y seg1)
				      (seg-y seg2))))
			    ((< (round (seg-z seg1))
				(round (seg-z seg2))))
			    (t nil)))))
	  ;; hot spots are red, cold spots are blue
	  (setf color (if (high-dose-region? spot)
			  'sl:red
			'sl:blue))
	  ;; go thru all segments, and for each z plane, make list of
	  ;; points on the left and points on the right, which form
	  ;; left and right sides of the spot contour.  Approximate by
	  ;; assuming top and bottom is flat.
	  (dolist (seg (all-segs spot))
	    ;; round off z`s to make things easier
	    (setf next-z (float (round (seg-z seg))))
	    (setf next-y (seg-y seg))
	    (cond ((equal curr-z next-z)
		   (cond ((equal curr-y next-y)
;; *****************************************************************
			  (format t "test case curr-y = next-y~%")
			  (setf min-x (seg-min-x seg)
				max-x (seg-max-x seg)))
;; @@@			  (pr:lo-hi-compare min-x (seg-min-x seg) max-x)
;; @@@			  (pr:lo-hi-compare min-x (seg-max-x seg) max-x))
;; *****************************************************************

			 (t (setf left-pts (cons (list min-x curr-y) left-pts))
			    (setf right-pts (cons (list max-x curr-y) 
						  right-pts))
			    (setf curr-y next-y
				  min-x (seg-min-x seg)
				  max-x (seg-max-x seg)))))
		  (t 
		   (when curr-z 
		     (setf conts 
		       (cons (make-instance 'pr:contour
			       :z curr-z 
			       :vertices (if (or left-pts right-pts)
					     (append left-pts 
						     (reverse right-pts))
					   (list (list min-x curr-y)))
			       :display-color color)
			     conts)))
		   (setf curr-z next-z
			 curr-y (seg-y seg)
			 min-x (seg-min-x seg)
			 max-x (seg-max-x seg)
			 left-pts nil
			 right-pts nil))))
	  ;; make last contour
	  (when (and min-x max-x curr-y)
	    (setf left-pts (cons (list min-x curr-y) left-pts))
	    (setf right-pts (cons (list min-x curr-y) right-pts)))
	  (setf conts 
	    (cons (make-instance 'pr:contour
		    :z curr-z 
		    :vertices (append left-pts (reverse right-pts))
		    :display-color color)
		  conts))
	  (make-instance 'pr:pstruct 
	    :contours conts 
	    :display-color color ))))))

(defmethod high-dosep ((organ pr:organ) limit peak)
  (< limit peak))

(defmethod high-dosep ((target pr:target) limit peak)
  (declare (ignore limit peak))
  nil)    ; can't have high-dose-regions in targets

(defmethod low-dosep ((target pr:target) limit peak)
  (> limit peak))

(defmethod low-dosep ((organ pr:organ) limit peak)
  (declare (ignore limit peak))
  nil)    ; can't have low-dose-regions in organs

(defun high-dose-region? (spot)
  (high-dosep (surrounding-pstruct spot) (limit spot) (peak-dose spot)))

(defun low-dose-region? (spot)
  (low-dosep (surrounding-pstruct spot) (limit spot) (peak-dose spot)))

(defmethod physical-volume ((spot spot))
  (pr:physical-volume (pstruct-form spot)))

;;;#+ignore  
;;;(defun dose-voxel-volume (plan)
;;;  (let ((dimensions (array-dimensions (slot-value plan 'dose-array)))
;;;	(lengths (slot-value plan 'dose-array-size)))
;;;    (abs (apply '* (mapcar '/ lengths dimensions)))))
 
(defclass irradiated-region ()
  ((surrounding-pstruct :initarg :surrounding-pstruct 
			:accessor surrounding-pstruct)

   ;; holds the pstruct form of the region (contours and all)
   (pstruct-form :initarg :pstruct-form 
		 :accessor pstruct-form)
   ))

(defmethod physical-volume ((reg irradiated-region))
  (pr:physical-volume (pstruct-form reg)))

;;;-----------------------------------------------------
;;; End.
