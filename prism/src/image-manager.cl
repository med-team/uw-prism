;;;
;;; image-manager
;;;
;;; Mediators and functions to keep images displayed in views
;;; consistent with the view position etc.
;;;
;;; 16-Oct-1992 J. Unger initial revision, using object-manager code
;;; as a guide.
;;; 13-Dec-1992 J. Unger modify refresh-image so that it calls draw if
;;; there is an image at that z-level and otherwise deletes the
;;; image-primitive object from the view if there is no such image at
;;; this z-level.  Also add draw command to image-view-mediator
;;; init-inst method.
;;; 31-Dec-1992 I. Kalet reorganize refresh function for image-view
;;; mediator, since there is no image graphic primitive.  Also set
;;; origin and scale of view after drawing new image in refresh-image
;;; 06-Jan-1993 J. Unger enhance refresh-view to compute reformatted images
;;; for subsequent display in coronal & sagittal views on demand.  Also
;;; add a current-image cache to image-view-mediator to make the drawing
;;; of cor/sag images more efficient.
;;; 07-Jan-1993 J. Unger modify make-sagittal-image and make-coronal-image
;;; to consider the origin of each slice from the original image set to 
;;; lie in the middle of the image's thickness (so anatomy appears in the
;;; center of each strip, rather than on one end).
;;; 24-Mar-1993 J. Unger fix type declaration related problems for cmucl
;;; compiler.
;;;  1-May-1993 I. Kalet move some functions to medical-images module
;;;  5-Nov-1993 I. Kalet reset view origin and/or scale on image
;;;  refresh if changed while image is not displayed.
;;; 18-Apr-1994 I. Kalet update refs to view origin
;;;  8-Jan-1995 I. Kalet remove proclaim form
;;;  3-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx.
;;; 10-Jul-1998 I. Kalet in refresh-image check if BEV, to make new
;;; image in that case.
;;; 12-Aug-1998 I. Kalet add code to update DRR if BEV changes.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;;  5-Jan-2000 I. Kalet relax plane match criterion for display.
;;; 27-Jun-2000 I. Kalet parametrize format for printing plane z value
;;; 16-Jul-2000 I. Kalet reorganize refresh-image for OpenGL rendering
;;; of images in views.
;;;  4-Sep-2000 I. Kalet finish reorg for OpenGL: eliminate special
;;; handling of beam's eye views, eliminate window and level caches,
;;; handle explicit announcements instead of former generic refresh-bg.
;;; 13-Dec-2000 I. Kalet need *some* special handling of beam's eye
;;; views, for DRR, including default display-func for progressive
;;; display of DRR as it is generated, band by band.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defclass image-view-manager ()

  ((image-set :type list
              :accessor image-set
              :initarg :image-set
              :documentation "The set of image-2D's that are to appear
in the views.")

   (view-set ;; :type coll:collection
             :accessor view-set
             :initarg :view-set
             :documentation "The set of views for some plan.")

   (mediator-set ;; :type coll:collection
                 :accessor mediator-set
                 :initform (coll:make-collection)
                 :documentation "The set of image-view mediators.
Each one handles updates of a particular view for a particular image
set.  They are created when a view is created and added to the image
set.  They are deleted when a view is deleted.")

  )

  (:documentation "This is the object that creates and deletes the 
mediators for an set of image-2D's to appear in a given set of views.")

  )

;;;--------------------------------------------------

(defclass image-view-mediator ()

  ((images :reader images
           :initarg :images
           :documentation "The list of images this mediator manages
views for.")
                
   (view :reader view
         :initarg :view
         :documentation "The view in which an image may appear.")

   (image :accessor image
          :initarg :image
          :initform nil
          :documentation "A cache containing the image most recently
associated with the view.")

   ))

;;;--------------------------------------------------

(defun refresh-image (ivm v)

  "refresh-image ivm v

Refreshes the image determined by the image-view-mediator ivm's images
list and view v's view-position to the screen."

  (if (or (null (image ivm))
	  ;; compare view position with image position wrt. view
	  (not (poly:nearly-equal (view-position v)
				  (view-pos-from-image v (image ivm))
				  *display-epsilon*)))
      ;; try to generate the required image
      (let ((im (generate-image-from-set v (images ivm))))
	(setf (image ivm) im)
	(setf (image-cache v) nil)
	(if im (draw im v)
	  (progn
	    (format t (concatenate 'string
			"No image at plane " *display-format*  "~%")
		    (view-position v))
	    (clx:draw-rectangle (background v) ; fill area with black
				(sl:color-gc 'sl:black)
				0 0
				(clx:drawable-width (background v))
				(clx:drawable-height (background v))
				t))))
    ;; image present, so refresh as necessary
    (draw (image ivm) v)))

;;;--------------------------------------------------

(defmethod initialize-instance :after ((ivm image-view-mediator)
				       &rest initargs)

  "Draws the relevant image in the view initially as well as
registering for future updates."

  (declare (ignore initargs))
  (let ((v (view ivm)))
    (ev:add-notify ivm (new-position v)
		   #'(lambda (med vw newpos)
		       (declare (ignore newpos))
		       (if (background-displayed vw)
			   (refresh-image med vw)
			 (setf (image med) nil
			       (image-cache vw) nil))))
    (ev:add-notify ivm (new-scale v)
		   #'(lambda (med vw newscl)
		       (declare (ignore newscl))
		       (if (background-displayed vw)
			   (draw (image med) vw)
			 (setf (image-cache vw) nil))))
    (ev:add-notify ivm (new-origin v)
		   #'(lambda (med vw org)
		       (declare (ignore org))
		       (if (background-displayed vw)
			   (draw (image med) vw)
			 (setf (image-cache vw) nil))))
    (ev:add-notify ivm (new-winlev v)
		   #'(lambda (med vw)
		       (setf (image-cache vw) nil)
		       (if (background-displayed vw)
			   (draw (image med) vw))))
    (ev:add-notify ivm (bg-toggled v)
		   #'(lambda (med vw)
		       (if (background-displayed vw)
			   (unless (image-cache vw)
			     (refresh-image med vw)))))
    ;; some extra initialization for BEVs
    (when (typep v 'beams-eye-view)
      ;; this is in case the *direction* of the view's beam changes
      ;; we need to find a better way to detect and handle it.
      (ev:add-notify ivm (reset-image v)
		     #'(lambda (med vw)
			 (setf (image med) nil)
			 (setf (image-cache vw) nil)
			 (setf (background-displayed vw) nil)))
      (if (not (display-func v))
	  (setf (display-func v) #'(lambda (bev)
				     (setf (image-cache bev) nil)
				     (draw (image ivm) bev)
				     (display-view bev)))))
    (when (background-displayed v)
      (refresh-image ivm v)
      (display-view v))))

;;;--------------------------------------------------

(defun make-image-view-mediator (images view)

  "make-image-view-mediator images view

Makes and returns a mediator between a list of images and a view.
When a new position event is announced by the view, the image on the
list corresponding to the value of the new position along the axis
perpendicular to the view is displayed in the view.  If no such image
corresponds, the backing pixmap of the view is erased but the view is
not changed otherwise."

  (make-instance 'image-view-mediator :images images :view view))

;;;--------------------------------------------------

(defmethod destroy ((ivm image-view-mediator))

  (ev:remove-notify ivm (new-position (view ivm)))
  (ev:remove-notify ivm (new-scale (view ivm)))
  (ev:remove-notify ivm (new-origin (view ivm)))
  (ev:remove-notify ivm (new-winlev (view ivm)))
  (ev:remove-notify ivm (bg-toggled (view ivm)))
  (if (typep (view ivm) 'beams-eye-view)
      (ev:remove-notify ivm (reset-image (view ivm))))
  )

;;;--------------------------------------------------

(defmethod initialize-instance :after ((m image-view-manager)
				       &rest initargs)

  "Fills the mediator set by iterating over views with images
and creates the links to dynamically create and delete mediators as 
necessary when objects and views are created and deleted."

  (declare (ignore initargs))
  (let ((is (image-set m))
        (vs (view-set m)))
    (dolist (v (coll:elements vs))
      (coll:insert-element (make-image-view-mediator is v)
			   (mediator-set m)))
    (ev:add-notify m (coll:inserted vs)
                   #'(lambda (md a v)
                       (declare (ignore a))
                       (coll:insert-element
			(make-image-view-mediator is v)  
			(mediator-set md))))
    (ev:add-notify m (coll:deleted vs)
                   #'(lambda (md a v)
                       (declare (ignore a))
                       (let ((med-set (mediator-set md)))
                         (dolist (med (coll:elements med-set))
                                 (when (eq (view med) v)
                                       (coll:delete-element med med-set)
                                       (destroy med))))))))

;;;--------------------------------------------------

(defun make-image-view-manager (image-set view-set)

  "make-image-view-manager image-set view-set

Returns an instance of an image-view-manager, a mediator between a
set of images and a set of views they appear in."

  (make-instance 'image-view-manager
		 :image-set image-set
                 :view-set view-set))

;;;--------------------------------------------------
;;; End.
