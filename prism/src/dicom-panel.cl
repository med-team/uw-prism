;;;
;;; dicom-panel
;;;
;;; The Dicom panel GUI.
;;; Contains functions used in Client only.
;;;
;;; 22-Jun-2000  J. Jacky  Based on write-neutron.cl
;;; Change "neutron" to "dicom" throughout (but leave "np" and "np-" prefixes)
;;;  so old write-neutron becomes write-dicom.
;;; Remove phys-name, presc-dose from panel
;;; initialize-instance: get-therapy-machine SL20A-6MV-MLC not CNTS-BLOCKS
;;; initialize-instance: add np-vert-fudge 45 to make more room for SL20 leaves
;;; defmethod :after new-plan: multileaf-coll not cnts-coll
;;; 26-Jun-2000  J. Jacky  Rename file to dicom-panel.cl
;;;                        Break up write-dicom into assemble-,log-,send-dicom
;;; 29-Jun-2000  J. Jacky  Separate out assemble-dicom etc. to dicom-rtplan.cl
;;; 11-Sep-2000  J. Jacky  Add textboxes for machine name, X1, X2, Y1, Y2
;;; 12-Sep-2000  J. Jacky  Exchange positions of Y1, Y2 textboxes on panel
;;;                        Invert sign of edited Elekta Y2 leaves (-x side)
;;;  8-Dec-2000  J. Jacky  show ack box with send-dicom status, msgs
;;;  9-Jul-2001  J. Jacky  write-file-b confirm: ".. a few minutes" not seconds
;;;                        add-beam-b: check constraint-violations
;;; 10-Jul-2001  J. Jacky  Change several textlines, buttons to readouts:
;;;                         gan-start gan-stop n-treat mu-treat col-ang
;;;                         couch-ang wdg-sel wdg-rot mach
;;;                        Add machine id readout
;;;                        add-notify's so can edit X1,X2,Y1,Y2
;;; 12-Jul-2001  J. Jacky  beam-s: add beam-problems, separate out set-leaves
;;; 13-Jul-2001  J. Jacky  set-leaves: open leaves at ends of field if needed
;;;                        add-beam-b: show no more than 25 constraint viol'ns
;;; 26-Jul-2001  J. Jacky  Round leaf, jaw settings to 0.1mm and display same
;;; 30-Jul-2001  J. Jacky  Get rid of "No Machine" after "Machine: " in caption
;;;                        Uncomment call to chart-panel
;;; 31-Jul-2001  J. Jacky  Call dicom-chart-panel not chart-panel
;;;  2-Aug-2001  J. Jacky  New row of buttons: Preview Chart, Add Segments etc.
;;;                        Remove date-rdt to make room (put data in plan-rdt)
;;;  3-Aug-2001  J. Jacky  reverse beam list in calls to dicom-chart-panel
;;;                        display total and daily MU to 0.1
;;; 27-Aug-2001  J. Jacky  fix set-leaves-jaws: handle out-of-range ymin,ymax
;;; 28-Aug-2001  J. Jacky  fix leaf left-tlns add-notify: Elekta Y2 leaf sign
;;; 28-Aug-2001  J. Jacky  set-leaves-jaws: call new make-flagpole
;;;  6-Sep-2001  J. Jacky  add-beam-b shows confirm box with new beam-warnings
;;;  7-Sep-2001  J. Jacky  Fix range check on left-leaf-tlns consistent w/28Aug
;;;                        Add flag-diff, shape-diff
;;; 11-Sep-2001  J. Jacky  Move out several functions to mlc-collimators.cl
;;;                        rename beam-warnings to collim-warnings, pass colls
;;;                        rename constraint-violations to collim-constraint-..
;;; 12-Sep-2001  J. Jacky  Delete set-leaves-jaws,call make-multileaf-coll etc.
;;; 12-Sep-2001  J. Jacky  send-dicom second value is just string,not list of..
;;; 13-Sep-2001  J. Jacky  add-seg-b, add-beam-b both call new add-beam
;;; 14-Sep-2001  J. Jacky  Pass beam-info to assemble-dicom, dicom-chart-panel
;;;                        beam-info contains segment information
;;; 18-Sep-2001  J. Jacky  add-beam uses new beam-rec struct
;;;                        send-beam calls new calc-seg-info, uses new seg-rec
;;; 19-Sep-2001  J. Jacky  Use list instead of beam-rec struct so can copy-tree
;;; 20-Sep-2001  J. Jacky  calc-seg-info: support new bnum field in seg-rec
;;;                        first-segment: add :initform nil
;;;                        add-beam, segment-violations: fix bugs
;;;                        dicom-panel, add-beam: support new segment-color
;;; 21-Sep-2001  J. Jacky  Remove first-segment, just use tail of OUTPUT-ALIST,
;;;                        separate out del-beam function
;;; 26-Sep-2001  J. Jacky  calc-seg-info: calculate new cum field in seg-rec
;;; 01-Oct-2001  J. Jacky  Move out calc-seg-info, segment-violations
;;;                        new ADD-SEG-INFO calls CALC-SEG-INFO.
;;; 12-Oct-2001  J. Jacky  beam-prolems: test machine-id's with equal not eq
;;; 24-Oct-2001  J. Jacky  Fix :label :info confusion in mach-, wdg- rdt's
;;; 26-Oct-2001  J. Jacky  Distinguish warnings from failures in write-file-b
;;; 31-Oct-2001  J. Jacky  Accommodate VJC collimators as well as MLC
;;;  2-Nov-2001  J. Jacky  beam-problems: ext. wedge, blocks not in same beam
;;;  5-Dec-2001  J. Jacky  write-file-b: prompt for patient ID for DICOM-RT/RTD
;;; 10-Dec-2001  J. Jacky  write-file-b: pass dicom-pat-id to dicom-chart-panel
;;;                         (but only after actual transfer, not chart preview)
;;; 31-Jan-2002 I. Kalet move round-digits from here to
;;; mlc-collimators to remove circular module dependency.
;;; 12-Feb-2002   BobGian  Two calls to dicom-chart-panel -> chart-panel.
;;; 29-May-2003 M Phillips added Dose Monitor Points section.
;;; 09-Aug-2002   BobGian  Add interface from panel to Dicom engine for
;;;                        Dose Monitoring Points.
;;;                        Rename var "np" and prefix "np-" -> "dp", "dp-".
;;;                        DOTIMES/NTH -> DO, IF -> WHEN/UNLESS/COND,
;;;                        LET* -> LET, and similar source->source
;;;                        simplifications and optimizations when possible.
;;; 27-Aug-2003    BobGian add Dose-Monitoring Points.
;;; 05-Sep-2003    BobGian regularize slot/accessor and local-variable names:
;;;                      Type:           Slot/Accessor:      Local var:
;;;                        BUTTON          xxx-button          xxx-bn
;;;                        FRAME           xxx-frame           xxx-frm
;;;                        READOUT         xxx-label           xxx-lbl
;;;                        READOUT         xxx-readout         xxx-rd
;;;                        SCROLLING-LIST  xxx-scrollinglist   xxx-sl
;;;                        SPREADSHEET     xxx-spreadsheet     xxx-ss
;;;                        TEXTLINE        xxx-textline        xxx-tl
;;; 12-Sep-2003    BobGian regularize some internal function names:
;;;                     ADD-BEAM  ->  ADD-BEAM-FCN
;;;                       Add DMPLIST arg to ADD-BEAM-FCN to convey DMPs
;;;                       to OUTPUT-ALIST.
;;;                     DEL-BEAM  ->  DEL-BEAM-FCN
;;;                     Change association lists [PLAN-ALIST, BEAM-ALIST, and
;;;                     OUTPUT-ALIST] from CDR-keyed [using ACONS and RASSOC]
;;;                     to more understandable CAR-keyed [CONS and ASSOC].
;;; 19-Sep-2003    BobGian Add new slot, COORDS, to DMP struct, carrying list
;;;                        of X,Y,Z coords in Dicom convention, millimeters,
;;;                        rounded to fixed precision [two decimal places].
;;; 03-Oct-2003    BobGian Add type declarations to DICOM-PANEL slots.
;;; 07-Oct-2003    BobGian Move ADD-SEG-INFO here -> "imrt-segments.cl".
;;; 20-Oct-2003    BobGian Move DMP defstruct here -> "dicom-rtplan"
;;;                        to simplify dependencies.
;;; 03-Nov-2003    BobGian Remove read-time evaluation for constant expressions
;;;                        where compile can optimize based on DEFCONSTANT in
;;;                        same file.
;;;                        Action fcns for Select-Plan and Select-Beam events
;;;                        clear DMPLIST directly rather than calling
;;;                        Deselect-Point action function.
;;; 18-Nov-2003    BobGian Got deselect-point action function business all
;;;                        screwed up.  Redid it correctly.  Ready for testing
;;;                        against real Elekta server.  Also: Added CONFIRM
;;;                        popup if no DMPs selected when ADD-BEAM invoked.
;;; 24-Nov-2003 BobGian: DMP auto-replication scheme altered.  As DMPs are
;;;    selected for a beam/segment, any existing dose values are pushed onto
;;;    lists in the the "OTHER-xxx-DOSES" slots, the current slots cleared,
;;;    and the current slots then accumulate dose values representing the
;;;    current beam/segment only.  Also, if point is deselected from a beam,
;;;    corresponding DMP must be tested for sharing: if shared, pop current
;;;    beam from beams contributing to this DMP; if not shared, deleted DMP
;;;    from current DMPLIST.  [See changelog notes, same date, in files
;;;    "dicom-rtplan" and "imrt-segments".]
;;; 25-Nov-2003 BobGian: ADD-SEG-INFO needs DMP-CNT arg to allow continued
;;;    counting while auto-replicating DMPs.
;;; 16-Dec-2003 BobGian: COMPUTE-DOSE-POINTS only called when a DMP is first
;;;    selected.  Later operations are guaranteed that point-doses are valid.
;;;    NUMBER-OF-FRACS-TEXTLINE -> REMAINING-FRACS-READOUT [read/only].
;;; 19-Dec-2003 BobGian:
;;;    Number of treatments [fractions] clarified:
;;;     NUM-TOTAL-FRAC is total number of fractions [set by (N-TREATMENTS <BI>)
;;;      in Prism plan] and is read/only here.
;;;     NUM-TREATED-FRAC is number of fractions already administered,
;;;      modifiable in DICOM-PANEL.
;;;     NUM-REMAINING-FRAC is difference between above two, calculated here.
;;;    UPDATE-DMP-SPREADSHEET, DISPLAY-DMP-SPREADSHEET, ERASE-DMP-SPREADSHEET:
;;;     All now only called at one point - open-coded at point of call.
;;;    REFRESH-DMP-SPREADSHEET: Order of args changed.
;;; 23-Dec-2003 BobGian: Added missing DESTROY calls for DICOM-PANEL resources.
;;; 25-Dec-2002 BobGian: Flushed all "OTHER-..." slots.  Now we allocate a
;;;    separate DMP object for each segment in which the DMP appears, linking
;;;    them through the list in the DMP-SEGLIST slot of each so that dose can
;;;    be accumulated properly across all segments in a single beam.
;;; 28-Dec-2003 BobGian: DMP slots ...-dose -> ...-cGy to emphasize dose units.
;;; 30-Dec-2003 BobGian, Mark Phillips: Decided to factor apart the packaging
;;;    of Prism beams into multisegmented beams from the allocation of DMPs
;;;    to beams.  Also, users is free to allocate DMPs to any subset of beams
;;;    desired - no auto-replication of DMPs whose dose comes from multiple
;;;    beams.  These design choices considerable simplify beam/DMP allocation
;;;    logic and user interface model, at cost of extra pop-up menus.
;;; 31-Dec-2003 BobGian: ADD-SEG-INFO no longer needs DMP-CNT - no replicating.
;;; 20-Jan-2004 M Phillips: modified DMP panel.
;;; 27-Jan-2004 BobGian integrated Mark's work with rest of Dicom panel.
;;; 10-Feb-2004 BobGian replaced ROUND-DIGITS with COERCE [to SINGLE-FLOAT],
;;;    since object generator prints flonums rounded to 2 decimal places.
;;; 27-Jan-2004 BobGian began integration of new DMP Panel by Mark Phillips
;;;    with rest of Dicom Panel and interface to Dicom SCU.
;;; 15-Feb-2004 BobGian: ADD-BEAM-FCN no longer handles DMPs.  New version of
;;;    DMP mechanism is based on factoring of DICOM panel into two panels,
;;;    Dicom Panel for segment aggregation into beams and DMP Panel for
;;;    allocation of DMPs to [aggregated or "Dicom"] beams.
;;; 19-Feb-2004 BobGian - introduced uniform naming convention explained
;;;    in file "imrt-segments".  This includes:
;;;    ADD-BEAM-FCN -> ADD-PRISM-BEAM-FCN
;;;    BEAM-ALIST -> PRISM-BEAM-ALIST
;;;    BEAM-LABEL -> PRISM-BEAM-LABEL
;;;    BEAM-PROBLEMS -> PRISM-BEAM-PROBLEMS
;;;    BEAM-READOUT -> PRISM-BEAM-READOUT
;;;    BEAM-SCROLLINGLIST -> PRISM-BEAM-SCROLLINGLIST
;;;    DEL-BEAM-FCN -> DEL-PRISM-BEAM-FCN
;;; 25-Feb-2004 BobGian - ADD-SEG-INFO -> GENERATE-PBEAM-INFO.
;;; 26-Feb-2004 BobGian completed DMP integration.
;;; 27-Feb-2004 BobGian made DICOM-PANEL operate at pushed event level.
;;; 28-Feb-2004 BobGian - undo pushed event level for Dicom Panel.
;;;    Reverse second arg to ADD-PRISM-BEAM-FCN [fixes grouper bug].
;;; 01-Mar-2004 BobGian place constraint-checking on beams/DMPs about to be
;;;    sent immediately on Send-Beams button press, before Patient ID typein.
;;;    WRITE-FILE-BUTTON slot [and local var] -> SEND-BEAMS-BUTTON.
;;; 29-Apr-2004 BobGian: Added declaration in SEGMENT-VIOLATIONS.
;;; 30-Apr-2004 BobGian: Renamed a few function parameters and local vars to
;;;    better distinguish between Original and Current Prism beam instances.
;;; 03-May-2004 BobGian:
;;;    1. Segmented SETF :after method of CURRENT-PRISM-BI into two separate
;;;       functions, CLEAR-CURRENT-PRISM-BI and SETUP-CURRENT-PRISM-BI, to
;;;       simplify operation.  Placed appropriate function call at each point
;;;       of assignment operation.
;;;    2. Fixed bug in incorrect assignment of new collimator to
;;;       ORIGINAL-PRISM-BI slot of DICOM-PANEL object.
;;; 12-May-2004 BobGian:
;;;    SEGMENT-VIOLATIONS - reversed args [consistent with other comparisons].
;;;      Also one of the arguments to FORMAT was mislabeled.
;;; 17-May-2004 BobGian complete process of extending all instances of Original
;;;      and Current Prism beam instances to include Copied beam instance too,
;;;      to provide copy for comparison with Current beam without mutating
;;;      Original beam instance.
;;;    CLEAR-CURRENT-PRISM-BI -> CLEAR-PRISM-BI-TRIPLE
;;;    SETUP-CURRENT-PRISM-BI -> SETUP-PRISM-BI-TRIPLE
;;; 24-May-2004 BobGian: Remove check for matching machine name when adding
;;;    segment in SEGMENT-VIOLATIONS.  Proper test [for matching first element
;;;    of IDENT slot of MACHINE] is already done by PRISM-BEAM-PROBLEMS.
;;;    Add matching energy check when adding new segment in ADD-PRISM-BEAM-FCN.
;;;    Proceeding on mismatch is confirmable rather than being disallowed.
;;; 27-May-2004 BobGian: Fix bugs in ADD-PRISM-BEAM-FCN.  Add checks for empty
;;;    beam list [in slot OUTPUT-ALIST of DP] in places where users of data
;;;    assume non-empty lists.  Fix ambiguities in ACKNOWLEDGE messages
;;;    relevant to "No beam selected" and "No beams/segs added" situations.
;;; 19-Sep-2004 BobGian: Call to CHECK-BEAM-DMP-CONSTRAINTS changed to
;;;    CHECK-BEAM-CONSTRAINTS.
;;; 05-Oct-2004 BobGian fixed a few lines to fit within 80 cols.
;;; 01-Nov-2004 BobGian add forgotten check for "New Segment" in check for
;;;    Wedge IN/OUT segment pair, altered leaf settings, in ADD-PRISM-BEAM-FCN.
;;; 04-Nov-2004 BobGian add default error message for *STATUS-ALIST*.
;;;

(in-package :prism)

;;;=============================================================

(defclass dicom-panel ( )

  ((frame :type sl:frame
	  :accessor frame
	  :documentation "The Slik frame that contains the Dicom panel.")

   ;; Three buttons along top, copied from neutron panel.

   (del-panel-button :type sl::button
		     :accessor del-panel-button
		     :documentation "The Delete-Panel button for this panel.")

   (add-prism-beam-button :type sl::button
			  :accessor add-prism-beam-button
			  :documentation "The Add-Beam button for this panel.")

   (send-beams-button :type sl::button
		      :accessor send-beams-button
		      :documentation "The Send-Beams button for this panel.")

   ;; Three new buttons along top, right below top row.

   (preview-chart-button :type sl::button
			 :accessor preview-chart-button
			 :documentation "The Preview-Chart button.")

   (add-seg-button :type sl::button
		   :accessor add-seg-button
		   :documentation "The Add-Segment button for this panel.")

   (dmp-panel-button :type sl::button
		     :accessor dmp-panel-button
		     :documentation "The DMP button creates the DMP panel.")

   (comments-box :type sl::textbox
		 :accessor comments-box
		 :documentation "The Plan-Comments box for this panel.")

   (comments-label :type sl::readout
		   :accessor comments-label
		   :documentation "The label for this panel's comments box.")

   (prism-beam-readout :type sl::readout
		       :accessor prism-beam-readout
		       :documentation "The beam readout for this panel.")

   (plan-readout :type sl::readout
		 :accessor plan-readout
		 :documentation "The plan name and date readout.")

   (plan-scrollinglist :type sl::scrolling-list
		       :accessor plan-scrollinglist
		       :documentation "A scrolling list of available plans.")

   (plan-label :type sl::readout
	       :accessor plan-label
	       :documentation "The label for the plans scrolling list.")

   (prism-beam-scrollinglist
     :type sl::scrolling-list
     :accessor prism-beam-scrollinglist
     :documentation "A scrolling list of available Prism beams.")

   (prism-beam-label
     :type sl::readout
     :accessor prism-beam-label
     :documentation "The label for the Prism-beams scrolling list.")

   (output-scrollinglist :type sl::scrolling-list
			 :accessor output-scrollinglist
			 :documentation "A scrolling list of Prism beams
to be output by the Dicom panel.")

   (output-label :type sl::readout
		 :accessor output-label
		 :documentation "The label for the output scrolling list.")

   (gantry-start-readout :type sl::readout
			 :accessor gantry-start-readout
			 :documentation "The gantry starting angle readout.")

   (gantry-stop-readout :type sl::readout
			:accessor gantry-stop-readout
			:documentation "The gantry stopping angle readout.")

   (n-treat-readout :type sl::readout
		    :accessor n-treat-readout
		    :documentation "The num treatments readout.")

   (tot-mu-readout :type sl::readout
		   :accessor tot-mu-readout
		   :documentation "The total monitor units readout.")

   (mu-treat-readout :type sl::readout
		     :accessor mu-treat-readout
		     :documentation "The monitor units per treatment readout.")

   (coll-angle-readout :type sl::readout
		       :accessor coll-angle-readout
		       :documentation "The collimator angle readout.")

   (couch-angle-readout :type sl::readout
			:accessor couch-angle-readout
			:documentation "The couch angle readout.")

   (wedge-sel-readout :type sl::readout
		      :accessor wedge-sel-readout
		      :documentation "The wedge selection readout.")

   (wedge-rot-readout :type sl::readout
		      :accessor wedge-rot-readout
		      :documentation "The wedge rotation readout.")

   (left-leaf-textlines
     :type list
     :accessor left-leaf-textlines
     :initform nil
     :documentation "A list of left side mlc leaf textlines.")

   (right-leaf-textlines
     :type list
     :accessor right-leaf-textlines
     :initform nil
     :documentation "A list of right side mlc leaf textlines.")

   (plan-alist :type list
	       :accessor plan-alist
	       :initform nil
	       :documentation "An association list of buttons and plans in
the panel's scrolling list of plans.")

   ;; This stores original uncopied Prism beams from Prism plans in an alist.
   (prism-beam-alist :type list
		     :accessor prism-beam-alist
		     :initform nil
		     :documentation "An association list of buttons and
Prism [original] beam instances in the panel's scrolling list of beams.")

   ;; Use original/copied/current-beam triples to flag changed items on chart.
   ;; This list is maintained in REVERSE order (new items pushed) while using
   ;; panel to accumulate beam-segments.  Order is reversed in
   ;; GENERATE-PBEAM-INFO before passing data to Dicom interface.
   (output-alist :type list
		 :accessor output-alist
		 :initform nil
		 :documentation
		 "The association list of buttons and beam-descriptor
objects [ <OrigBmInst> <CopyBmInst> <CurrBmInst> ... ]
in the panel's scrolling list of Prism beams to be output.")

   (current-patient :type patient
		    :accessor current-patient
		    :initarg :current-patient
		    :documentation "The current patient for the
Dicom panel, supplied at initialization time.")

   (current-plan :type plan
		 :accessor current-plan
		 :initform nil
		 :documentation "The plan that the Dicom panel is
currently displaying.")

   ;; Beam instance here is original Prism beam, not copied, and containing
   ;; beam's original DOSE-RESULT object.
   (original-prism-bi :type beam
		      :accessor original-prism-bi
		      :initform nil
		      :documentation "The original version of the
Prism beam instance that the Dicom panel is currently displaying.")

   ;; Beam instance here is copied from original Prism beam but not mutated.
   ;; Its collimator is changed to MLC for comparison against copy in
   ;; CURRENT-PRISM-BI, but it itself is NOT mutated by user actions.
   ;; Does NOT contain DOSE-RESULT object.
   (copied-prism-bi :type beam
		    :accessor copied-prism-bi
		    :initform nil
		    :documentation "The copied original version of the
Prism beam instance that the Dicom panel is currently displaying.")

   ;; Beam instance here is copied and user-mutated from original Prism beam.
   ;; Does NOT contain DOSE-RESULT object.
   (current-prism-bi :type beam
		     :accessor current-prism-bi
		     :initform nil
		     :documentation "The Prism beam instance that the
Dicom panel is currently displaying and modifying.")

   (collim-info :accessor collim-info
		:documentation "A cache for the collimator info of the
current Prism beam instance.")

   ;; New stuff to support Elekta accelerators

   (machine-readout :type sl::readout
		    :accessor machine-readout
		    :documentation "The machine selection readout.")


   (id-readout :type sl::readout
	       :accessor id-readout
	       :documentation "The machine identification readout.")

   (x1-textline :type sl::textline
		:accessor x1-textline
		:documentation "The X1 diaphragm textline.")

   (x2-textline :type sl::textline
		:accessor x2-textline
		:documentation "The X2 diaphragm textline.")

   (y1-textline :type sl::textline
		:accessor y1-textline
		:documentation "The Y1 diaphragm textline.")

   (y2-textline :type sl::textline
		:accessor y2-textline
		:documentation "The Y2 diaphragm textline.")

   (dicom-dmp-list :accessor dicom-dmp-list
		   :initform '()
		   :documentation "List that contains dose monitoring points
[Dicom sense: a DMP associated with a Dicom beam].  Used to initialize the
DMP Panel and to accumulate added DMPs.")

   (dicom-dmp-cnt :type fixnum
		  :accessor dicom-dmp-cnt
		  :initarg :dicom-dmp-cnt
		  :documentation "Instance counter for created DMPs.
Stored in Dicom Panel so DMP Panel can be initialized correctly."))

  (:documentation "The Dicom panel is used to select plans, to group Prism
beams [IMRT segments] into Dicom beams, and to send the Dicom beams
using DICOM-RT, acting as the Dicom client (SCU).")

  )

;;;-------------------------------------------------------------

(defparameter color-seq
  (vector 'sl:red 'sl:green 'sl:yellow 'sl:magenta 'sl:cyan))

;;;=============================================================
;;; Defconstants for Dicom panel.

(defconstant dp-off 10)                          ; Intercontrol spacing factor
(defconstant dp-rd-ht 30)                           ; readout height
(defconstant dp-rd-base 80)                         ; base readout width
(defconstant dp-sl-ht (* 4 dp-rd-ht))               ; scrolling list height
(defconstant dp-tb-ht (* 3 dp-rd-ht))               ; textbox height
(defconstant dp-ht
  (+ (* 11 dp-rd-ht)                             ; height of DMP Panel section
     (* 12  dp-off)
     dp-sl-ht
     45 ;extra space for more SL20 leaves - any larger won't fit on 1024 x 768
     dp-tb-ht))                                     ; panel height
(defconstant pln-sl-width (round (* 1.5 dp-rd-base)))

;;;=============================================================
;;; Main panel functionality
;;;=============================================================

(defmethod initialize-instance :after ((dp dicom-panel) &rest initargs)

  "Initializes the Dicom panel GUI."

  (let* ((cur-pat (current-patient dp))             ; The patient object

	 (dp-tl-color 'sl:green)                    ; textline border color
	 (dp-rd-color 'sl:white)                    ; readout border color
	 (dp-bt-color 'sl:cyan)                     ; button border color

	 (frm (apply #'sl:make-frame
		     ;; Width of DMP Panel section
		     (+ (* 6 dp-off)                ; panel width
			(* 10 dp-rd-base))
		     dp-ht                       ; height of DMP Panel section
		     :title
		     (format nil "Prism Dicom Panel -- ~A" (name cur-pat))
		     initargs))
	 (frm-win (sl:window frm))

	 (plan-sl (apply #'sl:make-radio-scrolling-list
			 (round (* 1.5 dp-rd-base)) dp-sl-ht
			 :parent frm-win
			 :ulc-x dp-off
			 :ulc-y (+ (* 3 dp-off) (* 3 dp-rd-ht))
			 :border-color dp-bt-color
			 initargs))
	 (p-bm-sl (apply #'sl:make-radio-scrolling-list
			 (round (* 1.5 dp-rd-base)) dp-sl-ht
			 :parent frm-win
			 :ulc-x (+ (* 2 dp-off) pln-sl-width)
			 :ulc-y (+ (* 3 dp-off) (* 3 dp-rd-ht))
			 :border-color dp-bt-color
			 initargs))
	 (output-sl (apply #'sl:make-scrolling-list
			   (* 3 dp-rd-base) dp-sl-ht
			   :parent frm-win
			   :ulc-x (+ (* 3 dp-off) (* 2 pln-sl-width))
			   :ulc-y (+ (* 3 dp-off) (* 3 dp-rd-ht))
			   :enable-delete t
			   :border-color dp-bt-color
			   initargs))

	 ;; Three buttons along top row
	 (del-panel-bn (apply #'sl:make-button
			      (* 2 dp-rd-base) dp-rd-ht
			      :parent frm-win
			      :ulc-x dp-off :ulc-y dp-off
			      :label "Delete Panel"
			      :button-type :momentary
			      :border-color dp-bt-color
			      initargs))

	 (add-prism-beam-bn (apply #'sl:make-button
				   (* 2 dp-rd-base) dp-rd-ht
				   :parent frm-win
				   :ulc-x (+ (* 2 dp-off) (* 2 dp-rd-base))
				   :ulc-y dp-off
				   :label "Add Beam"
				   :button-type :momentary
				   :border-color dp-bt-color
				   initargs))
	 (dmp-panel-bn (apply #'sl:make-button
			      (* 2 dp-rd-base) dp-rd-ht
			      :parent frm-win
			      :ulc-x (+ (* 3 dp-off) (* 4 dp-rd-base))
			      :ulc-y dp-off
			      :label "Dose Monitor Pts"
			      :button-type :momentary
			      :border-color dp-bt-color
			      initargs))
	 ;; Three buttons right underneath
	 (preview-chart-bn (apply #'sl:make-button
				  (* 2 dp-rd-base) dp-rd-ht
				  :parent frm-win
				  :ulc-x dp-off
				  :ulc-y (+ (* 2 dp-off) dp-rd-ht)
				  :label "Preview Chart"
				  :button-type :momentary
				  :border-color dp-bt-color
				  initargs))
	 (add-seg-bn (apply #'sl:make-button
			    (* 2 dp-rd-base) dp-rd-ht
			    :parent frm-win
			    :ulc-x (+ (* 2 dp-off) (* 2 dp-rd-base))
			    :ulc-y (+ (* 2 dp-off) dp-rd-ht)
			    :label "Add Segment"
			    :button-type :momentary
			    :border-color dp-bt-color
			    initargs))
	 (send-beams-bn (apply #'sl:make-button
			       (* 2 dp-rd-base) dp-rd-ht
			       :parent frm-win
			       :ulc-x (+ (* 3 dp-off) (* 4 dp-rd-base))
			       :ulc-y (+ (* 2 dp-off) dp-rd-ht)
			       :label "Send Beams"
			       :button-type :momentary
			       :border-color dp-bt-color
			       initargs))

	 ;; New stuff to support Elekta accelerators
	 (x1-tl (apply #'sl:make-textline
		       (round (* 1.5 dp-rd-base)) dp-rd-ht
		       :parent frm-win
		       :ulc-x dp-off               ; x position like gan-start
		       :ulc-y (+ (* 12 dp-off) (* 14 dp-rd-ht) dp-sl-ht)
		       :label "X1: "
		       :numeric t
		       :lower-limit 0.0 :upper-limit 20.0
		       :border-color dp-tl-color
		       initargs))

	 (x2-tl (apply #'sl:make-textline
		       (round (* 1.5 dp-rd-base)) dp-rd-ht
		       :parent frm-win
		       :ulc-x (+ (round (* 1.5 dp-off))
				 (round (* 1.5 dp-rd-base)))
		       :ulc-y (+ (* 12 dp-off) (* 14 dp-rd-ht) dp-sl-ht)
		       :label "X2: "
		       :numeric t
		       :lower-limit 0.0 :upper-limit 20.0
		       :border-color dp-tl-color
		       initargs))

	 ;; Y1 textline appears to right of Y2 textline because it's +X jaw
	 (y1-tl (apply #'sl:make-textline
		       (- (round (* 1.5 dp-rd-base)) 4) dp-rd-ht
		       :parent frm-win
		       :ulc-x (+ (round (* 3.5 dp-off))
				 (round (* 4.5 dp-rd-base)))
		       :ulc-y (+ (* 12 dp-off) (* 14 dp-rd-ht) dp-sl-ht)
		       :label "Y1: "
		       :numeric t
		       :lower-limit -12.5 :upper-limit 20.0
		       :border-color dp-tl-color
		       initargs))

	 (y2-tl (apply #'sl:make-textline
		       (round (* 1.5 dp-rd-base)) dp-rd-ht
		       :parent frm-win
		       :ulc-x (+ (* 3 dp-off) (* 3 dp-rd-base)) ;gan-stop
		       :ulc-y (+ (* 12 dp-off) (* 14 dp-rd-ht) dp-sl-ht)
		       :label "Y2: "
		       :numeric t
		       :lower-limit -12.5 :upper-limit 20.0
		       :border-color dp-tl-color
		       initargs))

	 (no-bm-sel-msg "No beam selected yet.")
	 (no-bms/segs-msg "No beams or segments added yet."))

    (setf (frame dp) frm

	  (comments-box dp)
	  (apply #'sl:make-textbox
		 (+ (* 6 dp-rd-base) (* 2 dp-off))
		 dp-tb-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 6 dp-off) (* 6 dp-rd-ht) dp-sl-ht)
		 :border-color dp-rd-color
		 initargs)

	  (comments-label dp)
	  (apply #'sl:make-readout
		 (* 2 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 6 dp-off) (* 5 dp-rd-ht) dp-sl-ht)
		 :border-color 'sl:black
		 :label "Plan Comments:"
		 initargs)

	  (prism-beam-readout dp)
	  (apply #'sl:make-readout
		 (+ (* 6 dp-rd-base) (* 2 dp-off))
		 dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 5 dp-off) (* 4 dp-rd-ht) dp-sl-ht)
		 :border-color dp-rd-color
		 :label "Beam Name: "
		 initargs)

	  (plan-readout dp)
	  (apply #'sl:make-readout
		 (+ (* 6 dp-rd-base) (* 2 dp-off))
		 dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 4 dp-off) (* 3 dp-rd-ht) dp-sl-ht)
		 :border-color dp-rd-color
		 :label "Plan: "
		 initargs)

	  (plan-label dp) (apply #'sl:make-readout
				 (round (* 1.5 dp-rd-base)) dp-rd-ht
				 :parent frm-win
				 :ulc-x dp-off
				 :ulc-y (+ (* 2 dp-rd-ht) (* 2 dp-off))
				 :border-color 'sl:black
				 :label "Plans:"
				 initargs)

	  (plan-scrollinglist dp) plan-sl

	  (prism-beam-label dp) (apply #'sl:make-readout
				       (round (* 1.5 dp-rd-base)) dp-rd-ht
				       :parent frm-win
				       :ulc-x (+ (* 2 dp-off) pln-sl-width)
				       :ulc-y (+ (* 2 dp-rd-ht) (* 2 dp-off))
				       :border-color 'sl:black
				       :label "Beams:"
				       initargs)

	  (prism-beam-scrollinglist dp) p-bm-sl

	  (output-label dp) (apply #'sl:make-readout
				   (* 3 dp-rd-base) dp-rd-ht
				   :parent frm-win
				   :ulc-x (+ (* 3 dp-off) (* 2 pln-sl-width))
				   :ulc-y (+ (* 2 dp-rd-ht) (* 2 dp-off))
				   :border-color 'sl:black
				   :label "Output:"
				   initargs)

	  (output-scrollinglist dp) output-sl
	  (del-panel-button dp) del-panel-bn
	  (add-prism-beam-button dp) add-prism-beam-bn
	  (send-beams-button dp) send-beams-bn
	  (preview-chart-button dp) preview-chart-bn
	  (add-seg-button dp) add-seg-bn
	  (dmp-panel-button dp) dmp-panel-bn

	  (gantry-start-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 8 dp-off) (* 10 dp-rd-ht) dp-sl-ht)
		 :label "Gan start: "
		 :border-color dp-rd-color
		 initargs)

	  (gantry-stop-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x (+ (* 3 dp-off) (* 3 dp-rd-base))
		 :ulc-y (+ (* 8 dp-off) (* 10 dp-rd-ht) dp-sl-ht)
		 :label "Gan Stop: "
		 :border-color dp-rd-color
		 initargs)

	  (n-treat-readout dp)
	  (apply #'sl:make-readout
		 (* 2 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 11 dp-off) (* 13 dp-rd-ht) dp-sl-ht)
		 :label "N Treat: "
		 :border-color dp-rd-color
		 initargs)

	  (tot-mu-readout dp)
	  (apply #'sl:make-readout
		 (* 2 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x (+ (* 3 dp-off) (* 4 dp-rd-base))
		 :ulc-y (+ (* 11 dp-off) (* 13 dp-rd-ht) dp-sl-ht)
		 :label "Tot Mu: "
		 :border-color dp-rd-color
		 initargs)

	  (mu-treat-readout dp)
	  (apply #'sl:make-readout
		 (* 2 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x (+ (* 2 dp-off) (* 2 dp-rd-base))
		 :ulc-y (+ (* 11 dp-off) (* 13 dp-rd-ht) dp-sl-ht)
		 :label "Mu/Treat: "
		 :border-color dp-rd-color
		 initargs)

	  (coll-angle-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 9 dp-off) (* 11 dp-rd-ht) dp-sl-ht)
		 :label "Collim Ang: "
		 :border-color dp-rd-color
		 initargs)

	  (couch-angle-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x (+ (* 3 dp-off) (* 3 dp-rd-base))
		 :ulc-y (+ (* 9 dp-off) (* 11 dp-rd-ht) dp-sl-ht)
		 :label "Couch Ang: "
		 :border-color dp-rd-color
		 initargs)

	  (wedge-sel-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 10 dp-off) (* 12 dp-rd-ht) dp-sl-ht)
		 :label "Wedge Sel: "
		 :border-color dp-rd-color
		 initargs)

	  (wedge-rot-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x (+ (* 3 dp-off) (* 3 dp-rd-base))
		 :ulc-y (+ (* 10 dp-off) (* 12 dp-rd-ht) dp-sl-ht)
		 :label "Wedge Rot: "
		 :border-color dp-rd-color
		 initargs)

	  (machine-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x dp-off
		 :ulc-y (+ (* 7 dp-off) (* 9 dp-rd-ht) dp-sl-ht)
		 :label "Machine: "
		 :border-color dp-rd-color
		 initargs)

	  (id-readout dp)
	  (apply #'sl:make-readout
		 (* 3 dp-rd-base) dp-rd-ht
		 :parent frm-win
		 :ulc-x (+ (* 3 dp-off) (* 3 dp-rd-base))
		 :ulc-y (+ (* 7 dp-off) (* 9 dp-rd-ht) dp-sl-ht)
		 :label "Machine ID: "
		 :border-color dp-rd-color
		 initargs)

	  (x1-textline dp) x1-tl
	  (x2-textline dp) x2-tl
	  (y1-textline dp) y1-tl
	  (y2-textline dp) y2-tl)

    ;; Set the collim-info cache for the panel.
    ;; (previously we used SL20C-6MV-MLC in the therapy-machines database)
    (setf (collim-info dp) *sl-collim-info*)

    ;; Setup leaf textlines
    (do* ((collim-data (collim-info dp))
	  (column-len (num-leaf-pairs collim-data))
	  (leaf-tl-height (round (/ (float (- dp-ht (* 2 dp-off)))
				    column-len)))
	  (leaf-pairs (leaf-pair-map collim-data) (cdr leaf-pairs))
	  (leaf-tl-y dp-off (+ leaf-tl-y leaf-tl-height))
	  (left-tls '())
	  (right-tls '())
	  (idx 0 (the fixnum (1+ idx))))
	 ((= idx column-len)
	  (setf (left-leaf-textlines dp) (nreverse left-tls))
	  (setf (right-leaf-textlines dp) (nreverse right-tls)))
      (declare (type fixnum column-len leaf-tl-height leaf-tl-y idx))
      (push (sl:make-textline
	      (* 2 dp-rd-base) leaf-tl-height
	      :parent frm-win
	      :ulc-x (+ (* 6 dp-rd-base) (* 4 dp-off))
	      :ulc-y leaf-tl-y
	      :numeric t
	      :lower-limit (- (leaf-overcenter-limit collim-data))
	      :upper-limit (leaf-open-limit collim-data)
	      :label (format nil "Leaf ~2@A: " (first (first leaf-pairs)))
	      :border-color dp-tl-color
	      :volatile-width 4)                    ; shows up better
	    left-tls)
      (push (sl:make-textline
	      (* 2 dp-rd-base) leaf-tl-height
	      :parent frm-win
	      :ulc-x (+ (* 8 dp-rd-base) (* 5 dp-off))
	      :ulc-y leaf-tl-y
	      :numeric t
	      :lower-limit (- (leaf-overcenter-limit collim-data))
	      :upper-limit (leaf-open-limit collim-data)
	      :label (format nil "Leaf ~2@A: " (second (first leaf-pairs)))
	      :border-color dp-tl-color
	      :volatile-width 4)                    ; shows up better
	    right-tls))

    ;; Setup plan scrolling list
    (dolist (pln (coll:elements (plans cur-pat)))
      (let ((btn (sl:make-list-button plan-sl (name pln))))
	(sl:insert-button btn plan-sl)
	(push (cons btn pln) (plan-alist dp))))

    ;; Select-Plan button pressed.
    (ev:add-notify dp (sl:selected plan-sl)
      #'(lambda (dp ann p-bn)
	  (declare (ignore ann))
	  (when (current-prism-bi dp)
	    (ev:remove-notify dp (new-id (wedge (current-prism-bi dp))))
	    (ev:remove-notify dp (new-rotation (wedge (current-prism-bi dp)))))
	  (setf (original-prism-bi dp) nil)
	  (setf (copied-prism-bi dp) nil)
	  (setf (current-prism-bi dp) nil)
	  (clear-prism-bi-triple dp)
	  ;; Set plan.
	  (setf (current-plan dp)
		(cdr (assoc p-bn (plan-alist dp) :test #'eq)))))

    ;; Deselect-Plan button pressed.
    (ev:add-notify dp (sl:deselected plan-sl)
      #'(lambda (dp a btn)
	  (declare (ignore a btn))
	  (setf (current-plan dp) nil)))

    ;; Select-Prism-Beam button pressed.
    (ev:add-notify dp (sl:selected p-bm-sl)
      #'(lambda (dp ann b-bn)
	  (declare (ignore ann))
	  (let* ((orig-pbi (cdr (assoc b-bn (prism-beam-alist dp) :test #'eq)))
		 (pl (prism-beam-problems orig-pbi dp)))
	    ;; ORIG-PBI is original Prism beam instance, not a copy.
	    (cond
	      ((consp pl)
	       (push (format nil "Cannot select ~S." (name orig-pbi)) pl)
	       (sl:acknowledge pl))
	      (t (when (current-prism-bi dp)
		   (ev:remove-notify dp
		     (new-id (wedge (current-prism-bi dp))))
		   (ev:remove-notify dp
		     (new-rotation (wedge (current-prism-bi dp)))))

		 ;; Original uncopied Prism beam, containing DOSE-RESULT obj.
		 (setf (original-prism-bi dp) orig-pbi)

		 ;; COPIED-PRISM-BI must be copy of ORIG-PBI so that mutation
		 ;; of its collimator in SETUP-PRISM-BI-TRIPLE does not change
		 ;; beam object in original Prism plan.
		 (setf (copied-prism-bi dp) (copy orig-pbi))

		 (let ((new-pbi (copy orig-pbi)))
		   (setf (current-prism-bi dp) new-pbi)
		   (setup-prism-bi-triple new-pbi dp))

		 ;; Register with the current Prism beam instance's
		 ;; wedge ID and rotation events.
		 (ev:add-notify dp (new-id (wedge (current-prism-bi dp)))
		   #'(lambda (dp wdg id)
		       (declare (ignore wdg))
		       (when (zerop id)
			 (setf (sl:info (wedge-rot-readout dp)) "NONE"))
		       (setf (sl:info (wedge-sel-readout dp))
			     (wedge-label
			       id (machine (current-prism-bi dp))))))

		 (ev:add-notify dp (new-rotation (wedge (current-prism-bi dp)))
		   #'(lambda (dp wdg rot)
		       (cond ((zerop (id wdg))
			      (setf (sl:info (wedge-rot-readout dp)) "NONE"))
			     (t (let ((mach (machine (current-prism-bi dp))))
				  (setf (sl:info (wedge-rot-readout dp))
					(first (scale-angle
						 rot
						 (wedge-rot-scale mach)
						 (wedge-rot-offset
						   mach))))))))))))))

    ;; Deselect-Prism-Beam button pressed.
    (ev:add-notify dp (sl:deselected p-bm-sl)
      #'(lambda (dp a btn)
	  (declare (ignore a btn))
	  (when (current-prism-bi dp)
	    (ev:remove-notify dp (new-id (wedge (current-prism-bi dp))))
	    (ev:remove-notify dp (new-rotation (wedge (current-prism-bi dp)))))
	  (setf (original-prism-bi dp) nil)
	  (setf (copied-prism-bi dp) nil)
	  (setf (current-prism-bi dp) nil)
	  (clear-prism-bi-triple dp)))

    ;; Add-Prism-Beam button pressed.
    (ev:add-notify dp (sl:button-on add-prism-beam-bn)
      #'(lambda (dp a)
	  (declare (ignore a))
	  (add-prism-beam-fcn dp t)                 ;New beam
	  (setf (sl:on add-prism-beam-bn) nil)))

    ;; Add-Segment button pressed.
    (ev:add-notify dp (sl:button-on add-seg-bn)
      #'(lambda (dp a)
	  (declare (ignore a))
	  (add-prism-beam-fcn dp nil)               ;Successor segment
	  (setf (sl:on add-seg-bn) nil)))

    ;; Delete-Prism-Beam button pressed.
    (ev:add-notify dp (sl:deleted output-sl)
      #'(lambda (dp a btn)
	  (declare (ignore a))
	  (del-prism-beam-fcn dp btn)))

    ;; "Dose Monitor Pts" button pressed.
    (ev:add-notify dp (sl:button-on dmp-panel-bn)
      ;;
      ;; (OUTPUT-ALIST dp) is list [in reverse order] of objects, each:
      ;;  ( <Button> <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan>
      ;;    <New-Bm?> <SegColor> )
      ;;  <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
      ;;
      ;; This list contains all Prism beams - that is, all segments for all
      ;; Dicom beams, arranged int Dicom-beam order - all segments for one
      ;; Dicom beam followed by all segments for the next, and so forth.
      ;;
      ;; CopyBmInst and CurrBmInst are both copied beams so that any changes
      ;; to their collimators will not side-effect real Prism beam objects.
      ;;
      #'(lambda (dp a)
	  (declare (ignore a))
	  (let ((o-alist (output-alist dp)))
	    ;; Check to see if dose is calculated for all Prism beams.
	    ;; Allocation of DMPs requires all Prism-beam [segment] doses
	    ;; to be available.
	    (cond
	      ((consp o-alist)
	       (dolist (o-bmdata o-alist)
		 ;; Beam used for dose calculation is Original Prism beam.
		 ;; Only it contains DOSE-RESULT object in RESULT slot.
		 ;; It is passed indirectly as a component buried in patient's
		 ;; PLAN, which is first arg [fifth of O-BMDATA].
		 (unless (valid-points (result (second o-bmdata)))
		   (compute-dose-points (fifth o-bmdata) cur-pat)
		   (return)))
	       ;; Group Prism beam instances into Dicom beams here.  Lists of
	       ;; Beams and DMPs passed to DMP Panel [and values cached on
	       ;; return] are Dicom-Beams and Dicom-DMPs.
	       (cond ((current-plan dp)
		      (run-dmp-panel
			:parent-panel dp
			:dicom-beam-list (pbeam->dbeam-grouper o-alist)
			:dicom-dmp-list (dicom-dmp-list dp)
			:dicom-dmp-cnt (dicom-dmp-cnt dp)))
		     (t (sl:acknowledge "No plan selected yet."))))
	      (t (sl:acknowledge no-bms/segs-msg)))
	    (setf (sl:on dmp-panel-bn) nil))))

    ;; Send-Beams button pressed.
    (ev:add-notify dp (sl:button-on send-beams-bn)
      #'(lambda (dp a &aux dicom-pat-id p-bm-info (o-alist (output-alist dp))
		    (d-dmp-list (dicom-dmp-list dp)))

	  (declare (type list p-bm-info o-alist d-dmp-list)
		   (ignore a))

	  (block send-beams

	    (unless (consp o-alist)
	      (sl:acknowledge "No beams added; no beams transferred.")
	      (return-from send-beams))

	    ;; P-BM-INFO is a list, in forward order, each entry:
	    ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <PrismBmObj> )
	    ;; with one entry for each segment.  Note that the list
	    ;; contains all Prism beams - that is, all segments for
	    ;; all Dicom beams.  They are grouped into Dicom beams
	    ;; in order - all segments for one Dicom beam followed by
	    ;; all segs for the next, and so forth.
	    ;;
	    ;; CopyBmInst and CurrBmInst are both copied beams so that changes
	    ;; to their collimators will not side-effect real Prism beams.
	    ;;
	    (setq p-bm-info (generate-pbeam-info o-alist))

	    (unless (check-beam-constraints p-bm-info d-dmp-list)
	      (sl:acknowledge "Cancelled; no beams transferred.")
	      (return-from send-beams))

	    (unless (sl:confirm
		      '("Ready to transfer beams, but first you must enter"
			"a nonblank Patient ID in the next dialog box."
			""
			"The transfer may take a few minutes."
			"A chart dialog box will be displayed when finished."
			"During transfer, please wait for chart dialog box."
			""
			"Ok to continue?"))
	      (sl:acknowledge "Transmission aborted; no beams transferred.")
	      (return-from send-beams))

	    (setq dicom-pat-id (sl:popup-textline
				 "" 600
				 :label
				 (format nil "Enter ID for ~A ~A: "
					 (hospital-id cur-pat)
					 (name cur-pat))
				 :title "Patient ID"))
	    (unless (and (typep dicom-pat-id 'simple-base-string)
			 (> (length (the simple-base-string dicom-pat-id)) 0))
	      (sl:acknowledge "No patient ID; no beams transferred.")
	      (return-from send-beams))

	    (sl:push-event-level)   ; Long wait coming up - ignore user input.

	    (multiple-value-bind (status msg)
		(send-dicom (assemble-dicom cur-pat p-bm-info
					    dicom-pat-id d-dmp-list)
			    d-dmp-list)
	      (declare (type fixnum status)
		       (type simple-base-string msg))
	      (cond
		((or (= status 0)                   ; success
		     (= status #xB000)              ; codes defined by Elekta
		     (= status #xB006)
		     (= status #xB007))
		 (chart-panel 'dicom cur-pat nil
			      p-bm-info
			      (date-time-string)
			      "DICOM transfer"
			      dicom-pat-id))
		(t (sl:acknowledge
		     (cond
		       ((< status 0)
			;; might look nicer if centered
			(list "DICOM transfer failed." msg))
		       (t (list "DICOM transfer failed."
				(format nil "~A (#x~4,'0X)"
					(or (cdr (assoc status *status-alist*
							:test #'=))
					    "Unknown error")
					status))))))))

	    (sl:pop-event-level))                   ; wait is over
	  (setf (sl:on send-beams-bn) nil)))

    ;; Action function for X1 diaphragm textline.
    ;; Elekta X1,X2 Y1,Y2 are Prism/Dicom y2,y1 x2,x2 respectively
    (ev:add-notify dp (sl:new-info x1-tl)           ; Elekta X1 is Prism Y2
      #'(lambda (dp a info)
	  (declare (ignore a))
	  (cond ((current-prism-bi dp)
		 ;; Need FLOAT or COERCE here, or compiled code crashes.
		 ;; Had ROUND-DIGITS, but should no longer be necessary.
		 ;; Side-effect here on collimator of CURRENT-PRISM-BI while
		 ;; preserving collimator of original beam in real Prism plan.
		 (let ((data (coerce (read-from-string info) 'single-float)))
		   (setf (y2 (collimator (current-prism-bi dp))) data)
		   (setf (sl:info x1-tl) (format nil "~7,2F" data))))
		(t (sl:acknowledge no-bm-sel-msg)
		   (setf (sl:info x1-tl) "")))))

    ;; Action function for X2 diaphragm textline.
    (ev:add-notify dp (sl:new-info x2-tl)           ; Elekta X2 is Prism -Y1
      #'(lambda (dp a info)
	  (declare (ignore a))
	  (cond ((current-prism-bi dp)
		 ;; Need FLOAT or COERCE here, or compiled code crashes.
		 ;; Had ROUND-DIGITS, but should no longer be necessary.
		 ;; Side-effect here on collimator of CURRENT-PRISM-BI while
		 ;; preserving collimator of original beam in real Prism plan.
		 (let ((data (coerce (read-from-string info) 'single-float)))
		   (setf (y1 (collimator (current-prism-bi dp))) (- data))
		   (setf (sl:info x2-tl) (format nil "~7,2F" data))))
		(t (sl:acknowledge no-bm-sel-msg)
		   (setf (sl:info x2-tl) "")))))

    ;; Action function for Y1 diaphragm textline.
    (ev:add-notify dp (sl:new-info y1-tl)           ; Elekta Y1 is Prism X2
      #'(lambda (dp a info)
	  (declare (ignore a))
	  (cond ((current-prism-bi dp)
		 ;; Need FLOAT or COERCE here, or compiled code crashes.
		 ;; Had ROUND-DIGITS, but should no longer be necessary.
		 ;; Side-effect here on collimator of CURRENT-PRISM-BI while
		 ;; preserving collimator of original beam in real Prism plan.
		 (let ((data (coerce (read-from-string info) 'single-float)))
		   (setf (x2 (collimator (current-prism-bi dp))) data)
		   (setf (sl:info y1-tl) (format nil "~7,2F" data))))
		(t (sl:acknowledge no-bm-sel-msg)
		   (setf (sl:info y1-tl) "")))))

    ;; Action function for Y2 diaphragm textline.
    (ev:add-notify dp (sl:new-info y2-tl)           ; Elekta Y2 is Prism -X1
      #'(lambda (dp a info)
	  (declare (ignore a))
	  (cond ((current-prism-bi dp)
		 ;; Need FLOAT or COERCE here, or compiled code crashes.
		 ;; Had ROUND-DIGITS, but should no longer be necessary.
		 ;; Side-effect here on collimator of CURRENT-PRISM-BI while
		 ;; preserving collimator of original beam in real Prism plan.
		 (let ((data (coerce (read-from-string info) 'single-float)))
		   (setf (x1 (collimator (current-prism-bi dp))) (- data))
		   (setf (sl:info y2-tl) (format nil "~7,2F" data))))
		(t (sl:acknowledge no-bm-sel-msg)
		   (setf (sl:info y2-tl) "")))))

    ;; Action function for Leaf textlines.
    (do ((left-tls (left-leaf-textlines dp) (cdr left-tls))
	 (right-tls (right-leaf-textlines dp) (cdr right-tls)))
	((null left-tls))

      ;; Action function for Left-Leaf textline.
      (ev:add-notify dp (sl:new-info (car left-tls))
	#'(lambda (dp tln info)
	    (cond
	      ((current-prism-bi dp)
	       ;; Need FLOAT or COERCE here, or compiled code crashes.
	       ;; Had ROUND-DIGITS, but should no longer be necessary.
	       ;; Side-effect here on collimator of CURRENT-PRISM-BI while
	       ;; preserving collimator of original beam in Prism plan.
	       (let ((pos (position tln (left-leaf-textlines dp) :test #'eq))
		     (ls (leaf-settings (collimator (current-prism-bi dp))))
		     (data (coerce (read-from-string info) 'single-float)))
		 (setf (sl:info tln) (format nil "~6,2F" data))
		 (setf (first (nth pos ls)) (- data)))) ; Elekta Y2, - sign
	      (t (sl:acknowledge no-bm-sel-msg)
		 (setf (sl:info tln) "")))))

      ;; Action function for Right-Leaf textline.
      (ev:add-notify dp (sl:new-info (car right-tls))
	#'(lambda (dp tln info)
	    (cond
	      ((current-prism-bi dp)
	       ;; Need FLOAT or COERCE here, or compiled code crashes.
	       ;; Had ROUND-DIGITS, but should no longer be necessary.
	       ;; Side-effect here on collimator of CURRENT-PRISM-BI while
	       ;; preserving collimator of original beam in Prism plan.
	       (let ((pos (position tln (right-leaf-textlines dp) :test #'eq))
		     (ls (leaf-settings (collimator (current-prism-bi dp))))
		     (data (coerce (read-from-string info) 'single-float)))
		 (setf (sl:info tln) (format nil "~6,2F" data))
		 (setf (second (nth pos ls)) data)))    ; Elekta Y1, same sign
	      (t (sl:acknowledge no-bm-sel-msg)
		 (setf (sl:info tln) ""))))))

    ;; Preview-Chart button pressed.
    (ev:add-notify dp (sl:button-on preview-chart-bn)
      #'(lambda (dp a)
	  (declare (ignore a))
	  ;; Note in preview here, we do NOT use DICOM-PAT-ID as Patient ID
	  (let ((o-alist (output-alist dp)))
	    (cond ((consp o-alist)
		   (chart-panel 'dicom cur-pat nil
				(generate-pbeam-info o-alist)
				(date-time-string)
				"Chart preview"
				(hospital-id cur-pat)))
		  (t (sl:acknowledge no-bms/segs-msg))))
	  (setf (sl:on preview-chart-bn) nil)))

    ;; Delete-Panel button pressed.
    (ev:add-notify dp (sl:button-on del-panel-bn)
      #'(lambda (dp a)
	  (declare (ignore a))
	  (destroy dp)))))

;;;-------------------------------------------------------------

(defmethod (setf current-plan) :after (new-plan (dp dicom-panel))

  (let ((p-bm-sl (prism-beam-scrollinglist dp)))
    (cond (new-plan
	    ;; Fill up Prism-beams scrolling list and alist w/ new info --
	    ;; only beams w/collimators of type multileaf-coll are considered.
	    ;; ORIG-PBI is an [uncopied] Original-Prism-Beam instance.
	    (dolist (orig-pbi (coll:elements (beams new-plan)))
	      (let ((b-bn (sl:make-list-button p-bm-sl (name orig-pbi))))
		(sl:insert-button b-bn p-bm-sl)
		(push (cons b-bn orig-pbi) (prism-beam-alist dp))))
	    ;; fill in plan readout
	    (setf (sl:info (plan-readout dp))
		  (format nil "~A    ~A"
			  (name new-plan) (time-stamp new-plan)))
	    ;; fill in plan-specific info on panel
	    (setf (sl:info (comments-box dp)) (comments new-plan)))

	  ;; Clean out Prism-beams scrolling list and alist.
	  (t (dolist (b-bn (sl:buttons p-bm-sl))
	       (sl:delete-button b-bn p-bm-sl))
	     (setf (prism-beam-alist dp) nil)      ;; clear plan info on panel
	     (setf (sl:info (plan-readout dp)) "")
	     (setf (sl:info (comments-box dp)) '(""))))))

;;;-------------------------------------------------------------

(defun prism-beam-problems (orig-pbi dp)

  "prism-beam-problems orig-pbi dp

Returns a list of strings describing problems with Prism beam
instance ORIG-PBI on panel DP, or NIL if there are none."

  (let* ((mach (machine orig-pbi))
	 (wedge-id (id (wedge orig-pbi)))
	 (mach-name (name mach))
	 (mach-ident (ident mach))
	 ;; BEAMS-TO-GO is list of CurrBmInst objects.
	 (beams-to-go (mapcar #'fourth (output-alist dp)))
	 (problem-list '()))

    (when (and (> wedge-id 0)
	       (string/= (wedge-label wedge-id mach) "Fixed Wedge")
	       (coll:elements (blocks orig-pbi)))
      (push "External wedge and external blocks not possible in same beam."
	    problem-list))

    (cond ((and (consp mach-ident)
		(= (length mach-ident) 5))
	   (let ((machine-id (first mach-ident)))
	     (when (consp beams-to-go)
	       (let ((send-machine-id
		       (first (ident (machine (car beams-to-go))))))
		 (unless (string= machine-id send-machine-id)
		   (push (format nil "~A ID ~A differs from ~A in output list."
				 mach-name machine-id send-machine-id)
			 problem-list))))))
	  (t (push (format nil "No Dicom server defined for ~A." mach-name)
		   problem-list)))

    problem-list))

;;;-------------------------------------------------------------

(defun add-prism-beam-fcn (dp new-beam? &aux segl vl wl (e1 0.0) (e2 0.0)
			   (o-alist (output-alist dp))
			   ;; Original [uncopied] Prism beam:
			   (orig-pbi (original-prism-bi dp))
			   ;; Current copied/non-mutated BmInst:
			   (copy-pbi (copied-prism-bi dp))
			   ;; Current copied/mutated BmInst:
			   (curr-pbi (current-prism-bi dp))
			   last-seg-info last-pbi curr-mach)

  "add-prism-beam-fcn dp new-beam?

Adds triple of OrigBmInst, CopyBmInst, and CurrBmInst [and some other data]
to (output-alist dp).  If NEW-BEAM? is T, it is an independent beam or first
in a multi-segment Dicom beam.  If NEW-BEAM? is NIL, handle it as a successor
segment in multisegment beam."

  ;; OUTPUT-ALIST is still in reverse order [last beam added is at front].

  (declare (type (member nil t) new-beam?)
	   (type list o-alist)
	   (type single-float e1 e2))

  (cond
    ((null (current-plan dp))
     (sl:acknowledge "No plan selected yet."))

    ;; Before invoking this function, user must have selected a beam
    ;; [via pressing the "Select-Prism-Beam" button], which checks
    ;; that the selected beam comes from the same physical machine
    ;; as did any previous segments [different energy is OK].
    ;; That user has already selected a beam is checked here.
    ((or (null orig-pbi)
	 (null copy-pbi)
	 (null curr-pbi))
     (sl:acknowledge "No beam selected yet."))

    ;; Attempt to add a segment on first beam addition.
    ((and (null o-alist)
	  (not new-beam?))
     (sl:acknowledge "You must add a beam before you add a segment."))

    ((progn
       ;; CURR-PBI must be non-NIL [checked above].
       (setq curr-mach (machine curr-pbi))          ;Machine of current BmInst
       (when (consp o-alist)
	 (setq last-seg-info (cdr (first o-alist)) ;Previously-added beam info
	       last-pbi (third last-seg-info)))   ;Prev current/mutated BmInst
       ;; Return NIL so COND clause processing continues.
       nil))

    ;; LAST-SEG-INFO [if O-ALIST is non-empty] is:
    ;; ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <New-Bm?> <SegColor> )
    ;; <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
    ;;
    ;; CopyBmInst and CurrBmInst are both copied beams so that changes
    ;; to their collimators will not side-effect real Prism beams.

    ;; Check segment matches preceding segments.
    ((setq segl (and (not new-beam?)
		     (consp o-alist)
		     (segment-violations last-pbi curr-pbi)))
     (sl:acknowledge
       (cons (format nil
		     "~S does not match preceding segment; cannot add."
		     (name curr-pbi))
	     segl)))

    ;; Check for Wedge IN/OUT segment pair with altered leaf settings.
    ((and (not new-beam?)               ;Forgotten condition added Nov 1 2004.
	  (= (length o-alist) 1)           ;2 segs [including one being added]
	  ;; O-ALIST non-empty -> LAST-PBI is a legitimate beam.
	  (let ((wedge-name1 (wedge-label (id (wedge last-pbi)) curr-mach))
		(wedge-name2 (wedge-label (id (wedge curr-pbi)) curr-mach)))
	    (declare (type simple-base-string wedge-name1 wedge-name2))
	    (and (or (and (string= wedge-name1 "Fixed Wedge")
			  (string/= wedge-name2 "Fixed Wedge"))
		     (and (string/= wedge-name1 "Fixed Wedge")
			  (string= wedge-name2 "Fixed Wedge")))
		 (not (equal (leaf-settings (collimator last-pbi))
			     (leaf-settings (collimator curr-pbi))))
		 (not (sl:confirm
			'("Leaf-settings changed on"
			  "Wedge IN/OUT segment pair."
			  ""
			  "OK to proceed?")))))))

    ;; Check energy constraints [this is a warning for successor
    ;; segments, not an enforceable constraint].  If new beam, or
    ;; if segment but energies are equal, or if seg and energies differ
    ;; but user confirms positively, continue.  Otherwise trap.
    ;;
    ;; NB: If NEW-BEAM? is NIL, then O-ALIST must be non-empty [checked
    ;; above] and therefore LAST-PBI must be a legitimate beam.
    ((not (or new-beam?
	      (= (setq e1 (energy (machine last-pbi)))
		 (setq e2 (energy curr-mach)))
	      (sl:confirm
		(list "Energy difference between segments:"
		      (format nil "~F <--> ~F" e1 e2)
		      ""
		      "Add this segment anyway?")))))

    ;; Check collim-constraint-violations.
    ((setq vl (collim-constraint-violations (collimator curr-pbi)))
     (let ((vll (last vl 25)))                      ; vl might be very long!
       (sl:acknowledge
	 (cons (format nil "Constraint violation in ~S; cannot send."
		       (name curr-pbi))
	       vll))))

    (t (setq wl (collim-warnings (collimator copy-pbi)
				 (collimator curr-pbi)))
       (when (if wl
		 ;; Check COLLIM-WARNINGS, optionally exit.
		 (sl:confirm
		   (append
		     (list (format nil "Warnings for ~A"
				   (name curr-pbi)))
		     (if (<= (length wl) 25)
			 wl
			 (subseq wl 0 25))
		     '("" "Add beam to output list anyway?")))
		 t)
	 ;; OK - add the Prism beam instances to the output list.
	 (let ((a-bn (sl:make-list-button
		       (output-scrollinglist dp)
		       (format nil "~A - ~A"
			       (name curr-pbi)
			       (if new-beam?
				   (name (current-plan dp))
				   "SEGMENT"))
		       :button-type :momentary))
	       (seg-color (if (consp last-seg-info)
			      (sixth last-seg-info)
			      0)))
	   (declare (type fixnum seg-color))
	   (when new-beam?
	     ;; Change color on each new Dicom beam.
	     (setq seg-color
		   (mod (the fixnum (1+ seg-color))
			(length (the (simple-array t 1) color-seq)))))
	   (setf (sl:bg-color a-bn)
		 (svref (the (simple-array t 1) color-seq) seg-color)
		 (sl:fg-color a-bn) 'sl:black)
	   (sl:insert-button a-bn (output-scrollinglist dp))
	   ;; We use this list much like a struct, but we use
	   ;; LIST not STRUCT, so COPY-TREE works right; we must
	   ;; avoid sharing structure.  First element A-BN is key, and
	   ;; rest of list is the datalist portion of the assoc list.
	   (push (list a-bn
		       orig-pbi                     ;Original Prism beam
		       copy-pbi                     ;Copied original beam
		       curr-pbi                     ;Copied/mutated beam
		       (current-plan dp)
		       new-beam?
		       seg-color)
		 (output-alist dp)))))))

;;;-------------------------------------------------------------

(defun segment-violations (last-seg-pbi curr-pbi)

  "segment-violations last-seg-pbi curr-pbi

Returns a list of strings describing why CURR-PBI cannot be a segment
in the multisegment Dicom beam whose last segment was LAST-SEG-PBI,
or NIL if there are none [everything is OK]."

  ;; All beam instances here [LAST-SEG-PBI and CURR-PBI] are copies made
  ;; from original beam in Prism plan.

  (let ((gan1 (gantry-angle last-seg-pbi))
	(gan2 (gantry-angle curr-pbi))
	(coll1 (collimator-angle last-seg-pbi))
	(coll2 (collimator-angle curr-pbi))
	(nfrac1 (n-treatments last-seg-pbi))
	(nfrac2 (n-treatments curr-pbi))
	(turnt1 (couch-angle last-seg-pbi))
	(turnt2 (couch-angle curr-pbi))
	(lat1 (couch-lateral last-seg-pbi))
	(lat2 (couch-lateral curr-pbi))
	(long1 (couch-longitudinal last-seg-pbi))
	(long2 (couch-longitudinal curr-pbi))
	(hght1 (couch-height last-seg-pbi))
	(hght2 (couch-height curr-pbi))
	;; If external wedge, must be the same.
	;; MLC fields can't have blocks.
	(problem-list nil))

    (declare (type single-float gan1 gan2 coll1 coll2 turnt1 turnt2
		   lat1 lat2 long1 long2 hght1 hght2)
	     (type fixnum nfrac1 nfrac2))

    (unless (poly:nearly-equal hght1 hght2)
      (push (format nil "Couch height ~5,1F differs from ~5,1F" hght2 hght1)
	    problem-list))
    (unless (poly:nearly-equal long1 long2)
      (push (format nil "Couch longitudinal position ~5,1F differs from ~5,1F"
		    long2 long1)
	    problem-list))
    (unless (poly:nearly-equal lat1 lat2)
      (push (format nil "Couch lateral position ~5,1F differs from ~5,1F"
		    lat2 lat1)
	    problem-list))
    (unless (poly:nearly-equal turnt1 turnt2)
      (push (format nil "Couch angle ~5,1F differs from ~5,1F" turnt2 turnt1)
	    problem-list))
    (unless (poly:nearly-equal coll1 coll2)
      (push (format nil "Collimator angle ~5,1F differs from ~5,1F"
		    coll2 coll1)
	    problem-list))
    (unless (poly:nearly-equal gan1 gan2)
      (push (format nil "Gantry angle ~5,1F differs from ~5,1F" gan2 gan1)
	    problem-list))
    (when (< 0.1 (arc-size last-seg-pbi))
      (push "Last segment was an arc field" problem-list))
    (when (< 0.1 (arc-size curr-pbi))
      (push "Current Prism beam is an arc field" problem-list))
    (unless (= nfrac1 nfrac2)
      (push (format nil "Number of fractions ~D differs from ~D" nfrac2 nfrac1)
	    problem-list))

    problem-list))

;;;-------------------------------------------------------------

(defun del-prism-beam-fcn (dp btn &aux (o-alist (output-alist dp)))

  "del-prism-beam-fcn dp btn

Delete the Prism beam instances indicated by BTN from output list on panel DP.
If deleted beam is initial segment in a sequence, mark next segment initial."

  ;; PAIR is an item of this form:
  ;;  ( <Button> <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan>
  ;;    <New-Bm?> <SegColor> )
  ;;  <New-Bm? T:Static/:Dynamic[1st-of-seq], NIL:subsequent-of-seq>
  ;;
  ;; (OUTPUT-ALIST dp) and the variable O-ALIST is a list [in reverse order]
  ;; of objects, each of form shown above.
  ;;
  (cond ((consp o-alist)
	 (let* ((pair (assoc btn o-alist :test #'eq))
		(pair-pos (position pair o-alist :test #'eq)))
	   ;; Prev will be next - O-ALIST is in reverse order.
	   (when (> pair-pos 0)
	     (let* ((prev-pos (1- pair-pos))
		    (prev-pair (nth prev-pos o-alist)))
	       ;; If deleting first in sequence, mark next as initial.
	       (when (and (sixth pair)
			  (not (sixth prev-pair)))
		 (setf (sixth prev-pair) t))))
	   (setf (output-alist dp) (delete pair o-alist :test #'eq))))
	(t (sl:acknowledge "No beams or segments added yet."))))

;;;-------------------------------------------------------------

(defun setup-prism-bi-triple (new-pbi dp)

  ;; NEW-PBI is a Current-Prism-Beam instance [ie, copy of OrigBmInst].
  ;; Places copies of collimator of OrigBmInst in COPIED-PRISM-BI [converted
  ;; to MLC but not later modified] and in CURRENT-PRISM-BI [converted to MLC,
  ;; modified by MAKE-FLAGPOLE and MAKE-ADJUSTED-ENDS, and possibly later
  ;; modified by user].
  ;;
  ;; This copying provides a MLC for comparison between COPIED-PRISM-BI's and
  ;; CURRENT-PRISM-BI's collimators, to check for user modifications, while
  ;; protecting original Prism plan's collimator from side-effects.

  (let ((mach (machine new-pbi)))

    (let ((collim (make-multileaf-coll
		    (collimator-angle new-pbi)
		    (typecase (collimator new-pbi)
		      (multileaf-coll (get-mlc-vertices new-pbi))
		      ;; VJC, rotate by collimator-angle
		      ;; to compensate for rotation in MAKE-MULTILEAF-COLL.
		      ;; See GET-MLC-VERTICES in "mlc.cl".
		      ;; What's distinction between portal and vertices?
		      (variable-jaw-coll
			(poly:rotate-vertices
			  (portal (collimator new-pbi))
			  (collimator-angle new-pbi)))
		      (otherwise nil))
		    (typecase (collimator new-pbi)
		      (multileaf-coll (collim-info dp))
		      (otherwise *sl-collim-info*)))))

      (setf (collimator (copied-prism-bi dp)) collim)
      (setf (collimator new-pbi) (make-adjusted-ends (make-flagpole collim))))

    (setf (sl:info (prism-beam-readout dp)) (name new-pbi))

    (setf (sl:info (gantry-start-readout dp))
	  (format nil "~6,1F" (first (scale-angle
				       (gantry-angle new-pbi)
				       (gantry-scale mach)
				       (gantry-offset mach)))))
    (setf (sl:info (gantry-stop-readout dp))
	  (format nil "~6,1F"
		  (mod (+ (gantry-angle new-pbi)
			  (arc-size new-pbi))
		       360.0)))

    (setf (sl:info (couch-angle-readout dp))
	  (format nil "~6,1F" (first (scale-angle
				       (couch-angle new-pbi)
				       (turntable-scale mach)
				       (turntable-offset mach)))))

    (let ((mu-tot (monitor-units new-pbi))
	  (num-frac (n-treatments new-pbi)))
      (declare (type single-float mu-tot)
	       (type fixnum num-frac))
      (setf (sl:info (n-treat-readout dp)) num-frac)
      (setf (sl:info (tot-mu-readout dp)) (format nil "~6,1F" mu-tot))
      (setf (sl:info (mu-treat-readout dp))
	    (format nil "~6,1F"                     ;Division by zero check.
		    (cond ((= num-frac 0) 0.0)
			  (t (/ mu-tot num-frac))))))

    (setf (sl:info (coll-angle-readout dp))
	  (format nil "~6,1F" (first (scale-angle
				       (collimator-angle new-pbi)
				       (collimator-scale mach)
				       (collimator-offset mach)))))

    (setf (sl:info (wedge-sel-readout dp))
	  (wedge-label (id (wedge new-pbi)) mach))
    (setf (sl:info (wedge-rot-readout dp))
	  (cond ((= (id (wedge new-pbi)) 0)
		 "NONE")
		(t (first (scale-angle
			    (rotation (wedge new-pbi))
			    (wedge-rot-scale mach)
			    (wedge-rot-offset mach))))))

    ;; set the leaf textline values
    (do* ((left-tls (left-leaf-textlines dp) (cdr left-tls))
	  (right-tls (right-leaf-textlines dp) (cdr right-tls))
	  (leaves (leaf-settings (collimator new-pbi)) (cdr leaves))
	  (leaf-pair (first leaves) (first leaves)))
	 ((null leaves))
      (setf (sl:info (car left-tls))
	    ;; Elekta Y2 leaves in -x plane are shown as positive positions
	    (format nil "~6,2F" (- (first leaf-pair))))
      (setf (sl:info (car right-tls))
	    (format nil "~6,2F" (second leaf-pair))))

    ;; Elekta X1,X2, Y1,Y2 are Prism/Dicom y2,-y1, x2,-x1 respectively
    (setf (sl:info (x1-textline dp))
	  (format nil "~7,2F" (y2 (collimator new-pbi))))
    (setf (sl:info (x2-textline dp))
	  (format nil "~7,2F" (- (y1 (collimator new-pbi)))))
    (setf (sl:info (y1-textline dp))
	  (format nil "~7,2F" (x2 (collimator new-pbi))))
    (setf (sl:info (y2-textline dp))
	  (format nil "~7,2F" (- (x1 (collimator new-pbi)))))

    (setf (sl:info (machine-readout dp)) (name mach))
    (setf (sl:info (id-readout dp)) (car (ident mach)))))

;;;-------------------------------------------------------------

(defun clear-prism-bi-triple (dp)

  (setf (sl:info (prism-beam-readout dp)) "")
  (setf (sl:info (gantry-start-readout dp)) "")
  (setf (sl:info (gantry-stop-readout dp)) "")
  (setf (sl:info (couch-angle-readout dp)) "")
  (setf (sl:info (n-treat-readout dp)) "")
  (setf (sl:info (tot-mu-readout dp)) "")
  (setf (sl:info (mu-treat-readout dp)) "")
  (setf (sl:info (coll-angle-readout dp)) "")
  (setf (sl:info (wedge-sel-readout dp)) "")
  (setf (sl:info (wedge-rot-readout dp)) "")

  (mapc #'(lambda (l-rd r-rd)
	    (setf (sl:info l-rd) "")
	    (setf (sl:info r-rd) ""))
    (left-leaf-textlines dp)
    (right-leaf-textlines dp))

  (setf (sl:info (x1-textline dp)) "")
  (setf (sl:info (x2-textline dp)) "")
  (setf (sl:info (y1-textline dp)) "")
  (setf (sl:info (y2-textline dp)) "")
  (setf (sl:info (id-readout dp)) "")
  (setf (sl:info (machine-readout dp)) ""))

;;;=============================================================

(defun make-dicom-panel (cur-pat &rest initargs)

  "make-dicom-panel cur-pat &rest initargs

Creates and returns a Dicom panel with the specified initargs."

  (cond ((> (the fixnum (patient-id cur-pat)) 0)
	 (apply #'make-instance 'dicom-panel
		:current-patient cur-pat
		:dicom-dmp-cnt 0
		initargs))
	(t (sl:acknowledge "Please select a patient first."))))

;;;-------------------------------------------------------------

(defmethod destroy ((dp dicom-panel))

  "Unmap the panel's frame."

  (setf (plan-alist dp) nil)
  (setf (prism-beam-alist dp) nil)
  (setf (output-alist dp) nil)
  (setf (current-patient dp) nil)
  (setf (current-plan dp) nil)
  (let ((tmp (current-prism-bi dp)))
    (when (and tmp (setq tmp (wedge tmp)))
      (ev:remove-notify dp (new-id tmp))
      (ev:remove-notify dp (new-rotation tmp))))
  (setf (original-prism-bi dp) nil)
  (setf (copied-prism-bi dp) nil)
  (setf (current-prism-bi dp) nil)
  (clear-prism-bi-triple dp)
  (setf (collim-info dp) nil)
  (sl:destroy (del-panel-button dp))
  (sl:destroy (add-prism-beam-button dp))
  (sl:destroy (send-beams-button dp))
  (sl:destroy (preview-chart-button dp))
  (sl:destroy (add-seg-button dp))
  (sl:destroy (dmp-panel-button dp))
  (sl:destroy (comments-box dp))
  (sl:destroy (comments-label dp))
  (sl:destroy (prism-beam-readout dp))
  (sl:destroy (plan-readout dp))
  (sl:destroy (prism-beam-label dp))
  (sl:destroy (plan-label dp))
  (sl:destroy (output-label dp))
  (sl:destroy (gantry-start-readout dp))
  (sl:destroy (gantry-stop-readout dp))
  (sl:destroy (n-treat-readout dp))
  (sl:destroy (tot-mu-readout dp))
  (sl:destroy (mu-treat-readout dp))
  (sl:destroy (coll-angle-readout dp))
  (sl:destroy (couch-angle-readout dp))
  (sl:destroy (wedge-sel-readout dp))
  (sl:destroy (wedge-rot-readout dp))
  (sl:destroy (x1-textline dp))
  (sl:destroy (x2-textline dp))
  (sl:destroy (y1-textline dp))
  (sl:destroy (y2-textline dp))
  (mapc #'sl:destroy (left-leaf-textlines dp))
  (mapc #'sl:destroy (right-leaf-textlines dp))
  (setf (left-leaf-textlines dp) nil)
  (setf (right-leaf-textlines dp) nil)
  ;; Remove event notifications before destroying scrolling lists.
  (dolist (sl (list (plan-scrollinglist dp)
		    (prism-beam-scrollinglist dp)
		    (output-scrollinglist dp)))
    (setf (sl:selected sl) nil)
    (setf (sl:deselected sl) nil)
    (setf (sl:inserted sl) nil)
    (setf (sl:deleted sl) nil)
    (sl:destroy sl))
  (sl:destroy (machine-readout dp))
  (sl:destroy (id-readout dp))
  (sl:destroy (frame dp)))

;;;=============================================================
;;; End.
