;;;
;;; prism-globals
;;;
;;; this file contains all the defvar and defparameter forms to define
;;; the global prism parameters.  This does not include some stuff that is
;;; specific to a particular panel or function.
;;;
;;; 13-May-1994 I. Kalet created from prism-system.
;;; 22-May-1994 I. Kalet add easel constants.
;;; 26-May-1994 J. Unger add line to hardcopy header.
;;; 27-May-1994 J. Unger change nil to none *immob-devices* list.
;;;  2-Jun-1994 I. Kalet modify and expand constants.
;;;  6-Jun-1994 I. Kalet add digitizer device list.
;;;  8-Jun-1994 J. Unger elim *save-plan-dose* mechanism for saving dose info.
;;;  7-Jul-1994 J. Unger add *config-directory* defvar form.
;;; 18-Sep-1994 J. Unger add neutron & mlc defvar forms, minor other mods.
;;; 03-Oct-1994 J. Unger change version string to October, 1994.
;;; 26-Jan-1995 I. Kalet change version string to January, 1995.  Move
;;; *therapy-machines* to therapy-machines module.  Not global.
;;; 12-Mar-1995 I. Kalet add digitizer input string processing and
;;; calibration parameters.
;;;  1-Aug-1995 I. Kalet change version string to Version 1.1 - July
;;;  1995.
;;; 26-Sep-1995 I. Kalet add "File only" to printer-dests, change
;;; defparameter to defvar.
;;; 29-Jan-1997 I. Kalet add *pi-over-180* in wake of elimination of
;;; geometry package, also *pi-over-2* is handy.  Eliminate dosecomp
;;; globals, dose comp is now integrated in the lisp code.
;;; 30-Apr-1997 I. Kalet add *ruler-color* to user configurables.
;;;  3-May-1997 I. Kalet change version string to Version 1.2X
;;; 18-Jun-1997 I. Kalet delete selector panel sizes, as they vary.
;;; 22-Aug-1997 I. Kalet change default directories for new UW Radonc
;;; cluster, add *irreg-database*
;;; 17-Sep-1997 I. Kalet add *machine-index-directory*, change
;;; defaults so beamdata directory is at higher level, since there
;;; does not need to be a separate research or test beamdata directory
;;; in the new system.
;;;  2-May-1998 I. Kalet drop *mlc-chart-file* and
;;; *neutron-chart-file* since they are not used anymore.
;;; 19-May-1998 I. Kalet Provide for multiple plotter destinations and
;;; types.  Delete plotter text color, it is set by plotter type.
;;;  1-Jul-1998 I. Kalet Make the PostScript plotter the default.
;;; 30-Nov-1998 I. Kalet add support for HP Design Jet 455C plotter.
;;; 28-Jun-1999 J. Zeman add *postscript-printers*
;;;  8-Sep-1999 I. Kalet add *mlc-leaf-color*
;;; 25-Oct-1999 I. Kalet remove autoplan stuff.
;;;  5-Jan-2000 I. Kalet add *brachy-database* and *display-epsilon*
;;; 25-Apr-2000 BobGian add *irreg-printout* to control irreg QA printout.
;;; 26-Apr-2000 I. Kalet make default nil for *irreg-printout*
;;; 27-May-2000 I. Kalet parametrize small and medium fonts for
;;; panels, remove *max-chart-lines* and *printers* since they are no
;;; longer used.
;;; 21-Jun-2000 BobGian remove *irreg-printout* - no longer used.
;;; 27-Jun-2000 I. Kalet add *display-format* which specifies how the
;;; z coordinates in the filmstrip and easel are displayed.  Also drop
;;; *display-epsilon* down to 0.001 instead of 0.005.
;;; 29-Jun-2000 I. Kalet add *special-functions* parameter for tools panel
;;; 13-Aug-2000 I. Kalet move most digitizer globals to digitizer module.
;;; 27-Dec-2000 I. Kalet change order of postscript printer list,
;;; change version number to 1.4.
;;; 18-Mar-2001 I. Kalet add configurable parameters *fg-gray-level*,
;;; *bg-gray-level* and *border-style*, and make the defaults black on
;;; gray with raised borders.
;;; 28-Jan-2002 I. Kalet add dicom-panel to built in special tools list
;;; 30-Aug-2002 BobGian add *dicom-ae-titles*, mapping hostnames to AE titles.
;;; 23-Sep-2002 BobGian Move pr::*DICOM-AE-TITLES* to DICOM package and from
;;;   "prism-globals" in Prism dir to "dicom-client.system" in Dicom dir.
;;; 12-Jun-2003 BobGian regularize database variables (values are generic here,
;;;   set to site-specific directories in "prism.config").  Also add
;;;   *structure-database* to parameterize structure-set import tool.
;;;   Structure-set importer now on *special-tools* menu rather than being
;;;   added via ADD-TOOLS.
;;; 27-Aug-2003 I. Kalet update release no. - DICOM fixes, window
;;; close intercept in SLIK
;;;  1-Nov-2003 I. Kalet update release no. - enhancements to patdbmgr
;;; panel, to allow multiple selections, sort by Prism ID, include
;;; patient name in image study display, also fix dependencies and
;;; take out unnecessary #. reader macros
;;; 25-Mar-2004 BobGian update release no - DMP functionality added to Dicom.
;;; 14-Jun-2004 BobGian update release to V1.4-5
;;;  2-Jul-2004 I. Kalet add *shared-database* for shared checkpoint
;;; directory, and *other-databases* for list of other checkpoint
;;; directories, add couch lateral and longitudinal limits, remove irreg
;;; support, update release no. to 1.4-6
;;; 04-Nov-2004 BobGian move *DICOM-LOG-DIR* and *PDR-DATA-FILE* from
;;;   "dicom-client.system" -> here.
;;; 22-Feb-2005 A. Simms add #+cmu byte-order detection
;;; 27-Jun-2007 I. Kalet update release to 1.5-1
;;; 25-May-2009 I. Kalet add new global *prism-version* to use with
;;; *features* and read-time conditionals
;;; 24-Jun-2009 I. Kalet move defpackage here to make independent of
;;; defsystem.
;;;

;;;-------------------------------------

 ;; needed for :use below, defined more fully in inference.cl
(defpackage "INFERENCE")

(defpackage "PRISM"
  (:nicknames "PR")
  (:use "COMMON-LISP" "INFERENCE")
  (:export "ACQ-DATE" "ACQ-TIME" "ADD-PATIENT" "ADD-TOOL" "ANATOMY"
	   "ARC-SIZE" "ATTEN-FACTOR" "ATTRIBUTE-EDITOR" "AVERAGE-SIZE"
	   "BACKGROUND" "BACKGROUND-DISPLAYED" "BEAM"
	   "BEAM-BLOCK" "BEAMS" "BEAMS-EYE-VIEW"
	   "BIN-ARRAY-FILENAME" "BIN-ARRAY-PATHNAME"
	   "BLOCKS"
	   "CAL-DISTANCE" "CASE-ID" "CELL-TYPE" "CHARACTERS"
	   "CM-CONTOUR" "CNTS-COLL"
	   "COLLIMATOR" "COLLIMATOR-ANGLE" "COLLIMATOR-TYPE"
	   "COLOR"
	   "COMBINATION-COLL" "COMMENTS" "COMPUTE-DOSE-GRID"
	   "COMPUTE-DOSE-POINTS" "CONE-SIZE" "CONTOUR"
	   "CONTOUR-EDITOR" "CONTOURS" "COPY-BEAM"
	   "CORONAL-VIEW" "COUCH-LATERAL"
	   "COUCH-LONGITUDINAL" "COUCH-HEIGHT" "COUCH-ANGLE"
	   "DATE-ENTERED" "DATE-TIME-STRING" "DENSITY"
	   "DESCRIPTION" "DESTROY" "DIAMETER"
	   "DISPLAY-COLOR" "DISPLAY-VIEW" "DOSE-GRID"
	   "DOSE-RESULT" "DOSE-SURFACE"
	   "DOSE-SURFACES" "DRAW" "DUMP-PRISM-IMAGE"
	   "ELECTRON-COLL" "ENERGY" "ENLARGE-ARRAY-2"
	   "FILMSTRIP" "FIND-TRANSVERSE-IMAGE" "FINDINGS"
	   "FIXED" "FOREGROUND"
	   "GANTRY-ANGLE" "GENERATE-IMAGE-FROM-SET"
	   "GENERIC-PANEL" "GENERIC-PRISM-OBJECT"
	   "GET-ALL-OBJECTS" "GET-CASE-DATA" "GET-CASE-LIST"
	   "GET-IMAGE-SET" "GET-IMAGE-SET-LIST" "GET-NUMBER"
	   "GET-OBJECT" "GET-PATIENT-LIST" "GET-STRING"
	   "GET-THERAPY-MACHINE" "GET-THERAPY-MACHINE-LIST"
	   "GET-TRANSVERSE-IMAGE" "GETENV"
	   "GRADE" "GRID" "GRID-GEOMETRY"
	   "HISTORY" "HOSP-NAME" "HOSPITAL-ID" "HOW-DERIVED"
	   "ID" "IMAGE" "IMAGE-2D" "IMAGE-3D" "IMAGE-SET"
	   "IMAGE-SET-ID" "IMAGES"
	   "IMG-TYPE" "IMMOB-DEVICE" "INDEX" "INDICES"
	   "INTERACTIVE-MAKE-VIEW"
	   "LEAF-SETTINGS" "LINE-SOURCE" "LINE-SOURCES"
	   "LLC-ANAT" "LOCATOR"
	   "MACHINE" "MAKE-ATTRIBUTE-EDITOR" "MAKE-BEAM"
	   "MAKE-CHARACTERS-PRIM" "MAKE-CONTOUR-EDITOR"
	   "MAKE-CORONAL-IMAGE"
	   "MAKE-DOSE-RESULT-MANAGER" "MAKE-DOSE-SURFACE"
	   "MAKE-DOSE-VIEW-MEDIATOR"
	   "MAKE-FILMSTRIP" "MAKE-GRID-GEOMETRY"
	   "MAKE-IMAGE-VIEW-MANAGER" "MAKE-LINES-PRIM"
	   "MAKE-OBJECT-VIEW-MANAGER" "MAKE-ORGAN"
	   "MAKE-PLAN" "MAKE-POINT-DOSE-PANEL"
	   "MAKE-RECTANGLES-PRIM" "MAKE-SAGITTAL-IMAGE"
	   "MAKE-SEGMENTS-PRIM" "MAKE-SELECTOR-PANEL"
	   "MAKE-TARGET" "MAKE-TUMOR" "MAKE-VIEW"
	   "MAKE-VIEW-SET-MEDIATOR" "MAX-LENGTH"
	   "MONITOR-UNITS" "MULTILEAF-COLL"
	   "N-STAGE" "N-TREATMENTS" "NAME"
	   "OBJECT" "OBJECT-SET" "ORGAN" "ORGAN-NAME" "ORIGIN"
	   "PARTICLE" "PART-OF" "PATIENT" "PATIENT-ID" "PAT-POS"
	   "PENUMBRA" "PHYSICAL-VOLUME"
	   "PICTURE" "PIX-PER-CM" "PIXEL-CONTOUR" "PIXELS"
	   "PIXMAPS" "PLAN" "PLAN-BY" "PLAN-VIEWS"
	   "PLANS" "POINTS" "POLYLINE" "PORTAL"
	   "PRESCRIPTION-USED" "PRINT-TREE" "PRISM"
	   "PRISM-TOP-LEVEL" "PROJECT-PORTAL" "PSTRUCT"
	   "PULM-RISK" "PUT-ALL-OBJECTS"
	   "PUT-CASE-DATA" "PUT-IMAGE-SET" "PUT-OBJECT"
	   "PUT-PLAN-DATA"
	   "RANGE" "READ-BIN-ARRAY" "RECTANGLES"
	   "REFRESH-BG" "REFRESH-FG" "REFRESH-IMAGE"
	   "REGION" "REQUIRED-DOSE" "RESIZE-IMAGE"
	   "ROTATION"
	   "SAGITTAL-VIEW" "SCALE" "SCANNER-TYPE" "SEED"
	   "SEEDS" "SIDE" "SITE" "SIZE"
	   "SSD" "STATUS-CHANGED"
	   "SUM-DOSE" "SYMMETRIC-JAW-COLL"
	   "T-STAGE" "TAB-PRINT" "TARGET"
	   "TARGET-TYPE" "TARGETS" "THERAPY-MACHINE"
	   "THICKNESS" "THRESHOLD" "TIME-STAMP"
	   "TOLERANCE-DOSE" "TRANSMISSION"
	   "TRANSVERSE-VIEW" "TUMOR"
	   "UID" "UNITS" "UPDATE-VIEW" "URC-ANAT"
	   "VALID-GRID" "VALID-POINTS" "VARIABLE-JAW-COLL"
	   "VERTICES" "VIEW" "VIEW-POSITION" "VIEW-SET"
	   "VOXEL-SIZE" "VOXELS"
	   "WEDGE" "WITHIN" "WRITE-BIN-ARRAY"
	   "X" "X-DIM" "X-INF" "X-ORIENT" "X-ORIGIN"
	   "X-SIZE" "X-SUP"
	   "Y" "Y-DIM" "Y-INF" "Y-ORIENT" "Y-ORIGIN"
	   "Y-SIZE" "Y-SUP"
	   "Z" "Z-DIM" "Z-ORIGIN" "Z-SIZE"
	   ;; following needed for backward compatibility
	   ;; with old case data files
	   "PATIENT-OF" "PLAN-OF" "RESULT" "TABLE-POSITION"
	   ))

(in-package :prism)

;;;-------------------------------------
;;; some useful symbolic constants
;;;-------------------------------------

(defconstant *pi-over-180* (coerce (/ pi 180.0) 'single-float))
(defconstant *pi-over-2* (coerce (/ pi 2.0) 'single-float))

;;;-------------------------------------
;;; nonconfigurable global parameters
;;;-------------------------------------

(defvar *config-directory* "/radonc/prism/"
  "The directory of the prism.config file")

(defconstant *prism-version* :prism-version-1.5
  "A symbol indicating the current version of Prism")

(defconstant *prism-version-string* "V1.5-2"
  "A string indicating the current version of Prism")

(defconstant *byte-order*
  #+big-endian :big-endian #+little-endian :little-endian
  #+cmu (if (= (extensions:htons 42) 42) :big-endian :little-endian)
  "Used to decide whether to swap image bytes or not.")

(defconstant *mini-image-size* 128 "The size of the mini-images to be
used in the easel and other applications.")

(defconstant small 256 "pixels on a side for small image")
(defconstant medium 512 "pixels on a side for medium image")
(defconstant large 768 "pixels on a side for large image")

;;;-------------------------------------
;;; configurable globals - per user
;;;-------------------------------------

(defvar *patient-database* "/prismdata/cases/"
  "The location of the Prism archive patient case data files.")

(defvar *local-database* "~/prismlocal/"
  "The location of the Prism checkpointed patient case data files.")

(defvar *shared-database* "/prismdata/casetemp/"
  "The location of the Prism shared checkpointed patient case data files.")

(defvar *other-databases* nil
  "Additional Prism checkpoint locations, e.g. of other users, from
  which to retrieve patient case data files.")

(defvar *therapy-machine-database* "/prismdata/beamdata/"
  "The location of the Prism therapy machine descriptive and dose
computation database files")

(defvar *machine-index-directory* "/prismdata/beamdata/"
  "The location of the machine.index and machine.supp files.")

(defvar *brachy-database* "/prismdata/clinical/"
  "The location of the Prism brachytherapy source catalog file.")

(defvar *image-database* "/prismdata/images/"
  "The location of the Prism image data files.")

(defvar *structure-database* "/prismdata/structures/"
  "Directory containing structure sets.")

(defvar *chart-file* "~/chart.cht"
  "The pathname to the file containing the generated chart.")

(defvar *plotter-file* "~/plot.plt"
  "The pathname to the file of plotter commands which is generated and
spooled upon creation of a plot.")

(defvar *neutron-setup-file* "~/neutron.dat"
  "The pathname to the file containing the output neutron beam setup info.")

(defvar *fine-grid-size* 0.5
  "The dimensions, in centimeters, of each voxel of a finely spaced
dose grid.")

(defvar *medium-grid-size* 1.0
  "The dimensions, in centimeters, of each voxel of a medium spaced
dose grid.")

(defvar *coarse-grid-size* 2.0
  "The dimensions, in centimeters, of each voxel of a coarsely spaced
dose grid.")

(defvar *minimum-grid-size* 4.0
  "The minimum allowable value for the overall length, width, or
height of the dose grid, in centimeters.")

(defvar *easel-size* medium
  "The size (in pixels) of the easel's contour editor drawing
region.")

(defvar *ruler-color* 'sl:white
  "The default or initial color of a tape measure, e.g. in the contour
editor or point editor etc.")

(defvar *mlc-leaf-color* 'sl:gray
  "The color of the MLC leaves in the mlc or CNTS collimator
portal/leaf editing panel.")

(defvar *display-epsilon* 0.001
  "The distance within which two planar contours or a contour and an
  image are considered in the same plane")

(defvar *display-format* "~,3F"
  "The format string used for display of z values for contours, in the
  filmstrip, the easel, and possibly elsewhere.")

(defvar *fg-gray-level* 0.0
  "The foreground gray level for all the Prism control panels - user
  settable as different gray levels might work better for different people.")

(defvar *bg-gray-level* 0.75
  "The background gray level for all the Prism control panels - user
  settable as different gray levels might work better for different people.")

(defvar *border-style* :raised
  "The default border style, should be coordinated with the previous
  parameters in order to look OK.")

(defvar *small-font* 'sl:helvetica-medium-12
  "Used for smaller buttons, etc. e.g. on beam panel")

(defvar *medium-font* 'sl:helvetica-medium-14
  "Used for larger buttons, e.g. on patient panel")

(defvar *couch-lat-lower* -75.0 "Lower limit for couch lateral
motion, configurable to allow for extended SSD treatments")

(defvar *couch-lat-upper* 75.0 "Upper limit for couch lateral
motion, configurable to allow for extended SSD treatments")

(defvar *couch-long-lower* -75.0 "Lower limit for couch longitudinal
motion, configurable to allow for odd calibrations of some CT sim systems")

(defvar *couch-long-upper* 75.0 "Upper limit for couch longitudinal
motion, configurable to allow for odd calibrations of some CT sim systems")

;;;-------------------------------------
;;; configurable globals - per system
;;;-------------------------------------

(defvar *immob-devices* '(("No immob dev" none)
			  ("Mask" mask)
			  ("Alpha cradle" alpha-cradle)
			  ("Plaster shell" plaster-shell))
  "Table for popup menu in patient panel")

(defvar *digitizer-devices* '(("none" "/dev/digit"))
  "Association list of digitizer device filenames for various hosts")

(defvar *spooler-command* "lp -c -d"
  "The command string to spool a chart file or a file of plotter
commands.  The -c flag instructs the spooler to make a copy of the
file in the spooling directory.  The -d flag indicates that the name
of a destination printer or plotter is to follow.  This name is
appended to the end of this command string, which is then executed.")

(defvar *plotters* '(("ps184" ps-plot)
		     ("PS File only" ps-plot)
		     ("hp7550a" hp7550a-plot)
		     ("dj455c" hp455c-plot)
		     ("HP File only" hp7550a-plot))
  "The available plotter queue names as known by the system's print
spooler, and plot types for each.")

(defvar *postscript-printers* '("ps146b" "ps184" "ps136" "File Only"))

(defvar *hardcopy-header* '("Radiation Oncology Department"
			    "University of Washington Medical Center")
  "The text that appears at the top of every chart and plot.")

(defvar *special-tools*
  '(("DICOM Transfer" make-dicom-panel)
    ("Neutron Transfer" make-neutron-panel)
    ("Import Structures" make-import-structure-set-panel))
  "Menu text and corresponding function names for the tools panel")

;;;-------------------------------------
;;; DICOM Parameters -- Configurable via "/radonc/prism/prism.config".

(defvar *dicom-log-dir* "/prismdata/pdr-logs/")     ;Transaction record.

;;; Debugging dump written in :Create/:Supersede mode so only most recent
;;; is preserved.  Note that this file gets written into the home directory
;;; of the Prism user.  This is OK since Prism client always runs as a user
;;; process - never as root.
(defvar *pdr-data-file* "~/pdr.dat")

;;;-------------------------------------
;;; End.
