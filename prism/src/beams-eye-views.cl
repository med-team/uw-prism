;;;
;;; beams-eye-views
;;;
;;; This is the implementation of Prism beam's eye views.
;;;
;;; 18-Jan-1993 I. Kalet from views module, this code written by J.
;;; Unger.  Also move add-notify code that updates the beams-eye-view
;;; slots to the beam-view-mediator in beam-graphics.
;;; 15-Feb-1993 I. Kalet add sad slot to cache the sad from the
;;; machine database.
;;; 25-Mar-1993 J. Unger move draw method for contours into beams eye views
;;; here from contours module to break up a file dependency cycle.
;;; 11-Apr-1993 I. Kalet replace explicit beam parameter copies with
;;; reference to beam itself.
;;; 22-Jul-1993 I. Kalet provide stub method for
;;; generate-image-from-set so that Show Image in BEV does not crash.
;;; Later this will produce a DRR.  Also move refresh notifys to the
;;; beam-graphics module.
;;;  5-Sep-1993 I. Kalet move draw method for contours in bev to
;;;  contour-graphics module.
;;; 18-Apr-1994 I. Kalet change refs to view origin to new ones
;;; 16-May-1994 I. Kalet add *bev-pix-per-cm* as default scale factor
;;; for beams-eye-view.
;;; 12-Jan-1995 I. Kalet use table-position from view, not beam.
;;; 21-Jan-1997 I. Kalet eliminate table-position, eliminate
;;; references to geometry package.
;;; 19-Jan-1998 I. Kalet cache an array for transform, not a bunch of
;;; slots, one for each coefficient.
;;; 11-Jun-1998 I. Kalet don't just set the origin, check if the slots
;;; are bound first.
;;; 19-Jun-1998 I. Kalet move method for generate-image-from-set to
;;; medical-images where the others are.
;;; 12-Aug-1998 I. Kalet add an event to announce that view background
;;; image needs recomputing, not just window or level.
;;; 13-Apr-1999 C. Wilcox added drr-state and drr-args slots to 
;;;  support background computation of drr's.  Added remove-bg-drr
;;;  for same purpose.
;;;  4-Sep-2000 I. Kalet rearrange events and announcements for OpenGL
;;; 16-Dec-2000 I. Kalet add a display-func slot to cache a DRR
;;; incremental display update function, initialize to nil.  Also,
;;; initialize name to include beam name.
;;;

(in-package :prism)

;;;----------------------------------------------------

(defparameter *bev-pix-per-cm* 15.0
  "Default scale factor for newly created beams-eye-view.")

;;;----------------------------------------------------

(defclass beams-eye-view (view)

  ((beam-for :type beam
	     :accessor beam-for
	     :initarg :beam-for
	     :documentation "The beam for which this is the beam's eye
view.")

   (bev-transform :type (simple-array single-float (12))
		  :accessor bev-transform
		  :documentation "The transformation matrix from
patient coordinate system to gantry coordinate system.")

   (reset-image :type ev:event
		:accessor reset-image
		:initform (ev:make-event)
		:documentation "Announced when something changes the
view that requires a new background image, not just a window or level,
e.g., the gantry angle changes for the beam of the view.")

   (drr-args :accessor drr-args
	     :initform nil
	     :documentation "The current args for the iterative drr
calculation.  It is currently defined as a vector with slots:
 0: pixels  1: float-array  2: value-function  3: current-row
 4: max-calculated-float.")

   (drr-state :type (member 'running 'stopped 'paused)
	      :accessor drr-state
	      :initform 'stopped
	      :documentation "The current state of the iterative drr
calculation, if drr-state equals 'stopped then drr-args is undefined.")

   (image-button :accessor image-button
		 :initform nil
		 :documentation "The image button in the view panel.")

   (display-func :accessor display-func
		 :initarg :display-func
		 :documentation "A function of one input, the view,
that handles incremental display updates during DRR calculations.")

   )

  (:documentation "A beam's eye view is a specialization of a view,
and is associated with a particular beam.  The view is projected from
the beam's source point to a plane perpendicular to the beam's central
axis at a location along the axis determined by view-position, which
defaults to 0.0, in which case the plane passes through the isocenter.
The beam is referenced here so that objects can be drawn in the view,
and the transformation matrix needed to draw anatomical objects into
the view is cached here for efficiency.")

  (:default-initargs :scale *bev-pix-per-cm* :display-func nil)

  )

;;;-------------------------------------

(defun compute-pstruct-transform (bev)

  "compute-pstruct-transform bev

Computes and returns the patient to gantry space transformation matrix
needed to project pstructs into the plane of the beam's eye view, bev.
This transformation is based on the location and orientation
attributes of the the beam's eye view, and consists of a simple array
of twelve terms -- the the homogeneous matrix entries:

  r00 r01 r02 r03
  r10 r11 r12 r13
  r20 r21 r22 r23

The bottom row is not computed, since it is always 0 0 0 1."

  (let* ((gan-ang (* (the single-float (gantry-angle (beam-for bev)))
		     *pi-over-180*))
         (trn-ang (* (the single-float (couch-angle (beam-for bev)))
		     *pi-over-180*))
         (sin-g (sin gan-ang))
	 (cos-g (cos gan-ang))
	 (sin-t (sin trn-ang))
	 (cos-t (cos trn-ang))
         (dx (- (the single-float (couch-lateral (beam-for bev)))))
         (dy (- (the single-float (couch-height (beam-for bev)))))
         (dz (- (the single-float (couch-longitudinal (beam-for bev)))))
         (fac (+ (* dx cos-t) (* dz sin-t)))
	 (tmp-array (make-array 12 :element-type 'single-float)))
    (declare (single-float gan-ang trn-ang sin-g cos-g sin-t cos-t
			   dx dy dz fac)
	     (type (simple-array single-float (12)) tmp-array))
    (setf (aref tmp-array 0) (* cos-t cos-g)
	  (aref tmp-array 1) (- sin-g)
	  (aref tmp-array 2) (* sin-t cos-g)
	  (aref tmp-array 3) (- (* dy sin-g) (* cos-g fac))

	  (aref tmp-array 4) sin-t
	  (aref tmp-array 5) 0.0
	  (aref tmp-array 6) (- cos-t)
	  (aref tmp-array 7) (- (* dz cos-t) (* dx sin-t))

	  (aref tmp-array 8) (* cos-t sin-g)
	  (aref tmp-array 9) cos-g
	  (aref tmp-array 10) (* sin-t sin-g)
	  (aref tmp-array 11) (- (+ (* dy cos-g) (* sin-g fac))))
    (setf (bev-transform bev) tmp-array)))

;;;-------------------------------------

(defun refresh-bev (bev b &rest other-pars)

  "refresh-bev bev b &rest other-pars

indirectly regenerates graphic primitives for everything in view bev,
ignoring b and any other parameters, and redraws the view."

  (declare (ignore b other-pars))
  (compute-pstruct-transform bev)
  (setf (drr-state bev) 'stopped)
  (ev:announce bev (reset-image bev))
  (ev:announce bev (refresh-fg bev))
  (display-view bev))

;;;-------------------------------------

(defmethod initialize-instance :after ((view beams-eye-view)
				       &rest initargs)
  (declare (ignore initargs))
  (setf (name view) (format nil "BEV for ~A" (name (beam-for view))))
  (compute-pstruct-transform view))

;;;---------------------------------------

(defun remove-bg-drr (bev)
  (sl:dequeue-bg-event #'(lambda(x) (and (eq (first x) 'drr-bg)
					 (eq (second x) bev)))))

;;;---------------------------------------

(defmethod (setf drr-state) :after (new-state (bev beams-eye-view))

  (let ((ib (image-button bev)))
    (when ib
      (cond
       ((eq new-state 'running)
	(setf (sl:fg-color ib) 'sl:green))
       ((eq new-state 'paused)
	(setf (sl:fg-color ib) 'sl:yellow))
       ((eq new-state 'stopped)
	(setf (sl:fg-color ib) 'sl:red))
       (t (setf (sl:fg-color ib) 'sl:white))))))

;;;---------------------------------------
;;; End.
