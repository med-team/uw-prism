;;;
;;; electron-dose
;;;
;;; The electron dose computation functions
;;;
;;; 13-Jun-1998 P. Cho started
;;; 17-Nov-1998 P. Cho complete standalone version
;;; 10-Dec-1998 P. Cho precompute sigmaRMS for homogeneous case
;;; 17-Dec-1998 P. Cho implement precomputation of error functions
;;; 18-Dec-1998 P. Cho working on Prism interface
;;; 28-Mar-1999 I. Kalet cleanup, modularize
;;; 11-Jul-1999 I. Kalet continuing cleanup
;;; 04-Feb-2000 BobGian integrate nested interpolation for DD-TABLES
;;;   and ROF-TABLES.  Change a few vars from global to local.  Begin
;;;   preliminary optimization.  AREA-OF-POLYGON -> POLYGONS package.
;;;   Make COMPUTE-ELECTRON-DOSE return T on success, NIL on failure (early
;;;   return on detection of nominal SSD or cutout dimensions out of range).
;;; 10-Feb-2000 BobGian first working version - fix bugs in quadtree code
;;;   plus various fencepost errors and array-bounds calculations.  Add
;;;   check for cutout extending beyond applicator.
;;; 02-Mar-2000 BobGian intermediate version - corrects fencepost errors
;;;   and iteration overruns; contains some optimizations.  This version
;;;   produces same results as Paul Cho's original version.  It also
;;;   generates considerable testing output (but even more commented out).
;;; 02-Nov-2000 BobGian inline common functions (square, distance), inline
;;;   functions used only once, add declaration, factor out redundant calls
;;;   to PATHLENGTH, replace PATHLENGTH where density is constant by use
;;;   of geometric distance, other optimizations.  Includes debugging
;;;   printout for testing optimization.
;;; 30-May-2001 BobGian - major restructuring of pathlength computation:
;;;   Replace organ density lookup via differential pathlength call with
;;;     direct lookup using structure returned by PATHLENGTH-RAYTRACE.
;;;   Replace FLU2DOSE normalization loop use of phantom (using pathlength
;;;     computation) with geometric distance calculation in semi-infinite
;;;     virtual phantom (extending over half-space with boundary at patient
;;;     surface).  Better handling of end-of-loop termination criteria.
;;;     Repair several potential divide-by-zero conditions in the process.
;;;   Separate raytracing from line integration so that redundant computation
;;;     can be factored out (PATHLENGTH-RAYTRACE called once to build structure
;;;     that can be queried by PATHLENGTH-INTEGRATE multiple times).
;;;   Change all calling points in Electron and Photon dose calc.
;;;   Wrap generic arithmetic with THE-declared types.
;;;   Move all DECONSTANTs, DEFSTRUCTs, and DEFMACROs to "dosecomp-decls".
;;;   Wrap array references [E0, RP, THETA-AIR, F1, F2, Z1, Z2 slot objects]
;;;     in (SIMPLE-ARRAY T 2) declarations to allow inlining.
;;; 03-Jun-2001 BobGian fix bug giving non-zero dose for point outside body.
;;; 24-Aug-2001 Paul Cho and BobGian - add two contours to QUANTIZE-EXPFIELD;
;;;     add missing PBeam weighing factor in rFluence.
;;; 27-Aug-2001 Paul Cho, BobGian - fix bug in PBEAM copy (forgot WEIGHT slot).
;;; 07-Dec-2001 Paul Cho, BobGian - fix bug in FIND-EQUIV-RECT.
;;; 11-Dec-2001 BobGian add storage of ROF and SSD in DOSE-RESULT.
;;; 15-Mar-2002 BobGian parameterize constants used for Pathlength
;;;   and electron dosecalc.
;;; 15-Mar-2002 BobGian change "erroneous but OK" conditions to call
;;;   sl:ACKNOWLEDGE rather than ERROR.  Some conditions are continuable;
;;;   others abort dosecalc by immediately returning NIL.
;;; 15-Mar-2002 BobGian PATHLENGTH-RAYTRACE used for "ray out-of-body"
;;;   detection.  Former errors on this condition now return gracefully.
;;; 15-Mar-2002 BobGian Organ-densities > 2.0 are now OK - computation
;;;   proceeds using parameters from highest density break-point
;;;   [ie, linear extrapolation].
;;; 29-Apr-2002 BobGian PATHLENGTH-RAYTRACE cannot be used alone for
;;;   "ray out-of-body" detection, since it traces full length of normalizing
;;;   distance.  Must also integrate to dosepoint for correct test.
;;; 03-Jan-2003 BobGian:
;;;   Pathlength raytraces along pencil beams are now cached in alternating
;;;    slots of PBEAM-ARRAY [the array holding the pencil-beam array objects].
;;;    This allows this operation to be factored out of the per-dose-point
;;;    computation and done only once per pencil beam.
;;;   QUANTIZE-EXPFIELD allocates double-sized PBEAM-ARRAY to hold cached
;;;    raytrace lists [to be computed later].
;;;   PATHLENGTH-INTEGRATE does both density-weighted and homogeneous calcs
;;;    in single call [when necessary].
;;;   PBEAM array objects hold both collimator and patient coordinates rather
;;;    than using separate collections of PBEAM objects.
;;;   PBEAM, QNODE, and TILE object representation changed from strucures to
;;;    arrays, with inlined accessor.
;;;   Argument-passing and value-returning conventions altered for
;;;    PATHLENGTH-RAYTRACE and PATHLENGTH-INTEGRATE.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 26-Mar-2003 BobGian - fix bug in PBeam Col->Pat coordinate transformation.
;;; 29-Aug-2003 BobGian - remove obsolete version number in change log header.
;;;   Instrument code with printouts for tracing dose calculation accuracy.
;;; 03-Nov-2003 BobGian - more specific/meaningful names for some constants:
;;;     Exp-Width -> Cutout-Expand-Width
;;;     Step-Size -> Electron-Step-Size
;;; 09-Nov-2003 BobGian - mark debugging code for deletion in production
;;;   version; reformat some comments and indentation.
;;; 23-Mar-2004 BobGian - delete debugging code.
;;; 12-Feb-2005 AMSimms - update SINGLE-FLOAT calls (an Allegro specific
;;;     coercion function) to use coerce esplicitly
;;;

(in-package :prism)

;;;=============================================================
;;; COMPUTE-ELECTRON-DOSE: Main electron dose calculation program
;;;=============================================================

(defun compute-electron-dose (bm bms pts gg organ-vertices-list
			      organ-z-extents organ-density-array)

  "compute-electron-dose bm bms pts gg
			 organ-vertices-list organ-z-extents
			 organ-density-array

computes the dose to each point in PTS, a list of points, and all
points in the grid specified by GG, a GRID-GEOMETRY, for electron beam
BM, stores the doses in the points and/or GRID attribute of the beam's
DOSE-RESULT.  One of PTS or GG should be NIL, the other non-NIL.
Rest of args describe patient's anatomy (beam-independent).
Returns T on success and NIL if unable to complete."

  (declare (type (simple-array single-float 1) organ-density-array)
	   (type list bms pts organ-vertices-list organ-z-extents))

  ;; The arrays below are all general [type T] as created by GET-OBJECT.  They
  ;; could be converted to (SIMPLE-ARRAY SINGLE-FLOAT 2) as later optimization,
  ;; by a preprocessing step.

  (let* ((mach (machine bm))
	 (dosedata (dose-data mach))
	 (num-beams (length bms))
	 (rslt (result bm))                         ;Object holding result
	 (beam-name (name bm))
	 (beam-num (the fixnum (1+ (position bm bms :test #'eq))))
	 (g-sad (cal-distance mach))  ; geometric source-to-isocenter distance
	 (coll (collimator bm))
	 (cutout-list (vertices coll))              ; cutout vertices list
	 (energy-value (energy coll))
	 (e-index
	   (position energy-value (energies (collimator-info mach)) :test #'=))
	 ;; Virtual Source-to-Axis distance.
	 (v-sad (nth e-index (the list (vsad dosedata))))
	 (aperture-value (cone-size coll))
	 (app-index (position aperture-value
			      (cone-sizes (collimator-info mach))
			      :test #'=))
	 (appl-size (nth app-index (the list (applic-sizes dosedata))))
	 (init-energy (aref (the (simple-array t 2) (e0 dosedata))
			    e-index app-index))     ; initial energy
	 (rp-val (aref (the (simple-array t 2) (rp dosedata))
		       e-index app-index))          ; practical range
	 (theta-air-val
	   (* #.(sqrt 2.0)                     ; initial in-air spatial spread
	      (the single-float
		(aref (the (simple-array t 2) (theta-air dosedata))
		      e-index app-index))))
	 (airgap-val (airgap dosedata)) ; air-gap between cutout and isocenter
	 (v-scd (- v-sad airgap-val))   ;virtual source to applicator distance
	 (tab (couch-angle bm))                     ; couch table angle
	 (gan (gantry-angle bm))                    ; gantry angle
	 (col (collimator-angle bm))                ; collimator angle

	 (r00 0.0)                         ;Terms of Collimator-to-Patient and
	 (r01 0.0)                           ;Patient-to-Collimator transform,
	 (r02 0.0)                        ;which are transposes of each other.
	 (r10 0.0)              ;Terms indicated here are terms of Coll-to-Pat
	 (r11 0.0)                 ;transform.  Terms of Pat-to-Coll transform
	 (r12 0.0)                ;are identical but with subscripts after the
	 (r20 0.0)                                  ;'R' reversed.
	 (r21 0.0)
	 (r22 0.0)

	 ;; FMCS parameters directly looked up and used immediately.
	 (fmcs (get-fmcs (aref (the (simple-array t 2) (f1 dosedata))
			       e-index app-index)
			 (aref (the (simple-array t 2) (f2 dosedata))
			       e-index app-index)
			 (aref (the (simple-array t 2) (z1 dosedata))
			       e-index app-index)
			 (aref (the (simple-array t 2) (z2 dosedata))
			       e-index app-index)
			 rp-val))
	 ;; In the patient coordinate system, the origin of the anatomy
	 ;; vertices coincides with the isocenter when the couch is in
	 ;; home position, therefore the isocenter is given by the following:
	 (iso-xp (- (the single-float (couch-lateral bm))))
	 (iso-yp (- (the single-float (couch-height bm))))
	 (iso-zp (- (the single-float (couch-longitudinal bm))))
	 (src-xp 0.0) (src-yp 0.0) (src-zp 0.0)  ;geometric source coord in PC
	 ;; Pre-compute SPATIAL-SPREAD for water for given energy and Rp-Val.
	 (spatial-spread-vector (get-spatial-spread-vector init-energy rp-val))
	 ;; Local variables initialized and used below.
	 (rect-width 0.0)
	 (rect-height 0.0)
	 (g-ssd 0.0)                       ; geometric source-to-skin distance
	 (v-ssd 0.0)                         ; virtual source-to-skin distance
	 (v-spx 0.0)                                ; virtual source in PC
	 (v-spy 0.0)
	 (v-spz 0.0)
	 (pen-num 0)
	 (pbeam-array)                              ;Array of Pencil beams
	 (quadtiles)                                ;Array of quadtree tiles
	 (eflist '())
	 (flu2dose fmcs)                         ;Binding for declaration only
	 (rof 0.0)                                  ;Relative Output Factor
	 (nquad 0)                                ;number of merged quad tiles
	 (erf-table *erf-table*)                    ;Lookup table
	 (arg-vec (make-array #.Argv-Size :element-type 'single-float)))

    (declare (type single-float v-spx v-spy v-spz iso-xp iso-yp iso-zp
		   appl-size init-energy src-xp src-yp src-zp tab gan col
		   rect-width rect-height rof airgap-val theta-air-val
		   v-scd v-sad g-ssd v-ssd g-sad rp-val energy-value
		   aperture-value r00 r01 r02 r10 r11 r12 r20 r21 r22)
	     (type simple-base-string beam-name)
	     (type (simple-array single-float 1) fmcs flu2dose
		   spatial-spread-vector)
	     (type (simple-array single-float (#.Erf-Table-Size)) erf-table)
	     (type (simple-array single-float (#.Argv-Size)) arg-vec)
	     (type list cutout-list eflist)
	     (type fixnum num-beams beam-num e-index app-index nquad pen-num))

    (format t "~&~%Computing ~A dose for beam ~S (~D of ~D).~%"
	    (if pts "points" "grid") beam-name beam-num num-beams)

    ;; Compute terms of the Collimator-to-Patient and Patient-to-Collimator
    ;; transforms.  These transforms, represented as matrices whose elements
    ;; are the Rxx terms here, are transposes of each other.  Therefore the
    ;; same terms are used for both when inline expansions are used following.
    ;; Be careful about which terms are which.
    (let* ((trn-rad (* tab #.(coerce (/ pi 180.0d0) 'single-float)))
	   (gan-rad (* gan #.(coerce (/ pi 180.0d0) 'single-float)))
	   (col-rad (* col #.(coerce (/ pi 180.0d0) 'single-float)))
	   (sin-t (sin trn-rad))
	   (cos-t (cos trn-rad))
	   (sin-g (sin gan-rad))
	   (cos-g (cos gan-rad))
	   (sin-c (sin col-rad))
	   (cos-c (cos col-rad)))
      (declare (type single-float gan-rad col-rad trn-rad sin-g cos-g
		     sin-c cos-c sin-t cos-t))
      (setq r00 (+ (* cos-t cos-g cos-c)
		   (* sin-t sin-c)))
      (setq r01 (- (* sin-t cos-c)
		   (* cos-t cos-g sin-c)))
      (setq r02 (* cos-t sin-g))
      (setq r10 (- (* sin-g cos-c)))
      (setq r11 (* sin-g sin-c))
      (setq r12 cos-g)
      (setq r20 (- (* sin-t cos-g cos-c)
		   (* cos-t sin-c)))
      (setq r21 (- (+ (* sin-t cos-g sin-c)
		      (* cos-t cos-c))))
      (setq r22 (* sin-t sin-g)))

    ;; Transform geometric source coord from coll to patient and offset
    ;; by couch x-y-z shift.
    (setq src-xp (+ (* r02 g-sad) iso-xp))
    (setq src-yp (+ (* r12 g-sad) iso-yp))
    (setq src-zp (+ (* r22 g-sad) iso-zp))

    ;;-----------------------------------------------------------
    ;; Determine central axis SSD.  Do this early to find out if
    ;; SSD for this patient is within the limits of allowable
    ;; extended SSD.  For now, maximum allowable SSD of 120 cm
    ;; is assumed.
    ;;-----------------------------------------------------------
    ;; Find geometric distance from source to isocenter and to patient surface.
    ;; Load argument vector for call to PATHLENGTH-RAYTRACE.
    (let ((scale-factor (/ #.Pathlength-Ray-Maxlength g-sad)))
      (declare (type single-float scale-factor))
      (setf (aref arg-vec #.Argv-Src-X) src-xp)
      (setf (aref arg-vec #.Argv-Src-Y) src-yp)
      (setf (aref arg-vec #.Argv-Src-Z) src-zp)
      (setf (aref arg-vec #.Argv-Dp-X)
	    (+ src-xp (* scale-factor (- iso-xp src-xp))))
      (setf (aref arg-vec #.Argv-Dp-Y)
	    (+ src-yp (* scale-factor (- iso-yp src-yp))))
      (setf (aref arg-vec #.Argv-Dp-Z)
	    (+ src-zp (* scale-factor (- iso-zp src-zp)))))

    (let ((ray-alphalist
	    (pathlength-raytrace arg-vec organ-vertices-list organ-z-extents)))
      (declare (type list ray-alphalist))

      (unless (consp ray-alphalist)
	(sl:acknowledge
	  (format nil "Central-Axis is outside patient in beam ~S (~D of ~D)."
		  beam-name beam-num num-beams))
	(return-from compute-electron-dose nil))
      (setq g-ssd (caar ray-alphalist)))

    ;; Compute G-SSD, geometric source-to-skin distance, and check that it is
    ;; within appropriate range.  Assuming that we have %DD data up to 120-cm
    ;; SSD, calculation request for SSD > 120 is rejected.
    (when (or (< g-ssd #.Electron-SSD-Minlength)
	      (> g-ssd #.Electron-SSD-Maxlength))
      (sl:acknowledge
	(list (format nil "Geometric SSD (~F) is outside ~F to ~F cm."
		      g-ssd #.Electron-SSD-Minlength #.Electron-SSD-Maxlength)
	      (format nil "In beam ~S (~D of ~D)."
		      beam-name beam-num num-beams)))
      (return-from compute-electron-dose nil))

    ;; Compute V-SSD, virtual source-to-skin distance, along central axis.
    (setq v-ssd (+ v-sad (- g-ssd g-sad)))

    ;; Check that cutout is within applicator dimensions.
    (let ((sz (* 0.5 appl-size)))
      (declare (type single-float sz))
      (dolist (vert cutout-list)
	(when (or (> (the single-float (abs (the single-float (first vert))))
		     sz)
		  (> (the single-float (abs (the single-float (second vert))))
		     sz))
	  (sl:acknowledge
	    (format nil
		    "Cutout is too big for applicator in beam ~S (~D of ~D)."
		    beam-name beam-num num-beams))
	  (return-from compute-electron-dose nil))))

    ;; Pre-compute fluence-to-dose calibration factor.
    ;; Reload virtual source instead of geometric source coordinates.
    ;; Terms are virtual source coordinates transformed from collimator
    ;; to patient coordinate system.
    (setf (aref arg-vec #.Argv-Src-X) 0.0)          ; virtual src in PC
    (setf (aref arg-vec #.Argv-Src-Y) v-sad)
    (setf (aref arg-vec #.Argv-Src-Z) 0.0)

    ;; Find equivalent rectangle.
    ;; Algorithm is not accurate unless cutout has minimum dimension
    ;; (smaller of width/length) of at least 2 cm.  Punt otherwise.
    ;; Max dimension CAN be larger than applicator size, as for a diagonal
    ;; cutout shape whose max dimension exceeds applicator edge length.
    (multiple-value-setq (rect-width rect-height)
	(find-equiv-rect cutout-list))

    (when (or (< rect-width #.Cutout-Min-Size)
	      (< rect-height #.Cutout-Min-Size))
      (sl:acknowledge
	(format nil "Cutout is too small for applicator in beam ~S (~D of ~D)."
		beam-name beam-num num-beams))
      (return-from compute-electron-dose nil))

    ;; FLU2DOSE interpolation, once RECT-WIDTH and RECT-HEIGHT are available.
    (setq flu2dose (depth-dose-interp (dd-tables dosedata)
				      energy-value aperture-value g-ssd
				      rect-width rect-height))

    (setq rof (rof-interp (rof-tables dosedata)
			  energy-value aperture-value
			  g-ssd rect-width rect-height))

    ;; Halve equivalent-rectangle dimensions for QUANTIZE-EFIELD and SAIR-RECT.
    (setq rect-width (* rect-width 0.5)
	  rect-height (* rect-height 0.5))

    ;; Quantize equivalent rectangle field [using half-width and half-height].
    (multiple-value-setq (pen-num pbeam-array)
	(quantize-efield (list (list rect-width rect-height)
			       (list (- rect-width) rect-height)
			       (list (- rect-width) (- rect-height))
			       (list rect-width (- rect-height)))
			 appl-size airgap-val arg-vec))

    ;; Transform pencil-beam coordinates from collimator to patient frame.
    (do ((ip 0 (the fixnum (1+ ip)))
	 (pb-obj))
	((= ip pen-num))
      (declare (type fixnum ip))
      (setq pb-obj (aref (the (simple-array t 1) pbeam-array) ip))
      (setf (pbeam-xp (the (simple-array single-float (7)) pb-obj))
	    (pbeam-xc (the (simple-array single-float (7)) pb-obj)))
      (setf (pbeam-yp (the (simple-array single-float (7)) pb-obj))
	    (pbeam-zc (the (simple-array single-float (7)) pb-obj)))
      (setf (pbeam-zp (the (simple-array single-float (7)) pb-obj))
	    (- (the single-float
		 (pbeam-yc (the (simple-array single-float (7)) pb-obj))))))

    ;; For every depth (in CC system) renormalize FLU2DOSE [except for entry
    ;; in slot 0 which remains 0.0].  Stop when depth exceeds Rp-Val for pencil
    ;; beam along central axis.  If depth for other pencil beams exceeds Rp-Val
    ;; before stopping iteration [due to slant depth], extrapolate with last
    ;; valid value.
    (do ((czz #.(- Electron-Step-Size) (- czz #.Electron-Step-Size))
	 (proj-0-factor (/ v-sad v-scd))    ;Projection factor for depth zero.
	 (idx 1 (the fixnum (1+ idx)))        ; FLU2DOSE array index <-> depth
	 (depth-lim (- rp-val)))
	((< czz depth-lim))

      (declare (type single-float czz proj-0-factor depth-lim)
	       (type fixnum idx))

      ;; Ray-trace through virtual phantom from virtual source to calc plane
      ;; along pencil-beam axis to get depth in phantom (in PC).  Normalization
      ;; is to density = 1.0 water phantom extending over semi-infinite region
      ;; of space [ie, all space for which Y coord (PC) <= 0.0 .
      (do ((ip 0 (the fixnum (1+ ip)))
	   (pb-obj)
	   (proj-factor (/ (- v-sad czz) v-scd))
	   (pbcx 0.0) (pbcy 0.0)                  ;Pencil Beam Collimator X/Y.
	   (projpx 0.0) (projpy 0.0) (projpz 0.0)   ;Patient coordinates.
	   (total-fluence 0.0)
	   (unitdepth 0.0)                        ; Used as local scratch var.
	   (sigma-rms 0.0))                ; Used as spatial spread parameter.
	  ((= ip pen-num)
	   (when (> total-fluence 0.0)
	     (setf (aref flu2dose idx)
		   (/ (the single-float (aref flu2dose idx)) total-fluence))))

	(declare (type single-float pbcx pbcy unitdepth sigma-rms
		       projpx projpy projpz proj-factor total-fluence)
		 (type fixnum ip))

	(setq pb-obj (aref (the (simple-array t 1) pbeam-array) ip)
	      pbcx (pbeam-xc (the (simple-array single-float (7)) pb-obj))
	      pbcy (pbeam-yc (the (simple-array single-float (7)) pb-obj)))

	;; Find pencil-beam axis coordinates at calc plane in PC.
	(setq projpx (* pbcx proj-factor))
	(setq projpy czz)
	(setq projpz (- (* pbcy proj-factor)))

	(setq unitdepth (3d-distance
			  (* pbcx proj-0-factor)    ;Ray projected to virtual
			  0.0                       ;phantom surface.
			  (- (* pbcy proj-0-factor))
			  projpx projpy projpz)) ;Ray projected to calc plane.

	;; Look up SIGMA-RMS and FMCS.  UNITDEPTH must be > 0.0 because
	;; iteration starts at depth Electron-Step-Size.
	(when (> unitdepth rp-val)
	  ;; If slant depth for this pencil beam exceeds Rp-Val, extrapolate
	  ;; using fluence value for Rp-Val.
	  (setq unitdepth rp-val))

	(let ((d (the fixnum
		   (round (the single-float
			    (* unitdepth #.(/ 1.0 Electron-Step-Size)))))))
	  (declare (type fixnum d))
	  (setq sigma-rms (* (the single-float (aref spatial-spread-vector d))
			     (the single-float (aref fmcs d)))))

	;; Calc pt is within 6 SIGMA-RMS [RMS units] of lateral distance
	;; between pencil axis and calc point.  Distance squared being compared
	;; to threshold squared.  <= changed to < comparison to exclude case
	;; of SIGMA-RMS = 0.0, causing div-by-zero in RFLUENCE.
	(when (< (+ (sqr-float (* pbcx proj-factor))
		    (sqr-float (* pbcy proj-factor)))
		 (* 36.0 sigma-rms sigma-rms))

	  ;; Accumulate fluence - Fast rect method.
	  (incf
	    total-fluence
	    (* (the single-float
		 (sair-rect
		   rect-width                       ; Find Sair in CC
		   rect-height
		   pbcx
		   pbcy
		   (3d-distance
		     (pbeam-xp (the (simple-array single-float (7)) pb-obj))
		     (pbeam-yp (the (simple-array single-float (7)) pb-obj))
		     (pbeam-zp (the (simple-array single-float (7)) pb-obj))
		     projpx projpy projpz)
		   theta-air-val
		   proj-factor
		   erf-table))
	       ;; Find CAX rfluence in cc.
	       (the single-float
		 (rfluence
		   pbcx pbcy
		   (pbeam-wt (the (simple-array single-float (7)) pb-obj))
		   0.0 0.0 sigma-rms proj-factor erf-table)))))))

    ;; Compute dose for user specified field.  Transform virtual source
    ;; coordinates from coll to patient and offset by couch x-y-z shift.
    ;; Load virtual source coords for calls to follow.
    (setf (aref arg-vec #.Argv-Src-X) (setq v-spx (+ (* r02 v-sad) iso-xp)))
    (setf (aref arg-vec #.Argv-Src-Y) (setq v-spy (+ (* r12 v-sad) iso-yp)))
    (setf (aref arg-vec #.Argv-Src-Z) (setq v-spz (+ (* r22 v-sad) iso-zp)))

    ;; Expand cutout-list and quantize into pencil beams.
    (multiple-value-setq (pen-num pbeam-array eflist)
	(quantize-expfield cutout-list
			   (+ appl-size #.Cutout-Expand-Width)
			   airgap-val arg-vec))

    ;; Transform pencil-beam coordinates from collimator to patient frame.
    (do ((ip 0 (the fixnum (+ ip 2)))
	 (pb-obj)
	 (pb-x 0.0) (pb-y 0.0) (pb-z 0.0))
	((= ip pen-num))
      (declare (type single-float pb-x pb-y pb-z)
	       (type fixnum ip))
      (setq pb-obj (aref (the (simple-array t 1) pbeam-array) ip)
	    pb-x (pbeam-xc (the (simple-array single-float (7)) pb-obj))
	    pb-y (pbeam-yc (the (simple-array single-float (7)) pb-obj))
	    pb-z (pbeam-zc (the (simple-array single-float (7)) pb-obj)))
      (setf (pbeam-xp (the (simple-array single-float (7)) pb-obj))
	    (+ (* r00 pb-x) (* r01 pb-y) (* r02 pb-z) iso-xp))
      (setf (pbeam-yp (the (simple-array single-float (7)) pb-obj))
	    (+ (* r10 pb-x) (* r11 pb-y) (* r12 pb-z) iso-yp))
      (setf (pbeam-zp (the (simple-array single-float (7)) pb-obj))
	    (+ (* r20 pb-x) (* r21 pb-y) (* r22 pb-z) iso-zp)))

    ;; Construct the root node structure.
    (let ((qtree (make-qnode 0.0 0.0 (+ appl-size #.Cutout-Expand-Width))))

      ;; Generate quadtree representation of expanded electron field EFLIST.
      (quadtree
	qtree                                       ; base node
	(cond ((< appl-size 0.0)                    ; node resolution
	       (error "COMPUTE-ELECTRON-DOSE [1] Negative applicator size: ~S"
		      appl-size))
	      ((<= appl-size 8.0) 32)
	      ((<= appl-size 25.0) 64)
	      (t (error "COMPUTE-ELECTRON-DOSE [2] Applicator too big: ~S"
			appl-size)))
	eflist arg-vec)

      ;; Count number of merged nodes to determine the exact dimension
      ;; of quadtile structure array.
      (setq quadtiles (make-array (count-qnodes qtree)
				  :element-type t :initial-element nil))

      ;; Traverse tree to tabulate survivors.
      (setq nquad (traverse-tree qtree quadtiles 0)))

    ;; Now ready to iterate over the points list or the dose grid.
    (when (consp pts)
      (setf (points rslt)
	    (mapcar #'(lambda (pt)
			(* rof
			   (the single-float
			     (electron-dose
			       (x pt) (y pt) (z pt) v-spx v-spy v-spz v-sad
			       v-scd v-ssd r00 r01 r02 r10 r11 r12 r20 r21 r22
			       rp-val init-energy theta-air-val pen-num
			       pbeam-array cutout-list fmcs flu2dose
			       organ-vertices-list organ-z-extents
			       organ-density-array iso-xp iso-yp iso-zp
			       quadtiles nquad erf-table arg-vec))))
	      pts)))

    (when gg
      (let* ((nx (x-dim gg))
	     (ny (y-dim gg))
	     (nz (z-dim gg))
	     (xp-step (/ (the single-float (x-size gg))
			 (coerce (the fixnum (1- nx)) 'single-float)))
	     (yp-step (/ (the single-float (y-size gg))
			 (coerce (the fixnum (1- ny)) 'single-float)))
	     (zp-step (/ (the single-float (z-size gg))
			 (coerce (the fixnum (1- nz)) 'single-float)))
	     (dose-array (grid rslt)))
	(declare (type (simple-array single-float 3) dose-array)
		 (type single-float xp-step yp-step zp-step)
		 (type fixnum nx ny nz))
	(do ((x-idx 0 (the fixnum (1+ x-idx)))
	     (xp (x-origin gg) (+ xp xp-step))
	     (y-orig (y-origin gg))
	     (z-orig (z-origin gg)))
	    ((= x-idx nx))
	  (declare (type single-float xp y-orig z-orig)
		   (type fixnum x-idx))
	  (format t "~&Beam ~D of ~D, Plane ~D of ~D.~%"
		  beam-num num-beams (the fixnum (1+ x-idx)) nx)
	  (do ((y-idx 0 (the fixnum (1+ y-idx)))
	       (yp y-orig (+ yp yp-step)))
	      ((= y-idx ny))
	    (declare (type single-float yp)
		     (type fixnum y-idx))
	    (do ((z-idx 0 (the fixnum (1+ z-idx)))
		 (zp z-orig (+ zp zp-step)))
		((= z-idx nz))
	      (declare (type single-float zp)
		       (type fixnum z-idx))
	      (setf (aref dose-array x-idx y-idx z-idx)
		    (* rof
		       (the single-float
			 (electron-dose
			   xp yp zp v-spx v-spy v-spz v-sad v-scd v-ssd r00
			   r01 r02 r10 r11 r12 r20 r21 r22 rp-val init-energy
			   theta-air-val pen-num pbeam-array cutout-list fmcs
			   flu2dose organ-vertices-list organ-z-extents
			   organ-density-array iso-xp iso-yp iso-zp
			   quadtiles nquad erf-table arg-vec)))))))))

    (setf (ssd rslt) g-ssd)
    (setf (output-comp rslt) rof))

  ;; Return T if computation completes successfully.  If something goes wrong,
  ;; function returns early with NIL indicating failure.  Return value sets
  ;; VALID-POINTS/VALID-GRID flags on return.
  t)

;;;-------------------------------------------------------------
;;; ELECTRON-DOSE: compute electron dose to a single point
;;;-------------------------------------------------------------
;;; (Px, Py, Pz) = dose point coordinates in patient geometry
;;;-------------------------------------------------------------

(defun electron-dose (px py pz v-spx v-spy v-spz v-sad v-scd v-ssd r00 r01 r02
		      r10 r11 r12 r20 r21 r22 rp-val init-energy theta-air-val
		      pen-num pbeam-array cutout-list fmcs flu2dose
		      organ-vertices-list organ-z-extents organ-density-array
		      iso-xp iso-yp iso-zp quadtiles nquad erf-table arg-vec
		      &aux (cx 0.0) (cy 0.0) (cz 0.0) (cz2 0.0)
		      (proj-factor 0.0) (total-dose 0.0))

  "electron-dose px py pz v-spx v-spy v-spz v-sad v-scd v-ssd
		 r00 r01 r02 r10 r11 r12 r20 r21 r22
		 rp-val init-energy theta-air-val pen-num
		 pbeam-array cutout-list fmcs flu2dose
		 organ-vertices-list organ-z-extents organ-density-array
		 iso-xp iso-yp iso-zp
		 quadtiles nquad erf-table arg-vec

computes electron dose to a single point px py pz."

  ;; Px Py Pz = calc point in patient coordinate system
  ;; init-energy = initial energy of the beam (not nominal E)
  ;; Rp-Val = practical range of the beam
  ;; unitplen = unit pathlength (geometric distance) between skin and calc pt
  ;; V-SPxyz = virtual source position in PC
  ;; projPxyz = pencil beam coordinates projected onto calc depth in PC

  (declare (type single-float px py pz cx cy cz v-spx v-spy v-spz v-sad v-scd
		 v-ssd init-energy rp-val theta-air-val iso-xp iso-yp iso-zp
		 proj-factor cz2 total-dose r00 r01 r02 r10 r11 r12 r20 r21 r22)
	   (type (simple-array single-float 1) fmcs flu2dose)
	   (type (simple-array t 1) pbeam-array quadtiles)
	   (type (simple-array single-float (#.Erf-Table-Size)) erf-table)
	   (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type list organ-vertices-list organ-z-extents)
	   (type (simple-array single-float 1) organ-density-array)
	   (type list cutout-list)
	   (type fixnum nquad pen-num))

  ;; Transform dose point coordinates from patient to coll.
  ;; (Px, Py, Pz) -> (Cx, Cy, Cz)
  (let ((pp-x (- px iso-xp))
	(pp-y (- py iso-yp))
	(pp-z (- pz iso-zp)))
    (declare (type single-float pp-x pp-y pp-z))
    (setq cx (+ (* r00 pp-x)      ; calc point in collimator coordinate system
		(* r10 pp-y)
		(* r20 pp-z)))
    (setq cy (+ (* r01 pp-x)
		(* r11 pp-y)
		(* r21 pp-z)))
    (setq cz (+ (* r02 pp-x)
		(* r12 pp-y)
		(* r22 pp-z)))
    (setq cz2 (+ cz (- v-ssd v-sad))))    ;calc depth rel to CAX-skin intersec

  ;; Find projection factor for this calc depth.
  (setq proj-factor (/ (- v-sad cz) v-scd))

  ;;For every pencil beam repeat
  (do ((ip 0 (the fixnum (+ ip 2)))
       (pb-obj)
       (pbcx 0.0)                             ;Pencil Beam Collimator X coord.
       (pbcy 0.0)                             ;Pencil Beam Collimator X coord.
       (projpx 0.0)                                 ;Projected pencil beam
       (projpy 0.0)                            ;coordinates in Patient coords.
       (projpz 0.0)
       (zeff 0.0)                                 ;Effective depth for pencil.
       (unitplen 0.0)                          ;Unit path length along pencil.
       (ray-alphalist nil)
       (spatial-spread 0.0))
      ((= ip pen-num))

    (declare (type single-float pbcx pbcy projpx projpy projpz zeff
		   unitplen spatial-spread)
	     (type fixnum ip))

    (setq pb-obj (aref pbeam-array ip)
	  pbcx (pbeam-xc (the (simple-array single-float (7)) pb-obj))
	  pbcy (pbeam-yc (the (simple-array single-float (7)) pb-obj)))

    ;; Find pencil-beam axis coordinates at calc plane in PC.
    (let ((pp-x (* pbcx proj-factor))
	  (pp-y (* pbcy proj-factor)))
      (declare (type single-float pp-x pp-y))
      (setq projpx (+ (* r00 pp-x) (* r01 pp-y) (* r02 cz) iso-xp))
      (setq projpy (+ (* r10 pp-x) (* r11 pp-y) (* r12 cz) iso-yp))
      (setq projpz (+ (* r20 pp-x) (* r21 pp-y) (* r22 cz) iso-zp)))

    ;; Ray-trace through anatomy from virtual source to calc plane along
    ;; pencil-beam axis to get depth in patient (in PC).  Source coordinates
    ;; were loaded before call to ELECTRON-DOSE.
    (let ((scale-factor
	    (/ #.Pathlength-Ray-Maxlength
	       (setf (aref arg-vec #.Argv-Raylen)
		     (3d-distance v-spx v-spy v-spz projpx projpy projpz))))
	  (ray-idx (the fixnum (1+ ip))))
      (declare (type single-float scale-factor)
	       (type fixnum ray-idx))
      (unless (listp (setq ray-alphalist (aref pbeam-array ray-idx)))
	(setf (aref arg-vec #.Argv-Dp-X)
	      (+ v-spx (* scale-factor (- projpx v-spx))))
	(setf (aref arg-vec #.Argv-Dp-Y)
	      (+ v-spy (* scale-factor (- projpy v-spy))))
	(setf (aref arg-vec #.Argv-Dp-Z)
	      (+ v-spz (* scale-factor (- projpz v-spz))))
	(setf (aref pbeam-array ray-idx)
	      (setq ray-alphalist
		    (pathlength-raytrace arg-vec
					 organ-vertices-list
					 organ-z-extents)))))

    ;; RAY-ALPHALIST must be CONSP in order to integrate, and
    ;; PATHLENGTH-INTEGRATE returns T to indicate dosepoint-in-body.
    (cond ((and (consp ray-alphalist)
		(pathlength-integrate arg-vec ray-alphalist
				      organ-density-array :Both))
	   (setq unitplen (aref arg-vec #.Argv-Return-0))
	   (setq zeff (aref arg-vec #.Argv-Return-1)))
	  (t (setq unitplen 0.0)
	     (setq zeff 0.0)))

    ;; Make sure pencil beam is in patient and not deeper than Rp-Val.
    (when (and (> zeff 0.0)                        ; Find SPATIAL-SPREAD in PC
	       (<= zeff rp-val))
      (let ((z-index (the fixnum
		       (round (the single-float
				(* zeff #.(/ 1.0 Electron-Step-Size)))))))
	(declare (type fixnum z-index))
	(setq spatial-spread
	      (* (the single-float (aref fmcs z-index))
		 (the (single-float 0.0 *)
		   ;; Compute spatial spread parameter for inhomogeneity.
		   (sqrt
		     (the (single-float 0.0 *)
		       (cond
			 ((< unitplen #.Electron-Step-Size)
			  (* unitplen unitplen unitplen
			     (the single-float
			       (spower unitplen init-energy rp-val))))

			 ;; Integrate in 1-mm increments.  Find skin distance.
			 (t (let* ((xdiff (- projpx v-spx))
				   (ydiff (- projpy v-spy))
				   (zdiff (- projpz v-spz))
				   (dtot (the (single-float 0.0 *)
					   (sqrt (the (single-float 0.0 *)
						   (+ (* xdiff xdiff)
						      (* ydiff ydiff)
						      (* zdiff zdiff)))))))

			      (declare (type single-float xdiff ydiff
					     zdiff dtot))

			      ;; Ray tracing loop.
			      (do ((zeta ;Source to skin along pencil-beam axis
				     (+ (- dtot unitplen) #.Electron-Step-Size)
				     (+ zeta #.Electron-Step-Size))
				   (zz 0.0)
				   (rstop 0.0)      ;stopping power ratio
				   (rscat 0.0)      ;scattering power ratio
				   (deff 0.0)       ;effective pathlength
				   (org-density 0.0)
				   (sigma-rms 0.0))
				  ((> zeta dtot)
				   sigma-rms)

				(declare (type single-float zeta zz rstop rscat
					       deff org-density sigma-rms))

				(do ((alpha-pairlist ray-alphalist
						     (cdr alpha-pairlist))
				     (alpha-item) (strctr-tag 0)
				     (strctr-stack (list 0))
				     (strctr-tag-pop 0))
				    ((null alpha-pairlist)
				     ;; Pencil-beam missed patient.
				     ;; Treat as air [density = 0.0].
				     (setq org-density 0.0))
				  (declare (type list alpha-pairlist alpha-item
						 strctr-stack)
					   (type fixnum strctr-tag
						 strctr-tag-pop))
				  (setq alpha-item (car alpha-pairlist)
					strctr-tag (cdr alpha-item)
					strctr-tag-pop (car strctr-stack))

				  (cond
				    ((< (the single-float (car alpha-item))
					zeta)
				     (cond
				       ((= strctr-tag strctr-tag-pop)
					(setq strctr-stack (cdr strctr-stack)))
				       (t (push strctr-tag strctr-stack))))
				    (t (setq org-density
					     (aref organ-density-array
						   strctr-tag-pop))
				       (return))))

				;; For a given tissue density compute
				;; scattering and stopping powers relative
				;; to water.  NOTE that fatal error occurs
				;; if organ density is outside [0.0,2.0].  Data
				;; points from ICRU-21, ICRP-23 and ICRU-35.
				;; Performs piecewise linear interpolation.
				;; BUILD-PATIENT-STRUCTURES range-checked
				;; ORG-DENSITY, and it cannot be negative, but
				;; computation is allowed with value exceeding
				;; positive bound, extrapolating by using
				;; parameter values for highest density..
				(cond
				  ((< org-density 0.0)
				   (error
				     "ELECTRON-DOSE [1] ORG-DENSITY negative: ~S"
				     org-density))

				  ((< org-density 0.33)
				   (setq rstop (+ (* 0.938757576 org-density)
						  0.00121)
					 rscat (+ (* 0.881181818 org-density)
						  0.00121)))

				  ((< org-density 1.0)
				   (setq rstop (- (* 1.028358209 org-density)
						  0.028358209)
					 rscat (- (* 1.056716418 org-density)
						  0.056716418)))

				  ((<= org-density 1.04)
				   (setq rstop (- (* 1.275 org-density) 0.275)
					 rscat org-density))

				  ((<= org-density 1.85)
				   (setq rstop (+ (* 0.77654321 org-density)
						  0.243395062)
					 rscat (- (* 1.962962963 org-density)
						  1.001481481)))

				  (t (setq rstop (- (* 1.5 org-density)
						    1.095)
					   rscat (- (* 1.785714286 org-density)
						    0.673571429))))

				;; Compute effective pathlength based
				;; on stopping power.
				(incf deff (* #.Electron-Step-Size rstop))
				;; Compute energy and feed into SPOWER which
				;; computes scattering power for water, then
				;; scale for inhomogeneity.
				(setq zz (- zeta dtot))
				(incf sigma-rms
				      (* rscat
					 (the single-float
					   (spower deff init-energy rp-val))
					 #.Electron-Step-Size
					 (+ (* zz zz)
					    (* #.Electron-Step-Size zz)
					    #.(/ (* Electron-Step-Size
						    Electron-Step-Size)
						 3.0)))))))))))))

	;; If calc point is within 6 SPATIAL-SPREAD [RMS units] ...  Distance
	;; squared being compared to threshold squared.  Predicate <= changed
	;; to < comparison to exclude case of SPATIAL-SPREAD = 0.0, causing
	;; div-by-zero error in RFLUENCE.
	(when (< (+ (sqr-float (- (* pbcx proj-factor) cx))
		    (sqr-float (- (* pbcy proj-factor) cy)))
		 (* 36.0 spatial-spread spatial-spread))
	  ;; Find slanted pencil ray distance in air between cutout
	  ;; and calc point and accumulate fluence.
	  (incf total-dose
		(* (the single-float
		     (sair nquad quadtiles pbcx pbcy
			   (3d-distance
			     (pbeam-xp
			       (the (simple-array single-float (7)) pb-obj))
			     (pbeam-yp
			       (the (simple-array single-float (7)) pb-obj))
			     (pbeam-zp
			       (the (simple-array single-float (7)) pb-obj))
			     projpx projpy projpz)
			   theta-air-val proj-factor erf-table))
		   (the single-float
		     (rfluence
		       pbcx pbcy
		       (pbeam-wt (the (simple-array single-float (7)) pb-obj))
		       cx cy spatial-spread proj-factor erf-table))
		   (the single-float (aref flu2dose z-index))
		   (/ (sqr-float (+ v-ssd zeff))
		      (sqr-float (- v-ssd cz2)))))))))

  ;;------------------------------------------------------------
  ;; If calc pt is beyond the depth of Rp
  ;; -AND- within the unexpanded geometric field boundary
  ;; -AND- if integrated electron/photon dose is less than gamma
  ;;   tail dose, use gamma tail dose instead.
  ;;------------------------------------------------------------
  (let ((scale-factor (/ #.Pathlength-Ray-Maxlength
			 (setf (aref arg-vec #.Argv-Raylen)
			       (3d-distance v-spx v-spy v-spz px py pz))))
	(effect-depth 0.0) (ray-alphalist))

    (declare (type single-float scale-factor effect-depth)
	     (type list ray-alphalist))

    ;; Source coordinates were loaded before call to ELECTRON-DOSE.
    (setf (aref arg-vec #.Argv-Dp-X) (+ v-spx (* scale-factor (- px v-spx))))
    (setf (aref arg-vec #.Argv-Dp-Y) (+ v-spy (* scale-factor (- py v-spy))))
    (setf (aref arg-vec #.Argv-Dp-Z) (+ v-spz (* scale-factor (- pz v-spz))))

    ;; RAY-ALPHALIST must be CONSP in order to integrate, and
    ;; PATHLENGTH-INTEGRATE returns T to indicate dosepoint-in-body.
    (cond
      ((and (consp (setq ray-alphalist
			 (pathlength-raytrace arg-vec organ-vertices-list
					      organ-z-extents)))
	    (pathlength-integrate arg-vec ray-alphalist
				  organ-density-array :Heterogeneous))

       (when (> (setq effect-depth (aref arg-vec #.Argv-Return-1)) rp-val)
	 (setf (aref arg-vec #.Argv-Enc-X) (/ cx proj-factor))
	 (setf (aref arg-vec #.Argv-Enc-Y) (/ cy proj-factor))
	 (when (encloses? cutout-list arg-vec)
	   (let ((flu2dose-index
		   (the fixnum
		     (round (the single-float
			      (* effect-depth #.(/ 1.0 Electron-Step-Size))))))
		 (depth-lim (array-total-size flu2dose)))
	     (declare (type fixnum flu2dose-index depth-lim))
	     (unless (< flu2dose-index depth-lim)
	       (setq flu2dose-index (the fixnum (1- depth-lim))))
	     (let ((photon-dose (aref flu2dose flu2dose-index)))
	       (declare (type single-float photon-dose))
	       (when (> photon-dose total-dose)
		 (setq total-dose photon-dose)))))))

      (t (setq total-dose 0.0))))

  total-dose)

;;;=============================================================
;;; FIND-EQUIV-RECT: find equivalent rectangle
;;;=============================================================

(defun find-equiv-rect (vlist)

  (declare (type list vlist))

  (let ((w1 0.0)
	(w2 0.0)
	(box-area 0.0)
	(new-area 0.0)
	(blist (poly:bounding-box vlist))
	(blist-1) (blist-1-1 0.0) (blist-1-2 0.0)
	(blist-2) (blist-2-1 0.0) (blist-2-2 0.0))

    (declare (type list blist blist-1 blist-2)
	     (type single-float w1 w2 box-area new-area blist-1-1
		   blist-1-2 blist-2-1 blist-2-2))

    (setq blist-1 (first blist)
	  blist-1-1 (first blist-1)
	  blist-1-2 (second blist-1)
	  blist-2 (second blist)
	  blist-2-1 (first blist-2)
	  blist-2-2 (second blist-2))

    ;; Find the area of initial bounding-box.
    (setq box-area (* (- blist-1-1 blist-2-1)
		      (- blist-1-2 blist-2-2)))
    (setq w1 (the single-float (abs (- blist-1-1 blist-2-1)))
	  w2 (the single-float (abs (- blist-1-2 blist-2-2))))

    ;; Rotate the contour to minimize the area of the bounding-box.
    (do ((angle 1.0 (+ angle 1.0))
	 (w1tmp 0.0)
	 (w2tmp 0.0))
	((> angle 180.0))
      (declare (type single-float angle w1tmp w2tmp))
      (setq blist (poly:bounding-box (poly:rotate-vertices vlist angle))
	    blist-1 (first blist)
	    blist-1-1 (first blist-1)
	    blist-1-2 (second blist-1)
	    blist-2 (second blist)
	    blist-2-1 (first blist-2)
	    blist-2-2 (second blist-2))
      (setq w1tmp (- blist-1-1 blist-2-1))
      (setq w2tmp (- blist-1-2 blist-2-2))
      (setq new-area (the single-float (abs (* w1tmp w2tmp))))
      (when (< new-area box-area)
	(setq box-area new-area
	      w1 (the single-float (abs w1tmp))
	      w2 (the single-float (abs w2tmp)))))

    ;; Estimate the equivalent rectangle as follows:
    ;;   (1) len = the length of the bounding box
    ;;   (2) wid = (area of electron field) / len
    (let ((electron-field-area (poly:area-of-polygon vlist))
	  (len (max w1 w2)))
      (declare (type single-float electron-field-area len))
      (values (/ electron-field-area len) len))))

;;;-------------------------------------------------------------
;;; SAIR: Find integrated fluence along the pencil beam axis
;;;-------------------------------------------------------------
;;; Input: Nq = number of quadtree tiles
;;;        pbcx = pencil beam collimator X coord
;;;        pbcy = pencil beam collimator Y coord
;;;        Zd = distance between the pencil-beam source at the
;;;             coutout plane and calc point
;;;        theta-Air-val = in-air spatial spread parameter
;;;        proj-factor = projection factor
;;;        erf-table = error function table
;;;-------------------------------------------------------------

(defun sair (nquad quadtiles pbcx pbcy zd theta-air-val proj-factor erf-table)

  "sair nquad quadtiles pbcx pbcy zd theta-air-val proj-factor erf-table

returns integrated fluence along the pencil beam axis (the inner sum)
input: nquad = number of quadtree tiles
       quadtiles = array of quadtree tiles
       pbcx = pencil beam collimator X coord
       pbcy = pencil beam collimator Y coord
       zd = distance between the pencil-beam source at the
	    cutout plane and calc point
       theta-air-val = in-air spatial spread parameter
       proj-factor = projection factor."

  (declare (type single-float pbcx pbcy zd theta-air-val proj-factor)
	   (type (simple-array t 1) quadtiles)
	   (type (simple-array single-float (#.Erf-Table-Size)) erf-table)
	   (type fixnum nquad))

  (let ((sigma-air (* theta-air-val zd))
	(tile-size 0.0)
	(x-pos 0.0)
	(y-pos 0.0)
	(a 0.0)
	(b 0.0)
	(c 0.0)
	(d 0.0)
	(accum 0.0))

    (declare (type single-float sigma-air tile-size x-pos y-pos a b c d accum))

    (do ((idx 0 (the fixnum (1+ idx))))
	((= idx nquad)
	 (* 0.25 accum))
      (declare (type fixnum idx))
      (let ((tile-obj (aref quadtiles idx)))
	(declare (type (simple-array single-float (3)) tile-obj))
	(setq tile-size (tile-dimension tile-obj)
	      x-pos (tile-xpos tile-obj)
	      y-pos (tile-ypos tile-obj)))

      (setq a (/ (* proj-factor (- (+ x-pos tile-size) pbcx)) sigma-air)
	    b (/ (* proj-factor (- (- x-pos tile-size) pbcx)) sigma-air)
	    c (/ (* proj-factor (- (+ y-pos tile-size) pbcy)) sigma-air)
	    d (/ (* proj-factor (- (- y-pos tile-size) pbcy)) sigma-air))

      (incf accum
	    (the single-float
	      (* (- (the single-float (error-function a erf-table))
		    (the single-float (error-function b erf-table)))
		 (- (the single-float (error-function c erf-table))
		    (the single-float (error-function d erf-table)))))))))

;;;-------------------------------------------------------------
;;; SAIR-RECT: A fast version of SAIR for rectangular field
;;;-------------------------------------------------------------
;;; Input: w1half = half width
;;;        w2half = half height
;;;        pbcx, pbcy = pencil beam axis coordinates in collimator system
;;;        Zd = distance between the pencil-beam source at the
;;;             coutout plane and calc point
;;;        theta-Air-val = in-air spatial spread parameter
;;;        proj-factor = projection factor
;;;        erf-table = error function table
;;;-------------------------------------------------------------

(defun sair-rect (w1half w2half pbcx pbcy zd theta-air-val
		  proj-factor erf-table)

  "sair-rect w1half w2half pbcx pbcy zd theta-air-val proj-factor erf-table

A fast version of SAIR for rectangular field:

input: w1half = half-width
       w2half = half-height
       pbcx, pbcy = pencil beam axis coordinates in collimator system
       zd = distance between the pencil-beam source at the
	    cutout plane and calc point
       theta-air-val = in-air spatial spread parameter
       proj-factor = projection factor"

  (declare (type (simple-array single-float (#.Erf-Table-Size)) erf-table)
	   (type single-float w1half w2half pbcx pbcy zd
		 theta-air-val proj-factor))

  (let* ((sigma-air (/ proj-factor (* theta-air-val zd)))
	 (a (* sigma-air (- w1half pbcx)))
	 (b (* sigma-air (- (* -1.0 w1half) pbcx)))
	 (c (* sigma-air (- w2half pbcy)))
	 (d (* sigma-air (- (* -1.0 w2half) pbcy))))

    (declare (type single-float sigma-air a b c d))

    (* 0.25
       (- (the single-float (error-function a erf-table))
	  (the single-float (error-function b erf-table)))
       (- (the single-float (error-function c erf-table))
	  (the single-float (error-function d erf-table))))))

;;;-------------------------------------------------------------
;;; RFLUENCE: calculate relative fluecne at calc point at a lateral
;;;           separation (x-x', y-y') from the pencil-beam axis
;;;
;;;     (pbcx,pbcy) = calc point coordinates in Collimator coordinates
;;;     pbwt = pencil-beam weight factor
;;;     spatial-spread = spatial spread parameter (sigma-RMS)
;;;     proj-factor = projection scaling factor
;;;     erf-table = error function table
;;;-------------------------------------------------------------

(defun rfluence (pbcx pbcy pbwt cx cy spatial-spread proj-factor erf-table)

  "rfluence pbcx pbcy pbwt cx cy spatial-spread proj-factor erf-table

calculates and returns relative fluence at calc point at a lateral
separation (x-x', y-y') from the pencil-beam axis

     (pbcx,pbcy) = calc point coordinates in Collimator system
     pbwt = pencil-beam weight factor
     spatial-spread = spatial spread parameter (sigma-rms)
     proj-factor = projection scaling factor"

  (declare (type single-float pbcx pbcy pbwt cx cy spatial-spread proj-factor)
	   (type (simple-array single-float (#.Erf-Table-Size)) erf-table))

  (let* ((sigma2 (* #.(sqrt 2.0) spatial-spread))
	 (a (/ (- (* (+ pbcx #.(* 0.5 Pen-Bm-Width)) proj-factor) cx) sigma2))
	 (b (/ (- (* (- pbcx #.(* 0.5 Pen-Bm-Width)) proj-factor) cx) sigma2))
	 (c (/ (- (* (+ pbcy #.(* 0.5 Pen-Bm-Width)) proj-factor) cy) sigma2))
	 (d (/ (- (* (- pbcy #.(* 0.5 Pen-Bm-Width)) proj-factor) cy) sigma2)))

    (declare (type single-float sigma2 a b c d))

    (* 0.25
       pbwt
       (- (the single-float (error-function a erf-table))
	  (the single-float (error-function b erf-table)))
       (- (the single-float (error-function c erf-table))
	  (the single-float (error-function d erf-table))))))

;;;-------------------------------------------------------------
;;; QUANTIZE-EFIELD: quantize electron field into pencil beams
;;;-------------------------------------------------------------
;;; input: cvlist      - cutout vertices list
;;;        appl-size   - square applicator dimension in cm
;;;        z-pos       - z coordinate for collimator plane (in CC)
;;;        arg-vec     - argument vector for use by ENCLOSES?
;;; ouput: pen-num     - total number of pencil beams
;;;        pbeam-array - array of pencil-beam objects
;;;-------------------------------------------------------------

(defun quantize-efield (cvlist appl-size z-pos arg-vec)

  "quantize-efield cvlist appl-size z-pos arg-vec

quantize electron field into pencil beams, returns total number of pencil
beams from cvlist: cutout vertices list, appl-size: square applicator
dimension in cm, z-pos: z coordinate for collimator plane (in CC)."

  (declare (type list cvlist)
	   (type single-float appl-size z-pos)
	   (type (simple-array single-float (#.Argv-Size)) arg-vec))

  ;; Scan limits in mm - scanning should start at a point such that
  ;; we hit the central axis.
  (let* ((ulim-fix (the fixnum
		     (1+ (the fixnum
			   (round (the single-float
				    (/ appl-size #.(* 2.0 Pen-Bm-Width))))))))
	 (ulim-flo (coerce ulim-fix 'single-float))
	 (llim-flo (- ulim-flo))
	 ;; Estimate number of pencil beams and allocate global array.
	 (pbeam-array (make-array (sqr-fix (* ulim-fix 2))
				  :element-type t :initial-element :EOF))
	 (pen-num 0))

    (declare (type (simple-array t 1) pbeam-array)
	     (type single-float ulim-flo llim-flo)
	     (type fixnum ulim-fix pen-num))

    ;; Count the number of pencil beams within the efield.
    (do ((y-val llim-flo (the single-float (1+ y-val))))
	((> y-val ulim-flo))
      (declare (type single-float y-val))
      (do ((x-val llim-flo (the single-float (1+ x-val)))
	   (x-res 0.0)
	   (y-res (* y-val #.Pen-Bm-Width)))
	  ((> x-val ulim-flo))
	(declare (type single-float x-val x-res y-res))
	(setq x-res (* x-val #.Pen-Bm-Width))
	(setf (aref arg-vec #.Argv-Enc-X) x-res)
	(setf (aref arg-vec #.Argv-Enc-Y) y-res)

	(when (encloses? cvlist arg-vec)
	  (setf (aref pbeam-array pen-num) (make-pbeam 1.0 x-res y-res z-pos))
	  (setq pen-num (the fixnum (1+ pen-num))))))

    (values pen-num pbeam-array)))

;;;-------------------------------------------------------------
;;; QUANTIZE-EXPFIELD: quantize expanded electron field into pencil
;;;                    beams with varying weights
;;;-------------------------------------------------------------
;;; input: cvlist          - cutout vertices list
;;;        appl-size       - expanded square applicator dimension in cm
;;;        z-pos           - z coordinate for collimator plane (in CC)
;;;        arg-vec         - argument vector for use by ENCLOSES?
;;; ouput: (1) pen-num     - total number of pencil beams (doubled)
;;;        (2) pbeam-array - array of pencil-beam objects and raytrace-lists
;;;        (3) expanded CVLIST by ??-mm orthogonally
;;;-------------------------------------------------------------

(defun quantize-expfield (cvlist appl-size z-pos arg-vec)

  "quantize-expfield cvlist appl-size z-pos arg-vec

quantize expanded electron field into pencil beams with varying weights.

input:  cvlist    - cutout vertices list
	appl-size - expanded square applicator dimension in cm
	z-pos     - z coordinate for collimator plane (in CC)

outputs: three values, pen-num     - total number of pencil beams
		       pbeam-array - array of pencil beams and raytrace-lists
		       CVLIST expanded by ??-mm orthogonally"

  (declare (type list cvlist)
	   (type single-float appl-size z-pos)
	   (type (simple-array single-float (#.Argv-Size)) arg-vec))

  ;; Scan limits in mm - scanning should start at a point such that we
  ;; hit the central axis - add 0.1-cm margin to expanded field.
  (let* ((ulim-fix (the fixnum
		     (1+ (the fixnum
			   (round (the single-float
				    (/ (+ appl-size 0.2)
				       #.(* 2.0 Pen-Bm-Width))))))))
	 (ulim-flo (coerce ulim-fix 'single-float))
	 (llim-flo (- ulim-flo))
	 ;; Estimate number of pencil beams and allocate array.
	 (pbeam-array (make-array (* (the (integer 0 100000)
				       (sqr-fix (* ulim-fix 2)))
				     2)
				  :element-type t :initial-element :EOF))
	 (pen-num 0)
	 ;; Expand field in 0.1-cm increments.
	 (explist1 (poly:ortho-expand-contour cvlist 0.1))
	 (explist2 (poly:ortho-expand-contour cvlist 0.2))
	 (explist3 (poly:ortho-expand-contour cvlist 0.3))
	 (explist4 (poly:ortho-expand-contour cvlist 0.4))
	 ;; Contract field in 0.1-cm increments.
	 (cntrlist1 (poly:ortho-expand-contour cvlist -0.1))
	 (cntrlist2 (poly:ortho-expand-contour cvlist -0.2))
	 (cntrlist3 (poly:ortho-expand-contour cvlist -0.3))
	 (cntrlist4 (poly:ortho-expand-contour cvlist -0.4)))

    (declare (type (simple-array t 1) pbeam-array)
	     (type list explist1 explist2 explist3 explist4
		   cntrlist1 cntrlist2 cntrlist3 cntrlist4)
	     (type single-float ulim-flo llim-flo)
	     (type fixnum ulim-fix pen-num))

    ;; Count the number of pencil beams within the efield.
    (do ((y-val llim-flo (the single-float (1+ y-val))))
	((> y-val ulim-flo))
      (declare (type single-float y-val))
      (do ((x-val llim-flo (the single-float (1+ x-val)))
	   (x-res 0.0)
	   (y-res (* y-val #.Pen-Bm-Width))
	   (encl-exp3?) (encl-exp2?) (encl-exp1?) (encl-cvl?)
	   (encl-cntr1?) (encl-cntr2?) (encl-cntr3?) (encl-cntr4?))
	  ((> x-val ulim-flo))

	(declare (type (member nil t) encl-exp3? encl-exp2? encl-exp1?
		       encl-cvl? encl-cntr1? encl-cntr2? encl-cntr3?
		       encl-cntr4?)
		 (type single-float x-val x-res y-res))

	(setq x-res (* x-val #.Pen-Bm-Width))
	(setf (aref arg-vec #.Argv-Enc-X) x-res)
	(setf (aref arg-vec #.Argv-Enc-Y) y-res)
	(setq encl-exp3? (encloses? explist3 arg-vec)
	      encl-exp2? (encloses? explist2 arg-vec)
	      encl-exp1? (encloses? explist1 arg-vec)
	      encl-cvl? (encloses? cvlist arg-vec)
	      encl-cntr1? (encloses? cntrlist1 arg-vec)
	      encl-cntr2? (encloses? cntrlist2 arg-vec)
	      encl-cntr3? (encloses? cntrlist3 arg-vec)
	      encl-cntr4? (encloses? cntrlist4 arg-vec))

	(cond
	  ((and (encloses? explist4 arg-vec)
		(not encl-exp3?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.1 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-exp3? (not encl-exp2?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.2 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-exp2? (not encl-exp1?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.3 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-exp1? (not encl-cvl?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.5 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-cvl? (not encl-cntr1?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.5 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-cntr1? (not encl-cntr2?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.7 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-cntr2? (not encl-cntr3?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.8 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  ((and encl-cntr3? (not encl-cntr4?))
	   (setf (aref pbeam-array pen-num) (make-pbeam 0.9 x-res y-res z-pos))
	   (setq pen-num (the fixnum (+ pen-num 2))))
	  (encl-cntr4?
	    (setf (aref pbeam-array pen-num)
		  (make-pbeam 1.0 x-res y-res z-pos))
	    (setq pen-num (the fixnum (+ pen-num 2)))))))

    (values pen-num pbeam-array explist4)))

;;;-------------------------------------------------------------
;;; SPOWER: Computes mass scattering power in water for a given mean
;;;         electron energy according to the ICRU-35 Table 2.6.
;;; I tried to find a single function to fit the entire latitude of energy.
;;; However, better accuracy was obtained by segmented fitting.
;;;
;;; Note: Energy must be less than 30MeV
;;;
;;; Sanity check:
;;;       energy     calc       ICRU-35
;;;       E=0.04   5.31E+02    5.16E+02
;;;       E=2      1.02E+00    1.03
;;;       E=10     6.92E-02    6.95E-02
;;;       E=20     1.997E-02   2.00E-02
;;;-------------------------------------------------------------

(defun spower (deff init-energy rp-val &aux (mean-energy 0.0))

  "spower deff init-energy rp-val

Computes mean electron energy at an effective depth using Harder's
linear relationship.  Then computes mass scattering power in water
for a given mean electron energy according to the ICRU-35 Table 2.6.
I tried to find a single function to fit the entire latitude of energy.
However, better accuracy was obtained by segmented fitting.
Note: Energy must be less than 30MeV.  Sanity check:
       energy     calc       ICRU-35
       E=0.04   5.31E+02    5.16E+02
       E=2      1.02E+00    1.03
       E=10     6.92E-02    6.95E-02
       E=20     1.997E-02   2.00E-02"

  (declare (type single-float deff init-energy rp-val mean-energy))

  (cond ((<= (setq mean-energy
		   (cond ((<= deff rp-val)
			  (* init-energy (- 1.0 (/ deff (+ rp-val 0.1)))))
			 (t 0.001)))
	     0.0)
	 (error "SPOWER [1] MEAN-ENERGY negative: ~S" mean-energy))

	((<= mean-energy 0.15)
	 (* 2.0332
	    (the single-float
	      (exp (* -1.7288
		      (the single-float
			(log (the (single-float 0.0 *) mean-energy))))))))

	((<= mean-energy 3.0)
	 (* 2.9521
	    (the single-float
	      (exp (* -1.5349
		      (the single-float
			(log (the (single-float 0.0 *) mean-energy))))))))

	((<= mean-energy 15.0)
	 (* 3.6654
	    (the single-float
	      (exp (* -1.7241
		      (the single-float
			(log (the (single-float 0.0 *) mean-energy))))))))

	((<= mean-energy 30.0)
	 (* 4.6735
	    (the single-float
	      (exp (* -1.821
		      (the single-float
			(log (the (single-float 0.0 *) mean-energy))))))))

	(t (error "SPOWER [2] MEAN-ENERGY out of range: ~S" mean-energy))))

;;;-------------------------------------------------------------
;;; GET-SPATIAL-SPREAD-VECTOR:
;;;    computes spatial spread parameter for density 1.0
;;;-------------------------------------------------------------
;;;  init-energy = initial energy of the beam (not nominal E)
;;;  Rp-Val = practical range of the beam
;;;  Computes for depth at steps of Electron-Step-Size from 0.0 to Rp-Val.
;;;-------------------------------------------------------------

(defun get-spatial-spread-vector (init-energy rp-val)

  "get-spatial-spread-vector init-energy rp-val

returns an array of spatial spread parameter values as a function of
depth, for unit density.
init-energy = initial energy of the beam (not nominal E)
rp-val = practical range of the beam"

  (declare (type single-float init-energy rp-val))

  (let ((spatial-spread-vector
	  (make-array
	    (1+ (the (integer 0 1000000)
		  (round (the single-float
			   (* #.(/ 1.0 Electron-Step-Size) rp-val)))))
	    :element-type 'single-float :initial-element 0.0)))

    (declare (type (simple-array single-float 1) spatial-spread-vector))

    ;; SPATIAL-SPREAD between 0 and 0.5 mm.
    (setf (aref spatial-spread-vector 0)           ; !!! depth is 0.05 cm here
	  (* #.(* 0.5 Electron-Step-Size
		  0.5 Electron-Step-Size
		  0.5 Electron-Step-Size)
	     (the single-float
	       (spower #.(* 0.5 Electron-Step-Size) init-energy rp-val))))

    ;; SPATIAL-SPREAD for other depths down to [and including] Rp-Val.
    (do ((idx 1 (the fixnum (1+ idx)))
	 (unitdepth #.Electron-Step-Size (+ unitdepth #.Electron-Step-Size)))
	((> unitdepth rp-val)
	 spatial-spread-vector)

      (declare (type single-float unitdepth)
	       (type fixnum idx))

      ;; Integrate along path - pencil-beam axis.
      (do ((zeta #.Electron-Step-Size (+ zeta #.Electron-Step-Size))
	   (diff 0.0)
	   (sigma-rms 0.0))
	  ((> zeta unitdepth)
	   (setf (aref spatial-spread-vector idx)
		 (the (single-float 0.0 *) (sqrt sigma-rms))))

	(declare (type single-float diff)
		 (type (single-float 0.0 *) zeta sigma-rms))

	(setq diff (- zeta unitdepth))
	(incf sigma-rms
	      (* #.Electron-Step-Size
		 (+ (* diff diff)
		    (* #.Electron-Step-Size diff)
		    #.(/ (* Electron-Step-Size Electron-Step-Size) 3.0))
		 (the single-float (spower zeta init-energy rp-val))))))))

;;;-------------------------------------------------------------
;;; GET-FMCS: computes spatial spread adjustment factor, FMCS
;;;           results are saved in an array
;;;-------------------------------------------------------------
;;;  F1-val = FMCS at a shallow depth; fmcs is usually greater than 1.0
;;;  F2-val = FMCS near or at Rp; usually less than 1.0
;;;  Z1-val = depth in cm where F1 is specified
;;;  Z2-val = depth in cm where F2 is specified; must be <= Rp
;;;  Rp-Val = practical range in cm
;;;-------------------------------------------------------------

(defun get-fmcs (f1-val f2-val z1-val z2-val rp-val)

  "get-fmcs f1-val f2-val z1-val z2-val rp-val

returns array containing computed spatial spread adjustment factor, fmcs.
  f1-val = fmcs at a shallow depth; fmcs is usually greater than 1.0
  f2-val = fmcs near or at Rp-Val; usually less than 1.0
  z1-val = depth in cm where F1-VAL is specified
  z2-val = depth in cm where F2-VAL is specified; must be <= Rp-Val
  Rp-Val = practical range in cm"

  (declare (type single-float f1-val f2-val z1-val z2-val rp-val))

  (cond ((<= z2-val rp-val)

	 (let* ((a (/ (- f2-val f1-val) (- z2-val z1-val)))
		(b (- f2-val (* a z2-val)))
		(fmcs (make-array
			(1+ (the (integer 0 1000000)
			      (round (the single-float
				       (* #.(/ 1.0 Electron-Step-Size)
					  rp-val)))))
			:element-type 'single-float :initial-element 0.0)))

	   (declare (type single-float a b)
		    (type (simple-array single-float 1) fmcs))

	   (do ((z-val 0.0 (+ z-val #.Electron-Step-Size)))
	       ((> z-val rp-val)
		fmcs)
	     (declare (type single-float z-val))
	     (setf (aref fmcs (the fixnum
				(round (the single-float
					 (* z-val
					    #.(/ 1.0 Electron-Step-Size))))))
		   (+ (* a z-val) b)))))

	(t (error "GET-FMCS [1] FMCS at depth ~S past Rp-val ~S"
		  z2-val rp-val))))

;;;=============================================================
;;; End.
