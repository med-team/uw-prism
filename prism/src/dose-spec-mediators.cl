;;;
;;; dose-spec-mediators
;;;
;;; maintain the relations between inputs to the dose computation
;;; model and the validity of the dose results, invalidating the
;;; latter when the former change.
;;;
;;; 15-Jan-1995 I. Kalet split off from dose-result-mediators.
;;;  9-Jun-1996 I. Kalet add brachy support, clean up a little.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass dose-specification-manager ()

 ((organs ;; :type coll:collection
          :accessor organs
          :initarg :organs
          :documentation "A collection of organs, normally supplied
from the patient at initialization.")

  (grid  :type grid-geometry
         :accessor grid
         :initarg :grid
         :documentation "A dose grid, normally supplied from the 
plan at initialization.")

  (beams ;; :type coll:collection
         :accessor beams
         :initarg :beams
         :documentation "A collection of beams, normally supplied
from the plan at initialization.")

  (seeds ;; :type coll:collection
         :accessor seeds
         :initarg :seeds
         :documentation "A collection of seeds, normally supplied
from the plan at initialization.")

  (line-sources ;; :type coll:collection
                :accessor line-sources
                :initarg :line-sources
                :documentation "A collection of line sources, normally
supplied from the plan at initialization.")

  (points ;; :type coll:collection
             :accessor points
             :initarg :points
             :documentation "A collection of points, normally from the
plan's patient at initialization.")

  )

 (:documentation "This mediator ensures that the the dose results for
radiation sources are invalidated when an event warranting invalidation,
e.g. addition of a new organ contour, occurs.")

 )

;;;--------------------------------------

(defun invalidate-dose-points (dsm &rest other-args)

  "INVALIDATE-DOSE-POINTS dsm &rest other-args

An action function which invalidates the points in the dose results of
all the beams, seeds, and line sources contained in the
dose-specification manager dsm."

  (declare (ignore other-args))
  (dolist (src (coll:elements (beams dsm)))
    (setf (valid-points (result src)) nil))
  (dolist (src (coll:elements (line-sources dsm)))
    (setf (valid-points (result src)) nil))
  (dolist (src (coll:elements (seeds dsm)))
    (setf (valid-points (result src)) nil)))

;;;--------------------------------------

(defun invalidate-dose-grid (dsm &rest other-args)

  "INVALIDATE-DOSE-GRID dsm &rest other-args

An action function which invalidates the grid in the dose results of
all the beams, seeds, and line sources contained in the dose-specification 
manager dsm."

  (declare (ignore other-args))
  (dolist (src (coll:elements (beams dsm)))
    (setf (valid-grid (result src)) nil))
  (dolist (src (coll:elements (line-sources dsm)))
    (setf (valid-grid (result src)) nil))
  (dolist (src (coll:elements (seeds dsm)))
    (setf (valid-grid (result src)) nil)))

;;;--------------------------------------

(defun invalidate-dose (dsm &rest other-args)

  "INVALIDATE-DOSE dsm &rest other-args

An action function which invalidates the dose results of all the
beams, seeds, and line sources contained in the dose-specification manager
dsm."

  (apply #'invalidate-dose-grid dsm other-args)
  (apply #'invalidate-dose-points dsm other-args))

;;;--------------------------------------

(defmethod initialize-instance :after ((dsm dose-specification-manager)
                                        &rest initargs)

  (declare (ignore initargs))

  ;; changes in organ density, contours, insertion of new organs, and
  ;; deletion of old ones all invalidate everything
  (dolist (organ (coll:elements (organs dsm)))
    (ev:add-notify dsm (new-density organ) #'invalidate-dose)
    (ev:add-notify dsm (new-contours organ) #'invalidate-dose))
  (ev:add-notify dsm (coll:inserted (organs dsm))
		 #'(lambda (dm a organ)
		     (ev:add-notify dm (new-density organ)
				    #'invalidate-dose)
		     (ev:add-notify dm (new-contours organ)
				    #'invalidate-dose)
		     (invalidate-dose dm a organ)))
  (ev:add-notify dsm (coll:deleted (organs dsm)) 
		 #'(lambda (dm a organ)
		     (ev:remove-notify dm (new-density organ))
		     (ev:remove-notify dm (new-contours organ))
		     (invalidate-dose dm a organ)))

  ;; changing the dose grid invalidates the grid data in the dose
  ;; results of all sources
  (ev:add-notify dsm (new-coords (grid dsm)) #'invalidate-dose-grid)
  (ev:add-notify dsm (new-voxel-size (grid dsm)) #'invalidate-dose-grid)

  ;; moving a point invalidates all the points in all the sources -
  ;; similarly adding or deleting a point.
  ;; not strictly necessary but simpler and not costly.
  (dolist (pt (coll:elements (points dsm)))
    (ev:add-notify dsm (new-loc pt) #'invalidate-dose-points))
  (ev:add-notify dsm (coll:inserted (points dsm)) 
		 #'(lambda (dm a pt)
		     (ev:add-notify dm (new-loc pt)
				    #'invalidate-dose-points)
		     (invalidate-dose-points dm a pt)))
  (ev:add-notify dsm (coll:deleted (points dsm))
		 #'(lambda (dm a pt)
		     (ev:remove-notify dm (new-loc pt))
		     (invalidate-dose-points dm a pt)))
  )

;;;--------------------------------------

(defmethod destroy ((dsm dose-specification-manager))

  (dolist (organ (coll:elements (organs dsm)))
    (ev:remove-notify dsm (new-density organ))
    (ev:remove-notify dsm (new-contours organ)))

  (ev:remove-notify dsm (coll:inserted (organs dsm)))
  (ev:remove-notify dsm (coll:deleted (organs dsm)))

  (ev:remove-notify dsm (new-coords (grid dsm)))
  (ev:remove-notify dsm (new-voxel-size (grid dsm)))

  (dolist (pt (coll:elements (points dsm)))
    (ev:remove-notify dsm (new-loc pt)))

  (ev:remove-notify dsm (coll:inserted (points dsm)))
  (ev:remove-notify dsm (coll:deleted (points dsm)))
  
  )

;;;--------------------------------------

(defun make-dose-specification-manager (&rest initargs)

  "MAKE-DOSE-SPECIFICATION-MANAGER &rest initargs

Creates and returns an organ dose manager from the supplied keyword 
initialization arguments."

  (apply #'make-instance 'dose-specification-manager initargs))

;;;--------------------------------------
