;;;
;;; dmp-panel
;;;
;;; Dicom-Subsystem GUI - Sub-panel for Dose-Monitoring Points.
;;; Contains functions used in Client only.
;;;
;;; Implements a panel that is created by means of a button on the Dicom panel.
;;; It creates a panel that allows the user to create and modify the list of
;;; dose monitoring points (DMPs) for transfer via Dicom-RT protocol.
;;;
;;; 20-Jan-2004 M Phillips Started work on the panel.
;;; 27-Jan-2004 BobGian: Began integration of new DMP Panel by Mark Phillips
;;;    with rest of Dicom Panel and interface to Dicom SCU.
;;; 19-Feb-2004 BobGian: Introduced uniform naming convention explained
;;;    in file "imrt-segments".
;;; 26-Feb-2004 BobGian: Completed DMP integration.
;;; 27-Feb-2004 BobGian: Made Dicom-Panel operate at pushed event level.
;;; 03-Mar-2004 BobGian: Changed Dicom-Panel back to same event level as
;;;    invoking context so other Prism operations can be run concurrently.
;;; 07-Mar-2004 BobGian: Modified DMP selection and beam addition/deletion
;;;    to handle extra DI-DMP slots [parallel beam/dose lists] consistently.
;;; 10-Mar-2004 BobGian: DI-DMP-PRIOR-CGY and DI-DMP-TOTAL-CGY -> FIXNUM.
;;; 17-Mar-2004 BobGian: Added consistency-checking and display for total-dose
;;;    at current DMP - anything which changes set of beams contributing to the
;;;    DMP results in DI-DMP-TOTAL-CGY reset to NIL and border-color of
;;;    Total-Dose textline and Calc-Dose button being set to RED.  Result of
;;;    a dose calculation sets dose slot to current value and textline/button
;;;    border colors to their initial default values.
;;; 04-Apr-2004 BobGian: Change DI-DMP-TOTAL-CGY to record either computed
;;;    dose or user-textline-typed dose [using DI-DMP-DOSE-TYPE to indicate
;;;    which via value :Computed or :User, respectively].  Latter must be set
;;;    appropriately whenever former is set or reset.
;;; 17-May-2004 BobGian complete process of extending all instances of Original
;;;    and Current Prism beam instances to include Copied beam instance too,
;;;    to provide copy for comparison with Current beam without mutating
;;;    Original beam instance.
;;; 05-Oct-2004 BobGian fixed a few lines to fit within 80 cols.
;;; 12-Oct-2004 BobGian DI-DMP-TREATED-CGY -> DI-DMP-PRIOR-CGY slot name change
;;;     for better consistency with Dicom-RT standard and Elekta documentation.
;;;    DI-DMP-TOTAL-CGY -> DI-DMP-ACCUM-CGY and DI-DMP-TOTAL-CGY.
;;;    PREV-DOSE-TEXTLINE -> PRIOR-DOSE-TEXTLINE in DMP-PANEL class.
;;;    Modify computation of DMP dose to get Total-cGy = Prior-cGy + Accum-cGy
;;;     in "Calculate Total dose" button action.
;;;

(in-package :prism)

;;;=============================================================

(defclass dmp-panel ( )

  ((frame :accessor frame
	  :documentation "Slik frame for this panel.")

   (parent-panel :accessor parent-panel
		 :initarg :parent-panel
		 :documentation "Dicom panel that is the parent of this one.")

   ;; Beam instances here are DICOM beams containing a slot referencing the
   ;; Prism beams composing them.  The Prism beams are Original beam instances
   ;; containing DOSE-RESULT objects.
   (dicom-beam-list :accessor dicom-beam-list
		    :initarg :dicom-beam-list
		    :documentation "List of current Dicom beams.")

   (dicom-dmp-list :accessor dicom-dmp-list
		   :initarg :dicom-dmp-list
		   :documentation "List of [Dicom] dose monitoring points.")

   (dicom-dmp-cnt :type fixnum
		  :accessor dicom-dmp-cnt
		  :initarg :dicom-dmp-cnt
		  :documentation "Instance counter for created DMPs.")

   (add-dicom-beam-button :accessor add-dicom-beam-button
			  :documentation
			  "Button for creating pop-up menu of Dicom beams.")

   (add-dmp-button :accessor add-dmp-button
		   :documentation
		   "Button for creating pop-up menu of points.")

   (dmp-scrollinglist :accessor dmp-scrollinglist
		      :documentation "Scrolling list of [Dicom] DMPs.")

   (dicom-beam-scrollinglist :accessor dicom-beam-scrollinglist
			     :documentation
			     "Scrolling list of Dicom beams for selected DMP.")

   (prior-dose-textline :accessor prior-dose-textline
			:documentation
			"Textline showing previously treated dose.")

   (total-dose-textline :accessor total-dose-textline
			:documentation "Textline showing total dose.")

   (dose-calc-button :accessor dose-calc-button
		     :documentation "Button for calculating dose to a DMP.")

   (del-panel-button :accessor del-panel-button
		     :documentation "Button for deleting the panel.")
   ))

;;;=============================================================
;;; Defconstants for DMP Panel.

(defconstant btn-height 25)
(defconstant btn-width 150)
(defconstant tl-width 200)

;;;=============================================================

(defmethod initialize-instance :after ((ptp dmp-panel) &rest initargs)

  "Initializes the Dose Monitoring Points panel."

  (let* ((dp (parent-panel ptp))
	 (cur-pat (current-patient dp))
	 (point-list (coll:elements (points cur-pat)))
	 (dp-tl-color 'sl:green)                    ; textline border color
	 (dp-bt-color 'sl:cyan)                     ; button border color
	 (frm (apply #'sl:make-frame 450 400
		     :title
		     (format nil "Dose Monitoring Points Panel -- ~A"
			     (name cur-pat))
		     initargs))
	 (frm-win (sl:window frm))
	 (add-dicom-beam-bn (apply #'sl:make-button btn-width btn-height
				   :parent frm-win
				   :ulc-x (+ 20 tl-width) :ulc-y 10
				   :label "Add Beams"
				   :border-color dp-bt-color
				   initargs))
	 (add-dmp-bn (apply #'sl:make-button btn-width btn-height
			    :parent frm-win
			    :ulc-x 10 :ulc-y 10
			    :label "Add DMPs"
			    :border-color dp-bt-color
			    initargs))
	 (d-dmp-sl (apply #'sl:make-radio-scrolling-list
			  tl-width (* 10 btn-height)
			  :parent frm-win
			  :ulc-x 10 :ulc-y 45
			  :border-color dp-bt-color
			  :enable-delete t
			  initargs))
	 (d-bm-sl (apply #'sl:make-scrolling-list tl-width (* 10 btn-height)
			 :parent frm-win
			 :ulc-x (+ 20 tl-width) :ulc-y 45
			 :border-color dp-bt-color
			 :enable-delete t
			 initargs))
	 (prior-dose-tl (apply #'sl:make-textline tl-width btn-height
			       :parent frm-win
			       :ulc-x 10 :ulc-y 325
			       :numeric t
			       :lower-limit 0
			       :upper-limit 100000
			       :label "Previous dose: "
			       :border-color dp-tl-color
			       initargs))
	 (total-dose-tl (apply #'sl:make-textline tl-width btn-height
			       :parent frm-win
			       :ulc-x 10 :ulc-y 355
			       :numeric t
			       :lower-limit 0
			       :upper-limit 100000
			       :label "Total dose: "
			       :border-color dp-tl-color
			       initargs))
	 (dose-calc-bn (apply #'sl:make-button btn-width btn-height
			      :parent frm-win
			      :ulc-x (+ 20 tl-width) :ulc-y 325
			      :label "Calc. DMP dose"
			      :border-color dp-bt-color
			      initargs))
	 (d-dmp-alist '())          ; assoc. list for Dicom DMP scrolling list
	 (d-bm-alist '())           ; assoc. list for Dicom Beams scroll. list
	 (cur-d-dmp nil)                            ; current active Dicom DMP

	 (no-dmp-msg "Please select a DMP first"))

    (declare (type list d-dmp-alist d-bm-alist))

    (setf (frame ptp) frm
	  (del-panel-button ptp) (apply #'sl:make-exit-button
					btn-width btn-height
					:parent frm-win
					:ulc-x (+ 20 tl-width) :ulc-y 355
					:label "Delete Panel"
					:fg-color 'sl:black :bg-color 'sl:red
					:border-color dp-bt-color
					initargs)
	  (add-dmp-button ptp) add-dmp-bn
	  (add-dicom-beam-button ptp) add-dicom-beam-bn
	  (dmp-scrollinglist ptp) d-dmp-sl
	  (dicom-beam-scrollinglist ptp) d-bm-sl
	  (prior-dose-textline ptp) prior-dose-tl
	  (total-dose-textline ptp) total-dose-tl
	  (dose-calc-button ptp) dose-calc-bn)

    ;; Make scrolling list of DMPs.
    (let ((d-dmp-list (dicom-dmp-list ptp)))
      (when (consp d-dmp-list)
	(dolist (d-dmp-obj d-dmp-list)
	  (let ((btn (sl:make-list-button d-dmp-sl (di-dmp-name d-dmp-obj))))
	    (sl:insert-button btn d-dmp-sl)
	    (push (cons btn d-dmp-obj) d-dmp-alist)))))

    ;; Select existing DMP.
    (ev:add-notify ptp (sl:selected d-dmp-sl)
      #'(lambda (ptp d-dmp-sl btn)
	  (declare (ignore ptp d-dmp-sl))
	  (setq d-bm-alist '())
	  (setq cur-d-dmp :Ignore) ;Fake out DELETED event handler for D-BM-SL.
	  (dolist (b-bn (sl:buttons d-bm-sl))       ;Clear Dicom beam list.
	    (sl:delete-button b-bn d-bm-sl))
	  (setq cur-d-dmp (cdr (assoc btn d-dmp-alist :test #'eq)))
	  (dolist (d-bm-obj (di-dmp-dbeams cur-d-dmp))
	    (let ((bn (sl:make-list-button d-bm-sl (di-beam-name d-bm-obj))))
	      (sl:insert-button bn d-bm-sl)
	      (push (cons bn d-bm-obj) d-bm-alist)))
	  ;; Prior-cGy, Accum-cGy, and Total-cGy are stored as FIXNUMs in
	  ;; centiGray.  Elekta DMPs represent doses this way.  They are
	  ;; conveyed via Dicom in Gray with 2-decimal-place precision.
	  (cond ((di-dmp-dose-type cur-d-dmp)       ; Type :Computed or :User
		 (setf (sl:border-color dose-calc-bn) dp-bt-color)
		 (setf (sl:border-color total-dose-tl) dp-tl-color)
		 (setf (sl:info total-dose-tl)
		       (format nil "~D" (di-dmp-total-cGy cur-d-dmp))))
		;; DOSE-TYPE = NIL -> not yet computed/stored - mark borders.
		(t (setf (sl:border-color dose-calc-bn) 'sl:red)
		   (setf (sl:border-color total-dose-tl) 'sl:red)
		   (setf (sl:info total-dose-tl) "")))
	  (setf (sl:info prior-dose-tl)
		(format nil "~D" (di-dmp-prior-cGy cur-d-dmp)))))

    ;; Add-DMP button actions
    (ev:add-notify ptp (sl:button-on add-dmp-bn)
      #'(lambda (ptp a)
	  (declare (ignore a))
	  (let ((sel-item nil)
		(sel-pt nil)
		(new-d-dmp nil))
	    ;; Prompt user for name of DMP and make scrolling list.
	    (when (setq sel-item (sl:popup-scroll-menu
				   (mapcar #'name point-list)
				   150 250 :multiple nil))
	      (setq sel-pt (nth sel-item point-list))
	      ;; Construct new DMP and do what is necessary.
	      (setq new-d-dmp (make-di-dmp
				:name (sl:popup-textline
					(string-trim " " (name sel-pt))
					300
					:label "Name for new DMP: ")
				:point sel-pt
				:counter
				(incf (the fixnum (dicom-dmp-cnt ptp)))
				:prior-cGy 0
				:accum-cGy 0
				:total-cGy 0
				:dose-type nil
				:dbeams nil
				:pdoses nil))
	      (setf (dicom-dmp-list ptp)
		    (nconc (dicom-dmp-list ptp) (list new-d-dmp)))
	      ;; Insert selected point in DMP scrolling list.
	      (let ((btn (sl:make-list-button d-dmp-sl
					      (di-dmp-name new-d-dmp))))
		(sl:insert-button btn d-dmp-sl)
		(push (cons btn new-d-dmp) d-dmp-alist)
		(sl:select-button btn d-dmp-sl))
	      (setq cur-d-dmp new-d-dmp)
	      (setf (sl:border-color dose-calc-bn) 'sl:red)
	      (setf (sl:border-color total-dose-tl) 'sl:red)
	      (setf (sl:info total-dose-tl) "")
	      (setf (sl:info prior-dose-tl) "0")))
	  (setf (sl:on add-dmp-bn) nil)))

    ;; Delete existing DMP
    (ev:add-notify ptp (sl:deleted d-dmp-sl)
      #'(lambda (ptp d-dmp-sl btn)
	  (declare (ignore d-dmp-sl))
	  (setf (dicom-dmp-list ptp)
		(delete (cdr (assoc btn d-dmp-alist :test #'eq))
			(dicom-dmp-list ptp)
			:test #'eq))))

    ;; Add-Dicom-Beam button actions
    (ev:add-notify ptp (sl:button-on add-dicom-beam-bn)
      #'(lambda (ptp a)
	  (declare (ignore a))
	  (cond
	    (cur-d-dmp
	      ;; Selected Dicom beam indices (list)
	      (let ((sel-list (sl:popup-scroll-menu
				(mapcar #'di-beam-name
				    (dicom-beam-list ptp))
				150 250
				:multiple t)))
		;; Add new Dicom beam(s).
		(when (consp sel-list)
		  (dolist (sel sel-list)
		    (let* ((new-d-bm (nth sel (dicom-beam-list ptp)))
			   (btn (sl:make-list-button
				  d-bm-sl (di-beam-name new-d-bm)))
			   (point-idx (position (di-dmp-point cur-d-dmp)
						point-list :test #'eq)))
		      (declare (type fixnum point-idx))
		      (setf (di-dmp-dbeams cur-d-dmp)
			    (nconc (di-dmp-dbeams cur-d-dmp)
				   (list new-d-bm)))
		      ;; Doses here are cGy as SMALL-FLOAT values.
		      (setf (di-dmp-pdoses cur-d-dmp)
			    (nconc (di-dmp-pdoses cur-d-dmp)
				   (list (mapcar
					     #'(lambda (seg-doses)
						 (nth point-idx seg-doses))
					   (di-beam-opbi-doses new-d-bm)))))
		      (sl:insert-button btn d-bm-sl)
		      (push (cons btn new-d-bm) d-bm-alist)))
		  (setf (di-dmp-accum-cGy cur-d-dmp) 0)
		  (setf (di-dmp-total-cGy cur-d-dmp) 0)
		  (setf (di-dmp-dose-type cur-d-dmp) nil)
		  (setf (sl:border-color dose-calc-bn) 'sl:red)
		  (setf (sl:border-color total-dose-tl) 'sl:red)
		  (setf (sl:info total-dose-tl) ""))))
	    (t (sl:acknowledge no-dmp-msg)))
	  (setf (sl:on add-dicom-beam-bn) nil)))

    ;; Delete Dicom beam from current Dicom DMP.
    (ev:add-notify ptp (sl:deleted d-bm-sl)
      #'(lambda (ptp d-bm-sl btn)
	  (declare (ignore ptp d-bm-sl))
	  (cond
	    ((eq cur-d-dmp :Ignore))
	    ((di-dmp-p cur-d-dmp)
	     ;; Doses here are cGy as SMALL-FLOAT values.
	     (do ((d-bmlist (di-dmp-dbeams cur-d-dmp) (cdr d-bmlist))
		  (doselist (di-dmp-pdoses cur-d-dmp) (cdr doselist))
		  (del-d-bm (cdr (assoc btn d-bm-alist :test #'eq)))
		  (filtered-bmlist '())
		  (filtered-doselist '()))
		 ((null d-bmlist)
		  (setf (di-dmp-dbeams cur-d-dmp)
			(nreverse filtered-bmlist))
		  (setf (di-dmp-pdoses cur-d-dmp)
			(nreverse filtered-doselist)))
	       (unless (eq (car d-bmlist) del-d-bm)
		 (push (car d-bmlist) filtered-bmlist)
		 (push (car doselist) filtered-doselist)))
	     (setf (di-dmp-accum-cGy cur-d-dmp) 0)
	     (setf (di-dmp-total-cGy cur-d-dmp) 0)
	     (setf (di-dmp-dose-type cur-d-dmp) nil)
	     (setf (sl:border-color dose-calc-bn) 'sl:red)
	     (setf (sl:border-color total-dose-tl) 'sl:red)
	     (setf (sl:info total-dose-tl) ""))
	    (t (sl:acknowledge no-dmp-msg)))))

    ;; Calculate total dose.
    (ev:add-notify ptp (sl:button-on dose-calc-bn)
      #'(lambda (ptp a)
	  (declare (ignore ptp a))
	  (cond (cur-d-dmp
		  (let ((accum-dose 0.0))
		    (declare (type single-float accum-dose))
		    ;; For the current DMP, iterate over all the Dicom beams
		    ;; contributing to the DMP and all the segments making up
		    ;; each Dicom Beam which so contributes [Accum-cGy].
		    ;; Total-cGy is sum of Prior-cGy and Accum-cGy.
		    ;; Doses here are cGy as SMALL-FLOAT values.
		    (dolist (doselist (di-dmp-pdoses cur-d-dmp))
		      (do ((seg-doses doselist (cdr seg-doses)))
			  ((null seg-doses))
			(incf accum-dose (the single-float (car seg-doses)))))
		    (setf (sl:info total-dose-tl)
			  (format nil "~D"
				  (setf (di-dmp-total-cGy cur-d-dmp)
					(+ (the fixnum
					     (di-dmp-prior-cGy cur-d-dmp))
					   (setf (di-dmp-accum-cGy cur-d-dmp)
						 (round accum-dose)))))))
		  (setf (di-dmp-dose-type cur-d-dmp) :Computed)
		  (setf (sl:border-color total-dose-tl) dp-tl-color)
		  (setf (sl:border-color dose-calc-bn) dp-bt-color))
		(t (sl:acknowledge no-dmp-msg)))
	  (setf (sl:on dose-calc-bn) nil)))

    ;; Enter value for total DMP dose.
    ;; Border color switched by :KEY-PRESS handler for textline.
    ;; Recommended that users use this textline as Read/Only.
    (ev:add-notify ptp (sl:new-info total-dose-tl)
      #'(lambda (ptp new-value info)
	  (declare (ignore ptp new-value))
	  (cond (cur-d-dmp
		  (when info
		    (setf (di-dmp-total-cGy cur-d-dmp)
			  (round (read-from-string info)))
		    (setf (di-dmp-dose-type cur-d-dmp) :User)))
		(t (sl:acknowledge no-dmp-msg)))))

    ;; Enter value for previously treated dose.
    ;; Border color switched by :KEY-PRESS handler for textline.
    ;; Temporary policy - do not use this textline.
    (ev:add-notify ptp (sl:new-info prior-dose-tl)
      #'(lambda (ptp new-value info)
	  (declare (ignore ptp new-value))
	  (cond (cur-d-dmp
		  (when info
		    (setf (di-dmp-prior-cGy cur-d-dmp)
			  (round (read-from-string info)))))
		(t (sl:acknowledge no-dmp-msg)))))))

;;;=============================================================

(defun run-dmp-panel (&rest initargs &aux ptp)

  "run-dmp-panel &rest initargs

Creates a DMP panel with the specified initargs
and runs it in a pushed event level."

  (sl:push-event-level)
  (setq ptp (apply #'make-instance 'dmp-panel initargs))
  (sl:process-events)

  (let ((dp (parent-panel ptp)))
    (setf (dicom-dmp-list dp) (dicom-dmp-list ptp))
    (setf (dicom-dmp-cnt dp) (dicom-dmp-cnt ptp)))
  (setf (dicom-dmp-list ptp) nil)
  (setf (dicom-beam-list ptp) nil)
  ;; Remove event notifications before destroying scrolling lists.
  (dolist (sl (list (dmp-scrollinglist ptp)
		    (dicom-beam-scrollinglist ptp)))
    (setf (sl:selected sl) nil)
    (setf (sl:deselected sl) nil)
    (setf (sl:inserted sl) nil)
    (setf (sl:deleted sl) nil)
    (sl:destroy sl))
  (sl:destroy (del-panel-button ptp))
  (sl:destroy (add-dmp-button ptp))
  (sl:destroy (add-dicom-beam-button ptp))
  (sl:destroy (prior-dose-textline ptp))
  (sl:destroy (total-dose-textline ptp))
  (sl:destroy (dose-calc-button ptp))
  (sl:destroy (frame ptp))

  (sl:pop-event-level))

;;;=============================================================
;;; End.
