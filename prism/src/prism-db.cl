;;;
;;; prism-db
;;;
;;; This code implements the Prism patient and image file system
;;; (database) initial design.  This could be replaced by database
;;; access routines later.
;;;
;;; 25-Sep-1992 I. Kalet created
;;;  1-Oct-1992 I. Kalet fix up filename conventions
;;; 30-Dec-1992 I. Kalet fix error in get-case-data
;;; 07-Jan-1993 I. Kalet change filesystem to database
;;; 18-Jan-1993 I. Kalet add functions for storing case data and
;;; updating patient and case lists.
;;; 16-Feb-1993 I. Kalet add functions for selecting patients and cases
;;;  2-Mar-1993 I. Kalet add protection from missing files or
;;;  directories, get image index item order right, and other fixes.
;;;  5-Mar-1993 I. Kalet provide NEW CASE option in select-case and
;;;  new case for case-id 0 in get-case-data, add get-patient-entry
;;;  4-Aug-1993 I. Kalet just use date-entered in put-case-data. In
;;;  get-case-data use name, hosp. id from patient index in every
;;;  case, old or new.
;;;  6-Aug-1993 I. Kalet In add-patient, no "New patient", create
;;;  entry if not there, otherwise leave it as is.
;;;  5-Nov-1993 J. Unger add some attribute init. to get-case-data.
;;;  implement put-plan-data.
;;;  3-Jan-1994 I. Kalet take out forwarding of patient data to plans.
;;; 14-Feb-1994 I. Kalet fix errors in let forms in add-patient, add-case
;;;  5-May-1994 J. Unger move bulk of add-patient to modify-database-list,
;;;  provide add-patient, edit-patient, & delete-patient functions which
;;;  call it.  Also made add-case call modify-database-list, added
;;;  delete-case and delete-case-file, delete-plan-from-case, and some
;;;  file manipulation functions.
;;; 18-May-1994 I. Kalet change reference to patient comments to first
;;; entry in patient comments list.
;;; 06-Jun-1994 J. Unger add conditional tenuring call to get-image-set,
;;; which will load images directly into oldspace in allegrocl.
;;; 29-Jun-1994 J. Unger make delete-plan-from-case search list of
;;; plans by plan timestamp, instead of plan name.
;;; 11-Oct-1994 J. Unger fix bug in put-plan-data.
;;; 07-Nov-1994 J. Unger ensure that timestamp of a plan written out
;;; from put-plan-data does not update at the time the plan is written.
;;; 27-Jul-1995 I. Kalet eliminate copy-plan in put-plan-data because
;;; there are no back pointers anymore.
;;; 12-Sep-1995 I. Kalet in get-case-data, coerce wedge rotation and
;;;  threshold values to single-float, to allow fast arithmetic.  But
;;;  save and restore the original plan timestamp while doing this.
;;; 22-May-1996 I. Kalet/D. Avitan implement put-image-set.
;;;  9-Oct-1996 I. Kalet use excl:tenuring in any Allegro.
;;;  2-Feb-1997 I. Kalet make default directory for all database
;;;  functions be the corresponding prism global, not the value of
;;;  *default-pathname-defaults*
;;;  7-Mar-1997 I. Kalet add event registrations between beams and
;;;  their collimator and wedge objects, when reading in plan data, in
;;;  get-case-data function.  This cannot be done in the
;;;  initialize-instance method for a beam.
;;; 24-Apr-1997 I. Kalet provide a means for filtering the patient
;;; list by patient name or number.
;;; 06-Jun-1997 BobGian massaged "peek-char" EOF detection to use READ
;;;  function with EOF detection instead - same functionality, cleaner.
;;;  Also changed DELETE-IMAGE-FILES to use lisp's DIRECTORY rather than
;;;  RUN-SUBPROCESS to get list of image files to delete.
;;; 26-Jun-1997 I. Kalet move case init code here from patient panel,
;;; so it is all in one place (only the stuff that cannot be done in
;;; the individual object init methods when data read from file).
;;; 22-Aug-1997 I. Kalet add get-full-case-list analagous to
;;; get-full-image-set-list.  Also, get-irreg-case.
;;; 12-Sep-1997 I. Kalet use get-index-list function in the
;;; file-functions module, in higher level functions here, instead of
;;; replicating file read and write code, since the index files are
;;; all similar.  Make database required, not optional, in all
;;; functions that need it.
;;;  9-Nov-1997 I. Kalet add optional parameter new to select-case,
;;;  defaults to t, which lists option of new case.  Set to nil when
;;;  the new case option should not be included in the menu.
;;; 28-Dec-1997 I. Kalet add select-patient-from-case-list, originally
;;; repeated code in patdb-panels.
;;; 28-Apr-1998 I. Kalet don't set patient case name and hospital ID
;;; here, because need *patient-database* for patient index.
;;;  5-Jun-1998 I. Kalet remove Allegro with-tenuring, as it tenures
;;;  garbage in addition to the images.
;;; 15-Jun-1998 I. Kalet add image set and image index functions for
;;; storing image sets, to use with DRR and DICOM.
;;; 11-May-1999 I. Kalet in select-patient-from-case-list add
;;; notification if the specified database has no information or is
;;; inaccessible.
;;;  2-Jan-2000 I. Kalet fix error in format directives in
;;; select-full-image-set function.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 27-Aug-2000 I. Kalet add byte swap of image data if the image
;;; database indicates a different byte order than the local lisp
;;; system, as specified by global constant, *byte-order*.
;;; 14-Oct-2001 I. Kalet in put-plan-data, first make an exact copy of
;;; the plan, then add it to the temporary patient and store them.
;;; Prevents mediators from creating double contours, etc. in views.
;;; 26-Dec-2001 I. Kalet add new function select-cases, which returns
;;; a list of the selected case entries rather than just one.  Also
;;;add optional search string to select-patient-from-case-list.
;;; 31-Oct-2003 I. Kalet add new function select-full-image-sets,
;;; which returns a list of the selected image set entries rather than
;;; just one.  Allows multiple image set deletion.  Also add new
;;; function select-patients-from-case-list for deleting multiple
;;; patients from checkpoint dir.
;;; 15-Feb-2004 I. Kalet delete IRREG functions, no longer supported
;;;

(in-package :prism)

;;;---------------------------------

(defun get-patient-list (database)

  "get-patient-list database

Returns a list of lists, each one containing data about one patient.
The database parameter is a pathname identifying the directory in
which the patient database is located, so the scheme allows multiple
databases, e.g., one for clinical use and one for test cases.  If the
index is missing or inaccessible the function returns NIL."

  (get-index-list "patient.index" database nil))

;;;---------------------------------

(defun get-patient-entry (patient-id database)

  "get-patient-entry patient-id database

Returns a list containing data about one patient.  The database
parameter is a pathname identifying the directory in which the patient
database is located, so the scheme allows multiple databases, e.g.,
one for clinical use and one for test cases.  If the patient is
not found, or the index is missing or inaccessible the function
returns NIL."

  (first (get-index-list "patient.index" database patient-id
			 :test #'=)))

;;;---------------------------------

(defun select-patient (database &optional (search-key ""))

  "select-patient database &optional search-key

returns a patient id number or NIL, after displaying the patient list
in a popup scrolling list for user selection.  If search-key is
provided it is used to filter the list and show only entries that
match."

  (let ((patlist (get-patient-list database)))
    (if patlist
	(let* ((items
		 (remove nil
			 (mapcar #'(lambda (item)
				     (let ((item-str
					     (format nil
						     "~5@A ~30A ~11A ~A"
						     (first item)
						     (second item)
						     (third item)
						     (fourth item))))
				       (if (search (string-upcase
						     search-key)
						   (string-upcase
						     item-str))
					   item-str)))
			   patlist)))
	       (selection (if items (sl:popup-scroll-menu
				      items 525 400
				      :font (symbol-value *small-font*))
			      (sl:acknowledge "No entries match request"))))
	  (if selection (read-from-string (nth selection items))))
	(sl:acknowledge "Patient index is inaccessible"))))

;;;---------------------------------

(defun modify-database-list (mod-fn filename database)

  "modify-database-list mod-fn filename database

Reads the file specified by filename from the specified database,
executes mod-fn on the list, and writes the modified list back to the
specified database.  Returns T if successful, NIL otherwise.  Mod-fn
takes as a single parameter the database list, and returns a modified
database list."

  (let ((data-list (get-index-list filename database nil)))
    (setq data-list (funcall mod-fn data-list))
    (with-open-file
      (stream (merge-pathnames filename database)
	      :direction :output :if-exists :new-version)
      (when (streamp stream)
	(mapc #'(lambda (entry) (format stream "~S~%" entry))
	  (reverse data-list))
	t))))                                       ; return success

;;;---------------------------------

(defun add-patient (pat-id pat-name hosp-no database)

  "add-patient pat-id pat-name hosp-no database

Adds an entry for pat-id, pat-name, hosp-no to the table of patients
in the patient list for the database specified by database.  Returns T
if successful, NIL otherwise.  If the pat-id was already found on the
patient list, returns T but does not add that patient again."

  (modify-database-list #'(lambda (lst)
			    (if (get-patient-entry pat-id database)
				lst
				(push (list pat-id pat-name
					    hosp-no (date-time-string))
				      lst)))
			"patient.index"
			database))

;;;---------------------------------

(defun delete-patient (pat-id database)

  "delete-patient pat-id database

Deletes the entry specified by pat-id from the table of patients in
the patient list for the database specified by database.  Returns
T if successful, NIL otherwise.  If pat-id was not found on the
patient list, returns T but does not change the database."

  (modify-database-list #'(lambda (lst)
			    (remove pat-id lst :key #'first))
			"patient.index"
			database))

;;;---------------------------------

(defun edit-patient (pat-id pat-name hosp-no database)

  "edit-patient pat-id pat-name hosp-no database

Edits entry specified by pat-id from the table of patients in the
patient list for the database specified by database, replacing
pat-name and hosp-no.  Returns T if successful, NIL otherwise.  If
pat-id was not found on the patient list, returns T but does not
change the database."

  (modify-database-list #'(lambda (lst)
			    (let ((entry (find pat-id lst :key #'first)))
			      (when entry
				(setf (second entry) pat-name
				      (third entry) hosp-no))
			      lst))
			"patient.index"
			database))

;;;---------------------------------

(defun select-patient-from-case-list (patdb casedb
				      &optional (search-key ""))

  "select-patient-from-case-list patdb casedb &optional search-key

returns a patient id number or NIL, after displaying a list of the
patients with entries in the case index of casedb, using the patient
index from patdb in a popup scrolling list for user selection.  This
is useful for retrieving information from case databases that hold
limited sets of cases and no separate patient index, e.g., the user's
checkpoint database.  If search-key is provided it is used to filter
the list and show only entries that match."

  (let* ((entries
	   (remove nil
		   (mapcar #'(lambda (pat)
			       (let* ((item (get-patient-entry pat patdb))
				      (item-str (format nil
							"~5@A ~30A ~11A ~A"
							(first item)
							(second item)
							(third item)
							(fourth item))))
				 (if (search (string-upcase search-key)
					     (string-upcase item-str))
				     item-str)))
		     (remove-duplicates
		       (mapcar #'first (get-full-case-list casedb))
		       :from-end t))))
	 (selection (if entries (sl:popup-scroll-menu
				  entries 525 400
				  :font (symbol-value *small-font*))
			(sl:acknowledge "No entries match request"))))
    (if selection (read-from-string (nth selection entries)))))

;;;---------------------------------

(defun select-patients-from-case-list (patdb casedb
				       &optional (search-key ""))

  "select-patients-from-case-list patdb casedb &optional search-key

returns a list of patient id numbers or NIL, after displaying a list
of the patients with entries in the case index of casedb, using the
patient index from patdb in a popup scrolling list for user selection.
This is useful for retrieving information from case databases that
hold limited sets of cases and no separate patient index, e.g., the
user's checkpoint database.  If search-key is provided it is used to
filter the list and show only entries that match."

  (let* ((entries
	   (sort
	     (remove nil
		     (mapcar #'(lambda (pat)
				 (let* ((item (get-patient-entry pat patdb))
					(item-str (format nil
							  "~5@A ~30A ~11A ~A"
							  (first item)
							  (second item)
							  (third item)
							  (fourth item))))
				   (if (search (string-upcase search-key)
					       (string-upcase item-str))
				       item-str)))
		       (remove-duplicates
			 (mapcar #'first (get-full-case-list casedb))
			 :from-end t)))
	     #'< :key #'read-from-string))
	 (selections (if entries (sl:popup-scroll-menu
				   entries 525 400
				   :font (symbol-value *small-font*)
				   :multiple t)
			 (sl:acknowledge "No entries match request"))))
    (if selections
	(mapcar #'(lambda (sel)
		    (read-from-string (nth sel entries)))
	  selections))))

;;;---------------------------------

(defun get-case-list (patient-id database)

  "get-case-list patient-id database

Returns a list of lists, each one containing data about one patient
case, without the patient id.  Only cases for the patient specified by
patient-id are listed.  This provides for multiple cases or sets of
anatomy for a given patient.  If the case index file is inaccessible
the function returns NIL."

  (nreverse (mapcar #'rest (get-index-list "case.index" database
					   patient-id :test #'=))))

;;;---------------------------------

(defun get-full-case-list (database)

  "get-full-case-list database

Returns a list of lists, each one containing data about one patient
case.  All cases are listed and each includes the patient id.  This is
most useful for checkpoint databases, where there is no patient list.
If the case index file is inaccessible the function returns NIL."

  (nreverse (get-index-list "case.index" database nil)))

;;;---------------------------------

(defun select-case (pat-id database &optional (new t))

  "select-case pat-id database &optional (new t)

returns a case id by displaying a popup scrolling list of case
information for the cases under patient pat-id, and allowing user
selection.  The NEW CASE option is displayed unless new is nil.
Returns 0 for NEW CASE, NIL if no selection."

  (let* ((caselist (if new (cons (list 0 "NEW CASE" "")
				 (get-case-list pat-id database))
		       (get-case-list pat-id database)))
	 (items (mapcar #'(lambda (item)
			    (format nil "~5@A ~40A ~A"
				    (first item) (second item)
				    (third item)))
		  caselist))
	 (selection (sl:popup-scroll-menu items 525 150
					  :font (symbol-value *small-font*))))
    (if selection (first (nth selection caselist)))))

;;;---------------------------------

(defun select-cases (pat-id database)

  "select-case pat-id database

returns a list of case ids by displaying a popup scrolling list of case
information for the cases under patient pat-id, and allowing user
selection.  Returns NIL if no selection."

  (let* ((caselist (get-case-list pat-id database))
	 (items (mapcar #'(lambda (item)
			    (format nil "~5@A ~40A ~A"
				    (first item) (second item)
				    (third item)))
		  caselist))
	 (selections (sl:popup-scroll-menu items 525 150
					   :font (symbol-value *small-font*)
					   :multiple t)))
    (mapcar #'(lambda (sel) (first (nth sel caselist)))
      selections)))

;;;---------------------------------

(defun add-case (pat-id case-id descrip time-stamp database)

  "add-case pat-id case-id descrip time-stamp database

adds the case description record specified by the given parameters to
the list of cases.  Returns T if successful, NIL otherwise."

  (modify-database-list #'(lambda (lst)
			    (push (list pat-id case-id descrip time-stamp)
				  lst))
			"case.index"
			database))

;;;---------------------------------

(defun delete-case (pat-id case-id database)

  "delete-case pat-id case-id database

Deletes the entry specified by pat-id and case-id from the table
of cases in the case list for the database specified by database.
Returns T if successful, NIL otherwise.  If the pat-id/case-id
combination was not found on the case list, returns T but does
not change the database."

  (modify-database-list #'(lambda (lst)
			    (remove (find-if
				      #'(lambda (entry)
					  (and (= pat-id (first entry))
					       (= case-id (second entry))))
				      lst)
				    lst))
			"case.index"
			database))

;;;---------------------------------

(defun delete-case-file (pat-num case-num database)

  "delete-case-file pat-num case-num database

Deletes the files corresponding to the specified patient and case
numbers from the specified database.  Returns T if the file existed
before deletion, NIL if it could not be found."

  (let ((filename (merge-pathnames (format nil "pat-~D.case-~D"
					   pat-num case-num)
				   database)))
    (when (probe-file filename)
      (delete-file filename))))

;;;---------------------------------------

(defun get-case-data (patient-id case-id database)

  "get-case-data patient-id case-id database

Returns the case data for the case specified by patient-id and case-id
in the patient database specified by database.  Also initializes
certain object-valued slots of plans and beams here because they are
read in from the case file, not initialized by default.  This includes
the dose-grid and grid-view-manager slots in each plan, and the
collimator and wedge slots in each beam."

  (let ((pat (if (= case-id 0) (make-instance 'patient)
		 (first (get-all-objects (merge-pathnames
					   (format nil "pat-~D.case-~D"
						   patient-id case-id)
					   database))))))
    (when pat
      (setf (patient-id pat) patient-id            ;; finish init of plans and
	    ;; beams, as in make-plan and make-beam
	    (case-id pat) case-id)
      (dolist (pln (coll:elements (plans pat)))
	;; Save/restore plan time stamp: setting wedge rot. changes it
	(let ((ts (time-stamp pln)))
	  (dolist (bm (coll:elements (beams pln)))
	    (ev:add-notify bm (new-coll-set (collimator bm))
	      #'invalidate-results)
	    (ev:add-notify bm (new-id (wedge bm))
	      #'invalidate-results)
	    (ev:add-notify bm (new-rotation (wedge bm))
	      #'invalidate-results)
	    ;; Make sure old wedge rots from files are single-floats
	    (when (rotation (wedge bm))
	      (setf (rotation (wedge bm))
		    (coerce (rotation (wedge bm)) 'single-float))))
	  (setf (time-stamp pln) ts))
	;; update the plan's timestamp when dose grid changes
	(ev:add-notify pln (new-coords (dose-grid pln))
	  #'(lambda (pl a)
	      (declare (ignore a))
	      (setf (time-stamp pl) (date-time-string))))
	(ev:add-notify pln (new-voxel-size (dose-grid pln))
	  #'(lambda (pl a v)
	      (declare (ignore a v))
	      (setf (time-stamp pl) (date-time-string))))
	;; update ref. to grid and result in dose surfaces, also
	;; some threshold values in the files are not single floats
	(dolist (ds (coll:elements (dose-surfaces pln)))
	  (setf (dose-grid ds) (dose-grid pln)
		(threshold ds) (coerce (threshold ds) 'single-float)
		(result ds) (sum-dose pln)))
	;; and arrange for each new dose surface to get set similarly
	(ev:add-notify pln (coll:inserted (dose-surfaces pln))
	  #'(lambda (pl ann ds)
	      (declare (ignore ann))
	      (setf (dose-grid ds) (dose-grid pl)
		    (result ds) (sum-dose pl))))
	(setf (grid-vm pln) (make-object-view-manager
			      (coll:make-collection (list (dose-grid pln)))
			      (plan-views pln)
			      #'make-grid-view-mediator))))
    pat))

;;;---------------------------------

(defun put-case-data (pat-case database)

  "put-case-data pat-case database

adds the patient case pat-case to the patient database specified by
database.  An entry in the case list is made as well as an entry for
the data.  Returns T if successful, NIL otherwise."

  (let ((pat-id (patient-id pat-case))
	(case-id (case-id pat-case))
	(descrip (first (comments pat-case)))       ; comments is a list
	(time-stamp (date-entered pat-case)))
    (when (equal case-id 0)
      (setq case-id
	    (1+ (apply #'max 0                 ; need at least one number here
		       (mapcar #'first
			   (get-case-list pat-id database)))))
      (setf (case-id pat-case) case-id)
      (put-all-objects (list pat-case)
		       (merge-pathnames
			 (format nil "pat-~D.case-~D" pat-id case-id)
			 database))
      (if (listp descrip) (setq descrip (first descrip)))
      (add-case pat-id case-id descrip time-stamp database))))

;;;---------------------------------

(defun put-plan-data (pat-id case-id plan database)

  "put-plan-data pat-id case-id plan database

Appends the specified plan to the plans for patient id pat-id under
patient case case-id, in the patient database specified by database.
Returns T if successful, NIL otherwise."

  (let ((temp-pat (get-case-data pat-id case-id database))) ;; get case
    (if temp-pat                 ;; if found, add plan to it, and write it out
	(let ((temp-plan (copy plan)))
	  (coll:insert-element temp-plan (plans temp-pat))
	  (put-all-objects (list temp-pat)
			   (merge-pathnames
			     (format nil "pat-~D.case-~D" pat-id case-id)
			     database))
	  t)
	nil)))

;;;---------------------------------

(defun delete-plan-from-case (pat-id case-id plan database)

  "delete-plan-from-case pat-id case-id plan database

Deletes the specified plan from the plans for patient id pat-id under
patient case case-id, in the patient database specified by database.
Returns T if successful, NIL otherwise.  If a plan is not found in
the case's list of plans, T is still returned."

  (let ((temp-pat (get-case-data pat-id case-id database)))
    (when temp-pat
      (coll:delete-element plan (plans temp-pat)
			   :test #'(lambda (a b)
				     (string-equal (time-stamp a)
						   (time-stamp b))))
      (put-all-objects (list temp-pat)
		       (merge-pathnames
			 (format nil "pat-~D.case-~D" pat-id case-id)
			 database))
      t)))

;;;---------------------------------

(defun get-image-set-list (patient-id database)

  "get-image-set-list patient-id database

Returns a list of two element lists each containing an image-id and a
description string, corresponding to patient-id.  Returns NIL if none
available or image index inaccessible."

  (mapcar #'rest (get-index-list "image.index" database
				 patient-id :test #'=)))

;;;---------------------------------

(defun get-full-image-set-list (database)

  "get-full-image-set-list database

Returns a list of three element lists each containing an patient-id,
an image-id, and a description string -- the entire contents of the
specified image database.  Returns NIL if the database is
unavailable."

  (nreverse (get-index-list "image.index" database nil)))

;;;---------------------------------

(defun select-image-set (pat-id database)

  "select-image-set pat-id database

lists image sets available for patient pat-id in the specified
database, and returns NIL for no selection or none available, or the
image-set-id if one is selected."

  (let* ((study-list (get-image-set-list pat-id database))
	 (studies (mapcar #'second study-list))
	 (item (if studies (sl:popup-menu studies)
		   (sl:acknowledge "No image studies available"))))
    (if item (first (nth item study-list)))))

;;;---------------------------------

(defun select-full-image-set (database &rest initargs)

  "select-full-image-set database &rest initargs

lists all available image sets in the specified database, and returns
NIL for no selection or none available, or a (pat-id image-id descrip)
list if one is selected."

  (let* ((study-list (sort (get-full-image-set-list database)
			   #'< :key #'first))
	 (selections (mapcar #'(lambda (sdy)
				 (format nil "~4@A ~4@A ~50A"
					 (first sdy)
					 (second sdy)
					 (third sdy)))
		       study-list))
	 (item (if study-list
		   (apply #'sl:popup-scroll-menu selections
			  525 150 :font (symbol-value *small-font*)
			  initargs)
		   (sl:acknowledge "No image studies available"))))
    (if item (nth item study-list))))

;;;---------------------------------

(defun select-full-image-sets (database &rest initargs)

  "select-full-image-sets database &rest initargs

lists all available image sets in the specified database, and returns
NIL for no selection or none available, or a list of (pat-id image-id
descrip) lists if one or more are selected."

  (let* ((study-list (get-full-image-set-list database))
	 (selections
	   (mapcar #'(lambda (stdy)
		       (format nil "~5@A ~A ~4@A ~50A"
			       (first stdy)
			       (second (get-patient-entry
					 (first stdy) *patient-database*))
			       (second stdy)
			       (third stdy)))
	     study-list))
	 (items (if study-list
		    (apply #'sl:popup-scroll-menu selections
			   525 150 :font (symbol-value *small-font*)
			   :multiple t
			   initargs)
		    (sl:acknowledge "No image studies available"))))
    (if items (mapcar #'(lambda (sel) (nth sel study-list))
		items))))

;;;---------------------------------

(defun add-image-set (pat-id image-set-id descrip database)

  "add-image-set pat-id image-set-id descrip database

adds the image set description record specified by the given
parameters to the list of image sets.  Returns T if successful, NIL
otherwise."

  (modify-database-list #'(lambda (lst)
			    (push (list pat-id image-set-id descrip)
				  lst))
			"image.index"
			database))

;;;-------------------------------------------

(defun byte-swap (binarray)

  "byte-swap binarray

swaps the bytes of each element of binarray, a 2-d array of unsigned
16-bit words.  Used when reading in image data if necessary."

  (let* ((dims (array-dimensions binarray))
	 (xdim (first dims))
	 (ydim (second dims)))
    (declare (type (simple-array (unsigned-byte 16) (* *))
		   binarray)
	     (fixnum xdim ydim))
    (dotimes (i xdim)
      (declare (fixnum i))
      (dotimes (j ydim)
	(declare (fixnum j))
	(let ((val (aref binarray i j)))
	  (declare (type (unsigned-byte 16) val))
	  (setf (aref binarray i j)
		(+ (ash (logand 65280 val) -8)
		   (ash (logand 255 val) 8))))))))

;;;---------------------------------

(defun get-image-set (patient-id image-id database)

  "get-image-set patient-id image-id database

Returns a list of images that are in the specified image set,
corresponding to patient-id and image-id."

  (let ((images (get-all-objects (merge-pathnames
				   (format nil "pat-~D.image-set-~D"
					   patient-id image-id)
				   database)))
	(img-byte-order (aif (probe-file (merge-pathnames
					   "image.config" database))
			     (with-open-file (config it)
			       (read config))
			     *byte-order*)))
    (unless (eq *byte-order* img-byte-order)
      (format t "Swapping bytes in image set...~%")
      (dolist (img images)
	(format t "Swapping bytes in image ~A~%" (id img))
	(byte-swap (pixels img))))
    images))

;;;---------------------------------

(defun put-image-set (patient-id image-set database)

  "put-image-set patient-id image-set database

adds the specified image set to the image sets for patient id pat-id,
in the image database specified by database.  If successful the
function returns T.  If the database does not exist, the function
returns NIL."

  (let ((image-set-id (1+ (apply #'max 0       ; need at least one number here
				 (mapcar #'first
				     (get-image-set-list
				       patient-id database))))))
    (dolist (im image-set)
      (setf (patient-id im) patient-id
	    (image-set-id im) image-set-id))
    (put-all-objects image-set (merge-pathnames
				 (format nil "pat-~D.image-set-~D"
					 patient-id image-set-id)
				 database))
    (add-image-set patient-id image-set-id
		   (description (first image-set))
		   database)))

;;;---------------------------------

(defun delete-image-set (pat-id img-set-id database)

  "delete-image-set pat-id img-set-id database

Deletes the entry specified by pat-id & img-set-id from the table of
images in the image list for the database specified by database.
Returns T if successful, NIL otherwise.  If the the pat-id/img-set-id
combination was not found on the image list, returns T but does not
change the database."

  (modify-database-list
    #'(lambda (lst)
	(remove (find-if
		  #'(lambda (entry)
		      (and (= pat-id (first entry))
			   (= img-set-id (second entry))))
		  lst)
		lst))
    "image.index"
    database))

;;;---------------------------------

(defun delete-image-files (pat-num img-num database)

  "delete-image-files pat-num img-num database

Deletes the image files (and image-set file) corresponding to the
specified patient and image numbers from the specified database.  If
the image-set file existed before, deletes image files (if any) and
returns T; if not, deletes nothing and returns NIL."

  ;; for now, delete all pat-i.image-j-k files for all k's, even if
  ;; there are some k's that are not specified in the image-set file.

  (let ((image-set-filename
	  (merge-pathnames
	    (format nil "pat-~D.image-set-~D" pat-num img-num)
	    database)))
    (when (probe-file image-set-filename)
      (delete-file image-set-filename)
      (dolist (image-file
		(directory
		  (merge-pathnames
		    (format nil "pat-~D.image-~D-*" pat-num img-num)
		    database)))
	(delete-file image-file))
      t)))                ;; return T if image-set file existed, otherwise NIL

;;;---------------------------------
;;; End.
