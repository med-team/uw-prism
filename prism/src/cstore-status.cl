;;;
;;; cstore-status
;;;
;;; Dictionary of Elekta CSTORE status codes from Table 17 in DICOM Conformance
;;; Statement for Elekta Precise Treatment System Release 2.01 24 March 2000.
;;; Contains data used in Client only.
;;;
;;;  1-Dec-2000  J. Jacky  Copied from Elekta conformance statement 2.01
;;; 26-Mar-2001  J. Jacky  Add a few more codes from 3.00
;;; 23-Jan-2002   BobGian  Move to Prism package and :Prism system.
;;; 04-Nov-2004   BobGian  *CSTORE-STATUS-ALIST* -> *STATUS-ALIST* in
;;;                        preparation for adding codes for more operations.

(in-package :Prism)

;;;=============================================================

(defparameter *status-alist*

  '((#x0000 . "Success")

    ;; Refused

    ;; #xA7XX  Out of resources

    (#xA701 . "Patient locked")
    (#xA702 . "Feature not licensed")

    ;; Error

    ;; #xA9XX  Data set does not match SOP class

    (#xA901 . "Invalid Dicom message")
    (#xA902 . "Invalid Beam Sequence")
    (#xA903 . "Invalid Dose Reference Sequence")
    (#xA904 . "Invalid Tolerance Table Sequence")
    (#xA905 . "Invalid Patient Setup Sequence")
    (#xA906 . "Invalid Fraction Group Sequence")

    ;; #xCXXX Cannot Understand

    (#xC001 . "Missing Patient Identification data")
    (#xC002 . "Inconsistent Patient data")
    (#xC003 . "Missing Treatment Machine Name")
    (#xC004 . "Unrecognized Linac")
    (#xC005 . "Invalid Linac Energy or Radiation Type")
    (#xC006 . "Invalid Beam Limiting Device")
    (#xC007 . "Incomplete Beam Limiting Device combination")
    (#xC008 . "Unrecognized Block Tray ID")
    (#xC009 . "Inconsistent Block Tray ID")
    (#xC00A . "Unsupported Dosimeter Unit")
    (#xC00B . "Unsupported Wedge")
    (#xC00C . "Under-specified Wedge Position Sequence")
    (#xC00D . "Applicator specified with X-rays")
    (#xC00E . "Unsupported Applicator")
    (#xC00F . "MLC shape specified with Electrons")
    (#xC010 . "Geometric parameter out of customized range")
    (#xC011 . "Unsupported machine movements")
    (#xC012 . "Beam too complex")
    (#xC013 . "Missing Cumulative Meterset Weight")
    (#xC014 . "Segment Meterset too small")
    (#xC015 . "Plan contains Brachy data")
    (#xC016 . "Unsupported Treatment Delivery Type")
    (#xC017 . "Unsupported Fraction Dosimetry")
    (#xC018 . "Inconsistent Tolerance Table data")
    (#xC019 . "Invalid MLC shape or Leaf Positions")
    (#xC01A . "Under-specified Energy changes")

    ;; Warning

    (#xB000 . "Coercion of Data Elements")
    (#xB007 . "Data Set does not match SOP Class")
    (#xB006 . "Elements Discarded")

    ))

;;;=============================================================
;;; End.
