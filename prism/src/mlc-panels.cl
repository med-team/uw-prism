;;;
;;; mlc-panels
;;;
;;; The combined Prism multileaf collimator portal and leaf editing
;;; panel and associated functions.  Originally leaf-panels.
;;;
;;; 21-Jul-1994 J. Unger implement from spec.
;;; 02-Aug-1994 J. Unger add code to accommodate var jaw colls.
;;; 05-Aug-1994 J. Unger elim machine attr - determine from beam-for attr, 
;;; also elim typecases for cnts special cases (now handled more mlc case).
;;; 08-Aug-1994 J. Unger take out code to update panel when new-coll-set
;;; is announced.  The code (and intent) is confusing.
;;; 15-Aug-1994 J. Jacky 5,1 not 5,2 format for leaf setting textlines
;;;                SCX control software only goes to nearest millimeter!
;;; 23-Aug-1994 J. Jacky Change centerline-list to edge-list
;;; 23-Sep-1994 J. Unger make panel narrower to fit on 1024x768 screen; make
;;;                font smaller to suit.  Also make slightly taller to
;;;                give more height to the SL20 textlines.
;;; 28-Nov-1994 J. Unger destroy bev & leaf editor in leaf pnl destroy
;;; method.
;;; 13-Jan-1995 I. Kalet destroy textlines too.  Change beam-for to
;;; beam-of, add plan-of and patient-of for bev-draw-all.
;;; 19-Sep-1996 I. Kalet update calls to bev-draw-all for new
;;; signature.
;;; 21-Jan-1997 I. Kalet eliminate table-position.
;;; 29-Apr-1997 I. Kalet add name of beam to title bar of leaf panel.
;;;  5-Jun-1997 I. Kalet machine returns object, not name.
;;; 16-Jun-1997 I. Kalet change auto leaf adjustments when collimator
;;; angle changes to be the Auto mode on the leaf editor.  Move
;;; parameters for sizes to init inst local variables.  Manage Accept
;;; button in new style (on if volatile data present).
;;; 09-Jul-1997 BobGian added commentary about results of
;;;  compute-mlc-vertices returning a degenerate (zero-area) contours.
;;;  Leaf editor and/or update-mlc-contour-from-leaves must be fixed.
;;; 14-Oct-1997 BobGian fix misspelling in comment.
;;; 11-Jun-1998 I. Kalet change :beam to :beam-for in make-view call.
;;; 23-Jun-1998 I. Kalet destroy bev after leaf-editor, since bev
;;; pixmap is leaf editor background.
;;; 25-Apr-1999 I. Kalet changes for multiple colormap support.
;;; 14-Sep-1999 I. Kalet build from leaf panel and related stuff, move
;;; get-mlc-vertices to mlc since it is used in charts and write-neutron.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 13-Dec-2000 I. Kalet add support for DRR background, including
;;; Image button, window and level controls, and rearrange controls to
;;; fit better.
;;; 19-Jan-2005 I. Kalet change make-contour-editor to make-planar-editor
;;; 19-May-2010 I. Kalet textlines return strings, so add conversion
;;; to float before using format to write the info back to the leaf
;;; setting textlines
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass mlc-panel (generic-panel)

  ((fr :accessor fr
       :documentation "The SLIK frame that contains the leaf panel.")

   (delete-b :accessor delete-b
	     :documentation "The Delete Panel button.")

   (beam-of :type beam
	    :accessor beam-of
	    :initarg :beam-of
	    :documentation "The beam for this leaf panel")

   (plan-of :initarg :plan-of
	    :accessor plan-of
	    :documentation "The plan containing the beam.")

   (patient-of :initarg :patient-of
	       :accessor patient-of
	       :documentation "The current patient.")

   (filmdist :type single-float
	     :accessor filmdist
	     :initarg :filmdist
	     :documentation "The source to film distance when using
simulator or port films on the digitizer.  Forwarded from the
containing collimator panel.")

   (bev :type beams-eye-view
        :accessor bev
        :documentation "A beam's eye view, not displayed, but used as
background for the display region.")

   (image-mediator :accessor image-mediator
		   :initform nil
		   :documentation "A single image-view-mediator to
manage creation of DRR images as necessary for the view background.")

   (window-control :accessor window-control
		   :documentation "The textline that displays and sets
the window for the background view's image.")

   (level-control :accessor level-control
		  :documentation "The textline that displays and sets
the level for the background view's image.")

   (ce :type planar-editor
       :accessor ce
       :documentation "A contour/point editor, in which the bev
       background is displayed.  The point mode is not used.")

   (rotate-mode-btn :accessor rotate-mode-btn
		    :documentation "The button that toggles either
manual or automatic leaf setting as the collimator rotates.")

   (set-leaves-btn :accessor set-leaves-btn
		   :documentation "The button for setting the leaves
to a best fit to the current contour at the current collimator
angle.")

   (set-contour-btn :accessor set-contour-btn
		    :documentation "The button for setting the contour
to match the leaf shapes at their current settings.")

   (image-button :accessor image-button
		 :documentation "The button that toggles display of
image data in this view.")

   (fg-button :accessor fg-button
	      :documentation "Brings up a popup menu of objects in the
view to display them or not on a view by view basis.")

   (viewlist-panel :accessor viewlist-panel
		   :initform nil
		   :documentation "Temporarily holds the list of
objects visible on the screen.")

   (leaf-settings :type list
                  :accessor leaf-settings
                  :initform nil
                  :documentation "A list of x y pairs, each x and y
being the setting for a left and right collimator leaf position at
the same y-coord in collimator space.")

   (left-leaf-tlns :type list
                   :accessor left-leaf-tlns
                   :initform nil
                   :documentation "A list of left leaf position textlines.")

   (right-leaf-tlns :type list
                    :accessor right-leaf-tlns
                    :initform nil
                    :documentation "A list of right leaf position textlines.")

   (busy :type (member t nil)
         :accessor busy
         :initform nil
         :documentation "A busy flag to prevent the leaf editor's vertex
list from updating in response to collimator changes that were themselves
caused by updates to the vertex list.")

   )

  (:default-initargs :filmdist 100.0)

  (:documentation "The mlc panel displays a view of the treatment
volume from the beam source to isocenter, with portal editing facility
and mlc leaves overlaid on top, and textlines for editing individual
leaf positions on the sides of the panel.")

  )

;;;---------------------------------------------

(defmethod (setf filmdist) :after (newfd (pan mlc-panel))

  (setf (digitizer-mag (ce pan))
    (/ newfd (isodist (beam-of pan)))))

;;;---------------------------------------------

(defun update-mlc-editor (mlc-pan)

  (let* ((bm (beam-of mlc-pan))
	 (coll (collimator bm))
	 (bev (bev mlc-pan))
	 (scale (scale bev))
	 (x0 (x-origin bev))
	 (y0 (y-origin bev))
	 (color (sl:color-gc *mlc-leaf-color*))
	 (prim (find coll (foreground bev) :key #'object))
	 (collim-info (collimator-info (machine bm)))
	 (xmax (leaf-open-limit collim-info))
	 (xmin (- xmax))
	 (edge-list (edge-list collim-info))
	 (angle (collimator-angle bm))
	 (leaf-pairs (mapcar
		      #'(lambda (yu yl x-pair)
			  (let ((left-leaf (pixel-contour
					    (poly:rotate-vertices
					     (counter-clockwise-rectangle
					      xmin yu (first x-pair) yl)
					     angle)
					    scale x0 y0))
				(right-leaf (pixel-contour
					     (poly:rotate-vertices
					      (counter-clockwise-rectangle
					       (second x-pair) yu xmax yl)
					      angle)
					     scale x0 y0)))
			    (list (nconc left-leaf
					 (list (first left-leaf)
					       (second left-leaf)))
				  (nconc right-leaf
					 (list (first right-leaf)
					       (second right-leaf))))))
		      (butlast edge-list) (rest edge-list)
		      (leaf-settings mlc-pan))))
    ;; maybe should also draw blocks of omitted beam?
    (bev-draw-all bev (plan-of mlc-pan) (patient-of mlc-pan) bm)
    ;; draw leaves
    (setf (name coll) "MLC Leaves") ;; to appear in declutter menu
    (unless prim
      (setq prim (make-lines-prim nil color :object coll))
      (push prim (foreground bev)))
    (setf (color prim) color
	  (points prim) (apply #'append leaf-pairs))
    (display-view bev)) ;; redraw the primitives into the pixmap
  (display-planar-editor (ce mlc-pan)))

;;;---------------------------------------------

(defmethod initialize-instance :after ((mp mlc-panel) &rest initargs)

  "Initializes the mlc panel gui."

  (let* ((bm (beam-of mp))
	 (bev-size 768)			; Size of the bev
	 (btw 130)			; Width of leaf textlines
	 (left-x 5)
	 (ctl-btw (- btw (* 2 left-x)))	; Width of control buttons
	 (right-x (+ left-x btw bev-size))
	 (bth 25) ;; this and following are magic numbers - see planar-editor
	 (top-y 5)
	 (frm-height (+ bth 10 bev-size))
	 (frm (apply #'sl:make-frame (+ (* 2 btw) bev-size) frm-height
		     :title (format nil "Leaf and Portal Editor for ~A"
				    (name bm))
		     initargs))
         (frm-win (sl:window frm))
	 (font (symbol-value *small-font*)))
    (setf (fr mp) frm)
    (setf (bev mp)
      (make-view bev-size bev-size 'beams-eye-view :beam-for bm
		 :display-func #'(lambda (vw)
				   (setf (image-cache vw) nil)
				   (draw (image (image-mediator mp)) vw)
				   (display-view vw)
				   (display-planar-editor (ce mp)))))
    (setf (ce mp) (apply #'make-planar-editor
			 :vertices (get-mlc-vertices bm)
			 :parent (sl:window (fr mp))
			 :background (sl:pixmap (picture (bev mp)))
			 :x-origin (round (/ bev-size 2))
			 :y-origin (round (/ bev-size 2))
			 :scale (scale (bev mp))
			 :digitizer-mag (/ (filmdist mp) (isodist bm))
			 :color (sl:color-gc (display-color bm))
			 :ulc-x btw :ulc-y 0
			 initargs))
    (update-mlc-editor mp)
    (setf (delete-b mp) (apply #'sl:make-button ctl-btw bth
			       :button-type :momentary
			       :font font :label "Delete Panel"
			       :parent frm-win
			       :ulc-x left-x :ulc-y top-y
			       initargs))
    (setf (set-leaves-btn mp) (apply #'sl:make-button ctl-btw bth
				     :button-type :momentary
				     :font font :label "Set Leaves"
				     :parent frm-win
				     :ulc-x left-x
				     :ulc-y (bp-y top-y bth 1)
				     initargs))
    (setf (set-contour-btn mp) (apply #'sl:make-button ctl-btw bth
				      :button-type :momentary
				      :font font :label "Set Contour"
				      :parent frm-win
				      :ulc-x left-x
				      :ulc-y (bp-y top-y bth 2)
				      initargs))
    (setf (rotate-mode-btn mp) (apply #'sl:make-button ctl-btw bth
				      :font font :label "Auto Leaf"
				      :parent frm-win
				      :ulc-x left-x
				      :ulc-y (bp-y top-y bth 3)
				      initargs))
    (setf (fg-button mp) (apply #'sl:make-button ctl-btw bth
				:font font :label "Objects"
				:parent frm-win
				:ulc-x left-x
				:ulc-y (bp-y top-y bth 4)
				initargs))
    (setf (image-button mp) (apply #'sl:make-button ctl-btw bth
				   :font font :label "Image"
				   :parent frm-win
				   :ulc-x right-x
				   :ulc-y top-y
				   initargs))
    (setf (window-control mp)
      (apply #'sl:make-sliderbox ctl-btw bth 1.0 2047.0 9999.0
	     :parent frm-win :font font :label "Win: "
	     :ulc-x (- right-x left-x) :ulc-y (bp-y top-y bth 1)
	     :border-width 0 :display-limits nil
	     initargs))
    (setf (level-control mp)
      (apply #'sl:make-sliderbox ctl-btw bth 1.0 4095.0 9999.0
	     :parent frm-win :font font :label "Lev: "
	     :ulc-x (- right-x left-x) :ulc-y (bp-y top-y bth 3)
	     :border-width 0 :display-limits nil
	     initargs))
    ;; create and fill leaf textlines
    (do* ((collim-info (collimator-info (machine (beam-of mp))))
	  (column-len (1- (length (edge-list collim-info))))
	  (height (truncate (/ (- frm-height (bp-y top-y bth 5))
			       column-len)))
	  (leaf-pairs (leaf-pair-map collim-info) (rest leaf-pairs))
	  (xl 0)
	  (xr (+ btw bev-size))
	  (y (bp-y top-y bth 5) (+ y height))
	  (i 0 (1+ i)))
	((= i column-len))
      (push
       (sl:make-textline btw height
			 :font font :parent frm-win
			 :ulc-x xl :ulc-y y
			 :numeric t
			 :lower-limit (- (leaf-open-limit collim-info))
			 :upper-limit (leaf-overcenter-limit collim-info)
			 :volatile-width 4
			 :label (format nil "LEAF ~2@a: "
					(first (first leaf-pairs))))
       (left-leaf-tlns mp))
      (push
       (sl:make-textline btw height
			 :font font :parent frm-win
			 :ulc-x xr :ulc-y y
			 :numeric t
			 :lower-limit (- (leaf-overcenter-limit collim-info))
			 :upper-limit (leaf-open-limit collim-info)
			 :volatile-width 4
			 :label (format nil "LEAF ~2@a: "
					(second (first leaf-pairs))))
       (right-leaf-tlns mp)))
    (setf (left-leaf-tlns mp) (reverse (left-leaf-tlns mp)))
    (setf (right-leaf-tlns mp) (reverse (right-leaf-tlns mp)))
    (ev:add-notify mp (new-scale (ce mp))
		   #'(lambda (pan ed new-sc)
		       (declare (ignore ed))
		       (let ((bev (bev pan)))
			 (setf (scale bev) new-sc)
			 (update-mlc-editor pan))))
    (ev:add-notify mp (new-origin (ce mp))
		   #'(lambda (pan ed new-org)
		       (declare (ignore ed))
		       (let ((bev (bev pan)))
			 (setf (origin bev) new-org)
			 (update-mlc-editor pan))))
    (ev:add-notify mp (new-coll-angle bm)
		   #'(lambda (pan bem new-ang)
		       (declare (ignore bem new-ang))
		       ;; in manual mode we just rotate, but
		       ;; in auto mode we adjust the leaves too.
		       (if (sl:on (rotate-mode-btn pan))
			   (update-leaf-settings-from-portal pan))
		       (update-mlc-editor pan)))
    (ev:add-notify mp (sl:button-on (delete-b mp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (destroy pan)))
    (ev:add-notify mp (sl:button-on (set-leaves-btn mp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (update-leaf-settings-from-portal pan)
		       (update-mlc-editor pan)))
    (ev:add-notify mp (sl:button-on (set-contour-btn mp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (update-mlc-contour-from-leaves pan)
		       (update-mlc-editor pan)))
    (setf (image-button (bev mp)) (image-button mp))
    (setf (drr-state (bev mp)) (drr-state (bev mp))) ;; to init the button
    (ev:add-notify mp (sl:button-on (image-button mp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (bev pan)) t)
			 (update-mlc-editor pan)
			 (setf (busy pan) nil))))
    (ev:add-notify mp (sl:button-off (image-button mp))
		   #'(lambda (pan bt)
                       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (bev pan)) nil)
			 (update-mlc-editor pan)
			 (setf (busy pan) nil))))
    (ev:add-notify mp (sl:button-2-on (image-button mp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (case (drr-state (bev pan))
			   ;;'stopped is a noop
			   ('running
			    (setf (drr-state (bev pan)) 'paused))
			   ('paused
			    (setf (drr-state (bev pan)) 'running)
			    (drr-bg (bev pan))))
			 (setf (busy pan) nil))))
    (ev:add-notify mp (bg-toggled (bev mp))
		   #'(lambda (pan vw)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:on (image-button pan))
			   (background-displayed vw))
			 (setf (busy pan) nil))))
    (ev:add-notify mp (sl:button-on (fg-button mp))
		   #'(lambda (pan bt)
		       (setf (viewlist-panel pan)
			 (make-instance 'viewlist-panel
			   :refresh-fn #'(lambda (vw)
					   (display-view vw)
					   (display-planar-editor
					    (ce pan)))
			   :view (bev pan)))
		       (ev:add-notify mp (deleted (viewlist-panel mp))
				      #'(lambda (pnl vlpnl)
					  (declare (ignore vlpnl))
					  (setf (viewlist-panel pnl) nil)
					  (when (not (busy pnl))
					    (setf (busy pnl) t)
					    (setf (sl:on bt) nil)
					    (setf (busy pnl) nil))))))
    (ev:add-notify mp (sl:button-off (fg-button mp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (destroy (viewlist-panel pan))
			 (setf (busy pan) nil))))
    (ev:add-notify mp (new-vertices (ce mp))
		   #'(lambda (pan ed verts)
		       (declare (ignore ed))
		       (unless (busy pan)
			 (setf (busy pan) t)
			 (set-mlc-vertices (beam-of pan) verts)
			 (update-mlc-editor pan)
			 (setf (busy pan) nil))))
    (ev:add-notify mp (new-coll-set (collimator bm))
		   #'(lambda (cp coll)
		       (declare (ignore coll))
		       (unless (busy cp)
			 (setf (busy cp) t)
			 (setf (vertices (ce cp))
			   (get-mlc-vertices bm))
			 (update-mlc-editor cp)
			 (setf (busy cp) nil))))
    ;; leaf setting textlines
    (do ((left-tlns (left-leaf-tlns mp) (rest left-tlns))
	 (right-tlns (right-leaf-tlns mp) (rest right-tlns)))
	((null left-tlns))
      ;; the info from the textline is a STRING!
      (ev:add-notify mp (sl:new-info (first left-tlns))
		     #'(lambda (pan tln info)
			 (let ((pos (position tln (left-leaf-tlns pan)))
			       (float-info (float (read-from-string info))))
			   (setf (sl:info tln)
			     (format nil "~5,1F" float-info))
			   (setf (first (nth pos (leaf-settings pan))) 
			     float-info))
			 (update-mlc-editor pan)))
      (ev:add-notify mp (sl:new-info (first right-tlns))
		     #'(lambda (pan tln info)
			 (let ((pos (position tln (right-leaf-tlns pan)))
			       (float-info (float (read-from-string info))))
			   (setf (sl:info tln)
			     (format nil "~5,1F" float-info))
			   (setf (second (nth pos (leaf-settings pan))) 
			     float-info))
			 (update-mlc-editor pan))))
    (setf (sl:setting (window-control mp))
      (coerce (window (bev mp)) 'single-float))
    (ev:add-notify mp (sl:value-changed (window-control mp))
		   #'(lambda (pan wc win)
		       (declare (ignore wc))
		       (setf (window (bev pan)) (round win))
		       (if (background-displayed (bev pan))
			   (display-planar-editor (ce pan)))))
    (setf (sl:setting (level-control mp))
      (coerce (level (bev mp)) 'single-float))
    (ev:add-notify mp (sl:value-changed (level-control mp))
		   #'(lambda (pan lc lev)
		       (declare (ignore lc))
		       (setf (level (bev pan)) (round lev))
		       (if (background-displayed (bev pan))
			   (display-planar-editor (ce pan)))))
    (if (image-set (patient-of mp))
	(setf (image-mediator mp)
	  (make-image-view-mediator (image-set (patient-of mp)) (bev mp))))
    ;; this is an abbreviated beam-view mediator for this view only
    (ev:add-notify (bev mp) (new-gantry-angle bm) #'refresh-bev)
    (ev:add-notify (bev mp) (new-couch-angle bm) #'refresh-bev)
    (ev:add-notify (bev mp) (new-couch-lat bm) #'refresh-bev)
    (ev:add-notify (bev mp) (new-couch-ht bm) #'refresh-bev)
    (ev:add-notify (bev mp) (new-couch-long bm) #'refresh-bev)
    (ev:add-notify (bev mp) (new-machine bm) #'refresh-bev)
    (update-leaf-settings-from-portal mp)
    (update-mlc-editor mp)))

;;;---------------------------------------------

(defun set-mlc-vertices (bm verts)

  "set-mlc-vertices bm verts

For beams with a collimator of type multileaf-coll, assigns the verts
parameter to the collimator's vertices attribute.  For beams with a
collimator of type cnts-coll, calls compute-vj-block to get new
collimator settings and a C-shaped block, then sets the collimator
jaws to the returned collim settings and and replaces any blocks with
the C-shaped block.  Does nothing for any other type of collimator."

  (let ((coll (collimator bm)))
    (typecase coll
      (multileaf-coll (setf (vertices coll) verts))
      (cnts-coll
       (let* ((col-blk (compute-vj-block
			(poly:rotate-vertices
			 verts (- (collimator-angle bm)))))
	      (new-coll (first col-blk))
	      (new-blk (make-beam-block "Computed C block"
					:vertices (second col-blk))))
	 (setf (x-inf coll) (x-inf new-coll)
	       (y-inf coll) (y-inf new-coll)
	       (x-sup coll) (x-sup new-coll)
	       (y-sup coll) (y-sup new-coll))
	 (dolist (old-blk (coll:elements (blocks bm)))
	   (coll:delete-element old-blk (blocks bm)))
	 (coll:insert-element new-blk (blocks bm))))
      (t nil))))

;;;---------------------------------------------

(defun update-mlc-contour-from-leaves (mp)

  "update-mlc-contour-from-leaves mp

Updates the contour of the mlc panel from the panel's leaf-settings."

  (let ((b (beam-of mp)))
    ;;
    ;; Need to do something here (or rewiring of leaf editor) so that
    ;; if compute-mlc-vertices returns nil (a degenerate contour) we
    ;; don't propagate that bad data throughout system.
    ;;
    ;; At minimum, don't setf vertices to bad data.
    ;; Probably also warn user and turn button back on too.
    ;;
    (setf (vertices (ce mp))
      (compute-mlc-vertices (collimator-angle b)
			    (leaf-settings mp) 
			    (edge-list (collimator-info (machine b)))))
    (unless (sl:on (accept-btn (ce mp)))
      (setf (sl:on (accept-btn (ce mp))) t))))

;;;---------------------------------------------

(defun update-leaf-settings-from-portal (mp)

  "update-leaf-settings-from-portal mp

Updates the leaf settings and textlines using the collimator portal
vertices."

  (let* ((b (beam-of mp)))
    (setf (leaf-settings mp)
      (compute-mlc (collimator-angle b)
		   (vertices (ce mp))
		   (edge-list (collimator-info (machine b)))))
    ;; update the leaf textlines
    (mapc #'(lambda (pair l-tln r-tln)
              (setf (sl:info l-tln) (format nil "~5,1F" (first pair)))
              (setf (sl:info r-tln) (format nil "~5,1F" (second pair))))
	  (leaf-settings mp)
	  (left-leaf-tlns mp)
	  (right-leaf-tlns mp))))

;;;---------------------------------------------

(defun make-mlc-panel (&rest initargs)

  "make-mlc-panel &rest initargs

Creates and returns a leaf panel with the specified initialization args."

  (apply #'make-instance 'mlc-panel initargs))

;;;---------------------------------------------

(defmethod destroy :before ((mp mlc-panel))

  "Unmap the panel's frame and unregisters w/ external events."

  (let ((vw (bev mp))
	(bm (beam-of mp)))
    ;; ensure that there are not any lingering 
    ;;   background jobs for this view-panel
    (remove-bg-drr vw)
    (when (eq 'running (drr-state vw))
      (setf (drr-state vw) 'paused))
    (setf (image-button vw) nil)
    (ev:remove-notify vw (new-gantry-angle bm))
    (ev:remove-notify vw (new-couch-angle bm))
    (ev:remove-notify vw (new-couch-lat bm))
    (ev:remove-notify vw (new-couch-ht bm))
    (ev:remove-notify vw (new-couch-long bm))
    (ev:remove-notify vw (new-machine bm))
    (ev:remove-notify mp (new-coll-angle bm))
    (ev:remove-notify mp (new-coll-set (collimator bm)))
    (if (image-mediator mp) (destroy (image-mediator mp)))
    (destroy vw))
  (destroy (ce mp))
  (sl:destroy (delete-b mp))
  (sl:destroy (set-leaves-btn mp))
  (sl:destroy (set-contour-btn mp))
  (sl:destroy (rotate-mode-btn mp))
  (sl:destroy (image-button mp))
  (sl:destroy (window-control mp))
  (sl:destroy (level-control mp))
  (if (sl:on (fg-button mp)) (setf (sl:on (fg-button mp)) nil))
  (sl:destroy (fg-button mp))
  (dolist (tl (left-leaf-tlns mp)) (sl:destroy tl))
  (dolist (tl (right-leaf-tlns mp)) (sl:destroy tl))
  (sl:destroy (fr mp)))

;;;---------------------------------------------
;;; End.
