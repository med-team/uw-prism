;;;
;;; file-functions
;;;
;;; This module provides functions for storing and retrieving
;;; object data from files.
;;;
;;; 04-May-1992 I. Kalet taken from earlier prism
;;; 13-Jul-1992 I. Kalet fix error omitting allegro-v4.1 in slot-names
;;; 29-Jul-1992 I. Kalet change get-filename to generic function
;;; bin-array-filename
;;;  9-Aug-1992 I. Kalet add support in get-object, put-object for
;;;  slot-type :collection
;;; 19-Jan-1993 I. Kalet return nil from get-all-objects if file does
;;; not exist
;;; 23-Mar-1993 J. Unger expand slot-names to cmulisp; modify #+/+-'s
;;; 14-Feb-1994 I. Kalet fix Lucid for SunCL and add Genera.
;;;  4-Mar-1994 I. Kalet consolidate Lucid, Allegro, Genera
;;;  7-Jun-1994 J. Unger update for allegro cl v4.2
;;; 21-Jun-1994 I. Kalet add support for slot type :timestamp
;;; 12-Jan-1995 I. Kalet take out proclaim form, and explicit support
;;;  for VAXlisp and Lucid.  Put in support for slot names to ignore,
;;;  so that obsolete data in files will not cause an error.
;;; 13-Aug-1995 I. Kalet add lispworks in MOP version of slot-names
;;; 19-Apr-1997 I. Kalet just assume MOP supported - no more support
;;; for old CMU Lisp or VAXlisp.
;;; 29-Aug-1997 BobGian clarified comments in GET-OBJECT and
;;; PUT-OBJECT.
;;; 12-Sep-1997 I. Kalet add get-index-list - used by db functions
;;; 28-Jan-1998 BobGian slight speedup: EQL on symbols -> EQ.
;;;  5-Jun-1998 I. Kalet use read-sequence to speed up read-bin-array,
;;;  also use Allegro-dependent :allocation :old to tenure the arrays.
;;; 10-Oct-1998 C. Wilcox added the ability to swap byte orders to
;;; address endian issues between HP-UX and Linux (x86).
;;;  3-Dec-1998 I. Kalet took out byte swap hack, it is NOT portable.
;;;  Binary files should always be read in host byte order, standard
;;;  CL, and it is up to the creator of such files to create them in
;;;  host byte order on any host.  This means that copying binary
;;;  files from a little endian machine to a big endian machine
;;;  requires that the copy operation swap the bytes, not an
;;;  application like Prism.
;;;  2-Jan-2000 I. Kalet requalify use of MOP by #+allegro, add clisp
;;; 27-Aug-2000 I. Kalet just add progress report when reading bin
;;; arrays but move restored byte swap code to prism-db.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 07-Nov-2004 A. Simms condense slot-names definitions for Allegro, CMUCL
;;; and CLisp to a single function with a Lisp specific mapcar form.
;;; 18-Apr-2005 I. Kalet cosmetic fixes.
;;; 24-Jun-2009 I. Kalet add explicit require for Allegro Gray streams
;;; modules to handle non-byte streams and read-sequence
;;;

#+allegro
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :streamc))

(in-package :prism)

;;;-------------------------------------------------------


(defun slot-names (obj)

  "slot-names obj

returns a list of slot names defined for the class of which object obj
is a member, using the MOP."

  #+allegro
  (mapcar #'clos:slot-definition-name
	  (clos:class-slots (class-of obj)))

  #+cmu
  (mapcar #'(lambda (x) (pcl::slot-value x 'pcl::name))
      (pcl::class-slots (class-of obj)))

  #+clisp
  (mapcar #'clos::slotdef-name
	  (clos::class-slots (class-of obj)))

  )


;;;------------------------------------------
;;; These are the least specific methods for
;;; generic functions slot-type and not-saved.
;;;------------------------------------------

(defmethod slot-type ((object t) slotname)

  "slot-type object slotname

This is a default method for the generic function that returns the
slot type of a slot.  Individual classes must provide their own
methods to return one of the keywords, :simple, :object-list,
:collection or :bin-array, if any slots are different from :simple.
If all slots are of type :simple then the class needs no method and
this default method will suffice."

  (declare (ignore slotname))
  :simple)

;;;-------------------------------------------

(defmethod not-saved ((object t))

  "not-saved object

The default method for the generic function that returns a list of
slot names which should NOT be saved in an external file.  An example
of this method for class foo which does not want to save slot c would
look like (defmethod not-saved ((object foo)) '(c))"

  nil)

;;;-------------------------------------------

(defmethod bin-array-pathname ((obj t))

  "bin-array-pathname obj

returns a string to be used as a directory name to merge in with the
binary array filename when calling read-bin-array or write-bin-array."

  *default-pathname-defaults*)               ; this is just the default method

;;;-------------------------------------------

(defun get-object (in-stream &key (parent nil) )

  "get-object in-stream &key parent

reads forms from in-stream, filling in slots of a new instance of the
class for the first symbol read from the stream.  The data are assumed
to be in the form <slot name> <slot value>, except if the slot is a
list of other objects,in which case, get-object is called recursively
to construct the list.  The data for an object are terminated with a
keyword :END.  It returns the newly created instance along with any
component objects, or nil if the first keyword read from in-stream is
the keyword :END. So, :END means either end of an object list, or end
of an object.  If the slot type is :parent, the value of parent is
bound to the slot.  If the slot type is :timestamp the value is held
until the end, since the slot may get updated by other code in the
system as slots get filled in."

  (let* ((current-key (read in-stream))
	 (object (if (eq current-key :end) nil      ; end of object list
		     (make-instance current-key)))
	 (timestamp-slotname nil)                   ; temporary storage
	 (timestamp nil))                  ; temp storage for timestamp string
    (unless (null object)
      (loop
	(setq current-key (read in-stream))
	(when (eq current-key :end)                 ; end of object
	  (when timestamp                           ; update that slot now
	    (setf (slot-value object timestamp-slotname) timestamp))
	  (return object))
	(if (eq (slot-type object current-key) :ignore)
	    (read in-stream)              ; throw away the value - usually nil
	    (setf (slot-value object current-key)   ; otherwise process it
		  (case (slot-type object current-key)
		    (:simple (read in-stream))
		    (:bin-array
		      (let ((bin-info (read in-stream)))
			(format t "Reading ~A~%" bin-info)
			(read-bin-array (merge-pathnames
					  (first bin-info)
					  (bin-array-pathname object))
					(rest bin-info))))
		    (:object (get-object in-stream :parent object))
		    (:object-list
		      (let ((slotlist '())
			    (next-object nil))
			(loop
			  (setq next-object
				(get-object in-stream :parent object))
			  (cond (next-object
				  (push next-object slotlist))
				(t (return (nreverse slotlist)))))))
		    ;; We assume this slot is already initialized with an
		    ;; empty collection - we use it because other stuff may be
		    ;; connected to it (see for example the plans module).
		    (:collection
		      (let ((slotset (slot-value object current-key))
			    (next-object nil))
			(loop
			  (setq next-object
				(get-object in-stream :parent object))
			  (cond (next-object
				  (coll:insert-element next-object slotset))
				(t (return slotset))))))
		    (:parent (progn (read in-stream)    ; discard value
				    parent))        ; just use parent
		    (:timestamp (setq timestamp-slotname current-key)
				(setq timestamp (read in-stream))))))))))

;;;----------------------------------

(defun tab-print (item stream tab &optional (cr nil))

  "tab-print item stream tab &optional (cr nil)

Given an item (eg symbol), a stream, a tab value (an integer), and
optionally instructions to format a carriage return, a string
representation of the item is printed after the appropriate number of
blank spaces, as specified by tab value."

  (format stream "~a"
	  (concatenate 'string
		       (make-string tab :initial-element #\space)
		       (write-to-string item :pretty t)
		       (make-string 2 :initial-element #\space)))
  (when cr (format stream "~%")))

;;;----------------------------------

(defmethod bin-array-filename ((obj t) slotname)

  "Default method for generating a name for a bin-array data file.
Uses slot name and generates lower-case to work easily with Unix.  You
can provide more sophisticated methods for various object classes."

  (concatenate 'string
	       (string-downcase (remove #\: (write-to-string slotname)))
	       ".bin"))

;;;----------------------------------

(defun put-object (object out-stream &optional (tab 0))

  "put-object object out-stream &optional (tab 0)

writes a printed representation of object to the stream out-stream, in
a form suitable to be read in by get-object.  It needs two generic
functions, slot-type and not-saved.  For each slot except those
returned by not-saved, it writes the slot name, then a form that
depends on the type of data supposed to be in that slot, as specified
by the value of (slot-type object slotname).  Tabs are optionally used
to indent object names and slot-values hierarchically to make files
more readable by humans."

  (tab-print (class-name (class-of object)) out-stream tab t)
  (mapc #'(lambda (slotname)
	    (when (slot-boundp object slotname)
	      (tab-print slotname out-stream (+ 2 tab))
	      (case (slot-type object slotname)
		((:simple :timestamp) (tab-print
					(slot-value object slotname)
					out-stream 0 t))
		(:bin-array
		  (let* ((the-data (slot-value object slotname))
			 (filename (bin-array-filename object
						       slotname))
			 (dimensions (array-dimensions the-data)))
		    (tab-print (push filename dimensions)
			       out-stream 0 t)
		    (write-bin-array (merge-pathnames
				       (bin-array-pathname object)
				       filename)
				     the-data)))
		(:object (fresh-line out-stream)
			 (put-object (slot-value object slotname)
				     out-stream (+ 4 tab)))
		(:object-list
		  (fresh-line out-stream)
		  (mapc #'(lambda (obj)
			    (put-object obj out-stream (+ 4 tab)))
		    (slot-value object slotname))
		  (tab-print :end out-stream (+ 2 tab) t))  ; terminates list
		(:collection                        ; like :object-list
		  (fresh-line out-stream)
		  (mapc #'(lambda (obj)
			    (put-object obj out-stream (+ 4 tab)))
		    (coll:elements (slot-value object slotname)))
		  (tab-print :end out-stream (+ 2 tab) t))  ; terminates list
		(:parent (tab-print nil out-stream 0 t))))) ; just write NIL
    (set-difference (slot-names object) (not-saved object)))
  (tab-print :end out-stream tab t))                ; terminates object

;;;----------------------------------

(defun read-bin-array (filename dimensions)

  "read-bin-array filename dimensions

reads an array of dimensions specified by dimensions from a binary
file named 'filename' into an array of (unsigned-byte 16).  Arrays of
1 through 3 dimensions are currently supported."

  (let* ((bin-dim (if (numberp dimensions) (list dimensions)
		      dimensions))
	 (num-dim (length bin-dim)))
    (with-open-file (infile filename :direction :input
			    :element-type '(unsigned-byte 16))
      (case num-dim
	(1 (let ((bin-array (make-array bin-dim
					:element-type
					'(unsigned-byte 16)
					#+allegro :allocation
					#+allegro :old)))
	     (declare (type (simple-array (unsigned-byte 16) (*))
			    bin-array))
	     (read-sequence bin-array infile)
	     bin-array))
	(2 (let* ((bin-array (make-array bin-dim
					 :element-type
					 '(unsigned-byte 16)
					 #+allegro :allocation
					 #+allegro :old))
		  (disp-array (make-array (array-total-size bin-array)
					  :element-type
					  '(unsigned-byte 16)
					  :displaced-to bin-array)))
	     (declare (type (simple-array (unsigned-byte 16) (* *))
			    bin-array)
		      (type (simple-array (unsigned-byte 16) (*))
			    disp-array))
	     (read-sequence disp-array infile)
	     bin-array))
	(3 (let* ((bin-array (make-array bin-dim
					 :element-type
					 '(unsigned-byte 16)
					 #+allegro :allocation
					 #+allegro :old))
		  (disp-array (make-array (array-total-size bin-array)
					  :element-type
					  '(unsigned-byte 16)
					  :displaced-to bin-array)))
	     (declare (type (simple-array (unsigned-byte 16) (* * *))
			    bin-array)
		      (type (simple-array (unsigned-byte 16) (*))
			    disp-array))
	     (read-sequence disp-array infile)
	     bin-array))))))

;;;----------------------------------

(defun write-bin-array (filename bin-array)

  "write-bin-array filename bin-array

writes an array of (unsigned-byte 16)s to a binary file named
'filename'.  Arrays of 1 through 3 dimensions are currently
supported."

  (let* ((bin-dim (array-dimensions bin-array))
	 (num-dim (length bin-dim))
	 (x-dim (nth (- num-dim 1) bin-dim))
	 (y-dim (if (< num-dim 2) 0
		    (nth (- num-dim 2) bin-dim)))
	 (z-dim (if (< num-dim 3) 0
		    (first bin-dim))))
    (declare (fixnum num-dim x-dim y-dim z-dim))
    (declare (type (simple-array (unsigned-byte 16)) bin-array))
    (with-open-file (outfile filename
			     :direction :output
			     :element-type '(unsigned-byte 16)
			     :if-exists :new-version)
      (case num-dim
	(1 (dotimes (i x-dim)
	     (write-byte (aref bin-array i) outfile)))
	(2 (dotimes (j y-dim)
	     (dotimes (i x-dim)
	       (write-byte (aref bin-array j i)
			   outfile))))
	(3 (dotimes (k z-dim)
	     (format t "writing plane ~a...~%" k)
	     (dotimes (j y-dim)
	       (dotimes (i x-dim)
		 (write-byte (aref bin-array k j i)
			     outfile)))))))))

;;;----------------------------------

(defun get-all-objects (filename)

  "get-all-objects filename

opens file named filename, iteratively calls get-object to accumulate
a list of all the objects found in the file, until end of file is
reached.  Returns the list of object instances.  If the file does not
exist, returns nil."

  (with-open-file (stream filename
			  :direction :input
			  :if-does-not-exist nil)
    (when (streamp stream)
      (let ((object-list '()))
	(loop
	  (cond ((eq (peek-char t stream nil :eof) :eof)
		 (return object-list))
		(t (push (get-object stream) object-list))))))))

;;;----------------------------------

(defun put-all-objects (object-list filename)

  "put-all-objects object-list filename

opens file named filename, iteratively calls put-object on successive
elements of the list object-list.  If a file named filename already
exists, a new version is created."

  (with-open-file (stream filename
			  :direction :output
			  :if-exists :new-version)
    (dolist (obj object-list)
      (put-object obj stream))))

;;;----------------------------------

(defun get-index-list (filename database item
		       &key (key #'first) (test #'equal))

  "get-index-list filename database item
		  &key (key #'first) (test #'equal)

returns a list of lists, each one containing data about one database
entry, a patient, a case, an image study, a therapy machine or other,
from an index file.  The parameters are: filename, a string naming the
index file, database, a string or pathname specifying where the file
is located, and item, a string or other entity to look for in the
file.  If item is not nil then the key and test functions are used to
select only records that match the item.  The returned list is in
reverse order of the entries in the file."

  (with-open-file (stream (merge-pathnames filename database)
			  :if-does-not-exist nil)
    ;; If (streamp stream) is nil, when returns nil.
    (when (streamp stream)
      (do ((entry (read stream nil :eof) (read stream nil :eof))
	   (entries '()))
	  ((eq entry :eof) entries)
	(when (or (not item)
		  (funcall test (funcall key entry) item))
	  (push entry entries))))))

;;;----------------------------------
;;; End.
