;;;
;;; pathlength
;;;
;;; Provides the pathlength ray-tracing function that finds the
;;; density-weighted path from point A to point B through a bunch of
;;; PSTRUCTs.  The prism construction functions are here, along with
;;; the actual PATHLENGTH function.
;;;
;;; 16-Jan-1997 I. Kalet started implementation (stub so far).
;;; 23-Jun-1997 BobGian merged with Gavin Young's work (real thing).
;;; 03-Sep-1997 BobGian reworking code to interface with Prism.
;;; 07-Sep-1997 BobGian moved clipping code here from beam-dose.  This file
;;;  serves as a utilities file for beam-dose, which depends on pathlength
;;;  but not conversely.
;;;  7-Oct-1997 BobGian move CONTOUR-ENCLOSES-P to POLYGONS package.
;;; 10-Nov-1997 BobGian incorporate bug fixes from Gavin, inline crossproduct.
;;; 22-Jan-1998 BobGian update with new faster version [array-type decls,
;;;  array-access and arithmetic inlining, argument-vector usage].  Also,
;;;  move all polygon clipping code to separate file: clipper.cl.
;;; 09-Mar-1998 BobGian modest upgrade of dose-calc code.
;;; 22-May-1998 BobGian major overhaul of PATHLENGTH function:
;;;   - DEFCONSTANTS naming slots in Arg-Vec moved to new file
;;;     "dosecomp-decls", which also contains macros used here.
;;;   - Arg-Vec: making calling conventions more consistent, also
;;;     allowing sharing of this technique [and same vector] for calls
;;;     between functions in "beam-dose.cl" and "clipper.cl".
;;;   - Restructuring data flows to decrease use of MAPCAR and SORT.
;;;     Processing data sequentially rather than building large structures
;;;     to be passed in functional style through various stages.
;;;     Building structures incrementally in sorted order rather than
;;;     sorting after entire structure built.  These optimizations alone
;;;     account for a factor of 3 speedup.
;;;   - Inlining: ASSEMBLE-L, ASSEMBLE-LI, ASSEMBLE-LI-HAT,
;;;     INTERSECT-ALPHAS, ROTATE-IF-NECESSARY.
;;;   - Converting from tail-recursive [structure-recursive argument-
;;;     copying] to iterative [argument copying only when necessary]
;;;     in REPLACE-CONSEC-VRTS and REMOVE-DUPLICATES-BY-PAIRS.
;;;   - New version of contour-encloses-point algorithm included here.
;;; 01-Jun-1998 BobGian fix mistake in ENCLOSES? (contour-encloses-pt)
;;;   function - missing expression in collinearity test; also now
;;;   returns T if point is on boundary (simplifies calling code slightly).
;;; 08-Jun-1998 BobGian optimization update: all mapping functions replaced
;;;   by in-line iteration, bug-fix in preserving stack order of structure
;;;   tags when sorting identically-valued ray alpha coordinates.
;;; 26-Jun-1998 BobGian further optimization: pass ORGAN-DENSITY-ARRAY as
;;;   array rather than list, condense redundant sorting, avoid redundant
;;;   consing, compress redundant temp variables.
;;; 17-Jul-1998 BobGian modify order of arguments to PATHLENGTH to make
;;;   consistent with new function BUILD-PATIENT-STRUCTURES, which is added
;;;   to factor beam-independent portion of PATHLENGTH setup out of
;;;   COMPUTE-BEAM-DOSE so it can be called once per dosecalc, for all beams.
;;; 20-Jul-1998 BobGian optimize a few more array declarations and accessors.
;;; 13-Aug-1998 BobGian make PATHLENGTH return "dosepoint-inside-patient-p"
;;;   flag (numerical value returned via Arg-Vec) so COMPUTE-BEAM-DOSE
;;;   can set dose outside patient to zero.
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization).
;;; 02-Mar-2000 BobGian add declarations in PATHLENGTH, REPLACE-CONSEC-VRTS.
;;; 24-Mar-2000 BobGian add check for empty contours in preprocessor function
;;;   BUILD-PATIENT-STRUCTURES - otherwise PATHLENGTH can crash.
;;; 27-Jun-2000 BobGian correct comment above about comments of this file.
;;; 02-Nov-2000 BobGian replace old ENCLOSES? (based on winding angle) with
;;;   new function based on faster ray/contour-intersection algorithm.
;;;   Minor variable-name changes in PATHLENGTH preparatory to new version
;;;   being developed for electron code.
;;; 30-May-2001 BobGian - major restructuring of pathlength computation:
;;;   Separate raytracing from line integration so that redundant computation
;;;     can be factored out (PATHLENGTH-RAYTRACE called once to build structure
;;;     that can be queried by PATHLENGTH-INTEGRATE multiple times).
;;;   Change all calling points in Electron and Photon dose calc.
;;;   Wrap generic arithmetic with THE-declared types.
;;; 03-Jun-2001 BobGian fix PATHLENGTH-INTEGRATE to report whether target
;;;   point is inside or outside body.
;;; 15-Mar-2002 BobGian PATHLENGTH-RAYTRACE used for "ray out-of-body"
;;;   detection rather than PATHLENGTH-INTEGRATE, which just returns zero
;;;   rather than an "out-of-body" flag in this case.  Normally it will
;;;   never be called in this case, the condition having been detected
;;;   earlier by PATHLENGTH-RAYTRACE.
;;;   Also BUILD-PATIENT-STRUCTURES checks all organ densities and returns
;;;   a flag indicating whether none are present [dosecalc can't proceed then]
;;;   or some are out of range [dosecalc can proceed but user is warned first].
;;; 29-Apr-2002 BobGian PATHLENGTH-RAYTRACE cannot be used alone for
;;;   "ray out-of-body" detection, since it traces full length of normalizing
;;;   distance.  Must also integrate to dosepoint for correct test.
;;; 20-Sep-2002 BobGian BUILD-PATIENT-STRUCTURES checks organs for presence
;;;   of contours as well as checking contours for presence of vertices.
;;; 03-Jan-2003 BobGian:
;;;   REPLACE-CONSEC-VERTS inlined in PATHLENGTH-RAYTRACE.  Two slot formerly
;;;    used in argument vector to pass args to it now flushed.
;;;   PATHLENGTH-RAYTRACE and -INTEGRATE now use CONS cells rather than 2-elem
;;;    lists for internal data structures [X/Y coordinate pairs].
;;;   PATHLENGTH-INTEGRATE can calculate both density-weighted pathlength and
;;;    homogeneous pathlength at same time, returning either or both, as
;;;    controlled by last argument, in pair of slots in argument vector.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 31-Jan-2005 AMSimms - corrected use of Allegro specific coercions, now 
;;;   using coerce explicitly.
;;;

(in-package :prism)

;;;=============================================================

(defun build-patient-structures (patient-anatomy &aux organ-vertices-list
				 organ-zvals-list organ-density-list
				 the-density density-flag)

  "build-patient-structures patient-anatomy

returns three stuctures [two lists and one array] representing the patient's
anatomy in a form suitable for PATHLENGTH-RAYTRACE.

Returns: Organ-Vertices-List Organ-Z-Extents Organ-Density-Array Density-Flag"

  ;; PATIENT-ANATOMY is the value of the PATIENT object's ANATOMY slot, which
  ;; is a COLLECTION object.  From this we build three lists representing
  ;; patient anatomy in a form suitable for passing to PATHLENGTH-RAYTRACE.

  ;; We represent each organ [including the outer body contour] by three
  ;; items, a Density [a single flonum], a ZValue-List [list of Z coordinates
  ;; associated with each contour], and a Vertices-List [list of Vertices
  ;; associated with each contour].  The ZValue-List and Vertices-List must
  ;; correspond element-by-element with each other: the first ZValue is
  ;; associated with the first Vertex-List [sublist in the Vertices-List],
  ;; and so on.  Both lists are ordered by increasing ZValue.  Therefore, the
  ;; ZValue-List itself will be a linearly-ordered increasing sequence of
  ;; flonums, and the Vertices-List will be a list of the Vertex-Lists of the
  ;; corresponding contours.  Vertices and Z-Values are sorted by increasing
  ;; Z within an organ, but organs are not ordered with respect to each other.

  ;; Contours as stored in the PATIENT-ANATOMY passed in are not guaranteed
  ;; to be so ordered.  Therefore, we must build the sorted data structure
  ;; here before passing anything to PATHLENGTH-RAYTRACE, which assumes such
  ;; ordering.  PATHLENGTH-RAYTRACE also takes its arguments in this parallel
  ;; list format rather than as a list of ORGAN objects, so we also destructure
  ;; ORGAN objects while building the sorted lists.

  ;; ORGAN-ZVALS-LIST is a LIST of the ZValue-Lists, one element per organ.
  ;; ORGAN-VERTICES-LIST is a LIST of the Vertices-Lists, one per organ.
  ;; ORGAN-DENSITY-LIST is a LIST of the organ densities [single flonum value
  ;; representing the radiological density for each organ].  NB: each item
  ;; represents a LIST of organs, not a single organ.

  ;; Corresponding elements [by order] of these lists represent the same organ.
  ;; Then we CONS a single 0.0 to the front of ORGAN-DENSITY-LIST to represent
  ;; the zero density of the "contour at infinity" [ie, the air outside the
  ;; patient's body], for the convenience of PATHLENGTH-RAYTRACE's internals.
  ;; From this point on, the three lists correspond but ORGAN-DENSITY-LIST is
  ;; longer by one than the others, and its organ-by-organ correspondence is
  ;; shifted rearward by one element with respect to the other two.
  ;; ORGAN-DENSITY-LIST is returned as an array [second return value]
  ;; for easy random access.

  ;; We build these structures once before starting the dose calculation
  ;; loop, because these structures are invariant over an entire calculation.

  (declare (type list organ-vertices-list organ-zvals-list organ-density-list)
	   (type (or null float) the-density)
	   (type (member nil :Too-Large) density-flag))

  (dolist (organ-obj (coll:elements patient-anatomy))

    (when (setq the-density (density organ-obj))
      ;; Organ density of NIL -> ignore this organ in pathlength computation.
      (setq the-density (coerce the-density 'single-float)) ;Just in case ...
      (let ((organ-contour-objects (contours organ-obj)))
	(declare (type list organ-contour-objects))
	;; Check that organ has contours.
	(when (consp organ-contour-objects)
	  (let* ((first-contour-obj (first organ-contour-objects))
		 ;; Initialize accumulators to first of each input list.
		 ;; Then insert rest of elements in sorted order.
		 (organ-zvals (list (z first-contour-obj)))
		 (organ-vertices (list (vertices first-contour-obj))))
	    (declare (type list organ-zvals organ-vertices))
	    (do ((contour-objects (cdr organ-contour-objects)
				  (cdr contour-objects))
		 (the-object) (the-vertices))
		((null contour-objects)
		 ;; Done with organ - push each component onto output list.
		 (push organ-zvals organ-zvals-list)
		 (push organ-vertices organ-vertices-list)
		 (when (> the-density #.Tissue-Maximum-Density)
		   (setq density-flag :Too-Large))
		 (push the-density organ-density-list))
	      (declare (type list contour-objects the-vertices))
	      (setq the-object (car contour-objects))   ;Item being inserted
	      ;; Check that contours are legit.  If an empty one has slipped
	      ;; through, pass over it.  Otherwise PATHLENGTH-RAYTRACE crashes.
	      (when (consp (setq the-vertices (vertices the-object)))
		(do ((the-z (z the-object))         ;Its Z value - sort key
		     ;; Insertion-location pointers for ZValue.
		     (zvals-headptr organ-zvals (cdr zvals-headptr))
		     (zvals-tailptr nil zvals-headptr)
		     ;; Insertion-location pointers for Vertices-List
		     (verts-headptr organ-vertices (cdr verts-headptr))
		     (verts-tailptr nil verts-headptr))
		    ((null zvals-headptr)
		     ;; Didn't find insertion spot - append to ends of lists.
		     (setf (cdr zvals-tailptr) (list the-z))
		     (setf (cdr verts-tailptr) (list the-vertices)))
		  (declare (type single-float the-z))
		  ;; Scan for insertion point.
		  (when (< the-z (the single-float (car zvals-headptr)))
		    ;; Insert new element at this point and return.
		    (cond ((null zvals-tailptr) ;Insertion is at front of list.
			   (push the-z organ-zvals)
			   (push the-vertices organ-vertices))
			  (t (setf (cdr zvals-tailptr)
				   (cons the-z zvals-headptr))
			     (setf (cdr verts-tailptr)
				   (cons the-vertices verts-headptr))))
		    (return))))))))))

  (values organ-vertices-list

	  ;; ORGAN-Z-EXTENTS, which has contours-like format,
	  ;; but has prism ceiling and floor Z values.
	  ;; (((s1c1z- s1c1z+) (s1c2z- s1c2z+) (s1c3z- s1c3z+))
	  ;;  ((s2c1z- s2c1z+) (s2c2z- s2c2z+))
	  ;;  ((s3c1z- s3c1z+) (s3c2z- s3c2z+)))
	  (mapcar #'(lambda (a-strctr-zs a-strctr-zdiffs)
		      (mapcar #'(lambda (zval zval-m zval-p)
				  (declare (type single-float zval
						 zval-m zval-p))
				  (list (- zval zval-m) (+ zval zval-p)))
			a-strctr-zs
			(cons 0.0 a-strctr-zdiffs)
			(nconc a-strctr-zdiffs (list 0.0))))

	    organ-zvals-list

	    ;; A-STRCTR-ZDIFFS has same format as ORGAN-ZVALS-LIST but with
	    ;; one fewer elements per structure.  For each organ, it is a list
	    ;; of the half-widths [in Z-value] of the consecutive segments in
	    ;; ORGAN-ZVALS-LIST.
	    (mapcar
		#'(lambda (a-strctr-zs)
		    (mapcar
			#'(lambda (z-cntr1 z-cntr2)
			    (declare (type single-float z-cntr1 z-cntr2))
			    (* 0.5 (- z-cntr2 z-cntr1)))
		      a-strctr-zs (cdr a-strctr-zs)))
	      organ-zvals-list))

	  ;; ORGAN-DENSITY-ARRAY.
	  (make-array (the fixnum (1+ (length organ-density-list)))
		      :element-type 'single-float
		      :initial-contents (cons 0.0 organ-density-list))

	  ;; Flag indicating that no densities are present
	  ;; or that some organ density is out of range.
	  (if (null organ-density-list) :Missing density-flag)))

;;;=============================================================
;;; Main functions for computing radiological equivalent pathlength.
;;; This set of functions was written by Gavin Young as an implementation
;;; of his Master's thesis.

(defun pathlength-raytrace (arg-vec organ-vertices-list organ-z-extents
			    &aux (dp-x 0.0) (dp-y 0.0) (dp-z 0.0) (src-x 0.0)
			    (src-y 0.0) (src-z 0.0) (dx 0.0) (dy 0.0) (dz 0.0)
			    templist-1 templist-2)

  "pathlength-raytrace arg-vec organ-vertices-list organ-z-extents

returns a descriptor (list of prisms/densities) from which to compute
tissue-equivalent-pathlength from source (SRC-X, SRC-Y, SRC-Z)
to dose-point (DP-X, DP-Y, DP-Z), through anatomy represented by
ORGAN-VERTICES-LIST and ORGAN-Z-EXTENTS.  Args are stored in
ARG-VEC in slots named Argv-Src-X, Argv-Src-Y, Argv-Src-Z, Argv-Dp-X,
Argv-Dp-Y, and Argv-Dp-Z."

  (declare (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type list organ-vertices-list organ-z-extents
		 templist-1 templist-2)
	   (type single-float src-x src-y src-z dp-x dp-y dp-z dx dy dz))

  ;; Dereference 6 SINGLE-FLOAT arguments from argument vector.  These are
  ;; in place from original call to PATHLENGTH-RAYTRACE.  Load the two
  ;; "difference" locals for convenient passage to callees.
  (setq dp-x (aref arg-vec #.Argv-Dp-X)
	dp-y (aref arg-vec #.Argv-Dp-Y)
	dp-z (aref arg-vec #.Argv-Dp-Z)
	src-x (aref arg-vec #.Argv-Src-X)
	src-y (aref arg-vec #.Argv-Src-Y)
	src-z (aref arg-vec #.Argv-Src-Z)
	dx (- dp-x src-x)
	dy (- dp-y src-y)
	dz (- dp-z src-z))

  ;; Load args for ENCLOSES? - these args are fixed
  ;; for extent of this function call.
  (setf (aref arg-vec #.Argv-Enc-X) dp-x)
  (setf (aref arg-vec #.Argv-Enc-Y) dp-y)

  ;; Next expression has the ALPHAs for each prism's ceiling and floor
  ;; [ordered by decreasing alpha, irrespective of ceiling/floor Z-values]
  ;; and in same format as ORGAN-Z-EXTENTS.
  (dolist (strctr-z-extent organ-z-extents)
    (let ((tmp3 '()))
      (dolist (cntr-ze strctr-z-extent)
	(push (cond ((= dz 0.0)
		     (cons nil nil))
		    (t (let ((tmp1 (/ (- (the single-float (second cntr-ze))
					 src-z)
				      dz))
			     (tmp2 (/ (- (the single-float (first cntr-ze))
					 src-z)
				      dz)))
			 (cond ((> tmp1 tmp2)
				(cons tmp1 tmp2))
			       (t (cons tmp2 tmp1))))))
	      tmp3))
      (push (nreverse tmp3) templist-1)))

  (do ((szes organ-z-extents (cdr szes))
       (svs organ-vertices-list (cdr svs))
       (sacfs (nreverse templist-1) (cdr sacfs))
       (templist-3 nil nil))
      ((null szes))
    (declare (type list szes svs sacfs templist-3))
    ;; List1: List of ( prism-floor-Z prism-ceil-Z ) for each organ.
    ;; List2: List of vertex-lists for each organ.
    ;; List3: ( >-prism-alpha . <-prism-alpha ) for each organ.
    (do ((list1 (car szes) (cdr list1))
	 (list2 (car svs) (cdr list2))
	 (list3 (car sacfs) (cdr list3))
	 (prsm-z-e) (cntr) (prsm-alpha-c-f)
	 (z-minus 0.0) (z-plus 0.0))
	((null list1))
      (declare (type list prsm-z-e cntr prsm-alpha-c-f)
	       (type single-float z-minus z-plus))
      (setq prsm-z-e (car list1)
	    cntr (car list2)
	    prsm-alpha-c-f (car list3)
	    z-minus (first prsm-z-e)
	    z-plus (second prsm-z-e))
      (cond
	((or (and (< src-z z-minus)        ;SRC and DP both below prism floor.
		  (< dp-z z-minus))
	     (and (>= src-z z-plus)      ;SRC and DP both above prism ceiling.
		  (>= dp-z z-plus))))

	((and (= dp-x src-x)                  ;SRC->DP ray parallel to Z axis.
	      (= dp-y src-y))
	 (when (encloses? cntr arg-vec)             ;Ray intersects prism.
	   (setq templist-3 (nconc templist-3
				   (list (car prsm-alpha-c-f)
					 (cdr prsm-alpha-c-f))))))

	;; Otherwise, find all alphas for intersection of polygon described
	;; by CNTR and ray described by SRC and DP where alpha(SRC)=0 and
	;; alpha(DP)=1.  Assumes CNTR is legal [at least three points, no
	;; adjacent triples which are collinear, and non-self-intersecting].
	;; Rotate the vertices in CNTR until first and last are NOT on SRC->DP
	;; ray.  As long as CNTR has no collinear triples, we can do this
	;; in at most a single rotation.
	(t (setq templist-1 nil)
	   (let ((first-elem (first cntr))
		 (last-elem (car (last cntr))))
	     (let ((fex (first first-elem))
		   (fey (second first-elem))
		   (lex (first last-elem))
		   (ley (second last-elem)))
	       (declare (type single-float fex fey lex ley))
	       (when (and
		       ;; ZEROP First-Elem->SRC cross First-Elem->DP.
		       (= (* (- src-x fex)
			     (- dp-y fey))
			  (* (- src-y fey)
			     (- dp-x fex)))
		       ;; ZEROP Last-Elem->SRC cross Last-Elem->DP.
		       (= (* (- src-x lex)
			     (- dp-y ley))
			  (* (- src-y ley)
			     (- dp-x lex))))
		 (setq cntr (cons last-elem (butlast cntr))))))

	   ;; Replace consecutive vertices that lie exactly on the SRC->DP ray
	   ;; with one vertex at the midpoint of the line from one vertex to
	   ;; the other.  Does NOT link last vertex to first, so make sure
	   ;; (first, last) are NOT on SRC->DP ray.
	   (do ((cntr-tail cntr (cdr cntr-tail))
		(copy? t))
	       ((null (cdr cntr-tail)))
	     (declare (type list cntr-tail)
		      (type (member nil t) copy?))
	     (let ((v1 (first cntr-tail))
		   (v2 (second cntr-tail)))
	       (let ((v1x (first v1))
		     (v1y (second v1))
		     (v2x (first v2))
		     (v2y (second v2)))
		 (declare (type single-float v1x v1y v2x v2y))
		 (when (and
			 ;; ZEROP SRC->V1 cross SRC->DP
			 (= (* (- v1x src-x) dy)
			    (* (- v1y src-y) dx))
			 ;; ZEROP SRC->V2 cross SRC->DP
			 (= (* (- v2x src-x) dy)
			    (* (- v2y src-y) dx)))
		   (let ((new-vertex
			   (list (* 0.5 (+ v1x v2x)) (* 0.5 (+ v1y v2y)))))
		     (cond
		       ((eq cntr cntr-tail)
			(setq cntr-tail
			      (setq cntr (cons new-vertex (cddr cntr)))))
		       (copy?
			 ;; Found a pair of vertices to merge.  Must copy the
			 ;; entire chain so as to avoid damaging shared list
			 ;; structure.  From then on we can make all changes
			 ;; destructively to this new copy, returning it when
			 ;; done as final value of the function.
			 ;;
			 ;; Duplicate chain from start to split point [up to
			 ;; but not including first vertex of pair to be
			 ;; merged].  Leave CNTR-TAIL pointing to last CONS
			 ;; of this chain, so that its successive CDRs can be
			 ;; changed if new vertices need to be spliced into the
			 ;; chain.  After appending the new collapsed vertex
			 ;; we must copy the rest of the chain, in case any
			 ;; further modifications are necessary [if not, we
			 ;; have wasted a few CONS cells that could have been
			 ;; shared, but we have save considerable complexity
			 ;; - and this case should arise extremely rarely].
			 (do ((accum '())
			      (head cntr (cdr head)))
			     ((eq head cntr-tail)
			      (setq cntr-tail accum)
			      (setq cntr (nreverse accum))
			      ;; After NREVERSE, CNTR-TAIL points to last CONS
			      ;; of chain in list which is new value of CNTR.
			      ;; Splice in new vertex followed by copied tail
			      ;; of original list.
			      (setf (cdr cntr-tail)
				    (setq cntr-tail
					  (cons new-vertex
						(copy-list (cddr cntr-tail)))))
			      (setq copy? nil))
			   (push (car head) accum)))
		       ;;
		       ;; In case just above and that to follow, we end with
		       ;; CNTR-TAIL pointing at the CONS cell whose CAR is the
		       ;; new collapsed vertex.  On next iteration we examine
		       ;; the next two vertices AFTER collapsed one.  We know
		       ;; that those two cannot be collinear with the collapsed
		       ;; one or else all three uncollapsed vertices would have
		       ;; been collinear beforehand - and we checked for that
		       ;; before starting PATHLENGTH-RAYTRACE.
		       ;;
		       ;; List already copied.  Since we can make changes
		       ;; destructively, we can reuse the current CONS cell
		       ;; [one pointed to by CNTR-TAIL, whose CAR is first
		       ;; vertex of pair to be merged] to contain instead
		       ;; [set its CAR to point to] the new collapsed vertex
		       ;; and set its CDR to point to rest of list just beyond
		       ;; the merged pair.  This avoids necessity of keeping
		       ;; a pointer to one cell back in the list for appending
		       ;; to it the new vertex. Splice in collapsed vertex.
		       (t (setf (car cntr-tail) new-vertex)
			  ;; Skip over second of pair.
			  (setf (cdr cntr-tail) (cddr cntr-tail)))))))))

	   (prog ((vs-pre) (vs-nxt) (tmpvert) (vrtx-cur) (vrtx-nxt)
		  (cp-prev 0.0) (cp-curr 0.0) (cp-next 0.0))
	     (declare (type single-float cp-prev cp-curr cp-next))
	     (setq vs-pre cntr
		   tmpvert (car vs-pre)
		   cp-prev (- (* (- (the single-float (first tmpvert)) src-x)
				 dy)
			      (* (- (the single-float (second tmpvert)) src-y)
				 dx))
		   tmpvert (cdr vs-pre)
		   vrtx-cur (car tmpvert)
		   cp-curr (- (* (- (the single-float (first vrtx-cur)) src-x)
				 dy)
			      (* (- (the single-float (second vrtx-cur)) src-y)
				 dx))
		   vs-nxt (cdr tmpvert)
		   vrtx-nxt (car vs-nxt)
		   cp-next (- (* (- (the single-float (first vrtx-nxt)) src-x)
				 dy)
			      (* (- (the single-float (second vrtx-nxt)) src-y)
				 dx)))

	     LOOP1
	     (when (or (and (> cp-curr 0.0) (< cp-next 0.0))
		       (and (< cp-curr 0.0) (> cp-next 0.0))
		       (and (= cp-curr 0.0) (> cp-next 0.0) (< cp-prev 0.0))
		       (and (= cp-curr 0.0) (< cp-next 0.0) (> cp-prev 0.0)))
	       ;; / first arg is SRC->VRTX-CUR cross SRC->VRTX-NXT.
	       (push (/ (- (* (- (the single-float (first vrtx-cur)) src-x)
			      (- (the single-float (second vrtx-nxt)) src-y))
			   (* (- (the single-float (second vrtx-cur)) src-y)
			      (- (the single-float (first vrtx-nxt)) src-x)))
			(- cp-curr cp-next))
		     templist-1))
	     (cond
	       ((null (setq vs-pre (cdr vs-pre))))
	       (t (setq vs-nxt (or (cdr vs-nxt) cntr)
			vrtx-cur vrtx-nxt
			vrtx-nxt (car vs-nxt)
			cp-prev cp-curr
			cp-curr cp-next
			cp-next (- (* (- (the single-float (first vrtx-nxt))
					 src-x)
				      dy)
				   (* (- (the single-float (second vrtx-nxt))
					 src-y)
				      dx)))
		  (go LOOP1))))

	   (unless (= dp-z src-z)                 ;Compute line intersections.
	     (do ((alphas templist-1 (cdr alphas))
		  (p-a-c-f-1 (car prsm-alpha-c-f))
		  (p-a-c-f-2 (cdr prsm-alpha-c-f))
		  (alpha 0.0))
		 ((null alphas))
	       (declare (type single-float alpha p-a-c-f-1 p-a-c-f-2))
	       (setq alpha (car alphas))
	       (cond ((> alpha p-a-c-f-1)
		      (setf (car alphas) p-a-c-f-1))
		     ((< alpha p-a-c-f-2)
		      (setf (car alphas) p-a-c-f-2)))))

	   (setq templist-3
		 (nconc templist-3
			(remove-duplicates-by-pairs (sort templist-1 #'<)))))))

    (push (remove-duplicates-by-pairs (sort templist-3 #'<)) templist-2))

  (setq templist-1 nil)
  (do ((strctr-list templist-2 (cdr strctr-list))
       (strctr-tag (length templist-2) (the fixnum (1- strctr-tag))))
      ((null strctr-list))
    (declare (type fixnum strctr-tag))
    (do ((alphas (car strctr-list) (cdr alphas))
	 (alpha 0.0))
	((null alphas))
      (declare (type single-float alpha))
      (setq alpha (car alphas))
      (cond ((< alpha 0.0)
	     (push (cons 0.0 strctr-tag) templist-1))
	    ((< alpha 1.0)
	     ;; Pushing ALPHAs normalized to fixed ray length of
	     ;; Pathlength-Ray-Maxlength (400.0) centimeters.
	     (push (cons (* #.Pathlength-Ray-Maxlength alpha) strctr-tag)
		   templist-1)))))

  (when (consp (cdr templist-1))
    (setq templist-1 (sort templist-1 #'< :key #'car))
    (prog ((items1 templist-1) items2 items3 item1 item2 item3)
      (setq items2 (cdr items1))
      (unless (consp (setq items3 (cdr items2)))
	(return))
      (setq item1 (car items1)
	    item2 (car items2)
	    item3 (car items3))
      LOOP2
      (cond ((and (= (the single-float (car item2))
		     (the single-float (car item3)))
		  (= (the fixnum (cdr item1))
		     (the fixnum (cdr item3))))
	     ;; Possible mistake here.
	     (setf (cdr items1)
		   (setq items1 (list* item3 item2
				       (setq items3 (cdr items3)))))
	     (setq items2 (cdr items1))
	     (when (consp items3)
	       (setq item1 item3 item3 (car items3))
	       (go LOOP2)))
	    (t (setq items1 items2 items2 items3)
	       (when (consp (setq items3 (cdr items3)))
		 (setq item1 item2 item2 item3 item3 (car items3))
		 (go LOOP2))))))

  templist-1)

;;;-------------------------------------------------------------
;;; PATHLENGTH-INTEGRATE is only called if RAY-ALPHALIST is a non-NIL list,
;;; meaning that the ray passes through the patient.  If called on null ray,
;;; it returns an effective pathlength of zero.  Function return value is
;;; flag indicating whether dosepoint is inside body or not.

(defun pathlength-integrate (arg-vec ray-alphalist organ-density-array
			     homogeneity-mode
			     &aux (ray-length (aref arg-vec #.Argv-Raylen))
			     homogeneous? heterogeneous?)

  "pathlength-integrate arg-vec ray-alphalist organ-density-array
			homogeneity-mode

computes tissue-equivalent-pathlength from source to dose-point (given this
distance as the Argv-Raylen slot of ARG-VEC) from descriptor generated by
PATHLENGTH-RAYTRACE.  If HOMOGENEITY-MODE is :Heterogeneous, the function
includes densities in the anatomic structures; if :Homogeneous, assumes them
to be 1.0, giving Euclidean distance from patient surface to dose-point along
the beam; and if :Both it does both calculations.  Returns effective pathlength
whether ray intersects patient or not [zero if not]; returns homogeneous result
in ARG-VEC Argv-Return-0 and density-corrected result in slot Argv-Return-1.
Function returns T or NIL indicating whether dosepoint is inside body or not."

  (declare (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type (simple-array single-float 1) organ-density-array)
	   (type list ray-alphalist)
	   (type (member :Homogeneous :Heterogeneous :Both) homogeneity-mode)
	   (type (member nil t) homogeneous? heterogeneous?)
	   (type single-float ray-length))

  (cond ((eq homogeneity-mode :Heterogeneous)
	 (setq heterogeneous? t))
	((eq homogeneity-mode :Both)
	 (setq homogeneous? t heterogeneous? t))
	(t (setq homogeneous? t)))

  (do ((last-alpha 0.0 current-alpha)
       (strctr-stack (list 0))
       (alpha-pairlist ray-alphalist (cdr alpha-pairlist))
       (alpha-item) (homogeneous-sum 0.0) (heterogeneous-sum 0.0)
       (current-alpha 0.0) (strctr-tag 0) (strctr-tag-pop 0))
      ((null alpha-pairlist)
       (setf (aref arg-vec #.Argv-Return-0) homogeneous-sum)
       (setf (aref arg-vec #.Argv-Return-1) heterogeneous-sum)
       nil)                                        ;Dosepoint outside patient.

    (declare (type list strctr-stack alpha-pairlist alpha-item)
	     (type single-float last-alpha current-alpha
		   homogeneous-sum heterogeneous-sum)
	     (type fixnum strctr-tag strctr-tag-pop))

    (setq alpha-item (car alpha-pairlist)
	  current-alpha (car alpha-item)
	  strctr-tag (cdr alpha-item)
	  strctr-tag-pop (car strctr-stack))

    (cond ((< current-alpha ray-length)
	   (cond ((= strctr-tag strctr-tag-pop)
		  (setq strctr-stack (cdr strctr-stack)))
		 (t (push strctr-tag strctr-stack))))

	  ((cdr strctr-stack)                       ;Dosepoint inside patient.
	   (when homogeneous?
	     (incf homogeneous-sum (the single-float
				     (* (- ray-length last-alpha)
					(if (= strctr-tag-pop 0) 0.0 1.0)))))
	   (when heterogeneous?
	     (incf heterogeneous-sum
		   (the single-float
		     (* (- ray-length last-alpha)
			(the single-float
			  (aref organ-density-array strctr-tag-pop))))))
	   (setf (aref arg-vec #.Argv-Return-0) homogeneous-sum)
	   (setf (aref arg-vec #.Argv-Return-1) heterogeneous-sum)
	   (return t))

	  ;; Done - dosepoint outside patient, but ray may or may not
	  ;; have passed through patient.
	  (t (setf (aref arg-vec #.Argv-Return-0) homogeneous-sum)
	     (setf (aref arg-vec #.Argv-Return-1) heterogeneous-sum)
	     (return nil)))

    (when homogeneous?
      (incf homogeneous-sum (the single-float
			      (* (- current-alpha last-alpha)
				 (if (= strctr-tag-pop 0) 0.0 1.0)))))
    (when heterogeneous?
      (incf heterogeneous-sum
	    (the single-float
	      (* (- current-alpha last-alpha)
		 (the single-float
		   (aref organ-density-array strctr-tag-pop))))))))

;;;-------------------------------------------------------------

(defun remove-duplicates-by-pairs (intersec-list)

  ;; Remove [destructively] duplicate entries from the sorted INTERSEC-LIST
  ;; by pairs - two at a time.
  (do ((prev-cons nil)                   ;CONS one back - for splicing its CDR
       (test-cons intersec-list) ;CONS containing first element of comparison.
       ;; CONS containing second element of comparison.
       (next-cons (cdr intersec-list)))
      ((null next-cons)
       intersec-list)
    (cond ((= (the single-float (car test-cons))
	      (the single-float (car next-cons)))
	   (cond ((consp prev-cons)
		  (setf (cdr prev-cons) (setq test-cons (cdr next-cons))))
		 (t (setq intersec-list (cdr next-cons)
			  test-cons intersec-list)))
	   (setq next-cons (cdr test-cons)))
	  (t (setq prev-cons test-cons
		   test-cons next-cons
		   next-cons (cdr next-cons))))))

;;;=============================================================
;;; Ray-edge crossing-counter algorithm.
;;; Fast version - calculates only one ray.

(defun encloses? (vlist arg-vec &aux (px (aref arg-vec #.Argv-Enc-X))
		  (py (aref arg-vec #.Argv-Enc-Y)))

  ;; As VLIST is an open list representing a closed contour, there is an
  ;; implied edge present from last to first vertex.  Traversal can be in
  ;; either direction, CW or CCW.

  (declare (type list vlist)
	   (type (simple-array single-float (#.Argv-Size)) arg-vec)
	   (type single-float px py))

  (do ((verts vlist (or (cdr verts) vlist))
       (endmarker (cdr vlist))
       (vert)                                       ;Actual Vertex
       (bx 0.0) (by 0.0)                            ;Coords of BACK point
       (mx 0.0) (my 0.0)                            ;Coords of CURRENT point
       (fx 0.0) (fy 0.0)                            ;Coords of FWD point
       (mxby 0.0) (bxmy 0.0)                        ;Cross-terms
       (back-point nil)                             ;Status flag
       ;; Axis-ray crosser - known which half - parity counts:
       (x+ nil))
      ((and (eq back-point :Done)
	    (eq verts endmarker))
       x+)

    (declare (type list verts vert endmarker)
	     (type (member nil :Set :Done) back-point)
	     (type (member nil t) x+)
	     (type single-float bx by mx my fx fy mxby bxmy))

    (when (eq back-point :Set)
      (setq back-point :Done))

    (setq vert (car verts)
	  mx (- (the single-float (first vert)) px)
	  my (- (the single-float (second vert)) py))

    (cond ((and (= mx 0.0) (= my 0.0))
	   ;; If any vertex matches test point, return T.
	   (return t))

	  ;; If test point is on test ray, then if vertex before and vertex
	  ;; after current are on same side of ray, pull current vertex an "
	  ;; infinitessimal" distance away in same direction.  Otherwise [
	  ;; contour crosses test ray at current vertex] push current vertex
	  ;; an "infinitessimal" distance in other direction.  Thus MX and
	  ;; therefore BX never are exactly zero in decision tree to follow.
	  ((= mx 0.0)
	   (setq vert (or (second verts) (first vlist))
		 fx (- (the single-float (first vert)) px))
	   (cond ((> bx 0.0)
		  (cond ((>= fx 0.0)
			 (incf mx 1.0e-8))
			(t (decf mx 1.0e-8))))
		 ((<= fx 0.0)
		  (decf mx 1.0e-8))
		 (t (incf mx 1.0e-8))))

	  ;; Exactly equivalent logic but interchanging X and Y axes.
	  ((= my 0.0)
	   (setq vert (or (second verts) (first vlist))
		 fy (- (the single-float (first vert)) py))
	   (cond ((> by 0.0)
		  (cond ((>= fy 0.0)
			 (incf my 1.0e-8))
			(t (decf my 1.0e-8))))
		 ((<= fy 0.0)
		  (decf my 1.0e-8))
		 (t (incf my 1.0e-8)))))

    (cond ((null back-point)
	   ;; Preset BX, BY on first iter only - will never = 0.0 exactly.
	   (setq bx mx by my back-point :Set))

	  ;; Decision tree testing contour-segment/test-ray crossings.
	  ((> mx 0.0)
	   (cond ((> my 0.0)
		  (cond ((< by 0.0)
			 (cond ((> bx 0.0)
				(setq x+ (not x+)))
			       ((= (setq mxby (* mx by))
				   (setq bxmy (* bx my)))
				(return t))
			       ((< mxby bxmy)
				(setq x+ (not x+)))))))
		 ((> by 0.0)
		  (cond ((> bx 0.0)
			 (setq x+ (not x+)))
			((= (setq mxby (* mx by))
			    (setq bxmy (* bx my)))
			 (return t))
			((> mxby bxmy)
			 (setq x+ (not x+))))))
	   (setq bx mx by my))

	  ((> my 0.0)
	   (cond ((< by 0.0)
		  (cond ((< bx 0.0))
			((= (setq mxby (* mx by))
			    (setq bxmy (* bx my)))
			 (return t))
			((< mxby bxmy)
			 (setq x+ (not x+))))))
	   (setq bx mx by my))

	  ((> by 0.0)
	   (cond ((< bx 0.0))
		 ((= (setq mxby (* mx by))
		     (setq bxmy (* bx my)))
		  (return t))
		 ((> mxby bxmy)
		  (setq x+ (not x+))))
	   (setq bx mx by my))

	  (t (setq bx mx by my)))))

;;;=============================================================
;;; End.
