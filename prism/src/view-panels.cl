;;;
;;; view-panels
;;;
;;; This is the implementation of Prism view-panels.  A view-panel is
;;; the frame that contains the view buttons and other controls.
;;; When a view-panel is created, the picture which is part of the
;;; view is mapped as a child window in the panel frame.
;;;
;;; 30-Jun-1992 I. Kalet started, from earlier views module
;;; 31-Jul-1992 I. Kalet use apply to make buttons and stuff, add
;;; destroy method, and title.
;;; 22-Aug-1992 I. Kalet add busy bit/mediator code for position
;;; 16-Sep-1992 I. Kalet use generic-panel base class, add
;;; remove-notify for (new-pos view)
;;; 25-Oct-1992 I. Kalet use window, not pixmap to get view size
;;; 06-Nov-1992 J. Unger reconfigure placement of buttons/textlines,
;;; add functionality for window/level.
;;; 30-Nov-1992 J. Unger modify button config to save space.
;;; 13-Dec-1992 I. Kalet change image-displayed to
;;; background-displayed
;;; 21-Dec-1992 I. Kalet coerce view-position, scale to single-float
;;; 15-Feb-1993 I. Kalet change scale to slider, not textline
;;; 24-Apr-1993 I. Kalet parametrize button positions, fix doc.
;;;  5-Nov-1993 I. Kalet disable scale slider if background displayed
;;; 31-Jan-1994 J. Unger add code for Plot View button.
;;; 15-Jun-1994 I. Kalet make numeric input textlines check validity
;;; 12-Jan-1995 I. Kalet cache reference to plan, patient here, pass to
;;; interactive-make-plot, instead of using back-pointer in view.
;;; 30-Apr-1997 I. Kalet add name textline to view panel
;;; 17-Jul-1998 I. Kalet add a button and menu subpanel to select what
;;; objects are visible in the view, in addition to image on/off
;;; button.
;;; 21-Jul-1998 I. Kalet fix error in declutter menu.
;;; 12-Aug-1998 I. Kalet add action for toggle of background-displayed.
;;; 11-Mar-1999 I. Kalet change window and level controls to sliderboxes.
;;; 10-Apr-1999 C. Wilcox added support for interruptable background drr's.
;;;  4-Mar-2000 I. Kalet added long awaited tape measure in views.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 16-Jul-2000 I. Kalet allow zoom and pan in views, supported by OpenGL.
;;; 16-Dec-2000 I. Kalet set title to initial value of view name.
;;; 11-Mar-2001 I. Kalet if BEV, update title when view name changes.
;;; 17-Mar-2002 I. Kalet change interactive-make-plot to
;;; make-plot-panel, add a slot for a plot panel, not a dialog box.
;;; 31-Jul-2002 I. Kalet add oblique-view-panel with rotation dials
;;;  7-Aug-2002 J. Sager add room-view-panel support
;;; 19-Aug-2002 J. Sager modify image-button action, since room-views
;;; need the background displayed always to render.
;;; 23-Sep-2002 I. Kalet minor mods to clean up, also for room view,
;;; viewlist panel should reference gl-prims instead of foreground.
;;; 29-Jan-2003 I. Kalet increase upper limit on scale control.
;;;  1-Feb-2003 I. Kalet move default method for name to prism-objects.
;;; 12-May-2003 M. Phillips added button to viewlist-panel that clears
;;; all objects from view.
;;; 25-May-2009 I. Kalet remove room-view-panel support
;;;

(in-package :prism)

;;;----------------------------------------------------

(defclass view-panel (generic-panel)

  ((view-frame :accessor view-frame
	       :documentation "The SLIK frame that holds all the view
stuff, including the actual picture, and the controls.")

   (view-for :accessor view-for
	     :initarg :view-for
	     :documentation "The view this panel contains.")
   
   (plan-of :initarg :plan-of
	    :accessor plan-of
	    :documentation "The plan containing the view.")
   
   (patient-of :initarg :patient-of
	       :accessor patient-of
	       :documentation "The current patient.")

   (delete-view :accessor delete-view
		:documentation "The DELETE button, for delete panel.")

   (plot-view :accessor plot-view
	      :documentation "The PLOT VIEW button.")

   (plot-panel :accessor plot-panel
	       :documentation "A temporary storage for the plot
control panel, no longer a dialog box")

   (name-box :accessor name-box
	     :documentation "Textline for view name.")

   (local-bar-button :accessor local-bar-button
		     :documentation "The button that toggles the
locator bars in this view.")

   (remote-bar-button :accessor remote-bar-button
		      :documentation "The button that toggles the
locator bars for this view in the other views if any.")

   (image-button :accessor image-button
		 :documentation "The button that toggles display of
image data in this view.")

   (fg-button :accessor fg-button
	      :documentation "Brings up a popup menu of objects in the
view to display them or not on a view by view basis.")

   (viewlist-panel :accessor viewlist-panel
		   :initform nil
		   :documentation "Temporarily holds the list of
objects visible on the screen.")

   (position-control :accessor position-control
		     :documentation "The control, a textline or
slider, that displays and sets the position of the view.")

   (busy :accessor busy
	 :initform nil
	 :documentation "The mediator busy bit for controlling updates 
between the view panel controls and the view attributes.")

   (scale-control :accessor scale-control
		  :documentation "A slider that displays and sets the
scale factor for the view.")

   (window-control :accessor window-control
		   :documentation "The textline that displays and sets
the window for the view's image.")

   (level-control :accessor level-control
		  :documentation "The textline that displays and sets
the level for the view's image.")

   (tape-measure-btn :accessor tape-measure-btn
		     :documentation "The Ruler button.  Pressing it
causes a tape measure to appear in the display area.")

   )

  (:documentation "The view-panel class contains the view controls and
the frame containing them and the view picture.")

  )

;;;-------------------------------------

(defun make-view-panel (v &rest other-initargs)

  (apply #'make-instance
	 (cond ((typep v 'oblique-view) 'oblique-view-panel)
	       (T 'view-panel))
	 :view-for v other-initargs))

;;;-------------------------------------

(defmethod initialize-instance :after ((vp view-panel) &rest initargs)

  (let* ((v (view-for vp))
	 (pic (picture v))
	 (win (sl:window pic))
	 (pic-w (clx:drawable-width win))
	 (pic-h (clx:drawable-height win))
	 (vpf (symbol-value *small-font*))
	 (vf (apply #'sl:make-frame (+ pic-w 140) pic-h
		    :title (name v)
		    initargs))
	 (vw (sl:window vf))
	 (sbw 55) ; small button width
	 (lbw 120) ; large button width
	 (bh 25) ; button height
	 (dx 10) ; left margin
	 (rx (+ dx sbw 10)) ; middle x placement
	 (top-y 8)) ; top button y placement
    (setf (view-frame vp) vf
	  (delete-view vp) (apply #'sl:make-button sbw bh 
				  :parent vw
				  :button-type :momentary
				  :font vpf :label "Del Pan"
				  :ulc-x dx :ulc-y top-y initargs)
	  (plot-view vp) (apply #'sl:make-button sbw bh
				:parent vw
				:font vpf :label "Plot"
				:ulc-x rx :ulc-y top-y initargs)
	  (name-box vp) (apply #'sl:make-textline lbw bh
			       :parent vw
			       :ulc-x dx :ulc-y (bp-y top-y bh 1)
			       :font vpf initargs)
	  (local-bar-button vp) (apply #'sl:make-button sbw bh
				       :parent vw
				       :font vpf :label "Local"
				       :ulc-x dx
				       :ulc-y (bp-y top-y bh 2)
				       initargs)
	  (remote-bar-button vp) (apply #'sl:make-button sbw bh
					:parent vw
					:font vpf :label "Remote"
					:ulc-x rx
					:ulc-y (bp-y top-y bh 2)
					initargs)
	  (image-button vp) (apply #'sl:make-button sbw bh
				   :parent vw
				   :font vpf :label "Image"
				   :ulc-x dx
				   :ulc-y (bp-y top-y bh 3)
				   initargs)
	  (fg-button vp) (apply #'sl:make-button sbw bh
				:parent vw
				:font vpf :label "Objects"
				:ulc-x rx
				:ulc-y (bp-y top-y bh 3)
				initargs)
	  (position-control vp) (apply #'sl:make-textline lbw bh
				       :parent vw
				       :font vpf :label "Pos: "
				       :ulc-x dx
				       :ulc-y (bp-y top-y bh 4)
				       :numeric t
				       :lower-limit -500.0
				       :upper-limit 500.0
				       initargs)
	  (scale-control vp) (apply #'sl:make-slider lbw bh 5.0 100.0
				    :parent vw
				    :ulc-x dx
				    :ulc-y (bp-y top-y bh 5)
				    initargs)
	  (window-control vp) (apply #'sl:make-sliderbox
				     lbw bh 1.0 2047.0 9999.0
				     :parent vw
				     :font vpf :label "Win: "
				     :ulc-x (- dx 5)
				     :ulc-y (bp-y top-y bh 6)
				     :border-width 0
				     :display-limits nil
				     initargs)
	  (level-control vp) (apply #'sl:make-sliderbox
				    lbw bh 1.0 4095.0 9999.0
				    :parent vw
				    :font vpf :label "Lev: "
				    :ulc-x (- dx 5)
				    :ulc-y (bp-y top-y bh 8)
				    :border-width 0
				    :display-limits nil
				    initargs)
	  (tape-measure-btn vp) (apply #'sl:make-button lbw bh
				       :parent vw
				       :font vpf :label "Ruler"
				       :ulc-x dx
				       :ulc-y (+ (bp-y top-y bh 10) 5)
				       :button-type :momentary
				       initargs))
    (ev:add-notify vp (sl:button-on (delete-view vp))
		   #'(lambda (panel bt)
		       (declare (ignore bt))
		       (destroy panel)))
    (ev:add-notify vp (sl:button-on (plot-view vp))
		   #'(lambda (pan bt)
		       (setf (plot-panel pan)
			 (make-plot-panel (view-for pan) vp
					  (plan-of pan)
					  (patient-of pan)))
		       (ev:add-notify pan (deleted (plot-panel pan))
				      #'(lambda (pnl ptpnl)
					  (declare (ignore ptpnl))
					  (setf (plot-panel pnl) nil)
					  (when (not (busy pnl))
					    (setf (busy pnl) t)
					    (setf (sl:on bt) nil)
					    (setf (busy pnl) nil))))))
    (ev:add-notify vp (sl:button-off (plot-view vp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (destroy (plot-panel pan))
			 (setf (busy pan) nil))))
    
    (setf (sl:info (name-box vp)) (name v))
    (ev:add-notify vp (sl:new-info (name-box vp))
		   #'(lambda (pan bx inf)
		       (declare (ignore bx))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (name (view-for pan)) inf)
			 (setf (busy pan) nil))))
    (ev:add-notify vp (new-name v)
		   #'(lambda (pan vw nn)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (name-box pan)) nn)
			 (if (typep vw 'beams-eye-view)
			     (setf (sl:title (view-frame pan)) nn))
			 (setf (busy pan) nil))))
    (setf (sl:on (local-bar-button vp)) (local-bars-on v))
    (ev:add-notify v (sl:button-on (local-bar-button vp))
		   #'(lambda (vu pan)
		       (declare (ignore pan))
		       (setf (local-bars-on vu) t)))
    (ev:add-notify v (sl:button-off (local-bar-button vp))
		   #'(lambda (vu pan)
		       (declare (ignore pan))
		       (setf (local-bars-on vu) nil)))
    (setf (sl:on (remote-bar-button vp)) t) ; locators are always
					; created visible
    (ev:add-notify v (sl:button-on (remote-bar-button vp))
		   #'(lambda (vu pan)
		       (declare (ignore pan))
		       (ev:announce vu (remote-bars-toggled vu) t)))
    (ev:add-notify v (sl:button-off (remote-bar-button vp))
		   #'(lambda (vu pan)
		       (declare (ignore pan))
		       (ev:announce vu (remote-bars-toggled vu) nil)))
    (setf (sl:info (position-control vp)) (view-position v))
    (ev:add-notify vp (sl:new-info (position-control vp))
		   #'(lambda (pan pc inf)
		       (declare (ignore pc))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (view-position (view-for pan))
			   (coerce (read-from-string inf) 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify vp (new-position v)
		   #'(lambda (pan vw pos)
		       (declare (ignore vw))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (position-control pan)) pos)
			 (setf (busy pan) nil))))
    (setf (sl:setting (scale-control vp)) (scale v))
    (ev:add-notify vp (sl:value-changed (scale-control vp))
		   #'(lambda (pan sc val)
		       (declare (ignore sc))
		       (let ((vf (view-for pan)))
			 (when (not (busy pan))
			   (setf (busy pan) t)
			   (setf (scale vf) val)
			   (setf (busy pan) nil)))))
    (ev:add-notify vp (new-scale v)
		   #'(lambda (pan vw val)
		       (declare (ignore vw))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (scale-control pan)) val)
			 (setf (busy pan) nil))))
    (setf (sl:setting (window-control vp))
      (coerce (window v) 'single-float))
    (ev:add-notify vp (sl:value-changed (window-control vp))
		   #'(lambda (pan wc win)
		       (declare (ignore wc))
		       (setf (window (view-for pan)) (round win))))
    (setf (sl:setting (level-control vp))
      (coerce (level v) 'single-float))
    (ev:add-notify vp (sl:value-changed (level-control vp))
		   #'(lambda (pan lc lev)
		       (declare (ignore lc))
		       (setf (level (view-for pan)) (round lev))))
    (when (typep (view-for vp) 'beams-eye-view)
      (setf (image-button v) (image-button vp))
      ;; we want the side effects from setf drr-state
      (setf (drr-state v) (drr-state v)) )
    (setf (sl:on (image-button vp)) (background-displayed v))
    (ev:add-notify vp (sl:button-on (image-button vp))
		   #'(lambda (pan bt)
                       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (view-for pan)) t)
			 (setf (busy pan) nil))))
    (ev:add-notify vp (sl:button-off (image-button vp))
		   #'(lambda (pan bt)
                       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (view-for pan)) nil)
			 (setf (busy pan) nil))))
    (ev:add-notify vp (sl:button-2-on (image-button vp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (when (typep (view-for pan) 'beams-eye-view)
			   (case (drr-state (view-for pan))
			     ;;'stopped is a noop
			     ('running
			      (setf (drr-state (view-for pan)) 'paused))
			     ('paused
			      (setf (drr-state (view-for pan)) 'running)
			      (drr-bg (view-for pan)))))
			 (setf (busy pan) nil))))
    (ev:add-notify vp (bg-toggled v)
		   #'(lambda (pan vw)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:on (image-button pan))
			   (background-displayed vw))
			 (setf (busy pan) nil))))
    (ev:add-notify vp (sl:button-on (fg-button vp))
		   #'(lambda (pan bt)
		       (setf (viewlist-panel pan)
			 (make-instance 'viewlist-panel
			   :view (view-for pan)))
		       (ev:add-notify pan (deleted (viewlist-panel
						    pan))
				      #'(lambda (pnl vlpnl)
					  (declare (ignore vlpnl))
					  (setf (viewlist-panel pnl) nil)
					  (when (not (busy pnl))
					    (setf (busy pnl) t)
					    (setf (sl:on bt) nil)
					    (setf (busy pnl) nil))))))
    (ev:add-notify vp (sl:button-off (fg-button vp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (destroy (viewlist-panel pan))
			 (setf (busy pan) nil))))
    (ev:add-notify vp (sl:button-on (tape-measure-btn vp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (let ((vw (view-for pan)))
			 (unless (tape-measure vw) 
			   (let ((center (/ (sl:width (picture vw)) 2))
				 (x-origin (x-origin vw))
				 (y-origin (y-origin vw))
				 (scale (scale vw)))
			     (setf (tape-measure vw)
			       (make-tape-measure
				:picture (picture vw)
				:scale scale
				:origin (list x-origin y-origin)
				:x1 (cm-x (- center 20) x-origin scale)
				:y1 (cm-x (- center 20) y-origin scale)
				:x2 (cm-x (+ center 20) x-origin scale)
				:y2 (cm-x (+ center 20) y-origin scale)))
			     (setf (sl:label (tape-measure-btn pan)) 
			       (write-to-string (fix-float
						 (tape-length
						  (tape-measure vw)) 2)))
			     (ev:add-notify pan (new-length (tape-measure vw))
					    #'(lambda (pnl tp len)
						(declare (ignore tp))
						(setf (sl:label
						       (tape-measure-btn pnl)) 
						  (write-to-string
						   (fix-float len 2)))))
			     (ev:add-notify vw (refresh (tape-measure vw))
					    #'(lambda (vu tp)
						(declare (ignore tp))
						(display-view vu)))
			     (ev:add-notify pan (deleted (tape-measure vw))
					    #'(lambda (pnl tp)
						(declare (ignore tp))
						(let ((vu (view-for pnl)))
						  (setf
						      (sl:label
						       (tape-measure-btn pnl))
						    "Ruler")
						  (setf (tape-measure vu) nil)
						  (display-view vu))))
			     (display-view vw))))))
    (clx:reparent-window (sl:window pic) vw 140 0)
    (clx:map-window (sl:window pic))
    (sl:flush-output)))

;;;--------------------------------------

(defclass oblique-view-panel (view-panel)

  ((azi-dialbox :accessor azi-dialbox
		:documentation "The dialbox that controls the azimuth
angle of the view plane.")

   (alt-dialbox :accessor alt-dialbox
		:documentation "The dialbox that controls the altitude
angle of the view plane.")

   )

  (:documentation "An oblique view panel has two additional controls,
to tilt the plane to arbitrary orientations.")

  )

;;;--------------------------------------

(defmethod initialize-instance :after ((ovp oblique-view-panel) &rest initargs)

  (let* ((vpf (symbol-value *small-font*))
	 (vf (view-frame ovp))
	 (vpw (sl:window vf))
	 (sbw 50) ; small button width
	 (bh 25) ; button height
	 (dx 0) ; left margin
	 (rx (+ dx sbw 20)) ; middle x placement
	 (top-y 8)) ; top button y placement
    (setf (azi-dialbox ovp)
      (apply #'sl:make-dialbox 25
	     :parent vpw
	     :font vpf :label "Azi:"
	     :ulc-x dx
	     :ulc-y (+ (bp-y top-y bh 11) 5)
	     initargs))
    (setf (alt-dialbox ovp)
      (apply #'sl:make-dialbox 25
	     :parent vpw
	     :font vpf :label "Alt:"
	     :ulc-x rx
	     :ulc-y (+ (bp-y top-y bh 11) 5)
	     initargs))
    (ev:add-notify ovp (sl:value-changed (azi-dialbox ovp))
		   #'(lambda (pan db newazi)
		       (declare (ignore db))
		       (setf (azimuth (view-for pan)) newazi)))
    (ev:add-notify ovp (sl:value-changed (alt-dialbox ovp))
		   #'(lambda (pan db newalt)
		       (declare (ignore db))
		       (setf (altitude (view-for pan)) newalt)))
    (setf (sl:angle (azi-dialbox ovp)) (azimuth (view-for ovp))
	  (sl:angle (alt-dialbox ovp)) (altitude (view-for ovp)))))

;;;--------------------------------------

(defclass viewlist-panel (generic-panel)

  ((view :accessor view
	 :initarg :view
	 :documentation "The view displaying the list of objects.")

   (frame :accessor frame
	  :documentation "The frame containing the panel on the
display.")

   (oblist :accessor oblist
	   :initarg :oblist
	   :documentation "The list of objects that can be turned on
and off in the view.")

   (ob-menu :accessor ob-menu
	    :documentation "The scrolling list of names of objects or
classes to turn on or off.")

   (refresh-fn :accessor refresh-fn
	       :initarg :refresh-fn
	       :initform #'display-view
	       :documentation "The function to call to refresh the
view after an object is turned on or off.")

   (delete-btn :accessor delete-btn
	       :documentation "The Delete Panel button.")

   (clear-all-btn :accessor clear-all-btn
		  :documentation "The CLEAR-ALL button.")

   )

  (:documentation "The viewlist panel has a scrolling list of buttons,
one for points and one for each of the other objects in the view, so
the user can select them for display or no display.")

  )

;;;--------------------------------------

(defmethod initialize-instance :after ((vlp viewlist-panel)
				       &rest initargs)

  (declare (ignore initargs))
  (let* ((vw (view vlp))
	 (prims (foreground vw))
	 (oblist (remove-if #'(lambda (obj) (typep obj 'mark))
			    (remove-duplicates
			     (mapcar #'object prims))))
	 (vlpf (symbol-value *small-font*))
	 (mark-prim (find-if #'(lambda (x) (typep x 'mark))
			     prims :key #'object))
	 (other-names (mapcar #'name oblist))
	 (items (if mark-prim ;; Points are lumped
		    (cons "Points" other-names)
		  other-names))
	 (obmenu (sl:make-scrolling-list
		  (+ 40 (apply #'max (mapcar #'(lambda (item)
						 (clx:text-width vlpf item))
					     items)))
		  300 ;; arbitrary height
		  :items items
		  :font vlpf :mapped nil))
	 (height (+ 65 (sl:height obmenu)))
	 (btw (sl:width obmenu))
	 (frm (sl:make-frame (+ 10 btw) height :title "Display list"))
	 (clear-all-flag nil))
    (setf (oblist vlp) oblist
	  (frame vlp) frm
	  (ob-menu vlp) obmenu
	  (delete-btn vlp) (sl:make-button btw 25
					   :parent (sl:window frm)
					   :button-type :momentary
					   :font vlpf :label "Del Pan"
					   :ulc-x 5 :ulc-y 5)
	  ;; this button is a toggle to clear/display all objects
	  (clear-all-btn vlp) (sl:make-button btw 25
					      :parent (sl:window frm)
					      :button-type :momentary
					      :font vlpf :label "Clear All"
					      :ulc-x 5 :ulc-y 30))
    (clx:reparent-window (sl:window obmenu) (sl:window frm) 5 60)
    (clx:map-window (sl:window obmenu))
    (clx:map-subwindows (sl:window obmenu))
    ;; set the initial state of the buttons according to visibility
    (let ((i 0)
	  (btns (sl:buttons obmenu)))
      (when mark-prim
	(if (visible mark-prim)
	    (sl:select-button (nth i btns) obmenu))
	(incf i))
      (dolist (ob oblist)
	(if (visible (find ob prims :key #'object))
	    (sl:select-button (nth i btns) obmenu))
	(incf i)))
    (ev:add-notify vlp (sl:button-on (delete-btn vlp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (destroy pan)))
    (flet ((declutter-fn (pnl btn scr newset)
	     (let* ((prims (foreground (view pnl)))
		    (offset (if (find-if #'(lambda (x)
					     (typep x 'mark))
					 prims
					 :key #'object)
				1 0))
		    (sel (position btn (sl:buttons scr))))
	       (if (and (= offset 1) ;; points present and
			(= sel 0)) ;; were selected
		   (dolist (gp prims)
		     (if (typep (object gp) 'mark)
			 (setf (visible gp) newset)))
		 (let ((obj (nth (- sel offset)
				 (oblist pnl))))
		   (dolist (gp prims)
		     (if (eq (object gp) obj)
			 (setf (visible gp) newset)))))
	       (funcall (refresh-fn pnl) (view pnl)))))
      (ev:add-notify vlp (sl:button-on (clear-all-btn vlp))
		     #'(lambda (pan bt)
			 (declare (ignore pan))
			 (dolist (s (sl:buttons obmenu))
			   (if clear-all-flag
			       (sl:select-button s obmenu)
			     (sl:deselect-button s obmenu)))
			 (setf clear-all-flag (not clear-all-flag))
			 (setf (sl:on bt) nil)))
      (ev:add-notify vlp (sl:selected obmenu)
		     #'(lambda (pan mn btn)
			 (declutter-fn pan btn mn t)))
      (ev:add-notify vlp (sl:deselected obmenu)
		     #'(lambda (pan mn btn)
			 (declutter-fn pan btn mn nil))))))

;;;--------------------------------------

(defmethod destroy :before ((vlp viewlist-panel))

  (sl:destroy (ob-menu vlp))
  (sl:destroy (delete-btn vlp))
  (sl:destroy (clear-all-btn vlp))
  (sl:destroy (frame vlp)))

;;;--------------------------------------

(defmethod destroy :before ((vp view-panel))
  ;; ensure that there are not any lingering 
  ;;   background jobs for this view-panel

  (when (typep (view-for vp) 'beams-eye-view)
    (remove-bg-drr (view-for vp))
    (when (eq 'running (drr-state (view-for vp)))
      (setf (drr-state (view-for vp)) 'paused))
    (setf (image-button (view-for vp)) nil))

  (ev:remove-notify vp (new-position (view-for vp)))
  (ev:remove-notify vp (new-scale (view-for vp)))
  (ev:remove-notify vp (new-name (view-for vp)))
  (ev:remove-notify vp (bg-toggled (view-for vp)))
  (sl:destroy (delete-view vp))
  (if (sl:on (plot-view vp)) (setf (sl:on (plot-view vp)) nil))
  (sl:destroy (plot-view vp))
  (sl:destroy (name-box vp))
  (sl:destroy (local-bar-button vp))
  (sl:destroy (remote-bar-button vp))
  (sl:destroy (image-button vp))
  (if (sl:on (fg-button vp)) (setf (sl:on (fg-button vp)) nil))
  (sl:destroy (fg-button vp))
  (sl:destroy (position-control vp))
  (sl:destroy (scale-control vp))
  (sl:destroy (window-control vp))
  (sl:destroy (level-control vp))
  (when (tape-measure (view-for vp))
    (destroy (tape-measure (view-for vp))))
  (sl:destroy (tape-measure-btn vp))
  (let* ((win (sl:window (picture (view-for vp))))
	 (root (third (multiple-value-list (clx:query-tree win)))))
    (clx:unmap-window win)
    (clx:reparent-window win root 0 0))
  (sl:destroy (view-frame vp)))

;;;------------------------------------------
;;; End.
