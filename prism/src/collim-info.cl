;;;
;;; collim-info
;;;
;;; contains the stuff defining names of collimator jaws or mlc
;;; leaves, etc.
;;;
;;; 10-May-1994 J. Unger Add definitions for the collim-info objects.
;;; 05-Aug-1994 J. Unger add cnts-collim-info class definition.
;;; 23-Aug-1994 J. Jacky change centerline-list to edge-list
;;;  5-Jan-1996 I. Kalet split off from therapy-machines, add
;;;  electron-collim-info and srs-collim-info.
;;; 17-Dec-1998 I. Kalet add energies to electron-collim-info.
;;; 10-Sep-2000 I. Kalet remove srs collimator, now obsolete.  Revise
;;; for new MLC representation.  Downcase names.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defclass symmetric-jaw-collim-info ()

  ((x-name :type string
           :initarg :x-name
           :accessor x-name
           :documentation "Name of the x-axis collimator jaw.")

   (y-name :type string
           :initarg :y-name
           :accessor y-name
           :documentation "Name of the y-axis collimator jaw.")
   )

  (:documentation "Supplemental collimator attribute and value
information for symmetric jaw collimators.")

  )

;;;--------------------------------------------------

(defun make-symmetric-jaw-collim-info (&rest initargs)

  "make-symmetric-jaw-collim-info &rest initargs)

Creates and returns a symmetric-jaw-collim-info object with the
specified initialization args."

  (apply #'make-instance 'symmetric-jaw-collim-info initargs))

;;;--------------------------------------------------

(defclass combination-collim-info ()

  ((x-inf-name :type string
               :initarg :x-inf-name
               :accessor x-inf-name
               :documentation "Name of the x-axis inferior collimator
jaw.")

   (x-sup-name :type string
               :initarg :x-sup-name
               :accessor x-sup-name
               :documentation "Name of the x-axis superior collimator
jaw.")
 
   (x-sym-name :type string
               :initarg :x-sym-name
               :accessor x-sym-name
               :documentation "Name of the x-axis collimator jaw when
the superior and inferior jaws are the same distance from the central
axis.")
 
   (y-name :type string
           :initarg :y-name
           :accessor y-name
           :documentation "Name of the y-axis collimator jaw.")
   )

  (:documentation "Supplemental collimator attribute and value
information for combination jaw collimators.")

  )

;;;--------------------------------------------------

(defun make-combination-collim-info (&rest initargs)

  "make-combination-collim-info &rest initargs)

Creates and returns a combination-collim-info object with the
specified initialization args."

  (apply #'make-instance 'combination-collim-info initargs))

;;;--------------------------------------------------

(defclass asymmetric-jaw-collim-info ()

  ((x-inf-name :type string
               :initarg :x-inf-name
               :accessor x-inf-name
               :documentation "Name of the x-axis inferior collimator
jaw.")

   (x-sup-name :type string
               :initarg :x-sup-name
               :accessor x-sup-name
               :documentation "Name of the x-axis superior collimator
jaw.")

   (y-inf-name :type string
               :initarg :y-inf-name
               :accessor y-inf-name
               :documentation "Name of the y-axis inferior collimator
jaw.")

   (y-sup-name :type string
               :initarg :y-sup-name
               :accessor y-sup-name
               :documentation "Name of the y-axis superior collimator
jaw.")

   )

  (:documentation "Supplemental collimator attribute and value
information for asymmetric jaw collimators.")

  )

;;;--------------------------------------------------

(defun make-asymmetric-jaw-collim-info (&rest initargs)

  "make-asymmetric-jaw-collim-info &rest initargs)

Creates and returns an asymmetric-jaw-collim-info object with the
specified initialization args."

  (apply #'make-instance 'asymmetric-jaw-collim-info initargs))

;;;--------------------------------------------------

(defclass multileaf-collim-info ()

  ((col-headings :type string
                 :initarg :col-headings
                 :accessor col-headings
                 :documentation "A string, a line of text up to a page
wide, including all the column headings for the leaf setting page of
the chart.")

   (num-leaf-pairs :type fixnum
                   :initarg :num-leaf-pairs
                   :accessor num-leaf-pairs
                   :documentation "The number of leaf pairs for the MLC.")

   (edge-list :type list
	      :initarg :edge-list
	      :accessor edge-list
	      :documentation "A list of Y-coordinates of the
edge of leaf travel for each leaf pair in the MLC, starting at
the most positive Y-coordinate, which appears as the first line toward
the top of the printout page.  For a collimator of N leaves, there are N+1
edge coordinates in the list")

   (leaf-pair-map :type list
                  :initarg :leaf-pair-map
                  :accessor leaf-pair-map
                  :documentation "A list of (left-label right-label)
pairs, both elements strings.  The Nth pair of the list represents the
labels to print on the left and right sides of the Nth row from the
top of the MLC page of the chart.")

   (inf-leaf-scale :type single-float
		   :initarg :inf-leaf-scale
		   :accessor inf-leaf-scale
		   :documentation "The inferior leaf scale factor, one
of -1.0 or 1.0")

   (leaf-open-limit :type single-float
		    :initarg :leaf-open-limit
		    :accessor leaf-open-limit
		    :documentation "Maximum value of leaf opening away
from centerline, in CM.  Absolute value, always positive")

   (leaf-overcenter-limit :type single-float
			  :initarg :leaf-overcenter-limit
			  :accessor leaf-overcenter-limit
			  :documentation "Maximum value of leaf
overcentering past centerline, in CM.  Absolute value, always positive
or zero")

   )

  (:documentation "Supplemental collimator attribute and value
information for multileaf collimators.")

  )

;;;--------------------------------------------------

(defun make-multileaf-collim-info (&rest initargs)

  "make-multileaf-collim-info &rest initargs)

Creates and returns a multileaf-collim-info object with the specified
initialization args."

  (apply #'make-instance 'multileaf-collim-info initargs))

;;;--------------------------------------------------

(defclass cnts-collim-info (asymmetric-jaw-collim-info multileaf-collim-info)

  ()

  (:documentation "The cnts collim-info class inherits all attributes
and from both the asymmetric-jaw-collim-info and multileaf-collim-info
classes.")

  )

;;;--------------------------------------------------

(defun make-cnts-collim-info (&rest initargs)

  "make-cnts-collim-info &rest initargs)

Creates and returns a cnts-collim-info object with the specified
initialization args."

  (apply #'make-instance 'cnts-collim-info initargs))

;;;--------------------------------------------------

(defclass electron-collim-info ()

  ((energies :type list
	     :initarg :energies
	     :accessor energies
	     :documentation "A list of nominal electron energies
	     available for this electron machine.")

   (cone-sizes :type list
	       :initarg :cone-sizes
	       :accessor cone-sizes
	       :documentation "A list of cone sizes available for this
electron machine.")

   )

  (:documentation "The electron collim-info class provides information
about the available electron energies and cones for this machine.")

  )

;;;--------------------------------------------------

(defun make-electron-collim-info (&rest initargs)

  "make-electron-collim-info &rest initargs)

Creates and returns an electron-collim-info object with the specified
initialization args."

  (apply #'make-instance 'electron-collim-info initargs))

;;;--------------------------------------------------
;;; End.
