;;;
;;; dose-results
;;;
;;; Definitions of dose results and dose surfaces, for storage and
;;; display of dose information in Prism.
;;;
;;; 11-Oct-1993 J. Unger created from current implementation report.
;;; 12-Oct-1993 J. Unger make dose surface name reflect theshold.
;;; 22-Oct-1993 J. Unger modify setf valid after method in dose-result
;;; object def to improve system efficiency.
;;; 29-Oct-1993 J. Unger make dose-surface's dose-grid and result not-saved;
;;; remove default initargs for those slots.  
;;; 18-Feb-1994 D. Nguyen add copy-dose-result.
;;;  8-Apr-1994 I. Kalet split off from dose-objects
;;;  5-May-1994 J. Unger split valid attrib into valid-points & valid-grid,
;;; also split status-changed event into two separate events.
;;; 15-May-1994 D. Nguyen update copy-dose-result to handle valid-grid and
;;; valid-points.
;;; 01-Jun-1994 J. Unger minor adjs to status-changed to bring it to
;;; spec.
;;; 13-Jun-1994 I. Kalet take message out of copy-dose-result
;;; 16-Jun-1994 I. Kalet change color in dose surface to display-color
;;; 28-Sep-1994 J. Unger add some more initialization args to
;;; dose-surface
;;; 31-May-1995 I. Kalet make name a required parameter to
;;; make-dose-surface, consistent with other object constructors.
;;; 10-Jun-1996 I. Kalet make copy-dose-result a method for generic
;;;  copy, other fixups also.
;;; 29-Jan-1997 I. Kalet change name of tpr slot to ca-tpr, to avoid
;;; name conflict with tpr function in dose-info.
;;;  3-May-1997 I. Kalet the definition of make-dose-surface was in
;;;  conflict with the Implementation Report - make it conform to the
;;;  report, and fix the copy method.  Name was formerly required for
;;;  a selector panel constructor - if it is needed use a lambda.
;;; 26-Jun-1997 I. Kalet don't init grid slot: can't be right, will be
;;; initialized in the compute dose action function when necessary.
;;; 21-Feb-2000 I. Kalet remove rest pars from copy methods.
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass dose-result (generic-prism-object)

  ((grid :type (simple-array single-float 3)
         :initarg :grid
         :accessor grid
	 :documentation "The 3D array of dose values.")

   (points :type list
           :initarg :points
           :accessor points
           :documentation "The list of dose point values.")

   (valid-grid :type (or t nil)
               :initarg :valid-grid
               :accessor valid-grid
               :documentation "The validity of this object's dose
grid values.")

   (valid-points :type (or t nil)
                 :initarg :valid-points
                 :accessor valid-points
                 :documentation "The validity of this object's point
dose values.")

   (grid-status-changed :type ev:event
			:accessor grid-status-changed
			:initform (ev:make-event)
			:documentation "Announced when the valid-grid
attribute changes.")

   (points-status-changed :type ev:event
			  :accessor points-status-changed
			  :initform (ev:make-event)
			  :documentation "Announced when the
valid-points attribute changes.")

   (ssd :type single-float
        :initarg :ssd
        :accessor ssd
        :documentation "The source to surface distance - only
applicable to dose results of beams.")

   (tpr-at-iso :type single-float
	       :initarg :tpr-at-iso
	       :accessor tpr-at-iso
	       :documentation "The tissue phantom ratio at isocenter,
only applicable to dose results of beams.")

   (output-comp :type single-float
		:initarg :output-comp
		:accessor output-comp
		:initform 0.0
		:documentation "The computed output factor, only
applicable to dose results of beams.")

   (equiv-square :type single-float
                 :initarg :equiv-square
                 :accessor equiv-square
                 :documentation "The computed equivalent square, only
applicable to dose results of beams.")

   )

  (:default-initargs :valid-grid nil :valid-points nil
		     :ssd 0.0 :tpr-at-iso 0.0 :output-comp 0.0
		     :equiv-square 0.0)

  (:documentation "A dose result specifies the result of computing
dose for a field, seed, line source, or for an entire plan's worth of
sources.")

  )

;;;---------------------------------------------

(defmethod not-saved ((dr dose-result))

  (append (call-next-method)
    '(name grid-status-changed points-status-changed)))

;;;---------------------------------------------

(defmethod (setf valid-grid) :around (new-val (dr dose-result))

  (let ((old-val (valid-grid dr)))
    (call-next-method)
    (when (or old-val new-val) ;; announce if changed or both t!
      (ev:announce dr (grid-status-changed dr) new-val))))

;;;---------------------------------------------

(defmethod (setf valid-points) :around (new-val (dr dose-result))

  (let ((old-val (valid-points dr)))
    (call-next-method)
    (when (or old-val new-val) ;; announce if changed or both t!
      (ev:announce dr (points-status-changed dr) new-val))))

;;;---------------------------------------------

(defun make-dose-result (&rest initargs)

  "MAKE-DOSE-RESULT &rest initargs

Returns an empty dose-result object."

  (apply #'make-instance 'dose-result initargs))

;;;---------------------------------------------

(defmethod copy ((dr dose-result))

  "Copies and returns a dose-result object.  The actual results are
not copied, so the valid flags are not copied."

  (declare (ignore pars))
  (apply #'make-dose-result
	 (if (slot-boundp dr 'grid)
	     (list :grid (make-array (array-dimensions (grid dr))
				     :element-type 'single-float
				     :initial-element 0.0))
	   nil)))

;;;---------------------------------------------
;;; dose surfaces are the isodose level specs.
;;;---------------------------------------------

(defclass dose-surface (generic-prism-object)

  ((threshold :type single-float
              :initarg :threshold
              :accessor threshold
              :documentation "The threshold value for this surface.")

   (new-threshold :type ev:event
                  :accessor new-threshold
                  :initform (ev:make-event)
                  :documentation "Announced when dose surface threshold 
changes.")

   (display-color :type symbol
		  :accessor display-color
		  :initarg :display-color
		  :documentation "A symbol representing the color of
this isodose surface.")

   (new-color :type ev:event
              :accessor new-color
              :initform (ev:make-event)
              :documentation "Announced when dose surface color changes.")

   (dose-grid :type grid-geometry
              :accessor dose-grid
              :initarg :dose-grid
              :documentation "A grid-geometry object from which the origin, 
size, and dimensions of the result's dose-array can be obtained.")

   (result :type dose-result
           :accessor result
           :initarg :result
           :documentation "The dose-result object in which this surface is 
embedded.")

   )

  (:default-initargs :threshold 100.0 :display-color 'sl:white)

  (:documentation "Dose surfaces are embedded in 3D dose matrices and are
drawn into views."))

;;;---------------------------------------------

(defmethod not-saved ((object dose-surface))

  (append (call-next-method)
	  '(name new-threshold new-color dose-grid result)))

;;;---------------------------------------------

(defmethod (setf threshold) :after (thresh (ds dose-surface))

  (setf (name ds) (write-to-string thresh))
  (ev:announce ds (new-threshold ds) thresh))

;;;---------------------------------------------

(defmethod (setf display-color) :after (col (ds dose-surface))

  (ev:announce ds (new-color ds) col))

;;;---------------------------------------------

(defmethod initialize-instance :after ((ds dose-surface) &rest initargs)

  (declare (ignore initargs))
  (setf (name ds) (write-to-string (threshold ds))))

;;;---------------------------------------------

(defun make-dose-surface (&rest initargs)

  "MAKE-DOSE-SURFACE &rest initargs

Returns a dose surface object with specified parameters."

  (apply #'make-instance 'dose-surface initargs))

;;;---------------------------------------------

(defmethod copy ((ds dose-surface))

  "Copies and returns a dose-surface object."

  (declare (ignore pars))
  (make-dose-surface :threshold (threshold ds) 
		     :display-color (display-color ds)))

;;;---------------------------------------------
