;;;
;;; planar-editor
;;;
;;; The planar editor provides a graphical display with a background
;;; picture and a list of points to be edited, along with mouse
;;; interaction techniques to enter and edit the points and some
;;; related controls.  It provides support for editing lists of points
;;; of interest and planar contours.
;;;
;;; 10-Jul-1992 I. Kalet created contour editor, made many modifications.
;;; 24-Feb-1993 J. Unger rewrite from I. Kalet's original code.
;;; 13-Apr-1993 J. Unger revise after extensive discussions.
;;; 11-Mar-1994 I. Kalet major rewrite, move ruler to separate
;;; module, reorganize event dispatching, use pickable objects.
;;; 17-Mar-1994 I. Kalet don't announce new-vertices in setf method,
;;; only when Accept button is pressed.
;;;  1-Apr-1994 I. Kalet further mods in major rewrite.
;;; 25-Apr-1994 I. Kalet use new pickable objects and stuff
;;; 10-May-1994 I. Kalet fix error in ce-deselected, landmark code,
;;; change type of image data to unsigned byte 8.  Fix error in
;;; delete-duplicate-vertices.
;;; 17-May-1994 I. Kalet take free-pixmap out of destroy method
;;; 22-May-1994 I. Kalet ignore grab box motion unless button 1 down
;;; 30-May-1994 I. Kalet split off to share code and reduce
;;; duplication.  Add pan-zoom flag to lock scale and origin when
;;; image displayed, don't depend on image slot.
;;; 10-Jun-1994 I. Kalet add full digitizer support.
;;; 30-Jun-1994 I. Kalet don't recalibrate digitizer every time.
;;; 11-Jul-1994 J. Unger work on getting digitizer dialogs to show up
;;; (not finished).
;;; 11-Jul-1994 J. Unger implement call to tape-measure; impl may change
;;; after some more design work.
;;; 22-Jul-1995 J. Unger make title of planar editor panel a keyword param,
;;; pass 'enabled' param into call to make-square.
;;; 03-Aug-1994 J. Unger add get-pe-magnification method and call 
;;; in use-digitizer.
;;; 04-Aug-1994 J. Unger make use-digitizer a method.
;;; 13-Jan-1995 I. Kalet don't make destroy method free background pixmap
;;; but do change name of digitizer routine from gp8 to digit.
;;; 23-May-1995 I. Kalet move pop-event-level up in use-digitizer to
;;; avoid problems with side effects of accept-vertices in
;;; 3d-point-editor
;;; 20-May-1997 I. Kalet finally fix tape measure implementation to
;;; eliminate circularity, update tape-measure scale and origin from
;;; here.
;;; 22-Jun-1997 I. Kalet move "global" params to init-inst let form,
;;; make Accept button work like Accept Cmts button on patient and
;;; plan panels, setting when vertices are added, deleted or changed,
;;; and reset when client sets vertices.  Simplify protocol for
;;; accept-vertices.  Add set-pe-origin function to provide for
;;; external change of origin.
;;; 17-Apr-1998 I. Kalet add display-planar-editor when ruler is
;;; deleted.
;;; 23-Jun-1998 I. Kalet but not when destroying the whole panel,
;;; because the background pixmap might have been deleted.
;;; 25-Feb-1999 I. Kalet change primary method for delete-vertex to be
;;; a default method to handle user deleting last vertex in digitizer
;;; mode, when there are no vertices.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;; 19-Dec-1999 I. Kalet coerce float in digitizer magnification input
;;; 20-Jul-2000 I. Kalet take out enable-pan-zoom, now always enabled
;;; with GL support.
;;; 29-Jan-2003 I. Kalet raise upper limit on scale slider.
;;;  8-Feb-2004 I. Kalet remove unnecessary get-pe-magnification method
;;; 10-May-2004 I. Kalet merge contour editor back in here, move
;;; digitizer stuff to digitizers module, other stuff to polygons package.
;;; ------------ change log excerpts from contour-editor --------------
;;; 31-May-1994 I. Kalet don't look for leader value on empty
;;;  scratch-vertices list.
;;; 07-Jun-1994 I. Kalet make image slot (unsigned-byte 16) since we
;;;  are not providing grayscale mapped image anymore.  Change back to
;;;  (unsigned-byte 8) if you restore mapping in volume-editor
;;; 02-Mar-1997 I. Kalet change image slot back to (unsigned-byte 8),
;;;  since we are back again to autocontouring on mapped images.
;;; 16-Jun-1997 I. Kalet take out resetting the Accept button, now
;;;  done in planar-editor.  Make ACCEPT-VERTICES call planar-editor method.
;;; 07-Jul-1997 BobGian added collinearity to set of conditions tested
;;;    when accepting a contour.  Poly:CANONICAL-CONTOUR is used to remove
;;;    redundant (nearly identical) vertices and to remove vertices internal
;;;    to a chain of collinear vertices (with wraparound).  Call and fixup
;;;    happens in ACCEPT-VERTICES method for class CONTOUR-EDITOR before
;;;    LEGAL-CONTOUR checks other requirements.
;;;  5-Feb-2000 I. Kalet Allow adding contour points, breaking
;;; segments, even in autocontour mode.  Change names to lower case.
;;; -------------------------------------------------------------------
;;; 17-May-2004 I. Kalet continued overhaul to merge contour and point
;;; editing
;;; 24-Jan-2005 I. Kalet change make-contour-editor to
;;; make-planar-editor, other fixes towards finishing the overhaul.
;;; 26-Aug-2005 I. Kalet more changes to handle points
;;; 24-Jun-2007 I. Kalet announce pt-selected when user clicks on a
;;; point vertex.  Also, remove event notifies for point new-color and
;;; new-loc when planar editor is in point mode and scratch-vertices
;;; are cleared or planar-editor is destroyed.
;;; 25-Jun-2008 I. Kalet don't allow Auto edit mode for points
;;; 15-Jun-2009 I. Kalet fix error in setf vertices method
;;;

(in-package :prism)

;;;-----------------------------------

(defparameter *ce-sketch-tolerance* 1
  "A distance criterion that determines how closely a set of vertices
output by reduce-contour approximates an input set of vertices traced
out in sketch mode.  The lower the number, the closer the
approximation.")

(defvar *default-point-color* 'sl:yellow
  "determines what color new points are in the contour/point editor")

;;;-----------------------------------

(defclass planar-editor (generic-panel)

  ((vertices :type list
	     :accessor vertices
	     :initarg :vertices
	     :documentation "The vertices to be edited.  The
coordinates are in model space (eg: cm).")

   (background :type clx:pixmap
	       :accessor background
	       :initarg :background
	       :documentation "The background pixmap for the drawing
region of the planar-editor.")

   (x-origin :type fixnum
	     :accessor x-origin
	     :initarg :x-origin
	     :documentation "The x pixel coordinate of the origin of
model space on the planar editor's picture.")

   (y-origin :type fixnum
	     :accessor y-origin
	     :initarg :y-origin
	     :documentation "The y pixel coordinate of the origin of
model space on the planar editor's picture.")

   (scale :type single-float
	  :accessor scale
          :initarg :scale
          :documentation "The number of pixels per unit of model
space.")

   (digitizer-mag :type single-float
		  :accessor digitizer-mag
		  :initarg :digitizer-mag
		  :documentation "The magnification factor from cm on
the digitizing tablet to cm in model space.")

   (new-vertices :type ev:event
		 :accessor new-vertices
		 :initform (ev:make-event)
		 :documentation "Announced when the vertices attribute
is updated.")

   (new-origin :type ev:event
	       :accessor new-origin
	       :initform (ev:make-event)
	       :documentation "Announced when a new origin is set.")

   (new-scale :type ev:event
	      :accessor new-scale
	      :initform (ev:make-event)
	      :documentation "Announced when a new scale is set.")

   (pt-selected :type ev:event
		:accessor pt-selected
		:initform (ev:make-event)
		:documentation "Announced when a point vertex is
		clicked with mouse-1 by the user.")

   ;; change to (unsigned-byte 16) if not using grayscale mapping for
   ;; autocontour operations
   (image :type (simple-array (unsigned-byte 8) 2)
          :accessor image
          :initarg :image
          :documentation "The raw image array from which
autocontouring is done.  If nil, autocontouring is not available.")

   (img-x0 :accessor img-x0
	   :initarg :img-x0
	   :documentation "The image pixel space x coordinate of the
patient origin")

   (img-y0 :accessor img-y0
	   :initarg :img-y0
	   :documentation "The image pixel space y coordinate of the
patient origin")

   (img-ppcm :accessor img-ppcm
	     :initarg :img-ppcm
	     :documentation "The pixels per cm scale factor of the
image array, not necessarily the same as for the display window of the
contour editor.")

   (next-mark-id :type fixnum
		 :accessor next-mark-id
		 :initarg :next-mark-id
		 :documentation "The integer to be assigned to the
next new mark created in the contour/point editor.")

   (contour-mode :type (member t nil)
		 :accessor contour-mode
		 :initarg :contour-mode
		 :initform t
		 :documentation "A boolean flag indicating whether the
points are connected as a contour or are individual points.  If all
points are deleted, we need to be able to retain this mode information
somehow..")

   ;;-------------------------------------------------------
   ;; internal attributes of the planar editor from here on
   ;;-------------------------------------------------------

   (scratch-vertices :type list
		     :accessor scratch-vertices
		     :initform nil
		     :documentation "The working vertext list being
edited, a list of vertex objects.  This is the contour or set of
points which appears on the screen.")

   (scratch-points :type list
                   :accessor scratch-points
                   :initform nil
                   :documentation "A temporary placeholder for cached
point information, for example, the temporary vertices in sketch
mode.")

   (landmarks :type list
              :accessor landmarks
              :initform nil
              :documentation "A list of landmark objects.")

   (tape-measure :accessor tape-measure
		 :initform nil
		 :documentation "A ruler that appears in the picture on
demand.")

   (edit-mode :type (member :manual :automatic :landmarks :digitizer)
              :accessor edit-mode
              :initform :manual
              :documentation "Determines the mode for pointer
operations in the drawing region when the pointer is not on a pickable
object such as the ruler or a landmark, or digitizer mode.")

   (color :type clx:gcontext
	  :accessor color
          :initarg :color
          :documentation "The color of the contour or point being edited.")

   (fr :type sl:frame
       :accessor fr
       :documentation "The SLIK frame that contains the planar
editor.")

   (picture :type sl:picture
	    :accessor picture
	    :initform nil
	    :documentation "The picture for the drawing region.  The
picture's pixmap contains all the graphics, including a copy of the
background pixmap and any foreground graphics, such as vertices, the
ruler, landmarks, etc.")

   (accept-btn ; :type sl:button
	       :accessor accept-btn
               :documentation "The Accept button.  Pressing it causes
the vertices being edited to be accepted.")

   (clear-btn ; :type sl:button
	      :accessor clear-btn
              :documentation "The Clear button.  Pressing it causes
the vertices being edited to be erased, for the purposes of starting
over.")

   (tape-measure-btn :accessor tape-measure-btn
		     :documentation "The Ruler button.  Pressing it causes a
ruler to appear in the display area.")

   (edit-mode-btn ; :type sl:button
		  :accessor edit-mode-btn
                  :documentation "The editing mode button.")

   (scale-sdr ; :type sl:slider
	      :accessor scale-sdr
              :documentation "The scale slider.")

   (busy :accessor busy
	 :initform nil
	 :documentation "The busy bit for controlling updates between
planar editor attributes and planar editor controls.")

   )

  (:default-initargs :vertices nil :scale 20.0 :digitizer-mag 1.0
		     :color (sl:color-gc 'sl:green) :image nil
		     :next-mark-id 1)

  (:documentation "A planar editor provides a drawing region for
drawing and editing contours or points, and some controls for managing
the editing process.")

  )

;;;-----------------------------------

(defclass vertex ()

  ((pe :type planar-editor
       :accessor pe
       :initarg :pe
       :documentation "The planar editor in which this vertex
appears.")

   (x :accessor x
      :initarg :x
      :documentation "x coordinate in model space")

   (y :accessor y
      :initarg :y
      :documentation "y coordinate in model space")

   (xpix :accessor xpix
	 :initarg xpix
	 :documentation "x coordinate in pixel space on the screen")

   (ypix :accessor ypix
	 :initarg ypix
	 :documentation "y coordinate in pixel space on the screen")

   (marker :accessor marker
	   :documentation "The SLIK square that is the visible
vertex on the planar editor drawing area.")

   )

  (:documentation "A vertex is a point of interest or a point on a
contour.")

  )

;;;-----------------------------------

(defclass point-vertex (vertex)

  ((point :accessor point
	  :initarg :point
	  :initform nil
	  :documentation "The point or mark associated with this vertex.")

   )

  (:documentation "A point vertex is the visible manifestation of a
mark object in the 2d point editor.") 

  )

;;;-----------------------------------

(defclass contour-vertex (vertex)

  ((leader :accessor leader
	   :initarg :leader
	   :initform nil
	   :documentation "The boolean indicating that this vertex is
the leading one in the contour.")

   )

  (:documentation "A contour-vertex is a point on a contour.")

  )

;;;-----------------------------------

(defun display-planar-editor (pe)

  "display-planar-editor pe

Refreshes the planar editor drawing area with the background, the
subject of drawing, e.g., contour or points, and the various grab
boxes and circles, i.e., vertices, landmarks, and the tape measure."

  (let* ((pic (picture pe))
         (pm (sl:pixmap pic))
         (size (clx:drawable-width pm)))
    (clx:copy-area (background pe) (sl:color-gc 'sl:white)
		   0 0 size size pm 0 0)
    ;; draw the segments or numbers
    (if (contour-mode pe)
	(let ((col (color pe))
	      (sv (apply #'append ;; get a flat list of x and y pixels
			 (mapcar #'(lambda (v) (list (xpix v) (ypix v)))
				 (scratch-vertices pe)))))
	  (clx:draw-lines pm col sv)
	  ;; if there are vertices and no leading vertex, close the contour
	  (when (and sv (not (leader (first (scratch-vertices pe)))))
	    (let ((end (nthcdr (- (length sv) 2) sv)))
	      (clx:draw-line pm col
			     (first sv) (second sv)
			     (first end) (second end)))))
      ;; draw the numbers next to each point
      (dolist (pv (scratch-vertices pe))
	(clx:draw-glyphs pm (sl:color-gc (display-color (point pv))) 
			 (+ (xpix pv) 5) (+ (ypix pv) 10)
			 (write-to-string (id (point pv))))))
    (if (tape-measure pe) (draw-tape-measure-tics (tape-measure pe)))
    (sl:display-picture pic))) ;; draw the grab boxes, landmarks etc.

;;;-----------------------------------

(defmethod marker-motion ((vt vertex) mk xp yp state)

  (if (member :button-1 (clx:make-state-keys state))
      (let* ((pe (pe vt))
	     (ppcm (scale pe)))
	(setf (xpix vt) xp
	      (ypix vt) yp
	      (x vt) (cm-x xp (x-origin pe) ppcm)
	      (y vt) (cm-y yp (y-origin pe) ppcm))
	(sl:update-pickable-object mk xp yp)
	;; and do it when the point moves
	(unless (sl:on (accept-btn pe))
	  (setf (sl:on (accept-btn pe)) t))
	(display-planar-editor pe))))

;;;-----------------------------------

(defmethod marker-motion :after ((vt point-vertex) mk xp yp state)

  (declare (ignore mk xp yp state)) ;; for now...
  ;; this automatically announces the new-loc event
  (let ((pe (pe vt)))
    (when (not (busy pe))
      (setf (busy pe) t)
      (setf (x (point vt)) (x vt)
	    (y (point vt)) (y vt))
      (setf (busy pe) nil))))

;;;-----------------------------------

(defmethod initialize-instance :after ((v vertex) &rest initargs
                               &key (enabled t) &allow-other-keys)

  (declare (ignore initargs))
  (let* ((pe (pe v))
	 (ppcm (scale pe))
	 (xp (pix-x (x v) (x-origin pe) ppcm))
	 (yp (pix-y (y v) (y-origin pe) ppcm)))
    (setf (xpix v) xp
	  (ypix v) yp
	  (marker v) (sl:make-square v xp yp :enabled enabled))
    (sl:add-pickable-obj (marker v) (picture pe))
    ;; turn on the Accept button anytime you add a point, unless it is
    ;; already on, or the Accept button was just turned off and new
    ;; vertices are being added as a result (busy).
    (unless (or (busy pe) (sl:on (accept-btn pe)))
      (setf (sl:on (accept-btn pe)) t)) ;; no guard needed for turning on
    (ev:add-notify v (sl:motion (marker v))
		   #'marker-motion)))

;;;-----------------------------------

(defmethod delete-vertex :around ((vt vertex))

  (let ((pe (pe vt)))
    (sl:remove-pickable-objs vt (picture pe))
    (call-next-method)
    (display-planar-editor pe)))

;;;-----------------------------------

(defmethod delete-vertex ((vt contour-vertex))

  "for contour points, remove the vertex, make the next one leader if
this was the leader"

  (let ((pe (pe vt)))
    (setf (scratch-vertices pe)
      (remove vt (scratch-vertices pe)))
    (if (and (leader vt) (scratch-vertices pe))
	(setf (leader (first (scratch-vertices pe))) t))
    (setf (sl:on (accept-btn pe)) t)))
  
;;;-----------------------------------

(defmethod delete-vertex ((vt point-vertex))

  "for point-vertex remove the corresponding point, and let the caller
 update everything."

  (let ((pe (pe vt)))
    (setf (vertices pe)
      (remove-if #'(lambda (pt) (eql pt (point vt)))
		 (vertices pe)))
    (ev:announce pe (new-vertices pe) (vertices pe))))

;;;-----------------------------------

(defmethod initialize-instance :after ((pv point-vertex) &rest initargs)

  (declare (ignore initargs))
  (setf (sl:color (marker pv))
    (sl:color-gc (display-color (point pv))))
  (ev:add-notify pv (new-color (point pv))
		 #'(lambda (vt pt color)
		     (declare (ignore pt))
		     (setf (sl:color (marker vt)) (sl:color-gc color))))
  (ev:add-notify pv (new-loc (point pv))
		 #'(lambda (vt pt loc)
		     (declare (ignore pt))
		     (let* ((pe (pe vt))
			    (ppcm (scale pe)))
		       (when (not (busy pe))
			 (setf (busy pe) t)
			 (setf (x vt) (first loc)
			       (y vt) (second loc)
			       (xpix vt) (pix-x (first loc)
						(x-origin pe) ppcm)
			       (ypix vt) (pix-y (second loc)
						(y-origin pe) ppcm))
			 (sl:update-pickable-object (marker vt)
						    (xpix vt) (ypix vt))
			 (unless (sl:on (accept-btn pe))
			   (setf (sl:on (accept-btn pe)) t))
			 (display-planar-editor pe)
			 (setf (busy pe) nil)))))
  (ev:add-notify pv (sl:selected (marker pv))
		 #'(lambda (vt mk code x y)
		     (declare (ignore mk x y))
		     (case code
		       (1 (let ((ped (pe vt)))
			    (ev:announce ped (pt-selected ped)
					 (point vt))))
		       (2  (delete-vertex vt))))))

;;;-----------------------------------

(defmethod initialize-instance :after ((v contour-vertex)
				       &rest initargs)

  (declare (ignore initargs))
  (let ((ce (pe v))
	(mk (marker v)))
    (setf (sl:color mk) (color ce)
	  (sl:filled mk) (leader v))
    (ev:add-notify v (sl:selected (marker v))
		   #'(lambda (vt mk code x y)
		       (declare (ignore mk x y))
		       (when (= code 2) (delete-vertex vt))))))

;;;-----------------------------------

(defun make-point-vertex (&rest initargs)

  (apply #'make-instance 'point-vertex initargs))

;;;-----------------------------------

(defun make-contour-vertex (&rest initargs)

  (apply #'make-instance 'contour-vertex initargs))

;;;-----------------------------------

(defmethod (setf leader) :after (new-val (v vertex))

  (setf (sl:filled (marker v)) new-val))

;;;-----------------------------------

(defun vertex-list (pe points)

  "Returns a list of vertex objects created from the coordinate pair
list or mark list, points, for the contour editor pe."

  (mapcar #'(lambda (pt)
	      (if (contour-mode pe)
		  (make-contour-vertex :pe pe :x (first pt) :y (second pt))
		(make-point-vertex :pe pe :x (x pt) :y (y pt)
				   :point pt)))
	  points))

;;;-----------------------------------

(defmethod (setf color) :after (new-color (pe planar-editor))

  "When a new color is supplied in contour mode for the contour
editor's vertices, redraw the picture with the vertices in the new color."

  (if (contour-mode pe)
      (mapc #'(lambda (vt) (setf (sl:color (marker vt)) new-color))
	    (scratch-vertices pe)))
  (display-planar-editor pe))

;;;-----------------------------------

(defclass landmark ()

  ((pe :type planar-editor
       :initarg :pe
       :accessor pe
       :documentation "The planar editor in which the landmark
appears.")

   (x :accessor x
      :initarg :x
      :documentation "x coordinate in model space of the landmark")

   (y :accessor y
      :initarg :y
      :documentation "y coordinate in model space of the landmark")

   (marker :accessor marker
	   :documentation "The SLIK circle that is the visible
landmark on the planar editor drawing area.")

   )

  (:documentation "A landmark is a point on the drawing area
corresponding to a point in real space, but only 2-d, i.e., it
persists in the same location from plane to plane if the background
changes.")

  )

;;;-----------------------------------

(defmethod initialize-instance :after ((l landmark) &rest initargs)

  (declare (ignore initargs))
  (let* ((pe (pe l))
	 (ppcm (scale pe)))
    (setf (marker l)
      (sl:make-circle l (pix-x (x l) (x-origin pe) ppcm)
		      (pix-y (y l) (y-origin pe) ppcm)
		      :color (sl:color-gc 'sl:cyan)))
    (sl:add-pickable-obj (marker l) (picture pe))
    (ev:add-notify l (sl:selected (marker l))
		   #'(lambda (lm mk code x y)
		       (declare (ignore x y))
		       (case code
			 (1 nil)
			 (2 (let ((pe (pe lm)))
			      (setf (landmarks pe)
				(remove lm (landmarks pe)))
			      (sl:remove-pickable-objs lm (picture pe))
			      (display-planar-editor pe)))
			 (3 (let ((new-col (sl:popup-color-menu)))
			      (when new-col
				(setf (sl:color (marker lm))
				  (sl:color-gc new-col))
				(display-planar-editor pe)))
			    ;; the popup menu will preempt a button
			    ;; release event, so handle it here
			    (setf (sl:active mk) nil)))))
    (ev:add-notify l (sl:motion (marker l))
		   #'(lambda (lm mk xpix ypix state)
		       (when (member :button-1
				     (clx:make-state-keys state))
			 (let* ((pe (pe lm))
				(ppcm (scale pe)))
			   (setf (x lm) (cm-x xpix (x-origin pe) ppcm)
				 (y lm) (cm-y ypix (y-origin pe) ppcm))
			   (sl:update-pickable-object mk xpix ypix)
			   (display-planar-editor (pe lm))))))))

;;;-----------------------------------

(defun make-landmark (&rest initargs)

  (apply #'make-instance 'landmark initargs))

;;;-----------------------------------

(defun pe-rescale (pe)

  "pe-rescale pe

Computes new pixel coordinates for the scratch vertices & landmarks."

  (let* ((ppcm (scale pe))
	 (xorig (x-origin pe))
	 (yorig (y-origin pe))
	 (pic (picture pe)))
    (mapc #'(lambda (vt)
	      (let ((vbox (first (sl:find-pickable-objs vt pic)))
		    (xp (pix-x (x vt) xorig ppcm))
		    (yp (pix-y (y vt) yorig ppcm)))
		(setf (xpix vt) xp
		      (ypix vt) yp
		      (sl:x-center vbox) xp
		      (sl:y-center vbox) yp)))
	  (scratch-vertices pe))
    (mapc #'(lambda (lm)
	      (let ((lbox (first (sl:find-pickable-objs lm pic))))
		(setf (sl:x-center lbox) (pix-x (x lm) xorig ppcm)
		      (sl:y-center lbox) (pix-y (y lm) yorig ppcm))))
	  (landmarks pe))))

;;;-----------------------------------

(defmethod (setf scale) :after (new-scale (pe planar-editor))

  (pe-rescale pe)
  (unless (busy pe)
    (setf (busy pe) t)
    (setf (sl:setting (scale-sdr pe)) new-scale)
    (setf (busy pe) nil))
  (when (tape-measure pe) ;; update the tape-measure scale
    (setf (scale (tape-measure pe)) new-scale))
  (ev:announce pe (new-scale pe) new-scale)
  (display-planar-editor pe))

;;;-----------------------------------

(defun set-pe-origin (pe x0 y0)

  "set-pe-origin pe x0 y0

updates the x and y origins of the planar editor pe, announces
new-origin but does not refresh the window.  So it can be part of a
sequence of updates that conclude with a single call to
display-planar-editor to refresh the window."

  (setf (x-origin pe) x0
	(y-origin pe) y0)
  (pe-rescale pe)
  (when (tape-measure pe) ;; update the tape-measure origin
    (setf (origin (tape-measure pe)) (list x0 y0)))
  (ev:announce pe (new-origin pe) (list x0 y0)))

;;;-----------------------------------

(defmethod (setf background) :after (new-bkgnd (pe planar-editor))

  "When a new background is supplied to the planar editor, redraw the
picture with the new background."

  (declare (ignore new-bkgnd))
  (display-planar-editor pe))

;;;-----------------------------------

(defmethod (setf vertices) :after (new-verts (pe planar-editor))

  "When a new list of vertices is supplied, cancel any vertices being
edited, reset the scratch-vertices from the new vertices, and redraw
the picture."

  (declare (ignore new-verts))
  (mapc #'(lambda (obj)
	    (sl:remove-pickable-objs obj (picture pe))
	    ;; don't depend on editor mode here
	    (when (typep obj 'point-vertex)
	      (ev:remove-notify obj (new-loc (point obj)))
	      (ev:remove-notify obj (new-color (point obj)))))
	(scratch-vertices pe))
  (setf (busy pe) t) ;; insure that Accept button will not turn on
  (setf (scratch-vertices pe) (vertex-list pe (vertices pe)))
  ;; turn off the Accept button only if it is on, i.e., when a new set
  ;; of vertices is in and an old one was pending.
  (if (sl:on (accept-btn pe)) (setf (sl:on (accept-btn pe)) nil))
  (setf (busy pe) nil)
  (display-planar-editor pe))

;;;-----------------------------------

(defun point-near-contour (x y sv-list)

  "point-near-contour x y sv-list

If the point at (x y) is near the segments determined by the scratch
vertex list sv-list, returns the index of the trailing vertex of the
segment (so an index of 1 through (1- (length sv)), inclusive, may be
returned for open contours and 1 through (length sv), inclusive for
closed contours).  If (x y) is not near any segment, returns NIL."

  (position t 
	    (cons nil
		  (maplist #'(lambda (vt-list)
			       (when (rest vt-list)
				 (let ((x1 (xpix (first vt-list)))
				       (y1 (ypix (first vt-list)))
				       (x2 (xpix (second vt-list)))
				       (y2 (ypix (second vt-list)))
				       (tolerance 2))
				   (sl:point-near-segment x y
							  x1 y1
							  x2 y2
							  tolerance))))
			   (if (and sv-list (leader (first sv-list)))
			       sv-list ;; open list
			     (append sv-list ;; closed list
				     (list (first sv-list))))))))

;;;------------------------------------

(defun pe-selected (pe pic code xpix ypix)

  "An action function which determines which editing operation to
invoke when a mouse button is pressed while the pointer is over the
picture."

  (declare (ignore pic))
  (case code
    (1 (case (edit-mode pe)
	 ((:manual :automatic :digitizer)
	  (if (contour-mode pe)
	      (let ((ppcm (scale pe))
		    (index (point-near-contour xpix ypix
					       (scratch-vertices pe))))
		(if index
		    (setf (scratch-vertices pe)
		      (append (subseq (scratch-vertices pe) 0 index)
			      (cons (make-contour-vertex
				     :pe pe
				     :x (cm-x xpix (x-origin pe) ppcm)
				     :y (cm-y ypix (y-origin pe) ppcm))
				    (subseq (scratch-vertices pe) index))))
		  (case (edit-mode pe)
		    ((:manual :digitizer) ;; new point or sketch 
		     (let ((sv (scratch-vertices pe)))
		       (when (or (null sv) ;; no points yet, or
				 (leader (first sv))) ;; contour is still open
			 (if sv (setf (leader (first sv)) nil))
			 (push (make-contour-vertex ;; add point, maybe sketch
				:pe pe
				:x (cm-x xpix (x-origin pe) ppcm)
				:y (cm-y ypix (y-origin pe) ppcm)
				:leader t)
			       (scratch-vertices pe))
			 (setf (scratch-points pe) ;; reset with this pt only
			   (list (list xpix ypix))))))
		    (:automatic ;; automated contour using pe image data
		     (when (and (image pe) (= code 1))
		       (let* ((img (image pe))
			      (size (array-dimension img 0))
			      (img-scale (img-ppcm pe))
			      (mag (/ (scale pe) img-scale))
			      (img-x0 (img-x0 pe))
			      (img-y0 (img-y0 pe))
			      (img-x (+ (round (/ (- xpix (x-origin pe)) mag))
					img-x0))
			      (img-y (+ (round (/ (- ypix (y-origin pe)) mag))
					img-y0)))
			 ;; first remove old grab boxes
			 (mapc #'(lambda (obj) (sl:remove-pickable-objs
						obj (picture pe)))
			       (scratch-vertices pe))
			 ;; then find and make new vertices
			 (setf (scratch-vertices pe)
			   (vertex-list
			    pe
			    ;; needs cm coordinates and autocon gives pixels
			    (mapcar #'(lambda (pix-pair)
					(list (cm-x (first pix-pair)
						    img-x0 img-scale)
					      (cm-y (second pix-pair)
						    img-y0 img-scale)))
				    (autocontour img img-x img-y
						 0 0 (1- size) (1- size)
						 *ce-sketch-tolerance*))))
			 ))))))
	    ;; point mode
	    ;; still need to get it inserted into point collection
	    (if (member (edit-mode pe) '(:manual :digitizer))
		(let* ((ppcm (scale pe))
		       (pt (make-point (format nil "~A" (gensym "POINT-"))
				       :x (cm-x xpix (x-origin pe) ppcm)
				       :y (cm-y ypix (y-origin pe) ppcm)
				       :z 0.0 ;; reset later by volume editor
				       :id (next-mark-id pe)
				       :display-color *default-point-color*)))
		  (incf (next-mark-id pe))
		  (setf (vertices pe)
		    (append (vertices pe) (list pt)))
		  (ev:announce pe (new-vertices pe) (vertices pe))
		  ))))
	 (:landmarks (let ((ppcm (scale pe)))
		       (push (make-landmark
			      :x (cm-x xpix (x-origin pe) ppcm)
			      :y (cm-y ypix (y-origin pe) ppcm)
			      :pe pe)
			     (landmarks pe))))))
    (2 nil) ;; button 2 is ignored
    (3 (setf (scratch-points pe) (list xpix ypix)))) ;; button 3 does pan
  (display-planar-editor pe))

;;;-----------------------------------

(defun pe-deselected (pe pic code x y)

  "An action function for mouse button release while the pointer is
over the picture.  If left button and there are 3 or more temporary
points, those points are reduced by removing duplicates, and appended
to the scratch vertices in the planar editor pe."

  (declare (ignore pic x y))
  (if (and (contour-mode pe)
	   (eql (edit-mode pe) :manual)
	   (= code 1)
	   (third (scratch-points pe))) ;; sketch mode
      (let ((xorig (x-origin pe))
	    (yorig (y-origin pe))
	    (ppcm (scale pe)))
	(setf (leader (first (scratch-vertices pe))) nil)
	(setf (scratch-vertices pe)
	  (append ;; the new ones
	   (vertex-list pe
			;; needs cm but reduce-contour gives pixels
			(mapcar #'(lambda (pix-pair)
				    (list (cm-x (first pix-pair)
						xorig ppcm)
					  (cm-y (second pix-pair)
						yorig ppcm)))
				(butlast
				 (reduce-contour (scratch-points pe)
						 *ce-sketch-tolerance*))))
	   (scratch-vertices pe))) ;; to the old ones
	(setf (leader (first (scratch-vertices pe))) t)))
  (setf (scratch-points pe) nil) ;; reset every time
  (display-planar-editor pe))

;;;-----------------------------------

(defun pe-motion (pe pic x y state)

  "An action function which determines what to do upon detecting
pointer motion."

  (declare (ignore pic))
  (let ((keys (clx:make-state-keys state)))
    (cond  ;; note - button 2 down is ignored
     ((and (member :button-1 keys);; to sketch, need all these
	   (contour-mode pe)
	   (eql (edit-mode pe) :manual)
	   (scratch-vertices pe) ;; check if there are any points
	   (leader (first (scratch-vertices pe)))) ;; and an open contour
      (clx:draw-point (sl:window (picture pe)) (color pe) x y)
      (push (list x y) (scratch-points pe))
      (sl:flush-output))
     ((member :button-3 keys) ;; pan, works in any mode
      (set-pe-origin pe
		     (+ (x-origin pe)
			(- x (first (scratch-points pe))))
		     (+ (y-origin pe)
			(- y (second (scratch-points pe)))))
      (display-planar-editor pe)
      (setf (scratch-points pe) (list x y))))))

;;;-----------------------------------

(defun clear-vertices (pe bt)

  "clear-vertices pe bt

resets the scratch vertices to an empty list if not already empty."

  (declare (ignore bt))
  (mapc #'(lambda (vt)
	    (sl:remove-pickable-objs vt (picture pe))
	    (if (not (contour-mode pe))
		(let ((pt (point vt)))
		  (ev:remove-notify vt (new-color pt))
		  (ev:remove-notify vt (new-loc pt)))))
	(scratch-vertices pe))
  (setf (scratch-vertices pe) nil)
  ;; turn on Accept button to indicate modification
  (setf (sl:on (accept-btn pe)) t)
  (display-planar-editor pe))

;;;-----------------------------------

(defun legal-contour (sv &optional (quiet nil))

  "legal-contour sv &optional (quiet nil)

If sv has at least 3 vertices and does not cross over itself, then
returns t.  Otherwise, returns nil, and depending on quiet, displays a
pop-up acknowledge box informing user of detected problem or just
writes a line to standard output."

  (let ((flat-sv (apply #'append sv)))
    (cond ((not (sixth flat-sv))
	   (if quiet
	       (format t "~%Contour not added - fewer than three vertices.")
	     (sl:acknowledge '("Contour not accepted."
			       "It has fewer than three vertices.")))
	   nil)
	  ((not (poly:simple-polygon flat-sv))
	   (if quiet
	       (format t "~%Contour not added - it is self-intersecting.")
	     (sl:acknowledge '("Contour not accepted."
			       "Some segments would cross each other.")))
	   nil)
	  (t t))))

;;;-----------------------------------

(defun accept-vertices (pe bt)

  "accept-vertices pe bt

action function for when the ACCEPT button is turned off, signaling to
register the temporary data in the planar editor."

  (unless (busy pe)
    ;; insures that the button will not turn on again or off redundantly
    (setf (busy pe) t)
    (if (contour-mode pe)
	(let ((sv (poly:canonical-contour
		   (mapcar #'(lambda (v) ;; get x-y pairs
			       (list (x v) (y v)))
			   (scratch-vertices pe)))))
	  (cond ((legal-contour sv)	;; then pass it on
		 (setf (vertices pe) sv)
		 (ev:announce pe (new-vertices pe) (vertices pe)))
		(t (setf (sl:on bt) t)))) ;; otherwise turn button back on
      (ev:announce pe (new-vertices pe) (vertices pe))) ;; point mode
    (setf (busy pe) nil)))

;;;-----------------------------------

(defun use-digitizer (pe)

  "use-digitizer pe

synchronously accepts input from sonic digitizer."

  (if (digitizer-present)
      (progn
	(sl:push-event-level)
	(digit-calibrate)
	(let* ((fr (sl:make-frame 160 75 :title "Digitizer"))
	       (win (sl:window fr))
	       (mb (sl:make-textline 150 30
				     :parent win
				     :ulc-x 5 :ulc-y 5
				     :label "Mag: "
				     :numeric t
				     :lower-limit 0.1
				     :upper-limit 10.0))
	       (eb (sl:make-exit-button 150 30
					:parent win
					:ulc-x 5 :ulc-y 40
					:bg-color 'sl:blue
					:label "Accept")))
	  (setf (sl:info mb) (digitizer-mag pe))
	  (sl:process-events)
	  (setf (digitizer-mag pe)
	    (coerce (read-from-string (sl:info mb)) 'single-float))
	  (sl:destroy mb)
	  (sl:destroy eb)
	  (sl:destroy fr))
	(let ((mag (float (/ (digitizer-mag pe))))
	      (scale (scale pe))
	      (pb (sl:make-readout 300 30 :title "Digitizer directions"))
	      state
	      x0 y0)
	  (loop
	    (setf (sl:info pb) "Please digitize the origin.")
	    (multiple-value-setq (state x0 y0) (digitize-point))
	    (when (eql state :point) (return)))
	  (loop
	    (setf (sl:info pb) "Now digitize points")
	    (multiple-value-bind (status x y) (digitize-point)
	      (case status
		(:done (sl:destroy pb)
		       (sl:pop-event-level)
		       (accept-vertices pe (accept-btn pe)) (return))
		(:point (pe-selected pe nil 1
				     (pix-x (* (- x x0) mag)
					    (x-origin pe) scale)
				     (pix-y (* (- y y0) mag)
					    (y-origin pe) scale)))
		(:delete-last (delete-vertex
			       (first (scratch-vertices pe))))
		(:delete-all (clear-vertices pe (clear-btn pe)))
		)))))
    (sl:acknowledge "Digitizer not available.")))

;;;-----------------------------------

(defmethod initialize-instance :after ((pe planar-editor)
				       &rest initargs &key title
				       &allow-other-keys)

  "Initializes the user interface for the planar editor.  The caller
  should provide an initial value for the next-mark-id slot and the
  contour-mode slot, if different from the default."

  (let* ((pic-size (clx:drawable-height (background pe))) ;; square
	 (btw 80)
	 (bth 25)
	 (peft (symbol-value *small-font*))
	 (bt-off 5)
         (ctrl-height (+ (* 2 bt-off) bth))
         (frm-height (+ pic-size ctrl-height))
         (frm (apply #'sl:make-frame pic-size frm-height
		     :title (or title "Prism Planar Editor")
		     initargs))
         (frm-win (sl:window frm))
         (pic (apply #'sl:make-picture pic-size pic-size
		     :parent frm-win
		     :ulc-x 0
		     :ulc-y ctrl-height
		     :border-width 0 ;; so doesn't flash
		     initargs))
         (hspace (+ btw bt-off))
         (accept-b (apply #'sl:make-button btw bth
			  :parent frm-win
			  :ulc-x bt-off :ulc-y bt-off
			  :font peft :label "Accept"
			  initargs))
         (clear-b (apply #'sl:make-button btw bth
			 :parent frm-win
			 :ulc-x (+ hspace bt-off)
			 :ulc-y bt-off
			 :font peft :label "Clear"
			 :button-type :momentary
			 initargs))
         (edit-mode-b (apply #'sl:make-button btw bth
			     :parent frm-win
			     :ulc-x (+ (* 2 hspace) bt-off)
			     :ulc-y bt-off
			     :font peft :label "Manual" ;; initial setting
			     :button-type :momentary
			     initargs))
         (tape-measure-b  (apply #'sl:make-button btw bth
				 :parent frm-win
				 :ulc-x (+ (* 3 hspace) bt-off)
				 :ulc-y bt-off
				 :font peft :label "Ruler"
				 :button-type :momentary
				 initargs))
	 (slider-x (+ (* 4 hspace) bt-off))
         (scale-s (apply #'sl:make-slider  ;; size to fit
			 (- pic-size slider-x bt-off) bth
			 5.0 100.0 ;; lo and hi range
			 :parent frm-win
			 :setting (scale pe)
			 :ulc-x slider-x :ulc-y bt-off
			 initargs)))
    (setf (fr pe) frm
	  (picture pe) pic
	  (scale-sdr pe) scale-s
	  (accept-btn pe) accept-b
	  (clear-btn pe) clear-b
	  (edit-mode-btn pe) edit-mode-b
	  (tape-measure-btn pe) tape-measure-b)
    (when (vertices pe)
      (setf (busy pe) t) ;; guard against turning Accept button on
      (setf (scratch-vertices pe) (vertex-list pe (vertices pe)))
      (setf (busy pe) nil))
    (ev:add-notify pe (sl:button-press pic) #'pe-selected)
    (ev:add-notify pe (sl:button-release pic) #'pe-deselected)
    (ev:add-notify pe (sl:motion-notify pic) #'pe-motion)
    (ev:add-notify pe (sl:value-changed scale-s)
		   #'(lambda (pe a new-val)
		       (declare (ignore a))
		       (when (not (busy pe))
			 (setf (busy pe) t)
			 (setf (scale pe) new-val)
			 (setf (busy pe) nil))))
    (ev:add-notify pe (sl:button-off accept-b) #'accept-vertices)
    (ev:add-notify pe (sl:button-on clear-b) #'clear-vertices)
    (ev:add-notify pe (sl:button-on edit-mode-b)
		   #'(lambda (pe bt)
		       (let ((selection (sl:popup-menu '("Manual"
							 "Automatic"
							 "Landmarks"
							 "Digitizer")))
			     (old-mode (edit-mode pe)))
			 (when selection
			   (case selection
			     (0 (setf (edit-mode pe) :manual))
			     (1 (if (contour-mode pe)
				    (setf (edit-mode pe) :automatic)
				  (sl:acknowledge '("Auto mode not available"
						    "for points"))))
			     (2 (setf (edit-mode pe) :landmarks))
			     (3 (setf (edit-mode pe) :digitizer)
				(setf (sl:label (edit-mode-btn pe))
				  "Digitizer")
				(use-digitizer pe)
				(setf (edit-mode pe) old-mode)))
			   (setf (sl:label (edit-mode-btn pe))
			     (case (edit-mode pe)
			       (:manual "Manual")
			       (:automatic "Auto")
			       (:landmarks "Landmarks"))))
			 (setf (sl:on bt) nil))))
    (ev:add-notify pe (sl:button-on tape-measure-b)
		   #'(lambda (pe bt)
		       (declare (ignore bt))
		       (unless (tape-measure pe) 
			 (let ((center (/ *easel-size* 2))
			       (x-origin (x-origin pe))
			       (y-origin (y-origin pe))
			       (scale (scale pe)))
			   (setf (tape-measure pe)
			     (make-tape-measure
			      :picture (picture pe)
			      :scale scale
			      :origin (list x-origin y-origin)
			      :x1 (cm-x (- center 20) x-origin scale)
			      :y1 (cm-x (- center 20) y-origin scale)
			      :x2 (cm-x (+ center 20) x-origin scale)
			      :y2 (cm-x (+ center 20) y-origin scale)))
			   (setf (sl:label (tape-measure-btn pe)) 
			     (write-to-string (fix-float
					       (tape-length
						(tape-measure pe)) 2)))
			   (ev:add-notify pe (new-length (tape-measure pe))
					  #'(lambda (pe tp len)
					      (declare (ignore tp))
					      (setf (sl:label
						     (tape-measure-btn pe)) 
						(write-to-string
						 (fix-float len 2)))))
			   (ev:add-notify pe (refresh (tape-measure pe))
					  #'(lambda (ped tp)
					      (declare (ignore tp))
					      (display-planar-editor
					       ped)))
			   (ev:add-notify pe (deleted (tape-measure pe))
					  #'(lambda (ped tp)
					      (declare (ignore tp))
					      (setf (sl:label
						     (tape-measure-btn ped))
						"Ruler")
					      (setf (tape-measure ped) nil)
					      (unless (busy ped)
						(display-planar-editor
						 ped))))
			   (display-planar-editor pe)))))))

;;;-----------------------------------

(defun make-planar-editor (&rest initargs)

  "make-planar-editor &rest initargs

Returns a contour/point editor with specified parameters."

  (apply #'make-instance 'planar-editor initargs))

;;;-----------------------------------

(defmethod destroy :before ((pe planar-editor))

  "Releases X resources used by this panel and its children."

  (if (not (contour-mode pe))
      (mapc #'(lambda (vt)
		(let ((pt (point vt)))
		  (ev:remove-notify vt (new-color pt))
		  (ev:remove-notify vt (new-loc pt))))
	    (scratch-vertices pe)))
  (sl:destroy (scale-sdr pe))
  (sl:destroy (accept-btn pe))
  (sl:destroy (clear-btn pe))
  (sl:destroy (edit-mode-btn pe))
  (when (tape-measure pe)
    (setf (busy pe) t)
    (destroy (tape-measure pe))
    (setf (busy pe) nil))
  (sl:destroy (tape-measure-btn pe))
  (sl:destroy (picture pe))
  (sl:destroy (fr pe)))

;;;-----------------------------------
;;; End.
