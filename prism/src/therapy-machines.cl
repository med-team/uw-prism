;;;
;;; therapy-machines
;;;
;;; This module contains the definition of the therapy-machine class.
;;; The data for each of the therapy machines used in Prism are
;;; contained in text files, one per therapy-machine instance, like
;;; the patient case data.  Thus the data can be read in and a
;;; therapy machine created by using get-all-objects as in the
;;; prism-db functions.
;;;
;;; 29-Dec-1992 I. Kalet from old prism
;;; 15-Apr-1993 I. Kalet change reader for collimator from coll-type
;;; 22-Apr-1993 I. Kalet add wedge support
;;; 24-Aug-1993 J. Unger remove plural 's' from particle types.
;;; 22-Oct-1993 J. Unger change names of Clinac therapy machine instances
;;; to conform to Beam data File Description report (TR-93-08-01).
;;; 01-Nov-1993 J. Unger add SL20-18MV machine (penumbra = 1.0 for now).
;;; 16-Nov-1993 J. Unger add CNTS machine.
;;; 03-Mar-1994 J. Unger add tray factor to therapy machine definition.
;;; 10-May-1994 J. Unger enhance object def as discussed in spec.
;;; 13-May-1994 I. Kalet move globals to prism-globals
;;; 13-May-1994 J. Unger add #'string-equal to get-therapy-machine.
;;;  1-Jun-1994 J. Jacky Actual machine data now in therapy-machines.dat
;;; 23-Jun-1994 I. Kalet add type single-float to energy.
;;; 23-Jun-1994 J. Jacky move scale-angle in from charts.cl
;;; 25-Jun-1994 J. Jacky correct scale-angle when lower limit is
;;; retrograde
;;; 27-Jun-1994 I. Kalet change "NO WEDGE" to lower case.
;;; 24-Aug-1994 J. Unger add inverse-scale-angle defun.
;;; 26-Jan-1995 I. Kalet add slots for dose computation support data,
;;; comments.  Change readers to accessors for beam utility.  Move
;;; *therapy-machines* here - not global, really internal to this
;;; module.  Makes this module not depend on prism-globals and
;;; therefore can more easily be used in beam data utility.  Add
;;; load-therapy-machines function.
;;; 18-Oct-1995 I. Kalet further mods for accomodating electron
;;; collimators, stereotactic radiosurgery beams (srs) and transfer
;;; data.
;;;  9-Jan-1996 I. Kalet split collim-info stuff to separate file, add
;;;  defaults for wedge list and other stuff.
;;;  2-Feb-1997 I. Kalet redo get-therapy-machine to load on demand
;;; rather than have all loaded at startup.  Also change
;;; get-therapy-machine-list to list all available machines, not just
;;; the loaded ones, by reading the index file, machine.index.  Make
;;; *therapy-machine-database* the default, not the current directory.
;;;  5-Jun-1997 I. Kalet change name of collimator slot to
;;;  collimator-type, change from peek-char to read with eof detection
;;;  in get-therapy-machine-list.
;;; 30-Jun-1997 I. Kalet change default for wedge-rot-angles to
;;; single-float value.
;;; 28-Aug-1997 BobGian modified comments in get-therapy-machine to
;;;   pave way for new Lisp dose calculation.
;;; 17-Sep-1997 I. Kalet Modify get-therapy-machine and
;;;   get-therapy-machine-list for new machine name and file name
;;;   scheme.  Add ident slot to hold short string to identify data set.
;;; 19-Sep-1997 BobGian notes here that references to old function
;;;   load-therapy-machines now refer instead to new function
;;;   get-therapy-machine.
;;; 15-Oct-1997 BobGian implement new wedge-info scheme.
;;; 26-Oct-1997 I. Kalet make wedge-id semantics more abstract, return
;;;   machine index list in same order as in the file.
;;; 22-Jan-1998 BobGian update to major revision using direct-mapping table
;;;   lookups and specialized multidimensional array access. Modify
;;;   get-therapy-machine accordingly and add convert-array and build-mapper
;;;   to build appropriate dose-info objects when reading in new machine.
;;;   Add new slots for mapper-arrays to wedge-info class defn.
;;; 09-Mar-1998 BobGian update convert-array, build-mapper, and
;;;   get-therapy-machine to conform with changes in dose calc.
;;; 13-Mar-1998 BobGian move mapper-vector code partially to dose-info
;;;   (portion which depends on specific dose/wedge-info slots) and rest
;;;   to new file: table-lookups (application-independent portion).
;;;   Also move wedge-info defclass and slot-type method to dose-info.
;;;   Wedge-related functions which access therapy-machine object slots
;;;   remain here.
;;; 28-Apr-1998 BobGian move build-mapper-tables here from dose-info to
;;;   resolve dependency conflict.
;;; 11-Jun-1998 I. Kalet downcase a bunch of names while checking
;;; correctness of get-therapy-machine.
;;; 18-Dec-1998 I. Kalet modifications to get-therapy-machine for
;;; electrons.
;;;  9-Feb-1999 I. Kalet add machine-postprocess to call
;;; build-mapper-tables for photons, default to nothing, generic
;;; function to allow for other postprocessing of machine data on
;;; loading.
;;;  6-Jul-1999 I. Kalet put erf stuff for electron beams here rather
;;; than in electron-dose, as it is potentially more general.
;;;  3-Feb-2000 BobGian optimize error-function table referencing: convert
;;; from general to special single-float array in machine-postprocess
;;; method for electron-dose-info, rewrite error-function to access
;;; table via argument rather than global variable, add declarations.
;;;  2-Mar-2000 BobGian correct datatype of single-float constant 1000.0
;;; in function error-function.
;;; 25-Apr-2000 BobGian add Irreg-specific constructors/convertors to
;;; build-mapper-tables.
;;; 26-Apr-2000 BobGian fix build-mapper-tables to allow for optional
;;; Irreg slots.  Remove HVL Irreg tables.
;;;  5-Feb-2001 I. Kalet revised use of ident slot for PDR, the Prism
;;; DICOM-RT client, add tray-accessory-code for same reason.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 27-Jun-2004 BobGian - remove all irreg-related slots:
;;;   PSF-TABLE-VECTOR, PSF-RADIUS-MAPPER, PSF-RADII, PSF-TABLE,
;;;   OAF-TABLE-VECTOR, OAF-RADIUS-MAPPER, OAF-RADII, OAF-TABLE.
;;; 1-Jul-2004 BobGian BUILD-MAPPER -> INTERPOLATE-MAPPER (more descriptive).
;;;   Discovered bug in it - fencepost error in generating mapping array.
;;; 9-Jul-2004 BobGian: Finish fix to INTERPOLATE-MAPPER bug - array resizing
;;;   on too-large-array caused rare boundary clash resulting in missing data
;;;   bin.  Fix was to remove resizing - bin size determined solely by data.
;;;   Scale-factor for float->integer conversion changed 1.0e4 -> 1.0e3.
;;;   Too large value causes over-large array allocation as inaccuracies in
;;;   floating-point arithmetic when scaled lead to very small GCDs and thus
;;;   very small bin sizes, causing array size overflow.  For example, using
;;;   1.0e6 caused array of size 26,000,000 to be requested (max is 16 Meg).
;;;   Tried range of values - 1.0e3, 1.0e4, and 1.0e5 all gave identical
;;;   results.  1.0e3 seems safest and still allows mapper to work with tables
;;;   measured to 1/1000 of a centimeter (or 1/100 of a millimeter).
;;; 31-Jan-2005 AMSimms - corrected use of Allegro specific coercions, now 
;;;   using coerce explicitly.
;;; 22-Jun-2007 I. Kalet fix declaration in machine-postprocess
;;;

(in-package :prism)

;;;=============================================================

(defvar *therapy-machines* nil
  "The list of actual therapy machine instances, which grows as needed
by reading in data from the therapy machines files.")

(defvar *therapy-machine-list* nil
  "The list of therapy machine names, which is initialized from the
index file the first time get-therapy-machine-list references it.")

(defvar *machine-supp-list* nil
  "The list of old or decommissioned therapy machine names, which is
initialized from the machine name supplemental file the first time it
is referenced.")

(defvar *erf-table* nil
  "The table of precomputed values for the error function, used in the
electron dose code, and loaded once, on demand.")

;;;=============================================================

(defclass therapy-machine ()

  ((name :type string
	 :initarg :name
	 :accessor name
	 :documentation "A unique short string identifying which
particular machine this data set is for.  This is a generic name, like
CLINAC2500-6MV, not a name identifying when the data were measured or
different actual measurements.  That information is in the comments slot.")

   (ident :type list
	  :initarg :ident
	  :accessor ident
	  :documentation "A list of three strings, used by the
DICOM-RT client: a unique short string identifying the particular
machine to the machine DICOM-RT server, the AE title for this
machine's DICOM-RT server, and the IP address of the DICOM-RT server")

   (comments :type list
	     :initarg :comments
	     :accessor comments
	     :documentation "A list of strings of comments about the
current data set.  Could be used to note details about changes in the
data, like date taken, reference depth for tpr data and other tables, etc.")

   (particle :initarg :particle
	     :accessor particle
	     :documentation "Symbol, one of, e.g., photon, neutron, electron")

   (energy :type single-float
	   :initarg :energy
	   :accessor energy
	   :documentation "The nominal beam energy, in MeV")

   (penumbra :type single-float
	     :initarg :penumbra
	     :accessor penumbra
	     :documentation "The approximate penumbra width in cm,
i.e., distance from 90 percent of central axis dose to 10 percent.")

   (cal-distance :type single-float
		 :initarg :cal-distance
		 :accessor cal-distance
		 :documentation "The nominal source to axis distance
for this machine, in cm, typically 100.0 cm for photon linacs.")

   (tray-factor :type single-float
		:initarg :tray-factor
		:accessor tray-factor
		:documentation "The transmittance of the blocking tray for
this machine.  Total opacity is 0.0, and total transmittance is 1.0.")

   (tray-accessory-code :initarg :tray-accessory-code
			:accessor tray-accessory-code
			:documentation "The code used by the DICOM-RT
client to identify the use of the blocking tray in a treatment field.")

   (gantry-scale :type single-float
		 :initarg :gantry-scale
		 :accessor gantry-scale
		 :documentation "Gantry angle scale factor.")

   (gantry-offset :type single-float
		  :initarg :gantry-offset
		  :accessor gantry-offset
		  :documentation "Gantry angle offset.")

   (turntable-scale :type single-float
		    :initarg :turntable-scale
		    :accessor turntable-scale
		    :documentation "Turntable angle scale factor.")

   (turntable-offset :type single-float
		     :initarg :turntable-offset
		     :accessor turntable-offset
		     :documentation "Turntable angle offset.")

   (turntable-negative-flag :type (member nil t)
			    :initarg :turntable-negative-flag
			    :accessor turntable-negative-flag
			    :documentation "Turntable angle negative flag.")

   (turntable-upper-limit :type single-float
			  :initarg :turntable-upper-limit
			  :accessor turntable-upper-limit
			  :documentation "Upper limit of turntable motion.")

   (turntable-lower-limit :type single-float
			  :initarg :turntable-lower-limit
			  :accessor turntable-lower-limit
			  :documentation "Lower limit of turntable motion.")

   (collimator-scale :type single-float
		     :initarg :collimator-scale
		     :accessor collimator-scale
		     :documentation "Collimator angle scale factor.")

   (collimator-offset :type single-float
		      :initarg :collimator-offset
		      :accessor collimator-offset
		      :documentation "Collimator angle offset.")

   (collimator-upper-limit :type single-float
			   :initarg :collimator-upper-limit
			   :accessor collimator-upper-limit
			   :documentation
			   "Upper limit of collimator rotation.")

   (collimator-lower-limit :type single-float
			   :initarg :collimator-lower-limit
			   :accessor collimator-lower-limit
			   :documentation
			   "Lower limit of collimator rotation.")

   (collimator-negative-flag :type (member nil t)
			     :initarg :collimator-negative-flag
			     :accessor collimator-negative-flag
			     :documentation
			     "Collimator angle negative flag.")

   (collimator-type :initarg :collimator-type
		    :accessor collimator-type
		    :documentation "Symbol naming one of the
collimator types, e.g., symmetric-jaw-coll")

   ;; Change to collimator-data to be consistent with others below?
   (collimator-info                          ;; :type a collimator-info object
     :initarg :collimator-info
     :accessor collimator-info
     :documentation "Collimator-info object, with
collimator-specific attributes and values.")

   (wedge-rot-scale :type single-float
		    :initarg :wedge-rot-scale
		    :accessor wedge-rot-scale
		    :documentation "Wedge rotation angle scale factor.")

   (wedge-rot-offset :type single-float
		     :initarg :wedge-rot-offset
		     :accessor wedge-rot-offset
		     :documentation "Wedge rotation angle offset.")

   (wedge-rot-print-flag :type (member nil t)
			 :initarg :wedge-rot-print-flag
			 :accessor wedge-rot-print-flag
			 :documentation "Indicates whether to print
wedge rotation information on chart or not.")

   (wedges :type list
	   :initarg :wedges
	   :accessor wedges
	   :documentation "A list of wedge-info objects, one for each
wedge the machine can use.")

   (dose-data :initarg :dose-data
	      :accessor dose-data
	      :documentation "A dose-info object with slots and
contents that depend on the type of machine, photon, electron etc.")

   (transfer-data :initarg :transfer-data
		  :accessor transfer-data
		  :documentation "The information needed to write out a
file or files of beam parameters to be sent to a computer controlled
accelerator.")

   )

  (:default-initargs :comments nil :tray-factor 1.0 :wedges nil
		     :wedge-rot-scale 1.0
		     :wedge-rot-offset 0.0
		     :wedge-rot-print-flag nil
		     :transfer-data nil)

  (:documentation "Therapy-machine describes a particular external
radiation source - its properties, rather than the settings of the
adjustable parameters of treatment.  A plan may have several beams all
of which use the same therapy machine, or which use different ones.
The slots of a machine object should not be updated by Prism planning
code but should be updated by a separate machine data management
program.")

  )

;;;--------------------------------------------------

(defmethod slot-type ((obj therapy-machine) slotname)

  (case slotname
    ((collimator-info dose-data transfer-data) :object)
    (wedges :object-list)
    (otherwise :simple)))

;;;--------------------------------------------------

(defun make-therapy-machine (&rest initargs)

  "make-therapy-machine &rest initargs

Returns a therapy machine object with the specified initialization args."

  (apply #'make-instance 'therapy-machine initargs))

;;;--------------------------------------------------

(defun get-machine-filename (mach-name indexdir)

  "get-machine-filename mach-name indexdir

returns the filename for the machine whose name is MACH-NAME.
First checks the machine index, then the supplemental list.  In the
supplemental list it first checks if the equivalent generic name has
an associated filename, and if not, uses the supplemental filename.
If the therapy-machine list is not yet read in, INDEXDIR is used as
the directory for the index files."

  (if (find mach-name (get-therapy-machine-list indexdir)
	    :test #'equal)
      (second (assoc mach-name *therapy-machine-list* :test #'equal))
      (let ((supp-entry (assoc mach-name
			       (or *machine-supp-list*
				   (setq *machine-supp-list*
					 (get-index-list "machine.supp"
							 indexdir nil)))
			       :test #'equal)))
	(if supp-entry
	    (if (equal (second supp-entry) "") (third supp-entry)
		(second (assoc (second supp-entry)
			       *therapy-machine-list*
			       :test #'equal)))))))

;;;--------------------------------------------------

(defun get-therapy-machine (mach-name database indexdir)

  "get-therapy-machine mach-name database indexdir

returns the therapy machine instance named by the string MACH-NAME.
If it is not already loaded into Prism, it is retrieved from the
database specified by database, a directory name or pathname, and kept
in working memory.  The MACH-NAME string is used as a key to look up the
data file name in the machine list, and if it is not found there the
supplemental list is used.  The index files are assumed to be in the
directory specified by indexdir, also a directory name or pathname."

  (or (find mach-name *therapy-machines* :key #'name :test #'equal)
      ;;
      (find (second (assoc mach-name
			   (or *machine-supp-list*
			       (setq *machine-supp-list*
				     (get-index-list "machine.supp"
						     indexdir nil)))
			   :test #'equal))
	    *therapy-machines*
	    :key #'name :test #'equal)
      ;;
      ;; Read in new machine and convert all dose-related tables to structure
      ;; required by direct-mapped specialized-array dose computation system.
      (let* ((machine-obj (first (get-all-objects
				   (merge-pathnames
				     (get-machine-filename mach-name indexdir)
				     database))))
	     (dose-info-obj (dose-data machine-obj)))
	;;
	(machine-postprocess dose-info-obj machine-obj)
	(push machine-obj *therapy-machines*)
	;;
	machine-obj)))

;;;--------------------------------------------------

(defmethod machine-postprocess ((dose-info-obj t) machine-obj)

  (declare (ignore machine-obj))
  nil)

;;;--------------------------------------------------

(defmethod machine-postprocess ((dose-info-obj photon-dose-info) machine-obj)

  (build-mapper-tables dose-info-obj machine-obj))

;;;--------------------------------------------------

(defmethod machine-postprocess ((dose-info-obj electron-dose-info) machine-obj)

  (declare (ignore machine-obj))

  (unless (arrayp *erf-table*)
    (with-open-file (strm (merge-pathnames "erf.tab"
					   *therapy-machine-database*)
			  :direction :input)
      (let* ((old-erf-table (read strm))
	     (new-erf-table (make-array (array-total-size old-erf-table)
					:element-type 'single-float)))
	(declare (type (simple-array t (#.erf-table-size))
		       old-erf-table)
		 (type (simple-array single-float (#.erf-table-size))
		       new-erf-table))
	(dotimes (idx #.erf-table-size)
	  (declare (type fixnum idx))
	  (setf (aref new-erf-table idx) (aref old-erf-table idx)))
	(setq *erf-table* new-erf-table)))))

;;;-----------------------------------------------

(defun error-function (a erf-table)

  "error-function a erf-table

Returns error function value for input A, single-float, via table look-up"

  (declare (type (simple-array single-float (#.erf-table-size)) erf-table)
	   (type single-float a))

  (let ((sign-a (if (< a 0.0) -1.0 1.0)))
    (declare (type single-float sign-a))
    (setq a (the single-float (abs a)))
    (cond ((= a 0.0)
	   0.0)
	  ((< a 3.0)
	   (* sign-a
	      (the single-float
		(aref erf-table
		      (the fixnum (round (the single-float (* a 1000.0))))))))
	  (t sign-a))))

;;;--------------------------------------------------

(defun get-therapy-machine-list (indexdir)

  "get-therapy-machine-list indexdir

Returns a list of strings, each one identifying one therapy machine.
If the special variable *therapy-machine-list* is non-nil it is used,
otherwise the machine index file in indexdir is read in and
*therapy-machine-list* is set to the resulting list.  The indexdir
parameter is a pathname identifying the directory in which the machine
database index is located, so the scheme allows variant mappings of
official machine names to different data file names e.g., one for
clinical use and one for testing.  If the index is missing or
inaccessible the function returns nil."

  (mapcar #'first
      (or *therapy-machine-list*
	  (setq *therapy-machine-list*
		(nreverse (get-index-list "machine.index" indexdir nil))))))

;;;--------------------------------------------------

(defun wedge-label (wedge-id mach)

  "wedge-label wedge-id mach

Returns the string that labels the wedge specified by WEDGE-ID for
machine object mach, or a \"No wedge\" label if wedge-id is 0."

  (if (eql wedge-id 0) "No wedge"
      (name (find wedge-id (wedges mach) :key #'ID))))

;;;--------------------------------------------------

(defun wedge-id-from-name (wedge-name mach)

  "wedge-id-from-name wedge-name mach

returns the wedge id of the wedge whose name is wedge-name in machine
mach."

  (if (string-equal wedge-name "No wedge") 0
      (id (find wedge-name (wedges mach)
		:key #'name :test #'string-equal))))

;;;--------------------------------------------------

(defun wedge-rot-angles (wedge-id mach)

  "wedge-rot-angles wedge-id mach

returns a list of allowable rotation angles for wedge wedge-id in
machine mach.  Each wedge may have different allowed rotation angles."

  (rot-angles (find wedge-id (wedges mach) :key #'id)))

;;;--------------------------------------------------

(defun wedge-names (mach)

  "wedge-names mach

Returns a list of strings labeling the available wedges for machine
object mach."

  (cons "No wedge" (mapcar #'name (wedges mach))))

;;;--------------------------------------------------

(defun scale-angle (angle scale offset &optional n-flag lower upper)

  "scale-angle angle scale offset &optional n-flag lower upper

Returns a list of two things: the scaled machine rotation angle, and a
label.  The scaled rotation angle (gantry, collim, table, or wedge) is
computed from angle in the internal Prism coordinate system to the
machine-specific coordinate system, using scale, offset, n-flag, lower
and upper, the scaling constants from the beam's therapy machine.  The
label is the string deg if the angle is within range, or if the
optional upper and lower limit parameters are absent or nil.  The
label is the string *** if limits are specified and the scaled
rotation angle is out of range for that machine.  The scaled machine
rotation is usually adjusted to lie in the range 0 to 360.  The
optional n-flag argument is the name of the function used to look up a
boolean flag which determines whether angles in the range 180 to 360
should be shifted to angles in the range -180 to 0."

;;; Because these are rotations, there is a special case where the lower
;;; limit represents retrograde motion.  For the SL20 turntable, the home
;;; position is 0, the upper limit is 115 degrees, and the lower limit is
;;; 229 degress.  Actually this lower limit represents a retrograde motion
;;; back to -131 degrees.  These situations are indicated by a lower limit
;;; that is larger than the upper limit.

  (let* ((as (+ offset (* scale angle)))
	 (am (cond ((< as 0) (if n-flag as (+ as 360)))
		   ((<= 0 as 180) as)
		   ((< 180 as 360) (if n-flag (- as 360) as))
		   ((<= 360 as) (- as 360))))
	 (label (cond ((not lower) "deg")           ; no limits specified
		      ((<= lower am upper)          ; limits specified, angle
		       ; in range
		       "deg")
		      ((> lower upper)           ; retrograde motion - special
		       ; case
		       (let ((ln (- lower 360))
			     (an (if (<= am upper) am (- am 360))))
			 (if (<= ln an upper) "deg" "***")))
		      (t "***"))))                ; limits specifed, angle not
    ; in range
    (list am label)))

;;;----------------------------------------------------

(defun inverse-scale-angle (angle scale offset)

  "inverse-scale-angle angle scale offset

Scales an angle from a vendor-specific coordinate system back
to a Prism coordiate system.  This is the inverse of scale-angle.
The inverse-scale'd angle (and no label) is returned."

  (mod (/ (- angle offset) scale)
       360.0))

;;;--------------------------------------------------
;;; Function for building mapping tables.
;;;--------------------------------------------------

(defun build-mapper-tables (dose-info-obj machine-obj)

  "build-mapper-tables dose-info-obj machine-obj

builds the mapping tables used for fast table lookup in the dose
calculation by storing the converted arrays and mapper tables into
tpr-table, etc, slots in the dose-info object in dose-info-obj and
into profile-table, etc, slots in the wedge-info objects in wedges
slot of machine-obj."

  ;; Outputfactor tables.
  (setf (outputfactor-table dose-info-obj)
	(convert-array (outputfactor-table dose-info-obj)))
  (multiple-value-bind (of-sf of-ofs of-map)
      (interpolate-mapper
	(setf (outputfactor-fieldsizes dose-info-obj)
	      (convert-array (outputfactor-fieldsizes dose-info-obj))))
    (declare (type (simple-array t 1) of-map)
	     (type single-float of-sf of-ofs))
    ;; Slot 0 for input/output, other two for these parameters.
    (let ((of-vec (make-array 3 :element-type 'single-float)))
      (declare (type (simple-array single-float (3)) of-vec))
      (setf (aref of-vec 1) of-sf)
      (setf (aref of-vec 2) of-ofs)
      (setf (outputfactor-vector dose-info-obj) of-vec))
    (setf (outputfactor-fss-mapper dose-info-obj) of-map))

  ;; TPR0 tables.
  (setf (tpr0-table dose-info-obj)
	(convert-array (tpr0-table dose-info-obj)))
  (multiple-value-bind (tpr0-sf tpr0-ofs tpr0-map)
      (interpolate-mapper
	(setf (tpr0-depths dose-info-obj)
	      (convert-array (tpr0-depths dose-info-obj))))
    (declare (type (simple-array t 1) tpr0-map)
	     (type single-float tpr0-sf tpr0-ofs))
    ;; Slot 0 for input/output, other two for these parameters.
    (let ((tpr0-vec (make-array 3 :element-type 'single-float)))
      (declare (type (simple-array single-float (3)) tpr0-vec))
      (setf (aref tpr0-vec 1) tpr0-sf)
      (setf (aref tpr0-vec 2) tpr0-ofs)
      (setf (tpr0-table-vector dose-info-obj) tpr0-vec))
    (setf (tpr0-depth-mapper dose-info-obj) tpr0-map))

  ;; TPR tables.
  (setf (tpr-table dose-info-obj)
	(convert-array (tpr-table dose-info-obj)))
  (multiple-value-bind (tpr-fss-sf tpr-fss-ofs tpr-fss-map)
      (interpolate-mapper
	(setf (tpr-fieldsizes dose-info-obj)
	      (convert-array (tpr-fieldsizes dose-info-obj))))
    (declare (type (simple-array t 1) tpr-fss-map)
	     (type single-float tpr-fss-sf tpr-fss-ofs))
    (multiple-value-bind (tpr-depth-sf tpr-depth-ofs tpr-depth-map)
	(interpolate-mapper
	  (setf (tpr-depths dose-info-obj)
		(convert-array (tpr-depths dose-info-obj))))
      (declare (type (simple-array t 1) tpr-depth-map)
	       (type single-float tpr-depth-sf tpr-depth-ofs))
      ;; Slots 0,1 for input/output, rest for these parameters.
      (let ((tpr-vec (make-array 6 :element-type 'single-float)))
	(declare (type (simple-array single-float (6)) tpr-vec))
	(setf (aref tpr-vec 2) tpr-fss-sf)
	(setf (aref tpr-vec 3) tpr-depth-sf)
	(setf (aref tpr-vec 4) tpr-fss-ofs)
	(setf (aref tpr-vec 5) tpr-depth-ofs)
	(setf (tpr-table-vector dose-info-obj) tpr-vec))
      (setf (tpr-fss-mapper dose-info-obj) tpr-fss-map)
      (setf (tpr-depth-mapper dose-info-obj) tpr-depth-map)))

  ;; SPR tables.
  (setf (spr-table dose-info-obj)
	(convert-array (spr-table dose-info-obj)))
  (multiple-value-bind (spr-rad-sf spr-rad-ofs spr-rad-map)
      (interpolate-mapper
	(setf (spr-radii dose-info-obj)
	      (convert-array (spr-radii dose-info-obj))))
    (declare (type (simple-array t 1) spr-rad-map)
	     (type single-float spr-rad-sf spr-rad-ofs))
    (multiple-value-bind (spr-depth-sf spr-depth-ofs spr-depth-map)
	(interpolate-mapper
	  (setf (spr-depths dose-info-obj)
		(convert-array (spr-depths dose-info-obj))))
      (declare (type (simple-array t 1) spr-depth-map)
	       (type single-float spr-depth-sf spr-depth-ofs))
      ;; Slots 0,1 for input/output, rest for these parameters.
      (let ((spr-vec (make-array 6 :element-type 'single-float)))
	(declare (type (simple-array single-float (6)) spr-vec))
	(setf (aref spr-vec 2) spr-rad-sf)
	(setf (aref spr-vec 3) spr-depth-sf)
	(setf (aref spr-vec 4) spr-rad-ofs)
	(setf (aref spr-vec 5) spr-depth-ofs)
	(setf (spr-table-vector dose-info-obj) spr-vec))
      (setf (spr-radius-mapper dose-info-obj) spr-rad-map)
      (setf (spr-depth-mapper dose-info-obj) spr-depth-map)))

  ;; OCR tables.
  (setf (ocr-table dose-info-obj)
	(convert-array (ocr-table dose-info-obj)))
  (multiple-value-bind (ocr-fss-sf ocr-fss-ofs ocr-fss-map)
      (interpolate-mapper
	(setf (ocr-fieldsizes dose-info-obj)
	      (convert-array (ocr-fieldsizes dose-info-obj))))
    (declare (type (simple-array t 1) ocr-fss-map)
	     (type single-float ocr-fss-sf ocr-fss-ofs))
    (multiple-value-bind (ocr-depth-sf ocr-depth-ofs ocr-depth-map)
	(interpolate-mapper
	  (setf (ocr-depths dose-info-obj)
		(convert-array (ocr-depths dose-info-obj))))
      (declare (type (simple-array t 1) ocr-depth-map)
	       (type single-float ocr-depth-sf ocr-depth-ofs))
      (multiple-value-bind (ocr-fan-sf ocr-fan-ofs ocr-fan-map)
	  (interpolate-mapper
	    (setf (ocr-fanlines dose-info-obj)
		  (convert-array (ocr-fanlines dose-info-obj))))
	(declare (type (simple-array t 1) ocr-fan-map)
		 (type single-float ocr-fan-sf ocr-fan-ofs))
	;; Slots 0,1,2 for input/output, rest for these parameters.
	(let ((ocr-vec (make-array 9 :element-type 'single-float)))
	  (declare (type (simple-array single-float (9)) ocr-vec))
	  (setf (aref ocr-vec 3) ocr-fss-sf)
	  (setf (aref ocr-vec 4) ocr-depth-sf)
	  (setf (aref ocr-vec 5) ocr-fan-sf)
	  (setf (aref ocr-vec 6) ocr-fss-ofs)
	  (setf (aref ocr-vec 7) ocr-depth-ofs)
	  (setf (aref ocr-vec 8) ocr-fan-ofs)
	  (setf (ocr-table-vector dose-info-obj) ocr-vec))
	(setf (ocr-fss-mapper dose-info-obj) ocr-fss-map)
	(setf (ocr-depth-mapper dose-info-obj) ocr-depth-map)
	(setf (ocr-fanline-mapper dose-info-obj) ocr-fan-map))))

  ;; Wedge tables.
  (dolist (wdg-info-obj (wedges machine-obj))
    (setf (profile-table wdg-info-obj)
	  (convert-array (profile-table wdg-info-obj)))
    (multiple-value-bind (wdg-depth-sf wdg-depth-ofs wdg-depth-map)
	(interpolate-mapper
	  (setf (profile-depths wdg-info-obj)
		(convert-array (profile-depths wdg-info-obj))))
      (declare (type (simple-array t 1) wdg-depth-map)
	       (type single-float wdg-depth-sf wdg-depth-ofs))
      (multiple-value-bind (wdg-posn-sf wdg-posn-ofs wdg-posn-map)
	  (interpolate-mapper
	    (setf (profile-positions wdg-info-obj)
		  (convert-array (profile-positions wdg-info-obj))))
	(declare (type (simple-array t 1) wdg-posn-map)
		 (type single-float wdg-posn-sf wdg-posn-ofs))
	;; Slots 0,1 for input/output, rest for these parameters.
	(let ((wdg-vec (make-array 6 :element-type 'single-float)))
	  (declare (type (simple-array single-float (6)) wdg-vec))
	  (setf (aref wdg-vec 2) wdg-depth-sf)
	  (setf (aref wdg-vec 3) wdg-posn-sf)
	  (setf (aref wdg-vec 4) wdg-depth-ofs)
	  (setf (aref wdg-vec 5) wdg-posn-ofs)
	  (setf (profile-table-vector wdg-info-obj) wdg-vec))
	(setf (profile-depth-mapper wdg-info-obj) wdg-depth-map)
	(setf (profile-position-mapper wdg-info-obj) wdg-posn-map)))))

;;;-------------------------------------------------------------

(defun interpolate-mapper (scale-array &aux
			   (scale-dim (array-total-size scale-array)))

  (declare (type (simple-array single-float 1) scale-array)
	   (type fixnum scale-dim))

  (cond
    ((= scale-dim 1)
     (values 0.0 0.0 (make-array 1 :element-type t :initial-element 0)))

    (t (let* ((offset (aref scale-array 0))
	      (last-val (aref scale-array 1))
	      (gcd-so-far (the fixnum (round (* 1.0e3 (- last-val offset)))))
	      (float-gcd (coerce gcd-so-far 'single-float)))
	 (declare (type single-float offset last-val float-gcd)
		  (type fixnum gcd-so-far))
	 (unless (> gcd-so-far 0)
	   (error "INTERPOLATE-MAPPER [1]"))
	 (do ((idx1 2 (1+ idx1))
	      (next-round-val 0)
	      (this-val 0.0))
	     ((= idx1 scale-dim)
	      ;;
	      (let ((scale-factor (/ 1.0e3 float-gcd))
		    (sz 0))
		(declare (type single-float scale-factor)
			 (type fixnum sz))
		(setq sz (1+ (the fixnum
			       (round (/ (* 1.0e3 (- last-val offset))
					 float-gcd)))))
		(let ((map-array
			(make-array sz :element-type t :initial-element -1)))
		  (declare (type (simple-array t 1) map-array))
		  (do ((idx2 0 (1+ idx2)))
		      ((= idx2 scale-dim))
		    (declare (type fixnum idx2))
		    (do ((map-array-idx
			   (the fixnum
			     (round (* scale-factor
				       (- (aref scale-array idx2) offset))))
			   (1- map-array-idx)))
			((or (< map-array-idx 0)
			     (/= (svref map-array map-array-idx) -1)))
		      (declare (type fixnum map-array-idx))
		      (setf (svref map-array map-array-idx) idx2)))
		  (values scale-factor offset map-array))))

	   (declare (type single-float this-val)
		    (type fixnum idx1 next-round-val))

	   (setq this-val (aref scale-array idx1)
		 next-round-val (round (* 1.0e3 (- this-val last-val))))
	   (unless (> next-round-val 0)
	     (error "INTERPOLATE-MAPPER [2]"))
	   (setq gcd-so-far (gcd gcd-so-far next-round-val)
		 float-gcd (coerce gcd-so-far 'single-float)
		 last-val this-val))))))

;;;--------------------------------------------------
;;; End.
