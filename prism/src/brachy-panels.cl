;;;
;;; brachy-panels
;;;
;;; Definitions of control panels for radiation sources for
;;; brachytherapy, i.e., line sources and seeds.
;;;
;;;  4-Jun-1996 I. Kalet created.
;;; 24-Aug-1997 I. Kalet continue construction.
;;; 19-Dec-1999 I. Kalet implement source spec. entry subpanels.
;;; 31-Jan-2000 I. Kalet implement source table panel and other functions.
;;; 27-Feb-2000 I. Kalet implement source coordinate entry from
;;; digitizer, and split source-specs-panel to separate module.
;;;  5-Mar-2000 I. Kalet split ortho film entry code to separate module.
;;; 27-Mar-2000 I. Kalet continuing implementation...
;;; 17-Apr-2000 I. Kalet added seed dose mini-spreadsheet
;;; 27-Apr-2000 I. Kalet protect from selecting seed dose spreadsheet
;;; when there are either no points or no seeds.  Add keyboard input.
;;; 11-May-2000 I. Kalet fix call to source-menu to conform to new
;;; definitions.  Also raise limits on source application times, and
;;; parametrize.
;;; 28-May-2000 I. Kalet parametrize small font.
;;; 26-Nov-2000 I. Kalet cosmetic changes in dialog box.
;;;  1-Apr-2002 I. Kalet big overhaul to make a nice interface.  Put
;;; seed spreadsheet directly on panel, rearrange all other controls.
;;;  5-May-2002 I. Kalet begin reimplementation of coordinate entry
;;; 26-Jul-2002 I. Kalet overhaul continued, add event regisrations, etc.
;;; 12-Aug-2002 I. Kalet add delta-z for seeds in ortho mode
;;;  6-Oct-2002 I. Kalet add line source support back in.
;;; 29-Jan-2003 I. Kalet add registration with events to synchronize
;;; the current and end source numbers when changing entry mode.
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass brachy-panel (generic-panel)

  ((line-sources :accessor line-sources
		 :initarg :line-sources
		 :documentation "The collection containing all the
line sources")

   (seeds :accessor seeds
	  :initarg :seeds
	  :documentation "The collection containing all the seeds.")

   (points :accessor points
	   :initarg :points
	   :documentation "The collection of points from the case.")

   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame for this panel.")

   (delete-b :accessor delete-b
	     :documentation "The Delete Panel button.")

   (entry-mode-label :accessor entry-mode-label
		     :documentation "A readout labeling the entry mode
menu")

   (entry-mode :accessor entry-mode
	       :initform 'individual
	       :documentation "The entry mode specifys which type of
sources is currently active, a symbol, either seeds or line-sources.")

   (entry-mode-menu :accessor entry-mode-menu
		    :documentation "The menu used to select the
coordinate entry mode.")

   (entry-method-label :accessor entry-method-label
		     :documentation "A readout labeling the entry
method menu")

   (entry-method :accessor entry-method
		 :initform 'xyz
		 :documentation "The entry method, a symbol specifying
the coordinate entry method currently active, one of xyz, ortho-film,
table-shift, image.")

   (entry-method-menu :accessor entry-method-menu
		      :documentation "The menu used to select the
coordinate entry method.")

   (entry-subpanel :accessor entry-subpanel
		   :initform nil
		   :documentation "The subpanel providing the controls
and displays that depend on the current coordinate entry mode.")

   (current :accessor current
	    :initform 1
	    :documentation "The cached value of the current source
	    being entered or modified so that it will be preserved
	    across changes of entry mode.")

   (end-source :accessor end-source
	       :initform 1
	       :documentation "The cached value of the last source to
	    be entered or modified so that it will be preserved
	    across changes of entry mode.")

   (dose-subpanel :accessor dose-subpanel
		  :initform nil
		  :documentation "The mini-spreadsheet for displaying
dose rates and total doses to points.")

   (source-update-subpanel :accessor source-update-subpanel
			   :documentation "The subpanel for modifying
and deleting source specs.")

   (line-specs-subpanel :accessor line-specs-subpanel
			:documentation "The subpanel for entering and
editing the numeric and catalog data about line sources.")

   (seed-specs-subpanel :accessor seed-specs-subpanel
			:documentation "The subpanel for entering and
editing the numeric and catalog data about seeds.")

   ))

;;;---------------------------------------------

(defun make-brachy-panel (&rest initargs)

  "make-brachy-panel &rest initargs

Returns a brachytherapy source panel for the two collections
line-sources and seeds, listed in initargs."

  (apply #'make-instance 'brachy-panel initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((bp brachy-panel) &rest initargs)

  (let* ((bpf (symbol-value *small-font*))
	 (bth 25) ;; button and textline height for small font
	 (btw 90) ;; regular button and textline width
	 ;; (sbw 20) ;; small button width
	 (dx 10) ;; left margin
	 (top-y 10)
	 (specs-pan-hgt (apply #'+ top-y top-y *brachy-specs-row-heights*))
	 (update-pan-hgt (apply #'+ top-y top-y *brachy-update-row-heights*))
	 (pan-fr (apply #'sl:make-frame
			(apply #'+ dx dx dx *brachy-specs-col-widths*)
			(+ 190 update-pan-hgt specs-pan-hgt specs-pan-hgt)
			:title "Prism Brachytherapy Panel" initargs))
	 (bp-win (sl:window pan-fr))
	 (del-b (apply #'sl:make-button btw bth :button-type :momentary
		       :ulc-x dx :ulc-y top-y
		       :font bpf :label "Delete Panel"
		       :parent bp-win initargs))
	 (mode-r (apply #'sl:make-readout btw bth
			:ulc-x dx
			:ulc-y (bp-y top-y bth 1)
			:border-width 0
			:font bpf :label "Source type"
			:parent bp-win initargs))
	 (mode-m (apply #'sl:make-radio-menu
			'("Seeds" "Line sources")
			:ulc-x dx
			:ulc-y (bp-y top-y bth 2)
			:font bpf
			:parent bp-win initargs))
	 (method-r (apply #'sl:make-readout btw bth
			  :ulc-x btw
			  :ulc-y (bp-y top-y bth 1)
			  :border-width 0
			  :font bpf :label "Entry method"
			  :parent bp-win initargs))
	 (method-m (apply #'sl:make-radio-menu
			  '("XYZ" "Ortho films" "Table shift" "Images")
			  :ulc-x (+ dx btw)
			  :ulc-y (bp-y top-y bth 2)
			  :font bpf
			  :parent bp-win initargs))
	 ;; initial entry mode is seeds, so second arg is nil
	 (mods-panel (make-brachy-update-panel (seeds bp) nil bp-win
					       (+ dx btw)
					       (- (bp-y top-y bth 6) 5)))
	 (dose-spr (make-brachy-dose-panel :seeds (seeds bp)
					   :line-sources (line-sources bp)
					   :pointlist (points bp)
					   :parent bp-win
					   :ulc-x 550
					   :ulc-y top-y))
	 (seed-specs-pan (make-brachy-specs-panel
			  (seeds bp) bp-win
			  dx (- (sl:height pan-fr) specs-pan-hgt
				specs-pan-hgt)))
	 (line-specs-pan (make-brachy-specs-panel
			  (line-sources bp) bp-win
			  dx (- (sl:height pan-fr) specs-pan-hgt))))
    (let ((sheet (panel-frame seed-specs-pan)))
      (sl:set-contents sheet 0 7 "    X")
      (sl:set-contents sheet 0 8 "    Y")
      (sl:set-contents sheet 0 9 "    Z")
      (sl:set-contents sheet 0 10 "Delta Z"))
    (setf (panel-frame bp) pan-fr ;; put all the widgets in the slots
	  (delete-b bp) del-b
	  (entry-mode-label bp) mode-r
	  (entry-mode-menu bp) mode-m
	  (entry-method-label bp) method-r
	  (entry-method-menu bp) method-m
	  (source-update-subpanel bp) mods-panel
	  (dose-subpanel bp) dose-spr
	  (seed-specs-subpanel bp) seed-specs-pan
	  (line-specs-subpanel bp) line-specs-pan)
    (ev:add-notify bp (sl:button-on del-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (destroy pan)))
    (ev:add-notify bp (sl:selected mode-m)
		   #'(lambda (pan mnu item)
		       (declare (ignore mnu))
		       (setf (entry-mode pan)
			 (nth item '(seeds line-sources)))
		       ))
    (ev:add-notify bp (sl:selected method-m)
		   #'(lambda (pan mnu item)
		       (declare (ignore mnu))
		       (setf (entry-method pan)
			 (nth item '(xyz ortho-film table-shift images)))))
    (sl:select-button 0 mode-m) ;; default is individual
    (sl:select-button 0 method-m) ;; default is xyz
    ))

;;;---------------------------------------------

(defmethod (setf entry-mode) :after (newmode (pan brachy-panel))

  (if (entry-subpanel pan) (destroy (entry-subpanel pan)))
  ;; need to change the mode of the source-update-subpanel
  (setf (src-coll (source-update-subpanel pan))
    (if (eql newmode 'line-sources) (line-sources pan) (seeds pan)))
  (setf (line-mode (source-update-subpanel pan)) (eql newmode 'line-sources))
  ;; and put up a new coordinate-entry subpanel
  (setf (entry-subpanel pan)
    (make-coord-entry-panel newmode (entry-method pan)
			    (source-update-subpanel pan)
			    (line-sources pan) (seeds pan)
			    :parent (sl:window (panel-frame pan))
			    :current (current pan)
			    :end-source (end-source pan)))
  (when (entry-subpanel pan)
    (ev:add-notify pan (new-current (entry-subpanel pan))
		   #'(lambda (bpnl subp new-id)
		       (declare (ignore subp))
		       (setf (current bpnl) new-id)))
    (ev:add-notify pan (new-end (entry-subpanel pan))
		   #'(lambda (bpnl subp new-id)
		       (declare (ignore subp))
		       (setf (end-source bpnl) new-id)))))

;;;---------------------------------------------

(defmethod (setf entry-method) :after (newmethod (pan brachy-panel))

  (if (entry-subpanel pan) (destroy (entry-subpanel pan)))
  (setf (entry-subpanel pan)
    (make-coord-entry-panel (entry-mode pan) newmethod
			    (source-update-subpanel pan)
			    (line-sources pan) (seeds pan)
			    :parent (sl:window (panel-frame pan))
			    :current (current pan)
			    :end-source (end-source pan)))
  (when (entry-subpanel pan)
    (ev:add-notify pan (new-current (entry-subpanel pan))
		   #'(lambda (bpnl subp new-id)
		       (declare (ignore subp))
		       (setf (current bpnl) new-id)))
    (ev:add-notify pan (new-end (entry-subpanel pan))
		   #'(lambda (bpnl subp new-id)
		       (declare (ignore subp))
		       (setf (end-source bpnl) new-id)))))

;;;---------------------------------------------

(defmethod destroy :before ((bp brachy-panel))

  "releases X resources used by this panel and its children."

  (sl:destroy (delete-b bp))
  (sl:destroy (entry-mode-label bp))
  (sl:destroy (entry-mode-menu bp))
  (sl:destroy (entry-method-label bp))
  (sl:destroy (entry-method-menu bp))
  (destroy (entry-subpanel bp))
  (destroy (source-update-subpanel bp))
  (destroy (dose-subpanel bp))
  (destroy (seed-specs-subpanel bp))
  (destroy (line-specs-subpanel bp))
  (sl:destroy (panel-frame bp)))

;;;---------------------------------------------
;;; End.
