;;;
;;; dose-result-mediators
;;;
;;; These mediators maintain consistency between the individual dose
;;; results of the plan sources, the source monitor units or strength,
;;; and the plan's summed dose results, both point data and grid data.
;;;
;;; 15-Oct-1993 J. Unger created from design report and earlier prototypes.
;;; 20-Oct-1993 J. Unger add dose-specification-manager.
;;; 25-Oct-1993 I. Kalet change attrib. name dose-result to result
;;; 18-Feb-1994 J. Unger add (call-next-method) to dose-view-mediator 
;;;   destroy method to get the destroy method of the parent class fired.
;;; 16-Mar-1994 J. Unger fix bug in update-dose-result when no sources
;;;   left.
;;;  8-Apr-1994 I. Kalet split off from dose-mediators
;;; 18-Apr-1994 I. Kalet replace new-origin and new-size with new-coords
;;; 22-Apr-1994 J. Unger fixup code that handles dose points
;;;  5-May-1994 J. Unger modify code to handle valid-grid & valid-points
;;;  1-Jun-1994 J. Unger decouple some updating of grid & points.
;;;  1-Jun-1994 J. Unger add code to dose-specification-manager to
;;;   handle invalidation of points when appropriate.
;;; 13-Jun-1994 I. Kalet make destroy a primary method, not :before
;;; 30-Jun-1994 I. Kalet eliminate brachy references for now.
;;;  4-Sep-1994 J. Unger add some add-notifies to point invalidation
;;; 15-Jan-1995 I. Kalet split off dose-view-mediators and
;;;   dose-spec-mediators into separate modules.
;;; 11-Jun-1996 I. Kalet add brachy support, change summarize to sum.
;;; 26-Jun-1997 I. Kalet don't check or make new dose array here -
;;;   handled elsewhere, but do make new point dose list each time.
;;; 22-Jan-1998 BobGian add THE decls to SUM-DOSE-GRID.
;;; 29-Jan-1998 BobGian rewrite SUMMED-DOSE-POINTS for speed (loop rather
;;;   than MAPCARing closure).
;;;  9-Mar-1998 BobGian modest upgrade of dose-calc code.
;;;  3-Feb-2000 BobGian cosmetic fixes (case regularization).
;;;  7-Feb-2000 I. Kalet add missing initial registration of new weight
;;; actions for brachy sources.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass dose-result-manager ()

  ((beams ;; :type coll:collection
    :accessor beams
    :initarg :beams
    :documentation "The collection of managed beams. Provided as an 
initialization argument.")

   (line-sources ;; :type coll:collection
    :accessor line-sources
    :initarg :line-sources
    :documentation "The collection of managed line sources.
Provided as an initialization argument.")

   (seeds ;; :type coll:collection
    :accessor seeds
    :initarg :seeds
    :documentation "The collection of managed seeds.  Provided as an 
initialization argument.")

   (result :type dose-result
	   :accessor result
	   :initarg :result
	   :documentation "The plan's dose result object.  Provided as
an initialization argument.")

   )

  (:documentation "The dose result manager maintains the relationship
between a plan's sources (and those sources' results) and the
plan's result.")

  )

;;;--------------------------------------

(defun weight (src)

  "weight src

returns the weight appropriate to the source type."

  (if (typep src 'beam) (monitor-units src)
    (* (activity src) (treat-time src)))) ;; otherwise brachy

;;;--------------------------------------

(defun sum-dose-grid (sources sum-grid)

  "sum-dose-grid sources sum-grid

computes the weighted sum of the grids of the dose results of SOURCES
and assigns it to SUM-GRID, point by point."

  (declare (type (simple-array single-float 3) sum-grid))
  (let ((xdim (array-dimension sum-grid 0))
	(ydim (array-dimension sum-grid 1))
	(zdim (array-dimension sum-grid 2)))
    (declare (fixnum xdim ydim zdim))
    (dotimes (i xdim)			; set all entries of sum-grid to 0.0
      (declare (fixnum i))
      (dotimes (j ydim)
	(declare (fixnum j))
	(dotimes (k zdim)
	  (declare (fixnum k))
	  (setf (aref sum-grid i j k) 0.0))))
    (dolist (source sources)
      (let ((wght (weight source))
	    (src-grid (grid (result source))))
	(declare (single-float wght)
		 (type (simple-array single-float 3) src-grid))
	(dotimes (i xdim)
	  (declare (fixnum i))
	  (dotimes (j ydim)
	    (declare (fixnum j))
	    (dotimes (k zdim)
	      (declare (fixnum k))
	      (incf (aref sum-grid i j k)
		    (* wght (aref src-grid i j k))))))))))

;;;--------------------------------------

(defun summed-dose-points (sources)

  "summed-dose-points sources

returns a list of numbers, the weighted sums, point by point, of the
doses to points, added up for each point from all the individual
sources."

  ;; add up the doses point by point
  (apply #'mapcar #'+
	 ;; over a list of lists, one list for each beam
	 (mapcar #'(lambda (src)
		     ;; each list has the weighted doses from a source
		     (let ((wght (weight src)))
		       (declare (single-float wght))
		       (mapcar #'(lambda (dose)
				   (declare (single-float dose))
				   (* dose wght))
			       (points (result src)))))
		 sources)))

;;;--------------------------------------

(defun update-sum-grid (drm &rest ignored)

  "update-sum-grid drm &rest ignored

An action function which updates the dose grid of the dose result
manager drm's result, in response to the validity of the grids in the
dose results in drm's collections of sources."

  (declare (ignore ignored))
  (let* ((sources (append (coll:elements (beams drm))
			  (coll:elements (line-sources drm))
			  (coll:elements (seeds drm))))
	 (all-grids-valid (and sources
			       (every #'valid-grid
				      (mapcar #'result sources)))))
    (when all-grids-valid
      (sum-dose-grid sources (grid (result drm))))
    (setf (valid-grid (result drm)) all-grids-valid)))

;;;--------------------------------------

(defun update-sum-points (drm &rest ignored)

  "update-sum-points drm &rest ignored

An action function which updates the dose points of the dose result
manager drm's result, in response to the validity of the points in the
dose results in drm's collections of sources."

  (declare (ignore ignored))
  (let* ((sources (append (coll:elements (beams drm))
			  (coll:elements (line-sources drm))
			  (coll:elements (seeds drm))))
	 (all-points-valid (and sources
				(every #'valid-points
				       (mapcar #'result sources)))))
    (when all-points-valid
      (setf (points (result drm)) (summed-dose-points sources)))
    (setf (valid-points (result drm)) all-points-valid)))

;;;--------------------------------------

(defun update-dose-result (drm &rest ignored)

  "update-dose-result drm &rest ignored

Updates drm's dose-result's grid and points."

  (declare (ignore ignored))
  (update-sum-grid drm)
  (update-sum-points drm))

;;;--------------------------------------

(defmethod initialize-instance :after ((drm dose-result-manager)
				       &rest initargs)
  (declare (ignore initargs))
  ;; 1. register with each existing source's dose result's
  ;; grid-status-changed and update the plan's summary grid in
  ;; response.
  ;; 2. register with each existing source's dose result's
  ;; points-status-changed and update the plan's summary points in
  ;; response.
  ;; 3. register with each beam's new-mu event, and line source or
  ;; seed's new-activity and new-treat-time events to update plan's
  ;; grid and points.
  (dolist (b (coll:elements (beams drm)))
    (ev:add-notify drm (grid-status-changed (result b))
		   #'update-sum-grid) 
    (ev:add-notify drm (points-status-changed (result b))
		   #'update-sum-points)
    (ev:add-notify drm (new-mu b)
		   #'update-dose-result))
  (dolist (ls (coll:elements (line-sources drm)))
    (ev:add-notify drm (grid-status-changed (result ls))
		   #'update-sum-grid)
    (ev:add-notify drm (points-status-changed (result ls))
		   #'update-sum-points)
    (ev:add-notify drm (new-activity ls)
		   #'update-dose-result)
    (ev:add-notify drm (new-treat-time ls)
		   #'update-dose-result))
  (dolist (sd (coll:elements (seeds drm)))
    (ev:add-notify drm (grid-status-changed (result sd))
		   #'update-sum-grid)
    (ev:add-notify drm (points-status-changed (result sd))
		   #'update-sum-points)
    (ev:add-notify drm (new-activity sd)
		   #'update-dose-result)
    (ev:add-notify drm (new-treat-time sd)
		   #'update-dose-result))
  ;; register each new beam with events, also update the plan's dose
  ;; result's grid & points now, since they might have changed.
  (ev:add-notify drm (coll:inserted (beams drm)) 
		 #'(lambda (drm a beam)
		     (ev:add-notify drm (grid-status-changed (result beam))
				    #'update-sum-grid)
		     (ev:add-notify drm (points-status-changed (result beam))
				    #'update-sum-points)
		     (ev:add-notify drm (new-mu beam)
				    #'update-dose-result)
		     (update-dose-result drm a beam)))
  ;; for a deleted beam, unregister events, update dose grid/pts
  (ev:add-notify drm (coll:deleted (beams drm)) 
		 #'(lambda (drm a beam)
		     (ev:remove-notify
		      drm (grid-status-changed (result beam)))
		     (ev:remove-notify
		      drm (points-status-changed (result beam)))
		     (ev:remove-notify drm (new-mu beam))
		     (update-dose-result drm a beam)))
  ;; ditto for new line sources and seeds...
  (ev:add-notify drm (coll:inserted (line-sources drm)) 
		 #'(lambda (drm a ls)
		     (ev:add-notify drm (grid-status-changed (result ls))
				    #'update-sum-grid)
		     (ev:add-notify drm (points-status-changed (result ls))
				    #'update-sum-points)
		     (ev:add-notify drm (new-activity ls)
				    #'update-dose-result)
		     (ev:add-notify drm (new-treat-time ls)
				    #'update-dose-result)
		     (update-dose-result drm a ls)))
  (ev:add-notify drm (coll:inserted (seeds drm)) 
		 #'(lambda (drm a sd)
		     (ev:add-notify drm (grid-status-changed (result sd))
				    #'update-sum-grid)
		     (ev:add-notify drm (points-status-changed (result sd))
				    #'update-sum-points)
		     (ev:add-notify drm (new-activity sd)
				    #'update-dose-result)
		     (ev:add-notify drm (new-treat-time sd)
				    #'update-dose-result)
		     (update-dose-result drm a sd)))
  ;; ditto for deleted line sources and seeds...
  (ev:add-notify drm (coll:deleted (line-sources drm)) 
		 #'(lambda (drm a ls)
		     (ev:remove-notify drm (grid-status-changed
					    (result ls)))
		     (ev:remove-notify drm (points-status-changed
					    (result ls)))
		     (ev:remove-notify drm (new-activity ls))
		     (ev:remove-notify drm (new-treat-time ls))
		     (update-dose-result drm a ls)))
  (ev:add-notify drm (coll:deleted (seeds drm)) 
		 #'(lambda (drm a sd)
		     (ev:remove-notify drm (grid-status-changed
					    (result sd)))
		     (ev:remove-notify drm (points-status-changed
					    (result sd)))
		     (ev:remove-notify drm (new-activity sd))
		     (ev:remove-notify drm (new-treat-time sd))
		     (update-dose-result drm a sd))))

;;;--------------------------------------

(defmethod destroy ((drm dose-result-manager))

  ;; unregister the beams and beam set...
  (dolist (beam (coll:elements (beams drm)))
    (ev:remove-notify drm (grid-status-changed (result beam)))
    (ev:remove-notify drm (points-status-changed (result beam)))
    (ev:remove-notify drm (new-mu beam)))
  (ev:remove-notify drm (coll:inserted (beams drm)))
  (ev:remove-notify drm (coll:deleted (beams drm)))
  ;; ditto for line sources...
  (dolist (ls (coll:elements (line-sources drm)))
    (ev:remove-notify drm (grid-status-changed (result ls)))
    (ev:remove-notify drm (points-status-changed (result ls)))
    (ev:remove-notify drm (new-activity ls))
    (ev:remove-notify drm (new-treat-time ls)))
  (ev:remove-notify drm (coll:inserted (line-sources drm)))
  (ev:remove-notify drm (coll:deleted (line-sources drm)))
  ;; and seeds
  (dolist (sd (coll:elements (seeds drm)))
    (ev:remove-notify drm (grid-status-changed (result sd)))
    (ev:remove-notify drm (points-status-changed (result sd)))
    (ev:remove-notify drm (new-activity sd))
    (ev:remove-notify drm (new-treat-time sd)))
  (ev:remove-notify drm (coll:inserted (seeds drm)))
  (ev:remove-notify drm (coll:deleted (seeds drm))))

;;;--------------------------------------

(defun make-dose-result-manager (&rest initargs)

  "make-dose-result-manager &rest initargs

Creates and returns a dose result manager from the supplied
keyword initialization arguments."

  (apply #'make-instance 'dose-result-manager initargs))

;;;--------------------------------------
;;; End.
