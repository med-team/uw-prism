;;;
;;;  patdb-panels
;;;
;;;  The Prism patient database management panel
;;;
;;; 29-Jun-1997 I. Kalet created, from dbmgr
;;; 14-Aug-1997 I. Kalet for case and plan deletion from checkpoint
;;; directory, generate patient list from case.index there, not
;;; patient.index in archive.
;;; 25-Aug-1997 I. Kalet add capability to delete cases from irreg
;;; database as well as archive and checkpoint.
;;;  9-Nov-1997 I. Kalet always use *patient-database* with
;;;  get-patient-entry because that is where patient.index is.  Use
;;;  new optional parameter to select-case to suppress NEW CASE in
;;;  delete operations.
;;; 28-Dec-1997 I. Kalet add delete patient button for easier cleanup
;;; of checkpoint directory, move duplicated code to new function
;;; select-patient-from-case-list and put in prism-db module.
;;; 31-Dec-2001 I. Kalet allow selection of multiple cases to delete
;;; in delete-old-case, and use match string for patient name and
;;; number for checkpoint as well as archive.
;;; 31-Oct-2003 I. Kalet allow selection of multiple image studies for
;;; deletion in delete-old-image, also multiple patients in
;;; delete-old-patient
;;;  2-Jul-2004 I. Kalet allow selection of shared db for delete
;;; operations as well as local checkpt and archive.  Remove IRREG
;;; support.
;;;

(in-package :prism)

;;;---------------------------------------

(defclass patdb-panel (generic-panel)

  ((frame :accessor frame
	  :documentation "The panel frame")

   (pat-id :accessor pat-id
	   :initarg :pat-id
	   :documentation "The patient ID of the currently selected
patient in this panel.")

   (pat-name :accessor pat-name
	     :initarg :pat-name
	     :documentation "The string patient name of the currently
selected patient in this panel.")

   (hosp-id :accessor hosp-id
	    :initarg :hosp-id
	    :documentation "The hospital ID of the patient currently
selected in this panel.")

   (new-flag :accessor new-flag
	     :initform nil
	     :documentation "This flag is set when the user presses
the Add button to get the next Prism ID number for adding a new
patient entry.")

   (database :accessor database
	     :initarg :database
	     :documentation "The database to use for add, update or
delete operations.")

   (delete-panel-btn :accessor delete-panel-btn
		     :documentation "The Delete Panel button.")

   (add-pat-btn :accessor add-pat-btn
		:documentation "The button for adding a new patient
entry.")

   (prism-num-rdt :accessor prism-num-rdt
		  :documentation "The readout displaying the Prism
assigned ID number of the current patient.  It is not editable.")

   (name-tln :accessor name-tln
	     :documentation "The textline showing the patient name.")

   (hosp-id-tln :accessor hosp-id-tln
		:documentation "The textline showing the patient
hospital ID.")

   (select-pat-btn :accessor select-pat-btn
		   :documentation "The button for selecting the
patient for updating the basic patient info.")

   (update-btn :accessor update-btn
	       :documentation "The button to press to update the
patient list with new information.")

   (db-select-btn :accessor db-select-btn
		  :documentation "The button to select either the
archive, IRREG, or checkpoint database for delete operations.")

   (delete-case-btn :accessor delete-case-btn
		    :documentation "The button to press to select a
single case of a specific patient, for deletion.")

   (delete-plan-btn :accessor delete-plan-btn
		    :documentation "The button to press to select a
single plan of a specific case of a specific patient, for deletion.")

   (delete-pat-btn :accessor delete-pat-btn
		   :documentation "The button to press to select a
patient from the checkpoint directory, for deletion of all that
patient's cases in the user's checkpoint directory.")

   (delete-img-stdy-btn :accessor delete-img-stdy-btn
			:documentation "The button to press to select
an image study for deletion, not necessarily associated with the
current patient.")

   )

  (:default-initargs :pat-id 0 :pat-name "" :hosp-id ""
		     :database *patient-database*)

  (:documentation "The patdb-panel provides the functions for adding a
new patient to the patient list, editing the patient name or hospital
id if it was entered wrong earlier, and for deleting cases and plans
and image studies that are no longer needed, from either the archive
or the checkpoint database.")

  )

;;;---------------------------------------

(defun update-db-panel (pan)

  "UPDATE-DB-PANEL pan

puts the current patient information into the textlines and readout."

  (setf (sl:info (prism-num-rdt pan)) (pat-id pan)
	(sl:info (name-tln pan)) (pat-name pan)
	(sl:info (hosp-id-tln pan)) (hosp-id pan)
	(sl:border-color (name-tln pan)) 'sl:white
	(sl:border-color (hosp-id-tln pan)) 'sl:white
	(sl:border-width (name-tln pan)) 1
	(sl:border-width (hosp-id-tln pan)) 1))

;;;---------------------------------------

(defmethod initialize-instance :after ((db-panel patdb-panel)
				       &rest initargs)

  (let* ((btw 150)
	 (bth 30)
	 (dx 5) ;; position of left side buttons etc.
	 (dx2 (+ (* 2 dx) btw)) ;; position of middle buttons
	 (dx3 (+ dx2 btw dx)) ;; position of right side buttons etc.
	 (top-y 5)
	 (delta 20) ;; space between patient list stuff and deletion stuff
         (frm (apply #'sl:make-frame
		     (+ dx (* 3 (+ btw dx)))
		     (+ top-y (* 5 (+ bth top-y)) delta)
		     :title "Prism Patient Database Manager"
		     initargs))
         (frm-win (sl:window frm))
	 (del-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y top-y
		       :label "Del. Panel"
		       :parent frm-win
		       :button-type :momentary
		       initargs))
         (add-b (apply #'sl:make-button btw bth
		       :ulc-x dx2 :ulc-y top-y
		       :label "Next Prism ID"
		       :parent frm-win
		       :button-type :momentary
		       initargs))
         (id-r (apply #'sl:make-readout btw bth
		      :ulc-x dx3 :ulc-y top-y
		      :label "PID: "
		      :parent frm-win
		      initargs))
         (name-t (apply #'sl:make-textline (+ (* 2 btw) dx) bth
			:ulc-x dx :ulc-y (bp-y top-y bth 1)
			:parent frm-win
			initargs))
         (hosp-t (apply #'sl:make-textline btw bth
			:ulc-x dx3 :ulc-y (bp-y top-y bth 1)
			:parent frm-win
			initargs))
         (sel-pat-b (apply #'sl:make-button btw bth
			   :ulc-x dx :ulc-y (bp-y top-y bth 2)
			   :label "Select Patient"
			   :parent frm-win
			   :button-type :momentary
			   initargs))
         (update-b (apply #'sl:make-button btw bth
			  :ulc-x dx3 :ulc-y (bp-y top-y bth 2)
			  :label "Update patient"
			  :parent frm-win
			  initargs))
         (db-sel-b (apply #'sl:make-button btw bth
			  :ulc-x dx
			  :ulc-y (+ (bp-y top-y bth 3) delta)
			  :label "DB: Archive"
			  :parent frm-win
			  :button-type :momentary
			  initargs))
         (del-case-b (apply #'sl:make-button btw bth
			    :ulc-x dx2
			    :ulc-y (+ (bp-y top-y bth 3) delta)
			    :label "Delete case"
			    :parent frm-win
			    :button-type :momentary
			    initargs))
         (del-plan-b (apply #'sl:make-button btw bth
			    :ulc-x dx3
			    :ulc-y (+ (bp-y top-y bth 3) delta)
			    :label "Delete plan"
			    :parent frm-win
			    :button-type :momentary
			    initargs))
         (del-pat-b (apply #'sl:make-button btw bth
			   :ulc-x (+ dx (/ btw 2))
			   :ulc-y (+ (bp-y top-y bth 4) delta)
			   :label "Delete patient"
			   :parent frm-win
			   :button-type :momentary
			   initargs))
	 (del-img-stdy-b (apply #'sl:make-button btw bth
				:ulc-x (+ dx2 (/ btw 2))
				:ulc-y (+ (bp-y top-y bth 4) delta)
				:label "Delete images"
				:parent frm-win
				:button-type :momentary
                                initargs)))
    (setf (frame db-panel) frm
	  (delete-panel-btn db-panel) del-b
	  (add-pat-btn db-panel) add-b
	  (prism-num-rdt db-panel) id-r
	  (name-tln db-panel) name-t
	  (hosp-id-tln db-panel) hosp-t
	  (select-pat-btn db-panel) sel-pat-b
	  (update-btn db-panel) update-b
	  (db-select-btn db-panel) db-sel-b
	  (delete-case-btn db-panel) del-case-b
	  (delete-plan-btn db-panel) del-plan-b
	  (delete-pat-btn db-panel) del-pat-b
	  (delete-img-stdy-btn db-panel) del-img-stdy-b)
    (update-db-panel db-panel) ;; initializes the contents of the display
    (ev:add-notify db-panel (sl:button-on del-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (destroy pan)))
    (ev:add-notify db-panel (sl:button-on add-b)
		   #'(lambda (pan bt)
		       (let* ((pat-list ;; always from/to archive
			       (get-patient-list *patient-database*))
			      (next-num
			       (if pat-list 
				   (1+ (apply #'max
					      (mapcar #'first pat-list)))
				 (sl:acknowledge
				  "Patient index is inaccessible"))))
			 (if next-num
			     (progn
			       (setf (new-flag pan) t)
			       (setf (pat-id pan) next-num
				     (pat-name pan) ""
				     (hosp-id pan) "")
			       (update-db-panel pan))
			   (setf (sl:on bt) nil)))))
    (ev:add-notify db-panel (sl:new-info name-t)
		   #'(lambda (pan tln info)
		       (declare (ignore tln))
		       (setf (sl:on (update-btn pan)) t)
		       (setf (pat-name pan) info)))
    (ev:add-notify db-panel (sl:new-info hosp-t)
		   #'(lambda (pan tln info)
		       (declare (ignore tln))
		       (setf (sl:on (update-btn pan)) t)
		       (setf (hosp-id pan) info)))
    (ev:add-notify db-panel (sl:button-on sel-pat-b)
		   #'(lambda (pan bt)
		       (let* ((id (select-patient
				   *patient-database*
				   (or (sl:popup-textline
					"" 300
					:label "Match with: "
					:title "Patient search string")
				       "")))
			      (pat-rec (if id (get-patient-entry
					       id *patient-database*))))
			 (when pat-rec
			   (setf (new-flag pan) nil)
			   (setf (pat-id pan) (first pat-rec)
				 (pat-name pan) (second pat-rec)
				 (hosp-id pan) (third pat-rec))
			   (update-db-panel pan))
			 (setf (sl:on bt) nil))))
    (ev:add-notify db-panel (sl:button-off update-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt)) ;; always use archive
		       (if (new-flag pan)
			   (progn (setf (new-flag pan) nil)
				  (add-patient (pat-id pan)
					       (pat-name pan)
					       (hosp-id pan)
					       *patient-database*))
			 (edit-patient (pat-id pan)
				       (pat-name pan)
				       (hosp-id pan)
				       *patient-database*))))
    (ev:add-notify db-panel (sl:button-on db-sel-b) ;; menu of dbs
		   #'(lambda (pan bt)
		       (let ((dbsel (sl:popup-menu '("Archive"
						     "Checkpoint"
						     "Shared temp"))))
			 (if dbsel
			     (case dbsel
			       (0 (progn (setf (database pan)
					   *patient-database*)
					 (setf (sl:label bt)
					   "DB: Archive")))
			       (1 (progn (setf (database pan)
					   *local-database*)
					 (setf (sl:label bt)
					   "DB: Local")))
			       (2 (progn (setf (database pan)
					   *shared-database*)
					 (setf (sl:label bt)
					   "DB: Shared"))))))
		       (setf (sl:on bt) nil)))
    (ev:add-notify db-panel (sl:button-on del-case-b)
		   #'(lambda (pan bt)
		       (delete-old-case (database pan))
		       (setf (sl:on bt) nil)))
    (ev:add-notify db-panel (sl:button-on del-plan-b)
		   #'(lambda (pan bt)
		       (delete-old-plan (database pan))
		       (setf (sl:on bt) nil)))
    (ev:add-notify db-panel (sl:button-on del-pat-b)
		   #'(lambda (pan bt)
		       (if (eql (database pan) *patient-database*)
			   (sl:acknowledge '("Cannot delete patients"
					     "from Archives"))
			 (delete-old-patient (database pan)))
		       (setf (sl:on bt) nil)))
    (ev:add-notify db-panel (sl:button-on del-img-stdy-b)
		   #'(lambda (pan bt)
		       (declare (ignore pan))
		       (delete-old-image-study *image-database*)
		       (setf (sl:on bt) nil)))))

;;;---------------------------------------

(defun make-patdb-panel (&rest initargs)

  "MAKE-PATDB-PANEL &rest initargs

returns an instance of a patdb-panel with the specified initargs."

  (apply #'make-instance 'patdb-panel initargs))

;;;---------------------------------------

(defmethod destroy :before ((dbpan patdb-panel))

  (sl:destroy (delete-panel-btn dbpan))
  (sl:destroy (add-pat-btn dbpan))
  (sl:destroy (prism-num-rdt dbpan))
  (sl:destroy (name-tln dbpan))
  (sl:destroy (hosp-id-tln dbpan))
  (sl:destroy (select-pat-btn dbpan))
  (sl:destroy (update-btn dbpan))
  (sl:destroy (db-select-btn dbpan))
  (sl:destroy (delete-case-btn dbpan))
  (sl:destroy (delete-plan-btn dbpan))
  (sl:destroy (delete-pat-btn dbpan))
  (sl:destroy (delete-img-stdy-btn dbpan))
  (sl:destroy (frame dbpan)))

;;;---------------------------------------

(defun delete-old-case (db)

  (let* ((match-string (or (sl:popup-textline
			    "" 300
			    :label "Match with: "
			    :title "Patient search string")
			   ""))
	 (pat-num (if (equal db *patient-database*)
		      (select-patient *patient-database* match-string)
		    (select-patient-from-case-list *patient-database*
						   db match-string)))
	 (case-nums (when (and pat-num (not (zerop pat-num)))
		      (select-cases pat-num db))))
    (dolist (case-num case-nums)
      (let ((case-entry (find case-num (get-case-list pat-num db)
			      :key #'first)))
	(when (sl:confirm
	       (list "Are you SURE you want to delete"
		     ""
		     (format nil "Case: ~a" (second case-entry))
		     (format nil "Date: ~a" (third case-entry))
		     (format nil "Database: ~a" db)))
	  (unless (delete-case pat-num case-num db)
	    (sl:acknowledge (list "Can't delete"
				  (format nil "patient ~a case ~a"
					  pat-num case-num)
				  "from case list")))
	  (unless (delete-case-file pat-num case-num db)
	    (sl:acknowledge (list "Can't find data file for"
				  (format nil "patient ~a case ~a"
					  pat-num case-num)))))))))

;;;---------------------------------------

(defun delete-old-plan (db)

  (let* ((match-string (or (sl:popup-textline
			    "" 300
			    :label "Match with: "
			    :title "Patient search string")
			   ""))
	 (pat-num (if (equal db *patient-database*)
		      (select-patient *patient-database* match-string)
		    (select-patient-from-case-list *patient-database*
						   db match-string)))
	 (case-num (when (and pat-num (not (zerop pat-num)))
		     (select-case pat-num db nil)))
         (case-data (when case-num
		      (get-case-data pat-num case-num db)))
         (plans (when case-data (coll:elements (plans case-data))))
         (plan-num (when plans (sl:popup-scroll-menu 
				(mapcar 
				 #'(lambda (pln) 
				     (format nil "~30a ~20a ~20a" 
					     (name pln) (plan-by pln)
					     (time-stamp pln)))
				 plans)
				600 300
				:title "Select a plan to DELETE")))
         (plan (when plan-num (nth plan-num plans))))
    (when (and plan-num
	       (sl:confirm
		(list "Are you SURE you want to delete"
		      ""
		      (format nil "Plan: ~a" (name plan))
		      (format nil "By: ~a" (plan-by plan))
		      (format nil "Date: ~a" (time-stamp plan))
		      (format nil "Database: ~a" db))))
      (unless (delete-plan-from-case pat-num case-num plan db)
        (sl:acknowledge 
	 (format nil "Can't delete patient ~a case ~a plan name ~a"
		 pat-num case-num (name plan)))))))

;;;---------------------------------------

(defun delete-old-patient (db)

  (let ((patnums (select-patients-from-case-list *patient-database*
						db)))
    (dolist (patnum patnums)
      (when (and patnum
		 (sl:confirm (list "Are you SURE you want to delete"
				   (format nil "Patient: ~a" patnum)
				   (format nil "from database ~a?" db))))
	(mapcar #'(lambda (casenum)
		    (delete-case-file patnum casenum db)
		    (delete-case patnum casenum db))
		(mapcar #'first (get-case-list patnum db)))))))

;;;---------------------------------------

(defun delete-old-image-study (db)

  (let ((img-entries (select-full-image-sets
		      db
		      :title "Select image studies to DELETE:")))
    (dolist (img-entry img-entries)
      (let* ((pat-id (first img-entry))
	     (img-id (second img-entry))
	     (pat-name (second (get-patient-entry
				pat-id *patient-database*))))
	(when (and img-entry
		   (sl:confirm
		    (list "Are you SURE you want to delete this image study?"
			  ""
			  (format nil "~5@A ~A ~4@A ~50A"
				  pat-id pat-name img-id (third img-entry)))))
	  (unless (delete-image-set pat-id img-id db)
	    (sl:acknowledge
	     (format nil "Can't delete image study ~a." img-id)))
	  (unless (delete-image-files pat-id img-id db)
	    (sl:acknowledge 
	     (format nil
		     "Can't find data files for patient ~a image study ~a"
		     pat-id img-id))))))))

;;;---------------------------------------
;;; End.
