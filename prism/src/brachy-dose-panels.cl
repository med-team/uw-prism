;;;
;;; brachy-dose-panels (formerly seed-spreadsheet)
;;;
;;; Definitions of mini-spreadsheet for brachy dose display and for
;;; setting uniform time and activities where applicable
;;;
;;; 17-Apr-2000 I. Kalet created.
;;; 23-Apr-2000 I. Kalet refinements to basic design - add more
;;; columns and add scroll arrows.
;;; 11-May-2000 I. Kalet parametrize application time and activity
;;; upper and lower limits.
;;;  1-Apr-2002 I. Kalet take out textlines for setting time and
;;; activity, not useful since rarely uniform.  Other fixes missing in
;;; previous version.  Take out delete button, other mods to put
;;; directly on brachy panel instead of separate window.
;;;  5-May-2002 I. Kalet adapt for possibility of button-off events
;;; 28-Jul-2002 I. Kalet don't make seed spreadsheet a subclass of
;;; generic-panel, it is not needed, but then make destroy method primary
;;; 12-Aug-2002 I. Kalet initialize "set time" button, make
;;; renormalization updates more efficient with "hold" flag (not yet
;;; working) and add registrations for points added and deleted.
;;; 13-Oct-2002 I. Kalet add line source doses and rename file
;;; 29-Dec-2002 I. Kalet add remove-notify for points insertion,
;;; deletion and name change when panel is destroyed.
;;;  2-Nov-2003 I. Kalet remove use of #. reader macro from
;;; *brachy-dose-cells* to allow compile without loading first
;;;  1-Dec-2003 I. Kalet use backquote in array initialization instead
;;; of quote, to allow eval of parameters.
;;; 20-Jul-2004 I. Kalet put in Balto's fix to brachy-dose-refresh, to
;;; check for non-nil allsrcs
;;; 31-Jan-2005 A. Simms add :allow-other-keys t to make-brachy-dose-panel
;;;

;;; *** still to do: finish efficiency hacks for update operations

(in-package :prism)

;;;---------------------------------------------

(defparameter *brachy-dose-min* 0.1)
(defparameter *brachy-dose-max* 20000.0)

;;;---------------------------------------------

(defvar *brachy-dose-row-heights* (make-list 12 :initial-element 25))

;;;---------------------------------------------

(defvar *brachy-dose-col-widths* '(40 100 60 60))

;;;---------------------------------------------

(defvar *brachy-dose-cells*
    (make-array '(12 4)
		:initial-contents
		`((nil
		   (:button "Compute Dose" nil nil :button-type :momentary)
		   (:button "Act.")
		   (:button "Time"))
		  (nil (:label "Point name") (:label "Dose rate")
		    (:label "Total dose"))
		  ((:up-arrow nil nil nil :fg-color sl:red)
		   (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  (nil (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*))
		  ((:down-arrow nil nil nil :fg-color sl:red)
		   (:readout "")
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)
		   (:number nil ,*brachy-dose-min* ,*brachy-dose-max*)))))

;;;---------------------------------------------

(defclass brachy-dose-panel ()

  ((fr :accessor fr
       :documentation "The SLIK spreadsheet panel that contains
all the control buttons, name cells, data cells and arrow buttons.")

   (seeds :accessor seeds
	  :initarg :seeds
	  :documentation "The seed collection for this brachy-dose
panel.")

   (line-sources :accessor line-sources
		 :initarg :line-sources
		 :documentation "The line-source collection for this
brachy-dose panel.")

   (pointlist :accessor pointlist
	      :initarg :pointlist
	      :documentation "The collection of points at which to
calculate the dose.")

   (compute-time :accessor compute-time
		 :initform t
		 :documentation "t if need to compute time from dose
and activity, and nil if compute activity from dose and time.")

   (point-pos :type fixnum
	      :accessor point-pos
	      :initform 0
	      :documentation "The position in the point list of the
point in the first data row of the seed dose panel spreadsheet.")

   (hold-updates :accessor hold-updates
		 :initform nil
		 :documentation "When this flag is set, the panel does
not update with every seed app. time or strength update, so that we
can avoid redundant updates which would be really slow.")

   )

  )

;;;---------------------------------------------

(defun make-brachy-dose-panel (&rest initargs)

  "make-brachy-dose-panel &rest initargs

Creates and returns a brachy-dose panel with the specified initargs."

  (apply #'make-instance 'brachy-dose-panel
	 :font (symbol-value *small-font*)
	 :allow-other-keys t
	 initargs))


;;;---------------------------------------------

(defun source-dose-rates (src)
  (if (valid-points (result src))
      (let ((act (activity src)))
	(mapcar #'(lambda (x) (* act x))
		(points (result src))))))

;;;---------------------------------------------

(defun source-doses (src)
  (let ((time (treat-time src)))
    (mapcar #'(lambda (x) (* time x))
	    (source-dose-rates src))))

;;;---------------------------------------------

(defmethod initialize-instance :after ((sdp brachy-dose-panel)
				       &rest initargs)

  "Initializes the user interface for the brachy-dose panel."

  (let ((sheet (apply #'sl:make-spreadsheet
		      *brachy-dose-row-heights* *brachy-dose-col-widths*
		      *brachy-dose-cells*
		      :title "Prism Brachy Dose Panel"
		      initargs)))
    (setf (fr sdp) sheet)
    (sl:set-button sheet 0 3 t)
    (brachy-dose-refresh sdp)
    ;; display point totals in rads per hour numbered 1 to n
    ;; accept point total desired for any point, scale hours or
    ;; activity, display totals
    (ev:add-notify sdp (sl:user-input sheet)
		   #'(lambda (pan sp i j info)
		       (let* ((seeds (coll:elements (seeds pan)))
			      (lines (coll:elements (line-sources pan)))
			      (allsrcs (append seeds lines))
			      (pts (coll:elements (pointlist pan)))
			      (lastrow (min (+ 1 (- (length pts)
						    (point-pos pan)))
					    11)))
			 (cond ((and (= i 2) (= j 0)) ;; up arrow
				(brachy-dose-scroll pan (case info
							  (1 -1)
							  (2 -10))))
			       ((and (= i 11) (= j 0)) ;; down arrow
				(brachy-dose-scroll pan (case info
							  (1 1)
							  (2 10))))
			       ((and (= i 0) (= j 1))
				(when (= info 1)
				  ;; compute point doses
				  (dolist (src seeds)
				    (let ((result (result src)))
				      (unless (valid-points result)
					(setf (valid-points result)
					  (compute-seed-dose src pts nil)))))
				  (dolist (src lines)
				    (let ((result (result src)))
				      (unless (valid-points result)
					(setf (valid-points result)
					  (compute-line-dose src pts nil)))))
				  (brachy-dose-refresh pan)))
			       ((and (= i 0) (= j 2))
				(when (= info 1)
				  (setf (compute-time pan) nil)
				  (sl:set-button sp i 3 nil)))
			       ((and (= i 0) (= j 3))
				(when (= info 1)
				  (setf (compute-time pan) t)
				  (sl:set-button sp i 2 nil)))
			       ;; new point dose rate - renormalize activity
			       ((and (> i 1) (<= i lastrow) (= j 2))
				(if (every #'(lambda (x)
					       (valid-points (result x)))
					   allsrcs)
				    (let* ((pt-rates
					    (apply #'mapcar #'+
						   (mapcar #'source-dose-rates
							   allsrcs)))
					   (old-rate
					    (nth (+ i (point-pos pan) -2)
						 pt-rates))
					   (ratio (coerce (/ info old-rate)
							  'single-float)))
				      ;; (setf (hold-updates pan) t)
				      (dolist (src allsrcs)
					(setf (activity src)
					  (* ratio (activity src))))
				      ;; (setf (hold-updates pan) nil)
				      ;; (brachy-dose-refresh pan)
				      )
				  (progn
				    (sl:acknowledge
				     '("        No results!"
				       "Compute raw dose rates first"))
				    (sl:erase-contents sp i j)))
				(brachy-dose-refresh pan))
			       ;; new point total dose - renormalize
			       ;; either time or activity
			       ((and (> i 1) (<= i lastrow) (= j 3))
				(if (every #'(lambda (x)
					       (valid-points (result x)))
					   allsrcs)
				    (let* ((pt-doses
					    (apply #'mapcar #'+
						   (mapcar #'source-doses
							   allsrcs)))
					   (old-dose
					    (nth (+ i (point-pos pan) -2)
						 pt-doses))
					   (ratio (coerce (/ info old-dose)
							  'single-float)))
				      ;; (setf (hold-updates pan) t)
				      (if (compute-time pan)
					  (dolist (src allsrcs)
					    (setf (treat-time src)
					      (* ratio (treat-time src))))
					(dolist (src allsrcs)
					  (setf (activity src)
					    (* ratio (activity src)))))
				      ;; (setf (hold-updates pan) nil)
				      ;; (brachy-dose-refresh pan)
				      )
				  (progn
				    (sl:acknowledge
				     '("        No results!"
				       "Compute raw dose rates first"))
				    (sl:erase-contents sp i j)))
				(brachy-dose-refresh pan))
			       ;; could come here, user entered a number
			       ;; in an empty textline
			       (t (sl:acknowledge "That cell is empty")
				  (sl:erase-contents sp i j))))))
    ;; need to register with changes in source activities and
    ;; times from elsewhere
    (dolist (source (coll:elements (seeds sdp)))
      (ev:add-notify sdp (new-activity source)
		     #'(lambda (pan src act)
			 (declare (ignore src act))
			 (brachy-dose-refresh pan)))
      (ev:add-notify sdp (new-treat-time source)
		     #'(lambda (pan src time)
			 (declare (ignore src time))
			 (brachy-dose-refresh pan)))
      (ev:add-notify sdp (new-source-type source)
		     #'(lambda (pan src time)
			 (declare (ignore time))
			 (setf (valid-points (result src))
			   (compute-seed-dose src
					      (coll:elements (pointlist pan))
					      nil))
			 (brachy-dose-refresh pan))))
    (dolist (source (coll:elements (line-sources sdp)))
      (ev:add-notify sdp (new-activity source)
		     #'(lambda (pan src act)
			 (declare (ignore src act))
			 (brachy-dose-refresh pan)))
      (ev:add-notify sdp (new-treat-time source)
		     #'(lambda (pan src time)
			 (declare (ignore src time))
			 (brachy-dose-refresh pan)))
      (ev:add-notify sdp (new-source-type source)
		     #'(lambda (pan src time)
			 (declare (ignore time))
			 (setf (valid-points (result src))
			   (compute-line-dose src
					      (coll:elements (pointlist pan))
					      nil))
			 (brachy-dose-refresh pan))))
    (ev:add-notify sdp (coll:inserted (seeds sdp))
		   #'(lambda (pan coll newsrc)
		       (declare (ignore coll))
		       (ev:add-notify pan (new-activity newsrc)
				      #'(lambda (pnl src act)
					  (declare (ignore src act))
					  (brachy-dose-refresh pnl)))
		       (ev:add-notify pan (new-treat-time newsrc)
				      #'(lambda (pnl src time)
					  (declare (ignore src time))
					  (brachy-dose-refresh pnl)))
		       (ev:add-notify sdp (new-source-type newsrc)
				      #'(lambda (pnl src time)
					  (declare (ignore time))
					  (setf (valid-points (result src))
					    (compute-seed-dose
					     src
					     (coll:elements (pointlist pnl))
					     nil))
					  (brachy-dose-refresh pnl)))
		       (setf (valid-points (result newsrc))
			 (compute-seed-dose newsrc
					    (coll:elements (pointlist pan))
					    nil))
		       (brachy-dose-refresh pan)))
    (ev:add-notify sdp (coll:deleted (seeds sdp))
		   #'(lambda (pan coll src)
		       (declare (ignore coll src))
		       (brachy-dose-refresh pan)))
    (ev:add-notify sdp (coll:inserted (line-sources sdp))
		   #'(lambda (pan coll newsrc)
		       (declare (ignore coll))
		       (ev:add-notify pan (new-activity newsrc)
				      #'(lambda (pnl src act)
					  (declare (ignore src act))
					  (brachy-dose-refresh pnl)))
		       (ev:add-notify pan (new-treat-time newsrc)
				      #'(lambda (pnl src time)
					  (declare (ignore src time))
					  (brachy-dose-refresh pnl)))
		       (ev:add-notify sdp (new-source-type newsrc)
				      #'(lambda (pnl src time)
					  (declare (ignore time))
					  (setf (valid-points (result src))
					    (compute-line-dose
					     src
					     (coll:elements (pointlist pnl))
					     nil))
					  (brachy-dose-refresh pnl)))
		       (setf (valid-points (result newsrc))
			 (compute-line-dose newsrc
					    (coll:elements (pointlist pan))
					    nil))
		       (brachy-dose-refresh pan)))
    (ev:add-notify sdp (coll:deleted (line-sources sdp))
		   #'(lambda (pan coll src)
		       (declare (ignore coll src))
		       (brachy-dose-refresh pan)))
    ;; register for points added and deleted, and for point name change
    (dolist (pt (coll:elements (pointlist sdp)))
      (ev:add-notify sdp (new-name pt)
		     #'(lambda (pan pnt newname)
			 (declare (ignore pnt newname))
			 (brachy-dose-refresh pan))))
    (ev:add-notify sdp (coll:inserted (pointlist sdp))
		   #'(lambda (pan coll pt)
		       (declare (ignore coll))
		       (ev:add-notify pan (new-name pt)
				      #'(lambda (pnl pnt newname)
					  (declare (ignore pnt newname))
					  (brachy-dose-refresh pnl)))
		       (brachy-dose-refresh pan)))
    (ev:add-notify sdp (coll:deleted (pointlist sdp))
		   #'(lambda (pan coll pt)
		       (declare (ignore coll pt))
		       (brachy-dose-refresh pan)))
    ))

;;;---------------------------------------------

(defun brachy-dose-refresh (seedpan)

  (let* ((sp (fr seedpan))
	 (points (coll:elements (pointlist seedpan)))
	 (allsrcs (append (coll:elements (seeds seedpan))
			  (coll:elements (line-sources seedpan))))
	 (pt-dose-rates (if allsrcs ;; to insure 2 args to mapcar
			    (apply #'mapcar #'+
				   (mapcar #'source-dose-rates allsrcs))))
	 (pt-doses (if allsrcs ;; to insure 2 args to mapcar
		       (apply #'mapcar #'+
			      (mapcar #'source-doses allsrcs))))
	 (pt-pos (point-pos seedpan)))
    (dotimes (n 10)
      (if (< (+ n pt-pos) (length points))
	  (let* ((i (+ n pt-pos))
		 (dose-rate (nth i pt-dose-rates))
		 (dose (nth i pt-doses))
		 (pt-name (format nil "~2A ~14A"
				  (id (nth i points))
				  (name (nth i points)))))
	    (sl:set-contents sp (+ n 2) 1 pt-name)
	    (when dose-rate ;; could be nil if not yet computed
	      (sl:set-contents sp (+ n 2) 2 (format nil "~6,1F" dose-rate))
	      (sl:set-contents sp (+ n 2) 3 (format nil "~6,1F" dose))))
	(dotimes (i 3)
	  (sl:set-contents sp (+ n 2) (1+ i) ""))))))

;;;---------------------------------------------

(defun brachy-dose-scroll (panel amt)

  (when amt ;; could be nil - see case forms above
    (let ((tmp (+ (point-pos panel) amt))
	  (ptlist (coll:elements (pointlist panel))))
      (when (and (>= tmp 0) (< tmp (length ptlist)))
	(setf (point-pos panel) tmp)
	(brachy-dose-refresh panel)))))

;;;---------------------------------------------

(defmethod destroy ((sdp brachy-dose-panel))

  (dolist (source (coll:elements (seeds sdp)))
    (ev:remove-notify sdp (new-activity source))
    (ev:remove-notify sdp (new-treat-time source))
    (ev:remove-notify sdp (new-source-type source)))
  (dolist (source (coll:elements (line-sources sdp)))
    (ev:remove-notify sdp (new-activity source))
    (ev:remove-notify sdp (new-treat-time source))
    (ev:remove-notify sdp (new-source-type source)))
  (ev:remove-notify sdp (coll:inserted (seeds sdp)))
  (ev:remove-notify sdp (coll:deleted (seeds sdp)))
  (ev:remove-notify sdp (coll:inserted (line-sources sdp)))
  (ev:remove-notify sdp (coll:deleted (line-sources sdp)))
  (dolist (pt (coll:elements (pointlist sdp)))
    (ev:remove-notify sdp (new-name pt)))
  (ev:remove-notify sdp (coll:inserted (pointlist sdp)))
  (ev:remove-notify sdp (coll:deleted (pointlist sdp)))
  (sl:destroy (fr sdp)))

;;;---------------------------------------------
;;; End.
