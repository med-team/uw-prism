;;;
;;; write-neutron.cl
;;;
;;; The neutron panel gui and supporting code.
;;;
;;; 21-Jun-1994 I. Kalet write stub functions.
;;; 19-Jul-1994 J. Unger implement from spec.
;;; 21-Jul-1994 J. Unger partially adapt to arb collim size.  Needs work.
;;; 28-Jul-1994 J. Unger work on some more; check in partial impl.
;;; 02-Aug-1994 J. Unger impl leaf setting values.
;;; 05-Aug-1994 J. Unger misc modifications.
;;; 07-Aug-1994 J. Unger more mods to accommodate new cnts-coll & info.
;;; 09-Aug-1994 J. Jacky fill in write-neutron-file (stub by jmu)
;;; 10-Aug-1994 J. Jacky fix case wedge-rot -- rotation is float not fix
;;;                       also, add no wedge case
;;; 11-Aug-1994 J. Unger reverse beams before supplying to write-neutron-file.
;;; 11-Aug-1994 J. Jacky on panel, scale collim rot, wedge rot to
;;; machine coord on panel, "none" not NIL for wedge rot when no wedge
;;; on panel, prescribed dose defaults to 1600 not 0
;;; 11-Aug-1994 jmu/jpj  don't attempt to write file if no beams selected
;;; 15-Aug-1994 J. Jacky 5,1 not 5,2 format for leaf setting textlines
;;; SCX control software only goes to nearest millimeter!
;;; 23-Aug-1994 J. Jacky change centerline-list to edge-list
;;; 23-Aug-1994 J. Unger change readouts to textlines; make editable.
;;; 26-Aug-1994 J. Unger fix minor info/label bug in wedge-sel-btn
;;; 30-Aug-1994 J. Unger add call to transfer output neutron file to VAXes.
;;; 31-Aug-1994 J. Unger numerous final touches, impl neutron-beam class.
;;; 11-Sep-1994 J. Unger change make-volatile-textline to make-textline and
;;; add to destroy method.
;;; 21-Sep-1994 J. Unger add plan date, slight reorganization of controls.
;;; 23-Sep-1994 J. Unger remove label parameter from call to
;;; interactive-make-neutron-charts.
;;;  4-Oct-1994 J. Jacky Round monitor units to nearest whole monitor
;;;  unit throughout -- so total mu is always exactly equal to daily
;;;  mu times number of fractions, and number written out to file is
;;;  always of form nnn.0
;;;  4-Oct-1994 J. Unger minor change to plan-of pointer in call to
;;;  write-neutron-file.
;;; 19-Oct-1994 J. Jacky Don't assume integer items will be integers
;;; --- we find n of fractions in some Prism case files are floats;
;;; printing these in CL "a" format makes files unreadable by SCX
;;; software.  Fix: explicitly round, then print using CL "d" format.
;;; CL "d" requires integer, but CL "round" arg can be any numeric type.
;;; 21-Oct-1994 J. Unger change default presc dose to 0, put patient
;;; name in title.
;;; 19-Jan-1995 I. Kalet use current beam of panel instead of beam-for
;;;  of wedge (wedges no longer have back pointers).  Add plan to beam
;;;  pairs in output-alist.
;;;  9-Mar-1995 I. Kalet/J. Jacky write a 1, for 90 degrees, instead
;;;  of a 0, 0 degrees, for wedge rotation code, when there is no
;;;  wedge.
;;;  3-Sep-1995 I. Kalet take out beams-differ - not used anywhere
;;;  5-Jun-1997 I. Kalet machine returns the object, not the name
;;; 25-Aug-1997 I. Kalet remove invalid type specifier for
;;; collim-info, use the machine named CNTS-BLOCKS for collim-info
;;; cache, don't search the whole database.
;;; 16-Sep-1997 I. Kalet database in get-therapy-machine now required.
;;; Also make panel parameters local, not special.
;;; 24-Oct-1997 I. Kalet wedge-rot-angles now needs wedge id parameter
;;;  2-May-1998 I. Kalet use new chart-panel for printing chart pages
;;; 24-Dec-1998 I. Kalet take out wait t in run-subprocess, now default
;;; 22-Apr-1999 J. Jacky Revisions for new CNTS control system
;;; In this version run-subprocess cnts_xfer not neutron_xfer 
;;; Record 11 change from 2-digit to 4-digit year for Y2K
;;;  just simplify calculation of output-date in initialize-instance
;;; Record 21 print out pat-id, case-id, time-stamp for QA traceability
;;;  pass these as additional parameters to write-neutron
;;; Record 22 add transfer date, transfer user for info on Select Field screen
;;; Record 22 add also completion status, origin, and parent field
;;; 23-Apr-1999 Change prompt "...10 to 30 seconds..." to "a few seconds"
;;;  2-Jan-2000 I. Kalet add #+allegro qualifier to call to sys:getenv
;;; 23-Jan-2000 I. Kalet restore missing fix of 15-Sep-1999: adjust
;;; for changes to compute-mlc in regard to collimator angle.
;;; 17-Feb-2000 I. Kalet add code to set wedge parameters after call
;;; to copy-beam, since copy-beam now always deletes the wedge.  Also,
;;; make sure wedge rotation displayed as NONE when no wedge is selected.
;;; 23-Feb-2000 I. Kalet change copy-beam to just copy, so no need to
;;; separately set the wedge parameters.
;;; 19-Mar-2000 I. Kalet revisions for new chart code.
;;; 29-Jun-2000 I. Kalet modify signature of make-neutron-panel to fit
;;; new style of tools panel function invocation.
;;; 23-Feb-2001 J. Jacky write-neutron-file: new seq-trunc truncates strings 
;;; to max. field width.
;;; 24-Feb-2001 I. Kalet add end line, take out blank lines.
;;; 17-Feb-2005 A. Simms replace occurrence of sys:getenv with misc.cl 
;;; getenv function.
;;; 19-May-2010 I. Kalet textlines return strings so use
;;; read-from-string before using format to write back values to leaf
;;; textlines.
;;; 

(in-package :prism)

;;;--------------------------------------

(defclass neutron-panel (generic-panel)

  ((fr :type sl:frame
       :accessor fr
       :documentation "The SLIK frame that contains the neutron panel.")

   (del-pnl-btn ;; :type sl:button
    :accessor del-pnl-btn
    :documentation "The delete panel button for this panel.")

   (add-beam-btn ;; :type sl:button
    :accessor add-beam-btn
    :documentation "The add beam button for this panel.")

   (write-file-btn ;; :type sl:button
    :accessor write-file-btn
    :documentation "The write file button for this panel.")

   (comments-box ;; :type sl:textbox
    :accessor comments-box
    :documentation "The plan comments box for this panel.")

   (comments-label ;; :type sl:readout
    :accessor comments-label
    :documentation "The label for this panel's comments box.")

   (beam-rdt ;; :type sl:readout
    :accessor beam-rdt
    :documentation "The beam readout for this panel.")

   (plan-rdt ;; :type sl:readout
    :accessor plan-rdt
    :documentation "The plan name readout for this panel.")

   (date-rdt ;; :type sl:readout
    :accessor date-rdt
    :documentation "The plan date readout for this panel.")

   (plan-scr ;; :type sl:scrolling-list
    :accessor plan-scr
    :documentation "A scrolling list of available plans.")

   (plan-label ;; :type sl:readout
    :accessor plan-label
    :documentation "The label for the plans scrolling list.")

   (beam-scr ;; :type sl:scrolling-list
    :accessor beam-scr
    :documentation "A scrolling list of available beams.")

   (beam-label ;; :type sl:readout
    :accessor beam-label
    :documentation "The label for the beams scrolling list.")

   (output-scr ;; :type sl:scrolling-list
    :accessor output-scr
    :documentation "A scrolling list of beams that are
to be output by the neutron panel.")

   (output-label ;; :type sl:readout
    :accessor output-label
    :documentation "The label for the output scrolling list.")

   (phys-name-tln ;; :type sl:readout
    :accessor phys-name-tln
    :documentation "The physician's name textline.")

   (presc-dose-tln ;; :type sl:readout
    :accessor presc-dose-tln
    :documentation "The prescribed dose textline.")

   (gan-start-tln ;; :type sl:textline
    :accessor gan-start-tln
    :documentation "The gantry starting angle textline.")

   (gan-stop-tln ;; :type sl:textline
    :accessor gan-stop-tln
    :documentation "The gantry stopping angle textline.")

   (n-treat-tln ;; :type sl:textline
    :accessor n-treat-tln
    :documentation "The num treatments textline.")

   (tot-mu-rdt ;; :type sl:readout
    :accessor tot-mu-rdt
    :documentation "The total monitor units readout.")

   (mu-treat-tln ;; :type sl:textline
    :accessor mu-treat-tln
    :documentation "The monitor units per treatment textline.")

   (col-ang-tln ;; :type sl:textline
    :accessor col-ang-tln
    :documentation "The collimator angle textline.")

   (couch-ang-tln ;; :type sl:textline
    :accessor couch-ang-tln
    :documentation "The couch angle textline.")

   (wdg-sel-btn ;; :type sl:button
    :accessor wdg-sel-btn
    :documentation "The wedge selection button.")

   (wdg-rot-btn ;; :type sl:button
    :accessor wdg-rot-btn
    :documentation "The wedge rotation button.")

   (left-leaf-tlns ;; :type list
    :accessor left-leaf-tlns
    :initform nil
    :documentation "A list of left side mlc leaf textlines.")

   (right-leaf-tlns ;; :type list
    :accessor right-leaf-tlns
    :initform nil
    :documentation "A list of right side mlc leaf textlines.")

   (plan-alist :type list
               :accessor plan-alist
               :initform nil
               :documentation "An assoc list of plans and buttons in
the panel's scrolling list of plans.")

   (beam-alist :type list
               :accessor beam-alist
               :initform nil
               :documentation "An assoc list of beams and buttons in
the panel's scrolling list of beams.")

   (output-alist :type list
                 :accessor output-alist
                 :initform nil
                 :documentation "The association list of (original-beam
current-beam) pairs and buttons in the panel's scrolling list of beams to 
be output.")

   (current-patient :type patient
                    :accessor current-patient
                    :initarg :current-patient
                    :documentation "The current patient for the
neutron panel, supplied at initialization time.")

   (current-plan :type plan
                 :accessor current-plan
                 :initform nil
                 :documentation "The plan that the neutron panel is
currently displaying.")

   (current-beam :type beam
                 :accessor current-beam
                 :initform nil
                 :documentation "The beam that the neutron panel is
currently displaying.")

   (original-beam :type beam
                  :accessor original-beam
                  :initform nil
                  :documentation "The original version of the beam that 
the neutron panel is currently displaying.")

  (phys-name :type string
             :accessor phys-name
             :initform "NO PHYS NAME"
             :documentation "The physician name")

  (presc-dose :type fixnum
              :accessor presc-dose
              :initform 0
              :documentation "The prescribed dose")

  (collim-info :accessor collim-info
               :documentation "A cache for the collimator info of the
current beam.")

  )

  (:documentation "The neutron panel is used to select plans and beams
for subsequent writing to the filesystem and (outside of prism) later
transfer to the cyclotron.")

  )

;;;---------------------------------------------

(defmethod initialize-instance :after ((np neutron-panel) &rest initargs)

  "Initializes the neutron panel gui."

  (let* ((np-off 10)			; Intercontrol spacing factor
	 (np-rdt-ht 30)			; readout height
	 (np-rdt-base 80)		; base readout width
	 (np-scr-ht (* 4 np-rdt-ht))	; scrolling list height
	 (np-tb-ht (* 3 np-rdt-ht))	; textbox height
	 (np-wd (+ (* 6 np-off)
		   (* 10 np-rdt-base)))	; panel width
	 (np-ht (+ (* 11 np-rdt-ht)
			 (* 12  np-off)
			 np-scr-ht
			 np-tb-ht))	; panel height
	 (np-tl-color 'sl:green)	; textline border color
	 (np-rdt-color 'sl:white)	; readout border color
	 (np-bt-color 'sl:cyan)		; button border color
	 (frm (apply #'sl:make-frame np-wd np-ht 
		     :title (format nil "Prism NEUTRON Panel -- ~a" 
				    (name (current-patient np)))
		     initargs))
         (frm-win (sl:window frm))
         (cmts-r (apply #'sl:make-readout
			(* 2 np-rdt-base) np-rdt-ht
			:parent frm-win
			:ulc-x np-off
			:ulc-y (+ (* 6 np-off) (* 5 np-rdt-ht)
				  np-scr-ht)
			:border-color 'sl:black
			:label "Plan Comments:"
                        initargs))
         (cmts-bx (apply #'sl:make-textbox
			 (+ (* 6 np-rdt-base) (* 2 np-off))
			 np-tb-ht
			 :parent frm-win
			 :ulc-x np-off
			 :ulc-y (+ (* 6 np-off) (* 6 np-rdt-ht)
				   np-scr-ht)
			 :border-color np-rdt-color
			 initargs))
         (date-r (apply #'sl:make-readout
			(+ (* 6 np-rdt-base) (* 2 np-off))
			np-rdt-ht
			:parent frm-win
			:ulc-x np-off
			:ulc-y (+ (* 4 np-off) (* 3 np-rdt-ht)
				  np-scr-ht)
			:border-color np-rdt-color
			:label "Plan Date: "
                        initargs))
         (plan-r (apply #'sl:make-readout
			(+ (* 6 np-rdt-base) (* 2 np-off))
			np-rdt-ht
			:parent frm-win
			:ulc-x np-off
			:ulc-y (+ (* 3 np-off) (* 2 np-rdt-ht)
				  np-scr-ht)
			:border-color np-rdt-color
			:label "Plan Name: "
                        initargs))
         (beam-r (apply #'sl:make-readout
			(+ (* 6 np-rdt-base) (* 2 np-off))
			np-rdt-ht
			:parent frm-win
			:ulc-x np-off
			:ulc-y (+ (* 5 np-off) (* 4 np-rdt-ht)
				  np-scr-ht)
			:border-color np-rdt-color
			:label "Beam Name: "
                        initargs))
         (plan-l (apply #'sl:make-readout
			(round (* 1.5 np-rdt-base)) np-rdt-ht
			:parent frm-win
			:ulc-x np-off
			:ulc-y (+ np-rdt-ht (* 2 np-off))
			:border-color 'sl:black
			:label "Plans:"
                        initargs))
         (plan-s (apply #'sl:make-radio-scrolling-list 
			(round (* 1.5 np-rdt-base)) np-scr-ht
			:parent frm-win
			:ulc-x np-off
			:ulc-y (+ (* 2 np-off) (* 2 np-rdt-ht))
			:border-color np-bt-color
                        initargs))
         (beam-l (apply #'sl:make-readout
			(round (* 1.5 np-rdt-base)) np-rdt-ht
			:parent frm-win
			:ulc-x (+ (* 2 np-off) (sl:width plan-s))
			:ulc-y (+ np-rdt-ht (* 2 np-off))
			:border-color 'sl:black
			:label "Beams:"
                        initargs))
         (beam-s (apply #'sl:make-radio-scrolling-list 
			(round (* 1.5 np-rdt-base)) np-scr-ht
			:parent frm-win
			:ulc-x (+ (* 2 np-off) (sl:width plan-s))
			:ulc-y (+ (* 2 np-off) (* 2 np-rdt-ht))
			:border-color np-bt-color
			initargs))  
         (output-l (apply #'sl:make-readout
			  (* 3 np-rdt-base) np-rdt-ht
			  :parent frm-win
			  :ulc-x (+ (* 3 np-off) (* 2 (sl:width plan-s)))
			  :ulc-y (+ np-rdt-ht (* 2 np-off))
			  :border-color 'sl:black
			  :label "Output:"
			  initargs))
         (output-s (apply #'sl:make-scrolling-list 
			  (* 3 np-rdt-base) np-scr-ht
			  :parent frm-win
			  :ulc-x (+ (* 3 np-off) (* 2 (sl:width plan-s)))
			  :ulc-y (+ (* 2 np-off) (* 2 np-rdt-ht))
			  :enable-delete t
			  :border-color np-bt-color
			  initargs))  
         (del-pnl-b (apply #'sl:make-button 
			   (* 2 np-rdt-base) np-rdt-ht 
			   :parent frm-win
			   :ulc-x np-off :ulc-y np-off
			   :label "Del Panel"
			   :button-type :momentary
			   :border-color np-bt-color
                           initargs))
         (add-beam-b (apply #'sl:make-button 
			    (* 2 np-rdt-base) np-rdt-ht 
			    :parent frm-win
			    :ulc-x (+ (* 2 np-off) (* 2 np-rdt-base))
			    :ulc-y np-off
			    :label "Add Beam"
			    :button-type :momentary
			    :border-color np-bt-color
                            initargs))
         (write-file-b (apply #'sl:make-button 
			      (* 2 np-rdt-base) np-rdt-ht 
			      :parent frm-win
			      :ulc-x (+ (* 3 np-off) (* 4 np-rdt-base))
			      :ulc-y np-off
			      :label "Write File"
			      :button-type :momentary
			      :border-color np-bt-color
			      initargs))
         (phys-name-t (apply #'sl:make-textline
			     (* 3 np-rdt-base) np-rdt-ht
			     :parent frm-win
			     :ulc-x np-off
			     :ulc-y (+ (* 7 np-off) (* 9 np-rdt-ht)
				       np-scr-ht)
			     :label "Phys name: "
			     :border-color np-tl-color
			     initargs))
         (presc-dose-t (apply #'sl:make-textline
			      (* 3 np-rdt-base) np-rdt-ht
			      :parent frm-win
			      :ulc-x (+ (* 3 np-off) (* 3 np-rdt-base))
			      :ulc-y (+ (* 7 np-off) (* 9 np-rdt-ht)
					np-scr-ht)
			      :label "Presc Dose: "
			      :numeric t
			      :lower-limit 0.0 :upper-limit 10000.0
			      :border-color np-tl-color
			      initargs))
         (gan-start-t (apply #'sl::make-textline
			     (* 3 np-rdt-base) np-rdt-ht
			     :parent frm-win
			     :ulc-x np-off
			     :ulc-y (+ (* 8 np-off) (* 10 np-rdt-ht)
				       np-scr-ht)
			     :label "Gan start: "
			     :numeric t
			     :lower-limit 0.0 :upper-limit 359.9
			     :border-color np-tl-color
                             initargs))
         (gan-stop-t (apply #'sl:make-textline
			    (* 3 np-rdt-base) np-rdt-ht
			    :parent frm-win
			    :ulc-x (+ (* 3 np-off) (* 3 np-rdt-base))
			    :ulc-y (+ (* 8 np-off) (* 10 np-rdt-ht)
				      np-scr-ht)
			    :label "Gan Stop: "
			    :numeric t
			    :lower-limit 0.0 :upper-limit 359.9
			    :border-color np-tl-color
			    initargs))
         (n-treat-t (apply #'sl:make-textline
			   (* 2 np-rdt-base) np-rdt-ht
			   :parent frm-win
			   :ulc-x np-off
			   :ulc-y (+ (* 11 np-off) (* 13 np-rdt-ht)
				     np-scr-ht)
			   :label "N Treat: "
			   :numeric t
			   :lower-limit 0 :upper-limit 99
			   :border-color np-tl-color
                           initargs))
         (tot-mu-r (apply #'sl:make-readout
			  (* 2 np-rdt-base) np-rdt-ht
			  :parent frm-win
			  :ulc-x (+ (* 3 np-off) (* 4 np-rdt-base))
			  :ulc-y (+ (* 11 np-off) (* 13 np-rdt-ht)
				    np-scr-ht)
			  :label "Tot Mu: "
			  :border-color np-rdt-color
			  initargs))
         (mu-treat-t (apply #'sl:make-textline
			    (* 2 np-rdt-base) np-rdt-ht
			    :parent frm-win
			    :ulc-x (+ (* 2 np-off) (* 2 np-rdt-base))
			    :ulc-y (+ (* 11 np-off) (* 13 np-rdt-ht)
				      np-scr-ht)
			    :label "Mu/Treat: "
			    :numeric t
			    :lower-limit 0.0 :upper-limit 999.0
			    :border-color np-tl-color
			    initargs))
         (col-ang-t (apply #'sl:make-textline
			   (* 3 np-rdt-base) np-rdt-ht
			   :parent frm-win
			   :ulc-x np-off
			   :ulc-y (+ (* 9 np-off) (* 11 np-rdt-ht)
				     np-scr-ht)
			   :label "Collim Ang: "
			   :numeric t
			   :lower-limit 0.0 :upper-limit 359.9
			   :border-color np-tl-color
                           initargs))
         (couch-ang-t (apply #'sl:make-textline
			     (* 3 np-rdt-base) np-rdt-ht
			     :parent frm-win
			     :ulc-x (+ (* 3 np-off) (* 3 np-rdt-base))
			     :ulc-y (+ (* 9 np-off) (* 11 np-rdt-ht)
				       np-scr-ht)
			     :label "Couch Ang: "
			     :numeric t
			     :lower-limit 0.0 :upper-limit 359.9
			     :border-color np-tl-color
			     initargs))
         (wdg-sel-b (apply #'sl:make-button
			   (* 3 np-rdt-base) np-rdt-ht
			   :parent frm-win
			   :ulc-x np-off
			   :ulc-y (+ (* 10 np-off) (* 12 np-rdt-ht)
				     np-scr-ht)
			   :label "Wedge Sel: No wedge"
			   :border-color np-bt-color
			   initargs))
         (wdg-rot-b (apply #'sl:make-button
			   (* 3 np-rdt-base) np-rdt-ht 
			   :parent frm-win
			   :ulc-x (+ (* 3 np-off) (* 3 np-rdt-base))
			   :ulc-y (+ (* 10 np-off) (* 12 np-rdt-ht)
				     np-scr-ht)
			   :label "Wedge Rot: NONE"
			   :border-color np-bt-color
                           initargs)))
    (setf
	(fr np) frm
	(comments-box np) cmts-bx
	(comments-label np) cmts-r
	(beam-rdt np) beam-r
	(plan-rdt np) plan-r
	(date-rdt np) date-r
	(plan-label np) plan-l
	(plan-scr np) plan-s
	(beam-label np) beam-l
	(beam-scr np) beam-s
	(output-label np) output-l
	(output-scr np) output-s
	(del-pnl-btn np) del-pnl-b
	(add-beam-btn np) add-beam-b
	(write-file-btn np) write-file-b    
	(phys-name-tln np) phys-name-t
	(presc-dose-tln np) presc-dose-t
	(gan-start-tln np) gan-start-t
	(gan-stop-tln np) gan-stop-t
	(n-treat-tln np) n-treat-t
	(tot-mu-rdt np) tot-mu-r
	(mu-treat-tln np) mu-treat-t
	(col-ang-tln np) col-ang-t
	(couch-ang-tln np) couch-ang-t
	(wdg-sel-btn np) wdg-sel-b
	(wdg-rot-btn np) wdg-rot-b)
    ;; Set the collim-info cache for the panel.  Use the machine named
    ;; CNTS-BLOCKS in the therapy-machines database to set up the leaf
    ;; textlines in this panel.
    (setf (collim-info np)
      (collimator-info (get-therapy-machine "CNTS-BLOCKS"
					    *therapy-machine-database*
					    *machine-index-directory*)))
    ;; setup leaf textlines
    (do* ((collim-info (collim-info np))
	  (column-len (1- (length (edge-list collim-info))))
	  (width (* 2 np-rdt-base))
	  (height (round (/ (- np-ht (* 2 np-off)) column-len)))
	  (leaf-pairs (leaf-pair-map collim-info) (rest leaf-pairs))
	  (xl (+ (* 6 np-rdt-base) (* 4 np-off)))
	  (xr (+ (* 8 np-rdt-base) (* 5 np-off)))
	  (y np-off (+ y height))
	  (i 0 (1+ i)))
	((= i column-len))
      (push 
       (sl:make-textline width height
			 :parent frm-win
			 :ulc-x xl :ulc-y y
			 :numeric t 
			 :lower-limit (- (leaf-open-limit
					  (collim-info np)))
			 :upper-limit (leaf-overcenter-limit
				       (collim-info np))
			 :label (format nil "Leaf ~2@a: "
					(first (first leaf-pairs)))
			 :border-color np-tl-color
			 :volatile-width 4) ; shows up better
       (left-leaf-tlns np))
      (push
       (sl:make-textline width height
			 :parent frm-win
			 :ulc-x xr :ulc-y y
			 :numeric t 
			 :lower-limit (- (leaf-overcenter-limit
					  (collim-info np)))
			 :upper-limit (leaf-open-limit (collim-info np))
			 :label (format nil "Leaf ~2@a: "
					(second (first leaf-pairs)))
			 :border-color np-tl-color
			 :volatile-width 4) ; shows up better
       (right-leaf-tlns np)))
    (setf (left-leaf-tlns np) (reverse (left-leaf-tlns np)))
    (setf (right-leaf-tlns np) (reverse (right-leaf-tlns np)))
    ;; setup plan scrolling list
    (dolist (pln (coll:elements (plans (current-patient np))))
      (let ((btn (sl:make-list-button (plan-scr np) (name pln))))
	(sl:insert-button btn (plan-scr np))
	(setf (plan-alist np) (acons pln btn (plan-alist np)))))
    ;; setup physician name and prescribed dose text fields
    (setf (sl:info phys-name-t) (phys-name np))
    (setf (sl:info presc-dose-t) (write-to-string (presc-dose np)))
    ;; setup add-notifies
    (ev:add-notify np (sl:selected plan-s)
		   #'(lambda (np ann p-btn)
		       (declare (ignore ann))
		       (when (current-beam np)
			 (ev:remove-notify
			  np (new-id (wedge (current-beam np))))
			 (ev:remove-notify
			  np (new-rotation (wedge (current-beam np)))))
		       (setf (original-beam np) nil)
		       (setf (current-beam np) nil)
		       (setf (current-plan np)
			 (first (rassoc p-btn (plan-alist np))))))
    (ev:add-notify np (sl:selected beam-s)
		   #'(lambda (np ann b-btn)
		       (declare (ignore ann))
		       (when (current-beam np)
			 (ev:remove-notify
			  np (new-id (wedge (current-beam np))))
			 (ev:remove-notify
			  np (new-rotation (wedge (current-beam np)))))
		       (setf (original-beam np)
			 (first (rassoc b-btn (beam-alist np))))
		       (setf (current-beam np) (copy (original-beam np)))
		       ;; register with the current beam's wedge's id
		       ;; and rotation events
		       (ev:add-notify
			np (new-id (wedge (current-beam np)))
			#'(lambda (np wdg id)
			    (declare (ignore wdg))
			    (if (zerop id) (setf (sl:label (wdg-rot-btn np))
					     "Wedge Rot: NONE"))
			    (setf (sl:label (wdg-sel-btn np))
			      (format nil "Wedge Sel: ~a"
				      (wedge-label id (machine
						       (current-beam np)))))))
		       (ev:add-notify
			np (new-rotation (wedge (current-beam np)))
			#'(lambda (np wdg rot)
			    (if (zerop (id wdg))
				(setf (sl:label (wdg-rot-btn np))
				  "Wedge Rot: NONE")
			      (let ((mach (machine
					   (current-beam np))))
				(setf (sl:label (wdg-rot-btn np))
				  (format nil "Wedge Rot: ~a" 
					  (first (scale-angle 
						  rot
						  (wedge-rot-scale mach)
						  (wedge-rot-offset mach)))))))
			    ))))
    (ev:add-notify np (sl:deselected plan-s)
		   #'(lambda (np a btn)
		       (declare (ignore a btn))
		       (setf (current-plan np) nil)))
    (ev:add-notify np (sl:deselected beam-s)
		   #'(lambda (np a btn)
		       (declare (ignore a btn))
		       (when (current-beam np)
			 (ev:remove-notify
			  np (new-id (wedge (current-beam np))))
			 (ev:remove-notify
			  np (new-rotation (wedge (current-beam np)))))
		       (setf (original-beam np) nil)
		       (setf (current-beam np) nil)))
    (ev:add-notify np (sl:button-on del-pnl-b)
		   #'(lambda (np a)
		       (declare (ignore a))
		       (destroy np)))
    (ev:add-notify np (sl:button-on add-beam-b)
		   #'(lambda (np a)
		       (declare (ignore a))
		       (if (and (current-plan np) (current-beam np))
			   (let ((a-btn (sl:make-list-button 
					 (output-scr np) 
					 (format nil "~a - ~a" 
						 (name (current-beam
							np))
						 (name (current-plan np)))
					 :button-type :momentary)))
			     (sl:insert-button a-btn (output-scr np))
			     (setf (output-alist np) 
			       (acons 
				(list (original-beam np)
				      (current-beam np)
				      (current-plan np))
				a-btn 
				(output-alist np))))
			 (sl:acknowledge "Please select a beam to add."))
		       (setf (sl:on add-beam-b) nil)))
    (ev:add-notify np (sl:button-on write-file-b)
		   #'(lambda (np a)
		       (declare (ignore a))
		       (if (sl:confirm
		    '("Ready to transfer neutron file."
		      "This may take a few seconds."
		      "A chart dialog box will be displayed when finished."
		      "During transfer, please wait for chart dialog box."
		      "Ok to continue?"))
			   (if (output-alist np) 
			       (let* ((dts (date-time-string))
				      (blank (position #\Space dts))
				      (date (subseq dts 0 blank))
				      (fp (open *neutron-setup-file*
						:direction :output
						:if-exists :supersede
						:if-does-not-exist :create))
				      (beam-pairs (mapcar #'first
							  (output-alist np)))
				      (pln (third (first beam-pairs)))
				      (pat (current-patient np))
				      (output-date (if (= 11 (length date)) 
						    date
						  (format nil " ~a" date))))
				 ;; long wait coming up, ignore user input
				 (sl:push-event-level)
				 (write-neutron-file 
				  fp (patient-id pat) 
				  (case-id pat) (time-stamp pln) (name pat) 
					; different beams in list may come from
					; different plans so time-stamp may be
					; wrong for some beams - JJ 4/22/99
				  (hospital-id pat) output-date
				  (first (comments pln))
				  (phys-name np) (presc-dose np) 
				  (reverse ; current beams
				   (mapcar #'second beam-pairs)))
				 (close fp)
				 (run-subprocess "cnts_xfer")
				 (sl:pop-event-level) ; long wait is over
				 (chart-panel 'neutron
					      pat nil beam-pairs dts))
			     ;; used to say "destroy np" but causes
			     ;; asynch drawable error
			     (sl:acknowledge
			      "No beams selected; NO file transferred!"))
			 (sl:acknowledge "Neutron file NOT transferred!"))
		       (setf (sl:on write-file-b) nil)))
    (ev:add-notify np (sl:deleted output-s)
		   #'(lambda (np a btn)
		       (declare (ignore a))
		       (let ((pair (rassoc btn (output-alist np))))
			 (setf (output-alist np)
			   (remove pair (output-alist np))))))
    (ev:add-notify np (sl:new-info phys-name-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (setf (phys-name np) info)))
    (ev:add-notify np (sl:new-info presc-dose-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (setf (presc-dose np)
			 (round (read-from-string info)))))
    (ev:add-notify np (sl:new-info gan-start-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let* ((cb (current-beam np))
				  (mach (machine cb)))
			     (setf (gantry-angle cb) 
			       (inverse-scale-angle 
				(read-from-string info)
				(gantry-scale mach)
				(gantry-offset mach)))
			     (setf (arc-size cb) 0.0)
			     (setf (sl:info gan-start-t) 
			       (format nil "~6,1F" (read-from-string info)))
			     (setf (sl:info gan-stop-t) 
			       (format nil "~6,1F" (sl:info gan-start-t))))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:info gan-start-t) "")))))
    (ev:add-notify np (sl:new-info gan-stop-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let* ((cb (current-beam np))
				  (mach (machine cb)))
			     (setf (arc-size cb)
			       (- (inverse-scale-angle 
				   (read-from-string info)
				   (gantry-scale mach)
				   (gantry-offset mach))
				  (gantry-angle cb)))
			     (setf (sl:info gan-stop-t) 
			       (format nil "~6,1F" (read-from-string info))))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:info gan-stop-t) "")))))
    (ev:add-notify np (sl:new-info n-treat-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let ((cb (current-beam np)))
			     (setf (n-treatments cb)
			       (truncate (read-from-string info)))
			     (setf (monitor-units cb) 
			       (* (n-treatments cb) 
				  (round (read-from-string
					  (sl:info mu-treat-t)))))
			     (setf (sl:info tot-mu-r) 
			       (let* ((mu-tot (monitor-units cb))
				      (n (n-treatments cb))
				      (r-mu-per-frac (round (/ mu-tot n))))
				 (* r-mu-per-frac n))))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:info n-treat-t) "")))))
    (ev:add-notify np (sl:new-info mu-treat-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let ((cb (current-beam np)))
			     (setf (monitor-units cb) 
			       (* (n-treatments cb)
				  (round (read-from-string info))))
			     (setf (sl:info tot-mu-r) 
			       (let* ((mu-tot (round (monitor-units cb)))
				      (n (n-treatments cb))
				      (r-mu-per-frac (round (/ mu-tot n))))
				 (* r-mu-per-frac n))))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:info mu-treat-t) "")))))
    (ev:add-notify np (sl:new-info col-ang-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let* ((cb (current-beam np))
				  (mach (machine cb)))
			     (setf (collimator-angle cb)
			       (inverse-scale-angle
				(read-from-string info)
				(collimator-scale mach)
				(collimator-offset mach)))
			     (setf (sl:info col-ang-t) 
			       (format nil "~6,1F" (read-from-string info))))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:info col-ang-t) "")))))
    (ev:add-notify np (sl:new-info couch-ang-t)
		   #'(lambda (np a info)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let* ((cb (current-beam np))
				  (mach (machine cb)))
			     (setf (couch-angle cb)
			       (inverse-scale-angle
				(read-from-string info)
				(turntable-scale mach)
				(turntable-offset mach)))
			     (setf (sl:info couch-ang-t) 
			       (format nil "~6,1F" (read-from-string info))))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:info couch-ang-t) "")))))
    (ev:add-notify np (sl:button-on wdg-sel-b)
		   #'(lambda (np a)
		       (declare (ignore a))
		       (if (current-beam np)
			   (let* ((cb (current-beam np))
				  (mach (machine cb))
				  (new-wdg-no (sl:popup-menu
					       (wedge-names mach))))
			     (when new-wdg-no
			       (setf (id (wedge cb)) new-wdg-no)))
			 (progn
			   (sl:acknowledge "Please select a beam first.")
			   (setf (sl:label wdg-sel-b) "Wedge Sel: No wedge")))
		       (setf (sl:on wdg-sel-b) nil)))
    (ev:add-notify np (sl:button-on wdg-rot-b)
		   #'(lambda (np a)
		       (declare (ignore a))
		       (if (current-beam np)
			   (if (zerop (id (wedge (current-beam np))))
			       (sl:acknowledge "Please select a wedge first.")
			     (let* ((cb (current-beam np))
				    (mach (machine cb))
				    (angles (wedge-rot-angles (id (wedge cb))
							      mach))
				    (scl-ang (mapcar
					      #'(lambda (angle) 
						  (first
						   (scale-angle
						    angle
						    (wedge-rot-scale mach)
						    (wedge-rot-offset mach))))
					      angles))
				    (pos (sl:popup-menu
					  (mapcar #'write-to-string scl-ang)))
				    (choice (when pos (nth pos angles))))
			       (when choice
				 (setf (rotation (wedge cb)) choice))))
			 (sl:acknowledge "Please select a beam first."))
		       (setf (sl:on wdg-rot-b) nil)))
    ;; add-notifies for the leaf textlines
    (do ((left-tlns (left-leaf-tlns np) (rest left-tlns))
	 (right-tlns (right-leaf-tlns np) (rest right-tlns)))
	((null left-tlns))
      (ev:add-notify np (sl:new-info (first left-tlns))
		     #'(lambda (np tln info)
			 (if (current-beam np)
			     (let* ((pos (position tln (left-leaf-tlns np)))
				    (cb (current-beam np))
				    (ls (leaf-settings (collimator cb)))
				    (float-info
				     (float (read-from-string info))))
			       (setf (sl:info tln)
				 (format nil "~5,1F" float-info))
			       (setf (first (nth pos ls))
				 float-info))
			   (progn
			     (sl:acknowledge "Please select a beam first.")
			     (setf (sl:info tln) "")))))
      (ev:add-notify np (sl:new-info (first right-tlns))
		     #'(lambda (np tln info)
			 (if (current-beam np)
			     (let* ((pos (position tln (right-leaf-tlns np)))
				    (cb (current-beam np))
				    (ls (leaf-settings (collimator cb)))
				    (float-info
				     (float (read-from-string info))))
			       (setf (sl:info tln)
				 (format nil "~5,1F" float-info))
			       (setf (second (nth pos ls))
				 float-info))
			   (progn
			     (sl:acknowledge "Please select a beam first.")
			     (setf (sl:info tln) ""))))))))

;;;---------------------------------------------

(defmethod (setf current-plan) :after (new-plan (np neutron-panel))

  (if new-plan
      (progn
	;; fill up beams scrolling list and alist w/ new info -- only beams 
	;; w/collimators of type cnts-coll are considered.
	(dolist (bm (remove-if-not 
		     #'(lambda (coll) (typep coll 'cnts-coll))
		     (coll:elements (beams new-plan))
		     :key #'collimator))
	  (let ((b-btn (sl:make-list-button (beam-scr np) (name bm))))
	    (sl:insert-button b-btn (beam-scr np))
	    (setf (beam-alist np) (acons bm b-btn (beam-alist np)))))
					; fill in plan readout
	(setf (sl:info (plan-rdt np)) (name new-plan))
	(setf (sl:info (date-rdt np)) (time-stamp new-plan))
	;; fill in plan-specific info on panel
	(setf (sl:info (comments-box np)) (comments new-plan)))
    (progn ;; clean out beams scrolling list and alist
      (dolist (b-btn (sl:buttons (beam-scr np)))
        (sl:delete-button b-btn (beam-scr np)))
      (setf (beam-alist np) nil) ;; clear plan-specific info on panel
      (setf (sl:info (plan-rdt np)) "")
      (setf (sl:info (date-rdt np)) "")
      (setf (sl:info (comments-box np)) '("")))))

;;;---------------------------------------------

(defmethod (setf current-beam) :after (new-beam (np neutron-panel))

  (if new-beam
      (let ((mach (machine new-beam)))
	(setf (sl:info (beam-rdt np)) (name new-beam))
	(setf (sl:info (gan-start-tln np)) 
	  (format nil "~6,1F" (first (scale-angle 
				      (gantry-angle new-beam)
				      (gantry-scale mach)
				      (gantry-offset mach)))))
	(setf (sl:info (gan-stop-tln np)) 
	  (format nil "~6,1F"
		  (mod (+ (gantry-angle new-beam) (arc-size new-beam)) 360)))
	(setf (sl:info (couch-ang-tln np)) 
	  (format nil "~6,1F" (first (scale-angle 
				      (couch-angle new-beam)
				      (turntable-scale mach)
				      (turntable-offset mach)))))
	(setf (sl:info (n-treat-tln np)) (n-treatments new-beam))
	(let* ((mu-tot (monitor-units new-beam)) ; no fractional mu
	       (n (n-treatments new-beam))
	       (r-mu-per-frac (round (/ mu-tot n)))
	       (r-tot-mu (* r-mu-per-frac n)))
	  (setf (sl:info (tot-mu-rdt np))	r-tot-mu)
	  (setf (sl:info (mu-treat-tln np)) r-mu-per-frac))
	(setf (sl:info (col-ang-tln np))
	  (format nil "~6,1F" (first (scale-angle 
				      (collimator-angle new-beam)
				      (collimator-scale mach)
				      (collimator-offset mach)))))
	(setf (sl:label (wdg-sel-btn np)) 
	  (format nil "Wedge Sel: ~a"
		  (wedge-label (id (wedge new-beam)) (machine new-beam))))
	(let ((scaled-wdg-rot (if (zerop (id (wedge new-beam))) "NONE"
				(first (scale-angle
					(rotation (wedge new-beam))
					(wedge-rot-scale mach)
					(wedge-rot-offset mach))))))
	  (setf (sl:label (wdg-rot-btn np)) 
	    (format nil "Wedge Rot: ~a" scaled-wdg-rot)))

	;; set this beam's collimator's leaf-settings cache, and the
	;; cache of the original copy of this beam as well
	(setf (leaf-settings (collimator new-beam))
	  (compute-mlc (collimator-angle new-beam)
		       (get-mlc-vertices new-beam)
		       (edge-list (collim-info np))))
	(setf (leaf-settings (collimator (original-beam np)))
	  (compute-mlc (collimator-angle (original-beam np))
		       (get-mlc-vertices new-beam)
		       (edge-list (collim-info np))))

	;; set the leaf textline values
	(do* ((l-tlns (left-leaf-tlns np) (rest l-tlns))
	      (r-tlns (right-leaf-tlns np) (rest r-tlns))
	      (leaves (leaf-settings (collimator new-beam)) (rest leaves))
	      (leaf-pair (first leaves) (first leaves)))
	    ((null leaves))
	  (setf (sl:info (first l-tlns))
	    (format nil "~5,1F" (first leaf-pair)))
	  (setf (sl:info (first r-tlns))
	    (format nil "~5,1F" (second leaf-pair)))))
    (progn
      (setf (sl:info (beam-rdt np)) "")
      (setf (sl:info (gan-start-tln np)) "")
      (setf (sl:info (gan-stop-tln np)) "")
      (setf (sl:info (couch-ang-tln np)) "")
      (setf (sl:info (n-treat-tln np)) "")
      (setf (sl:info (tot-mu-rdt np)) "")
      (setf (sl:info (mu-treat-tln np)) "")
      (setf (sl:info (col-ang-tln np)) "")
      (setf (sl:label (wdg-sel-btn np)) "Wedge Sel: No wedge")
      (setf (sl:label (wdg-rot-btn np)) "Wedge Rot: NONE")
      (mapc #'(lambda (l-rdt r-rdt) 
                (setf (sl:info l-rdt) "")
                (setf (sl:info r-rdt) ""))
	    (left-leaf-tlns np)
	    (right-leaf-tlns np)))))

;;;---------------------------------------------

(defun make-neutron-panel (pat &rest initargs)

  "make-neutron-panel pat &rest initargs

Creates and returns a neutron panel with the specified initargs."

  (apply #'make-instance 'neutron-panel :current-patient pat initargs))

;;;---------------------------------------------

(defmethod destroy :before ((np neutron-panel))

  "Unmap the panel's frame."

  (when (current-beam np)
    (ev:remove-notify np (new-id (wedge (current-beam np))))
    (ev:remove-notify np (new-rotation (wedge (current-beam np)))))
  (sl:destroy (del-pnl-btn np))
  (sl:destroy (add-beam-btn np))
  (sl:destroy (write-file-btn np))
  (sl:destroy (comments-box np))
  (sl:destroy (comments-label np))
  (sl:destroy (beam-rdt np))
  (sl:destroy (plan-rdt np))
  (sl:destroy (date-rdt np))
  (sl:destroy (beam-label np))
  (sl:destroy (plan-label np))
  (sl:destroy (output-label np))
  (sl:destroy (phys-name-tln np))
  (sl:destroy (presc-dose-tln np))
  (sl:destroy (gan-start-tln np))
  (sl:destroy (gan-stop-tln np))
  (sl:destroy (n-treat-tln np))
  (sl:destroy (tot-mu-rdt np))
  (sl:destroy (mu-treat-tln np))
  (sl:destroy (col-ang-tln np))
  (sl:destroy (couch-ang-tln np))
  (sl:destroy (wdg-sel-btn np))
  (sl:destroy (wdg-rot-btn np))
  (mapcar #'sl:destroy (left-leaf-tlns np))
  (mapcar #'sl:destroy (right-leaf-tlns np))
  ;;  Destroying the scrolling lists gives async drawable errors....
  ;;  (sl:destroy (plan-scr np))
  ;;  (sl:destroy (beam-scr np))
  ;;  (sl:destroy (output-scr np))
  (sl:destroy (fr np)))

;;;---------------------------------------------

(defun write-neutron-file (fp pat-id case-id plan-time pat-name hosp-id date 
			   plan-comment phys-name presc-dose beams)

  "write-neutron-file fp pat-id case-id plan-time pat-name hosp-id date
                      plan-comment phys-name presc-dose beams

Writes a file full of beam-specific neutron setup to stream fp, based
on the supplied patient id, case id, plan time stamp, patient name, 
hospital id, plan comment string, physician name, prescribed dose, 
and list of beams."

;;;
;;; Lines from sample output file, with no blanks before start of data
;;; 
;;;11  4268 LASTNAME, FIRSTNAME            85-62-92        23-Apr-1999
;;;12 KR                                 0.0     0.0
;;;13 composite: boost with initial fields                                    
;;;21  5 RPO BOOST                      I N N T  4268  2 21-Apr-1999 15:57:01
;;;22  3  0  486.0    0.0  162.0 0 1  0  270.0 23-Apr-1999 jon      X T  0
;;;23  120.0   50.0   50.0  180.0  180.0  250.0  250.0
;;;24 0  -4.2  -5.0  -5.0  -5.0   0.0   0.0   0.0   0.0   0.0   0.0
;;;24 1  -3.1  -2.0  -1.5  -1.5   0.0   0.0   0.0   0.0   0.0   0.0
;;;24 2   5.8   5.8   5.6   5.0   0.0   0.0   0.0   0.0   0.0   0.0
;;;24 3   5.2   4.0   2.9   2.9   0.0   0.0   0.0   0.0   0.0   0.0
;;;21  6 LAO BOOST                      I N N T
;;;22  3  0  384.0    0.0  128.0 0 1  0  270.0
;;; etc ...

  ;; header --- just once per file.  Date here must be in dd-mmm-yyyy form.
  (format fp "11 ~5@a ~30a ~15a ~11@a~%" 
	  pat-id (seq-trunc 30 pat-name) (seq-trunc 15 hosp-id) date)
  (format fp "12 ~30a ~7,1f ~7,1f~%" 
	  (seq-trunc 30 phys-name) presc-dose 0.0)
  (format fp "13 ~60a~%" (seq-trunc 60 plan-comment)) 
  ;; accum-dose above is always 0.0
  
  (let ((bm-num 0) ;; No beam number in Prism -- just count 'em up here
	(mach nil)) ;; just so mach isnt' "special"
    (dolist (bm beams)
      (setq bm-num (+ 1 bm-num))
      (setq mach (machine bm))

      ;; record 21 
      (format fp "21 ~2@a ~30a I ~1a N T ~5d ~2d ~20@a~%" 
					; I N T means iso,no ext blks,use table
	      bm-num (seq-trunc 30 (name bm)) 
	      (if (zerop (arc-size bm)) "N" "Y")
	      pat-id case-id plan-time)

      ;; record 22 
      (let* ((mu-tot (monitor-units bm))
	     (n (n-treatments bm))
	     (r-mu-per-frac (float (round (/ mu-tot n))))
	     (r-mu-tot (float (* r-mu-per-frac n)))
					; change 71.1 to 71.0 etc.
	     (wdg (wedge bm))
	     (wedge-id (id wdg))
	     (wedge-code (case wedge-id	; just tabulate it -- nothing fancy
			   ((0) 0)	; no wedge 
			   ((1 2) 1)	; Prism 30-SF, 30-LF --> Scx 30 degree 
			   ((3 4) 2)	; 45-SF, 45-LF 
			   ((5 6) 3)))	; 60-SF, 60-LF
	     (wedge-rot-code
	      (if (zerop wedge-id) 1 ;; 90 degrees for no wedge
		(case (first (scale-angle (rotation wdg)
					  (wedge-rot-scale mach)
					  (wedge-rot-offset mach)))
		  ((0.0) 0)
		  ((90.0) 1)
		  ((180.0) 2) 
		  ((270.0) 3))))
	     (scaled-collim-angle (first 
				   (scale-angle (collimator-angle bm)
						(collimator-scale mach)
						(collimator-offset mach)))))
	(format fp 
        "22 ~2d ~2@a ~6,1f ~6,1f ~6,1f ~1a ~1a ~2@a ~6,1f ~11@a ~8a X T  0 ~%"
		(round n) 0 r-mu-tot 0.0 r-mu-per-frac 
					; note mon units always of form nnn.0
					; accum n, accum dose always zero
		wedge-code wedge-rot-code 0 scaled-collim-angle
		date (seq-trunc 8 (getenv "USER"))))
                ; X T  0 are completion flag (X = not completed), 
                ; origin (T = transfered) and parent beam (0 = none)
      
      ;; record 23
      (let* ((scaled-couch-angle (first (scale-angle (couch-angle bm)
						     (turntable-scale mach)
						     (turntable-offset mach))))
	     (scaled-gantry-angle (first (scale-angle (gantry-angle bm)
						      (gantry-scale mach)
						      (gantry-offset mach))))
	     (scaled-gantry-stop (first (scale-angle (+ (gantry-angle bm)
							(arc-size bm))
						     (gantry-scale mach)
						     (gantry-offset mach)))))
	(format fp "23~{ ~6,1f~}~%" 
		(list 120.0 50.0 50.0 scaled-couch-angle 180.0 
		      ;; ignore couch-height, couch-lateral, couch-long
		      ;; always PSA vert 120, lat 50, long 50, top rot 180 
		      scaled-gantry-angle scaled-gantry-stop)))

      ;; record 24 --- leaves
      (let* ((leaves (leaf-settings (collimator bm)))
	     (leaves0-19 (mapcar #'first leaves)) ; leaf order is bizarre!
	     (leaves0-9  (reverse-first-ten leaves0-19))
	     (leaves10-19 (skip-ten leaves0-19))
	     (leaves20-39 (mapcar #'second leaves))
	     (leaves20-29 (skip-ten leaves20-39)) ; leaves 20-29 are at end
	     (leaves30-39 (reverse-first-ten leaves20-39))) ; 30-39 at front
	(dolist (line-num '(0 1 2 3))
	  (format fp "24 ~1a~{ ~5,1f~}~%" line-num 
		  (case line-num 
		    ((0) leaves0-9) ((1) leaves10-19) 
		    ((2) leaves20-29) ((3) leaves30-39))))))))

;;;---------------------------------------------

(defun reverse-first-ten (list)

  "reverse-first-ten list

Return a list which is the first ten elements of input list, in
reverse order.  Used to extract and reorder leaf settings."

  (let ((rlist nil)) (dotimes (i 10 rlist) (push (nth i list) rlist))))

;;;----------------------------------------------

(defun skip-ten (list)

  "skip-ten list

Return a list which is all but the first ten elements of input list.
Used to extract and reorder leaf settings."

  (let ((rlist list)) (dotimes (i 10 rlist) (setq rlist (rest rlist)))))

;;;-----------------------------------------------

(defun seq-trunc (width seq)
  
  "seq-trunc width seq

Truncate sequence to width so it doesn't overflow fixed-width column"

  (subseq seq 0 (min width (length seq)))) ; avoid array ref out-of-bounds

;;;-----------------------------------------------
;;; End.
