;;;
;;; dvh-panel
;;;
;;; ??-Aug-1998 C. Wilcox created
;;; 14-Apr-1999 I. Kalet modify some labels, also some code formatting
;;; 21-Jun-1999 J. Zeman implement print
;;; 22-Nov-1999 I. Kalet cleanup, fix some missing updates, list only
;;; plans that have valid dose distributions.
;;; 26-Nov-2000 I. Kalet cosmetic changes in dialog box.
;;;

(in-package :prism)

;;;-----------------------------------------

(defclass dvh-panel (generic-panel)

  ((frame :accessor frame
	  :documentation "The frame for the panel.")

   (the-plot  :accessor the-plot
	      :documentation "The 2d-plot widget.")

   (the-patient :accessor the-patient
		:initarg :the-patient
		:documentation "The patient record.")

   (plan-coll :accessor plan-coll
	      :initarg :plan-coll
	      :documentation "The collection of plans for the current
patient.")
   
   (plan-menu :accessor plan-menu
	      :documentation "A scrolling list of plans for the
current patient.")
   
   (plan-buttons :accessor plan-buttons
		 :initform nil
		 :documentation "A list of pairs of plans and
their corresponding buttons in the scrolling list.")

   (object :accessor object
	   :initarg :object
	   :documentation "The object for which DVH's are calculated.")
   
   (max-dose-ro :accessor max-dose-ro
		:documentation "The readout for the maximum dose.")

   (cumulative :accessor cumulative
	       :initform t
	       :documentation "A flag that says whether the display
is cumulative (true) or differential (false).")

   (bin-size :accessor bin-size
	     :initarg :bin-size
	     :initform 2
	     :documentation "The bin size in [cGy] for the DVH calc's.")

   (del-pan-b :accessor del-pan-b
	      :documentation "The button which destroys the panel when
pressed.")

   (widgets :accessor widgets
	    :documentation "The other ui widgets for the panel.")

   )

  (:documentation "The DVH panel displays dose-volume histogram plots
for a single object and multiple plans.")

  )

;;;--------------------------------------

(defun update-plot (dvhp)

  (let* ((plot (the-plot dvhp))
	 (obj (object dvhp))
	 (bin-size (bin-size dvhp)))
    (setf (sl:info (max-dose-ro dvhp)) "0")
    ;; assuming that length of plan-list and series-list are equal
    (dolist (pb-pair (plan-buttons dvhp))
      (let ((plan (first pb-pair))
	    (button (second pb-pair)))
	(if (sl:on button)
	    (sl::update-series plot plan (display-color (dose-grid plan)) 
			       (calc-series dvhp plan obj bin-size
					    (cumulative dvhp)))
	  (sl::remove-series plot plan))))
    (sl::draw-plot-lines plot)))

;;;--------------------------------------

(defun calc-series (dvhp plan obj bin-size cumulative)

  "Assume plan has a valid grid, return a series."

  (multiple-value-bind (nul-val dvh-vals)
      (scan obj (dose-grid plan) (grid (sum-dose plan))
	    :dvh-bin-size bin-size)
    (declare (ignore nul-val))
    (let ((tempx bin-size)
	  (plot-vals nil)
	  (pct 100)
	  (prev-max-dose (read-from-string (sl:info (max-dose-ro dvhp))))
	  (cur-max-dose (* bin-size (length dvh-vals))))
      (when cumulative (push '(0 100) plot-vals))
      (dotimes (i (length dvh-vals))
	(if cumulative
	    (progn
	      (setf pct (- pct (* 100 (aref dvh-vals i))))
	      (push (list tempx pct) plot-vals))
	  (progn ;; differential
	    (push (list (- tempx bin-size) (* 100 (aref dvh-vals i)))
		  plot-vals)
	    (push (list tempx (* 100 (aref dvh-vals i)))
		  plot-vals)))
	(setf tempx (+ tempx bin-size)))
      (when (< prev-max-dose cur-max-dose)
	(setf (sl:info (max-dose-ro dvhp)) cur-max-dose))
      plot-vals)))

;;;--------------------------------------

(defun add-plan (pln dvhp)

  (let* ((ob (object dvhp))
	 (plan-sl (plan-menu dvhp))
	 (btn (sl:make-list-button plan-sl (name pln)))
	 (plot (the-plot dvhp)))
    ;; set its foreground color
    (setf (sl:fg-color btn)
      (display-color (dose-grid pln)))
    ;; add it to the scrolling list
    (sl:insert-button btn plan-sl)
    ;; add notifies to keep state synchronized with the plans
    (ev:add-notify dvhp (new-name pln)
		   #'(lambda (pan pln newname)
		       (declare (ignore pan pln))
		       (setf (sl:label btn) newname)))
    (ev:add-notify dvhp (sl:button-on btn)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
		       (sl::update-series plot pln
					  (display-color (dose-grid pln))
					  (calc-series pan pln ob
						       (bin-size pan) 
						       (cumulative pan)))
		       (sl::draw-plot-lines plot)))
    (ev:add-notify dvhp (sl:button-off btn)
		   #'(lambda (pan btn)
		       (declare (ignore pan btn))
		       (sl::remove-series plot pln)
		       (sl::draw-plot-lines plot)))
    (ev:add-notify dvhp (new-color (dose-grid pln))
		   #'(lambda (pan grid newc)
		       (declare (ignore pan grid))
		       (let ((plotline (find-if #'(lambda (x)
						    (equal pln (first x)))
						(coll:elements 
						 (sl::series-coll plot)))))
			 (when plotline
			   (setf (second plotline) newc)
			   (sl::draw-plot-lines plot))
			 (setf (sl:fg-color btn) newc))))
    (push (list pln btn) (plan-buttons dvhp))))

;;;--------------------------------------

(defun remove-plan (pln dvhp)

  (let* ((planlst (plan-buttons dvhp))
	 (pln-btn-pair (find pln planlst :key #'first))
	 (btn (second pln-btn-pair)))
    (when btn
      (ev:remove-notify dvhp (new-name pln))
      (ev:remove-notify dvhp (sl:button-on btn))
      (ev:remove-notify dvhp (sl:button-off btn))
      (ev:remove-notify dvhp (new-color (dose-grid pln)))
      ;; this removes the button AND destroys it
      (sl:delete-button btn (plan-menu dvhp))
      (setf (plan-buttons dvhp) (remove pln-btn-pair planlst))
      (sl::remove-series (the-plot dvhp) pln)
      (sl::draw-plot-lines (the-plot dvhp)))))

;;;--------------------------------------

(defmethod initialize-instance :after ((dvhp dvh-panel)
				       &rest initargs)

  (let* ((ob (object dvhp))
	 (obname (name ob))
	 (fr-width 700)			; frame width
	 (fr-height 600)		; frame height
	 (l-plot 160)			; left-coords for plot
	 (gutter 5)			; room between widgets
	 (b-height 25)			; height of each button
	 (b-rows 2)			; rows of buttons at bottom
	 (b-cols 3)			; columns of buttons at bottom
	 ;; bottom-coords for plot
	 (b-plot (- fr-height (* (+ 1 b-rows) gutter) (* b-rows b-height)))  
	 (b-list (- b-plot (* 5 gutter) (* 4 b-height) 50))
	 (col-width (floor fr-width b-cols))
	 (frm (sl:make-frame fr-width fr-height 
			     :title (format nil "DVH PANEL: ~s" obname)))
	 (win (sl:window frm))
         (del-b (sl:make-button (- l-plot gutter gutter) b-height
				:parent (sl:window frm)
				:button-type :momentary
				:label "Del Pan"
				:ulc-x gutter :ulc-y gutter))
	 (plot (sl:make-2d-plot (- fr-width l-plot gutter)
				(- b-plot gutter gutter)
				:parent win
				:ulc-x l-plot :ulc-y gutter
				:bottom-label "DOSE - cGy"
				:left-label "VOLUME - %"
				:right-label "VOLUME - cc"
				:y-scale-factor (/ (physical-volume ob)
						   100.0)
				:delta 0.01))
	 (plan-sl (sl:make-scrolling-list (- l-plot gutter gutter) 
					  (- b-list (* 3 gutter) b-height) 
					  :label "Plan List" :parent win
					  :ulc-x gutter 
					  :ulc-y (+ b-height gutter gutter)))
	 (slider-title-ro
	  (sl:make-readout (- l-plot gutter gutter) b-height
			   :parent win
			   :ulc-x gutter :ulc-y (+ gutter b-list)
			   :bg-color 'sl:blue
			   :label "" :info "Slider Bar Vals"))
	 (percent-tl (sl:make-textline (- l-plot gutter gutter) b-height 
				       :ulc-x gutter
				       :ulc-y (+ b-list (* 2 gutter)
						 (* 1 b-height))
				       :lower-limit 0 :upper-limit 500
				       :parent win 
				       :numeric t :label "Vol[%]: "))
	 (cc-tl (sl:make-textline (- l-plot gutter gutter) b-height
				  :parent win
				  :ulc-x gutter
				  :ulc-y (+ b-list (* 3 gutter)
					    (* 2 b-height))
				  :lower-limit 0 :upper-limit 1000000
				  :numeric t :label "Vol[cc]: "))
	 (gy-tl (sl:make-textline (- l-plot gutter gutter) b-height
				  :parent win
				  :ulc-x gutter
				  :ulc-y (+ b-list (* 4 gutter)
					    (* 3 b-height))
				  :lower-limit 0 :upper-limit 50000
				  :label "Dose[cGy]: "
				  :numeric t))
	 (bin-tl (sl:make-textline (- col-width (* 4 gutter)) b-height
				   :parent win
				   :ulc-x (+ (* 0 col-width) gutter)
				   :ulc-y (+ b-plot (* 1 gutter)
					     (* 0 b-height))
				   :lower-limit 0.0001 :upper-limit 10000
				   :label "Bin Size[cGy]: "
				   :numeric t))
	 (dose-ro (sl:make-readout  (- col-width (* 4 gutter)) b-height 
				    :parent win 
				    :ulc-x (+ (* 0 col-width) gutter)
				    :ulc-y (+ b-plot (* 2 gutter)
					      (* 1 b-height))
				    :label "Max Dose[cGy]: "))
	 (display-b  (sl:make-button (- col-width gutter gutter) b-height
				     :label "Cumulative" :parent win
				     :ulc-x (+ (* 1 col-width) gutter) 
				     :ulc-y (+ b-plot (* 1 gutter)
					       (* 0 b-height))))
	 (stat-b (sl:make-button (- col-width gutter gutter) b-height
				 :label "Statistics" :parent win
				 :ulc-x (+ (* 1 col-width) gutter)
				 :ulc-y (+ b-plot (* 2 gutter)
					   (* 1 b-height))))
	 (print-b (sl:make-button (- col-width gutter gutter) b-height
				  :label "Print" :parent win
				  :ulc-x (+ (* 2 col-width) gutter)
				  :ulc-y (+ b-plot (* 1 gutter)
					    (* 0 b-height))))
	 (write-b (sl:make-button (- col-width gutter gutter) b-height
				  :label "Write Hist" :parent win
				  :ulc-x (+ (* 2 col-width) gutter)
				  :ulc-y (+ b-plot (* 2 gutter)
					    (* 1 b-height)))))
    ;; assign values to slots
    (setf (del-pan-b dvhp) del-b
	  (frame dvhp) frm
	  (the-plot dvhp) plot
	  (plan-menu dvhp) plan-sl
	  ;; list of widgets to destroy when panel is destroyed
	  (widgets dvhp) (list percent-tl cc-tl gy-tl bin-tl
			       display-b stat-b print-b write-b
			       slider-title-ro)
	  (max-dose-ro dvhp) dose-ro)
    (dolist (pl (coll:elements (plan-coll dvhp)))
      (let ((tmp pl)) ;; need this in order to retain for later ref.
	(ev:add-notify dvhp (grid-status-changed (sum-dose tmp))
		       #'(lambda (pan dose-res newstat)
			   (declare (ignore dose-res))
			   (if newstat
			       (if (find tmp (plan-buttons pan)
					 :key #'first)
				   (update-plot pan)
				 (add-plan tmp pan))
			     (remove-plan tmp pan))))
	(when (valid-grid (sum-dose tmp))
	  (add-plan tmp dvhp))))
    ;; assign info values for widgets
    (setf (sl:info percent-tl) "0"
	  (sl:info cc-tl) "0"
	  (sl:info gy-tl) "0"
	  (sl:info bin-tl) "2"
	  (sl:info dose-ro) "0")
    ;; create notifies
    (ev:add-notify dvhp (sl:button-on stat-b)
		   #'(lambda (pan bt) 
		       (declare (ignore pan))
		       (sl:acknowledge "Stats are not yet implemented...")
		       (setf (sl:on bt) nil)))
    (ev:add-notify dvhp (sl:button-on print-b)
		   #'(lambda (pan bt)
		       (print-dvh-panel pan)
		       (setf (sl:on bt) nil)))
    (ev:add-notify dvhp (sl:button-on write-b)
		   ;; write all selected plans to disk
		   #'(lambda (pan bt)
		       (let ((fname
			      (sl:popup-textline "dvh-results" 300
						 :title "Select a filename"
						 :label "Filename: ")))
			 (when fname (write-dvh-data pan fname))
			 (setf (sl:on bt) nil))))
    (ev:add-notify dvhp (new-name (object dvhp))
		   #'(lambda (pan pstruct newname)
		       (declare (ignore pan pstruct))
		       (setf (sl:title frm)
			 (format nil "DVH PANEL: ~s" newname))))
    (ev:add-notify dvhp (new-contours (object dvhp))
		   #'(lambda (dvhp pstruct)
		       (declare (ignore pstruct))
		       (update-plot dvhp)))
    (ev:add-notify dvhp (coll:inserted (plan-coll dvhp))
		   #'(lambda (pan coll plan)
		       (declare (ignore coll))
		       (ev:add-notify dvhp (grid-status-changed
					    (sum-dose plan))
				      #'(lambda (pan dose-res newstat)
					  (declare (ignore dose-res))
					  (if newstat
					      (if (find plan
							(plan-buttons dvhp)
							:key #'first)
						  (update-plot pan)
						(add-plan plan pan))
					    (remove-plan plan pan))))
		       (when (valid-grid (sum-dose plan))
			 (add-plan plan pan))))
    (ev:add-notify dvhp (coll:deleted (plan-coll dvhp))
		   #'(lambda (pan coll plan)
		       (declare (ignore coll))
		       (ev:remove-notify dvhp (grid-status-changed
					       (sum-dose plan)))
		       (remove-plan plan pan)))
    (ev:add-notify dvhp (sl:button-on del-b)
		   #'(lambda (dvhp bt)
		       (declare (ignore bt))
		       (destroy dvhp)))
    (ev:add-notify dvhp (sl:button-on display-b)
		   #'(lambda (pan bt)
		       (if (equal (sl:label bt) "Cumulative")
			   (setf (sl:label bt) "Differential"
				 (cumulative pan) nil)
			 (setf (sl:label bt) "Cumulative"
			       (cumulative pan) t))
		       (update-plot pan)
		       (setf (sl:on bt) nil)))
    (ev:add-notify dvhp (sl::new-slider-val plot)
		   #'(lambda (pan plot xval yval)
		       (declare (ignore plot))
		       (setf (sl:info percent-tl)
			 (format nil "~4F" yval))
		       (setf (sl:info cc-tl)
			 (format nil "~4F" 
				 (* yval 
				    (physical-volume (object pan))
				    0.01)))
		       (setf (sl:info gy-tl)
			 (format nil "~4F" xval))))
    (ev:add-notify dvhp (sl:new-info percent-tl)
		   #'(lambda (pan bx inf)
		       (declare (ignore bx))
		       (let* ((vol (physical-volume (object pan)))
			      (new-val (read-from-string inf))
			      (cc-val (* new-val vol 0.01)))
			 (setf (sl:info cc-tl) (format nil "~4F" cc-val)
			       (sl::y-slider-val plot) new-val)
			 (sl::draw-plot-lines plot))))
    (ev:add-notify dvhp (sl:new-info cc-tl)
		   #'(lambda (pan bx inf)
		       (declare (ignore bx))
		       (let* ((vol (physical-volume (object pan)))
			      (new-val (read-from-string inf))
			      (pct-val (/ (* 100.0 new-val) vol)))
			 (setf (sl:info percent-tl)
			   (format nil "~4F" pct-val))
			 (setf (sl::y-slider-val plot)
			   pct-val)
			 (sl::draw-plot-lines plot))))
    (ev:add-notify dvhp (sl:new-info gy-tl)
		   #'(lambda (pan bx inf)
		       (declare (ignore bx pan))
		       (let ((new-val (read-from-string inf)))
			 (setf (sl::x-slider-val plot) new-val)
			 (sl::draw-plot-lines plot))))
    (ev:add-notify dvhp (sl:new-info bin-tl)
		   #'(lambda (pan bx inf)
		       (declare (ignore bx))
		       (let ((new-val (read-from-string inf)))
			 (setf (bin-size pan) new-val)
			 (update-plot pan))))))

;;;--------------------------------------

(defmethod destroy :before ((dvhp dvh-panel))

  (dolist (p (mapcar #'first (plan-buttons dvhp)))
    (remove-plan p dvhp))
  (dolist (pl (coll:elements (plan-coll dvhp)))
    (ev:remove-notify dvhp (grid-status-changed (sum-dose pl))))
  (ev:remove-notify dvhp (coll:inserted (plan-coll dvhp)))
  (ev:remove-notify dvhp (coll:deleted (plan-coll dvhp)))
  (ev:remove-notify dvhp (new-contours (object dvhp)))
  (ev:remove-notify dvhp (new-name (object dvhp)))
  (sl:destroy (the-plot dvhp))
  (sl:destroy (plan-menu dvhp))
  (sl:destroy (max-dose-ro dvhp))
  (dolist (w (widgets dvhp)) (sl:destroy w))
  (sl:destroy (del-pan-b dvhp))
  (sl:destroy (frame dvhp)))

;;;--------------------------------------

(defun print-dvh (dvhp printer num-copies)

  (let ((ob (object dvhp))
	(patient (the-patient dvhp)))
    (with-open-file (strm "dvh-print"
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
      (ps:initialize strm 0.5 0.5 7.5 10.0)
      (format strm "stroke~%")
      (ps:set-position strm 0.0 8.0)
      (sl:print-2dplot strm (the-plot dvhp) 7.5 7.5 t)
      ;; erase slider vales, so a different format can be used
      (format strm "gsave 358 182 moveto 550 182 lineto~%")
      (format strm "550 234 lineto 358 234 lineto~%")
      (format strm "closepath 1 setgray fill grestore~%")
      (ps:prism-logo strm 5.5 10.5 *prism-version-string*)
      ;; heading at top of page
      (ps:set-position strm 0.0 0.25)
      (ps:put-text strm (format nil "Patient: ~A" 
				(name patient)))
      (ps:put-text strm ( format nil "Case Date: ~A" 
				 (date-entered patient)))
      (ps:put-text strm (format nil "Pat ID: ~A" 
				(patient-id patient)))
      (ps:put-text strm (format nil "Hosp ID: ~A"
				(hospital-id patient)))
      ;;print 'tumor' or 'organ' as appropriate
      (cond ((string-equal (name ob) "Target")
	     (ps:put-text strm "Target"))
	    (t (ps:put-text strm (format nil "Organ: ~A" (name ob)))))
      (ps:put-text strm (format nil "Bin Size: ~,2F cGy"
				(bin-size dvhp)))
      ;; extra labels to 2d-plot
      (ps:set-position strm 0 8)
      (ps:put-text strm "Plans:")
      (let ((listcount 0))
	(dolist (plan (plan-buttons dvhp))
	  (let ((pln (first plan))
		(button (second plan))
		(colr nil))
	    (when (sl:on button)
	      (incf listcount)
	      (when (equal listcount 5)
		(ps:indent strm 3)
		(ps:set-position strm 3 8)
		(ps:put-text strm ""))
	      ;; get color
	      (cond ((eq (display-color (dose-grid pln))
			 'sl:red) (setf colr '(1 0 0)))
		    ((eq (display-color (dose-grid pln))
			 'sl:blue) (setf colr '(0 0 1)))
		    ((eq (display-color (dose-grid pln))
			 'sl:green) (setf colr '(0 1 0)))
		    ((eq (display-color (dose-grid pln))
			 'sl:magenta) (setf colr '(.7 0 1)))
		    ((eq (display-color (dose-grid pln))
			 'sl:cyan) (setf colr '(0 1 1)))
		    ((eq (display-color (dose-grid pln))
			 'sl:gray) (setf colr '(.5 .5 .5)))
		    (t (setf colr '(0 0 0))))
	      (ps:set-graphics strm :color colr)
	      (ps:put-text strm "")
	      (ps:put-text strm (name pln))
	      (ps:set-graphics strm :color '(0 0 0))
	      (ps:put-text strm (format nil" ~A" (time-stamp pln)))))))
      (ps:indent strm 0)
      (format strm "360 184 moveto~%")
      (ps:put-text strm (format nil "Y (in cc): ~,2F" 
				(float (* (sl:y-slider-val (the-plot dvhp)) 
					  (/(physical-volume ob) 
					    100)))))
      (format strm "360 198 moveto~%")
      (ps:put-text strm (format nil "Y (in %):~,2F"
				(float (sl:y-slider-val (the-plot dvhp)))))
      (format strm "360 212 moveto~%")
      (ps:put-text strm (format nil "X (in cGy):~,2F"
				(float (sl:x-slider-val (the-plot dvhp)))))
      ;;box around slider vals
      (format strm "1 4 div setlinewidth~%")
      (format strm "358 182 moveto 358 226 lineto 498 226 lineto~%")
      (format strm "498 182 lineto 358 182 lineto stroke~%")
      (ps:finish-page strm)) ;;end with-open-file 
    (unless (string-equal "File only" printer)
      (dotimes (i num-copies)
	(run-subprocess (format nil "~a~a ~a"
				*spooler-command* printer "dvh-print"))))))

;;;--------------------------------------

(defun print-dvh-panel (dvhp)

  (sl:push-event-level)
  (let* ((num-copies 1)
	 (printer (first *postscript-printers*))
	 (printer-menu (sl:make-radio-menu 
			*postscript-printers* :mapped nil))
	 (delta-y (+ 10 (max (sl:height printer-menu) 100)
		     10))
	 (cbox (sl:make-frame (+ 10 (sl:width printer-menu)
				 10 150 10)
			      (+ delta-y 30 10 30 10)
			      :title "Print DVH"))
	 (win (sl:window cbox))
	 (cpy-tln (sl:make-textline 150 30 :parent win 
				    :label "Copies: "
				    :info (write-to-string num-copies)
				    :numeric t
				    :lower-limit 1
				    :upper-limit 9
				    :ulc-x (+ 10 (sl:width printer-menu)
					      10)
				    :ulc-y delta-y))
	 (accept-x (round (/ (- (sl:width cbox) 170) 2)))
	 (accept-btn (sl:make-exit-button 80 30 :label "Accept"
					  :parent win
					  :ulc-x accept-x
					  :ulc-y (+ delta-y 40)
					  :bg-color 'sl:green))
	 (cancel-btn (sl:make-exit-button 80 30 :label "Cancel"
					  :parent win
					  :ulc-x (+ accept-x 90)
					  :ulc-y (+ delta-y 40))))
    (clx:reparent-window (sl:window printer-menu) win 10 10)
    (clx:map-window (sl:window printer-menu))
    (clx:map-subwindows (sl:window printer-menu))
    (sl:select-button 0 printer-menu)
    (ev:add-notify cbox (sl:new-info cpy-tln)
		   #'(lambda (cbox tl info)
		       (declare (ignore cbox tl))
		       (setq num-copies (round (read-from-string info)))))
    (ev:add-notify cbox (sl:selected printer-menu)
		   #'(lambda (cbox m item)
		       (declare (ignore cbox m))
		       (setq printer (nth item *postscript-printers*))))
    (ev:add-notify dvhp (sl:button-on accept-btn)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (print-dvh pan printer num-copies)))
    (sl:process-events)
    (sl:destroy printer-menu)
    (sl:destroy cpy-tln)
    (sl:destroy accept-btn)
    (sl:destroy cancel-btn)
    (sl:destroy cbox)
    (sl:pop-event-level)))

;;;--------------------------------------

(defun write-dvh-data (dvhp fname)

  (let ((patient (the-patient dvhp))
	(ob (object dvhp)))
    (with-open-file (strm fname
		     :direction :output
		     :if-exists :supersede
		     :if-does-not-exist :create)
      (format strm "Time Stamp = ~s~%" (date-time-string))
      (format strm "Patient Name = ~s~%" (name patient))
      (format strm "Patient ID = ~s~%" (patient-id patient))
      (format strm "Case ID = ~s~%" (case-id patient))
      (format strm "Anastruct = ~s~%" (name ob))
      (format strm "Bin Size = ~s~%" (bin-size dvhp))
      (dolist (p (plan-buttons dvhp))
	(when (and (sl:on (second p))
		   (valid-grid (sum-dose (first p))))
	  (format strm "~%Plan = ~s~%" (name (first p)))
	  (multiple-value-bind (nul-val dvh-vals)
	      (scan ob (dose-grid (first p)) 
		    (grid (sum-dose (first p))) 
		    :dvh-bin-size (bin-size dvhp))
	    (declare (ignore nul-val))
	    (format strm "Number of Bins = ~s~%" (length dvh-vals))
	    (dotimes (i (length dvh-vals))
	      (format strm " ~f~%" (aref dvh-vals i)))))))))

;;;---------------------------------------
;;; End.











