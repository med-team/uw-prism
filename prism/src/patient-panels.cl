;;;
;;; patient-panels
;;;
;;; The Prism patient-panel class and associated functions.
;;;
;;; 27-Feb-1993 I. Kalet created from patients module, add new case
;;; selection, rearrange init. to facilitate switch to new case
;;; without destroying panel and making a new one.
;;;  4-Aug-1993 I. Kalet add comments textline, change name, hospital
;;;  id to readouts, add date-entered readout, checkpoint database buttons.
;;; 26-Oct-1993 I. Kalet pass new cached mini-image-set to easel
;;; 01-Nov-1993 J. Unger add references to *save-plan-dose* to checkpoint fn
;;; 13-Dec-1993 M. Phillips and J. Unger Changed tools-panel to
;;; tools-panel-action and changed the arguments of tools-panel from
;;; pan to (the-patient pan).
;;; 27-Apr-1994 J. Unger enhance target selector panel button to give
;;; 3-way choice over creation of new targets.
;;;  8-Jun-1994 J. Unger add *current-patient* global variable,
;;;  eliminate *save-plan-dose* mechanism for saving dose info, do not
;;;  provide lin-expand/ptvt choices when creating a target if no
;;;  tumors exist or none has more than 2 contours.
;;; 12-Mar-1995 I. Kalet pass patient to plan panel.  Eliminate
;;; vestigial exiting event.  Delete plans in replace-patient-case
;;; to free up X resources in old plan views.  Keep
;;; patient-plan-manager here, not in patient.
;;; 27-Apr-1995 I. Kalet add patient number to patient name display,
;;; make timestamp border change to red when set, back to white when
;;; case is archived or checkpointed.
;;;  5-Jun-1995 I. Kalet destroy and recreate grid-view manager for
;;;  new case here, after reading in from case file, since initially
;;;  plan connects views with a default grid, and reading the file
;;;  replaces it.  Same for pointers to dose-grid in the dose surfaces.
;;; 25-Jul-1995 I. Kalet implement confirm box for replacing patient
;;; case, per spec.
;;;  3-May-1997 I. Kalet always use *patient-database* for patient
;;; list, regardless of archive or checkpoint, provide option of
;;; entering a patient name string or number to restrict the patient
;;; menu, and prevent storage of patient 0, provide option of an
;;; alternate checkpoint database directory for retrieve, change call
;;; to make-plan-panel to conform to new signature.
;;; 25-Jun-1997 I. Kalet move organs, tumors and targets selector
;;; panels to the volume editor, register with new-immob-dev to
;;; update the copy in the volume editor when it changes, fix bug in
;;; connect-pat-panel on timestamp border color, take out patient-plan
;;; mgr, this is now incorporated back into the patient case itself.
;;; 28-Jun-1997 I. Kalet add call to panel fns. for pat db and irreg
;;;  7-Sep-1997 I. Kalet in replace-patient-case for other than main
;;;  patient database, list only patients with cases present.
;;;  9-Nov-1997 I. Kalet always use *patient-database* with
;;;  get-patient-entry because that is where patient.index is.
;;; 28-Apr-1998 I. Kalet set patient name and hospital ID from patient
;;; index here, not in get-case-data, because need *patient-database*
;;; 17-Jun-1998 I. Kalet force global gc after reading in new case,
;;; cosmetic changes, after checking consistency of mediator
;;; registrations and cleanups.
;;;  3-Nov-1998 C. Wilcox added DVH button.
;;; 25-Feb-1999 I. Kalet if no patient selected don't ask if ok to
;;; select new one, just do it.  Also clean up dvh panels on exit.
;;; 11-May-1999 I. Kalet when retrieving from checkpoint db, if user
;;; presses cancel in the checkpoint db textline, really cancel, don't
;;; use the default checkpoint db.
;;;  6-Apr-2000 I. Kalet connect initial patient case to
;;; *current-patient* in make-patient-panel, don't wait for first setf.
;;; Add more informative messages for failure to archive or checkpoint.
;;; Add source table mgr button here instead of brachy source entry panel.
;;; 30-May-2000 I. Kalet change background to gray, add shaded raised
;;; buttons and lowered textbox.
;;; 29-Jun-2000 I. Kalet change "RTPT Tools" to "Other Tools" - there
;;; has not been any RTPT stuff here for a while.
;;; 26-Nov-2000 I. Kalet default background is now gray and defaults
;;; for widgets are already appropriate, so remove from here.
;;; 31-Dec-2001 I. Kalet use match string for Retrieve as well as Select
;;;  4-Jan-2002 I. Kalet make comments textbox slightly higher to
;;; accomodate the bottom line.
;;; 21-Jun-2004 I. Kalet make panel title include Prism version
;;; number, parametrize choice of checkpoint directory for retrieve,
;;; allowing for a shared checkpoint directory and a list of
;;; alternates in addition to user's own, remove IRREG button, IRREG
;;; is no longer supported.
;;; 25-Oct-2004 I. Kalet remove POINTS button - points panel merged
;;; with volume editor.
;;;  1-Jun-2009 I. kalet remove ref to mini-images.  Filmstrip uses
;;; original images, not precomputed mini-images.
;;;

(in-package :prism)

;;;---------------------------------------------

(defvar *current-patient* nil "A reference to the current patient, to be
used strictly for debugging purposes.")

;;;---------------------------------------------

(defclass patient-panel (generic-panel)

  ((the-patient :initarg :the-patient
		:accessor the-patient
		:documentation "The patient that this panel edits.")

   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame containing all the
panel stuff.")

   (exit-b :accessor exit-b
	   :documentation "The Exit button.")

   (select-b :accessor select-b
	     :documentation "The new patient selection button.")

   (archive-b :accessor archive-b
	      :documentation "The archive button.")

   (retrieve-b :accessor retrieve-b
	       :documentation "The patient selection button for the
checkpoint database.")

   (ckpt-b :accessor ckpt-b
	   :documentation "The checkpoint database button.")

   (image-b :accessor image-b
	    :documentation "The image study selection/load button.")

   (immob-b :accessor immob-b
	    :documentation "The Immob. Device button")

   (tools-b :accessor tools-b
	    :documentation "The RTPT software tools button")

   (dbmgr-b :accessor dbmgr-b
	    :documentation "The database manager panel button")

   (srctable-b :accessor srctable-b
	       :documentation "The brachytherapy source table manager
panel button")

   (dvh-b   :accessor dvh-b
 	    :documentation "The DVH Panel button")

   (anatomy-b :accessor anatomy-b
	      :documentation "The button that brings up a volume
editor for organs, tumors, targets and points for this patient.")

   (name-box :accessor name-box
	     :documentation "The readout for the patient name and
hospital ID.  They are not changed via the patient panel.")

   (timestamp-box :accessor timestamp-box
		  :documentation "The readout for the date-entered
timestamp.")

   (comments-box :accessor comments-box
		 :documentation "The textbox containing the comments
text for this patient case.")

   (comments-btn :accessor comments-btn
		 :documentation "The Accept button for accepting the
text in the comments box, i.e., making an update to the patient
comments slot.")

   (busy :accessor busy
	 :initform nil
	 :documentation "The mediator busy bit for updates to
textlines.")

   (plan-selector :accessor plan-selector
		  :documentation "The selector panel listing the plans
for this patient.")

   (volume-editor-pan :accessor volume-editor-pan
		      :documentation "The volume-editor panel created 
from this panel.")

   (point-editor-pan :accessor point-editor-pan
                     :documentation "The 3d-point-editor panel created 
from this panel.")

   (db-pan :accessor db-pan
	   :documentation "The patient database manager panel")

   (dvh-pans :accessor dvh-pans
	     :documentation "A list of currently open DVH panels.")

   )

  )

;;;---------------------------------------

(defmethod (setf the-patient) :after (pat (pp patient-panel))

  "Sets the current patient to a global variable, for debugging only."

  (setq *current-patient* pat))

;;;---------------------------------------

(defun connect-pat-panel (pp)

  "connect-pat-panel pp

initializes the textlines and other patient specific stuff so a new
case is set up in the patient panel pp."

  (let* ((pan-fr (panel-frame pp))
	 (pp-win (sl:window pan-fr))
	 (width (sl:width pan-fr))
	 (height (sl:height pan-fr))
	 (sp-width 150) ;; width of plans selector panel
	 (sp-height 205) ;; height of " " - not the same everywhere
	 (p (the-patient pp)))
    (setf (sl:info (name-box pp)) (concatenate 'string
				    (format nil "~A" (patient-id p))
				    " " (name p) " " (hospital-id p))
	  (sl:info (timestamp-box pp)) (date-entered p)
	  (sl:border-color (timestamp-box pp)) 'sl:white
	  (sl:info (comments-box pp)) (comments p)
	  ;; the immob device string labels the corresp. button
	  (sl:label (immob-b pp)) (first (find (immob-device p)
					       *immob-devices*
					       :key #'second)))
    (ev:add-notify pp (new-date p)
		   #'(lambda (pan pt info)
		       (declare (ignore pt))
		       (setf (sl:info (timestamp-box pan)) info)
		       (setf (sl:border-color (timestamp-box pan))
			 'sl:red)))
    (ev:add-notify pp (new-comments p)
		   #'(lambda (pan pt info)
		       (declare (ignore pt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:info (comments-box pan)) info)
			 (setf (busy pan) nil))))
    (setf (plan-selector pp)
      (make-selector-panel sp-width sp-height
			   "Add a plan" (plans p)
			   #'make-plan
			   #'(lambda (pln) (make-plan-panel pln p))
			   :parent pp-win
			   :ulc-x (- width 10 sp-width)
			   :ulc-y (- height 10 sp-height)))))

;;;---------------------------------------

(defun replace-patient-case (pan &optional (database
					    *patient-database*))

  "replace-patient-case pan &optional (database *patient-database*)

replaces the patient in the patient panel pan with a new case selected
from the available ones in the specified database.  If no new patient
or case is selected the function makes no change in the panel.  If a
new case is selected the old case is discarded, and the panel is
reinitialized with the new case.  The patient list is either from
*patient-database* or generated based on the patients with entries in
the case index of the specified database.  The value of database
determines the source for the case data and case list."

  (when (or (/= (case-id (the-patient pan)) 0)
	    (= (patient-id (the-patient pan)) 0)
	    (sl:confirm ;; if case-id is 0, case not archived so warn
	     '("Current case not archived or checkpointed"
	       "Selecting a new case will destroy current data")))
    (let* ((match-string (or (sl:popup-textline
			      "" 300
			      :label "Match with: "
			      :title "Patient search string")
			     ""))
	   (pat-id (if (equal database *patient-database*)
		       (select-patient database match-string)
		     (select-patient-from-case-list *patient-database*
						    database match-string)))
	   (case-id (if pat-id (select-case pat-id database)))
	   (new-case (if case-id (get-case-data pat-id case-id
						database))))
      (when new-case ;; this includes case-id = 0, but not "Cancel"
	(let ((patient-entry (get-patient-entry pat-id
						*patient-database*)))
	  ;; use name, ids from patient index
	  (setf (name new-case) (second patient-entry)
		(hospital-id new-case) (third patient-entry)))
	(setf (sl:info (name-box pan)) "")
	(setf (sl:info (timestamp-box pan)) "")
	(setf (sl:info (comments-box pan)) '(""))
	(if (sl:on (anatomy-b pan)) (setf (sl:on (anatomy-b pan)) nil))

 	;; free X resources for the dvh-panels before removing the
 	;; elements in the plan-set to avoid doing a refresh of the
 	;; dvh plots for each plan that is removed
 	(dolist (dvhp (dvh-pans pan))
 	  (destroy dvhp))
 	(setf (dvh-pans pan) nil)

	(let ((plan-set (plans (the-patient pan))))
	  (dolist (pln (coll:elements plan-set)) ;; frees X resources
	    (coll:delete-element pln plan-set))) ;; of any views
	(destroy (plan-selector pan))
	(setf (the-patient pan) new-case)
	(connect-pat-panel pan)
	#+allegro (excl:gc t)
	))))

;;;---------------------------------------

(defun make-patient-panel (pat &rest initargs)

  "make-patient-panel pat &rest initargs

returns an instance of a patient panel for the patient pat."

  (setq *current-patient* pat)
  (apply #'make-instance 'patient-panel :the-patient pat initargs))

;;;---------------------------------------

(defmethod initialize-instance :after ((pp patient-panel)
				       &rest initargs)

  (let* ((box-width 440) ;; width of comments box, pat. name textline
	 (box-height 85) ;; height of comments box
	 (bth 30) ;; button and textline height
	 (btw 135) ;; button width
	 (dx 10) ;; left margin
	 (dx2 (+ dx btw 5)) ;; comments box and 2nd button column
	 (top-y 10) ;; y position of top readout, textline or button
	 ;; buttons other than EXIT are at mid-y
	 (mid-y (+ top-y (* 2 bth) box-height 20))
	 (pan-fr (apply #'sl:make-frame (+ box-width 20) 390 ;; 425
			:title (format nil "Prism RTP System ~A"
				       *prism-version-string*)
			initargs))
	 (pp-win (sl:window pan-fr))
	 ;; bp-y function defined in prism-objects - button-placement-y
	 (ex-b (apply #'sl:make-exit-button btw bth
		      :ulc-x dx :ulc-y (bp-y top-y bth 1)
		      :label "EXIT PRISM"
		      :confirm-exit "EXIT your Prism session?"
		      :parent pp-win initargs))
	 ;; buttons in the first column
	 (sel-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y mid-y
		       :label "Select"
		       :parent pp-win initargs))
	 (arc-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y mid-y bth 1)
		       :label "Archive"
		       :parent pp-win initargs))
	 (ret-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y mid-y bth 2)
		       :label "Retrieve"
		       :parent pp-win initargs))
	 (ckp-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y mid-y bth 3)
		       :label "Checkpt"
		       :parent pp-win initargs))
	 (db-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y mid-y bth 4)
		       :label "Pat DB mgr"
		       :parent pp-win initargs))
	 (src-b (apply #'sl:make-button btw bth
		       :ulc-x dx :ulc-y (bp-y mid-y bth 5)
		       :fg-color 'sl:red
		       :label "Brachy src mgr"
		       :parent pp-win initargs))
	 ;; buttons in second column
	 (cmt-b (apply #'sl:make-button btw bth
		       :ulc-x dx2 :ulc-y mid-y
		       :label "Accept cmts"
		       :parent pp-win initargs))
	 (im-b (apply #'sl:make-button btw bth
		      :ulc-x dx2 :ulc-y (bp-y mid-y bth 1)
		      :label "Image study"
		      :button-type :momentary
		      :parent pp-win initargs))
	 (vols-b (apply #'sl:make-button btw bth
			:ulc-x dx2 :ulc-y (bp-y mid-y bth 2)
			:label "Anatomy/points"
			:parent pp-win initargs))
	 (dvhist-b (apply #'sl:make-button btw bth
			  :ulc-x dx2 :ulc-y (bp-y mid-y bth 3)
			  :label "DVH"
			  :button-type :momentary
			  :parent pp-win initargs))
	 (immob-b (apply #'sl:make-button btw bth
			 :ulc-x dx2 :ulc-y (bp-y mid-y bth 4)
			 :parent pp-win initargs))
	 (tls-b (apply #'sl:make-button btw bth
		       :ulc-x dx2 :ulc-y (bp-y mid-y bth 5)
		       :label "Other Tools"
		       :parent pp-win initargs))
	 ;; readouts and textlines
	 (name-r (apply #'sl:make-readout box-width bth
			:ulc-x dx :ulc-y top-y
			:parent pp-win initargs))
	 (date-r (apply #'sl:make-readout 200 bth
			:ulc-x 250 :ulc-y (bp-y top-y bth 1)
			:parent pp-win initargs))
	 (comments-t (apply #'sl:make-textbox box-width box-height
			    :ulc-x dx :ulc-y (bp-y top-y bth 2)
			    :parent pp-win initargs)))
    (setf (panel-frame pp) pan-fr ;; put all the widgets in the slots
	  (name-box pp) name-r
	  (exit-b pp) ex-b
	  (timestamp-box pp) date-r
	  (comments-box pp) comments-t
	  (select-b pp) sel-b
	  (archive-b pp) arc-b
	  (retrieve-b pp) ret-b
	  (ckpt-b pp) ckp-b
	  (dbmgr-b pp) db-b
	  (srctable-b pp) src-b
	  (dvh-b pp) dvhist-b
	  (dvh-pans pp) nil
	  (comments-btn pp) cmt-b
	  (image-b pp) im-b
	  (anatomy-b pp) vols-b
	  (immob-b pp) immob-b
	  (tools-b pp) tls-b)
    (ev:add-notify pp (sl:new-info comments-t)
		   #'(lambda (pan tb)
		       (declare (ignore tb))
		       (unless (sl:on (comments-btn pan))
			 (setf (sl:on (comments-btn pan)) t))))
    (ev:add-notify pp (sl:button-on sel-b)
		   #'(lambda (panel button)
		       (replace-patient-case panel *patient-database*)
		       (setf (sl:on button) nil)))
    (ev:add-notify pp (sl:button-on arc-b)
		   #'(lambda (pan button)
		       (let ((pat (the-patient pan)))
			 (if (= (patient-id pat) 0)
			     (sl:acknowledge
			      '("No patient selected yet in this session"
				"You must first select a patient case"))
			   (if (put-case-data pat *patient-database*)
			       (progn (sl:acknowledge
				       "Case saved in archive")
				      (setf (sl:border-color
					     (timestamp-box pan))
					'sl:white))
			     (sl:acknowledge
			      '("Archive not possible"
				"No changes made to this case"
				"since last archive or checkpoint")))))
		       (setf (sl:on button) nil)))
    (ev:add-notify pp (sl:button-on ret-b)
		   #'(lambda (panel button)
		       ;; retrieve from anywhere: own, shared or others
		       (let* ((items (cons (list "My own storage"
						 *local-database*)
					   (cons (list "Shared storage"
						       *shared-database*)
						 *other-databases*)))
			      (sel (sl:popup-menu
				    (mapcar #'first items)
				    :default 0 ;; first item
				    :title "Checkpoint database")))
			 (when sel
			   (replace-patient-case panel
						 (second (nth sel items)))))
		       (setf (sl:on button) nil)))
    (ev:add-notify pp (sl:button-on ckp-b)
		   #'(lambda (pan button)
		       (let ((pat (the-patient pan)))
			 (if (= (patient-id pat) 0)
			     (sl:acknowledge
			      '("No patient selected yet in this session"
				"You must first select a patient case"))
			   ;; checkpoint only to own or shared, not others
			   (let* ((items (list (list "My own storage"
						     *local-database*)
					       (list "Shared storage"
						     *shared-database*)))
				  (sel (sl:popup-menu
					(mapcar #'first items)
					:default 0 ;; first item
					:title "Checkpoint database")))
			     (when sel
			       (if (put-case-data pat (second (nth sel items)))
				   (progn
				     (sl:acknowledge
				      "Case saved in checkpoint area")
				     (setf (sl:border-color
					    (timestamp-box pan))
				       'sl:white))
				 (sl:acknowledge
				  '("Checkpoint not possible"
				    "No changes made to this case"
				    "since last archive or checkpoint")))))))
		       (setf (sl:on button) nil)))
    (ev:add-notify pp (sl:button-on db-b)
		   #'(lambda (pan btn)
		       (let ((dbp (make-patdb-panel)))
			 (setf (db-pan pan) dbp)
			 (ev:add-notify pan (deleted dbp)
					#'(lambda (pn dp)
					    (ev:remove-notify pn
							      (deleted dp))
					    (setf (db-pan pn) nil)
					    (when (not (busy pn))
					      (setf (busy pn) t)
					      (setf (sl:on btn) nil)
					      (setf (busy pn) nil)))))))
    (ev:add-notify pp (sl:button-off db-b)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
                       (when (not (busy pan))
                         (setf (busy pan) t)
                         (destroy (db-pan pan))
                         (setf (busy pan) nil))))
    (ev:add-notify pp (sl:button-on src-b)
		   #'(lambda (pan bt)
		       (declare (ignore pan))
		       (brachy-table-manager)
		       (setf (sl:on bt) nil)))
    (ev:add-notify pp (sl:button-off cmt-b)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
		       (setf (comments (the-patient pan))
			 (sl:info (comments-box pan)))))
    (ev:add-notify pp (sl:button-on im-b)
		   #'(lambda (pan btn)
		       (let* ((pat (the-patient pan))
			      (im-id (image-set-id pat))
			      (pat-id (patient-id pat)))
			 (if (> im-id 0) ;; image set was already selected
			     (if (not (image-set pat)) ;; so load it
				 (setf (image-set pat)
				   (get-image-set pat-id im-id
						  *image-database*)))
			   ;; otherwise list studies and select one
			   (let ((new-im-id (select-image-set
					     pat-id
					     *image-database*)))
			     (when new-im-id
			       (setf (image-set-id pat) new-im-id)
			       (setf (image-set pat)
				 (get-image-set pat-id
						new-im-id
						*image-database*))))))
		       (setf (sl:on btn) nil)))
    (ev:add-notify pp (sl:button-on dvhist-b)
 		   #'(lambda (pan btn)
 		       (if (= (patient-id (the-patient pan)) 0)
 			   (sl:acknowledge
 			    '("No patient selected yet in this session"
 			      "You must first select a patient case"))
			 ;;if there is an active patient...
 			 (let* ((curpat (the-patient pan))
 				(oblist (append
 					 (coll:elements (anatomy curpat))
 					 (coll:elements (targets curpat))
 					 (coll:elements (findings curpat))))
 				(selection (sl:popup-menu
					    (mapcar #'name oblist))))
 			   (when selection
 			     (let ((newpan (make-instance 'dvh-panel
					     :object (nth selection oblist)
					     :plan-coll (plans curpat)
					     :the-patient curpat)))
 			       (push newpan (dvh-pans pan))
 			       (ev:add-notify
				pan (sl:button-on (del-pan-b newpan))
				#'(lambda (pp btn)
				    (declare (ignore btn))
				    (format t "remove panel...~%")
				    (format t "length dvh-pans = ~s~%"
					    (length (dvh-pans pp)))
				    (setf (dvh-pans pp)
				      (remove newpan
					      (dvh-pans pp)))
				    (format t "length dvh-pans = ~s~%"
					    (length (dvh-pans pp)))))
 			       (format t "dvh-pans = ~s~%" (dvh-pans pan))
 			       ))))
 		       (setf (sl:on btn) nil)))
    (ev:add-notify pp (sl:button-on vols-b)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
		       (let* ((pat (the-patient pan))
			      (ved (make-volume-editor
				    :width (+ *easel-size* 320)
				    :images (image-set pat)
				    :immob-dev (immob-device pat)
				    :organ-coll (anatomy pat)
				    :tumor-coll (findings pat)
				    :target-coll (targets pat)
				    :point-coll (points pat))))
			 (setf (volume-editor-pan pan) ved)
			 (ev:add-notify ved (new-immob-dev pat)
					#'(lambda (ve pat new-immob)
					    (declare (ignore pat))
					    (setf (immob-dev ve)
					      new-immob)))
                         (ev:add-notify pan (deleted ved)
					#'(lambda (pn ve)
					    (ev:remove-notify
					     ve (new-immob-dev
						 (the-patient pn)))
					    (ev:remove-notify pn
							      (deleted ve))
					    (setf (volume-editor-pan pn)
					      nil)
					    (when (not (busy pn))
					      (setf (busy pn) t)
					      (setf (sl:on (anatomy-b pn))
						nil)
					      (setf (busy pn) nil)))))))
    (ev:add-notify pp (sl:button-off vols-b)
		   #'(lambda (pan btn)
		       (declare (ignore btn))
                       (when (not (busy pan))
                         (setf (busy pan) t)
                         (destroy (volume-editor-pan pan))
                         (setf (busy pan) nil))))
    (ev:add-notify pp (sl:button-on immob-b)
		   #'(lambda (pan btn)
		       (let* ((items (mapcar #'first *immob-devices*))
			      (item-no (sl:popup-menu items)))
			 (if item-no ;; could be nil - no selection
			     (let ((selection (nth item-no
						   *immob-devices*)))
			       (setf (immob-device (the-patient pan))
				 (second selection))
			       (setf (sl:label btn) (first selection))))
			 (setf (sl:on btn) nil))))
    (ev:add-notify pp (sl:button-on tls-b)
		   #'(lambda (pan btn)
		       (tools-panel (the-patient pan))
		       (setf (sl:on btn) nil)))
    (connect-pat-panel pp)))

;;;-----------------------------------------

(defmethod destroy :before ((pp patient-panel))

  "releases X resources used by this panel and its children."

  (dolist (dvhp (dvh-pans pp))
    (destroy dvhp))
  (sl:destroy (dvh-b pp))
  (sl:destroy (exit-b pp))
  (sl:destroy (select-b pp))
  (sl:destroy (archive-b pp))
  (sl:destroy (retrieve-b pp))
  (sl:destroy (ckpt-b pp))
  (sl:destroy (image-b pp))
  (sl:destroy (immob-b pp))
  (sl:destroy (tools-b pp))
  (if (sl:on (anatomy-b pp)) (setf (sl:on (anatomy-b pp)) nil))
  (sl:destroy (anatomy-b pp))
  (sl:destroy (name-box pp))
  (sl:destroy (timestamp-box pp))
  (sl:destroy (comments-box pp))
  (sl:destroy (comments-btn pp))
  (destroy (plan-selector pp))
  (sl:destroy (panel-frame pp))
  (let ((pat (the-patient pp)))
    (ev:remove-notify pp (new-date pat))
    (ev:remove-notify pp (new-comments pat))
    (ev:remove-notify pp (new-immob-dev pat))))

;;;-----------------------------------------
