;;;
;;; beam-dose
;;;
;;; The external Photon and Neutron beam dose computation functions
;;;
;;;  2-Jan-1997 I. Kalet started, based on work by Gavin Young
;;; 16-Jan-1997 I. Kalet define functions for both grid and points,
;;;   called by new version of dosecomp module.
;;; 21-Mar-1997 I. Kalet continuing work...
;;; 21-Jun-1997 BobGian posting progress-report version - more to do.
;;;  3-Jul-1997 BobGian update NEARLY-xxx -> poly:NEARLY-xxx .
;;; 11-Aug-1997 BobGian integrate separately written beam-dose calculation
;;;   code into this file with proper interface conventions.
;;; 21-Aug-1997 BobGian flush NEARLY-EQUAL.  Used only for wedge angles,
;;;   which are maintained by Prism system to be exactly one value from the
;;;   set {0.0, 90.0, 180.0, 270.0} and therefore exact equality works.
;;; 25-Aug-1997 BobGian change #.(expression (coerce PI 'SINGLE-FLOAT))
;;;                          to #.(coerce (expression PI))
;;;  3-Sep-1997 BobGian completed and began testing.
;;;  7-Sep-1997 BobGian move clipping code to pathlength.
;;; 22-Sep-1997 BobGian made BEAM-DOSE return 0.0 if dosepoint is outside pt.
;;;  7-Oct-1997 BobGian move CONTOUR-ENCLOSES-P to POLYGONS package.
;;; 25-Oct-1997 BobGian remodel lookup fcns for WEDGE-INFO objects.
;;; 28-Oct-1997 BobGian dose not scaled by TRAY-FACTOR unless blocks used.
;;; 30-Oct-1997 BobGian COMPUTE-BEAM-DOSE returns T on success,
;;;   NIL if result is not valid.
;;;  2-Nov-1997 BobGian Depth of Isocenter below surface now computes
;;;   as negative value if SSD > SAD [isocenter between source and patient].
;;;  9-Nov-1997 BobGian BLOCK-FACTOR broken - rewrite sector integration
;;;   for it, OUTPUTFACTOR-COL (MLC method), and MLC-OCR-FACTOR.
;;; 10-Nov-1997 BobGian add decls (THE) for speedup.
;;;  7-Jan-1998 BobGian change sector integration from min 10 segs and max
;;;   5.0 degrees/segment to min 1 seg and max 10.0 degrees/segment.
;;; 22-Jan-1998 BobGian update to major revision including LABELS-defined
;;;   local functions to avoid passing large arg lists, argument-vector for
;;;   passing flonums to avoid flonum boxing, and array declarations to
;;;   inline array accesses and avoid flonum boxing.  GRID results still
;;;   stored in ordinary SINGLE-FLOAT 3-D arrays, pending Franz patch
;;;   (ie, special arrays-of-arrays hack used only inside dosecalc, not
;;;   after results returned to rest of Prism).
;;; 09-Mar-1998 BobGian modest upgrade of dose-calc code.
;;; 13-Mar-1998 BobGian fix rotation of MLC portal as used for finding
;;;   bounding box in computing equivalent square.
;;; 22-May-1998 BobGian upgrade to latest version of dose calculation:
;;;    - Reparameterize Arg-Vec (using named slots) to make consistent
;;;        with other users - in PATHLENGTH and clipping code.
;;;    - Optimize PATHLENGTH prologue before main loop is entered.
;;;    - Special-case and inline calculation of portal fieldwidth
;;;        instead of using generic function.
;;;    - Convert clipping code to use Arg-Vec instead of ordinary
;;;        argument-passing conventions.
;;;    - Simplify printing on background window during grid calcs.
;;;    - Contour-containment checked using ENCLOSES? (in "pathlength.cl")
;;;        rather than CONTOUR-ENCLOSES-P (arg-passing consistency).
;;;    - Inline trigonometry using short series expansions for SIN and
;;;        ATAN (only in places where accuracy is not critical).
;;;    - Arg-Vec passed to LABELS-defined internal functions
;;;        (BEAM-DOSE and BLOCK-FACTOR) via lexical environment rather
;;;        than as explicit argument.
;;; 01-Jun-1998 BobGian simplify call to ENCLOSES? [zero-distance test
;;;   redundant because it is done inside ENCLOSES? anyway].
;;; 08-Jun-1998 BobGian minor update - PATHLENGTH consistency changes.
;;; 11-Jun-1998 BobGian Bug fix - raise threshold for degenerate sector
;;;   in block factor sector integration, add angle to test, and move
;;;   test slightly (to where angle is defined).
;;; 25-Jun-1998 BobGian fix OCR factor to use fanline ratio 2.0 when
;;;   rect coll is on CAX and dosepoint is in shadow region.
;;; 26-Jun-1998 BobGian pass ORGAN-DENSITY-ARRAY as array rather than
;;;   list - random-access faster.  (Needed by PATHLENGTH.)
;;; 17-Jul-1998 BobGian add Arc-Therapy - forgotten in original!!
;;;   Change of arguments to COMPUTE-BEAM-DOSE - factor patient descriptors
;;;   from COMPUTE-BEAM-DOSE to BUILD-PATIENT-STRUCTURES.
;;; 13-Aug-1998 BobGian PATHLENGTH returns "dosepoint-inside-patient-p"
;;;   flag (numerical value returned via Arg-Vec) so COMPUTE-BEAM-DOSE
;;;   can set dose outside patient to zero.
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization, clarify
;;;   comments about return value from COMPUTE-BEAM-DOSE).
;;; 08-Feb-2000 BobGian more cosmetic cleanup.
;;; 02-Mar-2000 BobGian add declarations and rename a few local vars
;;;   in COMPUTE-BEAM-DOSE for consistency with COMPUTE-ELECTRON-DOSE.
;;; 29-Jun-2000 BobGian cosmetics - comments, whitespace.
;;; 11-Aug-2000 BobGian remove debug printout accidently left in prev ver.
;;; 06-Sep-2000 BobGian fix BEAM-DOSE (when clipping blocks to portal)
;;;   to ignore blocks whose VERTICES list is empty.
;;; 02-Nov-2000 BobGian function name and argument order changes to make
;;;   consistent with new version of dose-calc used in electron code.
;;;   Also simplify termination condition for block-factor and MLC sector
;;;   integration routines.
;;; 30-May-2001 BobGian - change call interface between photon dose calc and
;;;   pathlength computation to be consistent with new factored scheme used
;;;   in electron dosecalc.  Wrap generic arithmetic with THE-declared types.
;;;   Other misc declarations and minor optimizations.  Move macro
;;;   definition MONUS to "dosecomp-decls".
;;; 03-Jun-2001 BobGian fix bug giving non-zero dose for point outside body.
;;; 22-Dec-2001 BobGian remove erroneous ERROR call when CAX ray misses pt.
;;; 15-Mar-2002 BobGian parameterize constants used for Pathlength calc.
;;; 15-Mar-2002 BobGian change "erroneous but OK" conditions to call
;;;   sl:ACKNOWLEDGE rather than ERROR.  Some conditions are continuable;
;;;   others abort dosecalc by immediately returning NIL.
;;; 15-Mar-2002 BobGian PATHLENGTH-RAYTRACE used for "ray out-of-body"
;;;   detection.  Former errors on this condition now return gracefully.
;;; 29-Apr-2002 BobGian PATHLENGTH-RAYTRACE cannot be used alone for
;;;   "ray out-of-body" detection, since it traces full length of normalizing
;;;   distance.  Must also integrate to dosepoint for correct test.
;;; 03-Jan-2003 BobGian:
;;;   Flush macros FAST-SIN and FAST-ATAN - not accurate enough.
;;;   Former arg to BEAM-DOSE now passed in Arg-Vector [it is a pass-through
;;;     to PATHLENGTH-INTEGRATE].
;;;   Update arg-passing and return-value-passing conventions for
;;;     PATHLENGTH-RAYTRACE and PATHLENGTH-INTEGRATE.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 29-Aug-2003 BobGian - remove obsolete version number in change log header.
;;; 12-Feb-2005 AMSimms - update SINGLE-FLOAT calls (an Allegro specific
;;;     coercion function) to use coerce explicitly
;;;  6-Jul-2007 I. Kalet replace a few more SINGLE-FLOAT calls that
;;; Andrew missed.
;;;

(in-package :prism)

;;; NB: In all below:
;;;
;;;   All flonums in Prism are SINGLE-FLOATs.
;;;
;;;   "Pair" means two-list rather than dotted-pair.
;;;
;;;   A "subcontour" is a contour resulting from the intersection of a portal
;;;   with a block contour.  In general, such an intersection may produce
;;;   zero, one, or more subcontours.  The general word CONTOUR denotes a
;;;   [non-closed] vertex list.  The specific word SUBCONTOUR denotes a
;;;   particular subtype of contour - namely, one of the contours resulting
;;;   from clipping a block contour to the portal contour.
;;;
;;;   All contours and subcontours are represented as a list of vertices,
;;;   each a "pair" [as above] of X,Y coordinates in the collimator system.
;;;   They are both OPEN contours; that is, the first element is NOT repeated
;;;   as the last - there is an implicit edge from last back to first.
;;;
;;;   All lists representing contours and subcontours list explicitly and
;;;    once only vertices in the contour.  There is an implied edge
;;;   closing the contour from the last back to the first vertex.  Block
;;;   CONTOURS can be traversed in either direction.  The clipping
;;;   code always generates Clipped Block SUBCONTOURS in the CCW direction.
;;;   This is not required by the sector-integration code but it is so
;;;   assumed because it eliminates need for checks in BLOCK-FACTOR.
;;;
;;;   Unless specifically indicated otherwise, all CONTOURS and SUBCONTOURS
;;;   are represented with vertices whose coordinates are in the COLLIMATOR
;;;   system and as projected to the ISOCENTER, not the DOSEPOINT plane.
;;;
;;;   Collimator-system coordinates XC, YC, and ZC of dosepoint in the tech
;;;   report are replaced in this code by XCI, YCI, XCD, YCD, and
;;;   ZCD to make clearer whether we mean dosepoint coordinates in the
;;;   collimator system as projected onto the isocenter plane or at the
;;;   dosepoint plane.  Using separate coordinates with scaling done once
;;;   only also avoids repeated rescalings throughout the code.
;;;
;;;   Coordinates XCI, YCI are X, Y coordinates [orthogonal to central axis]
;;;   of dosepoint in COLLIMATOR system projected to the ISOCENTER plane
;;;   [ie, the plane normal to the central axis].  There is no ZCI coordinate,
;;;   because ZC is the Z coord of dosepoint in the collimator system
;;;   [distance along central axis, with origin at isocenter], and in the
;;;   isocenter plane it would always equal ZERO.
;;;
;;;   Collimator-system coordinates of the portal are indicated as XCI-, XCI+,
;;;   YCI-, YCI+ [X,Y respectively, minimal or inf versus maximal or sup
;;;   respectively] as projected onto the isocenter plane.  Portal boundaries
;;;   are never projected onto the dosepoint plane.
;;;
;;;   Since collimator-jaw overcentering is not supported, we have that:
;;;      XCI+, YCI+ >= 0.0    and
;;;      XCI-, YCI- <= 0.0    always.
;;;
;;;   Coordinates XCD, YCD are X, Y coordinates of dosepoint in COLLIMATOR
;;;   system AT THE DOSEPOINT PLANE ["identity projection"].  ZCD is the Z
;;;   coord in collimator system of dosepoint - that is, distance along the
;;;   central axis from isocenter to dosepoint plane.
;;;
;;;   Patient-system coordinates of dosepoint are XP, YP, ZP.
;;;
;;;   File-wide abbreviations:
;;;     "DP" for "DosePoint".
;;;     "LU" for "Lookup" (as in "TPR table-lookup").

;;;=============================================================
;;; Main external photon beam dose calculation function.

(defun compute-beam-dose (bm bms pts gg organ-vertices-list organ-z-extents
			  organ-density-array &aux mach dosedata
			  (num-beams (length bms)))

  "compute-beam-dose bm bms pts gg organ-vertices-list
		     organ-z-extents organ-density-array

computes the dose to each point in PTS, a list of points (MARK objects),
and all points in the grid specified by GG, a GRID-GEOMETRY, for beam
BM, stores the doses in the points and/or grid attribute of the beam's
DOSE-RESULT.  One of PTS or GG should be NIL, the other non-NIL.
Rest of args describe patient's anatomy (beam-independent).
Returns T on success and NIL if unable to complete."

  ;; Enable all the table lookup functions to reference the beam's machine's
  ;; DOSE-INFO object [contents of machine's DOSE-DATA slot] as local variable
  ;; passed to accessor functions.  MACHINE of BM calls GET-THERAPY-MACHINE
  ;; which loads THERAPY-MACHINE object - including DOSE-INFO object in its
  ;; DOSE-DATA slot - from machine definition file if not already resident.
  (declare (type list bms pts organ-vertices-list organ-z-extents)
	   (type fixnum num-beams))

  (setq mach (machine bm)
	dosedata (dose-data mach))

  (prog ((sad (cal-distance mach))         ;Source-to-Isocenter Distance [SAD]
	 (rslt (result bm))                         ;Object holding result
	 (beam-name (name bm))
	 (beam-num (the fixnum (1+ (the fixnum (position bm bms :test #'eq)))))
	 (arc-sz (arc-size bm))        ;Total sweep - non-zero for Arc-Therapy
	 (num-arcs 0)                   ;Number of arc segments in Arc-Therapy
	 (arc-num 0)                    ;Current arc-segment evaluation number
	 (tpr-@-iso 0.0)                       ;TPR-AT-ISO for individual beam
	 (avg-tpr-@-iso 0.0)           ;Running avg TPR-AT-ISO for Arc-Therapy

	 ;; Weighting coefficient for averaging of dose and TPR-AT-ISO
	 ;; when doing Arc-Therapy - or unity for regular beam.
	 (arc-scale-factor 1.0)
	 (coll (collimator bm))                     ;Collimator object
	 (portal-vertices)                          ;Its portal
	 (outputfactor 0.0)
	 (cal*atten*trayfactor*of 0.0)              ;Product of factors
	 (dose-multiplier 0.0)                      ;Temporary factor

	 ;; Terms of the Patient-to-Collimator Transform.
	 (pct-r0 0.0) (pct-r1 0.0) (pct-r2 0.0)
	 (pct-r3 0.0) (pct-r4 0.0) (pct-r5 0.0)
	 (pct-r6 0.0) (pct-r7 0.0) (pct-r8 0.0)

	 (iso-xp (- (the single-float (couch-lateral bm)))) ;Isocenter coords
	 (iso-yp (- (the single-float (couch-height bm))))
	 (iso-zp (- (the single-float (couch-longitudinal bm))))
	 (src-xp 0.0) (src-yp 0.0) (src-zp 0.0)     ;Source coordinates

	 (ocr-vector (ocr-table-vector dosedata))   ;OCR tables
	 (ocr-fssmap (ocr-fss-mapper dosedata))
	 (ocr-fss-ar (ocr-fieldsizes dosedata))
	 (ocr-depmap (ocr-depth-mapper dosedata))
	 (ocr-dep-ar (ocr-depths dosedata))
	 (ocr-fanmap (ocr-fanline-mapper dosedata))
	 (ocr-fan-ar (ocr-fanlines dosedata))
	 (ocr-tbl-ar (ocr-table dosedata))

	 (tpr-vector (tpr-table-vector dosedata))   ;TPR tables
	 (tpr-fssmap (tpr-fss-mapper dosedata))
	 (tpr-fss-ar (tpr-fieldsizes dosedata))
	 (tpr-depmap (tpr-depth-mapper dosedata))
	 (tpr-dep-ar (tpr-depths dosedata))
	 (tpr-tbl-ar (tpr-table dosedata))

	 (tpr0-vector (tpr0-table-vector dosedata)) ;Zero-field TPR tables
	 (tpr0-depmap (tpr0-depth-mapper dosedata))
	 (tpr0-dep-ar (tpr0-depths dosedata))
	 (tpr0-tbl-ar (tpr0-table dosedata))

	 (spr-vector (spr-table-vector dosedata))   ;SPR tables
	 (spr-radmap (spr-radius-mapper dosedata))
	 (spr-rad-ar (spr-radii dosedata))
	 (spr-depmap (spr-depth-mapper dosedata))
	 (spr-dep-ar (spr-depths dosedata))
	 (spr-tbl-ar (spr-table dosedata))

	 (wedgedata) (wdg-rotation 0.0)             ;Wedge Descriptors
	 (wdg-vector) (wdg-depmap) (wdg-dep-ar)
	 (wdg-posmap) (wdg-pos-ar) (wdg-tbl-ar)
	 (wcaf-dep 0.0) (wcaf-fsz 0.0) (wcaf-con 0.0)  ;Wedge CAF Coefficients

	 (gan-rad (* (the single-float (gantry-angle bm))
		     #.(coerce (/ pi 180.0d0) 'single-float)))

	 (clipped-blocks '())                      ;Non-null if blocks present
	 (xci- 0.0) (xci+ 0.0) (yci- 0.0) (yci+ 0.0)    ;Portal boundaries
	 (wc 0.0)                                 ;Equiv-sq width at isocenter

	 (sin-t 0.0) (cos-t 0.0) (sin-g 0.0) (cos-g 0.0)
	 (sin-c 0.0) (cos-c 0.0) (iso-depth 0.0)

	 ;; ARG-VEC is SINGLE-FLOAT array with Argv-Size slots
	 ;; for passing args and returning results.
	 (arg-vec (make-array #.Argv-Size :element-type 'single-float)))
    ;;
    (declare (type single-float sad iso-xp iso-yp iso-zp wdg-rotation arc-sz
		   src-xp src-yp src-zp pct-r0 pct-r1 pct-r2 pct-r3 pct-r4
		   pct-r5 pct-r6 pct-r7 pct-r8 iso-depth wc arc-scale-factor
		   cal*atten*trayfactor*of gan-rad sin-t cos-t sin-g cos-g
		   sin-c cos-c outputfactor wcaf-dep wcaf-fsz wcaf-con xci-
		   xci+ yci- yci+ dose-multiplier tpr-@-iso avg-tpr-@-iso)
	     (type simple-base-string beam-name)
	     (type (simple-array t 1)
		   ocr-fssmap ocr-depmap ocr-fanmap tpr-fssmap tpr-depmap
		   tpr0-depmap spr-radmap spr-depmap ocr-tbl-ar tpr-tbl-ar
		   spr-tbl-ar)
	     (type (simple-array single-float (#.Argv-Size)) arg-vec)
	     (type (simple-array single-float (3)) tpr0-vector)
	     (type (simple-array single-float (6)) tpr-vector spr-vector)
	     (type (simple-array single-float (9)) ocr-vector)
	     (type (simple-array single-float 1)
		   ocr-fss-ar ocr-dep-ar ocr-fan-ar tpr-fss-ar tpr-dep-ar
		   tpr0-dep-ar tpr0-tbl-ar spr-rad-ar spr-dep-ar)
	     (type fixnum beam-num num-arcs arc-num))

    (when (consp pts)
      ;; This sets POINTS slot to list of zeros.  For Arc-Therapy, these values
      ;; will get incremented each iteration.  Note that we allocate this list
      ;; only ONCE and increment its elements with each iteration, ie, for each
      ;; sub-beam being integrated to get the entire arc.
      ;;
      ;; For a regular beam, these values will get replaced by calculated dose.
      ;; Only a single flonum box is wasted in initialization, because each
      ;; element of the list is a pointer to the SAME boxed flonum.
      (setf (points rslt) (make-list (length pts) :initial-element 0.0)))

    (when (> arc-sz 0.0)                            ;Ie, doing Arc-Therapy
      ;; Original ARC-SZ is size of entire arc in DEGREES.
      ;; Number of beam evaluations is one greater than NUM-ARCS [fencepost].
      (setq num-arcs (max (the fixnum (ceiling arc-sz 5.0)) 10))
      ;; New ARC-SZ is of gantry angle increment for each segment in RADIANS.
      (setq arc-sz (* (/ arc-sz (coerce num-arcs 'single-float))
		      #.(coerce (/ pi 180.0d0) 'single-float)))
      ;; ARC-SCALE-FACTOR weights zero-th and last terms in arc integration
      ;; by half as much as each of the "middle" terms.  This is necessary
      ;; to keep DOSE-MULTIPLIER in sync with ARC-SCALE-FACTOR.  Iteration
      ;; control conditional at end of loop doubles and halves value as needed.
      (setq arc-scale-factor (/ 0.5 (coerce num-arcs 'single-float))))

    (let ((wdg-object (wedge bm)))
      ;; FIND returns a WEDGE-INFO object for a wedge ID fitting a wedge
      ;; on the machine, or NIL for an ID of 0, NIL, or any other value.
      ;; Therefore, WEDGEDATA = NIL -> no wedge.
      (when (setq wedgedata (find (id wdg-object) (wedges mach) :key #'id))
	(setq wcaf-dep (caf-depth-coef wedgedata))  ;Wedge-CAF coefficients
	(setq wcaf-fsz (caf-fs-coef wedgedata))
	(setq wcaf-con (caf-constant wedgedata))
	(setq wdg-vector (profile-table-vector wedgedata))  ;Wedge tables
	(setq wdg-depmap (profile-depth-mapper wedgedata))
	(setq wdg-dep-ar (profile-depths wedgedata))
	(setq wdg-posmap (profile-position-mapper wedgedata))
	(setq wdg-pos-ar (profile-positions wedgedata))
	(setq wdg-tbl-ar (profile-table wedgedata))
	(when (typep (rotation wdg-object) 'single-float)
	  ;; WDG-ROTATION is meaningless if WEDGEDATA is NIL [no wedge].
	  ;; If WEDGEDATA is a WEDGE descriptor, ROTATION slot of WDG-OBJECT
	  ;; can be a SINGLE-FLOAT or might be NIL.  Must test.
	  (setq wdg-rotation (rotation wdg-object)))))

    ;; Terms of the Patient-to-Collimator transform, expanded inline
    ;; and cached since they are used in innermost loops.
    (let ((trn-rad (* (the single-float (couch-angle bm))
		      #.(coerce (/ pi 180.0d0) 'single-float)))
	  (col-rad (* (the single-float (collimator-angle bm))
		      #.(coerce (/ pi 180.0d0) 'single-float))))
      (declare (type single-float trn-rad col-rad))
      (setq sin-t (sin trn-rad)
	    cos-t (cos trn-rad)
	    sin-c (sin col-rad)
	    cos-c (cos col-rad)))

    ;; Will multiply in TRAYFACTOR [if /= 1.0] and OUTPUTFACTOR later.
    (setq cal*atten*trayfactor*of
	  (* (the single-float
	       (cal-factor dosedata))         ;cGy per MU at iso - usually 1.0
	     (the single-float
	       (atten-factor bm))))       ;Per-beam dosimetrist-provided atten

    ;; COLL-COORDS methods [one for each collimator type] return portal
    ;; vertices [must be non-empty list] and four place-holder zeros for an
    ;; MLC and return NIL [non-MLC flag] and four portal rectangular
    ;; coordinates for all rectangular collimators.  PORTAL-VERTICES
    ;; is used both to convey the MLC portal vertex list [an OPEN contour;
    ;; first elem NOT repeated as last, and in GANTRY space] and as a
    ;; multileaf/rectangular collimator flag.
    (multiple-value-setq (portal-vertices xci- xci+ yci- yci+)
	(coll-coords coll))

    (cond
      ((consp portal-vertices)
       ;; MLC Portal vertices must be transformed from Gantry-space to
       ;; Collimator-space, since they are defined with respect to the gantry
       ;; rather than rotating with the collimator.  Appropriate transformation
       ;; is the INVERSE of the collimator rotation - rotate by negative of
       ;; COLLIMATOR-ANGLE of BM.  Vertex coords are as projected onto the
       ;; isocenter plane, so Z component is zero and are AS SEEN BY THE
       ;; COLLIMATOR - that is, points which are defined with respect to
       ;; GANTRY space appear to rotate backwards in COLLIMATOR space as the
       ;; collimator rotates.  We use these portal vertices for two things:
       ;; MLC width WC derived from equivalent-square area, and MLC-OCR-Factor.
       (setq portal-vertices
	     (mapcar #'(lambda (vert)
			 (let ((xp (first vert))
			       (yp (second vert)))
			   (declare (type single-float xp yp))
			   (list (+ (* cos-c xp)    ;Portal-Vertex X-coord
				    (* sin-c yp))
				 (- (* cos-c yp)    ;Portal-Vertex Y-coord
				    (* sin-c xp)))))
	       portal-vertices))

       ;; WC defined by bounding box of MLC using 4A/P formula with
       ;; [inversely] rotated PORTAL-VERTICES.
       (let ((xlist (mapcar #'first portal-vertices))
	     (ylist (mapcar #'second portal-vertices)))
	 (let ((wid (- (the single-float (apply #'max xlist))
		       (the single-float (apply #'min xlist))))
	       (len (- (the single-float (apply #'max ylist))
		       (the single-float (apply #'min ylist)))))
	   (declare (type single-float wid len))
	   (setq wc (/ (* 2.0 wid len)
		       (+ wid len)))
	   (unless (> wc 0.0)
	     (error "COMPUTE-BEAM-DOSE [1] MLC WC (from 4A/P) = 0.0")))))

      ;; PORTAL-VERTICES = NIL -> rectangular coll -> blocking allowed.
      (t (let ((blk-list (coll:elements (blocks bm))))
	   (when (consp blk-list)
	     ;; Blocks actually used - multiply in TRAY-FACTOR from MACHINE
	     ;; object and call block-clipping function.  Note that we include
	     ;; TRAY-FACTOR even if no CLIPPED-BLOCKS are in the beam portal.
	     (setq cal*atten*trayfactor*of
		   (* cal*atten*trayfactor*of
		      (the single-float (tray-factor mach))))

	     ;; Load args to CLIP-BLOCKS [fixed for duration of call].
	     (setf (aref arg-vec #.Argv-Xci-) xci-)
	     (setf (aref arg-vec #.Argv-Xci+) xci+)
	     (setf (aref arg-vec #.Argv-Yci-) yci-)
	     (setf (aref arg-vec #.Argv-Yci+) yci+)

	     (do ((blk) (subcontours)
		  (blks blk-list (cdr blks)))
		 ((null blks))

	       ;; ALL CLIPPING IS DONE AT THE ISOCENTER PLANE because this
	       ;; function is called in a dosepoint-independent manner.
	       ;;
	       ;; Set CLIPPED-BLOCKS to a LIST of items, one for each block
	       ;; whose intersection with portal is non-empty.  Each item in
	       ;; list is a LIST consisting of the block object [needed by
	       ;; BLOCK-FACTOR] and subcontours representing intersection of
	       ;; the portal with a given block.  A block when clipped may
	       ;; yield zero, one, or more subcontours.
	       ;;
	       ;; Each subcontour is a list of vertices [CCW traversal],
	       ;; each a sublist of X and Y collimator coords at isocenter.

	       (setq blk (car blks))
	       (when (consp (setq subcontours (vertices blk)))
		 ;; Only clip block if it has vertices.
		 (setq subcontours (clip-blocks subcontours arg-vec))
		 (when (consp subcontours)
		   ;; Only save result if clipped sub-block is non-empty.
		   (push (cons blk subcontours) clipped-blocks))))))

	 ;; WC is defined by jaws of rectangular collimator using 4A/P formula
	 ;; and actual portal dimensions in collimator frame, rotated with the
	 ;; collimator.  COLL-WIDTH and COLL-LENGTH methods get
	 ;; portal dimensions for all rectangular collimator types.
	 (let ((wid (coll-width coll))
	       (len (coll-length coll)))
	   (declare (type single-float wid len))
	   (setq wc (/ (* 2.0 wid len)
		       (+ wid len)))
	   (unless (> wc 0.0)
	     (error "COMPUTE-BEAM-DOSE [2] VJC WC (from 4A/P) = 0.0")))))

    (setq outputfactor (outputfactor-col coll wc dosedata)
	  cal*atten*trayfactor*of (* cal*atten*trayfactor*of outputfactor))

    ARC-LOOP

    (format t "~&~%Computing ~A dose for beam ~S (~D of ~D~A).~%"
	    (if pts "points" "grid") beam-name beam-num num-beams
	    (if (= num-arcs 0)
		""
		(format nil ", Arc ~D of ~D" arc-num num-arcs)))

    (setq dose-multiplier (* cal*atten*trayfactor*of arc-scale-factor))

    (setq sin-g (sin gan-rad)
	  cos-g (cos gan-rad))

    (setq pct-r0 (+ (* cos-c cos-g cos-t)           ; r00
		    (* sin-c sin-t)))
    (setq pct-r1 (- (* cos-c sin-g)))               ; r01
    (setq pct-r2 (- (* cos-c cos-g sin-t)           ; r02
		    (* sin-c cos-t)))

    (setq pct-r3 (- (* cos-c sin-t)                 ; r10
		    (* sin-c cos-g cos-t)))
    (setq pct-r4 (* sin-c sin-g))                   ; r11
    (setq pct-r5 (- (+ (* sin-c cos-g sin-t)        ; r12
		       (* cos-c cos-t))))

    (setq pct-r6 (* sin-g cos-t))                   ; r20
    (setq pct-r7 cos-g)                             ; r21
    (setq pct-r8 (* sin-g sin-t))                   ; r22

    ;; Compute SRC coordinates by transforming SOURCE-TO-ISOCENTER
    ;; vector in collimator coords by COLL-TO-COUCH rotations.
    (setq src-xp (+ (* cos-t sin-g sad) iso-xp))
    (setq src-yp (+ (* cos-g sad) iso-yp))
    (setq src-zp (+ (* sin-t sin-g sad) iso-zp))

    ;; Load argument vector for call to PATHLENGTH-RAYTRACE.  Source coords
    ;; remain fixed for entire call to COMPUTE-BEAM-DOSE.  Only DP-X, DP-Y,
    ;; and DP-Z slots get reloaded as dosepoint changes from
    ;; one call to next of BEAM-DOSE and PATHLENGTH-RAYTRACE.
    (let ((scale-factor (/ #.Pathlength-Ray-Maxlength sad)))
      (declare (type single-float scale-factor))
      (setf (aref arg-vec #.Argv-Src-X) src-xp)
      (setf (aref arg-vec #.Argv-Src-Y) src-yp)
      (setf (aref arg-vec #.Argv-Src-Z) src-zp)
      (setf (aref arg-vec #.Argv-Dp-X)
	    (+ src-xp (* scale-factor (- iso-xp src-xp))))
      (setf (aref arg-vec #.Argv-Dp-Y)
	    (+ src-yp (* scale-factor (- iso-yp src-yp))))
      (setf (aref arg-vec #.Argv-Dp-Z)
	    (+ src-zp (* scale-factor (- iso-zp src-zp)))))

    ;; Find geometric distance from source to isocenter and to patient surface.
    (let ((ray-alphalist
	    (pathlength-raytrace arg-vec organ-vertices-list organ-z-extents)))
      (declare (type list ray-alphalist))
      (unless (consp ray-alphalist)
	(setf (ssd rslt) -1.0)
	(setf (tpr-at-iso rslt) -1.0)
	(sl:acknowledge
	  (format nil "Central-Axis is outside patient in beam ~S (~D of ~D)."
		  beam-name beam-num num-beams))
	(return-from compute-beam-dose nil))
      (setq iso-depth (- sad (the single-float (caar ray-alphalist)))))

    (when (and (> num-arcs 0)
	       (< iso-depth 0.0))
      ;; For Arc-Therapy the isocenter must be inside the patient
      ;; for all beams in the arc.  Set chart flag and punt if not.
      (setf (ssd rslt) -1.0)
      (setf (tpr-at-iso rslt) -1.0)
      (sl:acknowledge
	(format
	  nil
	  "Isocenter is outside patient in beam ~S (~D of ~D, Arc ~D of ~D)."
	  beam-name beam-num num-beams arc-num num-arcs))
      (return-from compute-beam-dose nil))

    (labels

      ((beam-dose
	 ( )

	 ;; Returns the dose in cGy/MU at point (XP, YP, ZP) in patient
	 ;; coordinates, or equivalently point (XCD, YCD, ZCD) in collimator
	 ;; coordinates, with wedge described by WEDGEDATA [none if NIL],
	 ;; according to equivalent pathlength through anatomy represented
	 ;; by the ORGAN-xxx lists, using precomputed parameters that are not
	 ;; dependent on the point location.  CLIPPED-BLOCKS is a list of
	 ;; lists, each a BEAM-BLOCK object followed by the subcontours
	 ;; produced by intersecting that block with the collimator's portal,
	 ;; all as projected to the isocenter plane.  Each subcontour
	 ;; is a CCW-traversed clipped block outline.  CCW-ness is essential.
	 ;;
	 ;; For rectangular collimators, XCI-, XCI+, YCI-, and YCI+ are portal
	 ;; coordinates in collimator system as projected onto isocenter plane
	 ;; [they don't change as collimator is rotated - they are properties
	 ;; of the COLLIMATOR, not of the GANTRY] and PORTAL-VERTICES is NIL.
	 ;;
	 ;; For MLCs, XCI- etc are dummy placeholders and PORTAL-VERTICES is
	 ;; the vertex list for the collimator - an open, non-empty contour.
	 ;; These vertices are properties of the PATIENT, not of the MLC leaf
	 ;; settings, and therefore they describe the portal as drawn on the
	 ;; anatomy rather than the leaf settings.  As the collimator rotates,
	 ;; the portal vertices remain fixed [in GANTRY coordinates] and are
	 ;; approximated by changing leaf settings.
	 ;;
	 ;; Functionality implemented here is specified in
	 ;; Prism Dose Computation Methods, Version 1.2 Technical Report.
	 ;;
	 ;; Names of variables in the body of this function should correspond
	 ;; pretty closely to the names in the TR.  See also TR Kalet et.al.
	 ;; Prism Implementation Report, version 1.2 [Except: SAD for F and
	 ;; XCI, XCD etc used for collimator coords - see comments above].

	 (let* ((xcd (aref arg-vec #.Argv-Xcd))
		(ycd (aref arg-vec #.Argv-Ycd))
		(m (- (the single-float (aref arg-vec #.Argv-Zcd))))
		(f+m (+ sad m))
		(divergence (/ f+m sad))
		(inv-divergence (/ sad f+m))
		(wd (* wc divergence))              ;Eq Sq Field Size at depth
		(dpth (+ m iso-depth))              ;Depth of DP along CAX
		(xci (* xcd inv-divergence))        ;DP proj onto isocenter
		(yci (* ycd inv-divergence))        ;DP proj onto isocenter
		;; Arguments for call to PATHLENGTH-RAYTRACE: XP, YP, and ZP
		;; are loaded by call to BEAM-DOSE.  Source coords loaded by
		;; initial call to PATHLENGTH-RAYTRACE before BEAM-DOSE-calling
		;; loop is entered.  No args need be loaded now.
		(ray-alphalist
		  (pathlength-raytrace arg-vec organ-vertices-list
				       organ-z-extents))
		(equiv-pl 0.0))

	   (declare (type single-float xcd ycd m f+m divergence
			  inv-divergence wd dpth xci yci equiv-pl)
		    (type list ray-alphalist))

	   ;; If RAY-ALPHALIST is non-NIL, ray intersects body and we can
	   ;; integrate.  If PATHLENGTH-INTEGRATE returns T, dosepoint is
	   ;; inside body.  If either condition fails, dosepoint is outside
	   ;; and we return zero dose.
	   (cond
	     ((and (consp ray-alphalist)
		   (pathlength-integrate arg-vec ray-alphalist
					 organ-density-array :Heterogeneous))
	      (setq equiv-pl (aref arg-vec #.Argv-Return-1))

	      ;; DPTH should be always positive for dosepoints inside patient.
	      ;; Model works only if DPTH >= 0.0; for consistency with Prism1
	      ;; model, DPTH < 0.0 is treated as = 0.0 .
	      (when (< dpth 0.0)
		(setq dpth 0.0))

	      (setf
		(aref arg-vec #.Argv-Return-0)
		(* inv-divergence                   ;Inverse-Square Factor
		   inv-divergence
		   ;; We dispatch on collimator type [via PORTAL-VERTICES]
		   ;; and presence of blocks in beam portal so as to do the
		   ;; fastest computation possible, with no run-time method
		   ;; dispatching, in this inner loop.
		   (the single-float
		     (cond
		       ((consp portal-vertices)     ;Multileaf Collimator
			;; Spec requires that an MLC must have a non-empty
			;; portal vertex list, enabling this arg to be used
			;; as a flag to dispatch on collimator type.
			(* (the single-float
			     (2d-lookup tpr-vector  ;TPR Lookup.
					wd dpth tpr-fss-ar tpr-dep-ar
					tpr-fssmap tpr-depmap tpr-tbl-ar))

			   (the single-float        ;MLC-OCR-Factor
			     (do ((v1-nodes portal-vertices (cdr v1-nodes))
				  (v1-node) (v2-nodes) (v2-node) (len-v1 0.0)
				  (len-v2 0.0) (v1x 0.0) (v1y 0.0) (v2x 0.0)
				  (v2y 0.0) (vjx 0.0) (vjy 0.0) (len-vj 0.0)
				  (perp-distance 0.0) (minrad 0.0))
				 ((null v1-nodes)

				  ;; Does portal enclose dosepoint?
				  (setf (aref arg-vec #.Argv-Enc-X) xci)
				  (setf (aref arg-vec #.Argv-Enc-Y) yci)
				  (unless (encloses? portal-vertices arg-vec)
				    ;; Distance POSITIVE inside and NEGATIVE
				    ;; outside portal.  Subtract neg MINRAD
				    ;; from WC giving fan-line ratio > 1.0 .
				    (setq minrad (- minrad)))

				  ;; Find fan-line ratio as fractional
				  ;; half-beamwidth from dosept to nearest pt
				  ;; on collimator portal, scaled to iso plane.
				  (3d-lookup        ;OCR Lookup.
				    ocr-vector
				    wc              ;Field-Width [full]
				    dpth            ;Surface -> dosept dist
				    ;; Fan-line ratio:
				    ;;   > 1.0 outside, < 1.0 inside portal.
				    (/ (- wc (* 2.0 minrad)) wc)
				    ocr-fss-ar ocr-dep-ar ocr-fan-ar ocr-fssmap
				    ocr-depmap ocr-fanmap ocr-tbl-ar))

			       (declare (type single-float v1x v1y v2x v2y
					      len-v1 len-v2 vjx vjy
					      perp-distance len-vj minrad))

			       ;; V1-NODE and V2-NODE are (X Y) coord pairs of
			       ;; vertex at head of V1 and V2 vectors.  V1X,
			       ;; V1Y, V2X, V2Y are X and Y coords of vectors
			       ;; V1 and V2 from dosepoint (XCI YCI) [projected
			       ;; on ISO plane] to verts V1-NODE and V2-NODE.
			       ;; VJ [variable not used] is vector from V1-NODE
			       ;; [vertex at tail] to V2-NODE [vertex at head].
			       ;; VJX and VJY are its X and Y coordinates.
			       (cond
				 ((eq v1-nodes portal-vertices)
				  ;; First time must compute everything. On
				  ;; successive iters we pass V2-values to V1.
				  (setq v1-node (car v1-nodes)
					v1x (- (the single-float
						 (first v1-node))
					       xci)
					v1y (- (the single-float
						 (second v1-node))
					       yci)
					len-v1 (sqrt (the (single-float 0.0 *)
						       (+ (* v1x v1x)
							  (* v1y v1y))))
					minrad len-v1))

				 (t (setq v1x v2x
					  v1y v2y
					  len-v1 len-v2)))

			       ;; PORTAL-VERTICES is an open CCW contour
			       ;; [first elem NOT repeated], so loop around
			       ;; to get last vertex.
			       (setq v2-nodes (or (cdr v1-nodes)
						  portal-vertices)
				     v2-node (car v2-nodes)
				     v2x (- (the single-float (first v2-node))
					    xci)
				     v2y (- (the single-float (second v2-node))
					    yci))

			       (setq len-v2 (sqrt (the (single-float 0.0 *)
						    (+ (* v2x v2x)
						       (* v2y v2y))))
				     vjx (- v2x v1x)
				     vjy (- v2y v1y)
				     len-vj (sqrt (the (single-float 0.0 *)
						    (+ (* vjx vjx)
						       (* vjy vjy)))))

			       (when (< len-v2 minrad)
				 (setq minrad len-v2))

			       (let ((v1-cross-vj (- (* v1x vjy)
						     (* v1y vjx))))
				 (declare (type single-float v1-cross-vj))
				 (setq perp-distance
				       (cond ((< len-vj 1.0e-5) len-v1)
					     ((< v1-cross-vj 0.0)
					      (/ (- v1-cross-vj) len-vj))
					     (t (/ v1-cross-vj len-vj)))))

			       (when (and (< (+ (* v1x vjx) ;V1-DOT-VJ
						(* v1y vjy))
					     0.0)
					  (> (+ (* v2x vjx) ;V2-DOT-VJ
						(* v2y vjy))
					     0.0)
					  (< perp-distance minrad))
				 (setq minrad perp-distance))))))

		       ;; Blocks in beam portal, and therefore must be a
		       ;; rectangular collimator.  Compute Block-Factor.
		       (t (monus
			    (* (the single-float
				 (2d-lookup tpr-vector  ;TPR Lookup.
					    wd dpth tpr-fss-ar tpr-dep-ar
					    tpr-fssmap tpr-depmap tpr-tbl-ar))

			       ;; Rectangular-Coll OCR Factor - X term.
			       (the single-float
				 (cond
				   ((>= xci 0.0)
				    ;; If XCI is positive, do OCR lookup with
				    ;; jaw on that side; full-width fan-line
				    ;; ratio is positive.
				    (3d-lookup      ;OCR Lookup.
				      ocr-vector (* xci+ 2.0) dpth
				      (cond ((and (= xci 0.0)   ;l'Hospital
						  (= xci+ 0.0))
					     1.0)
					    ;; Edge on CAX, pt beyond.
					    ((= xci+ 0.0)
					     2.0)
					    ;; Pt within portal ->
					    ;;   OCR fanline meaningful.
					    (t (/ xci xci+)))
				      ocr-fss-ar ocr-dep-ar ocr-fan-ar
				      ocr-fssmap ocr-depmap ocr-fanmap
				      ocr-tbl-ar))

				   ;; XCI < 0.0 -> use XCI- jaw.  XCI- is also
				   ;; negative [overcentering NOT ALLOWED], so
				   ;; multiplication by -2.0 makes full-width
				   ;; positive.  Dividing negative XCI by
				   ;; negative XCI- makes fan-line ratio
				   ;; positive too.
				   (t (3d-lookup    ;OCR Lookup.
					ocr-vector (* xci- -2.0) dpth
					;; XCI = 0.0 case excluded by COND
					;; one level up from this.  Edge on
					;; CAX, pt beyond.
					(cond ((= xci- 0.0)
					       2.0)
					      ;; Pt within portal.
					      (t (/ xci xci-)))
					ocr-fss-ar ocr-dep-ar ocr-fan-ar
					ocr-fssmap ocr-depmap ocr-fanmap
					ocr-tbl-ar))))

			       ;; Rectangular-Coll OCR Factor - Y term.  Same
			       ;; sign conventions apply: YCI, YCI-, and YCI+.
			       (the single-float
				 (cond
				   ((>= yci 0.0)
				    (3d-lookup      ;OCR Lookup.
				      ocr-vector (* yci+ 2.0) dpth
				      (cond ((and (= yci 0.0)   ;l'Hospital
						  (= yci+ 0.0))
					     1.0)
					    ;; Edge on CAX, pt beyond.
					    ((= yci+ 0.0)
					     2.0)
					    ;; Pt within portal.
					    (t (/ yci yci+)))
				      ocr-fss-ar ocr-dep-ar ocr-fan-ar
				      ocr-fssmap ocr-depmap ocr-fanmap
				      ocr-tbl-ar))

				   (t (3d-lookup    ;OCR Lookup.
					ocr-vector (* yci- -2.0) dpth
					;; YCI = 0.0 case excluded by COND
					;; one level up from this.
					;; Edge on CAX, pt beyond.
					(cond ((= yci- 0.0)
					       2.0)
					      ;; Pt within portal.
					      (t (/ yci yci-)))
					ocr-fss-ar ocr-dep-ar ocr-fan-ar
					ocr-fssmap ocr-depmap ocr-fanmap
					ocr-tbl-ar)))))

			    ;; Subtract Block-Factor if blocks present.
			    (cond ((consp clipped-blocks)
				   ;; Load args to BLOCK-FACTOR.
				   (setf (aref arg-vec #.Argv-Xci) xci)
				   (setf (aref arg-vec #.Argv-Yci) yci)
				   (setf (aref arg-vec #.Argv-Depth) dpth)
				   (setf (aref arg-vec #.Argv-Div) divergence)
				   (block-factor)
				   (aref arg-vec #.Argv-Return-0))
				  (t 0.0))))))

		   ;; INHOMOGENEITY Factor
		   (the single-float
		     (/ (the single-float
			  (2d-lookup tpr-vector     ;TPR Lookup.
				     wd equiv-pl tpr-fss-ar tpr-dep-ar
				     tpr-fssmap tpr-depmap tpr-tbl-ar))

			(the single-float
			  (2d-lookup tpr-vector     ;TPR Lookup.
				     wd
				     ;; Slant-height, surface to DP
				     (/ (* dpth
					   (the (single-float 0.0 *)
					     (sqrt (the (single-float 0.0 *)
						     (+ (* xcd xcd)
							(* ycd ycd)
							(* f+m f+m))))))
					f+m)
				     tpr-fss-ar tpr-dep-ar tpr-fssmap
				     tpr-depmap tpr-tbl-ar))))

		   ;; WEDGE Factor.
		   (the single-float
		     (cond
		       ((null wedgedata)            ;No wedge
			1.0)                        ;Unity transmission

		       ;; Alina's formula for Wedge CAF.
		       (t (* (+ (* wcaf-dep dpth)   ;Depth dependence.
				(* wcaf-fsz wc)     ;Fieldsize dependence.
				wcaf-con)           ;Constant term.
			     (the single-float
			       (2d-lookup           ;Wedge Profile Lookup.
				 wdg-vector
				 dpth
				 ;; Equality OK because wedge angles
				 ;; are EXACTLY one of these.
				 (cond
				   ((= wdg-rotation 0.0) yci)
				   ((= wdg-rotation 90.0) (- xci))
				   ((= wdg-rotation 180.0) (- yci))
				   ((= wdg-rotation 270.0) xci)
				   (t (error
					"COMPUTE-BEAM-DOSE [3] Bad Wedge-Rot: ~S"
					wdg-rotation)))
				 wdg-dep-ar wdg-pos-ar wdg-depmap wdg-posmap
				 wdg-tbl-ar)))))))))

	     ;; Dosepoint outside patient's body - return zero dose.
	     (t (setf (aref arg-vec #.Argv-Return-0) 0.0))))

	 ;; Return NIL so no flonum box need be allocated.
	 nil)

       (block-factor
	 (&aux (opacity 0.0) (accum 0.0)
	       (xci (aref arg-vec #.Argv-Xci))
	       (yci (aref arg-vec #.Argv-Yci))
	       (dpth (aref arg-vec #.Argv-Depth))
	       (divergence (aref arg-vec #.Argv-Div)))

	 ;; Returns summed block factor for rectangular collimator at dosepoint
	 ;; (XCI YCI), at depth DPTH, using DIVERGENCE factor from isocenter
	 ;; plane to dosepoint plane.
	 (declare (type single-float xci yci dpth divergence opacity accum))

	 (dolist (blk clipped-blocks)

	   ;; Sector-Integration routine.
	   ;;
	   ;; Note that a single block may give rise to multiple sublists
	   ;; [subcontours] in that block's element in CLIPPED-BLOCKS, because
	   ;; clipping of a block contour to the portal can produce
	   ;; more than one disjoint clipped subcontours.
	   ;;
	   ;; Those blocks totally outside the portal, of course, give rise
	   ;; to no clipped contours, and therefore entries corresponding
	   ;; to them are absent in CLIPPED-BLOCKS.
	   ;;
	   ;; NB: ALL computations done in sector integration are projected
	   ;; onto ISOCENTER plane, with sole exception of the radial argument
	   ;; used in the SPR lookup, which is scaled by DIVERGENCE
	   ;; to be projected onto the DOSEPOINT plane.
	   ;;
	   ;; (CAR BLK) is the beam block object.
	   (setq opacity (- 1.0 (the single-float (transmission (car blk)))))

	   ;; (CDR BLK) is list of subcontours [each CCW] for clipped block.
	   (dolist (subcontour (cdr blk))

	     (let ((minrad 0.0)
		   (block-scatter 0.0))

	       (declare (type single-float minrad block-scatter))

	       (do ((v1-nodes subcontour (cdr v1-nodes))
		    (v1-node) (v2-nodes) (v2-node) (len-v1 0.0) (len-v2 0.0)
		    (v1x 0.0) (v1y 0.0) (v2x 0.0) (v2y 0.0) (num-sectors 0)
		    (vjx 0.0) (vjy 0.0) (len-vj 0.0) (v1-cross-vj 0.0)
		    (v1-dot-vj 0.0) (perp-distance 0.0) (theta-j 0.0)
		    (theta-per-sector 0.0))
		   ((null v1-nodes))

		 (declare (type single-float v1x v1y v2x v2y len-v1 len-v2
				theta-j vjx vjy theta-per-sector v1-cross-vj
				len-vj v1-dot-vj perp-distance)
			  (type fixnum num-sectors))

		 ;; V1-NODE and V2-NODE are (X Y) coord pairs of the vertex
		 ;; at head of V1 and V2 vectors, respectively.  V1X, V1Y, V2X,
		 ;; V2Y are X and Y coords of vectors V1 and V2 from dosepoint
		 ;; (XCI YCI) to vertices V1-NODE and V2-NODE.  VJ [not used]
		 ;; is vector from V1-NODE [vertex at tail] to V2-NODE [vertex
		 ;; at head].  VJX and VJY are its X and Y coordinates.
		 (cond ((eq v1-nodes subcontour)
			;; First time must compute everything.  On successive
			;; iterations we can pass V2-values back to V1.
			(setq v1-node (car v1-nodes)
			      v1x (- (the single-float (first v1-node)) xci)
			      v1y (- (the single-float (second v1-node)) yci)
			      len-v1 (sqrt (the (single-float 0.0 *)
					     (+ (* v1x v1x)
						(* v1y v1y))))
			      minrad len-v1))
		       (t (setq v1x v2x
				v1y v2y
				len-v1 len-v2)))

		 ;; SUBCONTOUR is an open CCW contour - first element NOT
		 ;; repeated.  Loop back to get closing last element.
		 (setq v2-nodes (or (cdr v1-nodes) subcontour)
		       v2-node (car v2-nodes)
		       v2x (- (the single-float (first v2-node)) xci)
		       v2y (- (the single-float (second v2-node)) yci))

		 (setq len-v2 (sqrt (the (single-float 0.0 *)
				      (+ (* v2x v2x)
					 (* v2y v2y))))
		       vjx (- v2x v1x)
		       vjy (- v2y v1y)
		       len-vj (sqrt (the (single-float 0.0 *)
				      (+ (* vjx vjx)
					 (* vjy vjy))))
		       v1-cross-vj (- (* v1x vjy)
				      (* v1y vjx))
		       v1-dot-vj (+ (* v1x vjx)
				    (* v1y vjy)))

		 (when (< len-v2 minrad)
		   (setq minrad len-v2))

		 (setq perp-distance (cond ((< len-vj 1.0e-5) len-v1)
					   ((< v1-cross-vj 0.0)
					    (/ (- v1-cross-vj) len-vj))
					   (t (/ v1-cross-vj len-vj))))

		 (when (and (< v1-dot-vj 0.0)
			    (> (+ (* v2x vjx)       ;V2-DOT-VJ
				  (* v2y vjy))
			       0.0)
			    (< perp-distance minrad))
		   (setq minrad perp-distance))

		 ;; THETA-J and THETA-PER-SECTOR are always POSITIVE.
		 (setq theta-j (the single-float
				 (abs (the single-float
					(atan (- (* v1x v2y)    ;V1-CROSS-V2
						 (* v1y v2x))
					      (+ (* v1x v2x)    ;V1-DOT-V2
						 (* v1y v2y)))))))

		 ;; If segment is degenerate, the contribution of this sector
		 ;; to integral is zero.  Thresholds are experimental.
		 (unless (or (< len-v1 1.0e-5)
			     (< len-v2 1.0e-5)
			     (< len-vj 1.0e-5)
			     (< theta-j 1.0e-6)
			     (< perp-distance 1.0e-5))

		   ;; Experiment with the 1 and 10.0d0 here.  We currently
		   ;; use min of 1 sector per seg, each at most 10.0 degrees
		   ;; pie-width angle.
		   (setq num-sectors
			 (the fixnum
			   (ceiling theta-j #.(coerce (* pi (/ 10.0d0 180.0d0))
						      'single-float)))
			 theta-per-sector (/ theta-j
					     (coerce num-sectors
						     'single-float)))

		   (do ((psi (+ (- #.(coerce pi 'single-float)
				   (the single-float
				     (abs (the single-float
					    (atan v1-cross-vj v1-dot-vj)))))
				(* 0.5 theta-per-sector))
			     (+ psi theta-per-sector))
			(sector-scatter 0.0)
			(cnt num-sectors (the fixnum (1- cnt))))
		       ((= cnt 0)
			;; SECTOR-SCATTER is always non-negative; thus
			;; BLOCK-SCATTER should be INCREMENTED for CCW
			;; integration and DECREMENTED for CW integration.
			(when (< v1-cross-vj 0.0)
			  (setq theta-per-sector (- theta-per-sector)))
			(incf block-scatter
			      (* sector-scatter theta-per-sector)))

		     (declare (type single-float psi sector-scatter)
			      (type fixnum cnt))

		     ;; Radial argument for SPR lookup is as projected
		     ;; to DOSEPOINT plane; therefore, we scale radius
		     ;; by DIVERGENCE.
		     (incf sector-scatter ;SECTOR-SCATTER always non-negative.
			   (the single-float
			     (2d-lookup             ;SPR Lookup.
			       spr-vector
			       (* (/ perp-distance (sin psi)) divergence)
			       dpth spr-rad-ar spr-dep-ar spr-radmap
			       spr-depmap spr-tbl-ar))))))

	       ;; Normalize by 1/2*PI and inline ABS; BLOCK-SCATTER is
	       ;; always non-negative but result of sector integration may
	       ;; be negative if integration proceeded in CW orientation.
	       ;; BLOCK-SCATTER should always be positive.
	       (setq block-scatter (* #.(coerce (/ 1.0d0 (* 2.0d0 pi))
						'single-float)
				      (if (>= block-scatter 0.0)
					  block-scatter
					  (- block-scatter))))

	       ;; Does closest subcontour enclose dosepoint?  Don't test if
	       ;; MINRAD is "near" 0.0 - meaningless.
	       (setf (aref arg-vec #.Argv-Enc-X) xci)
	       (setf (aref arg-vec #.Argv-Enc-Y) yci)
	       (unless (encloses? subcontour arg-vec)
		 ;; Radius is POSITIVE inside and NEGATIVE outside subcontour.
		 (setq minrad (- minrad)))

	       (let* ((x-edge (cond ((> xci 0.0) xci+)  ;Use upper jaw.
				    ((< xci 0.0) xci-)  ;Use lower jaw.
				    ;; DP on axis and upper jaw closer.
				    ((< xci+ (- xci-)) xci+)
				    (t xci-)))      ;Use lower jaw.
		      ;; Use same procedure to choose closer jaw in Y direc.
		      (y-edge (cond ((> yci 0.0) yci+)
				    ((< yci 0.0) yci-)
				    ((< yci+ (- yci-))
				     yci+)
				    (t yci-)))
		      ;; Now choose jaw closer to DP.
		      (x-dist (the single-float (abs (- xci x-edge))))
		      (y-dist (the single-float (abs (- yci y-edge))))
		      ;; WN is the field-size HALF-WIDTH, ie, the distance
		      ;; from central axis to collimator jaw on same side as
		      ;; dosepoint, using whichever jaw is closer, or average
		      ;; distance if dosepoint is equidistant from both jaws.
		      (wn (cond ((< x-dist y-dist)
				 ;; X- or X+ jaw closer - use closer X jaw.
				 (the single-float (abs x-edge)))
				((< y-dist x-dist)
				 ;; Y- or Y+ jaw closer - use closer Y jaw.
				 (the single-float (abs y-edge)))
				(t (* 0.5     ;No diff - use average distance.
				      (+ (the single-float (abs x-edge))
					 (the single-float (abs y-edge))))))))

		 (declare (type single-float x-edge y-edge x-dist y-dist wn))

		 ;; NB: We use a separate table for TPR at zero field size
		 ;; because the TPR0 table is based on circular fields and
		 ;; the TPR table is based on square fields.
		 ;;
		 ;; If dosepoint is more than 1/10 half-width OUTSIDE block
		 ;; shadow, use SCATTER component for PRIMARY.  [See AA below.]
		 ;;
		 ;; Otherwise approximate PRIMARY component by treating block
		 ;; edge as a virtual collimator edge and do appropriate OCR
		 ;; lookup with WN to define field width and fan line.
		 (incf accum
		       (the single-float
			 (* opacity
			    (+ (* (the single-float
				    (1d-lookup      ;TPR0 Lookup
				      tpr0-vector dpth tpr0-dep-ar
				      tpr0-depmap tpr0-tbl-ar))
				  (cond ((< minrad (* -0.1 wn))
					 ;; See note AA above.
					 block-scatter)
					;; See note BB above.
					(t (the single-float
					     (3d-lookup ;OCR Lookup.
					       ocr-vector (* 2.0 wn) dpth
					       (cond ((= wn 0.0)
						      1.0)
						     (t (/ (- wn minrad) wn)))
					       ocr-fss-ar ocr-dep-ar ocr-fan-ar
					       ocr-fssmap ocr-depmap ocr-fanmap
					       ocr-tbl-ar)))))
			       ;; SCATTER component from sector integration.
			       block-scatter))))))))

	 ;; Pass return value in ARG-VEC.
	 (setf (aref arg-vec #.Argv-Return-0) accum)

	 ;; Return NIL so no flonum box need be allocated.
	 nil))

      ;; End of LABELS internal function definitions.

      (cond
	((consp pts)      ;Compute either Point doses or Grid doses, not both.
	 (do ((input-pts pts (cdr input-pts))
	      (output-pts (points rslt) (cdr output-pts))
	      (pt))
	     ((null input-pts))
	   (setq pt (car input-pts))                ;PT is a MARK object.
	   (let ((xp (x pt))
		 (yp (y pt))
		 (zp (z pt)))
	     (declare (type single-float xp yp zp))
	     (let ((xpi (- xp iso-xp))
		   (ypi (- yp iso-yp))
		   (zpi (- zp iso-zp)))
	       (declare (type single-float xpi ypi zpi))
	       (let ((scale-factor
		       (/ #.Pathlength-Ray-Maxlength
			  (setf (aref arg-vec #.Argv-Raylen)
				(3d-distance src-xp src-yp src-zp xp yp zp)))))
		 (declare (type single-float scale-factor))
		 (setf (aref arg-vec #.Argv-Dp-X)
		       (+ src-xp (* scale-factor (- xp src-xp))))
		 (setf (aref arg-vec #.Argv-Dp-Y)
		       (+ src-yp (* scale-factor (- yp src-yp))))
		 (setf (aref arg-vec #.Argv-Dp-Z)
		       (+ src-zp (* scale-factor (- zp src-zp))))
		 (setf (aref arg-vec #.Argv-Xcd)
		       (+ (* pct-r0 xpi)
			  (* pct-r1 ypi)
			  (* pct-r2 zpi)))
		 (setf (aref arg-vec #.Argv-Ycd)
		       (+ (* pct-r3 xpi)
			  (* pct-r4 ypi)
			  (* pct-r5 zpi)))
		 (setf (aref arg-vec #.Argv-Zcd)
		       (+ (* pct-r6 xpi)
			  (* pct-r7 ypi)
			  (* pct-r8 zpi)))

		 (beam-dose)

		 (cond ((= num-arcs 0)              ;Regular Beam.
			(setf (car output-pts)
			      (* dose-multiplier
				 (the single-float
				   (aref arg-vec #.Argv-Return-0)))))
		       (t (incf (the single-float (car output-pts)) ;Arc-Th.
				(* dose-multiplier
				   (the single-float
				     (aref arg-vec #.Argv-Return-0)))))))))))

	(t (let* ((nx (x-dim gg))
		  (ny (y-dim gg))
		  (nz (z-dim gg))
		  (xp-step (/ (the single-float (x-size gg))
			      (coerce (the fixnum (1- nx)) 'single-float)))
		  (yp-step (/ (the single-float (y-size gg))
			      (coerce (the fixnum (1- ny)) 'single-float)))
		  (zp-step (/ (the single-float (z-size gg))
			      (coerce (the fixnum (1- nz)) 'single-float)))
		  (dose-array (grid rslt)))     ;Use pre-made, pre-sized array

	     (declare (type single-float xp-step yp-step zp-step)
		      (type (simple-array single-float 3) dose-array)
		      (type fixnum nx ny nz))

	     (when (and (> num-arcs 0)              ;Doing Arc-Th.
			(= arc-num 0))              ;Zero-th iter.
	       ;; Arc-Therapy: must initialize DOSE-ARRAY and accumulate dose,
	       ;; Initialize ONLY on zero-th iteration and when doing Arc-Th.
	       (do ((x-idx 0 (the fixnum (1+ x-idx))))
		   ((= x-idx nx))
		 (declare (type fixnum x-idx))
		 (do ((y-idx 0 (the fixnum (1+ y-idx))))
		     ((= y-idx ny))
		   (declare (type fixnum y-idx))
		   (do ((z-idx 0 (the fixnum (1+ z-idx))))
		       ((= z-idx nz))
		     (declare (type fixnum z-idx))
		     (setf (aref dose-array x-idx y-idx z-idx) 0.0)))))

	     (do ((x-idx 0 (the fixnum (1+ x-idx)))
		  (xp (x-origin gg) (+ xp xp-step))
		  (y-orig (y-origin gg))
		  (z-orig (z-origin gg))
		  (xpi 0.0) (ypi 0.0) (zpi 0.0)
		  (xpi-r0 0.0) (xpi-r3 0.0) (xpi-r6 0.0)
		  (ypi-r1 0.0) (ypi-r4 0.0) (ypi-r7 0.0))
		 ((= x-idx nx))

	       (declare (type single-float xp xpi ypi zpi xpi-r0 xpi-r3
			      xpi-r6 ypi-r1 ypi-r4 ypi-r7 y-orig z-orig)
			(type fixnum x-idx))

	       ;; Progress report every outermost iteration.  For Arc-Therapy,
	       ;; this prints beam iteration number as zero through NUM-ARCS.
	       (cond
		 ((= num-arcs 0)
		  (format t "~&Beam ~D of ~D, Plane ~D of ~D.~%"
			  beam-num num-beams (the fixnum (1+ x-idx)) nx))
		 (t (format t
			    "~&Beam ~D of ~D, Arc ~D of ~D, Plane ~D of ~D.~%"
			    beam-num num-beams arc-num num-arcs
			    (the fixnum (1+ x-idx)) nx)))

	       (setq xpi (- xp iso-xp)
		     xpi-r0 (* pct-r0 xpi)
		     xpi-r3 (* pct-r3 xpi)
		     xpi-r6 (* pct-r6 xpi))

	       (do ((y-idx 0 (the fixnum (1+ y-idx)))
		    (yp y-orig (+ yp yp-step)))
		   ((= y-idx ny))
		 (declare (type single-float yp)
			  (type fixnum y-idx))
		 (setq ypi (- yp iso-yp)
		       ypi-r1 (* pct-r1 ypi)
		       ypi-r4 (* pct-r4 ypi)
		       ypi-r7 (* pct-r7 ypi))

		 (do ((z-idx 0 (the fixnum (1+ z-idx)))
		      (zp z-orig (+ zp zp-step))
		      (scale-factor 0.0))
		     ((= z-idx nz))
		   (declare (type single-float zp scale-factor)
			    (type fixnum z-idx))
		   (setq zpi (- zp iso-zp))

		   ;; Load args for BEAM-DOSE and PATHLENGTH-RAYTRACE.
		   (setq scale-factor (/ #.Pathlength-Ray-Maxlength
					 (setf (aref arg-vec #.Argv-Raylen)
					       (3d-distance src-xp src-yp
							    src-zp xp yp zp))))

		   (setf (aref arg-vec #.Argv-Dp-X)
			 (+ src-xp (* scale-factor (- xp src-xp))))
		   (setf (aref arg-vec #.Argv-Dp-Y)
			 (+ src-yp (* scale-factor (- yp src-yp))))
		   (setf (aref arg-vec #.Argv-Dp-Z)
			 (+ src-zp (* scale-factor (- zp src-zp))))

		   ;; Load rest of arg vector for call to BEAM-DOSE.
		   ;; The math is the Pat-to-Coll transform.
		   (setf (aref arg-vec #.Argv-Xcd)
			 (+ xpi-r0 ypi-r1 (* pct-r2 zpi)))
		   (setf (aref arg-vec #.Argv-Ycd)
			 (+ xpi-r3 ypi-r4 (* pct-r5 zpi)))
		   (setf (aref arg-vec #.Argv-Zcd)
			 (+ xpi-r6 ypi-r7 (* pct-r8 zpi)))

		   (beam-dose)

		   (cond
		     ((= num-arcs 0)                ;Regular Beam.
		      (setf (aref dose-array x-idx y-idx z-idx)
			    (* dose-multiplier
			       (the single-float
				 (aref arg-vec #.Argv-Return-0)))))
		     ;; Arc-Therapy.
		     (t (incf (the single-float
				(aref dose-array x-idx y-idx z-idx))
			      (* dose-multiplier
				 (the single-float
				   (aref arg-vec #.Argv-Return-0))))))))))))

      ;; Compute TPR-AT-ISO for each beam.  For Arc-Therapy, average values
      ;; weighted by ARC-SCALE-FACTOR; for regular beam, compute single value.
      (setq tpr-@-iso
	    (cond
	      ((< iso-depth 0.0)
	       ;; If isocenter is in front of patient, return a negative
	       ;; value as flag for chart to print message "EXTEND".
	       -1.0)

	      ;; Otherwise [normal case], TPR Lookup minus Block-Factor
	      ;; if blocks are used.
	      (t (monus
		   (2d-lookup tpr-vector            ;TPR Lookup.
			      wc iso-depth tpr-fss-ar tpr-dep-ar
			      tpr-fssmap tpr-depmap tpr-tbl-ar)
		   (cond ((consp clipped-blocks)
			  ;; Load args to BLOCK-FACTOR.  CLIPPED-BLOCKS
			  ;; is passed via lexical environment.
			  (setf (aref arg-vec #.Argv-Xci) 0.0)
			  (setf (aref arg-vec #.Argv-Yci) 0.0)
			  (setf (aref arg-vec #.Argv-Depth) iso-depth)
			  (setf (aref arg-vec #.Argv-Div) 1.0)
			  (block-factor)
			  (aref arg-vec #.Argv-Return-0))
			 (t 0.0))))))

      (cond
	((= arc-num 0)            ;Static beam or initial iter of Arc-Therapy.
	 ;; For arc-therapy, store SSD computed from ISO-DEPTH on initial
	 ;; iteration [starting beam].  ISO-DEPTH must be >= zero.
	 ;; EQUIV-SQUARE, OUTPUT-COMP don't depend on position within arc.
	 ;;
	 ;; For regular beam, ISO-DEPTH < 0 means SSD > SAD. This is OK.
	 ;; For Arc-Therapy it is not, but we exit early in this case.
	 (setf (ssd rslt) (- sad iso-depth))
	 (setf (equiv-square rslt)
	       (inv-outputfactor coll wc outputfactor dosedata))
	 (setf (output-comp rslt) outputfactor)
	 (cond ((= num-arcs 0)                      ;Regular Beam.
		(setf (tpr-at-iso rslt) tpr-@-iso))
	       ;; Arc-Therapy, initial iteration.
	       (t (setq avg-tpr-@-iso (* arc-scale-factor tpr-@-iso))
		  ;; ARC-SCALE-FACTOR was 1/2 of normal value for initial
		  ;; iter.  Double it for all the middle iterations.
		  (setq arc-scale-factor (* arc-scale-factor 2.0))
		  (incf gan-rad arc-sz)
		  (setq arc-num (the fixnum (1+ arc-num)))
		  (go ARC-LOOP))))

	((< arc-num num-arcs)                  ;Arc-Therapy, middle iterations
	 (incf avg-tpr-@-iso (* arc-scale-factor tpr-@-iso))
	 (incf gan-rad arc-sz)
	 (setq arc-num (the fixnum (1+ arc-num)))
	 (when (= arc-num num-arcs)
	   ;; Halve ARC-SCALE-FACTOR for upcoming last iteration.
	   (setq arc-scale-factor (* arc-scale-factor 0.5)))
	 (go ARC-LOOP))

	(t (setf (tpr-at-iso rslt)                ;Arc-Therapy, last iteration
		 (+ avg-tpr-@-iso (* arc-scale-factor tpr-@-iso)))))))

  ;; Return T if computation completes successfully.  If something goes wrong,
  ;; function returns early with NIL indicating failure.  Return value sets
  ;; VALID-POINTS/VALID-GRID flags on return.
  t)

;;;=============================================================
;;; End.
