;;;
;;; autocontour
;;;
;;; Routines to do an automatic contouring of a set of image data and
;;; to reduce the number of vertices in a generated contour to a more
;;; manageable level.
;;;
;;; The basic routines here (except reduce-contour) are closely
;;; translated from the file autocontour.pas, the pascal source, from
;;; UWPLAN.  The reduce-contour code is largely re-implemented
;;; directly from the article, referenced below.
;;;
;;; The sources referenced in the pascal source are as follows:
;;;
;;;    reduce-contour    - Ramer, Urs.  An iterative procedure for the 
;;;                        polygonal approximation of plane curves.  
;;;                        Computer Graphics and Image processing (1),
;;;                        pp 244-256, 1972.
;;;    follow-contour    - David W. Brumberg, program 'traceborders', 
;;;                        UW Computer Science Lab, Sept 1980.
;;;    
;;; 19-Apr-1993 J. Unger do initial translation.
;;; 28-May-1993 J. Unger minor fix to elim some compiler msgs.
;;; 12-May-1994 I. Kalet uncomment code to search for gradient from
;;; starting point.  Also, check for nil contour in reduce-contour
;;; 28-Jul-1994 J. Unger add some optimization & remove debugging
;;; stmts.
;;;  8-Jan-1995 I. Kalet remove proclaim form and extra right paren.
;;;    Nov-1999 J. Zeman add untangle-contour routine
;;; 12-Dec-1999 J. Zeman remove count from autocontour, allowing contours
;;; of any number of points.
;;; 11-Apr-2000 I. Kalet merge back into Prism without using new
;;; routines.
;;; 20-Jul-2000 J. Zeman add code to detect whether follow-borders is "stuck"
;;;  and return completed border if so.
;;;  1-Jan-2009 I. Kalet change declaration of image in follow-border
;;; to unsigned-byte 8 because we are using mapped images, not raw images.
;;; 

(in-package :prism)

;;;-----------------------------------

(defun rc-distance (p q sin-angle cos-angle tan-angle steep vertical)

  "rc-distance p q sin-angle cos-angle tan-angle steep vertical

Returns the distance of p from the line segment determined by pj 
and pk in the function reduce-contour.  The other parameters are
computed there and passed here to avoid recomputation."

  (let ((rise (- (second q) (second p)))
        (run (- (first q) (first p))))
    (if vertical (abs run)
      (if steep
	  (round (abs (* sin-angle (- run (/ rise tan-angle)))))
        (round (abs (* cos-angle (- rise (* run tan-angle)))))))))

;;;-----------------------------------

(defun reduce-contour (vertices tolerance)

  "reduce-contour vertices tolerance

Given vertices, a list of (x y) pairs, and tolerance, a maximum
distance criterion, reduce-contour computes and returns a second list
of vertices which contains a subset of the points of vertices but
which closely approximates the contour represented by vertices.
Tolerance represents the maximum distance of the input contour from
the output contour - if 0, only redundant collinear points are
removed.  Reference: Urs Ramer, An Iterative Procedure for the
Polygonal Approximation of Plane Curves, Computer Graphics and Image
Processing 1, p 244-256, 1972."

  ;; See the reference for a description of the algorithm.  We store
  ;; pointers to sublists of the vertices list on the open-verts and
  ;; closed-verts list, so that we can recover the contiguous runs of
  ;; vertices to traverse when making the maximum distance determination
  ;; for subsets of vertices.

  (if vertices ;; don't attempt to reduce an empty list!
      (let ((closed-verts nil)
	    (open-verts nil))

	(push vertices closed-verts)
	(push (last vertices) open-verts)

	;; loop until open-verts list is empty - return the first elt
	;; of each member of closed-verts when done
	(do ((pj (first (first open-verts)) (first (first open-verts)))
	     (pk (first (first closed-verts)) (first (first closed-verts)))
	     (angle 0.0 0.0) 
	     (sin-angle 0.0 0.0) 
	     (cos-angle 0.0 0.0) 
	     (tan-angle 0.0 0.0)
	     (vertical nil nil)
	     (steep nil nil)
	     (max-dist 0.0 0.0)
	     (max-ptr (first closed-verts) (first closed-verts)))
	    ((null open-verts) (mapcar #'first (reverse closed-verts)))

	  (if (= (first pj) (first pk))
	      (setq vertical t)
	    (progn
	      (setq vertical nil)
	      (setq tan-angle (float (/ (- (second pj) (second pk)) 
					(- (first pj) (first pk)))))
	      (setq angle (atan tan-angle))
	      (setq sin-angle (sin angle))
	      (setq cos-angle (cos angle))
	      (when (> (abs tan-angle) 1.0) 
		(setq steep t))))

	  ;; find the point (p) in this subset of vertices which is
	  ;; furthest from the line determined by pj & pk
	  (do* ((vert-ptr (first closed-verts) (rest vert-ptr))
		(p (first vert-ptr) (first vert-ptr))
		(cur-dist 0.0))
	      ((eq vert-ptr (first open-verts)))
	    (setq cur-dist 
	      (rc-distance p pk sin-angle cos-angle tan-angle
			   steep vertical))
	    (when (> cur-dist max-dist)
	      (setf max-dist cur-dist)
	      (setf max-ptr vert-ptr)))

	  ;; if the max dist is greater than tolerance, push the
	  ;; vertex at this distance onto the open verts list -
	  ;; otherwise, take the last vertex off the open list and put
	  ;; it on the closed list
	  (if (> max-dist tolerance)
	      (push max-ptr open-verts)
	    (push (pop open-verts) closed-verts)))
	)))

;;;-----------------------------------

(defun follow-border (image xbegin ybegin x1 y1 x2 y2 threshold)

  "follow-border image xbegin ybegin x1 y1 x2 y2 threshold

Follows the isovalue border at the threshold value in image, starting
from the point (xbegin,ybegin), bounded by the region determined by 
the points (x1,y1) and (x2,y2), and returns the extracted contour."

  (let ((x xbegin)
        (y ybegin)
        (result-list nil))

    (declare (fixnum x y xbegin ybegin x1 y1 x2 y2 threshold))
    (declare (type (simple-array (unsigned-byte 8) 2) image))

    ;; first, search image for the contour
    (do ((ytop (1- y2))
	 (xtop (1- x2)))
        ((or (= y ytop) (>= (aref image y x) threshold)))
      (do ()
          ((or (= x xtop) (>= (aref image y x) threshold)))
        (declare (fixnum xtop ytop))
        (incf x))
      (when (= x xtop)
        (incf y)
        (setq x x1)))

    (when (>= (aref image y x) threshold) ;; must have found contour so follow
      (let* ((x-border x)
             (y-border y) 
             (new-thresh (1- (aref image y-border x-border)))
             (mode :south)
             (start t)
             (last-south '(-1 -1)))
        (declare (fixnum x-border y-border new-thresh))
        (loop
          (when (or (/= x x-border)
		    (/= y y-border))	; (not (equal mode :south))
            (setq start nil))
          (case mode
            (:south
   ;;check for an infinite loop here.
           (if (equal (list x y) last-south)
              (progn ;;(format t "~%Error: Contour stuck. Please Redraw.~%")
                     (return-from follow-border result-list)))
           (setf last-south (list x y))
	     (setq mode :east)
	     (if (> x x1)
		 (if (> y y1)
		     (cond
		      ((> (aref image (1- y) (1- x)) new-thresh)
		       (setq x (1- x)
			     y (1- y)
			     mode :west))
		      ((> (aref image (1- y) x) new-thresh)
		       (setq y (1- y)
			     mode :south))))
	       (if (> y y1)
		   (cond
                    ((> (aref image (1- y) x) new-thresh)
		     (setq y (1- y)
			   mode :south))))))
            (:east
	     (setq mode :north)
	     (if (> y y1)
		 (if (< x x2)
		     (cond
		      ((> (aref image (1- y) (1+ x)) new-thresh)
		       (setq x (1+ x)
			     y (1- y)
			     mode :south))
		      ((> (aref image y (1+ x)) new-thresh)
		       (setq x (1+ x)
			     mode :east))))
	       (if (< x x2)
		   (cond
                    ((> (aref image y (1+ x)) new-thresh)
		     (setq x (1+ x)
			   mode :east))))))
            (:north
	     (setq mode :west)
	     (if (< x x2)
		 (if (< y y2)
		     (cond
		      ((> (aref image (1+ y) (1+ x)) new-thresh)
		       (setq x (1+ x)
			     y (1+ y)
			     mode :east))
		      ((> (aref image (1+ y) x) new-thresh) 
		       (setq y (1+ y)
			     mode :north))))
	       (if (< y y2)
		   (cond
                    ((> (aref image (1+ y) x) new-thresh) 
		     (setq y (1+ y)
			   mode :north))))))
            (:west
	     (setq mode :south)
	     (if (< y y2)
		 (if (> x x1)
		     (cond
		      ((> (aref image (1+ y) (1- x)) new-thresh) 
		       (setq x (1- x)
			     y (1+ y)
			     mode :north))
		      ((> (aref image y (1- x)) new-thresh) 
		       (setq x (1- x)
			     mode :west))))
	       (if (> x x1)
		   (cond     
                    ((> (aref image y (1- x)) new-thresh) 
		     (setq x (1- x)
			   mode :west)))))))
          (push (list x y) result-list) 
          (when (and (not start) (= x x-border) (= y y-border))
            (return)))))
    result-list))

;;;-----------------------------------

(defvar *use-untangle* nil)

;;;-----------------------------------

(defun autocontour (image xbegin ybegin x1 y1 x2 y2 tolerance)

  "autocontour image xbegin ybegin x1 y1 x2 y2 tolerance

Automatically extracts a contour from image, given a starting point
xbegin,ybegin on the contour, bounded by the region determined by
points x1,y1 and x2,y2.  First, extracts the contour from the
image by calling follow-border, and then eliminates extra vertices
from the contour by calling reduce-contour, with the supplied
tolerance, and returns this reduced contour."

  (when (and (>= ybegin y1) (< ybegin y2)
	     (>= xbegin x1) (< xbegin x2))
    (let* ((threshold (1+ (aref image ybegin xbegin)))
	   (temp-contour (follow-border image xbegin ybegin
					x1 y1 x2 y2 threshold)))
      (reduce-contour (if *use-untangle*
			  (untangle-contour temp-contour)
			temp-contour)
		      tolerance))))

;;;----------------------------------

(defun untangle-contour (verts)

  "untangle-contour verts

given vertices, returns a contour in which no point appears
more than once.  Note: this function can be used on a list of
any sort comparable by #'equal.  It will return a list with no
repetitions, and with values between repetitions removed. 
[1 2 3 4 2 5 6] -> [1 2 5 6]"

  ;;take care of lists with identical first and last verticies
  (when (equal (first verts) (first(last verts)))
    (pop verts))
  (let* ((final nil)
	 (remaining verts)
	 (next-loc 0)   
	 (point (first remaining)))
      (loop until (null remaining)
	do
	  (setf point (first remaining))
	  (setf final (append final (list (pop remaining))))
	  ;;(format t "~% position: ~%")
	  ;;(time
	  (setf next-loc (position point remaining :test #'equal 
				   :from-end t))
	  ;; )
	  ;;(format t "~% defined: ~%")
	  ;;(time
	  ;; (setf next-loc (find-point-in-contour point remaining))
	  ;; )
	  (when next-loc
	    ;;(format t "found ")
	    (setf remaining (subseq remaining (+ 1 next-loc))))
	  )
    (return-from untangle-contour final)))

;;;---------------------------------

;;this is so much slower than 'equal' that i believe equal works
;;the same way, and does a better job of it. not using this function
;;left for debug purposes, but will definitely be removed for final
;;version.

(defun faster-point-compare (p1 p2)

  "faster-point-compare p1 p2

a quicker way than equal to compare points: checks first
coordinate. only checks second if first the same. uses eq.
designed to minimize number of operations on the most common 
case: two points not at all similar."

  (when (not (eq (first p1) (first p2)))
    (return-from faster-point-compare nil))
  (return-from faster-point-compare
    (not (eq (second p1) (second p2)))))

;;;--------------------------------

(defun find-point-in-contour (pt lst)
  
  "find-point-in-contour pt lst

a quick way to find the point pt, which is known to
occur in lst. lst represents an unreduced contour: that is,
points vary by at most one unit x or y from eachother.
this function written to be faster than 'find' in a very
specialized case, and takes advantages of known contour 
properties. results are not determined for non-autocontour
generated point lists."
  
  ;;30 list items are checked at a time, by seeing how close
  ;;the number of every 30th item is to the sought-after point,
  ;;assuming individual points differ by at most one pixel, which is
  ;;the case for autocontour-generated points.
  
  ;;special provision for lists of less than 30 length
  (when (< (length lst) 30)
    (return-from find-point-in-contour 
      (position pt lst :test #'equal :from-end t)))
  (let* 
      ((x (first pt))
       (y (second pt))
       (length (- (length lst) 1))
       (pos (- length 15))
       (distance 0)
       (found nil)
       (finished nil))
    ;;backward search for reoccurance of point. 
    (loop
	while(not finished)
	do
	  ;;(format t "in loop")
	  (setf distance (abs (- x (first (elt lst pos)))))
	  (incf distance (abs (- y (second (elt lst pos)))))
	  (when (>= distance 15)
	    ;;(format t "zooming in")
	    ;;have found an area possibly containing the point in question. 
	    ;;check in more detail.
	    (setf found (position (list x y) lst
				  :start (- pos 15) :end (+ pos 15)
				  :test #'equal :from-end t))
	    (when found
	      ;;(format t "non dead end* ")
	      (return-from find-point-in-contour found)))
	  ;;(format t "pos = ~A" pos)
	  (setf pos (- pos 15))
	  (when (> 30 pos)
	    ;;just use position on last piece. it has to be in here,
	    ;;due to the circumstances under which this function is
	    ;;called.
	    (return-from find-point-in-contour
	      (position pt lst :end (+ pos 15) :test #'equal
			:from-end t))))))

;;;-------------------------------------
;;; End.
