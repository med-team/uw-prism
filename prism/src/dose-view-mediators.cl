;;;
;;; dose-view-mediators
;;;
;;; maintain the relations between dose surfaces and views.
;;;
;;; 15-Jan-1995 I. Kalet split off from dose-result-mediators
;;;

(in-package :prism)

;;;--------------------------------------

(defclass dose-view-mediator (object-view-mediator)

  ()

  (:documentation "This mediator connects a dose-surface with a view.")
  )

;;;--------------------------------------

(defmethod initialize-instance :after ((dvm dose-view-mediator)
                                         &rest initargs)
  (declare (ignore initargs))

  (let ((ds (object dvm)))
    (ev:add-notify dvm (new-threshold ds) #'update-view)
    (ev:add-notify dvm (new-color ds) #'update-view)
    (ev:add-notify dvm (grid-status-changed (result ds)) #'update-view)
  ))

;;;--------------------------------------

(defmethod destroy ((dvm dose-view-mediator))

  (ev:remove-notify dvm (new-threshold (object dvm)))
  (ev:remove-notify dvm (new-color (object dvm)))
  (ev:remove-notify dvm (grid-status-changed (result (object dvm))))
  (call-next-method))

;;;--------------------------------------

(defun make-dose-view-mediator (ds v)

  "MAKE-DOSE-VIEW-MEDIATOR ds v

Creates and returns a dose-view-mediator between dose-surface ds and
view v."

  (make-instance 'dose-view-mediator :object ds :view v))

;;;--------------------------------------
