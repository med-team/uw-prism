;;;
;;; dosecomp-decls
;;;
;;; Contains declarations for constants and macros used in dose
;;; computation whose usages are spread across multiple files.
;;;
;;; 22-May-1998 BobGian created.
;;; 03-Feb-2000 BobGian add ERF-TABLE-SIZE (for electron dosecalc); rename
;;;   vars in MYSIN and MYATAN to be distinct from names used elsewhere;
;;;   cosmetics (case regularization).
;;; 08-Feb-2000 BobGian correct comment in MYATAN, more cosmetic cleanup.
;;; 02-Mar-2000, 11-May-2000 BobGian add declarations to MYSIN, MYATAN.
;;; 02-Nov-2000 BobGian MYSIN -> FAST-SIN, MYATAN -> FAST-ATAN.
;;; 30-May-2001 BobGian move most DEFCONSTANTs, DEFSTRUCTs, and DEFMACROs
;;;   from dose calculation files to this file.
;;; 11-Jun-2001 BobGian replace type-specific arithmetic macros
;;;   with THE declarations.
;;; 15-Mar-2002 BobGian add parameterized constants for PATHLENGTH
;;;   and electron dosecalc.
;;; 03-Jan-2003 BobGian:
;;;   Modify constants naming slots in Arg-Vector:
;;;     Argv-Return -> Argv-Return-0 and Argv-Return-1 so PATHLENGTH-INTEGRATE
;;;       can return two values.
;;;     Argv-Raylen added to pass target ray distance to PATHLENGTH-INTEGRATE.
;;;     Argv-Pl-Dx and Argv-Pl-Dy no longer used.
;;;   Change structures used in electron code [PBEAM, QNODE, and TILE]
;;;     to arrays with inlined accessors and new declarations.
;;;   Flush macros FAST-SIN and FAST-ATAN - not accurate enough.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 03-Nov-2003 BobGian - more specific/meaningful names for some constants:
;;;     Exp-Width -> Cutout-Expand-Width
;;;     Step-Size -> Electron-Step-Size
;;;

(in-package :Prism)

;;;=============================================================
;;; DEFCONSTANTs for array sizes.

(defconstant ERF-Table-Size 3001)

;;;=============================================================
;;; DEFCONSTANTs for parameters in Pathlength and Electron Dosecalc.

(defconstant Pathlength-Ray-Maxlength 400.0)
(defconstant Electron-SSD-Minlength 99.5)
(defconstant Electron-SSD-Maxlength 120.0)
(defconstant Cutout-Min-Size 2.0)
(defconstant Tissue-Maximum-Density 2.0)

;;;=============================================================
;;; DEFCONSTANTs for named slots in Arg-Vec.

;;; ARG-VEC holds an array with Argv-Size slots for passing
;;; SINGLE-FLOAT args to functions and for returning results.  Doing so
;;; [rather than passing args the normal way] greatly decreases flonum boxing
;;; and GC overhead.  These contants define the slot-number [zero-indexed] vs
;;; usage pattern.  "Static" slots hold a value which does not change for the
;;; entire dose-calc, after being initialized at start.  Values in "Dynamic"
;;; slots change as the calculation proceeds [ie, a different value is passed
;;; on each function call].  "Mixed" slots hold data that does change but not
;;; too often - ie, it is fixed for some major iteration and therefore need
;;; only be updated at loop entry, not at every argument-passing point.

(defconstant Argv-Return-0 0)            ;Return first flonum value - dynamic.
(defconstant Argv-Return-1 1)           ;Return second flonum value - dynamic.

(defconstant Argv-Src-X 2)                    ;Source-X in Pt coords - static.
(defconstant Argv-Src-Y 3)                    ;Source-Y in Pt coords - static.
(defconstant Argv-Src-Z 4)                    ;Source-Z in Pt coords - static.

(defconstant Argv-Dp-X 5)                   ;DosePoint-X in Pt coords - mixed.
(defconstant Argv-Dp-Y 6)                   ;DosePoint-Y in Pt coords - mixed.
(defconstant Argv-Dp-Z 7)                   ;DosePoint-Z in Pt coords - mixed.

(defconstant Argv-Xcd 8)                  ;DosePoint-X in Coll coords - mixed.
(defconstant Argv-Ycd 9)                  ;DosePoint-Y in Coll coords - mixed.
(defconstant Argv-Zcd 10)                 ;DosePoint-Z in Coll coords - mixed.

(defconstant Argv-Raylen 11)         ;RayLength passed to BEAM-DOSE - dynamic.

(defconstant Argv-Xci 12)                ;DP-X, Coll coords, at iso - dynamic.
(defconstant Argv-Yci 13)                ;DP-Y, Coll coords, at iso - dynamic.

(defconstant Argv-Depth 14)             ;Surface-DosePoint distance - dynamic.
(defconstant Argv-Div 15)                           ;DIVERGENCE - dynamic.

(defconstant Argv-Xci- 16)                  ;Portal X-Min coll coord - static.
(defconstant Argv-Xci+ 17)                  ;Portal X-Max coll coord - static.
(defconstant Argv-Yci- 18)                  ;Portal Y-Min coll coord - static.
(defconstant Argv-Yci+ 19)                  ;Portal Y-Max coll coord - static.

(defconstant Argv-Enc-X 20)                         ;ENCLOSES? X arg.
(defconstant Argv-Enc-Y 21)                         ;ENCLOSES? Y arg.

(defconstant Argv-Size 22)                          ;Size of Argument Vector.

;;;=============================================================
;;; DEFCONSTANTs related to Polygon-Clipping code.

;;; ARG-VEC is bound to an array with Argv-Size slots for passing
;;; SINGLE-FLOAT args to functions and for returning results.  This file
;;; contains DEFCONSTANTs for defining slot names in ARG-VEC, since
;;; multiple files use ARG-VEC.

;;; Note that XCI-, XCI+, YCI-, and YCI+ are always passed in slots named
;;; Argv-Xci-, Argv-Xci+ Argv-Yci-, and Argv-Yci+ - once loaded, before call
;;; to CLIP-BLOCKS, they never need to be reloaded during entire clipping
;;; routine - or for the rest of the dose calculation, for that matter.

;;; Slot usage in the clipping routines is independent of usage in the rest
;;; of the dose computation, other than steering clear of the four slots just
;;; mentioned.  Actually, they are NOT currently used outside the clipping
;;; routines, but they might be at some future time.

;;; CLIP-BLOCKS:
;;;
;;;   Inputs:  VLIST   Passed as arg
;;;            XCI-    Slot Argv-Xci-
;;;            XCI+    Slot Argv-Xci+
;;;            YCI-    Slot Argv-Yci-
;;;            YCI+    Slot Argv-Yci+
;;;
;;;   Returns: LIST - Clipped block outlines in Counter-Clockwise traversal.

;;; GRAZER?:
;;;
;;;   Inputs:
(defconstant Argv-Bx 0)
(defconstant Argv-By 1)
(defconstant Argv-Cx 2)
(defconstant Argv-Cy 3)
(defconstant Argv-Nx 4)
(defconstant Argv-Ny 5)
;;;            XCI-    Slot Argv-Xci-
;;;            XCI+    Slot Argv-Xci+
;;;            YCI-    Slot Argv-Yci-
;;;            YCI+    Slot Argv-Yci+
;;;
;;;   Returns: BOOLEAN

;;; PUSHNODE:
;;;
;;;   Inputs:
(defconstant Argv-Vx 0)
(defconstant Argv-Vy 1)
;;;            XCI-    Slot Argv-Xci-
;;;            XCI+    Slot Argv-Xci+
;;;            YCI-    Slot Argv-Yci-
;;;            YCI+    Slot Argv-Yci+
;;;
;;;   Returns: Pointer to Node

;;; SINGLE-CROSS:
;;;
;;;   Inputs:
(defconstant Argv-Ix 0)
(defconstant Argv-Iy 1)
(defconstant Argv-Ox 2)
(defconstant Argv-Oy 3)
;;;            XCI-    Slot Argv-Xci-
;;;            XCI+    Slot Argv-Xci+
;;;            YCI-    Slot Argv-Yci-
;;;            YCI+    Slot Argv-Yci+
;;;
;;;   Returns:
(defconstant Argv-X 0)
(defconstant Argv-Y 1)

;;; DUAL-CROSS:
;;;
;;;   Inputs:
;(defconstant Argv-Ix 0)
;(defconstant Argv-Iy 1)
;(defconstant Argv-Ox 2)
;(defconstant Argv-Oy 3)
;;;            XCI-    Slot Argv-Xci-
;;;            XCI+    Slot Argv-Xci+
;;;            YCI-    Slot Argv-Yci-
;;;            YCI+    Slot Argv-Yci+
;;;
;;;   Returns:
(defconstant Argv-Xe 0)
(defconstant Argv-Ye 1)
(defconstant Argv-Xl 2)
(defconstant Argv-Yl 3)

;;;=============================================================
;;; Structure used by Polygon Clipping code.
;;;
;;; "Real" structure simulated by ordinary array referencing, in order to
;;; get inlined array access.

(defconstant Cnode-Xci 0)                           ;XCI coordinate of node.
(defconstant Cnode-Yci 1)                           ;YCI coordinate of node.
(defconstant Cnode-Next 2)              ;Ptr to next node on original contour.

;;; Type: INSIDE, ENTER, or LEAVE.
;;; Nodes on border are considered OUTSIDE, and no OUTSIDE nodes
;;; [strict or border] are saved - only vertices which interact
;;; with the interior of the portal are used.
(defconstant Cnode-Type 3)

;;; CODE is a fixnum indicating location of a vertex on the portal border:
;;;   NIL for vertices INSIDE the portal.
;;;   ODD values indicate sides not including the corner points.
;;;   EVEN values indicate corner points.
;;; Values start with ZERO at the SouthWest [ie, the (XCI-, YCI-) ] corner
;;; and increment around the portal.  The value is meaningful MOD 8.
(defconstant Cnode-Code 4)

(defconstant Cnode-Size 5)

;;;=============================================================
;;; Constants for Electron dosecalc.

(defconstant Cutout-Expand-Width 0.8)          ; 0.4 cm field margin times two
(defconstant Pen-Bm-Width 0.1)                      ; always 0.1 cm
(defconstant Electron-Step-Size 0.1)

;;;=============================================================
;;; Structure definition for Pencil Beams.
;;; Only the three collimator coefficients are settable at creation time.
;;; The three patient coefficients are set later via SETF.
;;; The weight coefficient is set at creation time
;;; and applies to both coordinate systems.

(defmacro make-pbeam (weight xc yc zc)
  `(let (($obj (make-array 7 :element-type 'single-float)))
     (declare (type (simple-array single-float (7)) $obj))
     (setf (aref $obj 0) (the single-float ,weight))
     (setf (aref $obj 1) (the single-float ,xc))
     (setf (aref $obj 2) (the single-float ,yc))
     (setf (aref $obj 3) (the single-float ,zc))
     $obj))

(defmacro pbeam-wt ($obj)                           ;Initial beam weight.
  `(aref (the (simple-array single-float (7)) ,$obj) 0))

(defmacro pbeam-xc ($obj)                      ;Collimator X-coordinate in cm.
  `(aref (the (simple-array single-float (7)) ,$obj) 1))

(defmacro pbeam-yc ($obj)                      ;Collimator Y-coordinate in cm.
  `(aref (the (simple-array single-float (7)) ,$obj) 2))

(defmacro pbeam-zc ($obj)                      ;Collimator Z-coordinate in cm.
  `(aref (the (simple-array single-float (7)) ,$obj) 3))

(defmacro pbeam-xp ($obj)                         ;Patient X-coordinate in cm.
  `(aref (the (simple-array single-float (7)) ,$obj) 4))

(defmacro pbeam-yp ($obj)                         ;Patient Y-coordinate in cm.
  `(aref (the (simple-array single-float (7)) ,$obj) 5))

(defmacro pbeam-zp ($obj)                         ;Patient Z-coordinate in cm.
  `(aref (the (simple-array single-float (7)) ,$obj) 6))

;;;=============================================================
;;; Structure definitions for Quadtree objects.

(defmacro make-qnode (x-pos y-pos dimension)
  `(let (($obj (make-array 8 :element-type t :initial-element nil)))
     (declare (type (simple-array t (8)) $obj))
     (setf (aref $obj 0) (the single-float ,x-pos))
     (setf (aref $obj 1) (the single-float ,y-pos))
     (setf (aref $obj 2) (the single-float ,dimension))
     $obj))

(defmacro qnode-xpos ($obj)                 ; Central X qnode coordinate in cm
  `(aref (the (simple-array t (8)) ,$obj) 0))

(defmacro qnode-ypos ($obj)                 ; Central Y qnode coordinate in cm
  `(aref (the (simple-array t (8)) ,$obj) 1))

(defmacro qnode-dimension ($obj)             ; Size of square qnode side in cm
  `(aref (the (simple-array t (8)) ,$obj) 2))

(defmacro qnode-child1 ($obj)                       ; Subnode
  `(aref (the (simple-array t (8)) ,$obj) 3))

(defmacro qnode-child2 ($obj)                       ; Subnode
  `(aref (the (simple-array t (8)) ,$obj) 4))

(defmacro qnode-child3 ($obj)                       ; Subnode
  `(aref (the (simple-array t (8)) ,$obj) 5))

(defmacro qnode-child4 ($obj)                       ; Subnode
  `(aref (the (simple-array t (8)) ,$obj) 6))

(defmacro qnode-status ($obj)   ; One of :Inside, :Outside, :Cantmerge, or NIL
  `(aref (the (simple-array t (8)) ,$obj) 7))

;;;=============================================================

(defmacro make-tile (x-pos y-pos dimension)
  `(let (($obj (make-array 3 :element-type 'single-float)))
     (declare (type (simple-array single-float (3)) $obj))
     (setf (aref $obj 0) (the single-float ,x-pos))
     (setf (aref $obj 1) (the single-float ,y-pos))
     (setf (aref $obj 2) (the single-float ,dimension))
     $obj))

(defmacro tile-xpos ($obj)                ; X coordinate of merged qnode in cm
  `(aref (the (simple-array single-float (3)) ,$obj) 0))

(defmacro tile-ypos ($obj)                ; Y coordinate of merged qnode in cm
  `(aref (the (simple-array single-float (3)) ,$obj) 1))

(defmacro tile-dimension ($obj)              ; Half-width of square tile in cm
  `(aref (the (simple-array single-float (3)) ,$obj) 2))

;;;=============================================================
;;; Macro used in COMPUTE-BEAM-DOSE.  This clamps dose when BLOCK-FACTOR
;;; is included to >= 0.0, in case amount subtracted over-estimated.

(defmacro monus (x y)
  (let ((val (gensym)))
    `(let ((,val (- (the single-float ,x) (the single-float ,y))))
       (the single-float
	 (if (< (the single-float ,val) 0.0) 0.0 (the single-float ,val))))))

;;;=============================================================

(defmacro sqr-float (arg)
  ;; Bind a local var to argument to avoid repeated evaluation of arg.
  `(let ((sqr.arg (the single-float ,arg)))
     (the single-float
       (* (the single-float sqr.arg)
	  (the single-float sqr.arg)))))

(defmacro sqr-fix (arg)
  ;; Bind a local var to argument to avoid repeated evaluation of arg.
  `(let ((sqr.arg (the fixnum ,arg)))
     (the fixnum
       (* (the fixnum sqr.arg)
	  (the fixnum sqr.arg)))))

;;;-------------------------------------------------------------
;;; 3D-DISTANCE: computes distance between two points in 3D space
;;; Inline expansion of DISTANCE-3D function used in other versions.
;;;-------------------------------------------------------------

(defmacro 3d-distance (xc1 yc1 zc1 xc2 yc2 zc2)

  "3d-distance xc1 yc1 zc1 xc2 yc2 zc2

returns the distance between two points in 3D space"

  `(let ((xdiff (- (the single-float ,xc2) (the single-float ,xc1)))
	 (ydiff (- (the single-float ,yc2) (the single-float ,yc1)))
	 (zdiff (- (the single-float ,zc2) (the single-float ,zc1))))

     (declare (type single-float xdiff ydiff zdiff))

     (the (single-float 0.0 *)
       (sqrt (the (single-float 0.0 *)
	       (+ (* xdiff xdiff)
		  (* ydiff ydiff)
		  (* zdiff zdiff)))))))

;;;=============================================================
;;; End.
