;;;
;;; dosecomp
;;;
;;; Functions which implement Prism dose computation methods.  The
;;; actual details for each type of source are in separate files.
;;;
;;;  2-Jan-1996 I. Kalet from original dosecomp.cl, split off stream
;;;   i/o to separate file in anticipation of rewrite of beam dose
;;;   calc. in lisp.  This code calls the source-specific code which is
;;;   in other files.
;;; 16-Jan-1996 I. Kalet modify calls to source-specific code, to use
;;;   new Lisp implementation, and not the old Pascal program.
;;; 21-Mar-1997 I. Kalet new calls to general compute-xxx-dose instead
;;;   of separate functions for pts and grid.
;;; 26-Jun-1997 I. Kalet add check for grid size and make new array if
;;;   necessary before calling compute-xxx-dose.  Check plan result
;;;   array size too.
;;; 30-Oct-1997 BobGian compute-xxx-dose fcns return t on success.
;;;   Return value of nil indicates failure - compute-dose-xxx then
;;;   does not set valid-xxx slot.
;;; 09-Mar-1998 BobGian modest upgrade of dose-calc code.
;;; 17-Jul-1998 BobGian factor beam-independent component of
;;; pathlength computation out of compute-beam-dose and into
;;; build-patient-structures.  Change arguments to compute-beam-dose.
;;; 22-Dec-1998 I. Kalet add call to compute-electron-dose
;;; 21-Sep-2003 BobGian - remove obsolete version number in change log header.
;;;

(in-package :prism)

;;;--------------------------------------------------

(defun insure-grid-size (result grid-dims)

  "insure-grid-size src grid-dims

checks if the grid of the dose-result RESULT has dimensions GRID-DIMS
and replaces it if not.  The grid slot may be unbound or NIL, so that
is checked first."

  (when (or (not (slot-boundp result 'GRID))
	    (null (grid result))
	    (notevery #'= grid-dims (array-dimensions (grid result))))
    (setf (grid result)
      (make-array grid-dims :element-type 'SINGLE-FLOAT))
    nil))

;;;--------------------------------------------------

(defun compute-dose-grid (plan pat)

  "compute-dose-grid plan pat

Given collections of organs and marks, a table position, a dose-grid
specification, and collections of beams, seeds, and line sources, all
contained within PLAN and PAT, this function computes the volumetric dose
for each radiation source and stores it in the grid attribute of the
source's DOSE-RESULT.  The function computes dose for each radiation
source whose dose result's grid is invalid, and sets VALID-GRID to T if
that computation completes successfully by returning T."

  (let* ((gg (dose-grid plan))
	 (dims (list (x-dim gg) (y-dim gg) (z-dim gg))))
    (insure-grid-size (sum-dose plan) dims)
    ;;
    (let ((sources (coll:elements (beams plan))))
      ;; Build structures representing patient anatomy - invariant over
      ;; entire dose calculation for all beams.
      (multiple-value-bind
	  (organ-vertices-list organ-z-extents organ-density-array)
	  (build-patient-structures (anatomy pat))
	;;
	(dolist (src sources)
	  (let ((result (result src)))
	    (unless (valid-grid result)
	      (insure-grid-size result dims)
	      (setf (valid-grid result)
		(if (typep (collimator src) 'electron-coll)
		    (compute-electron-dose
		     src sources nil gg organ-vertices-list
		     organ-z-extents organ-density-array)
		  (compute-beam-dose
		   src sources nil gg organ-vertices-list
		   organ-z-extents organ-density-array))))))))
    ;;
    (dolist (src (coll:elements (line-sources plan)))
      (let ((result (result src)))
	(unless (valid-grid result)
	  (insure-grid-size result dims)
	  (setf (valid-grid result)
	    (compute-line-dose src nil gg))))) ; no points!
    ;;
    (dolist (src (coll:elements (seeds plan)))
      (let ((result (result src)))
	(unless (valid-grid result)
	  (insure-grid-size result dims)
	  (setf (valid-grid result)
	    (compute-seed-dose src nil gg))))))) ; no points!

;;;--------------------------------------

(defun compute-dose-points (plan pat)

  "compute-dose-points plan pat

Given collections of organs and marks, a table position, a collection of
points, and collections of beams, seeds, and line sources, all contained
within PLAN and PAT, this function computes the dose to each point for
each radiation source and stores the doses in the points attribute of the
source's DOSE-RESULT.  The function only computes dose for each radiation
source whose dose result's points is invalid, and sets VALID-GRID to T if
that computation completes successfully by returning T."

  (let ((pointlist (coll:elements (points pat))))
    ;; Build structures representing patient anatomy -
    ;; invariant over dose calculation for all beams.
    (multiple-value-bind
	(organ-vertices-list organ-z-extents organ-density-array)
	(build-patient-structures (anatomy pat))
      (let ((sources (coll:elements (beams plan))))
	(dolist (src sources)
	  (let ((result (result src)))
	    (unless (valid-points result)
	      (setf (valid-points result)
		(if (typep (collimator src) 'electron-coll)
		    (compute-electron-dose
		     src sources pointlist nil organ-vertices-list
		     organ-z-extents organ-density-array)
		  (compute-beam-dose
		   src sources pointlist nil organ-vertices-list
		   organ-z-extents organ-density-array))))))))
    ;;
    (dolist (src (coll:elements (line-sources plan)))
      (let ((result (result src)))
	(unless (valid-points result)
	  (setf (valid-points result)
	    (compute-line-dose src pointlist nil))))) ; no grid
    ;;
    (dolist (src (coll:elements (seeds plan)))
      (let ((result (result src)))
	(unless (valid-points result)
	  (setf (valid-points result)
	    (compute-seed-dose src pointlist nil))))))) ; no grid

;;;--------------------------------------
;;; End.
