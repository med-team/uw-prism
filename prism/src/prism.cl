;;;
;;; prism
;;;
;;; This module provides a top level function that creates a patient
;;; panel.  If the optional argument is missing, a new patient object
;;; is created.
;;;
;;; 22-Aug-1992 I. Kalet created
;;; 30-Nov-1992 I. Kalet take out archive panel for now
;;; 30-Dec-1992 I. Kalet delete views from plans on exit
;;;  1-Jul-1993 I. Kalet return the right patient instance
;;; 31-Jul-1993 I. Kalet load slik here.
;;; 18-Oct-1993 J. Unger add dosecomp invokation and termination.
;;;  6-Apr-1994 I. Kalet add top level function
;;;  4-May-1994 J. Unger add load forms for ruler-system and
;;;  polygon-system.
;;; 13-May-1994 I. Kalet split off load forms to load-prism, load
;;; therapy machines and user and system config files here.
;;;  6-Jun-1994 I. Kalet add digitizer initialization
;;;  7-Jul-1994 J. Unger read config file from *config-directory*
;;;  variable.
;;; 26-Jan-1995 I. Kalet change digitizer function name from gp8 to
;;;  digit and correct comments above.  Also, use new function
;;;  load-therapy-machines instead of referencing *therapy-machines*
;;; 29-Jan-1997 I. Kalet mods for integrated dose computation -
;;; therapy machines now load on demand, no beamdose subprocess.
;;; 21-Jun-1997 BobGian - minor fixups (eliminated redundant vars).
;;; 19-Sep-1997 BobGian notes here that references to old function
;;;   load-therapy-machines now refer instead to new function
;;;   get-therapy-machine.
;;; 17-Jun-1998 I. Kalet add another debug hook, a global to hold the
;;; patient panel for access from a break loop.  Also use anaphors in
;;; loading config files.
;;; 30-Oct-1998 I. Kalet add read-time conditionals to handle wierd
;;; HP-UX bug regarding default X host with Allegro 5.0
;;; 15-Jun-1999 I. Kalet finally change X host determination from
;;; command line parameter to DISPLAY environment variable.
;;;  2-Jan-2000 I. Kalet add brachytherapy source catalog file load
;;;  4-Sep-2000 I. Kalet use localhost for blank host, it works
;;; everywhere, instead of the wierd HP behavior.  It seems that CLX
;;; and xlib are incompatible with respect to blank or empty strings.
;;; 11-Mar-2001 I. Kalet add copying of fg-gray-level, bg-gray-level
;;; and border-style to SLIK to make panel background level user
;;; configurable.  Also add dump-prism-image for convenience.
;;; 18-Mar-2001 I. Kalet use blank string where possible (linux in
;;; particular) as local Unix sockets are more efficient and available
;;; than the loopback path.
;;; 30-Jul-2003 I. Kalet restore dump-prism-image.
;;; 25-May-2009 I. Kalet add new variable *prism-version* to
;;; *features* to allow conditional load of patches etc. in prism.config
;;; 24-Jun-2009 I. Kalet move dump-prism-image to separate file, not
;;; really part of the Prism system.
;;; 13-Nov-2009 I. Kalet parametrize location of prism.config with
;;; environment variable PRISM_CONFIG_DIRECTORY, use getenv instead of
;;; sys:getenv, so hopefully will work in other lisps besides ACL.
;;; 16-Jul-2011 I. Kalet just use value of DISPLAY in prism-top-level
;;; since prism does too, and sl:initialize parses the host info,
;;; assuming it includes the display number.  This will allow for ssh
;;; tunneling with non-zero display number.
;;;

(in-package :prism)

;;;--------------------------------------

(defvar *patient-panel* nil "The patient panel, for debugging only.")

;;;--------------------------------------

(defun prism (host &optional pat)

  (format t "~%Prism ~A is starting.~%" *prism-version-string*)
  (push *prism-version* *features*) ;; for patch management

  ;; read in the prism.config file - optional, may not exist
  (aif (probe-file (merge-pathnames "prism.config"
				    (getenv "PRISM_CONFIG_DIRECTORY")))
       (load it))
  ;; read in the .prismrc file - ditto
  (aif (probe-file (merge-pathnames ".prismrc" (user-homedir-pathname)))
       (load it))
  ;; read in the brachytherapy source catalog file - optional, may not exist
  (aif (probe-file (merge-pathnames "source-catalog" *brachy-database*))
       (setf *brachy-tables* (get-all-objects it)))
  (setf sl:*fg-level* *fg-gray-level*)
  (setf sl:*bg-level* *bg-gray-level*)
  (setf sl:*default-border-style* *border-style*)
  (sl:initialize host)
  (let ((pat-panel (make-patient-panel
		    (or pat (make-instance 'patient))))
	(digitizer (second (assoc (sl:host) *digitizer-devices*
				  :test #'string-equal))))
    (setq *patient-panel* pat-panel)
    (when digitizer (digit-initialize digitizer))
    (sl:process-events)
    (setq pat (the-patient pat-panel))	; might have been replaced
    ;; delete all the views, since the display will be closed
    (dolist (pl (coll:elements (plans pat)))
      (dolist (v (coll:elements (plan-views pl)))
	(coll:delete-element v (plan-views pl))))
    (destroy pat-panel)
    (sl:terminate)
    (when digitizer (digit-close))
    (setq *patient-panel* nil)
    pat))

;;;--------------------------------------

#+allegro
(defun prism-top-level ()

  "prism-top-level is a function of no arguments, to be used as the
top-level function in an executable that just runs Prism instead of
the Common Lisp read-eval-print loop."

  (setf (sys:gsgc-switch :print) nil)
  (setf (sys:gsgc-switch :stats) nil)
  (setf (sys:gsgc-switch :verbose) nil)

  (prism (getenv "DISPLAY"))
  (excl:exit))

;;;--------------------------------------
;;; End.
