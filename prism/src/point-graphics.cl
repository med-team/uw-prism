;;;
;;; point-graphics
;;;
;;; Defines draw methods for drawing marks into views.
;;;
;;; 25-Apr-1994 J. Unger created.
;;; 26-Apr-1994 J. Unger combine common parts of methods into separate
;;; function
;;;  4-Sep-1995 I. Kalet call pix-x, pix-y, declare some types
;;; 19-Sep-1996 I. Kalet remove &rest from draw methods
;;;  6-Dec-1996 I. Kalet don't generate prims if color is invisible
;;; 03-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx .
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible
;;; 25-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;;  5-Jan-2000 I. Kalet relax z match criterion for display.
;;; 30-Jul-2002 I. Kalet add support for oblique views.
;;;

(in-package :prism)

;;;--------------------------------------

(defparameter *point-prim-size* 4 
  "Half the length and width of the point primitive hatchmark, in pixels.")

;;;--------------------------------------

(defun pixel-point (xpt ypt pix-per-cm xorig yorig)

  "Converts the real space coordinate pair determined by floating
point numbers xpt and ypt into a hatch mark in screen space and
a point at which to place a numerical id.  The three values are
returned via a (values...) form: hatch mark list, x coord of id,
and y coord of id."

  (let ((xt (pix-x xpt xorig pix-per-cm))
        (yt (pix-y ypt yorig pix-per-cm)))
    (declare (fixnum xt yt xorig yorig *point-prim-size*)
	     (single-float xpt ypt pix-per-cm))
    (values
      (list 
        (- xt *point-prim-size*) yt (+ xt *point-prim-size*) yt
        xt (- yt *point-prim-size*) xt (+ yt *point-prim-size*))
      (+ xt *point-prim-size*)
      (+ yt (* 3 *point-prim-size*))
)))

;;;--------------------------------------

(defun draw-point-in-view (pt v px py pz)

  "draw-point-in-view pt v px py pz

Draws point pt into view v.  The pair (px py) is the location of the
point on the plane of the view and pz is the position of the point
along the same axis to which the view is perpendicular."

  ; be careful not to just put tons of 'empty' graphic primitives on 
  ; the foreground list for the points - there may be a lot of points
  ; and lots of empty primitives on the foreground is inefficient when
  ; the foreground is drawn, so remove prims when we find them on the
  ; list and there are no points to draw.

  (let ((s-prim (find-if #'(lambda (prim) 
                             (and (eq (object prim) pt) 
                                  (typep prim 'segments-prim)))
			 (foreground v)))
        (c-prim  (find-if #'(lambda (prim) 
			      (and (eq (object prim) pt) 
				   (typep prim 'characters-prim)))
			  (foreground v)))
        (color (sl:color-gc (display-color pt)))
        (same-plane (poly:nearly-equal pz (view-position v)
				       *display-epsilon*))
        (hatchmarks nil)
        (x-anchor nil)
        (y-anchor nil))
    (when (and s-prim (not same-plane))
      (setf (foreground v) (remove s-prim (foreground v)))
      (setf (foreground v) (remove c-prim (foreground v))))
    (when same-plane
      (unless s-prim 
        (setq s-prim (make-segments-prim nil color :object pt))
        (push s-prim (foreground v))
        (setq c-prim (make-characters-prim nil nil nil color :object pt))
        (push c-prim (foreground v)))
      (setf (color s-prim) color)
      (setf (color c-prim) color)
      (multiple-value-setq 
	  (hatchmarks x-anchor y-anchor)
        (pixel-point px py (scale v) (x-origin v) (y-origin v)))
      (setf (points s-prim) hatchmarks)
      (setf (x c-prim) x-anchor)
      (setf (y c-prim) y-anchor)
      (setf (characters c-prim) (write-to-string (id pt))))))

;;;--------------------------------------

(defmethod draw :around ((pt mark) (v view))

  (if (eql (display-color pt) 'sl:invisible)
      (setf (foreground v) (remove pt (foreground v) :key #'object))
    (call-next-method)))

;;;--------------------------------------

(defmethod draw ((pt mark) (tv transverse-view))

  "draw (pt mark) (tv transverse-view)

Draws a mark in a transverse view if the point lies in the plane of the view."

  (draw-point-in-view pt tv (x pt) (y pt) (z pt)))

;;;--------------------------------------

(defmethod draw ((pt mark) (cv coronal-view))

  "draw (pt mark) (cv coronal-view)

Draws a mark in a coronal view if the point lies in the plane of the view."

  (draw-point-in-view pt cv (x pt) (- (z pt)) (y pt)))

;;;--------------------------------------

(defmethod draw ((pt mark) (sv sagittal-view))

  "draw (pt mark) (sv sagittal-view)

Draws a mark in a sagittal view if the point lies in the plane of the view."

  (draw-point-in-view pt sv (z pt) (y pt) (x pt)))

;;;--------------------------------------

(defmethod draw ((pt mark) (ov oblique-view))

  "draws the point in an oblique view if the point is in the plane of
the view"

  (let* ((x (x pt))
	 (y (y pt))
	 (z (z pt))
	 (azi-rad (* (azimuth ov) *pi-over-180*))
	 (alt-rad (* (altitude ov) *pi-over-180*))
	 (sin1 (sin azi-rad))
	 (cos1 (cos azi-rad))
	 (sin2 (sin alt-rad))
	 (cos2 (cos alt-rad))
	 (z-temp (+ (* x sin1) (* z cos1))))
    (draw-point-in-view pt ov
			(- (* x cos1) (* z sin1))
			(- (* y cos2) (* z-temp sin2))
			(+ (* y sin2) (* z-temp cos2)
			   (- (view-position ov))))))

;;;--------------------------------------
;;; End.
