;;;
;;; beam-blocks
;;;
;;; this module describes shielding blocks and their functions
;;;
;;; 16-May-1994 I. Kalet finally split off from collimators module.
;;;  2-Jun-1994 I. Kalet add more details.
;;; 23-Jun-1994 I. Kalet put copy-block here from beams.  Change float
;;; to single-float.
;;; 19-Oct-1994 J. Unger add new-transmission announcement when trans
;;; changes.
;;;  9-Jan-1995 I. Kalet delete beam-for attribute.
;;; 11-Sep-1995 I. Kalet add new-color event, DON'T SAVE IT.
;;; 19-Dec-1999 I. Kalet add keyword parameter :copy-name to copy-block
;;; 22-Feb-2000 I. Kalet replace copy-block with method for copy, and
;;; just copy straight.  If reflection is needed, do it to the copy.
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass beam-block (generic-prism-object contour)

  ((transmission :type single-float
		 :initarg :transmission 
		 :accessor transmission
		 :documentation "The nominal fractional transmission
through the block.")

   (new-transmission :type ev:event
		     :accessor new-transmission
		     :initform (ev:make-event)
		     :documentation "Announced when the block
transmission is changed.")

   (new-vertices :type ev:event
		 :accessor new-vertices
		 :initform (ev:make-event)
		 :documentation "Announced when the block vertices are
updated.")

   (new-color :type ev:event
	      :accessor new-color
	      :initform (ev:make-event)
	      :documentation "Announced when the block display-color
is updated.  The display-color is inherited from class contour.")

   )

  (:default-initargs :name "" :z 0.0 :transmission 0.05)

  (:documentation "Beam-blocks are always attached to some beam.  The
block outline is defined by filling in the slots inherited from class
contour.")

  )

;;;---------------------------------------------

(defmethod slot-type ((object beam-block) slotname)

  (case slotname
    (beam-for :ignore)
    (otherwise :simple)))

;;;---------------------------------------------

(defmethod not-saved ((blk beam-block))

  (append (call-next-method)
	  '(new-vertices new-transmission new-color)))

;;;---------------------------------------------

(defmethod (setf transmission) :after (new-trans (blk beam-block))

  (ev:announce blk (new-transmission blk) new-trans))

;;;---------------------------------------------

(defmethod (setf vertices) :after (new-verts (blk beam-block))

  (ev:announce blk (new-vertices blk) new-verts))

;;;---------------------------------------------

(defmethod (setf display-color) :after (new-col (blk beam-block))

  (ev:announce blk (new-color blk) new-col))

;;;---------------------------------------------

(defun make-beam-block (block-name &rest initargs)

  (apply #'make-instance 'beam-block
	 :name (if (equal block-name "")
		   (format nil "~A" (gensym "BLOCK-"))
		 block-name)
	 initargs))

;;;---------------------------------------------

(defmethod copy ((blk beam-block))

  "copy (blk beam-block)

Returns an exact copy of the supplied block.  If the block vertices
need to be reflected, do it to the copied new block."

  (make-beam-block (name blk)
		   :transmission (transmission blk)
		   :z (z blk)
		   :vertices (mapcar #'(lambda (pt)
					   (list (first pt)
						 (second pt)))
				       (vertices blk))
		   :display-color (display-color blk)))

;;;---------------------------------------------
;;; End.
