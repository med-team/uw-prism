;;;
;;; beam-graphics
;;;
;;; code for drawing beam portals in various views - includes code to
;;; project portals from the collimator coordinate system to the view
;;; plane, to clip them, and then to draw them.
;;;
;;; 18-Sep-1992 I. Kalet created from old prism files
;;; 02-Dec-1992 J. Unger eliminate table keyword param from draw
;;; method - use slot in beam, modify project-portal to eliminate
;;; radian-degree problems.
;;; 14-Dec-1992 J. Unger modify draw method to operate on view's foreground
;;; display list, move beam-view-mediator definition and init-instance here.
;;; 29-Dec-1992 J. Unger change angles to degrees, extend draw method
;;; for beams into views to work for sag/cor views as well as
;;; transverse views.
;;; 08-Jan-1993 J. Unger add 'mag' back into project portal; warning
;;; msg below.
;;; 20-Jan-1993 J. Unger modify logic of bev init-inst after method
;;; and destroy method to handle bev and non-bev's correctly. added
;;; primary-beam macro.
;;; 15-Feb-1993 I. Kalet get src-to-center from therapy-machine for
;;; the beam being drawn.  Also, update color attributes in primitives
;;; when redrawing.
;;; 11-Apr-1993 I. Kalet modify draw method for bev, since
;;; beams-eye-view has ref. to beam, not copies of parameters.  Delete
;;; primary-beam macro - eq does it now.
;;;  5-Sep-1993 I. Kalet move beam-view-mediator code to beam-mediators
;;; 07-Mar-1994 D. Nguyen modify project-portal to accept a beam transform
;;; and add get-transverse-beam-transform.
;;; 28-Mar-1994 J. Unger split off part of get-beam-transform for bev's
;;; into a function called make-col-pat-xfm, which can be used
;;; elsewhere.
;;; 18-Apr-1994 I. Kalet revised for new def. of view origin.
;;; 17-May-1994 I. Kalet modify project-portal to handle contours that
;;; do not repeat the first point as the last.  Also use collimator
;;; angle of 0.0 for multileaf collimator portals.
;;;  3-Jun-1994 I. Kalet draw blocks if any along with beam portal.
;;;  Check if the block has vertices before attempting to draw it.
;;; 23-Jun-1994 J. Unger add code to draw beam's isocenter and central
;;;  axis in views.
;;; 07-Jul-1994 J. Unger fixup wedge drawing code.
;;; 12-Jul-1994 J. Unger move compute-tics & supporting code to misc module.
;;; 27-Jul-1994 J. Unger fix bug in interpolate-x-y (replace 'eq' w/ '=').
;;; 24-Aug-1994 J. Unger fix bug where mlc wedge wouldn't rotate in 
;;; orthogonal views.
;;; 26-Aug-1994 J. Unger make same fix as 8/24 for other beams in bev.
;;; 18-Sep-1994 J. Unger blocks displayed in their own colors, primary bev 
;;; beams displayed with small distinguishing marks at vertices.
;;; 03-Oct-1994 J. Unger display central axis only when display-axis attrib
;;; is true.
;;; 03-Oct-1994 J. Unger display beam portals as dashed lines, other beam
;;; accoutrements as solid lines.
;;; 04-Oct-1994 J. Unger move find-dashed-color to misc module.
;;; 10-Oct-1994 J. Unger ensure beam & related graphics drawn correctly
;;; in bev where the beam is not the primary bev beam.
;;; 12-Jan-1995 I. Kalet remove proclaim form, use isodist function.
;;; Use table-position from views not from beams.
;;;  5-Sep-1995 I. Kalet change some macros to functions, add
;;;  declarations for fast arithmetic, eliminate some local variables,
;;;  use pix-x and pix-y, eliminate get-col-pat-transform since it is
;;;  the same as get-transverse-beam-transform at z = 0.0, rewrite
;;;  scale-and-clip-lines for efficiency.
;;;  8-Oct-1996 I. Kalet split off get-beam-transform methods into
;;; beam-transforms module, split off block drawing and wedge drawing
;;; to beam-block-graphics and wedge-graphics, but still draw the
;;; wedge with the beam here.  Consolidate drawing of primary beam
;;; portal, moved almost all stuff particular to beams-eye-views,
;;; including marker constants, to bev-graphics module.  Put package
;;; name on find-dashed-color, now in SLIK.  Move clipping code to
;;; pixel-graphics to remove circularity with wedge-graphics.  Move
;;; get-segments-prim and get-rectangles-prim to view-graphics.
;;;  5-Dec-1996 I. Kalet don't generate new graphic primitives if
;;;  color is sl:invisible.
;;; 24-Jan-1997 I. Kalet portal function returns only vertices, not
;;; contour object.
;;;  1-Mar-1997 I. Kalet update calls to nearly- functions.
;;; 03-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx.
;;; 20-Jan-1998 I. Kalet change to array instead of multiple values
;;; for beam transforms, add lots of declarations.
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible
;;; 23-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;; 23-Oct-1999 I. Kalet preserve declutter information if beam was
;;; already in view - the visible attribute of the graphic prim.
;;; 20-Sep-2002 I. Kalet punt on oblique view and room view
;;; 22-Jun-2007 I. Kalet take out inappropriate locally declare in macros
;;; 25-May-2009 I. Kalet remove room-view stub method for draw function
;;;

(in-package :prism)

;;;----------------------------------------------

(defconstant *central-axis-tic-length* 8)
(defconstant *isocenter-radius* 8)
(defconstant *ray-distance* 1000.0)
(defconstant *z-tol* 0.1)

;;;----------------------------------------------

(defmacro interpolate-x-y (x1 y1 z1 x2 y2 z2 xp yp zp cut-ratio)

  "interpolate-x-y x1 y1 z1 x2 y2 z2 xp yp zp

calculate (and setf) the value of xp and yp, given the line segment
from (x1,y1,z1) to (x2,y2,z2), with z1 <= zp <= z2"

  `(setf ,cut-ratio (if (= ,z1 ,z2) 0.0
		      (/ (- ,zp ,z1) (- ,z2 ,z1)))
	 ,xp (+ ,x1 (* ,cut-ratio (- ,x2 ,x1)))
	 ,yp (+ ,y1 (* ,cut-ratio (- ,y2 ,y1)))))

;;;----------------------------------------------

(defun project-x-y (x y x-src y-src mag)
  
  "project-x-y x y x-src y-src mag

given an orgin o (x-src, y-src) and a point p (x,y), return the
point at the tip of vector (p-o) * mag."

  (declare (single-float x y x-src y-src mag))
  (list (+ x-src (* mag (- x x-src)))
	(+ y-src (* mag (- y y-src)))))

;;;----------------------------------------------

(defmacro interpolate-and-project (xa ya za xc yc zc
				   xs ys cut-z z-mag cut-ratio)

  "interpolate-and-project xa ya za xc yc zc xs ys cut-z z-mag

interpolate the point between xa,ya,za and xc,yc,zc using cut-z
and then return the end point of the vector from xs,ys to
cut-x,cut-y using z-mag."

  `(progn
     (setf ,cut-ratio
       (cond ((= ,za ,zc) 0.0) (t (/ (- ,cut-z ,za) (- ,zc ,za)))))
     (project-x-y 
      (+ ,xa (* ,cut-ratio (- ,xc ,xa)))
      (+ ,ya (* ,cut-ratio (- ,yc ,ya)))
      ,xs ,ys ,z-mag)))

;;;----------------------------------------------

(defmacro ray-endpoint (x-src y-src cut-x cut-y tolerance ray-end-x)

  `(cond 
    ((poly:nearly-equal ,cut-x ,x-src ,tolerance) ;; ray is vertical
     (list ,x-src (+ ,y-src
		     (if (> ,cut-y ,y-src) *ray-distance*
		       (- *ray-distance*)))))
    (t
     (setf ,ray-end-x (+ ,x-src 
			 (if (> ,cut-x ,x-src) *ray-distance*
			   (- *ray-distance*))))
     (list ,ray-end-x
	   (+ ,y-src (* (/ (- ,cut-y ,y-src) (- ,cut-x ,x-src))
			(- ,ray-end-x ,x-src)))))))

;;;----------------------------------------------

(defun lt (a b)

  (declare (single-float a b))
  (if  (>= b 0.0)  (<= a b)  (> a b)))

;;;----------------------------------------------

(defconstant *pc-tolerance*      0.99)
(defconstant *pc-mini-tolerance* .001)
(defconstant *pc-z-mag*          2.0)

;;;----------------------------------------------

(defun project-contour (pt-list src-x src-y src-z)

  "project-contour pt-list src

Local procedure to do the projection when the source point is
NOT in the projection plane."

  (declare (single-float src-x src-y src-z))
  (let* ((prev nil)
	 (next nil)
	 (remainder pt-list)
	 (a-bit-less-than-z (* src-z *pc-tolerance*))
	 (cut-z (* src-z 0.5))
	 (mag-ok (not (poly:nearly-equal cut-z 0.0 *pc-mini-tolerance*)))
	 (xc 0.0) (yc 0.0) (zc 0.0)
	 (cut-ratio 1.0)
	 (out-list nil))
    (declare (single-float cut-ratio xc yc zc cut-z a-bit-less-than-z))
    (dolist (pt pt-list)
      (setq remainder (rest remainder)
	    next (first remainder)
	    xc (first pt)
	    yc (second pt)
	    zc (third pt))
      (if (lt zc a-bit-less-than-z)
	  ;;then  -- z of contour point < z of source  -- simple
	  (push (project-x-y xc yc src-x src-y
			     (/ src-z (- src-z zc)))
		out-list)
	;;else  -- z of contour point >= z of source -- tricky
	(when mag-ok
	  (progn
	    ;; previous point
	    (when (and prev (lt (third prev) a-bit-less-than-z))
	      (push (interpolate-and-project
		     (the single-float (first prev))
		     (the single-float (second prev))
		     (the single-float (third prev))
		     xc yc zc src-x src-y cut-z
		     *pc-z-mag* cut-ratio)
		    out-list))
	    ;; next point
	    (when (and next (lt (third next) a-bit-less-than-z))
	      (push (interpolate-and-project
		     (the single-float (first next))
		     (the single-float (second next))
		     (the single-float (third next))
		     xc yc zc src-x src-y cut-z
		     *pc-z-mag* cut-ratio)
		    out-list)))))
      (setq prev pt))
    ;;return list of x-y coords
    out-list))

;;;----------------------------------------------

(defconstant *tc-tolerance* 0.0001)

;;;----------------------------------------------

(defun traverse-contour (pt-list src-x src-y)

  "traverse-contour pt-list src

Local procedure to handle case where beam source lies in plane of
projection."

  (declare (type single-float  src-x src-y))
  (let* ((cut-x 0.0) (cut-y 0.0)
	 (remainder pt-list)
	 (next nil)
	 (src-x-y (list src-x src-y))
	 (ray-end nil)
	 (outward t)
	 (xc 0.0) (yc 0.0) (zc 0.0)
	 (xn 0.0) (yn 0.0) (zn 0.0)
	 (cut-ratio 1.0) (re 0.0)
	 (out-list nil))
    (declare (single-float cut-x cut-y cut-ratio xc yc zc xn yn zn re))
    (dolist (c pt-list)
      (setq remainder (rest remainder))
      (setq next (first remainder))
      (cond (next (setq zc (third c)
			zn (third next))
		  ;; if crossing, interpolate segment
		  (when (or (and (>= zc 0.0) (<= zn 0.0)) 
			    (and (<= zc 0.0) (>= zn 0.0)))
		    (setq xc (first c)
			  yc (second c)
			  xn (first next)
			  yn (second next))
		    (interpolate-x-y xc yc zc
				     xn yn zn
				     cut-x cut-y 0.0 cut-ratio)
		    (setq ray-end 
		      (ray-endpoint src-x src-y cut-x cut-y
				    *tc-tolerance* re))
		    (cond (outward (push src-x-y out-list)
				   (push ray-end out-list)
				   (setf outward nil))
			  (t (push ray-end out-list)
			     (push src-x-y out-list)
			     (setq outward t)))))))
    ;; return list of x-y coords
    out-list))

;;;----------------------------------------------

(defun project-portal (portal src-to-center bt pos)

  "project-portal portal src-to-center bt pos

Projects portal, a list of vertices presumed to be at z = 0.0, from
distance src-to-center onto the view plane whose beam transform is
represented in the array bt, at position pos.  Returns a list of
vertices depicting the connected set of segments comprising the
portal's projection into view plane."

  (declare (type (simple-array single-float (12)) bt)
	   (type single-float src-to-center pos))
  (let* ((r00 (aref bt 0))
	 (r01 (aref bt 1))
	 (r03 (aref bt 3))
         (r10 (aref bt 4))
	 (r11 (aref bt 5))
	 (r13 (aref bt 7))
         (r20 (aref bt 8))
	 (r21 (aref bt 9))
	 (r23 (aref bt 11))
         (mag (float (/ (- src-to-center pos) src-to-center)))
	 (px 0.0) (py 0.0)
	 (src-x (+ (* (aref bt 2) src-to-center) r03))
	 (src-y (+ (* (aref bt 6) src-to-center) r13))
	 (src-z (+ (* (aref bt 10) src-to-center) r23))
	 (out-list nil))
    (declare (single-float
	      r00 r01 r03 r10 r11 r13 r20 r21 r23 
	      px py src-x src-y src-z mag))

    ;; See Jacky and Kalet, Computerized Medical Imaging and Graphics,
    ;; Vol. 14, 1990, pp. 97-105, for the algorithm.  For efficiency,
    ;; we code matrix multiplication inline, since many terms are
    ;; known to be zero.
    ;; src is the coordinates of the beam source in view space; ie:
    ;;
    ;;       [ r00  r01  r02 ]   [      0        ]   [ couch x ]
    ;; src = [ r10  r11  r12 ] * [      0        ] + [ couch y ]
    ;;       [ r20  r21  r22 ]   [ src-to-center ]   [ couch z ]

    (dolist (pt (append portal (list (first portal))))
      (setq px (* mag (the single-float (first pt)))
	    py (* mag (the single-float (second pt))))
      (push (list (+ (* r00 px) (* r01 py) r03)
		  (+ (* r10 px) (* r11 py) r13)
		  (+ (* r20 px) (* r21 py) r23))
	    out-list))
    (if (poly:nearly-equal src-z 0.0 *z-tol*)
	(traverse-contour out-list src-x src-y)
      (project-contour out-list src-x src-y src-z))))

;;;----------------------------------------------

(defun draw-portal (prim portal bt sad v)

  "draw-portal prim portal bt sad v

Draws portal, a list of vertices, into the supplied graphic primitive,
using beam transform bt, source to axis distance sad, and a number of
attributes of the primitive's view v."

  (setf (points prim)
    (append (scale-and-clip-lines
	     (project-portal portal sad bt
			     (if (typep v 'beams-eye-view)
				 (view-position v) 0.0))
	     (scale v) (x-origin v) (y-origin v) 0 0 
	     (sl:width (picture v)) (sl:height (picture v)))
	    (points prim))))

;;;----------------------------------------------

(defun draw-isocenter (prim bt scale x-origin y-origin)

  "draw-isocenter prim bt scale x-origin y-origin

Draws an isocenter icon into graphic primitive prim, based upon beam
transform bt and the provided view plane scale, x-origin, and
y-origin."

  (declare (single-float scale) (fixnum x-origin y-origin)
	   (type (simple-array single-float (12)) bt))
  (when (poly:nearly-equal (aref bt 11) 0.0 *z-tol*)
    (setf (points prim) (append 
			 (draw-diamond-icon 
			  (list (aref bt 3) (aref bt 7))
			  scale x-origin y-origin *isocenter-radius*)
			 (points prim)))))

;;;----------------------------------------------

(defun draw-central-axis (prim bt sad scale x-origin y-origin)

  "draw-central-axis prim bt sad scale x-origin y-origin

Draws a central axis icon into the graphic primitive prim, based upon
beam transform bt, source to axis distance sad, and the provided view
plane scale, x-origin, and y-origin.  If the central axis lies in the
view plane, a line segment with tic marks spaced 1 cm apart is drawn.
If the central axis crosses through the view plane, a plus sign is
drawn at the point of intersection, unless the point of intersection
is the isocenter, in which case nothing is drawn (the drawing of the
isocenter is handled elsewhere).  If the central axis does not
intersect the plane, nothing is drawn."

  (declare (single-float sad scale) (fixnum x-origin y-origin)
	   (type (simple-array single-float (12)) bt))
  (let* ((r03 (aref bt 3))
         (r13 (aref bt 7))
         (r23 (aref bt 11))
         (src-x (+ (* (aref bt 2) sad) r03))
	 (src-y (+ (* (aref bt 6) sad) r13))
	 (src-z (+ (* (aref bt 10) sad) r23))
         (iso-in-plane (poly:nearly-equal r23 0.0 *z-tol*))
         (src-in-plane (poly:nearly-equal src-z 0.0 *z-tol*)))
    (declare (single-float r03 r13 r23 src-x src-y src-z))
    (cond
     ((and src-in-plane iso-in-plane) ;; axis in plane
      (let ((end-x (- (* 2.0 r03) src-x))
	    (end-y (- (* 2.0 r13) src-y)))
	(setf (points prim)
	  (nconc (pixel-segments (list (list src-x src-y end-x end-y))
				 scale x-origin y-origin)
		 (compute-tics src-x src-y end-x end-y 
			       scale x-origin y-origin
			       *central-axis-tic-length*)
		 (points prim)))))
     ((not (poly:nearly-equal src-z r23 *z-tol*)) ; axis crosses plane
      (unless iso-in-plane
	(let* ((fac (/ src-z (- src-z r23)))
	       (isec-x (+ src-x (* fac (- r03 src-x))))
	       (isec-y (+ src-y (* fac (- r13 src-y)))))
	  (declare (single-float fac isec-x isec-y))
	  (setf (points prim)
	    (append
	     (draw-plus-icon (list isec-x isec-y) 
			     scale x-origin y-origin *isocenter-radius*)
	     (points prim)))))))))

;;;----------------------------------------------

(defmethod draw ((b beam) (v view))

  "draw (b beam) (v view)

Computes the projection of beam b into orthogonal view v and adds two
graphics primitives, solid and dashed, containing the projected
segments to v's foreground display list.  This includes the drawing of
the beam's isocenter and central axis, and the wedge.  Does NOT draw
the beam's blocks."

  ;; start with new gp's each time, to avoid having to look for 
  ;; and disambiguate the solid and dashed segment-prims, which
  ;; would be very complicated.  But first catch the visible attribute
  ;; of a beam graphic prim if present.
  (let ((visible (aif (find b (foreground v) :key #'object)
		      (visible it) t)))
    (setf (foreground v) (remove b (foreground v) :key #'object))
    (unless (eql (display-color b) 'sl:invisible)
      (let* ((solid-clr (sl:color-gc (display-color b)))
	     (solid-prim (get-segments-prim b v solid-clr))
	     (dashed-prim (get-segments-prim
			   b v (sl:find-dashed-color solid-clr)))
	     (bt (beam-transform b v))
	     (sad (isodist b))
	     (scale (scale v))
	     (x-orig (x-origin v))
	     (y-orig (y-origin v))
	     (pic (picture v))
	     (wdg (wedge b)))
	(setf (visible solid-prim) visible)
	(setf (visible dashed-prim) visible)
	(draw-portal dashed-prim (portal (collimator b)) bt sad v)
	(draw-isocenter solid-prim bt scale x-orig y-orig)
	(when (display-axis b)
	  (draw-central-axis solid-prim bt sad scale x-orig y-orig))
	(unless (zerop (id wdg))
	  (draw-wedge solid-prim
		      (beam-transform b v t)
		      sad
		      (rotation wdg) 
		      scale x-orig y-orig
		      (sl:width pic) (sl:height pic)))))))

;;;----------------------------------------------

(defmethod draw ((b beam) (v oblique-view))

  "stub to prevent crashes - just don't draw it until we figure out
  the transforms."

  )

;;;----------------------------------------------
;;; End.
