;;;
;;; locators
;;;
;;; This file includes the locator class, and the mediators required
;;; to maintain the locator bars that reference other views in a given
;;; view.
;;;
;;; 18-Jan-1993 I. Kalet taken from views.cl
;;; 28-Jan-1994 I. Kalet locator bar grab boxes at last, also move a
;;; little bit of code to views module.
;;; 18-Apr-1994 I. Kalet update to use new pickable objects facility.
;;; Move some code here from views.  Uodate refs to view origin.
;;; 22-May-1994 I. Kalet ignore grab box motion unless button 1 down
;;;  8-Jan-1995 I. Kalet destroy locators when a view is deleted (in
;;;  delete-intersect code) in order to free the line gcontext
;;; 12-Mar-1995 I. Kalet in view-set-mediator, call display-view in
;;; lambda function for view deleted, only for the view that remains,
;;; not in delete-intersect.  This allows the view to be destroyed as
;;; well as deleted.
;;;  3-Sep-1995 I. Kalet add coerce to single-float in locator
;;;  position update from grab box.
;;; 19-Sep-1996 I. Kalet take out keywords and &rest from draw methods.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;; 13-Oct-2002 I. Kalet don't make locators for bev, oblique or room views.
;;; 25-May-2009 I. kalet remove ref to room view altogether.
;;;

(in-package :prism)

;;;-------------------------------------

(defvar *horiz-intersect-table*
  '((transverse-view coronal-view)
    (coronal-view sagittal-view)
    (coronal-view transverse-view))
  "The for-view in-view class name pairs that correspond to horizontal
locator bars.")

;;;-------------------------------------

(defun find-orient (for-v in-v)

  "find-orient for-v in-v

returns either :horizontal or :vertical depending on how the locator
bar should be oriented in in-v for for-v."

  (let ((f-cl (class-name (class-of for-v)))
	(v-cl (class-name (class-of in-v))))
    (if (member (list f-cl v-cl) *horiz-intersect-table* :test #'equal)
	:horizontal
      :vertical)))

;;;-------------------------------------

(defclass locator ()

  ((in-view :accessor in-view
	    :initarg :in-view
	    :documentation "The view in which this locator appears.")

   (loc-position :type single-float
		 :accessor loc-position
		 :initarg :loc-position
		 :documentation "The real space position of the
locator bar in its view.")

   (new-position :type ev:event
		 :accessor new-position
		 :initform (ev:make-event)
		 :documentation "Announced when the locator bar
position changes.")

   (orient :type (member :horizontal :vertical)
	   :accessor orient
	   :initarg :orient
 	   :documentation "The orientation of this locator in the view
in which it appears.")

   (visible :accessor visible
	    :initform t
	    :documentation "T if the bar is specified as visible by
the view it represents, false if it should not appear.")

   (line-gc :accessor line-gc
	    :initarg :line-gc
	    :initform (sl:make-duplicate-gc (sl:color-gc 'sl:blue))
	    :documentation "The color gc for the locator line.")

   )

  (:documentation "A locator bar is a line drawn in a view to
represent the view-position of another orthogonal view.")

  )

;;;--------------------------------------

(defun locator-pos (loc)

  "locator-pos loc

returns the pixel position of the locator in its view."

  (let* ((v (in-view loc))
	 (x0 (x-origin v))
	 (y0 (y-origin v))
	 (raw-pix (round (* (scale v) (loc-position loc))))) ;; cm to pix
    (if (eql (orient loc) :horizontal)
	(typecase v
	  (coronal-view (+ y0 raw-pix))
	  (t (- y0 raw-pix)))
      (+ x0 raw-pix))))

;;;--------------------------------------

(defun locator-box-xy (loc)

  "locator-box-xy loc

returns as multiple values the x and y pixel coordinates of the
location where the locator grab box for locator loc should go."

  (let ((horiz (eql (orient loc) :horizontal))
	(pos (locator-pos loc))
	(wid (clx:drawable-width
	      (sl:window (picture (in-view loc))))))
    (values (if horiz (- wid 20) ;; arbitrary - right hand side
	      pos)
	    (if horiz pos
	      (- wid 20))))) ;; arbitrary - bottom

;;;-------------------------------------

(defmethod draw ((l locator) (v view))

  "This draw method just draws the locator in the view with the
current gcontext.  It does not check for visible etc.  It adds or
updates a graphic primitive in the view's foreground list."

  (let* ((wid (clx:drawable-width (sl:window (picture v))))
	 (horiz (eql (orient l) :horizontal))
	 (pos (locator-pos l))
	 (x1 (if horiz 0 pos))
	 (x2 (if horiz wid pos))
	 (y1 (if horiz pos 0))
	 (y2 (if horiz pos wid))
	 (bar (list x1 y1 x2 y2))
	 (segs-prim (find l (foreground v) :key #'object)))
    (if segs-prim (setf (points segs-prim) bar)
      (push (make-segments-prim bar (line-gc l) :object l)
	    (foreground v)))))

;;;--------------------------------------

(defmethod destroy ((l locator))
  
  (clx:free-gcontext (line-gc l)))
 
;;;--------------------------------------

(defmethod initialize-instance :after ((loc locator) &rest initargs)

  "registers with view refresh-fg, makes grab box and registers with
it, adds locator to in-view locators set."

  (declare (ignore initargs))
  (let ((in-v (in-view loc))
	(grab-box (multiple-value-bind (x y) (locator-box-xy loc)
		    (sl:make-square loc x y))))
    (sl:add-pickable-obj grab-box (picture in-v))
    (if (and (visible loc) (local-bars-on in-v))
	(progn (draw loc in-v)
	       (display-view in-v)) ;; because it is a new one
      (setf (sl:enabled grab-box) nil)) ;; because default is t
    ;; update grab box and redraw locator bar when refreshing view
    ;; because scale or origin may have changed
    (ev:add-notify loc (refresh-fg in-v)
		   #'(lambda (l v)
		       (when (and (local-bars-on v) (visible l))
			 (let ((gb (first (sl:find-pickable-objs
					   l (picture v)))))
			   ;; update position of grab box
			   (multiple-value-bind (x y) (locator-box-xy l)
			     (setf (sl:x-center gb) x
				   (sl:y-center gb) y))
			   ;; and refresh the locator bar
			   (draw l v)))))
    (ev:add-notify loc (sl:motion grab-box)
		   #'(lambda (l gb x y state)
		       (when (member :button-1
				     (clx:make-state-keys state))
			 (sl:update-pickable-object gb x y)
			 (let* ((v (in-view l))
				(x0 (x-origin v))
				(y0 (y-origin v))
				(horiz (eql (orient l) :horizontal))
				(pos (if horiz y x)))
			   (setf (loc-position l)
			     (coerce (/ (if horiz
					    (typecase v
					      (coronal-view (- pos y0))
					      (t (- y0 pos)))
					  (- pos x0))
					(scale v))
				     'single-float))))))
    (ev:add-notify loc (sl:deselected grab-box)
		   #'(lambda (l gb)
		       (declare (ignore gb))
		       (ev:announce l (new-position l)
				    (loc-position l))))
    (coll:insert-element loc (locators in-v))))

;;;--------------------------------------

(defmethod (setf loc-position) :after (new-pos (l locator))

  "sets the loc-position attribute of locator l to new-pos and
announces new-position.  Redraws only if already drawn."

  (let* ((v (in-view l))
	 (gb (first (sl:find-pickable-objs l (picture v)))))
    ;; update position of grab box in view picture's pick-list
    (multiple-value-bind (x y) (locator-box-xy l)
      (setf (sl:x-center gb) x
	    (sl:y-center gb) y))
    ;; then draw locator if called for
    (when (and (visible l) (local-bars-on v))
      (draw l v)
      (display-view v))
    (ev:announce l (new-position l) new-pos)))

;;;--------------------------------------

(defun locator-draw-box-enable (l)

  "locator-draw-box-enable l

sets enable flag on grab box for locator l and draws it or deletes it
from the in-view display list."

  (let* ((v (in-view l))
	 (gb (first (sl:find-pickable-objs l (picture v)))))
    (if (and (local-bars-on v) (visible l))
	(progn
	  (setf (sl:enabled gb) t) ;; turn on grab boxes
	  (draw l v)) ;; draw bars that are visible
      (progn
	(setf (sl:enabled gb) nil) ;; turn off grab boxes
	(setf (foreground v) ;; remove others from display list
	  (remove l (foreground v) :test #'eq :key #'object))))))

;;;--------------------------------------

(defmethod (setf visible) :after (val (l locator))

  "draws or erases the locator bar as needed."

  (declare (ignore val))
  (locator-draw-box-enable l)
  (display-view (in-view l)))

;;;-------------------------------------

(defmethod (setf local-bars-on) :after (on (v view))

  "Redraws the locator graphics.  Provided with locators instead of
views since it depends on locator stuff and supplements the standard
method."

  (declare (ignore on))
  (mapc #'locator-draw-box-enable (coll:elements (locators v)))
  (display-view v))

;;;--------------------------------------

(defclass view-locator-mediator ()

  ((locator :accessor locator
	    :initarg :locator
	    :documentation "The locator this mediator manages.")

   (for-view :accessor for-view
	     :initarg :for-view
	     :documentation "The view that this locator represents.")

   (busy :accessor busy
	 :initform nil)

   )

  (:documentation "This mediator maintains the relation between a
locator and the view it represents.")

  )

;;;---------------------------------------

(defmethod initialize-instance :after ((vlm view-locator-mediator)
				       &rest initargs)

  (declare (ignore initargs))
  (ev:add-notify vlm (new-position (locator vlm))
		 #'(lambda (med a pos)
		     (declare (ignore a))
		     ;; check if grab-box active - if so, don't set
		     ;; the view-position yet
		     (let* ((loc (locator med))
			    (vw (in-view loc))
			    (grab-box (first (sl:find-pickable-objs
					      loc (picture vw)))))
		       (when (and (not (busy med))
				  (not (sl:active grab-box)))
			 (setf (busy med) t)
			 (setf (view-position (for-view med)) pos)
			 (setf (busy med) nil)))))
  (ev:add-notify vlm (new-position (for-view vlm))
		 #'(lambda (med v position)
		     (declare (ignore v))
		     (when (not (busy med))
		       (setf (busy med) t)
		       (setf (loc-position (locator med)) position)
		       (setf (busy med) nil))))
  (ev:add-notify vlm (remote-bars-toggled (for-view vlm))
		 #'(lambda (med a on)
		     (declare (ignore a))
		     (setf (visible (locator med)) on))))

;;;--------------------------------------

(defclass view-set-mediator ()

  ((views :accessor views
	  :initarg :views
	  :documentation "The set of all views in the plan.")

   (locator-mediators :accessor locator-mediators
		      :initform (coll:make-collection)
		      :documentation "The set of view-locator-mediators.")
   )

  (:documentation "This mediator maintains the relations between views
in a set in the face of addition or deletion of a view.")

  )

;;;---------------------------------------

(defun make-view-set-mediator (view-set)

  "make-view-set-mediator view-set

returns an instance of a view-set-mediator with view-set as its
initial set of views."

  (make-instance 'view-set-mediator :views view-set))

;;;---------------------------------------

(defun add-intersect (for-v in-v vsm)

  "add-intersect for-v in-v vsm

adds a locator to the locators in in-v for for-v, adds a locator
mediator to view-set-mediator vsm."

  (unless (or (typep for-v 'beams-eye-view)
	      (typep in-v 'beams-eye-view)
	      (typep for-v 'oblique-view)
	      (typep in-v 'oblique-view))
    (coll:insert-element
     (make-instance 'view-locator-mediator
       :locator (make-instance 'locator
		  :in-view in-v
		  :loc-position (view-position for-v)
		  :orient (find-orient for-v in-v)
		  :line-gc (sl:make-duplicate-gc
			    (sl:color-gc (sl:border-color (picture for-v)))))
       :for-view for-v)
     (locator-mediators vsm))))

;;;---------------------------------------

(defun delete-intersect (for-v in-v vsm)

  "delete-intersect for-v in-v vsn

deletes the locator and locator mediator for the combination of for-v
and in-v."

  (let* ((loc-m-set (locator-mediators vsm))
	 (loc-m (coll:collection-member	; find the locator mediator
		 (list for-v in-v) loc-m-set
		 :test #'(lambda (view-pair lm)
			   (if (equal (list (for-view lm)
					    (in-view (locator lm)))
				      view-pair)
			       lm nil))))
	 (locator (if loc-m (locator loc-m))))
    (when loc-m
      (coll:delete-element locator (locators in-v))
      (ev:remove-notify locator (refresh-fg in-v))
      (setf (foreground in-v) (remove locator (foreground in-v)
				      :test #'eq :key #'object))
      (sl:remove-pickable-objs locator (picture in-v))
      (ev:remove-notify loc-m (new-position for-v))
      (ev:remove-notify loc-m (remote-bars-toggled for-v))
      (coll:delete-element loc-m loc-m-set)
      (destroy locator)))) ;; to free the gcontext

;;;---------------------------------------

(defmethod initialize-instance :after ((view-sm view-set-mediator)
				       &rest initargs)

  (declare (ignore initargs))
  (ev:add-notify view-sm (coll:inserted (views view-sm))
		 #'(lambda (vsm view-set v)
		     (mapc #'(lambda (v1)
			       (when (not (equal (class-name
						  (class-of v))
						 (class-name
						  (class-of v1))))
				     (add-intersect v v1 vsm)
				     (add-intersect v1 v vsm)))
			   (coll:elements view-set))))
  (ev:add-notify view-sm (coll:deleted (views view-sm))
		 #'(lambda (vsm view-set v)
		     (mapc #'(lambda (v1)
			       (delete-intersect v v1 vsm)
			       (delete-intersect v1 v vsm)
			       (display-view v1))
			   (coll:elements view-set)))))

;;;---------------------------------------
;;; End.
