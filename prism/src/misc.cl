;;;
;;; misc - miscellaneous functions needed in various Prism modules
;;;
;;;  1-Aug-1992 I. Kalet created from the old sys-tools package
;;; 13-Nov-1992 I. Kalet change half-between to average, put
;;; get-string, get-number here instead of views
;;;  2-Jul-1993 I. Kalet put insert-at and delete-at macros here
;;; 15-Feb-1994 I. Kalet add run-subprocess def for Lucid.
;;; 19-Apr-1994 J. Unger move fix-float here from dose-panels
;;; 21-Apr-1994 J. Unger add draw-on-picture
;;; 25-Apr-1994 J. Unger add optimization to fix-float.
;;; 12-Jul-1994 J. Unger add compute-tics & supporting code fm beam-graphics
;;; 11-Aug-1994 J. Unger enhance def of run-subprocess to support :wait param
;;; 04-Oct-1994 J. Unger move solid <--> dashed color transformations here
;;; from other places.
;;; 04-Oct-1994 J. Unger fix omission in find-dashed/solid-color
;;; functions.
;;;  8-Jan-1995 I. Kalet remove proclaim form and VAX, Lucid support
;;;  4-Sep-1995 I. Kalet change some macros to functions, remove
;;; average, since it is never used.  Add type declarations for fast
;;; arithmetic.  Move compute-tics and pixel-segments to
;;; contour-graphics since they now use pix-x and pix-y.
;;; [contour-graphics now renamed to pixel-graphics - BobGian]
;;;  1-Oct-1995 I. Kalet in Allegro version of run-subprocess, use
;;;  excl:run-shell-command for both :wait t and :wait nil cases.
;;;  This should fix an error that occurs with excl:shell on the SGI
;;;  in Be'er Sheva.
;;; 15-Jan-1996 I. Kalet put average back in - used in coll-panels
;;;  8-Oct-1996 I. Kalet move find-dashed-color and find-solid-color
;;;  to clx-support, in SLIK.
;;; 21-Jan-1997 I. Kalet add macros to return coords of a simple 3
;;; component vector.
;;;  1-Mar-1997 I. Kalet change NEARLY- macros to functions, change
;;;  key to optional instead of keyword parameter.  Also change
;;;  AVERAGE to function.
;;;  8-May-1997 BobGian add SQR; inline SQR, AVERAGE.
;;;  8-May-1997 BobGian change (EXPT (some-form) 2) to (SQR
;;;  (some-form)).
;;;  8-Jun-1997 I. Kalet remove draw-on-picture, replaced by new SLIK
;;;  button type, icon-button.
;;;  3-Jul-1997 BobGian remove NEARLY-EQUAL, NEARLY-INCREASING, and
;;;   NEARLY-DECREASING from this file; they were duplicated here.  All
;;;   are now in math.cl and in the POLYGONS package.  (PRISM system now
;;;   explicitly depends on POLYGONS system.)  Updated all calls throughout
;;;   PRISM to use the new definitions.
;;;  3-Oct-1997 BobGian remove AVERAGE, LO-HI-COMPARE - now expanded in-place.
;;; 27-Oct-1997 BobGian redefine SQR as macro to force compiler to inline it.
;;;  Allegro compiler does not obey INLINE decl for user-defined functions,
;;;  which is perfectly legal by CommonLisp spec.
;;; 27-Jan-1998 I. Kalet add insert function, remove insert-at and
;;; delete-at macros, add anaphoric macros from Graham, On Lisp.
;;; 24-Dec-1998 I. Kalet fix up run-subprocess a little, and change
;;; upper case to lower.
;;; 23-Jan-2002 I. Kalet add listify function, add nearest function
;;; 17-Feb-2005 A. Simms add getenv function as an implementation neutral
;;;  mechanism to access environment variables from Allegro and CMUCL.
;;; 18-Apr-2005 I. Kalet cosmetic fixes
;;; 22-Jun-2007 I. Kalet take out declarations in vx, vy and vz macros
;;; - they are unnecessary and cause warnings in other functions.
;;;

(in-package :prism)

;;;------------------------------------------

(defun date-time-string ()
  
  "date-time-string 

Takes no parameters and returns the current system date and time as a
string."
  
  (multiple-value-bind
      (second minute hour date month year) (get-decoded-time)
    (format nil "~d-~[Jan~;Feb~;Mar~;Apr~;May~;Jun~;Jul~;Aug~;~
                 Sep~;Oct~;Nov~;Dec~]-~d ~d:~2,'0d:~2,'0d"
            date (1- month) year hour minute second)))

;;;------------------------------------------

(defun listify (str len)

  "listify str len

returns a list of strings, that are sequential substrings of str each
of length len."

  (let ((strlen (length str))
	str-list)
    (dotimes (i (ceiling strlen len) str-list)
      (setf str-list
	(append str-list
		(list (subseq str (* len i) (min (* len (1+ i)) strlen))))))))

;;;------------------------------------------

(defun max-length (s)

  "max-length s

Finds the longest item in sequence s, and returns the length of it.
The items are presumed to be themselves sequences."

  (apply #'max (mapcar #'length s)))

;;;------------------------------------------

(defun nearest (x lst epsilon &optional direction)

  "nearest x lst epsilon &optional direction

returns the value in lst that is closest to x but not within epsilon
of x.  If direction is the keyword :below, the nearest value less than
x is returned, and if :above, the nearest value greater than x.  If
direction is not specified the closest value is returned, except if
there is a tie between the values below and above, the below value is
returned.  If there are not other contours to copy, returns nil."

  (let* ((tmp (remove x lst :test #'(lambda (a b) (poly:nearly-equal
						   a b epsilon))))
	 (less-x (remove x tmp :test #'<))
	 (more-x (remove x tmp :test #'>))
	 (lower (if less-x (apply #'max less-x)))
	 (upper (if more-x (apply #'min more-x))))
    (cond ((eql direction :below) lower)
	  ((eql direction :above) upper)
	  ((null lower) upper)
	  ((null upper) lower)
	  ((poly:nearly-equal (- x lower) (- upper x)) lower)
	  ((< (- x lower) (- upper x)) lower)
	  (t upper))))

;;;------------------------------------------

(defun insert (item lst &key (test #'>) (key #'identity))

  "insert item lst &key (test #'>) (key #'identity)

returns a list with item inserted in the right place in the ordered
list lst, using test as a comparision function and key applied to each
element of lst to provide input to the test function."

  (cond ((null lst) (list item))
	((funcall test
		  (funcall key (first lst))
		  (funcall key item))
	 (cons item lst))
	(t (cons (first lst)
		 (insert item (rest lst) :test test :key key)))))

;;;------------------------------------------
;;; these anaphors are straight out of
;;; Graham, On Lisp (page 191)

(defmacro aif (test-form then-form &optional else-form)
  `(let ((it ,test-form))
     (if it ,then-form ,else-form)))

(defmacro awhen (test-form &body body)
  `(aif ,test-form
        (progn ,@body)))

(defmacro awhile (expr &body body)
  `(do ((it ,expr ,expr))
       ((not it))
     ,@body))

;;;------------------------------------------

(defun enlarge-array-2 (arr x-fac &optional (y-fac x-fac))

  "enlarge-array-2 arr x-fac &optional (y-fac x-fac)

Scales an array up by the given x and y factors (y-fac defaults to
x-fac if only one factor provided) and returns the enlarged array.
Essentially expands each element of the input array to fill a small
cell of elements of the result array.  No fancy interpolation, etc.
Note: x and y fac must be integers."

  (let* ((x-dim  (array-dimension arr 1))
         (y-dim  (array-dimension arr 0))
         (new-x  (* x-fac x-dim))
         (new-y  (* y-fac y-dim))
         (x-amt  0)
         (y-amt  0)
         (y-sum  0)
         (result (make-array (list new-y new-x) 
			     :element-type '(unsigned-byte 16))))

    (declare (type (simple-array (unsigned-byte 16) 2) arr result))
    (declare (fixnum x-fac y-fac x-dim y-dim new-x new-y x-amt y-amt
		     y-sum))

    (dotimes (j y-dim result)
      (declare (fixnum j))
      (setq y-amt (* j y-fac))
      (dotimes (i x-dim result)
	(declare (fixnum i))
	(setq x-amt (* i x-fac))
	(dotimes (v y-fac result)
	  (declare (fixnum v))
	  (setq y-sum (+ y-amt v))
	  (dotimes (u x-fac result)
	    (declare (fixnum u))
	    (setf (aref result y-sum (+ x-amt u)) (aref arr j i))))))))

;;;------------------------------------------
;;; these are macros in order to get the effect
;;; of compiling to inline code
;;;------------------------------------------

(defmacro vx (vec)

  "vx vec

returns the x component of the simple vector vec"

  `(svref ,vec 0))

;;;------------------------------------------

(defmacro vy (vec)

  "vy vec

returns the y component of the simple vector vec"

  `(svref ,vec 1))

;;;------------------------------------------

(defmacro vz (vec)

  "vz vec

returns the z component of the simple vector vec"

  `(svref ,vec 2))

;;;------------------------------------------

(defmacro sqr (x)

  "sqr x

Returns X squared (single-float in/out only)."

  (cond ((symbolp x)
	 ;; Simple case - can evaluate arg twice because it is a variable.
	 `(the single-float (* (the single-float ,x)
			       (the single-float ,x))))
	;;
	;; Slightly harder case - want to avoid double evaluation
	;; of argument form.
	(t (let ((var (gensym)))
	     `(let ((,var (the single-float ,x)))
		(declare (single-float ,var))
		(the single-float (* ,var ,var)))))))

;;;------------------------------------------

(defun get-string (prompt)

  "get-string prompt

Writes the prompt to *standard-output*, waits for input from
*standard-input*, and returns a string typed by the user."

  (princ prompt)
  (let ((str ""))
    (loop
      (setq str (read-line))
      (unless (equal str "") (return str)))))

;;;------------------------------------------

(defun get-number (prompt &optional ll ul)

  "get-number prompt &optional ll ul

Writes the prompt to *standard-output*, waits for input from
*standard-input*, and returns a number typed by the user.  If the
input is outside the range (ll ul), or not a number, the user is
reprompted."

  (let ((stuff ""))
    (loop
      (princ prompt)
      (setq stuff (read))
      (if (numberp stuff)
	  (if (and ll ul) ;; assume values are valid if not nil
	      (if (and (>= stuff ll)
		       (<= stuff ul))
		  (return stuff))
	    (return stuff))
	(format T "Please enter a number ~%")))
    stuff))

;;;------------------------------------------

(defun fix-float (flt int)

  "fix-float flt int

Returns flt, rounded to int significant digits to the right of the
decimal point."

  (let ((pow (expt 10.0 (float int))))
    (declare (single-float flt pow))
    (declare (fixnum int))
    (/ (round (* pow flt)) pow)))

;;;------------------------------------------
;;; versions of run-subprocess for different lisp
;;; implemenations.
;;;------------------------------------------

(defun run-subprocess (command &key (wait t))
  
  "run-subprocess command &key (wait t)

Invokes the string command in a shell as a subprocess.  If the keyword
parameter wait is t (the default), the subprocess is run
synchronously, i.e., the caller waits until the subprocess terminates
before control is returned to it and run-subprocess returns the exit
status.  If wait is nil, the subprocess runs asynchronously and the
function returns immediately, returning the process-id of the shell
that is created."

  #+allegro
  (multiple-value-bind (status v pid) ;; note - if wait is nil, status
      ;; is actually a stream but we don't care in that case
      (excl:run-shell-command (format nil "~a" command) :wait wait)
    (declare (ignore v))
    (if wait status pid))

  #+cmu
  (let (p)
    (setq p (extensions:run-program command :wait wait))
    (cond
     ((extensions:process-p p)
      (if wait
	  (extensions:process-status p)
	(extensions:process-pid p)))
     (t
      nil)))

  )

;;;-----------------------------------

(defun distance (a b c d)

  "distance a b c d

Returns the distance between the point (a,b) and the point (c,d)."

  (declare (single-float a b c d))
  (the single-float (sqrt (+ (sqr (- c a)) (sqr (- d b))))))

;;;-----------------------------------
;;; versions of getenv for different lisp
;;; implemenations.
;;;------------------------------------------

(defun getenv (varname)
  
  "getenv varname

Searches the set of enviornment variables for the name specified.  
If an environment variable of the specified name exists the
value of the variable is returned.  If the variable does not 
exist nil is returned."

  #+allegro
  (sys:getenv varname)

  #+cmu
  (if (stringp varname)
      (cdr (assoc (intern varname "KEYWORD") ext:*environment-list*))
    (cdr (assoc varname ext:*environment-list*)))

  )

;;;------------------------------------------
;;; End.
