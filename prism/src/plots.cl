;;;
;;; plots
;;;
;;; The plot class definitions, draw methods and related functions
;;;
;;; 14-Jan-1994 J. Unger started.
;;; 10-Feb-1994 J. Unger finish adding textual items to plot.
;;; 14-Feb-1994 I. Kalet put sl: package name in call to acknowledge
;;; 14-Feb-1994 J. Unger enhance hp7550a init-inst to handle big page size
;;; 18-Feb-1994 J. Unger add code to print isodose labels on plot.
;;; 02-Mar-1994 J. Unger change 'mu' --> 'cGy' in a couple places.
;;; 06-Apr-1994 J. Unger get plotter device name from configurable const.
;;; 06-Apr-1994 J. Unger put plotter popup menu in interactive-make-plot.
;;; 17-May-1994 I. Kalet move globals to prism-globals and consolidate.
;;;  8-Jun-1994 J. Unger add beam name to bev plots.
;;; 24-Jun-1994 J. Unger fix color bug in interactive-make-plot
;;; 09-Aug-1994 J. Unger make plot draw in off-screen view.
;;; 30-Aug-1994 J. Unger fix bug in off-screen view creation for bev's.
;;; 03-Oct-1994 J. Unger add support for dashed colors.
;;; 13-Jan-1995 I. Kalet get table-position from view, not plan.  Get
;;; plan and patient as passed parameters to interactive-make-plot.
;;; 31-May-1995 I. Kalet DON'T destroy the view at the end of
;;; make-plot.  It is done when the view is deleted from the view set.
;;;  3-Sep-1995 I. Kalet make Mag: textline numeric - should have
;;; been...also force single float arguments to nearly-equal.
;;;  8-Oct-1996 I. Kalet remove &rest parameter from draw method, add
;;;  package name to find-solid-color, as it is moved to slik.
;;; 21-Jan-1997 I. Kalet eliminate table-position.
;;; 03-Jul-1997 BobGian updated nearly-xxx -> poly:nearly-xxx .
;;; 19-May-1998 I. Kalet move max-plane-dose to dose-surface-graphics,
;;; reorganize, and add Postscript plot type.
;;; 11-Jun-1998 I. Kalet fix call to make-view to use :beam-for
;;;  2-Jul-1998 I. Kalet make yellow map to black on PostScript plots.
;;;  Also coerce mag factor to single-float in dialog box.
;;; 10-Jul-1998 I. Kalet print 2 digits to the right of the decimal
;;; point for the view position, instead of 1.
;;; 13-Oct-1998 I. Kalet add gray scale image output to the PostScript
;;; printer.
;;; 14-Dec-1998 I. Kalet add hp455c-plot.  Still need a way to map
;;; yellow to black.
;;; 24-Dec-1998 I. Kalet remove wait t in run-subprocess, now default
;;; 11-Jan-1999 I. Kalet reorganize page size info in order to add 14
;;; by 17 inch page size and other orientations.
;;;  1-Feb-1999 I. Kalet fix error in plot scaling parameters for
;;; small page sizes in dj455c plot initialization.  Every device uses
;;; slightly different HP-GL coordinate conventions.
;;; 15-Feb-1999 I. Kalet fix error in draw method for image in
;;; PostScript plot - which image origin coordinates to use depends on
;;; type of view you are plotting.
;;;  3-May-1999 I. Kalet mods to support multiple colormaps for X -
;;; the SLIK color symbols now hold lists of stuff, not just a
;;; gcontext for the default colormap.
;;;  8-Aug-2000 I. Kalet add capability for drawing cross hatch
;;; interiors for contours in Postscript plots.  Also, condense
;;; slightly initialization code for HP 455C plots.
;;; 26-Nov-2000 I. Kalet cosmetics for buttons in dialog box.
;;; 26-Dec-2000 I. Kalet add :use-gl parameter to view, so can avoid
;;; mysterious GL problem in off screen view.
;;; 11-Mar-2001 I. Kalet print view position on plot to 3 decimal places.
;;; 31-Dec-2001 I. Kalet remove black background from CT on plot, by
;;; doing a raster left-right fill.  Parametrize the value range that
;;; will be considered black by the conversion.
;;;  6-Jan-2002 I. Kalet add window and level at bottom of plot
;;; 13-Jan-2002 I. Kalet add number of copies and black background
;;; option on plot panel, make default black for BEV with DRR.
;;; 17-Mar-2002 I. Kalet add plot page rectangle in view as "preview"
;;; and don't make plot panel a dialog box, but allow other controls
;;; to operate while it is up.  Change interactive-make-plot to
;;; make-plot-panel, returns instance of the new plot-panel class.
;;;  1-Nov-2003 I. Kalet move push-plot-text macro so it is compiled
;;; before it is referenced.
;;;  3-Jan-2009 I. Kalet NOTE that plots of room-views are NOT
;;; supported because you can't NOT use OpenGL.  Will fix this later.
;;;

(in-package :prism)

;;;----------------------------------------------------

(defparameter *plotter-row-height* 0.5
  "The height, in cm, of a row of plotted text.")

(defvar *image-black* 0
  "The threshold for changing black background to white.")

;;;----------------------------------------------------

;;; The page-width's and page-height's are currently limited by the 
;;; hard-clip limits of the HP 7550A pen plotter - See the HP
;;; Interfacing and Programming Manual, page 2-9.  We use the same for
;;; Postscript and DesignJet plots, although the limits are more
;;; permissive there.

(defvar *plot-sizes* '((small "8.5x11" 19.05 25.4)
		       (wide-small "11x8.5" 25.4 19.05)
		       (ledger "17x11" 40.64 25.4)
		       (large "11x17" 25.4 40.64)
		       (film "14x17" 33.0 40.64)
		       (wide-film "17x14" 40.64 33.0)
		       (a4 "A4" 18.46 27.16)
		       (a4-wide "A4 wide" 27.16 18.46)
		       (a3 "A3" 27.16 39.46)
		       (a3-wide "A3 wide" 39.46 27.16)
		       )
  "Table of symbol, name, width and height in cm, for the available
plot sizes.  The sizes allow 1/2 inch margins.")

;;;----------------------------------------------------

(defclass plot (generic-prism-object)

  ((page-size :type symbol
              :accessor page-size
              :initarg :page-size
              :documentation "Symbol, small, large, or film,
              indicating the physical page size of the plot.")

   (magnification :type single-float
                  :accessor magnification
                  :initarg :magnification
                  :documentation "The plot's magnification, in relation
to patient space.")

   (patient-name :accessor patient-name
		 :initarg :patient-name
		 :documentation "The name of the patient, a string,
all we need from the patient case, for the plot.")

   (plan :accessor plan
	 :initarg :plan
	 :documentation "The plan that is being plotted.")

   (view :accessor view
	 :initarg :view
         :documentation "An off-screen view, into which the graphics
to be plotted are drawn.")

   (width :accessor width
	  :initarg :width
	  :documentation "The off-screen view width in pixels.")

   (height :accessor height
	   :initarg :height
	   :documentation "The off-screen view height in pixels.")

   (upperband :accessor upperband
	      :documentation "The height in pixels of the upper band
of plot label text")

   (lowerband :accessor lowerband
	      :documentation "The height in pixels of the lower band
of plot label text")

   (current-pen-color :accessor current-pen-color
		      :initform 0
		      :documentation "Keeps track of current color, so
on the HP pen plotter can avoid changing pens when not necessary.")

   (text-color :accessor text-color
	       :initarg :text-color
	       :documentation "A SLIK symbol specifying the color to
use for text primitives on the plot.  It may be different for
different plot devices.")

   (colormap :type list
             :accessor colormap
             :initarg :colormap
             :documentation "A list of gcontexts representing SLIK
colors.  For a HP pen plot, the index of a color on the list is the
plotter pen assignment for that color.  For a PostScript plot, the
Postscript RGB color specification string is found at that index in a
separate table.")

   (output-stream :accessor output-stream
                  :initarg :output-stream
                  :documentation "The stream to a file, into which
HP-GL or other plotting commands are sent.")

   )

  (:default-initargs :page-size 'small :magnification 1.0
		     :orientation 'portrait
		     :colormap (mapcar #'sl:color-gc
				       '(sl:black sl:red sl:blue
					 sl:magenta sl:green sl:white
					 sl:yellow sl:cyan sl:gray))
		     :output-stream nil)

  (:documentation "The general plot class for all types of plots.")

  )

;;;----------------------------------------------------

(defmacro push-plot-text (text x y inc plt)

  "push-plot-text text x y inc plt

Makes a characters-prim graphic primitive from text, x, y, and the
plot text-color and pushes it onto plt's list of graphic primitives.
The x and y parameters are the location of the text on the plot.  Also
increments y by inc, so that this macro can be called many times in
succession to generate a column of text."

  `(progn 
     (push (make-characters-prim 
	    ,text ,x ,y (sl:color-gc (text-color ,plt)) :object ,plt)
	   (foreground (view ,plt)))
     (incf ,y ,inc)))

;;;----------------------------------------------------

(defmethod initialize-instance :after ((plt plot) &rest initargs)

  "does the generic initialization, assuming certain slots are
initialized by initargs."

  (declare (ignore initargs))
  (let* ((mag (magnification plt))
	 (plot-width (width plt))
	 (plot-height (height plt))
         (init-y (round (* (scale (view plt)) *plotter-row-height*
			   (/ mag))))
         (init-x (round (/ init-y 2)))
         (text-x init-x)
         (text-y init-y)
         (y-inc init-y)
         (x-inc (* 5 init-y)) ;; for plotting dose values at bottom
	 (pln (plan plt))
	 (max-dose (max-plane-dose (view plt) (dose-grid pln)
				   (sum-dose pln)))
	 (dose-key (mapcar #'(lambda (srf)
			       (list (round (threshold srf)) 
				     (sl:color-gc (display-color srf))))
			   (coll:elements (dose-surfaces pln)))))
    ;; make a graphic primitive for the rectangular border
    (push (make-rectangles-prim (list 0 0 plot-width plot-height)
				(sl:color-gc (text-color plt))
				:object plt)
	  (foreground (view plt)))

    ;; institutional header
    (dolist (txt *hardcopy-header*)
      (push-plot-text txt text-x text-y y-inc plt))
    ;; make graphic primitives for various text items at top of plot
    (incf text-y y-inc)
    (push-plot-text (patient-name plt) text-x text-y y-inc plt)
    (push-plot-text (name pln) text-x text-y y-inc plt)      
    (push-plot-text (time-stamp pln) text-x text-y y-inc plt)
    (push-plot-text (concatenate 'string "Plot Magnification: "
				 (write-to-string mag))
		    text-x text-y y-inc plt)
    (setf (upperband plt) text-y) ;; for clipping images
    ;; make graphic primitives for various text items at bottom
    ;; only print out dose-specific info if non-zero max dose
    (if (zerop max-dose) (setq text-y (- plot-height init-y)
			       y-inc (- y-inc))
      (progn
        (setq text-y (- plot-height init-y
			(* y-inc (1+ (floor (length dose-key) 6)))))
        (push-plot-text "Isodose levels (cGy):" text-x text-y y-inc plt)
        (setq text-x (* 3 init-x))
        (setq dose-key (sort dose-key #'< :key #'first))
	(let ((count 0))
	  (dolist (pair dose-key)
	    (push (make-characters-prim (write-to-string (first pair)) 
					text-x text-y (second pair) 
					:object plt)
		  (foreground (view plt)))
	    (incf count)
	    (if (zerop (mod count 6))
		(setq text-x  (* 3 init-x)
		      text-y  (+ text-y y-inc))
	      (incf text-x x-inc))))
        (setq text-x init-x)
        (setq text-y (- plot-height init-y
			(* y-inc (+ 2 (floor (length dose-key) 6)))))
        (setq y-inc (- y-inc))
        (push-plot-text (concatenate 'string 
			  "Grid Size: "
			  (write-to-string (voxel-size (dose-grid pln)))
			  " cm")
			text-x text-y y-inc plt)
        (push-plot-text (concatenate 'string 
			  "Max Dose: "
			  (write-to-string max-dose) " cGy")
			text-x text-y y-inc plt)))
    ;; print the plot position and orientation in any case
    (push-plot-text (format nil "Plot Position: ~A~6,3F cm"
			    (typecase (view plt)
			      (transverse-view "Z=")
			      (coronal-view "Y=")
			      (sagittal-view "X=")
			      (beams-eye-view "D="))
			    (view-position (view plt)))
		    text-x text-y y-inc plt)
    (if (typep (view plt) 'beams-eye-view)
	(push-plot-text (format nil "Beam Name: ~A"
				(name (beam-for (view plt))))
			text-x text-y y-inc plt))
    (push-plot-text (format nil "Plot Orientation: ~A    Window=~A, Level=~A"
			    (typecase (view plt)
			      (transverse-view "Transverse")
			      (coronal-view "Coronal")
			      (sagittal-view "Sagittal")
			      (beams-eye-view "Beam's Eye"))
			    (window (view plt)) (level (view plt)))
		    text-x text-y y-inc plt)
    (setf (lowerband plt) (- plot-height text-y)))
  (setf (output-stream plt)
    (open *plotter-file* :direction :output 
	  :if-exists :supersede 
	  :if-does-not-exist :create)))

;;;----------------------------------------------------

(defmethod finish-plot ((plt plot))

  ;; remove the off screen view from the plan view collection - this
  ;; destroys the view
  (coll:delete-element (view plt) (plan-views (plan plt))))

;;;----------------------------------------------------

(defun draw-isodose-labels (plt x-sep y-sep width height)

  "draw-isodose-labels plt x-sep y-sep width height

Creates a characters primitive containing a label for each unconnected
isodose contour segment in plot plt, and adds those primitives to
plt's list of graphic primitives.  The labels are added in a manner so
that no two labels fall within the same x-sep by y-sep box, and no
label is printed outside the frame determined by 0,0 and width,height
since otherwise they would be off the page."

  (let* ((label-list nil)
         (dose-prims (remove-if-not 
		      #'(lambda (prim) 
			  (and (slot-boundp prim 'object)
			       (typep (object prim) 'dose-surface)))
		      (foreground (view plt)))))
    (dolist (prim dose-prims)
      (dolist (curve (points prim))
        (do* ((ptr curve (rest (rest ptr)))
              (x-pos (first ptr) (first ptr))
              (y-pos (second ptr) (second ptr))
              (done nil))
	    ((or done (null ptr)))
          (when (and (<= 0 x-pos (- width x-sep))
                     (<= y-sep y-pos height)
                     (notany #'(lambda (l) 
                                 (and (poly:nearly-equal 
				       (coerce x-pos 'single-float)
				       (coerce (first l)
					       'single-float)
				       x-sep)
                                      (poly:nearly-equal 
				       (coerce y-pos 'single-float)
				       (coerce (second l)
					       'single-float)
				       y-sep)))
			     label-list))
	    (push (list x-pos y-pos) label-list)
            (push (make-characters-prim 
		   (write-to-string (round (threshold (object prim))))
		   x-pos y-pos (color prim)
		   :object plt)
		  (foreground (view plt)))
            (setq done t)))))))
        
;;;----------------------------------------------------

(defclass plot-box (generic-panel)

  ((view-for :accessor view-for
	     :initarg :view-for
	     :documentation "The on-screen view to be plotted, not the
temporary internal off-screen view used to actually generate the plot.")

   (view-panel :accessor view-panel 
	       :initarg :view-panel)

   (plan-of :accessor plan-of
	    :initarg :plan-of)

   (patient :accessor patient
	    :initarg :patient)

   (pframe :accessor pframe
	   :documentation "The plot panel frame.")

   (black-btn :accessor black-btn
	      :documentation "A button to set the background to black
or white when an image is displayed in the view")

   (black-off :accessor black-off
	      :initarg :black-off
	      :documentation "Boolean, t if the background is to be
changed to white when an image is present, and nil if the background
is to be black")

   (mag-tln :accessor mag-tln
	    :documentation "A text line to specify the plot
magnification factor, where 1.0 means life-size.")

   (mag :accessor mag
	:initform 1.0)

   (copies-tln :accessor copies-tln
	       :documentation "A text line to set the number of
copies, if you want multiple copies of a plot")

   (numcopies :accessor numcopies
	      :initform 1)

   (size-menu :accessor size-menu
	      :documentation "A menu to select from the available
paper sizes and orientations.")

   (page-size :accessor page-size)

   (pmenu :accessor pmenu
	  :documentation "A menu to select from the available plotters")

   (plotter :accessor plotter)

   (accept-btn :accessor accept-btn
	       :documentation "A button that produces the plot and
removes the panel when pressed.")

   (cancel-btn :accessor cancel-btn
	       :documentation "A button that removes the panel
without producing a plot, when pressed.")

   )
  (:documentation "A plot box is a panel that comes up to specify the
   parameters of a hard copy plot for a view, and to make the plot
   when the accept button is pressed.")
  )

;;;----------------------------------------------------

(defun do-plot (pbox)

  "do-plot pbox

Makes and spools a plot of the appropriate type for the specified
queue, plotter, magnification factor, page size, view, plan and
patient case, all specified in pbox, an instance of a plot-box."

  (let* ((plotter (plotter pbox))
	 (mag (mag pbox))
	 (page-size (page-size pbox))
	 (vw (view-for pbox))
	 (pln (plan-of pbox))
	 (pat (patient pbox))
	 (scale (scale vw))
	 (size-data (find page-size *plot-sizes* :key #'first))
	 (plot-width (round (* scale (third size-data)
			       (/ mag))))
	 (plot-height (round (* scale (fourth size-data)
				(/ mag))))
	 (vw-picwin (sl:window (picture vw)))
	 (plt (make-instance (second ;; gives the plot type for plotter
			      (find plotter *plotters*
				    :key #'first :test #'string-equal))
		:name plotter
		:page-size page-size
		:magnification mag
		:patient-name (name pat)
		:plan pln
		:width plot-width
		:height plot-height
		:view
		(make-view plot-width plot-height (type-of vw)
			   :scale scale
			   :window (window vw)
			   :level (level vw)
			   :view-position (view-position vw)
			   :beam-for (if (typep vw 'beams-eye-view)
					 (beam-for vw))
			   :x-origin (round (+ (x-origin vw)
					       (/ (- plot-width
						     (clx:drawable-width
						      vw-picwin))
						  2)))
			   :y-origin (round (+ (y-origin vw)
					       (/ (- plot-height
						     (clx:drawable-height
						      vw-picwin))
						  2)))))))
    ;; add the off-screen view to the plan view collection, which will
    ;; generate all the graphic primitives in the off-screen view
    (coll:insert-element (view plt) (plan-views pln))
    ;; now add isodose labels, since the above has generated the
    ;; graphic prims for isodose curves, if they are there
    (let ((init-y (round (* scale *plotter-row-height* (/ mag)))))
      (draw-isodose-labels plt (* init-y 2.25) (* init-y 0.75)
			   plot-width plot-height))
    ;; draw the background image first if present
    (when (background-displayed vw) ;; the image button was pressed
      ;; find the image corresponding to view vw by looking through
      ;; the image-view mediators of the image manager for the plan.
      (setf (black-off plt) (black-off pbox))
      (let ((img-vw-mgr (im-vm (find pln (coll:elements
					  (pat-plan-mediator-set
					   pat))
				     :key #'the-plan))))
	;; also check that there is a study loaded
	(when img-vw-mgr
	  (draw (image (find vw (coll:elements
				 (mediator-set img-vw-mgr))
			     :key #'view))
		plt))))
    ;; sort graphic primitives by color - copy since sort is destructive
    (setf (foreground (view plt))
      (sort (copy-list (foreground (view plt)))
	    #'(lambda (pr1 pr2)
		(< (pen-color (color pr1) plt)
		   (pen-color (color pr2) plt)))))
    ;; then draw all the primitives, i.e., write to file
    (dolist (prim (foreground (view plt)))
      (let ((original (find (object prim) (foreground vw)
			    :key #'object)))
	(if (or (not original) ;; always draw the added text stuff
		(visible original))
	    (draw prim plt))))
    (finish-plot plt)
    (close (output-stream plt))
    (unless (or (string-equal (name plt) "HP File only")
		(string-equal (name plt) "PS File only"))
      (dotimes (i (numcopies pbox))
	(run-subprocess
	 (format nil "~a~a ~a"
		 *spooler-command* (name plt) *plotter-file*))))))

;;;----------------------------------------------------

(defun draw-plot-preview (p-panel)

  "draw-plot-preview p-panel

puts a temporary rectangle in the on-screen view to show what will
appear on the plot."

  (let* ((vw (view-for p-panel))
	 (mag (mag p-panel))
	 (ppcm (scale vw))
	 (pixw (truncate (clx:drawable-width (sl:window (picture vw)))
			 2))
	 (pixh (truncate (clx:drawable-height (sl:window (picture vw)))
			 2))
	 (size-data (find (page-size p-panel) *plot-sizes* :key #'first))
	 (w (round (* (third size-data) ppcm) mag))
	 (h (round (* (fourth size-data) ppcm) mag))
	 (ulc-x (- pixw (round w 2)))
	 (ulc-y (- pixh (round h 2)))
	 (preview-box (list ulc-x ulc-y w h))
	 (rect-prim (find p-panel (foreground vw) :key #'object)))
    (if rect-prim (setf (rectangles rect-prim) preview-box)
      (push (make-rectangles-prim preview-box (sl:color-gc 'sl:white)
				  :object p-panel)
	    (foreground vw)))
    (display-view vw)))

;;;----------------------------------------------------

(defmethod destroy :before ((pb plot-box))

  (ev:remove-notify pb (new-scale (view-for pb)))
  (setf (foreground (view-for pb))
    (remove pb (foreground (view-for pb)) :key #'object))
  (sl:destroy (pmenu pb))
  (sl:destroy (mag-tln pb))
  (sl:destroy (copies-tln pb))
  (sl:destroy (black-btn pb))
  (sl:destroy (size-menu pb))
  (sl:destroy (accept-btn pb))
  (sl:destroy (cancel-btn pb))
  (sl:destroy (pframe pb)))

;;;----------------------------------------------------

(defun make-plot-panel (v vp pln pat)

  "make-plot-panel v vp pln pat

Returns a plot panel, with controls for the user to specify/select
plotter, paper size and orientation, etc.  The plot is generated from
the supplied view v, plan pln and patient pat.  The view panel is needed
to synchronize the deletion of the plot panel if the user removes the
view panel."

  (make-instance 'plot-box :view-for v :view-panel vp
		 :plan-of pln :patient pat
		 :black-off (if (typep v 'beams-eye-view) nil t)))

;;;----------------------------------------------------

(defmethod initialize-instance :after ((pbox plot-box) &rest initargs)

  (declare (ignore initargs))
  (let* ((plotter-names (mapcar #'first *plotters*))
	 (paper-sizes (mapcar #'second *plot-sizes*))
         (pmenu (sl:make-radio-menu plotter-names :mapped nil))
         (size-menu (sl:make-radio-menu paper-sizes :mapped nil))
	 (delta-y (+ 10 (max (sl:height pmenu)
			     (sl:height size-menu))
		     10))
         (pframe (sl:make-frame (+ (sl:width pmenu)
				   (sl:width size-menu)
				   30)
				(+ delta-y 30 10 30 10 30 10)
				:title "Plot Parameters"))
         (win (sl:window pframe))
         (mag-tln (sl:make-textline (- (sl:width pframe) 20) 30
				    :parent win 
				    :label "Magnification: "
				    :info "1.0"
				    :numeric t
				    :lower-limit 0.1
				    :upper-limit 10.0
				    :ulc-x 10 :ulc-y delta-y))
	 (copies-tln (sl:make-textline 70 30
				       :label "Copies: "
				       :parent win
				       :info "1"
				       :numeric t
				       :lower-limit 1
				       :upper-limit 9
				       :ulc-x 10 :ulc-y (+ delta-y 40)))
         (black-btn (sl:make-button 70 30
				    :label "Background"
				    :border-style :flat
				    :fg-color 'sl:white
				    :bg-color 'sl:black
				    :font (symbol-value *small-font*)
				    :parent win
				    :ulc-x (- (sl:width pframe) 80)
				    :ulc-y (+ delta-y 40)))
	 (accept-btn (sl:make-exit-button 70 30
					  :label "Accept"
					  :parent win
					  :ulc-x 10
					  :ulc-y (+ delta-y 80)
					  :bg-color 'sl:green))
         (cancel-btn (sl:make-exit-button 70 30
					  :label "Cancel"
					  :parent win
					  :ulc-x (- (sl:width pframe) 80)
					  :ulc-y (+ delta-y 80))))
    (clx:reparent-window (sl:window pmenu) win 10 10)
    (clx:map-window (sl:window pmenu))
    (clx:map-subwindows (sl:window pmenu))
    (clx:reparent-window (sl:window size-menu)
			 win (+ (sl:width pmenu) 20) 10)
    (clx:map-window (sl:window size-menu))
    (clx:map-subwindows (sl:window size-menu))
    (setf (sl:on black-btn) (black-off pbox))
    (setf (pframe pbox) pframe
	  (pmenu pbox) pmenu
	  (mag-tln pbox) mag-tln
	  (copies-tln pbox) copies-tln
	  (black-btn pbox) black-btn
	  (size-menu pbox) size-menu
	  (accept-btn pbox) accept-btn
	  (cancel-btn pbox) cancel-btn)
    (ev:add-notify pbox (sl:selected pmenu)
                   #'(lambda (pbx m item)
                       (declare (ignore m))
		       (setf (plotter pbx)
			 (first (nth item *plotters*)))))
    (ev:add-notify pbox (sl:selected size-menu)
                   #'(lambda (pbx m item)
                       (declare (ignore m))
		       (setf (page-size pbx)
			 (first (nth item *plot-sizes*)))
		       ;; redraw plot area rectangle in on-screen view
		       (draw-plot-preview pbx)))
    (sl:select-button 0 pmenu) ;; sets default selection
    (sl:select-button 0 size-menu)  ;; ditto, and draws initial rectangle
    (ev:add-notify pbox (sl:new-info mag-tln)
                   #'(lambda (pbx tln info)
                       (declare (ignore tln))
                       (setf (mag pbx)
			 (coerce (read-from-string info) 'single-float))
		       ;; redraw plot area rectangle in on-screen view 
		       (draw-plot-preview pbx)))
    (ev:add-notify pbox (new-scale (view-for pbox))
		   #'(lambda (pbx vw scl)
		       (declare (ignore vw scl))
		       (draw-plot-preview pbx)))
    (ev:add-notify pbox (sl:new-info copies-tln)
                   #'(lambda (pbx tln info)
                       (declare (ignore tln))
                       (setf (numcopies pbx)
			 (coerce (read-from-string info) 'integer))))
    (ev:add-notify pbox (sl:button-on black-btn)
                   #'(lambda (pbx bt)
                       (declare (ignore bt))
		       (setf (black-off pbx) t)))
    (ev:add-notify pbox (sl:button-off black-btn)
                   #'(lambda (pbx bt)
                       (declare (ignore bt))
		       (setf (black-off pbx) nil)))
    (ev:add-notify pbox (sl:button-on accept-btn)
		   #'(lambda (pbx bt)
		       (declare (ignore bt))
		       (do-plot pbx)
		       (setf (foreground (view-for pbx))
			 (remove pbx (foreground (view-for pbx))
				 :key #'object))
		       (display-view (view-for pbx))
		       (destroy pbx)))
    (ev:add-notify pbox (sl:button-on cancel-btn)
		   #'(lambda (pbx bt)
		       (declare (ignore bt))
		       (setf (foreground (view-for pbx))
			 (remove pbx (foreground (view-for pbx))
				 :key #'object))
		       (display-view (view-for pbx))
		       (destroy pbx)))))

;;;----------------------------------------------------

(defmethod pen-color (color (plt plot))

  "Given color (a gcontext representing a SLIK color), returns the pen
index corresponding to that color as determined by the plot's
colormap.  Returns pen index #8 if no such color is in the colormap."

  (or (position color (colormap plt)) 8))

;;;----------------------------------------------------
;;; Definitions for subclasses - first, generic HPGL plotter
;;;----------------------------------------------------

(defclass hpgl-plot (plot)
 
  ()

  (:documentation "A plot object corresponding to the generic HPGL
plotter.")

  )

;;;----------------------------------------------------

(defmethod initialize-instance :after ((plt hpgl-plot)
				       &rest initargs)

  "Initialization operations for any HPGL plot.  Generates the Prism
logo."

  (declare (ignore initargs))
  (let* ((size-data (find (page-size plt) *plot-sizes* :key #'first))
	 (page-width (third size-data))
	 (text-y (round (* (scale (view plt)) *plotter-row-height*
			   (/ (magnification plt)))))
	 (y-inc text-y)
	 (text-x (round (* (scale (view plt))
			   (- page-width 5.0)
			   (/ (magnification plt))))))
    (push-plot-text "Prism RTP System" text-x text-y y-inc plt)
    (push-plot-text *prism-version-string*
		    text-x text-y y-inc plt)))

;;;----------------------------------------------------
;;; HP7550 pen plotter has own initialization and colors
;;;----------------------------------------------------

(defclass hp7550a-plot (hpgl-plot)
 
  ()

  (:default-initargs :text-color 'sl:blue)

  (:documentation "A plot object corresponding to the HP7550A pen
plotter.")

  )

;;;----------------------------------------------------

(defmethod initialize-instance :after ((plt hp7550a-plot)
				       &rest initargs)

  "Initialization operations for HP7550A plot."

  ;; The number of HP plotter units per centimeter (pupcm below) is 
  ;; found on p 3-2 of the HP 7550A Interfacing & Programming Manual

  (declare (ignore initargs))
  (let* ((str (output-stream plt))
         (pupcm 400)
         (P1x 0) 
         (P1y 0)
         (P2x 0)
         (P2y 0)
         (rot 0)
	 (size-data (find (page-size plt) *plot-sizes* :key #'first))
	 (page-width (third size-data))
	 (page-height (fourth size-data)))
    ;; initialize plotter
    (format str "IN;~%")
    (case (page-size plt)
      ((small a4) (setq P1x (round (* pupcm page-width))
			P2y (round (* pupcm page-height))
			rot 90))
      ((wide-small a4-wide) (setq P1x (round (* pupcm page-width))
				  P2y (round (* pupcm page-height))))
      ((ledger a3-wide) (setq P1y (round (* pupcm page-height))
			      P2x (round (* pupcm page-width))))
      ((large a3) (setq P1y (round (* pupcm page-height))
			P2x (round (* pupcm page-width))
			rot 90)))
    ;; rotate axes if needed
    (format str "RO~a;~%" rot)
    ;; reset P1 & P2 to bring origin into ulc of page and create a 
    ;; region on the page determined by page width & height.
    (format str "IP~a,~a,~a,~a;~%" P1x P1y P2x P2y)
    ;; set soft clip limits to this region
    (format str "IW~a,~a,~a,~a;~%" P1x P1y P2x P2y)
    ;; set plotter scale to region - maps screen space coords to region
    (format str "SC~a,~a,~a,~a;~%" 0 (width plt) 0 (height plt))))

;;;----------------------------------------------------

(defmethod finish-plot :after ((p hp7550a-plot))

  "Reset the current pen color of p and write a return-pen command
to stream."

  (setf (current-pen-color p) 0)
  (format (output-stream p) "SP0;~%"))

;;;----------------------------------------------------
;;; HP Design Jet 455C pen plotter has its own initialization and colors
;;;----------------------------------------------------

(defclass hp455c-plot (hpgl-plot)
 
  ()

  (:default-initargs :text-color 'sl:black
    ;; note that pen 0 is not used, so it is just a placeholder here.
    :colormap (mapcar #'sl:color-gc
		      '(nil sl:black sl:red sl:green
			sl:yellow sl:blue sl:magenta sl:cyan)))

  (:documentation "A plot object corresponding to the HP Design Jet
455C pen plotter.")

  )

;;;----------------------------------------------------

(defmethod initialize-instance :after ((plt hp455c-plot)
				       &rest initargs)

  "Initialization operations for HP Design Jet 455C plot."

  ;; The number of HP plotter units per centimeter (pupcm below) is 
  ;; found on p 3-2 of the HP 7550A Interfacing & Programming Manual.
  ;; The offset numbers are from Tim Fox of Emory University.

  (declare (ignore initargs))
  (let* ((str (output-stream plt))
         (pupcm 400)
         (P1x 0) 
         (P1y 0)
         (P2x 0)
         (P2y 0)
         (rot 0)
	 (size-data (find (page-size plt) *plot-sizes* :key #'first))
	 (page-width (third size-data))
	 (page-height (fourth size-data)))
    ;; initialize plotter
    (format str "IN;~%")
    (case (page-size plt)
      ((small a4) (setq P2x (round (* pupcm page-width))
			P1y (round (* pupcm page-height))))
      ((wide-small a4-wide) (setq P2x (round (* pupcm page-width))
				  P1y (round (* pupcm page-height))
				  rot 90))
      ((ledger a3-wide wide-film) (setq P1y (- (round (* pupcm page-height))
					       5200)
					P2x (- (round (* pupcm page-width))
					       7920)
					P1x -7920
					P2y -5200
					rot 90))
      ((large a3 film) (setq P1y (- (round (* pupcm page-height))
				    5200)
			     P2x (- (round (* pupcm page-width))
				    7920)
			     P1x -7920
			     P2y -5200)))
    ;; rotate axes if needed
    (format str "RO~a;~%" rot)
    ;; reset P1 & P2 to bring origin into ulc of page and create a 
    ;; region on the page determined by page width & height.
    (format str "IP~a,~a,~a,~a;~%" P1x P1y P2x P2y)
    ;; set soft clip limits to this region
    (format str "IW~a,~a,~a,~a;~%" P1x P1y P2x P2y)
    ;; set plotter scale to region - maps screen space coords to region
    (format str "SC~a,~a,~a,~a;~%" 0 (width plt) 0 (height plt))))

;;;----------------------------------------------------

(defmethod pen-color (color (plt hp455c-plot))

  "works like general method, but substitutes black for yellow also."

  (let ((n (or (position color (colormap plt)) 8)))
    (if (= n 4) 1 n)))

;;;----------------------------------------------------

(defmethod finish-plot :after ((p hp455c-plot))

  "Reset the current pen color of p and write a return-pen command
to stream."

  (setf (current-pen-color p) 0)
  (format (output-stream p) "SP0;~%")
  (format (output-stream p) "PG;~%")) ;; page eject

;;;----------------------------------------------------
;;; DRAW methods for graphic prims in any hpgl-plot
;;;----------------------------------------------------

(defmethod draw ((l lines-prim) (p hpgl-plot))

  "Draws lines primitive l into HP plot p."

  (let* ((str (output-stream p))
         (temp-col (sl:find-solid-color (color l))) ; nil if already solid
         (col (pen-color (or temp-col (color l)) p)))
    (unless (eq (color l) (sl:color-gc 'sl:invisible))
      (unless (= col (current-pen-color p))
        (setf (current-pen-color p) col)
        (format str "SP~a;~%" col))
      (when temp-col (format str "LT2,2;~%")) ; non-nil if dashed color
      (dolist (pts (points l))
        (format str "PA~a,~a; PD~%" (first pts) (second pts))
        (do* ((pt (rest (rest pts)) (rest (rest pt)))
              (x (first pt) (first pt))
              (y (second pt) (second pt)))
	    ((null pt))
          (format str "~a,~a,~%" x y))
        (format str "; PU;~%"))
      (when temp-col (format str "LT;~%")))))  ; non-nil if dashed color

;;;----------------------------------------------------

(defmethod draw ((s segments-prim) (p hpgl-plot))

  "Draws segments primitive s into HPGL plot p."

  (let* ((str (output-stream p))
         (temp-col (sl:find-solid-color (color s))) ; nil if already solid
         (col (pen-color (or temp-col (color s)) p))
         (format-string (if temp-col	; non-nil if dashed, nil if solid
			    "LT2,2; PA~a,~a; PD~a,~a; PU; LT;~%"
			  "PA~a,~a; PD~a,~a; PU;~%")))
    (unless (eq (color s) (sl:color-gc 'sl:invisible))
      (unless (= col (current-pen-color p))
        (setf (current-pen-color p) col)
        (format str "SP~a;~%" col))
      (do ((tup (points s) (nthcdr 4 tup)))
          ((null tup))
        (format str format-string
		(first tup) (second tup) (third tup) (fourth tup))))))

;;;----------------------------------------------------

(defmethod draw ((r rectangles-prim) (p hpgl-plot))

  "Draws rectangles primitive r into HPGL plot p."

  (let ((str (output-stream p))
        (col (pen-color (color r) p)))
    (unless (eq (color r) (sl:color-gc 'sl:invisible))
      (unless (= col (current-pen-color p))
        (setf (current-pen-color p) col)
        (format str "SP~a;~%" col))
      (do ((tup (rectangles r) (nthcdr 4 tup)))
          ((null tup))
        (let* ((x1 (first tup))
               (y1 (second tup))
               (x2 (+ x1 (third tup)))
               (y2 (+ y1 (fourth tup))))
          (format str "PA~a,~a; PD~a,~a,~a,~a,~a,~a,~a,~a; PU;~%" 
		  x1 y1 x2 y1 x2 y2 x1 y2 x1 y1))))))

;;;----------------------------------------------------

(defparameter *plotter-char-width* 0.187 
  "The width, in cm, of a plotted character.  For example, see the HP
7550A Interfacing and Programming Manual, page 7-14.")

(defparameter *plotter-char-height* 0.269
  "The height, in cm, of a plotted character.")

;;;----------------------------------------------------

(defmethod draw ((c characters-prim) (p hpgl-plot))

  "Draws characters primitive c into HPGL plot p."

  (let ((str (output-stream p))
        (col (pen-color (color c) p)))
    (unless (eq (color c) (sl:color-gc 'sl:invisible))
      (unless (= col (current-pen-color p))
        (setf (current-pen-color p) col)
        (format str "SP~a;~%" col))
      (format str "SI~a,~a;~%" *plotter-char-width* *plotter-char-height*)
      (format str "PA~a,~a;~%" (x c) (y c))
      (format str "LB~a~a;~%" (characters c) #\^C))))

;;;----------------------------------------------------
;;; Postscript plots go to PostScript color printers
;;;----------------------------------------------------

(defclass ps-plot (plot)
 
  ((ps-colormap :accessor ps-colormap
		:initarg :ps-colormap
		:documentation "A list of PostScript RGB values
corresponding to SLIK colors in the general plot colormap")

   (black-off :accessor black-off
	      :initarg :black-off
	      :documentation "A flag to specify whether to leave the
image background black or change it to white.")

   )

  ;; The order here is the same as in the colormap of the general
  ;; plot: black red blue magenta green white yellow cyan gray
  ;; except that screen white and yellow map to black on output.
  ;; Actually this will all work unchanged on a monochrome PostScript
  ;; printer - the printer remaps stuff in a reasonable way.

  (:default-initargs :text-color 'sl:black :black-off t
		     :ps-colormap '((0 0 0) (1 0 0) (0 0 1) (0.7 0 1)
				    (0 1 0) (0 0 0) (0 0 0) (0 1 1)
				    (0.5 0.5 0.5)))

  (:documentation "A plot object corresponding to a PostScript
printer.")

  )

;;;----------------------------------------------------

(defmethod initialize-instance :after ((plt ps-plot)
				       &rest initargs)

  (declare (ignore initargs))
  (let* ((strm (output-stream plt))
	 (inch-scale (/ (* (scale (view plt)) 2.54)
			(magnification plt)))
	 (size-data (find (page-size plt) *plot-sizes* :key #'first))
	 (page-width (/ (third size-data) 2.54))
	 (page-height (/ (fourth size-data) 2.54)))
    (ps:initialize strm 0.5 0.5
		   (/ (width plt) inch-scale)
		   (/ (height plt) inch-scale)
		   (+ page-width 1.0)
		   (+ page-height 1.0))
    (ps:translate-origin strm 0.5 (+ page-height 0.5))
    (ps:prism-logo strm (- page-width 3.0) -0.1 *prism-version-string*)
    (ps:set-graphics strm :width 1)))

;;;----------------------------------------------------

(defmethod finish-plot :after ((p ps-plot))

  "Prints the page."

  (ps:finish-page (output-stream p)))

;;;----------------------------------------------------

(defmethod draw ((pr lines-prim) (plt ps-plot))

  "Draws lines primitive pr into Postscript plot plt."

  (let* ((str (output-stream plt))
         (temp-col (sl:find-solid-color (color pr))) ;; nil if already solid
         (col (pen-color (or temp-col (color pr)) plt))
	 (inch-scale (/ (* (scale (view plt)) 2.54)
			(magnification plt))))
    (unless (eq (color pr) (sl:color-gc 'sl:invisible))
      (setf (current-pen-color plt) col)
      (ps:set-graphics str
		       :color (nth col (ps-colormap plt))
		       :pattern (if temp-col "[10 10] 0" "[] 0"))
      ;; experimental - draw mesh for tumors and targets
      (let ((draw-mesh (typep (object pr) '(or tumor target))))
	(declare (ignore draw-mesh)) ;; for now
	(dolist (pts (points pr))
	  (let ((inch-con (cm-contour pts inch-scale 0 0)))
	    ;; (if draw-mesh (ps:draw-poly-mesh str inch-con 0.25))
	    (ps:draw-lines str inch-con)))))))

;;;----------------------------------------------------

(defmethod draw ((pr segments-prim) (plt ps-plot))

  "Draws segments primitive pr into Postscript plot plt."

  (let* ((str (output-stream plt))
         (temp-col (sl:find-solid-color (color pr))) ;; nil if already solid
         (col (pen-color (or temp-col (color pr)) plt))
	 (inch-scale (/ (* (scale (view plt)) 2.54)
			(magnification plt))))
    (unless (eq (color pr) (sl:color-gc 'sl:invisible))
      (setf (current-pen-color plt) col)
      (ps:set-graphics str
		       :color (nth col (ps-colormap plt))
		       :pattern (if temp-col "[10 10] 0" "[] 0"))
      (do ((coords (points pr) (nthcdr 4 coords)))
	  ((null coords))
	(ps:draw-line str
		      (cm-x (first coords) 0 inch-scale)
		      (cm-y (second coords) 0 inch-scale)
		      (cm-x (third coords) 0 inch-scale)
		      (cm-y (fourth coords) 0 inch-scale))))))

;;;----------------------------------------------------

(defmethod draw ((pr rectangles-prim) (plt ps-plot))

  "Draws rectangles primitive pr into Postscript plot plt."

  (let ((str (output-stream plt))
	(col (pen-color (color pr) plt))
	(inch-scale (/ (* (scale (view plt)) 2.54)
		       (magnification plt))))
    (unless (eq (color pr) (sl:color-gc 'sl:invisible))
      (setf (current-pen-color plt) col)
      (ps:set-graphics str :color (nth col (ps-colormap plt)))
      (do ((rects (rectangles pr) (nthcdr 4 rects)))
	  ((null rects))
	(ps:draw-rectangle str
			   (cm-x (first rects) 0 inch-scale)
			   (cm-y (second rects) 0 inch-scale)
			   (cm-x (third rects) 0 inch-scale)
			   (cm-y (fourth rects) 0 inch-scale))))))

;;;----------------------------------------------------

(defmethod draw ((pr characters-prim) (plt ps-plot))

  "Draws characters primitive pr into Postscript plot plt."

  (let ((str (output-stream plt))
	(col (pen-color (color pr) plt))
	(inch-scale (/ (* (scale (view plt)) 2.54)
		       (magnification plt))))
    (unless (eq (color pr) (sl:color-gc 'sl:invisible))
      (setf (current-pen-color plt) col)
      (ps:set-graphics str :color (nth col (ps-colormap plt)))
      (ps:draw-text str
		    (cm-x (x pr) 0 inch-scale)
		    (cm-y (y pr) 0 inch-scale)
		    (characters pr)))))

;;;----------------------------------------------------

(defmethod draw ((im image-2d) (plt ps-plot))

  "Draws image-2d im into Postscript plot plt."

  (let* ((vw (view plt))
	 (image-8 (sl:map-raw-image (pixels im) (window vw) (level vw) 4095))
	 (cmppix (/ 1.0 (scale vw)))
	 (mag (* 0.3937 (magnification plt)))
	 (im-orig-x (svref (origin im)
			   (typecase vw
			     ((or transverse-view coronal-view
			       beams-eye-view) 0)
			     (sagittal-view 2))))
	 (im-orig-y (svref (origin im)
			   (typecase vw
			     ((or transverse-view sagittal-view
			       beams-eye-view) 1)
			     (coronal-view 2))))
	 (x (* mag (+ (* cmppix (x-origin vw)) im-orig-x)))
	 (y (* mag (- (if (typep vw 'coronal-view) (- im-orig-y)
			im-orig-y)
		      (* cmppix (y-origin vw))
		      (second (size im)))))
	 (strm (output-stream plt))
	 (inch-scale (/ (scale vw) mag)) ;; mag already has cm to in.
	 (width (/ (width plt) inch-scale))
	 (height (/ (height plt) inch-scale))
	 (lowerband (/ (lowerband plt) inch-scale))
	 (upperband (/ (upperband plt) inch-scale)))
    (declare (type (simple-array (unsigned-byte 8) 2) image-8))
    (when (black-off plt)
      ;; take out the black background by doing left and right raster
      ;; scans, converting black to white until bumping into non-black data
      (let ((xdim (array-dimension image-8 0))
	    (ydim (array-dimension image-8 1)))
	;; left scans
	(dotimes (i ydim)
	  (dotimes (j xdim)
	    (if (= (aref image-8 i j) *image-black*)
		(setf (aref image-8 i j) 127)
	      (return))))
	;; right scans
	(dotimes (i ydim)
	  (dotimes (j xdim)
	    (if (= (aref image-8 i (- xdim j 1)) *image-black*)
		(setf (aref image-8 i (- xdim j 1)) 127)
	      (return))))))
    (format strm "gsave~%")
    (ps:set-clip strm 0.0 (- lowerband height)
		 width (- height lowerband upperband))
    (ps:draw-image strm x y
		   (* mag (first (size im)))
		   (* mag (second (size im)))
		   (array-dimension image-8 0)
		   (array-dimension image-8 1)
		   image-8)
    (format strm "grestore~%")))

;;;----------------------------------------------------
;;; End.
