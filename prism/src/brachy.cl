;;;
;;; brachy
;;;
;;; Definitions of radiation sources for brachytherapy, i.e., line
;;; sources and seeds.
;;;
;;;  2-Sep-1992 I. Kalet created
;;; 23-Jun-1994 I. Kalet change float to single-float
;;; 30-Jan-1995 I. Kalet make classes subclasses of generic prism
;;;  classes, add more implementation details.
;;;  9-Jun-1996 I. Kalet lots more implementation details, split
;;;  panels off into brachy-panels.
;;;  7-Feb-2000 I. Kalet add announce of various slot update events,
;;; make default treat-time 1.0, not 0.0.
;;; 21-Feb-2000 I. Kalet take out rest pars in copy methods
;;; 27-Mar-2000 I. Kalet add raw coords slots.
;;;  6-Apr-2000 I. Kalet keep ap, lat mags and flags with each source,
;;; even though redundant.
;;; 10-May-2000 I. Kalet add application time and activity upper and
;;; lower limits parameters.
;;; 30-Jul-2000 I. Kalet replace distance-3d with inline code to make
;;; this module more self-contained.
;;; 31-Mar-2002 I. Kalet add support for permanent implants.
;;;  1-May-2002 I. Kalet add id attribute so sources can keep their
;;; numbers when some are deleted.
;;;  5-May-2002 I. Kalet add announcement of new treat-time when
;;; permanent flag is changed.
;;;  1-Aug-2002 I. Kalet include new data in announce for new coords,
;;; also announce update-plan when time or activity change, also copy
;;; id and display-color in copy methods.  Also change name of lateral
;;; film flag to lat-flag instead of ll-flag.
;;;  6-Oct-2002 I. Kalet change name of line-source event to
;;; new-location to match seed event name, move to parent class.
;;; 29-Jan-2003 I. Kalet change *brachy-activity-max*
;;;  3-Nov-2003 I. Kalet move some parameters here from
;;; brachy-dose-panels so they can be used with #. reader macro
;;;

(in-package :prism)

;;;--------------------------------------------------

(defparameter *brachy-activity-min* 0.0)
(defparameter *brachy-activity-max* 50000.0)
(defparameter *brachy-app-time-min* 0.0)
(defparameter *brachy-app-time-max* 3000.0)

;;;--------------------------------------------------

(defclass brachy-source (generic-prism-object)

  ((id :type fixnum
       :accessor id
       :initarg :id
       :documentation "Sources are numbered sequentially when first
created but keep their numbers even when some are deleted.")

   (source-type :initarg :source-type
		:accessor source-type
		:documentation "The type of radiation source, as well
as the designation of the model or size.")

   (new-source-type :type ev:event
		    :accessor new-source-type
		    :initform (ev:make-event)
		    :documentation "Announced when the source type is
updated.")

   (activity :type single-float
	     :initarg :activity
	     :accessor activity
	     :documentation "The source strength in e.g., millicuries,
or some other activity unit.")

   (new-activity :type ev:event
		 :accessor new-activity
		 :initform (ev:make-event)
		 :documentation "Announced when the activity is
changed.")

   (permanent :initarg :permanent
	      :accessor permanent
	      :documentation "t if permanent implant source.")

   (treat-time :type single-float
	       :initarg :treat-time
	       :accessor treat-time
	       :documentation "Number of hours the source is left
in.")

   (new-treat-time :type ev:event
		   :accessor new-treat-time
		   :initform (ev:make-event)
		   :documentation "Announced when the insertion time
is changed.")

   (display-color :initarg :display-color
		  :accessor display-color)

   (new-color :type ev:event
	      :accessor new-color
	      :initform (ev:make-event)
	      :documentation "Announced when the color is changed.")

   (update-plan :type ev:event
		:accessor update-plan
		:initform (ev:make-event)
		:documentation "Announced when anything happens that
should update a containing plan's time stamp.")

   (new-location :type ev:event
		 :accessor new-location
		 :initform (ev:make-event)
		 :documentation "Announced when the location for a
seed or an endpoint for a line source changes.")

   (ap-flag :accessor ap-flag
	    :initarg :ap-flag
	    :documentation "True if using AP film rather than PA")

   (ap-mag :type single-float
	   :accessor ap-mag
	   :initarg :ap-mag
	   :documentation "The AP film magnification, a number greater
than 1.0 usually")

   (lat-flag :accessor lat-flag
	     :initarg :lat-flag
	     :documentation "True if using right lateral film rather
than left lateral film")

   (lat-mag :type single-float
	    :accessor lat-mag
	    :initarg :lat-mag
	    :documentation "The lateral film magnification, a number
greater than 1.0 usually")

   (rotation :type single-float
	     :accessor rotation
	     :initarg :rotation
	     :documentation "The amount the orthogonal films are
rotated from exactly AP/Lateral.")

   (raw-ap-coords :type list
		  :accessor raw-ap-coords
		  :initarg :raw-ap-coords
		  :documentation "The raw data from orthogonal film
entry of source coordinates.")

   (raw-lat-coords :type list
		   :accessor raw-lat-coords
		   :initarg :raw-lat-coords
		   :documentation "The raw data from orthogonal film
entry of source coordinates.")

   (result :type dose-result
	   :initarg :result
	   :accessor result
	   :initform (make-dose-result)
	   :documentation "The result of computing dose from this
source.")

   )

  (:default-initargs :name "" :id 0 :activity 10.0 :permanent nil
		     :treat-time 1.0
		     :ap-flag t :ap-mag 1.0 :lat-flag t :lat-mag 1.0
		     :rotation 0.0
		     :raw-ap-coords nil :raw-lat-coords nil
		     :display-color 'sl:red)

  (:documentation "Brachy sources all share certain characteristics,
collected here in a base class.")

  )

;;;---------------------------------------------

(defmethod slot-type ((object brachy-source) slotname)

  (case slotname
	(result :object)
	(otherwise :simple)))

(defmethod not-saved ((object brachy-source)) 

  (append (call-next-method)
	  '(new-source-type new-activity new-treat-time new-color
	    update-plan new-location result)))

;;;---------------------------------------------

(defmethod invalidate-results ((src brachy-source) &rest ignored)

  "invalidate-results (src brachy-source) &rest ignored

An action function that invalidates a source's dose results and
announces update-plan event.  Called in response to various changes to
source attributes."

  (declare (ignore ignored))
  (setf (valid-grid (result src)) nil)
  (setf (valid-points (result src)) nil)
  (ev:announce src (update-plan src)))

;;;---------------------------------------------

(defmethod (setf source-type) :after (new-type (src brachy-source))

  (invalidate-results src)
  (ev:announce src (new-source-type src) new-type))

;;;---------------------------------------------

(defmethod (setf activity) :after (new-act (src brachy-source))

  (ev:announce src (update-plan src))
  (ev:announce src (new-activity src) new-act))

;;;---------------------------------------------

(defmethod treat-time :around ((src brachy-source))

  (if (permanent src)
      (/ (half-life (source-data (source-type src))) 0.693)
    (call-next-method)))

;;;---------------------------------------------

(defmethod (setf treat-time) :around (new-time (src brachy-source))

  (declare (ignore new-time))
  (if (permanent src)
      (treat-time src) ;; don't do anything but return correct value
    (call-next-method))) ;; go ahead and set the new value

;;;---------------------------------------------

(defmethod (setf treat-time) :after (new-time (src brachy-source))

  (ev:announce src (update-plan src))
  (ev:announce src (new-treat-time src) new-time))

;;;---------------------------------------------

(defmethod (setf permanent) :after (newval (src brachy-source))

  (declare (ignore newval))
  (ev:announce src (update-plan src))
  (ev:announce src (new-treat-time src) (treat-time src)))

;;;--------------------------------------

(defmethod (setf display-color) :after (col (src brachy-source))

  (ev:announce src (new-color src) col))

;;;---------------------------------------------

(defclass line-source (brachy-source)

  ((end-1 :initarg :end-1
	  :accessor end-1
	  :documentation "The x,y,z coordinates of one end of the
source.")

   (end-2 :initarg :end-2
	  :accessor end-2
	  :documentation "The x,y,z coordinates of the other end of
the source.")

   )

  (:default-initargs :name "Line source"
		     :end-1 '(0.0 0.0 1.0)
		     :end-2 '(0.0 0.0 -1.0))

  (:documentation "Line sources are sealed reusable tubes or needles
of radioactive material, radium, cesium or other...")

  )

;;;---------------------------------------------

(defun make-line-source (src-name &rest initargs)

  "make-line-source src-name

returns a line source with specified or default name and initargs."

  (apply #'make-instance 'line-source
	 :name (if (equal src-name "")
		   (format nil "~A" (gensym "LINESRC-"))
		 src-name)
	 initargs))

;;;---------------------------------------------

(defmethod (setf end-1) :after (new-end (src line-source))

  (invalidate-results src)
  (ev:announce src (new-location src) new-end))

;;;---------------------------------------------

(defmethod (setf end-2) :after (new-end (src line-source))

  (invalidate-results src)
  (ev:announce src (new-location src) new-end))

;;;---------------------------------------------

(defmethod copy ((obj line-source))

  (make-line-source ""
		    :id (id obj)
		    :source-type (source-type obj)
		    :activity (activity obj)
		    :treat-time (treat-time obj)
		    :display-color (display-color obj)
		    :result (copy (result obj))
		    :end-1 (end-1 obj)
		    :end-2 (end-2 obj)))

;;;---------------------------------------------

(defun source-length (src)

  (let* ((end1 (end-1 src))
	 (end2 (end-2 src))
	 (dx (- (first end2) (first end1)))
	 (dy (- (second end2) (second end1)))
	 (dz (- (third end2) (third end1))))
    (declare (single-float dx dy dz))
    (sqrt (+ (* dx dx) (* dy dy) (* dz dz)))))

;;;---------------------------------------------
;;;
;;; Seeds
;;;
;;;---------------------------------------------

(defclass seed (brachy-source)

  ((location :initarg :location
	     :accessor location
	     :documentation "The x,y,z coordinates of the source.")

   )

  (:default-initargs :name "Seed"
		     :location '(0.0 0.0 0.0))

  (:documentation "Seeds are iridium, gold or iodine placed surgically
in the tumor area.")

  )

;;;---------------------------------------------

(defun make-seed (src-name &rest initargs)

  "make-seed src-name

returns a seed with specified or default name and initargs."

  (apply #'make-instance 'seed
	 :name (if (equal src-name "")
		   (format nil "~A" (gensym "SEED-"))
		 src-name)
	 initargs))

;;;---------------------------------------------

(defmethod (setf location) :after (new-loc (src seed))

  (invalidate-results src)
  (ev:announce src (new-location src) new-loc))

;;;---------------------------------------------

(defmethod copy ((obj seed))

  (make-seed ""
	     :id (id obj)
	     :source-type (source-type obj)
	     :activity (activity obj)
	     :treat-time (treat-time obj)
	     :display-color (display-color obj)
	     :result (copy (result obj))
	     :location (location obj)))

;;;---------------------------------------------
;;; End.
