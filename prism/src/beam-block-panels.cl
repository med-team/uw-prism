;;;
;;; beam-block-panels
;;;
;;; this module defines the block editing panel and adjunct stuff.
;;;
;;;  2-Jun-1994 I. Kalet created, modified a lot.
;;; 11-Jul-1994 J. Unger transform entered block contours from gantry to
;;; collimator space, fixing bug.
;;; 15-Jul-1994 J. Unger fix some labels, make block panel use sfd to 
;;; scale input contours, rescale when sfd changes.
;;; 21-Jul-1994 J. Unger rename gantry-to-coll & coll-to-gantry to 
;;; rotate-vertices & move to polygons package.
;;; 01-Aug-1994 J. Unger make some scaling changes to reconform to spec.
;;; 02-Aug-1994 J. Unger turn contour-editor into block-editor and split
;;; off into its own module.
;;; 12-Jan-1995 I. Kalet destroy bev too.  Why was it commented out?
;;;  Also, move isodist function to beams.  Use here and elsewhere.
;;;  Get beam for the current block from passed parameter, not a block
;;;  attribute.  Same for plan-of and patient to pass to bev-draw-all.
;;; 30-Apr-1995 I. Kalet delete reference to block editor, just use a
;;; generic contour editor.  Set digitizer-mag in contour editor when
;;; sfd changes, just like mlc panel, in coll-panels.
;;; 19-Sep-1996 I. Kalet update call to bev-draw-all due to signature
;;; change, and make textlines numeric that should have been.
;;; 21-Jan-1997 I. Kalet eliminate table-position.
;;; 29-Apr-1997 I. Kalet add beam name to title bar of block panel.
;;; 11-Jun-1998 I. Kalet change :beam to :beam-for in make-view call.
;;; 23-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;;  5-Sep-1999 I. Kalet added declutter and DRR buttons, but no DRR yet.
;;; 28-May-2000 I. Kalet adjust button size and frame size.
;;; Parametrize font selection.
;;; 10-Sep-2000 I. Kalet add DRR image display support, by adding the
;;; bev to the plan view set, with controls here like the view panel.
;;; 27-Nov-2000 I. Kalet make this into a beam-blocks panel, for all
;;; the blocks at once, not one panel per block, i.e., make it like the
;;; volume editor, with all the organs.  Also include the block rotate
;;; button here, and window and level controls for DRR.  Separate out
;;; the name, color and transmission into a block-attribute-editor as
;;; for pstructs.
;;;  2-Dec-2000 I. Kalet don't use plan view set to generate
;;; background, as it introduces unpredictable updates.
;;; 14-Mar-2001 I. Kalet fix error that leaves old block graphic
;;; primitive in background view when block is deleted.  Allow
;;; deletion of currently selected block, and allow for no block
;;; selected.
;;; 23-Jun-2001 I. Kalet add remove-notify for deleted event for
;;; blocks set in beam when destroying the block panel.
;;; 23-Aug-2004 I. Kalet fix erroneous code that pretends to be a
;;; special beam-view-mediator, use bev-draw-all instead of
;;; refresh-bev, encapsulate in a local function, blp-update
;;; 19-Jan-2005 I. Kalet change make-contour-editor to make-planar-editor
;;;

(in-package :prism)

;;;---------------------------------------------

(defclass block-panel (generic-panel)

  ((current-block :accessor current-block
		  :initarg :current-block
		  :documentation "The block currently being edited.")

   (beam-of :accessor beam-of
	    :initarg :beam-of
	    :documentation "The beam holding the blocks.")

   (plan-of :accessor plan-of
	    :initarg :plan-of
	    :documentation "The plan of the current beam.")
   
   (patient-of :accessor patient-of
	       :initarg :patient-of
	       :documentation "The patient - needed for bev-draw-all.")

   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame for this panel.")

   (delete-b :accessor delete-b
	     :documentation "The delete panel button.")

   (sfd-box :accessor sfd-box
	    :documentation "The source to film distance textline.")

   (filmdist :accessor filmdist
	     :initarg :filmdist
	     :documentation "The source to film distance.")

   (block-rot-b :accessor block-rot-b
                :documentation "The block rotation button.")

   (image-button :accessor image-button
		 :documentation "The button that toggles display of
image data in this view.")

   (fg-button :accessor fg-button
	      :documentation "Brings up a popup menu of objects in the
view to display them or not on a view by view basis.")

   (viewlist-panel :accessor viewlist-panel
		   :initform nil
		   :documentation "Temporarily holds the list of
objects visible on the screen.")

   (window-control :accessor window-control
		   :documentation "The textline that displays and sets
the window for the background view's image.")

   (level-control :accessor level-control
		  :documentation "The textline that displays and sets
the level for the background view's image.")

   (block-sp :accessor block-sp
	     :documentation "The selector panel for blocks.")

   (block-ed :accessor block-ed
	     :documentation "The contour editor for the current block's
contour.")

   (bev :accessor bev
	:documentation "A beam's eye view used as background for the
contour editor.")

   (image-mediator :accessor image-mediator
		   :initform nil
		   :documentation "A single image-view-mediator to
manage creation of DRR images as necessary for the view background.")

   (busy :accessor busy
	 :initform nil
	 :documentation "The busy flag for managing updates to
settings.")

   )

  (:default-initargs :current-block nil :filmdist 100.0)

  (:documentation "A block panel provides for entry and edit of a set
of shielding blocks for a beam.")

  )

;;;---------------------------------------------

(defmethod (setf current-block) :before (new-blk (blp block-panel))

  "Disconnects the old block, if present, before setting the new one."

  (declare (ignore new-blk))
  (if (current-block blp)
    (ev:remove-notify blp (new-color (current-block blp)))))

;;;---------------------------------------------

(defmethod (setf current-block) :after (new-blk (blp block-panel))

  "Updates the contour editor background and vertices, and connections
to the new block, after the old one has been deselected.  The selector
panel creates and places the attribute editor."

  (setf (foreground (bev blp))
    (remove new-blk (foreground (bev blp)) :key #'object))
  (bev-draw-all (bev blp) (plan-of blp) (patient-of blp) new-blk)
  (display-view (bev blp))
  (if new-blk
      (progn
	(setf (vertices (block-ed blp))
	  (poly:rotate-vertices (vertices new-blk)
				(collimator-angle (beam-of blp))))
	(setf (color (block-ed blp)) (sl:color-gc (display-color new-blk)))
	(ev:add-notify blp (new-color new-blk)
		       #'(lambda (pan blk col)
			   (declare (ignore blk))
			   (setf (color (block-ed pan)) (sl:color-gc col)))))
    (setf (vertices (block-ed blp)) nil)))

;;;---------------------------------------------

(defmethod initialize-instance :after ((blp block-panel) &rest
				       initargs)

  (let* ((bm (beam-of blp))
	 (size large) ;; constant from prism-globals
	 (btw 150)
	 (bth 25)
	 (font (symbol-value *small-font*))
	 (margin 5)
	 (top-y margin)
	 (dy (+ bth margin))
	 (bpfr (apply #'sl:make-frame
		      (+ size btw (* 2 margin)) (+ size bth 10)
		      :title (format nil "Block Editor for ~A"
				     (name bm))
		      initargs))
	 (win (sl:window bpfr))
	 (del-b (apply #'sl:make-button btw bth
		       :font font :label "Delete Panel" :parent win
		       :ulc-x margin :ulc-y top-y
		       initargs))
	 (sfd-t (apply #'sl:make-textline btw bth
		       :font font :label "SFD: " :parent win
		       :ulc-x margin :ulc-y (bp-y top-y dy 1)
		       :numeric t :lower-limit 10.0 :upper-limit 200.0
		       initargs))
         (blk-rot-b (apply #'sl:make-button btw bth
			   :label "Rotate Blocks"
			   :ulc-x margin :ulc-y (bp-y top-y dy 2)
			   :parent win :font font
			   initargs))
	 (image-b (apply #'sl:make-button btw bth
			 :font font :label "Image" :parent win
			 :ulc-x margin :ulc-y (bp-y top-y dy 3)
			 initargs))
	 (fg-b (apply #'sl:make-button btw bth
		      :font font :label "Objects" :parent win
		      :ulc-x margin :ulc-y (bp-y top-y dy 4)
		      initargs))
	 (win-ctl (apply #'sl:make-sliderbox btw bth 1.0 2047.0 9999.0
			 :parent win
			 :font font :label "Win: "
			 :ulc-x 0 :ulc-y (bp-y top-y dy 5)
			 :border-width 0
			 :display-limits nil
			 initargs))
	 (lev-ctl (apply #'sl:make-sliderbox btw bth 1.0 4095.0 9999.0
			 :parent win
			 :font font :label "Lev: "
			 :ulc-x 0 :ulc-y (bp-y top-y dy 7)
			 :border-width 0
			 :display-limits nil
			 initargs))
	 (blk-sp (make-selector-panel
		  btw 150 "Add a block"
		  (blocks bm)
		  #'(lambda (name)
		      (make-beam-block name
				       :display-color (display-color bm)))
		  #'(lambda (blk)
		      (setf (current-block blp) blk)
		      (let ((bt (button-for blk (block-sp blp))))
			(setf (sl:allow-button-2 bt) t)
			(ev:add-notify blp (sl:button-2-on bt)
				       #'(lambda (pan b)
					   (declare (ignore b))
					   (setf (current-block pan) nil)))
			(ev:add-notify blp (sl:button-off bt)
				       #'(lambda (pan b)
					   (ev:remove-notify
					    pan (sl:button-2-on b))
					   (ev:remove-notify
					    pan (sl:button-off b)))))
		      (make-attribute-editor blk
					    :parent win :font font
					    :width btw
					    :ulc-x margin
					    :ulc-y (bp-y top-y dy 14)))
		  :ulc-x margin
		  :ulc-y (bp-y top-y dy 9)
		  :parent win :font font
		  :use-color t :radio t))
	 (bev (make-view size size 'beams-eye-view :beam-for bm
			 :display-func
			 #'(lambda (vw)
			     (setf (image-cache vw) nil)
			     (draw (image (image-mediator blp)) vw)
			     (display-view vw)
			     (display-planar-editor (block-ed blp)))))
	 (cb (first (coll:elements (blocks bm)))) ;; could be nil
	 (ce (apply #'make-planar-editor
		    :background (sl:pixmap (picture bev))
		    :vertices nil
		    :x-origin (/ size 2) :y-origin (/ size 2)
		    :scale (scale bev)
		    :digitizer-mag (/ (filmdist blp) (isodist bm))
		    :color (sl:color-gc (if cb (display-color cb)
					  (display-color bm)))
		    :ulc-x (+ btw (* 2 margin))
		    :parent win
		    initargs)))
    ;; install them and connect them up to the collimator settings
    (setf (delete-b blp) del-b
	  (sl:info sfd-t) (filmdist blp)
	  (sfd-box blp) sfd-t
	  (block-rot-b blp) blk-rot-b
	  (image-button blp) image-b
	  (fg-button blp) fg-b
	  (window-control blp) win-ctl
	  (level-control blp) lev-ctl
	  (block-sp blp) blk-sp
	  (bev blp) bev
	  (block-ed blp) ce
	  (panel-frame blp) bpfr)
    (ev:add-notify blp (sl:button-on del-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (destroy pan)))
    (ev:add-notify blp (sl:new-info sfd-t)
		   #'(lambda (pan tl info)
		       (declare (ignore tl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (filmdist pan) (read-from-string info))
                         (setf (digitizer-mag (block-ed pan))
			   (/ (filmdist pan) (isodist (beam-of pan))))
			 (setf (busy pan) nil))))
    (ev:add-notify blp (sl:button-on blk-rot-b)
		   #'(lambda (pan btn)
		       (let ((blks (coll:elements (blocks (beam-of pan)))))
			 (if blks
			     (let* ((choice (sl:popup-menu 
					     '("Rotate 90 degrees"
					       "Rotate 180 degrees"
					       "Rotate 270 degrees")))
				    (angle (when choice (* 90.0 (1+ choice)))))
			       (when choice
				 (dolist (blk (coll:elements
					       (blocks (beam-of pan))))
				   (setf (vertices blk)
				     (poly:rotate-vertices (vertices blk)
							   angle)))
				 (bev-draw-all (bev pan)
					       (plan-of pan)
					       (patient-of pan)
					       (current-block pan))
				 (display-view (bev pan))
				 (setf (vertices (block-ed pan))
				   (poly:rotate-vertices
				    (vertices (current-block pan))
				    (collimator-angle (beam-of pan))))))
			   (sl:acknowledge
			    '("No block added or selected"
			      "Please add or select a block first"))))
		       (setf (sl:on btn) nil)))
    (setf (image-button bev) (image-button blp))
    (setf (drr-state bev) (drr-state bev)) ;; to init the button
    (ev:add-notify blp (sl:button-on (image-button blp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (bev pan)) t)
			 (display-planar-editor (block-ed pan))
			 (setf (busy pan) nil))))
    (ev:add-notify blp (sl:button-off (image-button blp))
		   #'(lambda (pan bt)
                       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (bev pan)) nil)
			 (display-planar-editor (block-ed pan))
			 (setf (busy pan) nil))))
    (ev:add-notify blp (sl:button-2-on (image-button blp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (case (drr-state (bev pan))
			   ;;'stopped is a noop
			   ('running
			    (setf (drr-state (bev pan)) 'paused))
			   ('paused
			    (setf (drr-state (bev pan)) 'running)
			    (drr-bg (bev pan))))
			 (setf (busy pan) nil))))
    (ev:add-notify blp (bg-toggled bev)
		   #'(lambda (pan vw)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:on (image-button pan))
			   (background-displayed vw))
			 (setf (busy pan) nil))))
    (ev:add-notify blp (sl:button-on (fg-button blp))
		   #'(lambda (pan bt)
		       (setf (viewlist-panel pan)
			 (make-instance 'viewlist-panel
			   :refresh-fn #'(lambda (vw)
					   (display-view vw)
					   (display-planar-editor ce))
			   :view (bev pan)))
		       (ev:add-notify pan (deleted (viewlist-panel
						    pan))
				      #'(lambda (pnl vlpnl)
					  (declare (ignore vlpnl))
					  (setf (viewlist-panel pnl) nil)
					  (when (not (busy pnl))
					    (setf (busy pnl) t)
					    (setf (sl:on bt) nil)
					    (setf (busy pnl) nil))))))
    (ev:add-notify blp (sl:button-off (fg-button blp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (destroy (viewlist-panel pan))
			 (setf (busy pan) nil))))
    (ev:add-notify blp (new-vertices ce)
		   #'(lambda (pan ced new-verts)
		       (declare (ignore ced))
		       (if (current-block pan)
			   (setf (vertices (current-block pan))
			     (poly:rotate-vertices new-verts 
						   (- (collimator-angle
						       (beam-of pan)))))
			 (sl:acknowledge
			  '("No block added or selected"
			    "Please add or select a block first")))))
    (ev:add-notify blp (new-scale ce)
		   #'(lambda (pan ced new-sc)
		       (let ((bev (bev pan)))
			 (setf (scale bev) new-sc)
			 (bev-draw-all bev (plan-of pan) (patient-of pan)
				       (current-block pan))
			 (display-view bev)
			 (display-planar-editor ced))))
    (ev:add-notify blp (new-origin ce)
		   #'(lambda (pan ced new-org)
		       (let ((bev (bev pan)))
			 (setf (origin bev) new-org)
			 (bev-draw-all bev (plan-of pan) (patient-of pan)
				       (current-block pan))
			 (display-view bev)
			 (display-planar-editor ced))))
    (setf (sl:setting (window-control blp))
      (coerce (window bev) 'single-float))
    (ev:add-notify blp (sl:value-changed (window-control blp))
		   #'(lambda (pan wc win)
		       (declare (ignore wc))
		       (setf (window (bev pan)) (round win))
		       (if (background-displayed (bev pan))
			   (display-planar-editor (block-ed pan)))))
    (setf (sl:setting (level-control blp))
      (coerce (level bev) 'single-float))
    (ev:add-notify blp (sl:value-changed (level-control blp))
		   #'(lambda (pan lc lev)
		       (declare (ignore lc))
		       (setf (level (bev pan)) (round lev))
		       (if (background-displayed (bev pan))
			   (display-planar-editor (block-ed pan)))))
    (if (image-set (patient-of blp))
	(setf (image-mediator blp)
	  (make-image-view-mediator (image-set (patient-of blp)) bev)))
    ;; this is a special beam-view mediator for this view only
    (flet ((blp-update (pan bm arg)
	     (declare (ignore bm arg))
	     (let ((bev (bev pan)))
	       (setf (drr-state bev) 'stopped)
	       (ev:announce bev (reset-image bev))
	       (bev-draw-all bev (plan-of pan) (patient-of pan)
			     (current-block pan))
	       (display-view bev)
	       (display-planar-editor (block-ed pan)))))
      (ev:add-notify blp (new-color bm) #'blp-update)
      (ev:add-notify blp (axis-changed bm) #'blp-update)
      (ev:add-notify blp (new-coll-set (collimator bm))
		     #'(lambda (pnl coll)
			 (blp-update pnl coll nil)))
      (ev:add-notify blp (new-id (wedge bm)) #'blp-update)
      (ev:add-notify blp (new-rotation (wedge bm)) #'blp-update)
      (ev:add-notify blp (new-gantry-angle bm) #'blp-update)
      (ev:add-notify blp (new-couch-angle bm) #'blp-update)
      (ev:add-notify blp (new-couch-lat bm) #'blp-update)
      (ev:add-notify blp (new-couch-ht bm) #'blp-update)
      (ev:add-notify blp (new-couch-long bm) #'blp-update)
      (ev:add-notify blp (new-machine bm) 
		     #'(lambda (pnl b mach)
			 (ev:add-notify pnl (new-coll-set (collimator b))
					#'(lambda (pnl coll)
					    (blp-update pnl coll nil)))
			 (blp-update pnl b mach))))
    ;; this is to remove the contour of a block that is deleted
    (ev:add-notify blp (coll:deleted (blocks (beam-of blp)))
		   #'(lambda (pan blkset blk)
		       (declare (ignore blkset))
		       (let ((vw (bev pan)))
			 (setf (foreground vw)
			   (remove blk (foreground vw) :key #'object))
			 (display-view vw)
			 (display-planar-editor (block-ed pan)))))
    ;; this is to keep the current block consistent
    (ev:add-notify blp (new-coll-angle bm)
		   #'(lambda (pan bm4 newang)
		       (draw bm4 (bev pan))
		       (display-view (bev pan))
		       (if (current-block pan)
			   (setf (vertices (block-ed pan))
			     (poly:rotate-vertices
			      (vertices (current-block pan)) newang)))))
    (unless (select-1 blk-sp)
      (bev-draw-all bev (plan-of blp) (patient-of blp))
      (display-view bev))
    (display-planar-editor ce)))

;;;---------------------------------------------

(defmethod destroy :before ((bp block-panel))

  (let ((vw (bev bp))
	(bm (beam-of bp)))
    ;; ensure that there are not any lingering 
    ;;   background jobs for this view-panel
    (remove-bg-drr vw)
    (when (eq 'running (drr-state vw))
      (setf (drr-state vw) 'paused))
    (setf (image-button vw) nil)
    (ev:remove-notify bp (new-color bm))
    (ev:remove-notify bp (axis-changed bm))
    (ev:remove-notify bp (new-coll-set (collimator bm)))
    (ev:remove-notify bp (new-id (wedge bm)))
    (ev:remove-notify bp (new-rotation (wedge bm)))
    (ev:remove-notify bp (new-gantry-angle bm))
    (ev:remove-notify bp (new-couch-angle bm))
    (ev:remove-notify bp (new-couch-lat bm))
    (ev:remove-notify bp (new-couch-ht bm))
    (ev:remove-notify bp (new-couch-long bm))
    (ev:remove-notify bp (new-machine bm))
    (ev:remove-notify bp (new-coll-angle bm))
    (if (current-block bp)
	(ev:remove-notify bp (new-color (current-block bp))))
    (if (image-mediator bp) (destroy (image-mediator bp)))
    (ev:remove-notify bp (coll:deleted (blocks (beam-of bp))))
    (destroy vw))
  (sl:destroy (delete-b bp))
  (sl:destroy (sfd-box bp))
  (sl:destroy (block-rot-b bp))
  (sl:destroy (image-button bp))
  (if (sl:on (fg-button bp)) (setf (sl:on (fg-button bp)) nil))
  (sl:destroy (fg-button bp))
  (sl:destroy (window-control bp))
  (sl:destroy (level-control bp))
  (destroy (block-sp bp))
  (destroy (block-ed bp))
  (sl:destroy (panel-frame bp)))

;;;---------------------------------------------

(defun make-block-panel (bm pln pat)

  "make-block-panel bm pln pat

returns a block panel for beam bm, in plan pln, for patient pat."

  (make-instance 'block-panel
    :beam-of bm :plan-of pln :patient-of pat))

;;;---------------------------------------------

(defclass block-attribute-editor (attribute-editor)

  ((trans-box :accessor trans-box
	      :documentation "The textline for the transmission
factor.")
   )

  (:default-initargs :height 95)

  (:documentation "The subclass of attribute-editor that is specific
to beam blocks")
  )

;;;---------------------------------------------

(defmethod initialize-instance :after ((ble block-attribute-editor) 
				       &rest initargs)

  "Initializes the user interface for the beam block attribute editor."

  (let* ((frm (fr ble))
         (frm-win (sl:window frm))
	 (obj (object ble))
	 (dx 5)
	 (bth 25)
	 (btw (button-width ble))
	 (att-f (symbol-value *small-font*)) ;; the value, not the symbol
	 (tran-t (apply #'sl:make-textline btw bth
			:font att-f :label "Trans: " :parent frm-win
			:ulc-x dx :ulc-y (bp-y dx bth 2)
			:numeric t :lower-limit 0.0 :upper-limit 1.0
			initargs)))
    (setf (sl:info tran-t) (transmission obj)
	  (trans-box ble) tran-t)
    (ev:add-notify ble (sl:new-info tran-t)
		   #'(lambda (pan tl info)
		       (declare (ignore tl))
		       (setf (transmission (object pan))
			 (coerce (read-from-string info)
				 'single-float))))))

;;;---------------------------------------------

(defmethod make-attribute-editor ((blk beam-block) &rest initargs)

  "make-attribute-editor (blk beam-block) &rest initargs

Returns a beam-block-specific attribute-editor with specified parameters."

  (apply #'make-instance 'block-attribute-editor
	 :object blk :allow-other-keys t
	 initargs))

;;;---------------------------------------------

(defmethod destroy :before ((ble block-attribute-editor))

  (sl:destroy (trans-box ble)))

;;;---------------------------------------------
;;; End.
