;;;
;;; prism.config
;;;
;;; The global variable configuration file for the University of
;;; Washington Radiation Oncology Department.
;;;
;;; This file sets only those variables that are different from the
;;; defaults in prism-globals.
;;;

(in-package :prism)

;; The second entry in each pair in the *digitizer-devices*
;; association list is simply the device file name of the serial port
;; to which the digitizer is connected.  The prism digitizer code sets
;; the baud rate, etc.

(setf *digitizer-devices*
      '(("violin1.radonc.washington.edu" "/dev/ttyS0")
	("violin2.radonc.washington.edu" "/dev/ttyS0")
	("viola.radonc.washington.edu" "/dev/ttyS0")
	("cello.radonc.washington.edu" "/dev/ttyS0")
	("bass.radonc.washington.edu" "/dev/ttyS0")
	("dosim2.seattlecca.org" "/dev/ttyS0")
	))

(setf *fine-grid-size* 0.35)
(setf *medium-grid-size* 0.5)
(setf *coarse-grid-size* 1.0)

;;; sys:GETENV is Allegro-specific.  Put alternative here for other Lisps.
(let ((host #+:Allegro (sys:getenv "HOST") #-:Allegro ""))
  ;;
  (cond ((search "radonc.washington.edu" host)
	 (setf *postscript-printers* '("ps146b" "ps184" "ps136" "p790"
				       "ps143e" "ps146a" "simjet"
				       "ps136d" "File Only"))
	 (setf *plotters* '(("p790" ps-plot)
			    ("ps184" ps-plot)
			    ("ps146b" ps-plot)
			    ("ps143e" ps-plot)
			    ("ps146a" ps-plot)
			    ("simjet" ps-plot)
			    ("PS File only" ps-plot))))
	;;
	((search "seattlecca.org" host)
	 (setf *postscript-printers* '("ps146b" "ps184" "ps136" "p790"
				       "scca-bw" "scca-color"
				       "scca-ricoh" "File Only"))
	 (setf *plotters* '(("p790" ps-plot)
			    ("ps184" ps-plot)
			    ("ps146b" ps-plot)
			    ("scca-color" ps-plot)
			    ("scca-bw" ps-plot)
			    ("scca-ricoh" ps-plot)
			    ("PS File only" ps-plot))))
	;;
	(t (error "Bad domain in \"prism.config\" file: ~S" host))))

(setf *plot-sizes* '((small "8.5x11" 19.05 25.4)
		     (wide-small "11x8.5" 25.4 19.05)
		     (ledger "17x11" 40.64 25.4)
		     (large "11x17" 25.4 40.64)
		     (film "14x17" 33.0 40.64)
		     (wide-film "17x14" 40.64 33.0)
		     ))

(setf *easel-size* large)

(setf *fg-gray-level* 1.0)                         ;; white default foreground
(setf *bg-gray-level* 0.0)                         ;; black default background
(setf *border-style* :flat)                         ;; no Motif style here!

(setf dicom:*dicom-ae-titles*
      '(("bass.radonc.washington.edu" "prism-uw-bass")
	("bilbo.radonc.washington.edu" "prism-uw-bilbo")
	("cello.radonc.washington.edu" "prism-uw-cello")
	("eowyn.radonc.washington.edu" "prism-uw-eowyn")
	("flute.radonc.washington.edu" "prism-uw-flute")
	("gold.radonc.washington.edu" "prism-uw-gold")
	("imrt.radonc.washington.edu" "prism-uw-imrt")
	("jeeves.radonc.washington.edu" "prism-uw-jeeves")
	("mvi.radonc.washington.edu" "prism-uw-mvi")
	("ncd1.radonc.washington.edu" "prism-uw-ncd1")
	("silver.radonc.washington.edu" "prism-uw-silver")
	("viola.radonc.washington.edu" "prism-uw-viola")
	("violin1.radonc.washington.edu" "prism-uw-violin1")
	("violin2.radonc.washington.edu" "prism-uw-violin2")
	("woods.radonc.washington.edu" "prism-uw-woods")))

;;; 19-Sep-03 Set value for minimum leaf gap, which is a constraint
;;; on Elekta leaf settings.  Used by DICOM-RT panel
(setf *minimum-leaf-gap* 0.7)

;;; Checkpoint directory for individual user.
(setf *local-database* "~/prismlocal/")

;;; Main clinical patient case files.
(setf *patient-database* "/prismdata/clinical/cases/")

;;; Clinical shared checkpoint directory
(setf *shared-database* "/prismdata/clinical/casetemp/")

;;; Clinical images.
(setf *image-database* "/prismdata/clinical/images/")

;;; Clinical structure-set files.
(setf *structure-database* "/prismdata/clinical/structures/")

(load "/radonc/prism/pstable")

(load "/radonc/prism/point-calc")
(add-tool "PointCalc" 'point-calc)

#+allegro
(setf excl:*tenured-bytes-limit* 100000000)

;;; End.
