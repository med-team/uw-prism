;;;
;;; beam-transforms
;;;
;;; code for computing collimator to view space transforms for various
;;; views.
;;;
;;; 18-Sep-1996 I. Kalet split off from beam-graphics module.  Change
;;; signature to eliminate keywords.
;;;  4-Feb-1997 I. Kalet add coll-to-couch, couch-to-coll and other
;;; functions, for dose computation, also use in get-beam-transform
;;; methods.  Eliminate table-postion - always 0, 0, 0.  Eliminate
;;; references to geometry package.  Make
;;; get-transverse-beam-transform call coll-to-couch-transform instead
;;; of duplicating code.
;;; 19-Jan-1998 I. Kalet add declarations to matrix-multiply
;;; et.al. and rewrite the beam transform functions to use a simple
;;; array instead of multiple values.
;;;  7-Jul-1998 I. Kalet matrix-multiply also returns a simple array
;;;  instead of multiple values.
;;;

(in-package :prism)

;;;----------------------------------------------

(defun couch-to-coll-transform (tab gan col)

  "COUCH-TO-COLL-TRANSFORM tab gan col

Computes and returns the terms for the couch to collimator space
matrix transformation for couch angle tab, gantry angle gan and
collimator angle col.  See Prism Implementation Report for diagrams
and derivation."

  (declare (type single-float tab gan col))
  (let* ((trn-rad (* tab *pi-over-180*))
         (gan-rad (* gan *pi-over-180*))
         (col-rad (* col *pi-over-180*))
         (sin-t (sin trn-rad))
	 (cos-t (cos trn-rad))
	 (sin-g (sin gan-rad))
	 (cos-g (cos gan-rad))
	 (sin-c (sin col-rad))
	 (cos-c (cos col-rad))
	 (result (make-array 9 :element-type 'single-float)))
    (declare (type (simple-array single-float (9)) result)
	     (type single-float gan-rad col-rad trn-rad
		   sin-g cos-g sin-c cos-c sin-t cos-t))
    (setf (aref result 0) (+ (* cos-c cos-g cos-t)
			     (* sin-c sin-t)) ; r00
	  (aref result 1) (- (* cos-c sin-g)) ; r01
	  (aref result 2) (- (* cos-c cos-g sin-t)
			     (* sin-c cos-t)) ; r02
		 
	  (aref result 3) (- (* cos-c sin-t)
			     (* sin-c cos-g cos-t)) ; r10
	  (aref result 4) (* sin-c sin-g) ; r11
	  (aref result 5) (- (+ (* sin-c cos-g sin-t)
				(* cos-c cos-t))) ; r12

	  (aref result 6) (* sin-g cos-t) ; r20
	  (aref result 7) cos-g		; r21
	  (aref result 8) (* sin-g sin-t)) ; r22
    result))

;;;----------------------------------------------

(defun coll-to-couch-transform (tab gan col)

  "COLL-TO-COUCH-TRANSFORM tab gan col

Computes and returns the terms for the collimator to couch space
matrix transformation for couch angle tab, gantry angle gan and
collimator angle col.  See Prism Implementation Report for diagrams
and derivation."

  (declare (type single-float tab gan col))
  (let* ((trn-rad (* tab *pi-over-180*))
         (gan-rad (* gan *pi-over-180*))
         (col-rad (* col *pi-over-180*))
         (sin-t (sin trn-rad))
	 (cos-t (cos trn-rad))
	 (sin-g (sin gan-rad))
	 (cos-g (cos gan-rad))
	 (sin-c (sin col-rad))
	 (cos-c (cos col-rad))
	 (result (make-array 9 :element-type 'single-float)))
    (declare (type (simple-array single-float (9)) result)
	     (type single-float gan-rad col-rad trn-rad
		   sin-g cos-g sin-c cos-c sin-t cos-t))
    (setf (aref result 0) (+ (* cos-t cos-g cos-c)
			     (* sin-t sin-c)) ; r00
	  (aref result 1) (- (* sin-t cos-c)
			     (* cos-t cos-g sin-c)) ; r01
	  (aref result 2) (* cos-t sin-g) ; r02

	  (aref result 3) (- (* sin-g cos-c)) ; r10
	  (aref result 4) (* sin-g sin-c) ; r11
	  (aref result 5) cos-g		; r12

	  (aref result 6) (- (* sin-t cos-g cos-c)
			     (* cos-t sin-c)) ; r20
	  (aref result 7) (- (+ (* sin-t cos-g sin-c)
				(* cos-t cos-c))) ; r21
	  (aref result 8) (* sin-t sin-g)) ; r22
    result))

;;;----------------------------------------------

(defun matrix-multiply (xfrm x y z)

  "MATRIX-MULTIPLY xfrm x y z

returns an array, the x, y and z components of a vector resulting from
multiplying the 3 by 3 array represented by 1-dimensional array xfrm
by the vector represented by the components x, y, z."

  (declare (type (simple-array single-float (9)) xfrm)
	   (type single-float x y z))
  (make-array 3 :element-type 'single-float
	      :initial-contents (list
				 (+ (* (aref xfrm 0) x)
				    (* (aref xfrm 1) y)
				    (* (aref xfrm 2) z))
				 (+ (* (aref xfrm 3) x)
				    (* (aref xfrm 4) y)
				    (* (aref xfrm 5) z))
				 (+ (* (aref xfrm 6) x)
				    (* (aref xfrm 7) y)
				    (* (aref xfrm 8) z)))))

;;;----------------------------------------------

(defmethod beam-transform ((b beam) (tv transverse-view)
			   &optional wedge)

  "BEAM-TRANSFORM b tv &optional wedge

Computes and returns the terms for the collimator to view space matrix
transformation for beam b and transverse view tv.  Calls
transverse-beam-transform to do the actual work, since the result is
needed for situations that have no view as well as for a view."

  (transverse-beam-transform b (view-position tv) wedge))

;;;----------------------------------------------

(defun transverse-beam-transform (b vp &optional wedge)

  "TRANSVERSE-BEAM-TRANSFORM b vp &optional wedge

Computes and returns the terms for the collimator to view space matrix
transformation for beam b and position vp assuming a transverse view
at vp, though there needn't be an actual view present.  The transform
is computed using the coll-to-couch-transform function above, couch
displacement values, and the specified position, vp.  Twelve values
are returned in a 1-dimensional simple array -- the homogeneous matrix
entries:

  r00 r01 r02 r03
  r10 r11 r12 r13
  r20 r21 r22 r23

the bottom row is not returned, since it is always 0 0 0 1.  If the
keyword wedge parameter is nil (default) and b has a multileaf
collimator, then the transform returned is computed with a collimator
angle of 0.0, regardless of the actual collimator angle of b.  If
wedge is non-nil or b has another type of collimator, then b's actual
collimator angle is used when computing the transformation."

  (let ((matrix (coll-to-couch-transform (couch-angle b)
					 (gantry-angle b)
					 (if (and (not wedge)
						  (typep (collimator b)
							 'multileaf-coll))
					     0.0
					   (collimator-angle b))))
	(result (make-array 12 :element-type 'single-float)))
    (declare (type single-float vp)
	     (type (simple-array single-float (*)) matrix result))
    (setf (aref result 0) (aref matrix 0) ; r00
	  (aref result 1) (aref matrix 1) ; r01
	  (aref result 2) (aref matrix 2) ; r02
	  (aref result 3) (- (the single-float (couch-lateral b))) ; r03

	  (aref result 4) (aref matrix 3) ; r10
	  (aref result 5) (aref matrix 4) ; r11
	  (aref result 6) (aref matrix 5) ; r12
	  (aref result 7) (- (the single-float (couch-height b))) ; r13

	  (aref result 8) (aref matrix 6) ; r20
	  (aref result 9) (aref matrix 7) ; r21
	  (aref result 10) (aref matrix 8) ; r22
	  (aref result 11) (- 0.0 (+ (the single-float
				       (couch-longitudinal b))
				     vp))) ; r23
    result))

;;;----------------------------------------------

(defmethod beam-transform ((b beam) (cv coronal-view)
			   &optional wedge)

  "BEAM-TRANSFORM b cv &optional wedge

Computes and returns the terms for the collimator to view space matrix
transformation for beam b and coronal view cv.  The transform is
computed from b's gantry angle, collimator angle, turntable angle, and
couch displacement values, and cv's position.  Twelve values are
returned in a 1-dimensional simple array -- the homogeneous matrix
entries:

  r00 r01 r02 r03
  r10 r11 r12 r13
  r20 r21 r22 r23

the bottom row is not returned, since it is always 0 0 0 1. If the
keyword wedge parameter is nil, the default, and b has a multileaf
collimator, then the transform returned is computed with a collimator
angle of 0.0, regardless of the actual collimator angle of b.  If
wedge is non-nil or b has another type of collimator, then b's actual
collimator angle is used when computing the transformation."

  (let* ((gan-ang (* (the single-float (gantry-angle b))
		     *pi-over-180*))
         (col-ang (if (and (not wedge)
			   (typep (collimator b) 'multileaf-coll))
		      0.0
		    (* (the single-float (collimator-angle b))
		       *pi-over-180*)))
         (trn-ang (* (the single-float (couch-angle b))
		     *pi-over-180*))
         (sin-g (sin gan-ang))
	 (cos-g (cos gan-ang))
	 (sin-c (sin col-ang))
	 (cos-c (cos col-ang))
	 (sin-t (sin trn-ang))
	 (cos-t (cos trn-ang))
	 (result (make-array 12 :element-type 'single-float)))
    (declare (type single-float
		   gan-ang col-ang trn-ang
		   sin-g cos-g sin-c cos-c sin-t cos-t)
	     (type (simple-array single-float (12)) result))
    (setf (aref result 0) (+ (* cos-t cos-g cos-c)
			     (* sin-t sin-c)) ; r00
	  (aref result 1) (- (* sin-t cos-c)
			     (* cos-t cos-g sin-c)) ; r01
	  (aref result 2) (* cos-t sin-g) ; r02
	  (aref result 3) (- (the single-float (couch-lateral b))) ; r03

	  (aref result 4) (- (* cos-t sin-c)
			     (* sin-t cos-g cos-c)) ; r10
	  (aref result 5) (+ (* sin-t cos-g sin-c)
			     (* cos-t cos-c)) ; r11
	  (aref result 6) (- (* sin-t sin-g)) ; r12
	  (aref result 7) (the single-float (couch-longitudinal b)) ; r13

	  (aref result 8) (- (* sin-g cos-c)) ; r20
	  (aref result 9) (* sin-g sin-c) ; r21
	  (aref result 10) cos-g	; r22
	  (aref result 11) (- (+ (the single-float
				   (couch-height b))
				 (the single-float
				   (view-position cv))))) ; r23
    result))

;;;----------------------------------------------

(defmethod beam-transform ((b beam) (sv sagittal-view)
			   &optional wedge)

  "BEAM-TRANSFORM b sv &optional wedge

Computes and returns the terms for the collimator to view space matrix
transformation for beam b and sagittal view sv.  The transform is
computed from b's gantry angle, collimator angle, turntable angle, and
couch displacement values, and sv's position.  Twelve values are
returned in a 1-dimensional simple array -- the homogeneous matrix
entries:

  r00 r01 r02 r03
  r10 r11 r12 r13
  r20 r21 r22 r23

the bottom row is not returned, since it is always 0 0 0 1.  If the
keyword wedge parameter is nil, the default, and b has a multileaf
collimator, then the transform returned is computed with a collimator
angle of 0.0, regardless of the actual collimator angle of b.  If
wedge is non-nil or b has another type of collimator, then b's actual
collimator angle is used when computing the transformation."


  (let* ((gan-ang (* (the single-float (gantry-angle b))
		     *pi-over-180*))
         (col-ang (if (and (not wedge)
			   (typep (collimator b) 'multileaf-coll))
		      0.0
		    (* (the single-float (collimator-angle b))
		       *pi-over-180*)))
         (trn-ang (* (the single-float (couch-angle b))
		     *pi-over-180*))
         (sin-g (sin gan-ang))
	 (cos-g (cos gan-ang))
	 (sin-c (sin col-ang))
	 (cos-c (cos col-ang))
	 (sin-t (sin trn-ang))
	 (cos-t (cos trn-ang))
	 (result (make-array 12 :element-type 'single-float)))
    (declare (type single-float
		   gan-ang col-ang trn-ang
		   sin-g cos-g sin-c cos-c sin-t cos-t)
	     (type (simple-array single-float (12)) result))
    (setf (aref result 0) (- (* sin-t cos-g cos-c)
			     (* cos-t sin-c)) ; r00
	  (aref result 1) (- (+ (* sin-t cos-g sin-c)
				(* cos-t cos-c))) ; r01
	  (aref result 2) (* sin-t sin-g) ; r02
	  (aref result 3) (- (the single-float
			       (couch-longitudinal b))) ; r03

	  (aref result 4) (- (* sin-g cos-c)) ; r10
	  (aref result 5) (* sin-g sin-c) ; r11
	  (aref result 6) cos-g		; r12
	  (aref result 7) (- (the single-float (couch-height b))) ; r13

	  (aref result 8) (- (+ (* cos-t cos-g cos-c)
				(* sin-t sin-c))) ; r20
	  (aref result 9) (- (* cos-t cos-g sin-c)
			     (* sin-t cos-c)) ; r21
	  (aref result 10) (- (* cos-t sin-g)) ; r22
	  (aref result 11) (+ (the single-float
				(couch-lateral b))
			      (the single-float
				(view-position sv)))) ; r23
    result))

;;;----------------------------------------------

(defmethod beam-transform ((b beam) (bev beams-eye-view)
			   &optional wedge)

  "BEAM-TRANSFORM b bev &optional wedge

Computes and returns the terms for the collimator to view space matrix
transformation for beam b and beam's eye view bev.  The implication is
that b is not the primary beam of bev, and b's portal is just to
appear in the plane of bev.  The transform is computed from b's gantry
and collimator angles, and gantry angle of the primary beam of the
bev.  Twelve values are returned in a 1-dimensional simple array --
the homogeneous matrix entries:

  t00 t01 t02 t03
  t10 t11 t12 t13
  t20 t21 t22 t23

the bottom row is not returned, since it is always 0 0 0 1.  If the
keyword wedge parameter is nil, the default, and b has a multileaf
collimator, then the transform returned is computed with a collimator
angle of 0.0, regardless of the actual collimator angle of b.  If
wedge is non-nil or b has another type of collimator, then b's actual
collimator angle is used when computing the transformation."

  ;; The matrix r below takes points from patient space to gantry
  ;; space of the beam's eye view; the matrix s takes points from
  ;; collimator space of the beam b to patient space (i.e. same as a
  ;; transverse view at z = 0.0).  So composing them (rs) yields the
  ;; terms of a matrix that takes points from b's collimator space to
  ;; bev's gantry space.

  (let* ((bev-tr (bev-transform bev))
         (r00 (aref bev-tr 0))
	 (r01 (aref bev-tr 1))
	 (r02 (aref bev-tr 2))
	 (r03 (aref bev-tr 3))
         (r10 (aref bev-tr 4))
	 (r11 (aref bev-tr 5))
	 (r12 (aref bev-tr 6))
	 (r13 (aref bev-tr 7))
         (r20 (aref bev-tr 8))
	 (r21 (aref bev-tr 9))
	 (r22 (aref bev-tr 10))
	 (r23 (aref bev-tr 11))
	 (bt (transverse-beam-transform b 0.0 wedge))
         (s00 (aref bt 0))
	 (s01 (aref bt 1))
	 (s02 (aref bt 2))
	 (s03 (aref bt 3))
         (s10 (aref bt 4))
	 (s11 (aref bt 5))
	 (s12 (aref bt 6))
	 (s13 (aref bt 7))
         (s20 (aref bt 8))
	 (s21 (aref bt 9))
	 (s22 (aref bt 10))
	 (s23 (aref bt 11))
	 (result (make-array 12 :element-type 'single-float)))
    (declare (type (simple-array single-float (12)) bev-tr bt result)
	     (type single-float
		   r00 r01 r02 r03 r10 r11 r12 r13 r20 r21 r22 r23 
		   s00 s01 s02 s03 s10 s11 s12 s13 s20 s21 s22 s23))
    (setf (aref result 0) (+ (* r00 s00) (* r01 s10) (* r02 s20)) ; t00
	  (aref result 1) (+ (* r00 s01) (* r01 s11) (* r02 s21)) ; t01
	  (aref result 2) (+ (* r00 s02) (* r01 s12) (* r02 s22)) ; t02
	  (aref result 3) (+ (* r00 s03) (* r01 s13)
			     (* r02 s23) r03) ; t03

	  (aref result 4)  (+ (* r10 s00) (* r11 s10) (* r12 s20)) ; t10
	  (aref result 5) (+ (* r10 s01) (* r11 s11) (* r12 s21)) ; t11
	  (aref result 6) (+ (* r10 s02) (* r11 s12) (* r12 s22)) ; t12
	  (aref result 7) (+ (* r10 s03) (* r11 s13)
			     (* r12 s23) r13) ; t13

	  (aref result 8) (+ (* r20 s00) (* r21 s10) (* r22 s20)) ; t20
	  (aref result 9) (+ (* r20 s01) (* r21 s11) (* r22 s21)) ; t21
	  (aref result 10) (+ (* r20 s02) (* r21 s12) (* r22 s22)) ; t22
	  (aref result 11) (+ (* r20 s03) (* r21 s13)
			      (* r22 s23) r23)) ; t23
    result))

;;;----------------------------------------------
;;; End.
