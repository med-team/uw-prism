;;;
;;; tools-panel
;;;
;;; implements the RTPT tools menu and action functions
;;;
;;; 13-Dec-1993 M. Phillips implemented
;;;  4-Feb-1994 I. Kalet include gpet function here and add Autoplan
;;;  to tools menu
;;; 11-Jun-1994 I. Kalet remove PTVT.  It is implemented in the Add
;;; Target function call.
;;; 13-Jun-1994 I. Kalet change collections symbol to one colon
;;; 21-Jun-1994 I. Kalet add neutron function to write CNTS plan data
;;; to a file suitable to transfer to CNTS for automated setup.
;;;  7-Jul-1994 J. Jacky finish gpet for new Prism dose filenames dose1,2,3
;;;  7-Jul-1994 J. Jacky change pathname for gpet defaults file
;;;                      write the many gpet output files in ./prismlocal
;;;
;;; 19-Jul-1994 J. Unger change references to ./prismlocal to references 
;;;   to *local-database* instead.  Also redo the neutron interface.
;;; 11-Aug-1994 J. Unger minor mods to run-subprocess.
;;; 07-Oct-1994 J. Unger provide current display to gpet command line.
;;; 18-Jun-1998 I. Kalet eliminate DRR from menu, as it will be soon
;;; implemented as a background image in beams-eye-views.
;;; 24-Dec-1998 I. Kalet specify ":wait nil" in call to run-subprocess
;;; 25-Feb-1999 I. Kalet add Mark's import-anatomy panel.
;;; 25-Oct-1999 I. Kalet take off selections no longer in use.
;;; 29-Jun-2000 I. Kalet reorganize as data driven from
;;; *external-tools* global variable
;;;

(in-package :prism)

;;;-------------------------------------

(defun add-tool (tool-name tool-function)

  "add-tool tool-name tool-function

adds the string tool-name to the special tools menu.  The function
named by symbol tool-function executes when tool-name is selected."

  (push (list tool-name tool-function) *special-tools*))

;;;-------------------------------------

(defun tools-panel (pat)

  "tools-panel pat

provides a menu for the user to select an externally-provided tool for
immediate execution.  Selection of a given button on the tools-panel
menu will call a function designed to obtain all the necessary
information and begin program execution.  Returns whatever the tool
function call returns."

  (let ((tool-number (sl:popup-menu (mapcar #'first *special-tools*))))
    (when tool-number
      (funcall (nth tool-number (mapcar #'second *special-tools*))
	       pat))))

;;;-------------------------------------
;;; End.
