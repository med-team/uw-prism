;;;
;;; charts
;;;
;;; The functions for generating a chart are defined here.
;;;
;;; 11-Feb-1994 J. Unger started.
;;; 06-Mar-1994 J. Unger put printer popup menu in make-chart-dialog-box.
;;; 18-May-1994 I. Kalet move globals to prism-globals and
;;; consolidate.  Also, comments are now lists of strings.
;;; 25-May-1994 J. Unger add Combined Doses page, Dose per Treatment
;;; By Field page, and Total Dose by Field page to chart.
;;;  6-Jun-1994 J. Jacky begin mods to scale to machine coord system
;;; 22-Jun-1994 J. Jacky complete chart scaling for wedge rotation, arcs
;;; 22-Jun-1994 J. Jacky improve style to minimize funcall and #'
;;; 23-Jun-1994 J. Jacky handle collimator, blocks; add bounding-box fcn
;;; move scale-angle out to therapy-machines, correct MU on setup
;;; page: rounded MU/frac, not total correct Tray Fac: only apply when
;;; blocks present, correct SSD, Iso Depth when isocenter outside patient
;;; 24-Jun-1994 J. Jacky Fiddle with column alignment to improve readability
;;; correct rounding: no decimal point in angles etc., correct
;;; reversal of TOTAL DOSE.../DOSE PER... entries.
;;; 15-Jul-1994 J. Unger points' z coords now print to two decimal
;;; places and add patient id to each chart page.
;;; 21-Jul-1994 J. Unger move bounding-box to polygons pkg.
;;; 11-Aug-1994 J. Unger mods to run-subprocess command to print chart.
;;; 26-Aug-1994 J. Unger fix bug in run-subprocess call.
;;; 30-Aug-1994 J. Unger remove code to sort points - list should always be
;;; in correct order now.
;;; 31-Aug-194  J. Unger add make-neutron-chart function.
;;; 16-Sep-1994 J. Unger round mu's to nearest mu on neutron chart.  Also
;;; modify neutron chart code to produce a chart for every field sent,
;;; highlight the changed settings, rework interactive-make-chart calls
;;; to make each type of chart.
;;; 22-Sep-1994 J. Jacky Add write-leaf-settings and fill in
;;; make-leaf-chart and make-neutron-chart.
;;; 23-Sep-1994 J. Jacky make-neutron-chart uses brief-chart-header not main
;;;  4-Oct-1994 J. Jacky in make-neutron-chart, round mu before compare
;;; 26-Jan-1995 I. Kalet pass plan as parameter to make-leaf-chart and
;;; interactive-make-leaf chart, etc. and get plan as third element of
;;; beam-pairs.  Use machine of original instead of funny find-if code
;;; to get leaf-pair-map in make-neutron-chart.
;;; 27-Apr-1995 I. Kalet combine ext. beam dosimetry and setup pages.
;;; 14-Jun-1995 I. Kalet adjust page length for ext. beam pages.
;;;  3-Sep-1995 I. Kalet convert possibly integer arguments to
;;;  poly:NEARLY-EQUAL function to SINGLE-FLOAT.
;;; 26-Sep-1995 I. Kalet add range check for number of copies in
;;; make-chart-dialog-box and add "File only" option to printer list.
;;; 11-Oct-1996 I. Kalet use = instead of poly:NEARLY-EQUAL in cases
;;; where arguments are guaranteed to be integers.
;;; 29-Jan-1997 I. Kalet names of tpr and output-factor fields in dose
;;; result have changed - use new names.
;;;  1-May-1997 I. Kalet add menu for specifying only part of a chart
;;;  to be printed.
;;;  6-Jun-1997 I. Kalet machine returns the object, not the name
;;;  3-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx .
;;; 16-Sep-1997 I. Kalet eliminate remaining explicit calls to
;;; get-therapy-machine.
;;; 11-Nov-1997 I. Kalet fix egregious lisp gaff in print list menu
;;; code - remove returns a result, does not modify its argument.
;;; Also print Extend for TPR at ISO when it is negative, blank for
;;; various items in beam setup when axis misses, and don't print dose
;;; info sections when central axis of any beam misses.
;;; 30-Apr-1998 I. Kalet move irreg chart stuff here from irreg-panels
;;; and begin migration of all chart code to Postscript.
;;; 12-May-1998 I. Kalet change name of list of printers to *printers*
;;; 19-May-1998 I. Kalet move prism-logo to postscript module.
;;; 11-Jun-1998 I. Kalet make sure the Prism name is printed with the
;;; version string on plain charts, add explicit go to next page on
;;; main chart when not printing combined doses section.
;;; 15-Dec-1998 I. Kalet add list of organs and densities that were
;;; used in the dose computation, on first page of regular chart.
;;; 24-Dec-1998 I. Kalet remove wait t from run-subprocess, now default
;;; 25-Feb-1999 I. Kalet fix error in MU/degree for arcs - forgot to
;;; divide by the number of treatments.  Also put the printer list on
;;; the dialog box instead of yet another popup menu.
;;; 14-Sep-1999 I. Kalet in call to compute-mlc always use collimator
;;; angle, there is no difference between CNTS and Elekta here.
;;;  5-Mar-2000 I. Kalet begin adding brachytherapy support.
;;; 29-Mar-2000 I. Kalet ongoing work on brachy, and PostScript conversion.
;;; 25-Apr-2000 I. Kalet add activity units for seeds.
;;;  8-May-2000 I. Kalet add more printout for seeds, finish line
;;; source specs printout.
;;; 19-Jul-2000 I. Kalet cosmetic fine tuning of printing beam names.
;;; Also fix page count computation.
;;;  8-Aug-2000 I. Kalet add multi-page capability for dose per beam
;;; as well as total dose.
;;; 26-Nov-2000 I. Kalet cosmetics for buttons in dialog box.
;;; 11-Mar-2001 I. Kalet print point Z coords to 3 decimal places.
;;; 26-Nov-2001 J. Jacky beam-specs: separate photon/neutron, electron pages
;;;                       recompute page numbers etc. to match
;;;  3-Dec-2001 J. Jacky beam-specs: details, electron vs. photon-neutron page
;;;  7-Dec-2001 J. Jacky beam-specs: e tweaks,SSD and ROF are only dose-results
;;;  6-Jan-2002 I. Kalet print beam names in three rows of 10 chars
;;; 28-Jan-2002 I. Kalet/J. Jacky add dicom chart type to menu in
;;; chart panel, merge in the rest of the dicom chart functions.
;;; 12-Sep-2003 BobGian regularize function name: ADD-BEAM -> ADD-BEAM-FCN to
;;;   assist readability of Dose-Monitoring-Point code [comments only, here].
;;; 03-Oct-2003 BobGian change defstruct name and slot names in SEG-REC-... to
;;;   SEGMENT-DESCRIPTOR-... to make DMP code more readable.
;;;   Ditto with a few local variables.
;;;   STATIC, DYNAMIC, SEGMENT (Prism pkg) -> Keyword pkg.
;;;   Format indentation to get text within 80-column width.
;;; 04-Dec-2003 BobGian: SEGMENT-DESCRIPTOR-... -> SEGDATA-... .
;;; 23-Feb-2003 BobGian: update naming conventions which distinguish between
;;;   Prism vs Dicom beams and Prism vs Dicom DMPs.  This includes:
;;;   SEGDATA-... -> PR-BEAM-...
;;; 08-Mar-2003 BobGian: Edited DEFSTRUCTs for PR-BEAM in "imrt-segments".
;;;   Replaced PR-BEAM-TOTSEGS and PR-BEAM-SEGNUM slots by equivalent code.
;;; 17-May-2004 BobGian complete process of extending all instances of Original
;;;    and Current Prism beam instances to include Copied beam instance too,
;;;    to provide copy for comparison with Current beam without mutating
;;;    Original beam instance.
;;; 21-Jun-2004 I. Kalet take out IRREG support - IRREG discontinued
;;; 26-Sep-2004 BobGian rename slot PR-BEAM-CUM-MU -> PR-BEAM-CUM-MU-INC.
;;; 05-Oct-2004 BobGian fixed couple of lines to fit within 80 cols.
;;; 17-Feb-2005 A. Simms replaced an allegro getenv call with a misc.cl wrapper
;;;    getenv call.
;;;

(in-package :prism)

;;;----------------------------------------------------

(defparameter *pts-full-page* 48
  "Number of combined point dose lines that will fit on a full page")

;;;----------------------------------------------------

(defun chart-header (chart cur-pat pln total-pgs)

  "chart-header chart cur-pat pln total-pgs

writes the big header at the top of the first chart page, the same for
all charts."

  (ps:initialize chart 0.5 0.5 7.5 10.0)
  (ps:prism-logo chart 0.6 10.4 *prism-version-string*)
  (ps:set-font chart "Helvetica" 14)
  (ps:set-position chart 0.1 1.5)
  (ps:indent chart 0.1)
  (ps:put-text chart (first *hardcopy-header*))
  (ps:put-text chart (second *hardcopy-header*))
  (ps:set-font chart "Courier" 12)
  (ps:set-position chart 4.25 0.2)
  (ps:indent chart 4.25)
  (ps:put-text chart "          APPROVED BY:")
  (ps:put-text chart "")
  (ps:put-text chart "ATTENDING: ________ DATE: ______")
  (ps:put-text chart "")
  (ps:put-text chart "RESIDENT:  ________ DATE: ______")
  (ps:put-text chart "")
  (ps:put-text chart "PHYSICIST: ________ DATE: ______")
  (ps:put-text chart "")
  (ps:put-text chart "THERAPIST: ________ DATE: ______")
  (ps:put-text chart "")
  (ps:put-text chart "BILLED: ________________________")
  (ps:indent chart 0.1)
  (brief-header chart cur-pat pln 1 total-pgs 2.0)
  (dolist (cmt (comments cur-pat))
    (ps:put-text chart cmt))
  (ps:put-text chart "")
  (when pln
    (ps:put-text chart (format nil "DS: ~A" (plan-by pln)))
    (ps:put-text chart "")
    (dolist (cmt (comments pln))
      (ps:put-text chart cmt))
    (ps:put-text chart "")
    (ps:put-text chart "Organs used in dose computation:")
    (ps:put-text chart "--------------------------------")
    (ps:put-text chart "")
    (dolist (org (coll:elements (anatomy cur-pat)))
      (when (density org)
	(ps:put-text chart (format nil "~15A : ~4,2F"
				   (name org) (density org)))))
    (ps:put-text chart "")))

;;;----------------------------------------------------

(defun brief-header (chart cur-pat pln pgnum total-pgs
		     &optional (top-margin 0.0))

  "brief-header chart cur-pat pln pgnum total-pgs &optional (top-margin 0.0)

writes the brief header that appears at the top (or down an amount of
top-margin) on every chart page."

  (ps:draw-rectangle chart 0.5 0.5 7.5 10.0)
  (ps:set-position chart 6.0 (+ top-margin 0.2))
  (ps:put-text chart (format nil "PAGE:  ~A of ~A" pgnum total-pgs))
  (ps:set-position chart 0.1 (+ top-margin 0.5))
  (ps:indent chart 0.1)
  (ps:put-text chart (format nil "PATIENT: ~A" (name cur-pat)))
  (ps:put-text chart (format nil "PAT ID:  ~A" (patient-id cur-pat)))
  (when pln
    (ps:put-text chart (format nil "PLAN:    ~A" (name pln))))
  (ps:set-position chart 4.25 (+ top-margin 0.5))
  (ps:indent chart 4.25)
  (ps:put-text chart (format nil "CASE DATE: ~A" (date-entered cur-pat)))
  (ps:put-text chart (format nil "HOSP ID:   ~A" (hospital-id cur-pat)))
  (when pln
    (ps:put-text chart (format nil "PLAN DATE: ~A" (time-stamp pln))))
  (ps:indent chart 0.1)
  (ps:put-text chart ""))

;;;----------------------------------------------------

(defun print-points (chart pts doses start end)

  "prints point data and doses from point numbers start through end
inclusive from pts with column labels"

  (when (< start end)
    (ps:put-text chart
		 "   Site             Total Dose (cGy)   X        Y        Z")
    (ps:put-text chart "")
    (do* ((i start (1+ i))
	  (pt (nth i pts) (nth i pts))
	  (dose (nth i doses) (nth i doses))
	  (name (subseq (name pt) 0 (min 16 (length (name pt))))
		(subseq (name pt) 0 (min 16 (length (name pt))))))
	 ((= i end) nil)
      (ps:put-text chart
		   (format nil "~2@a. ~16a  ~5@a     ~8,1F ~8,1F ~8,3F"
			   (id pt) name (round dose)
			   (x pt) (y pt) (z pt))))))

;;;----------------------------------------------------

(defun combined-doses (chart cur-pat pln lines page-no total-pgs)

  "combined-doses chart cur-pat pln lines page-no total-pgs

prints the combined total doses from all sources, external beam or
brachytherapy in the specified plan PLN for patient CUR-PAT."

  (ps:put-text
    chart
    "----------------------- COMBINED POINT DOSES ----------------------")
  (ps:put-text chart "")
  (if (valid-points (sum-dose pln))
      (let* ((pts (coll:elements (points cur-pat)))
	     (doses (points (sum-dose pln)))
	     (end (length pts)))
	(if (<= end (- lines 2))
	    (progn
	      (print-points chart pts doses 0 end)
	      (ps:finish-page chart (< page-no total-pgs))
	      (incf page-no))
	    (let ((npts1 (max 0 (- lines 2))))
	      (print-points chart pts doses 0 npts1)
	      (ps:finish-page chart (< page-no total-pgs))
	      (incf page-no)
	      (do* ((pt-list (nthcdr npts1 pts)
			     (nthcdr *pts-full-page* pt-list))
		    (dose-list (nthcdr npts1 doses)
			       (nthcdr *pts-full-page* dose-list)))
		   ((null pt-list) nil)
		(brief-header chart cur-pat pln page-no total-pgs)
		(print-points chart pt-list dose-list
			      0 (min *pts-full-page* (length pt-list)))
		(ps:finish-page chart (< page-no total-pgs))
		(incf page-no)))))
      (progn
	(ps:put-text chart "Combined doses not available")
	(ps:finish-page chart (< page-no total-pgs))
	(incf page-no)))
  page-no)

;;;----------------------------------------------------

(defun beam-specs (chart cur-pat pln page-no total-pgs modality)

  (let ((bms
	  ;; I'm sure there's a more elegant way to choose remove-if/-if-not
	  ;; and also to shorten remove- call by using :key.
	  (if (eq modality 'electron)
	      ;; call argument modality because particle is a slot in machine
	      (remove-if-not (lambda (b) (eq (particle (machine b)) 'electron))
			     (coll:elements (beams pln)))
	      (remove-if (lambda (b) (eq (particle (machine b)) 'electron))
			 (coll:elements (beams pln))))))
    (do ((bm-list bms (nthcdr 4 bm-list)))
	((null bm-list) page-no)
      (brief-header chart cur-pat pln page-no total-pgs)
      (ps:put-text
	chart
	(format
	  nil
	  "------------------- ~A BEAM SETUP AND DOSIMETRY -----------------"
	  (if (eq modality 'electron) "ELECTRON" "EXTERNAL")))
      (ps:set-position chart 0.1 1.5)

      ;; the name is up to 30 chars in 10 character pieces
      (ps:put-text chart "Name      :")
      (ps:put-text chart "")
      (ps:put-text chart "")
      (ps:put-text chart "Machine   :")
      (ps:put-text chart "")
      (ps:put-text chart "Particle  :")
      (ps:put-text chart "Energy    :")
      (ps:put-text chart "MU/Frac   :")
      (ps:put-text chart "Fractions :")
      (ps:put-text chart "SSD       :")
      (ps:put-text chart "")
      (ps:put-text chart (if (eq modality 'electron) "" "Wedge Sel :"))
      (ps:put-text chart (if (eq modality 'electron) "" "Wedge Rot :"))
      (ps:put-text chart "")
      (ps:put-text chart (if (eq modality 'electron)
			     "Applicator:" "Collimator:"))
      (ps:put-text chart (if (eq modality 'electron)
			     "" "(cm)      :"))     ;electron accessory?
      (ps:put-text chart (if (eq modality 'electron)
			     "" "          :"))     ;electron fitment?
      (ps:put-text chart "          :")
      (ps:put-text chart "")
      (ps:put-text chart "Gantry    :")
      (ps:put-text chart "Arc Size  :")
      (ps:put-text chart "Coll Ang  :")
      (ps:put-text chart "Tabl Ang  :")
      (ps:put-text chart "")
      (ps:put-text chart (if (eq modality 'electron) "" "Blocks    :"))
      (ps:put-text chart "")
      (ps:put-text chart "Tabl Hgt  :")
      (ps:put-text chart "Tabl Lat  :")
      (ps:put-text chart "Tabl Lng  :")
      (ps:put-text chart "")
      ;; If any of the 4 are arcs, print this stuff
      (when (find-if-not #'(lambda (b) (zerop (arc-size b)))
			 bm-list)
	(ps:put-text chart "Start Ang :")
	(ps:put-text chart "Stop Ang  :")
	(ps:put-text chart "Arc Size  :")
	(ps:put-text chart "MU/deg    :")
	(ps:put-text chart ""))
      (ps:put-text chart "Iso Depth :")
      (ps:put-text chart "")
      (ps:put-text chart (if (eq modality 'electron) "" "Coll X    :"))
      (ps:put-text chart (if (eq modality 'electron) "" "Coll Y    :"))
      (ps:put-text chart (if (eq modality 'electron) "" "Equiv Sqr :"))
      (ps:put-text chart "")
      (ps:put-text chart "ROF       :")
      (ps:put-text chart (if (eq modality 'electron) "" "TPR @ Iso :"))
      (ps:put-text chart (if (eq modality 'electron) "" "Tray Fac  :"))
      (ps:put-text chart "Atten Fac :")

      ;; now do each of the beams on this page
      (dotimes (i (min 4 (length bm-list)))
	(ps:set-position chart (+ 1.3 (* i 1.6)) 1.5)
	(ps:indent chart (+ 1.3 (* i 1.6)))
	(let* ((bm (nth i bm-list))
	       (mach (machine bm))
	       (wdg (wedge bm))
	       (name-str (listify (name bm) 10)))
	  (dolist (str name-str)
	    (ps:put-text chart (format nil "~10A" str)))
	  (dotimes (i (- 3 (length name-str)))
	    (ps:put-text chart ""))
	  (ps:put-text chart (format nil "~14A" (machine-name bm)))
	  (ps:put-text chart "")
	  (ps:put-text chart (format nil "~14A" (particle mach)))
	  (ps:put-text chart (apply #'format nil "~6@A ~3A"
				    (if (eq (collimator-type mach)
					    'electron-coll)
					(list (energy (collimator bm))
					      "MeV")
					(list (energy mach) "MV"))))
	  (ps:put-text chart (format nil "~6@A MU/F"
				     (round (/ (monitor-units bm)
					       (n-treatments bm)))))
	  (ps:put-text chart (format nil "~6@A" (n-treatments bm)))
	  (ps:put-text chart (let ((d (ssd (result bm))))
			       (if (minusp d) "MISS"
				   (format nil "~6,1F cm" d))))
	  (ps:put-text chart "")
	  (ps:put-text chart (if (eq modality 'electron)
				 ""
				 (format nil "~16A"
					 (wedge-label (id wdg) mach))))
	  (if (or (zerop (id wdg)) (not (wedge-rot-print-flag mach)))
	      (ps:put-text chart "")
	      (let ((ang (scale-angle (rotation wdg)
				      (wedge-rot-scale mach)
				      (wedge-rot-offset mach))))
		(ps:put-text chart (format nil "~6@A ~A"
					   (round (first ang)) (second ang)))))
	  (ps:put-text chart "")

	  ;; collimator setup here
	  (let ((coll (collimator bm))
		(coll-info (collimator-info mach)))
	    ;; Put collimator y on first line because Varian calls it upper
	    (ps:put-text
	      chart
	      (apply #'format nil "~6,1F ~A"
		     (typecase coll
		       ((or symmetric-jaw-coll combination-coll)
			(list (y coll) (y-name coll-info)))
		       (variable-jaw-coll
			 (list (y-inf coll) (y-inf-name coll-info)))
		       (multileaf-coll
			 (let* ((box (poly:bounding-box
				       (vertices coll)))
				(ymin (second (first box)))
				(ymax (second (second box))))
			   (list (- ymax ymin) "height")))
		       (electron-coll
			 (list (cone-size coll) "cm")))))
	    ;; Put collimator x on second line because Varian calls it lower
	    (ps:put-text
	      chart
	      (if (eq modality 'electron)
		  ""                   ; possibly print accessory number here?
		  (apply #'format nil "~6,1F ~A"
			 (typecase coll
			   (symmetric-jaw-coll
			     (list (x coll) (x-name coll-info)))
			   (combination-coll
			     (if (poly:nearly-equal (x-inf coll)
						    (x-sup coll))
				 (list (+ (x-inf coll) (x-sup coll))
				       (x-sym-name coll-info))
				 (list (x-inf coll)
				       (x-inf-name coll-info))))
			   (variable-jaw-coll
			     (list (y-sup coll) (y-sup-name coll-info)))
			   (multileaf-coll
			     (let* ((box (poly:bounding-box
					   (vertices coll)))
				    (ymin (second (first box)))
				    (ymax (second (second box))))
			       (list (/ (+ ymax ymin) 2) "h offset")))
			   (electron-coll
			     (list (cone-size coll) ""))))))
	    ;; third line, we're done with simple collimators
	    (ps:put-text
	      chart
	      (if (eq modality 'electron)
		  ""                     ; possibly print fitment number here?
		  (apply #'format nil "~6,1F ~A"
			 (typecase coll
			   ((or symmetric-jaw-coll electron-coll)
			    (list "" ""))
			   (combination-coll
			     (if (poly:nearly-equal (x-inf coll)
						    (x-sup coll))
				 (list "" "")
				 (list (x-sup coll)
				       (x-sup-name coll-info))))
			   (variable-jaw-coll
			     (list (x-inf coll) (x-inf-name coll-info)))
			   (multileaf-coll
			     (let* ((box (poly:bounding-box
					   (vertices coll)))
				    (xmin (first (first box)))
				    (xmax (first (second box))))
			       (list (- xmax xmin) "width")))))))
	    ;; fourth line: variable jaw and mlc only
	    (ps:put-text
	      chart
	      (apply #'format nil "~6,1F ~A"
		     (typecase coll
		       ((or symmetric-jaw-coll combination-coll
			    electron-coll)
			(list "" ""))
		       (variable-jaw-coll
			 (list (x-sup coll) (x-sup-name coll-info)))
		       (multileaf-coll
			 (let* ((box (poly:bounding-box
				       (vertices coll)))
				(xmin (first (first box)))
				(xmax (first (second box))))
			   (list (/ (+ xmax xmin) 2)
				 "w offset")))))))
	  (ps:put-text chart "")
	  (let ((ang (scale-angle (gantry-angle bm)
				  (gantry-scale mach)
				  (gantry-offset mach))))
	    (ps:put-text chart (format nil "~6@A ~A"
				       (round (first ang)) (second ang))))
	  ;; arc size always positive, no scaling
	  (ps:put-text chart
		       (if (zerop (arc-size bm)) " fixed"
			   (format nil "~6@A deg" (round (arc-size bm)))))
	  (let ((ang (scale-angle (collimator-angle bm)
				  (collimator-scale mach)
				  (collimator-offset mach)
				  (collimator-negative-flag mach)
				  (collimator-lower-limit mach)
				  (collimator-upper-limit mach))))
	    (ps:put-text chart (format nil "~6@A ~A"
				       (round (first ang)) (second ang))))
	  (let ((ang (scale-angle (couch-angle bm)
				  (turntable-scale mach)
				  (turntable-offset mach)
				  (turntable-negative-flag mach)
				  (turntable-lower-limit mach)
				  (turntable-upper-limit mach))))
	    (ps:put-text chart (format nil "~6@A ~A"
				       (round (first ang)) (second ang))))
	  (ps:put-text chart "")
	  (ps:put-text chart (cond ((eq modality 'electron)
				    "")
				   ((typep (collimator bm) 'multileaf-coll)
				    "  leaf")
				   ((null (coll:elements (blocks bm)))
				    "  none")
				   (t "blocks")))
	  (ps:put-text chart "")
	  (ps:put-text chart (format nil "~6,1F cm" (couch-height bm)))
	  (ps:put-text chart (format nil "~6,1F cm" (couch-lateral bm)))
	  (ps:put-text chart (format nil "~6,1F cm" (couch-longitudinal bm)))
	  (ps:put-text chart "")

	  (when (find-if-not #'(lambda (b) (zerop (arc-size b)))
			     bm-list)
	    ;; Start angle is Prism gantry-angle, scaled
	    ;; Stop angle is Prism (gantry-angle + arc-size), scaled
	    ;; Therefore stop angle may be less than Start angle in
	    ;; machine system
	    (let ((ang (scale-angle (gantry-angle bm)
				    (gantry-scale mach)
				    (gantry-offset mach))))
	      (ps:put-text chart (format nil "~6@A ~A"
					 (round (first ang)) (second ang))))
	    (let ((ang (scale-angle (+ (gantry-angle bm)
				       (arc-size bm))
				    (gantry-scale mach)
				    (gantry-offset mach))))
	      (ps:put-text chart (format nil "~6@A ~A"
					 (round (first ang)) (second ang))))
	    (ps:put-text chart (format nil "~6@A deg" (round (arc-size bm))))
	    (ps:put-text chart (if (zerop (arc-size bm)) ""
				   (format nil "~6,2F MU"
					   (/ (monitor-units bm)
					      (* (arc-size bm)
						 (n-treatments bm))))))
	    (ps:put-text chart ""))

	  (let* ((dd (ssd (result bm)))
		 (depth (- (cal-distance mach) dd)))
	    (ps:put-text chart (cond ((minusp dd) "")
				     ((minusp depth) "EXTEND")
				     (t (format nil "~6,1F cm" depth)))))
	  (ps:put-text chart "")

	  ;; collimator x, y
	  (let* ((coll (collimator bm))
		 (x (typecase coll
		      (symmetric-jaw-coll (x coll))
		      ((or variable-jaw-coll combination-coll)
		       (+ (x-inf coll) (x-sup coll)))
		      (multileaf-coll
			(let ((box (poly:bounding-box (vertices coll))))
			  (- (first (second box)) (first (first box)))))
		      (electron-coll (cone-size coll))))   ; we don't use this
		 (y (typecase coll
		      ((or symmetric-jaw-coll combination-coll) (y coll))
		      (variable-jaw-coll (+ (y-inf coll) (y-sup coll)))
		      (multileaf-coll
			(let ((box (poly:bounding-box (vertices coll))))
			  (- (second (second box)) (second (first box)))))
		      (electron-coll (cone-size coll)))))  ; we don't use this
	    (ps:put-text chart (if (eq modality 'electron)`
				   ""
				   (format nil "~6,1F cm" x)))
	    (ps:put-text chart (if (eq modality 'electron)
				   ""
				   (format nil "~6,1F cm" y)))

	    ;; equiv sqr, ROF, TPR at iso
	    (if (not (valid-points (result bm)))
		;; now handle electron beams in each case below
		(progn (ps:put-text chart "")
		       (ps:put-text chart "")
		       (ps:put-text chart "")
		       (ps:put-text chart "")
		       (ps:put-text chart ""))
		(progn
		  (ps:put-text chart (if (or (minusp (ssd (result bm)))
					     (eq modality 'electron))
					 ""
					 (format nil "~6,1F cm"
						 (equiv-square (result bm)))))
		  (ps:put-text chart "")
		  (ps:put-text chart
			       (format nil "~6,3F" (output-comp (result bm))))
		  (let ((tpriso (tpr-at-iso (result bm))))
		    (ps:put-text
		      chart (cond ((eq modality 'electron) "")
				  ((minusp (ssd (result bm))) "")
				  ((minusp tpriso) "EXTEND")
				  (t (format nil "~6,3F" tpriso)))))
		  (ps:put-text
		    chart (if (null (coll:elements (blocks bm))) ""
			      (format nil "~6,3F" (tray-factor mach))))))
	    (ps:put-text chart (format nil "~6,3F" (atten-factor bm))))))
      (ps:finish-page chart (< page-no total-pgs))
      (incf page-no))
    page-no))

;;;----------------------------------------------------

(defun dose-per-beam (chart cur-pat pln page-no total-pgs fractional)

  (do* ((pt-list (coll:elements (points cur-pat))
		 (nthcdr *pts-full-page* pt-list))
	(sum-doses (if (valid-points (sum-dose pln))
		       (points (sum-dose pln)))
		   (nthcdr *pts-full-page* sum-doses))
	(start-pt 0 (+ start-pt *pts-full-page*))
	(npts (min *pts-full-page* (length pt-list))
	      (min *pts-full-page* (length pt-list))))
       ((null pt-list) nil)
    (do ((bm-list (coll:elements (beams pln)) (nthcdr 4 bm-list)))
	((null bm-list))
      (brief-header chart cur-pat pln page-no total-pgs)
      (ps:put-text
	chart
	(format nil "~A~A~A"
		"-------------------"
		(if fractional " DOSE PER TREATMENT BY FIELD (cGy) "
		    "---- TOTAL DOSE BY FIELD (cGy) ----")
		"-------------------"))
      ;; write column 1, the point names
      (ps:set-position chart 0.1 1.5)
      (ps:put-text chart "Site:")
      (ps:put-text chart "")
      (ps:put-text chart "")
      (do* ((i 0 (1+ i))
	    (pt (nth i pt-list) (nth i pt-list)))
	   ((= i npts) nil)
	(ps:put-text chart (format nil "~2@A. ~16A" (id pt)
				   (if (< (length (name pt)) 16) (name pt)
				       (subseq (name pt) 0 16)))))
      ;; if fractional skip the next column
      (unless fractional
	(ps:set-position chart 2.0 1.5)
	(ps:indent chart 2.0)
	(ps:put-text chart "Total")
	(ps:put-text chart "")
	(ps:put-text chart "")
	(if (valid-points (sum-dose pln))
	    (do* ((i 0 (1+ i))
		  (dose (nth i sum-doses) (nth i sum-doses)))
		 ((= i npts) nil)
	      (ps:put-text chart (format nil "~8,1F" (round dose))))))
      ;; do each beam up to 4 at a time
      (dotimes (i (min 4 (length bm-list)))
	(ps:set-position chart (+ 2.9 (* i 1.18)) 1.5)
	(ps:indent chart (+ 2.9 (* i 1.18)))
	(let* ((bm (nth i bm-list))
	       (name-str (listify (name bm) 10)))
	  (dolist (str name-str)
	    (ps:put-text chart (format nil "~10A" str)))
	  (dotimes (i (- 3 (length name-str)))
	    (ps:put-text chart ""))
	  (if (valid-points (result bm))
	      (do* ((i start-pt (1+ i))
		    (dose (nth i (points (result bm)))
			  (nth i (points (result bm)))))
		   ((= i (+ start-pt npts)) nil)
		(ps:put-text
		  chart (format nil "~8,1F"
				(if fractional
				    (/ (* dose (monitor-units bm))
				       (n-treatments bm))
				    (* dose (monitor-units bm)))))))))
      (ps:finish-page chart (< page-no total-pgs))
      (incf page-no)))
  page-no)

;;;----------------------------------------------------

(defun line-specs (chart line-sources)

  (ps:put-text chart "")
  (ps:put-text
    chart
    "--------------------------- LINEAR SOURCES ---------------------------")
  (ps:put-text chart "")
  (ps:put-text
    chart
    "     Source type      Appl. Activ Filt.   Meas.   Total  Active  Gamma")
  (ps:put-text
    chart
    "                      time  -ity  (mm)   len(cm) len(cm) len(cm)")
  (ps:put-text chart "")
  (do* ((srclist line-sources (rest srclist))
	(i 1 (1+ i)))
       ((null srclist))
    (let* ((src (first srclist))
	   (srcdata (find (source-type src) *brachy-tables*
			  :key #'src-type :test #'string-equal)))
      (ps:put-text chart
		   (format nil
			   "~2@A. ~16A~6,1F~6,1F~6,1F~8,2F~8,2F~8,2F~8,2F"
			   i (source-type src) (treat-time src)
			   (activity src) (* 10.0 (wall-thickness srcdata))
			   0.0                 ;; replace with computed length
			   (physlen srcdata) (actlen srcdata)
			   (* (dose-rate-const srcdata)
			      (anisotropy-fn srcdata)))))))

;;;----------------------------------------------------

(defun seed-specs (chart seeds)

  (ps:put-text chart "")
  (ps:put-text chart
	       "------------------------ SEEDS ------------------------")
  (ps:put-text chart "")
  (ps:put-text
    chart
    "                                Appl. Activity Dose Rate Aniso.")
  (ps:put-text
    chart
    "                                time           constant  factor")
  (ps:put-text chart "")
  (do* ((srclist seeds (rest srclist))
	(i 1 (1+ i))
	(start-count 1)
	(prev-type (source-type (first srclist)) (source-type src))
	(prev-treattime (treat-time (first srclist)) (treat-time src))
	(prev-act (activity (first srclist)) (activity src))
	(prev-table (source-data (source-type (first srclist)))
		    (source-data (source-type src)))
	(prev-drate (dose-rate-const prev-table) (dose-rate-const prev-table))
	(prev-units (activity-units prev-table) (activity-units prev-table))
	(prev-proto (protocol prev-table) (protocol prev-table))
	(prev-aniso (anisotropy-fn prev-table) (anisotropy-fn prev-table))
	(src (first srclist) (first srclist)))
       ((null srclist)
	(ps:put-text
	  chart
	  (format nil
		  "~3A thru ~3A  ~16A~6,1F~6,1F ~5A ~5,2F   ~5,2F"
		  start-count (1- i)
		  (concatenate 'string prev-type " " prev-proto)
		  prev-treattime prev-act
		  prev-units prev-drate prev-aniso)))
    (when (or (not (string-equal prev-type (source-type src)))
	      (/= prev-treattime (treat-time src))
	      (/= prev-act (activity src)))
      (ps:put-text
	chart
	(format nil
		"~3A thru ~3A  ~16A~6,1F~6,1F ~5A ~5,2F   ~5,2F"
		start-count (1- i)
		(concatenate 'string prev-type " " prev-proto)
		prev-treattime prev-act
		prev-units prev-drate prev-aniso))
      (setq start-count i))))

;;;----------------------------------------------------

(defun dose-per-source (chart cur-pat pln page-no total-pgs)

  (do ((srclist (coll:elements (line-sources pln)) (nthcdr 4 srclist)))
      ((null srclist))
    (brief-header chart cur-pat pln page-no total-pgs)
    (ps:put-text
      chart
      "--------------------- TOTAL DOSE BY SOURCE ---------------------")
    (ps:set-position chart 0.1 1.5)
    (ps:put-text chart "Site:")
    (ps:put-text chart "")
    (dolist (pt (coll:elements (points cur-pat)))
      (ps:put-text chart (format nil "~2@A. ~16A" (id pt)
				 (if (< (length (name pt)) 16) (name pt)
				     (subseq (name pt) 0 15)))))
    (dotimes (i (min 4 (length srclist)))
      (ps:set-position chart (+ 3.0 (* i 1.18)) 1.5)
      (ps:indent chart (+ 3.0 (* i 1.2)))
      (let ((src (nth i srclist)))
	(ps:put-text chart (format nil "Source ~2A" i))
	(ps:put-text chart "")
	(if (valid-points (result src))
	    (dolist (dose (points (result src)))
	      (ps:put-text chart (format nil "~8,1F"
					 (* dose (activity src)
					    (treat-time src))))))))
    (ps:finish-page chart (< page-no total-pgs))
    (incf page-no))
  page-no)

;;;----------------------------------------------------

(defun main-chart (parts cur-pat pln)

  "main-chart parts cur-pat pln

Generates a chart for the specified plan, and writes it to a file.
The doses to points are also computed before the chart is printed,
since they are written on the chart.  The parts numbered in the list
parts are included, 0 for the combined doses, 1 for the beam
settings, 2 for the doses per fraction, 3 for the total doses, 4 for
brachy source specs."

  (unless (valid-points (sum-dose pln))
    (when (coll:elements (points cur-pat))
      (compute-dose-points pln cur-pat)))
  ;; figure out how many pages the chart is
  (let* ((n-comment-lines (+ (length (comments cur-pat))
			     1                      ;; blank always printed
			     (if pln (+ (length (comments pln))
					(length (remove-if
						  #'(lambda (x)
						      (null (density x)))
						  (coll:elements
						    (anatomy cur-pat))))
					7)          ;; blanks and labels
				 0)))
	 (n-pts-page1 (- 36 n-comment-lines))
	 (n-pts (length (coll:elements (points cur-pat))))
	 ;; n-bm-pages is total of all beams, used for npt-pages
	 (n-bm-pages (if pln
			 (ceiling (length (coll:elements (beams pln))) 4)
			 0))
	 ;; n-e-pages is electron beams only, one set of pages
	 (n-e-pages (if pln
			(ceiling (length
				   (remove-if-not
				     (lambda (b) (eq (particle (machine b))
						     'electron))
				     (coll:elements (beams pln))))
				 4)
			0))
	 ;; n-pn-pages is photon/neutron beams only, another set of pages
	 (n-pn-pages (- n-bm-pages n-e-pages))
	 (npt-pages (* n-bm-pages (ceiling n-pts *pts-full-page*)))
	 (total-pages (+ 1 (if (and (member :combined parts)
				    (> n-pts n-pts-page1))
			       (ceiling (- n-pts n-pts-page1)
					*pts-full-page*)
			       0)
			 (if (member :beam-specs parts)
			     (+ n-e-pages n-pn-pages) 0)
			 (if (member :beam-frac-dose parts) npt-pages 0)
			 (if (member :beam-total-dose parts) npt-pages 0)
			 (if (or (member :line-specs parts)
				 (member :seed-specs parts)) 1 0)
			 (if (member :source-dose parts) 1 0)
			 ))
	 (page-no 1))
    (with-open-file (chart *chart-file*
			   :direction :output
			   :if-exists :supersede
			   :if-does-not-exist :create)
      (chart-header chart cur-pat pln total-pages)
      (if (member :combined parts)
	  (setq page-no (combined-doses chart cur-pat pln n-pts-page1
					page-no total-pages))
	  (progn
	    (ps:finish-page chart (< page-no total-pages))
	    (incf page-no)))
      (when (member :beam-specs parts)
	(setq page-no (beam-specs chart cur-pat pln page-no total-pages
				  'photon-neutron))
	(setq page-no (beam-specs chart cur-pat pln page-no total-pages
				  'electron)))
      (when (member :beam-frac-dose parts)
	(setq page-no (dose-per-beam
			chart cur-pat pln page-no total-pages t)))
      (when (member :beam-total-dose parts)
	(setq page-no (dose-per-beam
			chart cur-pat pln page-no total-pages nil)))
      (when (or (member :line-specs parts)
		(member :seed-specs parts))
	(brief-header chart cur-pat pln page-no total-pages)
	(if (member :line-specs parts)
	    (line-specs chart (coll:elements (line-sources pln))))
	(if (member :seed-specs parts)
	    (seed-specs chart (coll:elements (seeds pln))))
	(ps:finish-page chart (< page-no total-pages))
	(incf page-no))
      (when (member :source-dose parts)
	(dose-per-source chart cur-pat pln page-no total-pages)))))

;;;----------------------------------------------------

(defun chart-panel (chart-type cur-pat pln &rest pars)

  "chart-panel chart-type cur-pat pln &rest pars

Generates a chart of type chart-type from information specified by the
user through a dialog box, unless the user presses the cancel button
in the dialog box.  The chart file is written and if the user did not
specify File only, it is spooled to the user selected printer, to
produce the user specified number of copies."

  (sl:push-event-level)
  (let* ((num-copies 1)
	 (printer (first *postscript-printers*))
	 (printer-menu (sl:make-radio-menu *postscript-printers* :mapped nil))
	 (delta-y (+ 10 (max (sl:height printer-menu) 100)
		     10))
	 (cbox (sl:make-frame (+ 10 (sl:width printer-menu)
				 10 150 10)
			      (+ delta-y 30 10 30 10)
			      :title "Chart Parameters"))
	 (win (sl:window cbox))
	 (cpy-tln (sl:make-textline 150 30 :parent win
				    :label "Copies: "
				    :info (write-to-string num-copies)
				    :numeric t
				    :lower-limit 1
				    :upper-limit 9
				    :ulc-x (+ 10 (sl:width printer-menu)
					      10)
				    :ulc-y delta-y))
	 (accept-x (round (/ (- (sl:width cbox) 170) 2)))
	 (accept-btn (sl:make-exit-button 80 30 :label "Accept"
					  :parent win
					  :ulc-x accept-x
					  :ulc-y (+ delta-y 40)
					  :bg-color 'sl:green))
	 (cancel-btn (sl:make-exit-button 80 30 :label "Cancel"
					  :parent win
					  :ulc-x (+ accept-x 90)
					  :ulc-y (+ delta-y 40)))
	 parts-codes part-menu print-list)          ;; only for main chart
    (clx:reparent-window (sl:window printer-menu) win 10 10)
    (clx:map-window (sl:window printer-menu))
    (clx:map-subwindows (sl:window printer-menu))
    (sl:select-button 0 printer-menu)
    (when (eql chart-type 'main)
      (let ((pts (coll:elements (points cur-pat)))
	    (bms (coll:elements (beams pln)))
	    (lines (coll:elements (line-sources pln)))
	    (seeds (coll:elements (seeds pln)))
	    part-strings)
	(when seeds
	  (push :seed-specs parts-codes)
	  (push "Seed specs" part-strings))
	(when lines
	  (push :source-dose parts-codes)
	  (push "Dose per source" part-strings)
	  (push :line-specs parts-codes)
	  (push "Line sources" part-strings))
	(when (and bms pts)
	  (push :beam-total-dose parts-codes)
	  (push "Total doses" part-strings)
	  (push :beam-frac-dose parts-codes)
	  (push "Doses per treat." part-strings))
	(when bms
	  (push :beam-specs parts-codes)
	  (push "Beam settings" part-strings))
	(when (and pts (or bms lines seeds))
	  (push :combined parts-codes)
	  (push "Combined doses" part-strings))
	(setq part-menu (sl:make-menu part-strings :parent win
				      :ulc-x (+ 10 (sl:width printer-menu)
						10)
				      :ulc-y 10)))
      (ev:add-notify cbox (sl:selected part-menu)
	#'(lambda (bx mn num)
	    (declare (ignore bx mn))
	    (pushnew (nth num parts-codes) print-list)))
      (ev:add-notify cbox (sl:deselected part-menu)
	#'(lambda (bx mn num)
	    (declare (ignore bx mn))
	    (setf print-list
		  (remove (nth num parts-codes) print-list))))
      (dotimes (i (length parts-codes))
	(sl:select-button i part-menu)))
    (ev:add-notify cbox (sl:new-info cpy-tln)
      #'(lambda (cbox tl info)
	  (declare (ignore cbox tl))
	  (setq num-copies
		(round (read-from-string info)))))
    (ev:add-notify cbox (sl:selected printer-menu)
      #'(lambda (cbox m item)
	  (declare (ignore cbox m))
	  (setq printer (nth item *postscript-printers*))))
    (ev:add-notify cbox (sl:button-on accept-btn)
      #'(lambda (cbox bt)
	  (declare (ignore cbox bt))
	  (case chart-type
	    (main (main-chart print-list cur-pat pln))
	    (neutron (apply #'make-neutron-chart cur-pat pars))
	    ;; new dicom based on neutron but different
	    (dicom (apply #'make-dicom-chart cur-pat pars))
	    (leaf (apply #'make-leaf-chart cur-pat pln pars)))
	  (unless (string-equal "File only" printer)
	    (dotimes (i num-copies)
	      (run-subprocess (format nil "~a~a ~a"
				      *spooler-command*
				      printer *chart-file*))))))
    (sl:process-events)
    (when (eql chart-type 'main)
      (sl:destroy part-menu))
    (sl:destroy printer-menu)
    (sl:destroy cpy-tln)
    (sl:destroy accept-btn)
    (sl:destroy cancel-btn)
    (sl:destroy cbox)
    (sl:pop-event-level)))

;;;----------------------------------------------------

(defun write-leaf-settings (chart leaf-settings coll-info top)

  (let* ((leaf-data (mapcar #'(lambda (leaf-loc leaf-pair)
				(list (first leaf-loc)
				      (* (inf-leaf-scale coll-info)
					 (first leaf-pair))
				      (second leaf-pair)
				      (second leaf-loc)))
		      (leaf-pair-map coll-info) leaf-settings))
	 (nleaves (length leaf-data))
	 (halfway (1- (truncate nleaves 2))))  ; 1- because dotimes is 0-based
    (declare (type fixnum nleaves))
    (ps:set-position chart 1.0 top)
    (ps:indent chart 1.0)
    (ps:put-text chart (format nil "~A" (col-headings coll-info)))
    (dotimes (ileaf nleaves)
      (let ((lf (nth ileaf leaf-data)))
	(ps:put-text chart
		     (format nil
			     "~2D  ~5,1F                        ~5,1F  ~2D"
			     (first lf) (second lf) (third lf) (fourth lf)))
	(if (= ileaf halfway)                       ;; mark isocenter
	    (ps:put-text chart (format nil "~23@A" "+")))))))

;;;----------------------------------------------------

(defun make-leaf-chart (cur-pat pln beam)

  "make-leaf-chart cur-pat pln beam

Makes a leaf chart for MLC beam."

  (with-open-file (chart *chart-file*
			 :direction :output
			 :if-exists :supersede
			 :if-does-not-exist :create)
    (ps:initialize chart 0.5 0.5 7.5 10.0)
    (ps:set-font chart "Courier" 12)
    (ps:indent chart 0.1)
    (brief-header chart cur-pat pln 1 1)
    (ps:put-text
      chart
      "-------------------- LEAF COLLIMATOR SETTINGS --------------------")
    (ps:put-text chart "")
    (ps:put-text chart (format nil "For:  ~A" (name beam)))
    (write-leaf-settings chart
			 (compute-mlc (collimator-angle beam)
				      (get-mlc-vertices beam)
				      (edge-list
					(collimator-info (machine beam))))
			 (collimator-info (machine beam))
			 2.0)
    (ps:finish-page chart)))

;;;----------------------------------------------------

(defun make-neutron-chart (cur-pat beam-pairs date)

  "make-neutron-chart cur-pat beam-pairs date

Writes a neutron chart for the specified patient, for each
original-beam/current-beam pair, and date."

  (with-open-file (chart *chart-file*
			 :direction :output
			 :if-exists :supersede
			 :if-does-not-exist :create)
    (ps:initialize chart 0.5 0.5 7.5 10.0)
    (ps:set-font chart "Courier" 12)
    (ps:indent chart 0.1)
    (dolist (bms beam-pairs)
      (let* ((orig-bm (first bms))
	     (curr-bm (second bms))
	     (pln (third bms))
	     (o-val nil)
	     (c-val nil)
	     (mach (machine orig-bm)))
	(brief-header chart cur-pat pln 1 1)
	(ps:put-text
	  chart
	  "-------------------- NEUTRON BEAM TRANSFER --------------------")
	(ps:put-text chart (format nil "BEAM:       ~a" (name curr-bm)))
	(ps:put-text chart (format nil "XFER DATE:  ~a" date))
	(ps:put-text chart "")
	(ps:put-text chart
		     "         Setting Planned       Transfered Changed?")
	(ps:put-text chart "")
	;; print gantry starting angle
	(setq o-val (scale-angle (gantry-angle orig-bm)
				 (gantry-scale mach)
				 (gantry-offset mach)))
	(setq c-val (scale-angle (gantry-angle curr-bm)
				 (gantry-scale mach)
				 (gantry-offset mach)))
	(ps:put-text
	  chart
	  (format nil "~15a: ~6,1F ~5a  ~6,1F ~5a   ~5a"
		  "Gantry Start"
		  (first o-val) (second o-val)
		  (first c-val) (second c-val)
		  (if (poly:nearly-equal (gantry-angle orig-bm)
					 (gantry-angle curr-bm))
		      "     "
		      "*****")))
	;; print gantry stopping angle
	(setq o-val (scale-angle
		      (+ (arc-size orig-bm) (gantry-angle orig-bm))
		      (gantry-scale mach) (gantry-offset mach)))
	(setq c-val (scale-angle
		      (+ (arc-size curr-bm) (gantry-angle curr-bm))
		      (gantry-scale mach) (gantry-offset mach)))
	(ps:put-text
	  chart (format nil "~15a: ~6,1F ~5a  ~6,1F ~5a   ~5a"
			"Gantry Stop"
			(first o-val) (second o-val)
			(first c-val) (second c-val)
			(if (poly:nearly-equal
			      (+ (arc-size orig-bm) (gantry-angle orig-bm))
			      (+ (arc-size curr-bm) (gantry-angle curr-bm)))
			    "     "
			    "*****")))
	;; print collimator angle
	(setq o-val (scale-angle (collimator-angle orig-bm)
				 (collimator-scale mach)
				 (collimator-offset mach)
				 (collimator-negative-flag mach)
				 (collimator-lower-limit mach)
				 (collimator-upper-limit mach)))
	(setq c-val (scale-angle (collimator-angle curr-bm)
				 (collimator-scale mach)
				 (collimator-offset mach)
				 (collimator-negative-flag mach)
				 (collimator-lower-limit mach)
				 (collimator-upper-limit mach)))
	(ps:put-text
	  chart
	  (format nil "~15a: ~6,1F ~5a  ~6,1F ~5a   ~5a"
		  "Collim Angle"
		  (first o-val) (second o-val)
		  (first c-val) (second c-val)
		  (if (poly:nearly-equal (collimator-angle orig-bm)
					 (collimator-angle curr-bm))
		      "     "
		      "*****")))
	;; print turntable angle
	(setq o-val (scale-angle (couch-angle orig-bm)
				 (turntable-scale mach)
				 (turntable-offset mach)
				 (turntable-negative-flag mach)
				 (turntable-lower-limit mach)
				 (turntable-upper-limit mach)))
	(setq c-val (scale-angle (couch-angle curr-bm)
				 (turntable-scale mach)
				 (turntable-offset mach)
				 (turntable-negative-flag mach)
				 (turntable-lower-limit mach)
				 (turntable-upper-limit mach)))
	(ps:put-text
	  chart
	  (format nil "~15a: ~6,1F ~5a  ~6,1F ~5a   ~5a"
		  "Couch Angle"
		  (first o-val) (second o-val)
		  (first c-val) (second c-val)
		  (if (poly:nearly-equal (couch-angle orig-bm)
					 (couch-angle curr-bm))
		      "     "
		      "*****")))
	;; print num fractions
	(ps:put-text
	  chart
	  (format nil "~15a: ~5@a         ~5@a          ~5a"
		  "Fractions"
		  (n-treatments orig-bm) (n-treatments curr-bm)
		  (if (= (n-treatments orig-bm) (n-treatments curr-bm))
		      "     "
		      "*****")))
	;; print MU/fraction
	(let ((mu-orig-per-frac (round (/ (the single-float
					    (monitor-units orig-bm))
					  (the fixnum
					    (n-treatments orig-bm)))))
	      (mu-curr-per-frac (round (/ (the single-float
					    (monitor-units curr-bm))
					  (the fixnum
					    (n-treatments curr-bm))))))
	  (ps:put-text
	    chart
	    (format nil "~15a: ~5@a  ~5a  ~5@a  ~7a ~5a"
		    "Mu/Fraction"
		    mu-orig-per-frac "Mu/F"
		    mu-curr-per-frac "Mu/F"
		    (if (= mu-orig-per-frac mu-curr-per-frac)
			"     "
			"*****"))))
	;; print out the wedge selection and rotation discrepancies
	(let* ((wdg-orig (wedge orig-bm))
	       (wdg-curr (wedge curr-bm))
	       (id-orig (id wdg-orig))
	       (id-curr (id wdg-curr))
	       (rot-orig (rotation wdg-orig))
	       (rot-curr (rotation wdg-curr)))
	  (ps:put-text
	    chart
	    (format nil "~15a:   ~13a ~12a ~5a"
		    "Wedge Sel"
		    (wedge-label id-orig (machine orig-bm))
		    (wedge-label id-curr (machine curr-bm))
		    (if (= id-orig id-curr) "     "
			"*****")))
	  (setq o-val (if (zerop id-orig) '("NONE" "")
			  (scale-angle rot-orig
				       (wedge-rot-scale mach)
				       (wedge-rot-offset mach))))
	  (setq c-val (if (zerop id-curr) '("NONE" "")
			  (scale-angle rot-curr
				       (wedge-rot-scale mach)
				       (wedge-rot-offset mach))))
	  (ps:put-text
	    chart
	    (format nil "~15a: ~6,1F ~5a  ~6,1F ~5a   ~5a"
		    "Wedge Rot"
		    (first o-val) (second o-val)
		    (first c-val) (second c-val)
		    (if (or (= 0 id-orig id-curr)
			    ;; eql can compare nil to 90.0, eg.
			    (eql rot-orig rot-curr))
			"     "
			"*****"))))
	;; print out the leaf discrepancies - sort by increasing leaf number
	;; to make more readable on chart.
	(ps:put-text chart "")
	(do* ((leaf-names (leaf-pair-map (collimator-info mach)))
	      (orig-vals (leaf-settings (collimator orig-bm)))
	      (curr-vals (leaf-settings (collimator curr-bm)))
	      (triples (mapcar #'list
			   (reduce #'append leaf-names)
			 (reduce #'append orig-vals)
			 (reduce #'append curr-vals)))
	      (sorted (sort (copy-tree triples) #'< :key #'first)
		      (rest sorted))
	      (triple (first sorted) (first sorted)))
	     ((null sorted))
	  (unless (poly:nearly-equal (second triple) (third triple))
	    (ps:put-text chart
			 (format nil "Leaf ~10a: ~6,1F ~5a  ~6,1F ~5a   ~5a"
				 (first triple) (second triple) "cm "
				 (third triple) "cm " "*****"))))
	;; write out all the leaf settings
	(write-leaf-settings chart
			     (leaf-settings (collimator curr-bm))
			     (collimator-info (machine curr-bm))
			     4.0)
	(ps:finish-page chart t)))))

;;;----------------------------------------------------
;;; DICOM chart functions written by Jon Jacky
;;;----------------------------------------------------
;;; Like brief-header in charts.cl but compressed and rearranged
;;; to better fit page and match info on Eletka RTD screens
;;; also add date parameter, also note beam number is page number
;;; add col2-horiz parameter.

(defun dicom-header (chart cur-pat pln date label pgnum total-pgs col2-horiz
		     dicom-pat-id &optional (top-margin 0.0))

  "dicom-header chart cur-pat pln pgnum total-pgs &optional (top-margin 0.0)

writes the brief header that appears at the top (or down an amount of
top-margin) on every chart page."

  (declare (type single-float top-margin))
  (ps:draw-rectangle chart 0.5 0.5 7.5 10.0)
  (ps:set-position chart 0.1 (+ top-margin 0.2))
  (let ((user (getenv "USER")))
    (ps:put-text chart (format nil "~A on ~A by ~A" label date user)))
  (ps:set-position chart 6.0 (+ top-margin 0.2))
  (ps:put-text chart (format nil "PAGE:  ~A of ~A" pgnum total-pgs))
  (ps:set-position chart 0.1 (+ top-margin 0.5))
  (ps:indent chart 0.1)
  (ps:put-text chart (format nil "PAT. ID: ~A" dicom-pat-id))
  (ps:put-text chart (format nil "PATIENT: ~A" (name cur-pat)))
  (when pln
    (ps:put-text chart (format nil "PLAN:    ~A" (name pln))))
  (ps:set-position chart col2-horiz (+ top-margin 0.5)) ; start a new column
  (ps:indent chart col2-horiz) ; set left margin for subsequent lines in column
  (ps:put-text chart (format nil "HOSPITAL NUMBER    : ~A"
			     (hospital-id cur-pat)))
  (ps:put-text chart (format nil "PRISM PATIENT, CASE: ~A, ~A"
			     (patient-id cur-pat) (case-id cur-pat)))
  (when pln
    (ps:put-text chart (format nil "PLAN DATE:  ~A" (time-stamp pln))))
  (ps:set-position chart 0.1 (+ top-margin 1.0625))
  (ps:indent chart 0.1)
  (ps:put-text
    chart
    "-----------------------------------------------------------------------")

  (ps:put-text chart ""))                        ; put "cursor" back at indent

;;;----------------------------------------------------

(defun write-dicom-leaf-settings (chart copy-coll curr-coll coll-info horiz)
  ;; Only write "*" next to leaves where differences change field shape
  ;;  not at leaves moved to make flagpole or open under jaw edge
  (let* ((end-tol 0.3) ; if edge is 3mm different,dosimetrist prob'ly intended
	 (print-tol 0.01)             ; print * on chart if they differ at all
	 (copy-leaves (leaf-settings copy-coll))
	 (curr-leaves (leaf-settings curr-coll))
	 (shapes (shape-diff copy-coll curr-coll end-tol))
	 (nleaves (length shapes))                  ; should be 40 for SL
	 (halfway (1- (truncate nleaves 2))))  ; 1- because dotimes is 0-based
    (declare (type single-float end-tol print-tol)
	     (type fixnum nleaves))
    (ps:indent chart horiz)                      ; so subsequent lines line up
    (ps:put-text chart (format nil     ; hard code captions to align with data
			       "Y2   Transf Planned Planned Transf Y1"))
    (dotimes (ileaf nleaves)
      (let ((lf (nth ileaf shapes))
	    (copy-pair (nth ileaf copy-leaves))
	    (curr-pair (nth ileaf curr-leaves)))
	(ps:put-text
	  chart
	  (format nil
		  "~2D ~A ~6,2F ~6,2F   ~6,2F ~6,2F ~A"
		  (first (nth ileaf (leaf-pair-map coll-info)))
		  (cond ((or (and (lpair-open-o lf)
				  (not (lpair-open lf)))
			     (and (not (lpair-open-o lf))
				  (lpair-open lf))
			     (and (lpair-open-o lf)
				  (lpair-open lf)
				  (> (abs (- (lpair-xl-o lf)
					     (lpair-xl lf)))
				     print-tol)))
			 "*")                       ; shape changed at leaf
			((lpair-open lf) ".")       ; open and not changed
			(t " "))                    ; closed and not changed
		  (* (the single-float (inf-leaf-scale coll-info))
		     (the single-float (first curr-pair)))
		  (* (the single-float (inf-leaf-scale coll-info))
		     (the single-float (first copy-pair)))
		  (second copy-pair)
		  (second curr-pair)
		  (cond ((or (and (lpair-open-o lf)
				  (not (lpair-open lf)))
			     (and (not (lpair-open-o lf))
				  (lpair-open lf))
			     (and (lpair-open-o lf)
				  (lpair-open lf)
				  (> (abs (- (lpair-xr-o lf)
					     (lpair-xr lf)))
				     print-tol)))
			 "*")
			((lpair-open lf) ".")
			(t " "))))
	(if (= ileaf halfway)                       ;; mark isocenter
	    (ps:put-text chart (format nil "                    +")))))))

;;;----------------------------------------------------
;;; Based on make-neutron-chart, but quite a bit different now.

(defun make-dicom-chart (cur-pat p-bm-info date label dicom-pat-id)

  "make-dicom-chart cur-pat p-bm-info date label

Writes a chart for CUR-PAT with all the Prism beams and segments in
P-BM-INFO [a list of segments in the Dicom beams]."

  ;; P-BM-INFO is a list, in forward order, each entry being:
  ;;  ( <OrigBmInst> <CopyBmInst> <CurrBmInst> <Plan> <Prism-Beam-Object> )
  ;; with one entry for each segment.  Note that the list contains all Prism
  ;; beams - that is, all segments for all Dicom beams.  They are grouped
  ;; into Dicom beams in order - all segments for one Dicom beam followed by
  ;; all segments for the next, and so forth.
  ;;
  ;; OrigBmInst is uncopied original Prism beam.
  ;; CopyBmInst and CurrBmInst are both copied beams so that changes
  ;; to their collimators will not side-effect real Prism beams.

  (with-open-file (chart *chart-file*
			 :direction :output
			 :if-exists :supersede
			 :if-does-not-exist :create)
    (ps:initialize chart 0.5 0.5 7.5 10.0)
    (ps:set-font chart "Courier" 12)
    (ps:indent chart 0.1)
    (do ((p-bms p-bm-info (cdr p-bms))
	 (total-pages (length p-bm-info))
	 (col2-horiz 3.75) (beam-vert 1.25) (warn-vert 9.0)
	 (segnum 0) (totsegs 0) (n-warn 5) (pgnum 0)
	 (curr-pbi)             ;Current [copied/mutated] Prism beam instance.
	 (copy-coll)                                ;Collimator of Copied Beam
	 (curr-coll)                               ;Collimator of Current Beam
	 (pln)                                      ;Current Plan
	 (p-bm-obj)                             ;Prism-Beam structure instance
	 (seg-type :static)                    ; :Static, :Dynamic, or :Static
	 (p-bmdata) (c-val) (mach))
	((null p-bms))
      (declare (type list p-bms p-bmdata c-val)
	       (type (member :static :dynamic :segment) seg-type)
	       (type single-float col2-horiz beam-vert warn-vert)
	       (type fixnum total-pages n-warn pgnum segnum totsegs))
      (setq p-bmdata (car p-bms)
	    curr-pbi (third p-bmdata)    ;Current [copied/mutated] Prism beam.
	    pln (fourth p-bmdata)                   ;Current Plan
	    p-bm-obj (fifth p-bmdata)           ;Prism-Beam [segment] instance
	    copy-coll (collimator (second p-bmdata)) ;Collimator of Copied Beam
	    curr-coll (collimator curr-pbi)        ;Collimator of Current Beam
	    seg-type (pr-beam-segtype p-bm-obj)     ;Prism beam type
	    mach (machine (first p-bmdata)))   ;Orig [uncopied] beam's machine
      (setq pgnum (the fixnum (1+ pgnum)))
      (dicom-header chart cur-pat pln date label pgnum
		    total-pages col2-horiz dicom-pat-id)
      (ps:set-position chart 0.1 beam-vert)
      (ps:put-text chart (format nil "LINAC:     ~A" (car (ident mach))))
      (ps:put-text chart (format nil "BEAM NAME: ~D. ~A"
				 (pr-beam-dbeam-num p-bm-obj)
				 (name curr-pbi)))
      (ps:put-text
	chart
	(cond ((eq seg-type :static) "")
	      ((eq seg-type :dynamic)
	       (setq segnum 1)
	       (do ((pp (cdr p-bms) (cdr pp))
		    (cnt 1 (the fixnum (1+ cnt))))
		   ((null pp)
		    (setq totsegs cnt))
		 (unless (eq (pr-beam-segtype (fifth (car pp))) :segment)
		   (setq totsegs cnt)
		   (return)))
	       (format nil "SEGMENT:   1 of ~D" totsegs))
	      ((eq seg-type :segment)
	       (format nil "SEGMENT:   ~D of ~D, from Prism beam  ~A"
		       (setq segnum (the fixnum (1+ segnum)))
		       totsegs
		       (name curr-pbi)))))

      ;; Items in same order as Geometry tab on Elekta RTD beam panel.
      (ps:put-text chart "")
      (ps:put-text chart "GEOMETRY:")
      (ps:put-text chart "")
      (setq c-val (scale-angle (gantry-angle curr-pbi)
			       (gantry-scale mach)
			       (gantry-offset mach)))
      (ps:put-text chart (format nil "Gantry Angle:       ~6,1F ~5A"
				 (first c-val) (second c-val)))
      (setq c-val (scale-angle (collimator-angle curr-pbi)
			       (collimator-scale mach)
			       (collimator-offset mach)
			       (collimator-negative-flag mach)
			       (collimator-lower-limit mach)
			       (collimator-upper-limit mach)))
      (ps:put-text chart (format nil "Diaphragm rotation: ~6,1F ~5A"
				 (first c-val) (second c-val)))
      (ps:put-text chart "")

      ;; Elekta X1,X2 Y1,Y2 are Prism/DICOM y2,-y1 x2,-x1 respectively.
      (ps:put-text chart (format nil "Diaphragm X1:       ~7,2F cm"
				 (y2 curr-coll)))
      (ps:put-text chart (format nil "Diaphragm X2:       ~7,2F cm"
				 (- (y1 curr-coll))))
      (ps:put-text chart (format nil "Diaphragm Y1:       ~7,2F cm"
				 (x2 curr-coll)))
      (ps:put-text chart (format nil "Diaphragm Y2:       ~7,2F cm"
				 (- (x1 curr-coll))))
      (ps:put-text chart "")

      (setq c-val (scale-angle (couch-angle curr-pbi)
			       (turntable-scale mach)
			       (turntable-offset mach)
			       (turntable-negative-flag mach)
			       (turntable-lower-limit mach)
			       (turntable-upper-limit mach)))
      (ps:put-text chart (format nil "Isocenter rotation: ~6,1F ~5A"
				 (first c-val) (second c-val)))

      ;; Sort of like the Radiation tab on the Elekta RTD beam panel.
      (ps:put-text chart "")
      (ps:put-text chart "")
      (ps:put-text chart "RADIATION:")
      (ps:put-text chart "")
      (ps:put-text chart (format nil "Radiation type:     ~A" (particle mach)))
      (ps:put-text chart (format nil "Energy:             ~A MV"
				 (energy mach)))
      (ps:put-text chart "")

      (when (eq seg-type :static)
	(let ((mu-val (monitor-units curr-pbi))
	      (n-frac (n-treatments curr-pbi)))
	  (declare (type single-float mu-val)
		   (type fixnum n-frac))
	  (ps:put-text
	    chart (format nil "Total MU planned:   ~6,1F MU" mu-val))
	  (ps:put-text chart (format nil "Fractions:            ~2D" n-frac))
	  ;; Printed Fractions * printed Daily MU may not equal Total MU.
	  (ps:put-text chart (format nil "Daily MU:            ~3D   MU/F"
				     (round (/ mu-val n-frac))))
	  (ps:put-text chart "")))

      (when (eq seg-type :dynamic)
	(let ((tot-mu (pr-beam-tot-mu p-bm-obj))
	      (n-frac (n-treatments curr-pbi)))
	  (declare (type single-float tot-mu)
		   (type fixnum n-frac))
	  (ps:put-text
	    chart (format nil "Total MU, all segs: ~6,1F MU" tot-mu))
	  (ps:put-text chart (format nil "Fractions:            ~2D" n-frac))
	  (ps:put-text chart (format nil "Daily MU, all segs:  ~3D   MU/F"
				     (round (/ tot-mu n-frac))))
	  (ps:put-text chart "")))

      (when (or (eq seg-type :dynamic)
		(eq seg-type :segment))
	(let ((tot-mu (pr-beam-tot-mu p-bm-obj))
	      (seg-mu (pr-beam-seg-mu p-bm-obj))
	      (seg-cum (pr-beam-cum-mu-inc p-bm-obj))
	      (n-frac (n-treatments curr-pbi)))
	  (declare (type single-float tot-mu seg-mu seg-cum)
		   (type fixnum n-frac))
	  (ps:put-text chart
		       (format nil "Total MU, this seg: ~6,1F (~5,1F%)"
			       seg-mu
			       (* 100.0 (/ seg-mu tot-mu))))
	  (ps:put-text chart
		       (format nil "Daily MU, this seg:  ~3D   MU/F"
			       (round (/ seg-mu n-frac))))
	  (ps:put-text chart
		       (format nil "Cumu. MU, this seg: ~6,1F (~5,1F%)"
			       seg-cum
			       (* 100.0 (/ seg-cum tot-mu))))
	  (ps:put-text chart
		       (format nil "DayCu MU, this seg:  ~3D   MU/F"
			       (round (/ seg-cum n-frac))))
	  (ps:put-text chart "")))

      ;; Special cases for internal, external wedges, external blocks
      (let* ((wedge-id (id (wedge curr-pbi)))
	     (wedge-name (wedge-label wedge-id mach))
	     (wedge-fixed (string-equal wedge-name "Fixed Wedge"))
	     (wedge-ext (and (not (zerop wedge-id)) (not wedge-fixed)))
	     (blocks-ext (if (coll:elements (blocks curr-pbi)) t nil))
	     ;; shadow tray holds any external wedge or external blocks
	     (shadow-tray
	       (cond (wedge-ext
		       (accessory-code
			 (find wedge-id (wedges mach) :key #'ID)))
		     (blocks-ext (tray-accessory-code mach))
		     (t "NONE"))))
	;; Internal wedge is either in or out, nothing else to specify.
	(ps:put-text chart
		     (format nil "Internal wedge pos: ~A"
			     (if (string-equal wedge-name "Fixed Wedge")
				 "IN" "OUT")))

	;; May have ext wedge or ext blocks but not both - only one tray!
	(cond (wedge-ext
		(ps:put-text
		  chart (format nil "External wedge:     ~A" wedge-name))
		(setq c-val (scale-angle (rotation (wedge curr-pbi))
					 (wedge-rot-scale mach)
					 (wedge-rot-offset mach)))
		(ps:put-text
		  chart
		  (format nil "Ext. wedge rot:     ~6,1F ~5A"
			  (first c-val) (second c-val))))
	      (blocks-ext
		(ps:put-text chart "")
		(ps:put-text chart "External blocks"))
	      (t (ps:put-text chart "") ; no ext wedges or blocks - make space
		 (ps:put-text chart "")))
	(ps:put-text chart "")

	(ps:put-text chart (format nil "Shadow Tray:        ~A" shadow-tray)))

      ;; Applicator, fitment are for electron beams only.
      (ps:put-text chart "Applicator:         NONE")
      (ps:put-text chart "Fitment Number:     NONE")

      ;; Make bottom of DIAPHRAGMS section line up with bottom of leaves.
      (dotimes (i (case seg-type
		    (:static 6)
		    (:segment 5)
		    (:dynamic 1)))
	(ps:put-text chart ""))

      (ps:put-text chart "DIAPHRAGMS:")
      (ps:put-text chart "")
      (ps:put-text chart "      Planned     Transferred")
      (let ((print-tol 0.01))        ; Print * on chart if they differ at all.
	(put-collim-line chart "X1" (y2 copy-coll) (y2 curr-coll) print-tol)
	(put-collim-line chart "X2"
			 (- (y1 copy-coll)) (- (y1 curr-coll)) print-tol)
	(put-collim-line chart "Y1" (x2 copy-coll) (x2 curr-coll) print-tol)
	(put-collim-line chart "Y2"
			 (- (x1 copy-coll)) (- (x1 curr-coll)) print-tol))

      ;; Start a second column.
      (ps:set-position chart col2-horiz beam-vert)
      (ps:indent chart col2-horiz)      ;Set left margin for subsequent lines.
      (ps:put-text chart (format nil "PRISM MACHINE: ~A" (name mach)))

      ;; Write out all the leaf settings.
      (ps:put-text chart "")                        ; Align with GEOMETRY.
      (ps:put-text chart "")
      (ps:put-text chart "")                 ; one more to match SEGMENTS line
      (write-dicom-leaf-settings chart copy-coll curr-coll
				 *sl-collim-info* col2-horiz)

      ;; Write warnings at bottom of page.
      (let ((wl (collim-warnings copy-coll curr-coll)))
	(if wl (let ((wll (if (<=  (length wl) n-warn)
			      wl
			      (append
				(subseq wl 0 (- n-warn 1))
				'("(There were more warnings ...)")))))
		 (ps:set-position chart 0.1 warn-vert)
		 (ps:indent chart 0.1)
		 (dolist (w wll) (ps:put-text chart w)))))

      (ps:finish-page chart t))))

;;;----------------------------------------------------

(defun put-collim-line (chart label copy-coll-data curr-coll-data tol)
  (declare (type single-float copy-coll-data curr-coll-data tol))
  (ps:put-text chart
	       (format nil "~A:   ~6,2F         ~6,2F ~A"
		       label copy-coll-data curr-coll-data
		       (if (< (abs (- copy-coll-data curr-coll-data)) tol)
			   " "
			   "*"))))

;;;----------------------------------------------------
;;; End.
