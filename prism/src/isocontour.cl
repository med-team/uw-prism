;;;
;;; isocontours
;;;
;;; This file contains updated code for the isodose contour extraction
;;; facility.  Originally from contour-functions.lsp in the old prism
;;; SCCS directory on eowyn.
;;;
;;; Reference: see CONLIN.FOR, an old piece of FORTRAN code used in PLAN32.
;;; The spirit, if not the letter of that code is followed below.
;;;
;;; 20-Sep-1993 J. Unger bring up to current specs.
;;; 05-Oct-1994 J. Unger fix bug in get-isodose-curves which would cause
;;; curves of less than three vertices to be returned when a samples array
;;; value exactly equal to the supplied threshold was detected.
;;;  8-Jan-1995 I. Kalet remove proclaim form
;;;  3-Sep-1995 I. Kalet change some macros to functions, delete
;;;  points-adjacent, not used anywhere.
;;; 03-Jul-1997 BobGian updated NEARLY-xxx -> poly:NEARLY-xxx .
;;;

(in-package :prism)

;;;---------------------------------------------

(defun found-new-contour (unmarked samples level a b c d)

  "FOUND-NEW-CONTOUR unmarked samples level a b c d

Returns true iff both of the following conditions hold:
  1. Either location (a,b) or location (c,d) in the samples array has not 
       yet been marked.
  2. The value of samples array entry (a,b), the supplied level parameter, 
       and the value of samples array entry (c,d) are strictly increasing.
       Testing one side of this inequality is adequate to determine whether
       an isolevel contour passes between (a,b) and (c,d) when the entire
       plane is searched, since if the inequality were true in the other
       direction, some other part of this contour would be detected
       elsewhere." 

  (and
   (or (aref unmarked a b) (aref unmarked c d))
   (< (aref samples a b) level (aref samples c d))))

;;;---------------------------------------------

(defun crossed-segment (samples level a b c d)

  "CROSSED-SEGMENT samples level a b c d

Returns true if level falls strictly between the values of the samples
  array at index (a,b) and index (c,d)."

  (or
   (< (aref samples a b) level (aref samples c d))
   (> (aref samples a b) level (aref samples c d))))

;;;---------------------------------------------

(defun out-of-bounds (dim-i dim-j a b c d)

  "OUT-OF-BOUNDS dim-i dim-j a b c d

Returns t iff the point (a,b) or (c,d) is outside of the samples array --
  ie, outside the region bounded by 0..(dim-1) in each dimension."

  (or (< a 0) (< b 0) (< c 0) (< d 0)
      (>= a dim-i) (>= b dim-j) (>= c dim-i) (>= d dim-j)))

;;;---------------------------------------------

(defun back-to-start (a b c d p q r s)

  "BACK-TO-START a b c d p q r s

Returns t iff the points (a,b) and (c,d) coincide (up to order) with
  the points (p,q) and (r,s)."

  (or
   (and (= a p) (= b q) (= c r) (= d s))
   (and (= a r) (= b s) (= c p) (= d q))))

;;;---------------------------------------------

(defun initialize-unmarked-array (unmarked dim-i dim-j)

  "INITIALIZE-UNMARKED-ARRAY unmarked dim-i dim-j

Sets all entries in the subarray of the unmarked array from (0,0) to
  (dim-i, dim-j) to t."

  (dotimes (i dim-i)
    (dotimes (j dim-j)
      (setf (aref unmarked i j) t))))

;;;---------------------------------------------

(defun borders-zero (samples a b c d)

  "BORDERS-ZERO samples a b c d

Returns t iff the dose at either of the points (a,b) or (c,d) in the
  samples array is zero."

  (or 
   (poly:nearly-equal (aref samples a b) 0.0) 
   (poly:nearly-equal (aref samples c d) 0.0)))

;;;---------------------------------------------

(defun compute-location (samples level a b c d)

  "COMPUTE-LOCATION samples level a b c d

Given indices (a,b) and (c,d) into the samples array, computes and
returns the location in 3-space of the point between the two through
which the contour of the specified level passes, using linear
interpolation."

  (let* ((level-ab (aref samples a b))
	 (level-cd (aref samples c d))
	 (frac     (float (/ (- level level-ab) (- level-cd level-ab)))))

    (declare (single-float level-ab level-cd frac))
    (declare (fixnum a b c d))

    (list 
     (+ a (* frac (- c a)))
     (+ b (* frac (- d b))))))

;;;---------------------------------------------

(defun follow-contour (unmarked samples level p q r s 
                       &key checking complete) 

  "FOLLOW-CONTOUR unmarked samples level p q r s 
                  &key checking complete)

Follows the contour between points pq and rs in the samples array,
  initially from the direction determined by checking, and returns the
  contour.  If complete is false, then the returned contour will have
  gaps where the isolevel curve is adjacent to regions of the samples
  matrix equal to 0.0; otherwise the complete curve is returned.  A list
  of 2-tuples is returned, where the first element of a given 2-tuple is
  one of :open or :closed (to indicate whether the associated vertex-list
  is implicitly a closed loop or an open segment), and the secone element
  of a given 2-tuple is the vertex-list itself.  More than one such 2-tuple
  may be returned, since it is possible (when complete is false) for 
  several disjoint components of the same isocurve (separated by regions of 
  zero-adjacency) to be returned.

  NOTE that the order of the input pair of points is critically dependent
  on the intial value of checking; in particular - 

  if checking = :left or :right then pq must be 'above' rs (ie > y val)
  if checking = :top or :bottom then pq must be 'to left of' rs (ie < x val)"

    ;; Algorithm: the locations indexed by pq and rs are known to lie on 
    ;; either side of the isocurve.  Consider the square in the lattice
    ;; one of whose sides consists of the segment from pq to rs (there are
    ;; actually two such squares, but the algorithm considers the one which
    ;; is appropriate for the direction of the search conducted by the calling
    ;; routine).  The isocurve enters the square through this segment and 
    ;; must leave it by one of the other three segments.  Systematically 
    ;; test the sample values at the endpoints of the other three segments 
    ;; against the supplied level to determine which the isocurve crosses
    ;; through to exit.  Compute the location of the exiting isocurve and
    ;; follow it to the next square, repeating the search, and adding these
    ;; points to the vertex list to be returned as we go.  Take care to 
    ;; mark the points along this isocontour so we don't 'find' it again
    ;; later in our search.  We stop following this isocontour when we
    ;; return to our starting point, or when we run off the edge of the 
    ;; samples array.  If we run off the samples array (out-of-bounds), 
    ;; then add the vertex-list from this-piece to pieces-seen (if this
    ;; piece is non-nil) and convert each vertex list on the pieces-seen
    ;; list to a 2-tuple of the form (:open vertex-list).  If we end up
    ;; back at the start, check pieces-seen -- if nil, then the entire
    ;; accumulated curve is in this-piece; return a list of the form 
    ;; (:closed this-piece) -- if non-nil, we may need to splice back
    ;; together a disconnected segment that we 'found' in the beginning
    ;; of the search and just finished finding at the end; see below
    ;; for details.  

    ;; Note that if complete is false, then we need to recognize when we've
    ;; followed the contour into a region where it is bordered by zero's, 
    ;; and if so, we continue to follow the contour but do not add successive
    ;; points to the result; any new contour points in a non-zero region 
    ;; are added to a new this-piece list.
 
  (let ((a p) (b q) (c r) (d s)
        (dim-i (array-dimension samples 0))
        (dim-j (array-dimension samples 1))
        (this-piece nil)                  ;; A single isocurve component 
        (pieces-seen nil))                ;; Multiple components, separated
                                          ;; by regions of zero-adjacency.

    (declare (fixnum a b c d p q r s))

    (loop 

      (when (crossed-segment samples level a b c d)
        (setq checking 
          (case checking 
            (:top :bottom) (:bottom :top) (:left :right) (:right :left)))

    ;; condition on 'if' directly below will be true when we're allowed
    ;; to add the point just found to the growing this-piece list; else, 
    ;; push any segment on the this-piece list onto the pieces-seen list, 
    ;; and make a new this-piece list, but don't add anything to the new
    ;; this-piece list since we're bordering zero's and need to watch for
    ;; that.

        (if (or complete (not (borders-zero samples a b c d)))
          (push (compute-location samples level a b c d) this-piece)
          (when this-piece
            (push this-piece pieces-seen)
            (setq this-piece nil)))
         
        (setf (aref unmarked a b) nil)    ;; flag these points as marked
        (setf (aref unmarked c d) nil))   ;; (end of 'when' at top of loop)

      (case checking
        (:bottom (setq checking :left)   (incf b) (decf c))
        (:left   (setq checking :top)    (incf c) (incf d)) 
        (:top    (setq checking :right)  (incf a) (decf d))
        (:right  (setq checking :bottom) (decf a) (decf b)))

    ;; if out of bounds, we can return any pieces-seen (& this-piece) as
    ;; open segments directly.

      (when (out-of-bounds dim-i dim-j a b c d) 
        (when this-piece (push this-piece pieces-seen))
        (return (mapcar #'(lambda (piece) (list :open piece)) pieces-seen)))

    ;; if back to start, first check pieces-seen -- if it's nil, then
    ;; all the info is contained in this-piece, which must be a single,
    ;; closed loop, so return it directly.  If pieces-seen is non-nil,
    ;; then some regions bordering 0 must have been omitted from the
    ;; curve.  Check this-piece -- it it's nil, then all info is on 
    ;; pieces seen and consists of a series of open segments delineated
    ;; by regions of 0-borders, so return all segments marked as open.
    ;; If this-piece is non-nil, then there is info on both lists, and
    ;; again all segments found are open, delineated by 0-borders.  But
    ;; we must splice back together the first segment encountered with
    ;; the last one, since these are two pieces of the same stretch of
    ;; contour, which we 'found' at the beginning and end of the search.
    ;; The two points which need to be spliced together are the first 
    ;; encountered point on pieces-seen and the last encountered point on 
    ;; this-piece.  The former point is the last point of the last vertex 
    ;; list on pieces-seen, since we pushed points onto the vertex list and 
    ;; pushed vertex lists onto pieces-seen in the order we saw them.  The 
    ;; latter point is the first point on this-piece, again, since we were 
    ;; pushing previously seen points onto this vertex list.  Append the 
    ;; last vertex-list on pieces-seen to the this-piece vertex list; and 
    ;; cons this composite list into the rest of the pieces-seen list. 

      (when (back-to-start a b c d p q r s)
        (if (null pieces-seen)
          (return (list (list :closed this-piece)))
          (if (null this-piece)
            (return (mapcar #'(lambda (piece) (list :open piece)) pieces-seen))
            (cons 
              (append (first (last pieces-seen)) this-piece) 
              (butlast pieces-seen))))))))

;;;---------------------------------------------

(defun normalize-isodose-curves (curves samples x-size y-size x-orig y-orig)

  "NORMALIZE-ISODOSE-CURVES curves samples x-size y-size x-orig y-orig

Takes the values of each point in each vertex list in curves and scales 
  it into the space determined by the size and origin of the original 
  samples array in that space."
 
  (let ((dx (float (/ x-size (1- (array-dimension samples 0)))))
        (dy (float (/ y-size (1- (array-dimension samples 1))))))

    (dolist (curve curves)
      (dolist (vertex (second curve))
        (setf (first vertex) (+ x-orig (* dx (first vertex))))
        (setf (second vertex) (+ y-orig (* dy (second vertex))))))

    curves))

;;;---------------------------------------------

(defun get-isodose-curves (samples x-size y-size x-orig y-orig level 
                           &key (unmarked nil) (complete t))

  "GET-ISODOSE-CURVES samples x-size y-size x-orig y-orig level 
                      &key (unmarked nil) (complete t)

Given samples (a 2D array of float values representing dose absorption
  information on a regular grid of lattice points), x-size and y-size
  both floats defining the size of each dimension of the grid in patient
  space), x-orig and y-orig (defining the patient space location of the
  x and y coordinates of the (0,0) entry of the grid), and level (a float 
  which represents the isodose threshold at which to extract curves), this 
  routine computes and returns a list of lists of coordinate pairs, 
  each list of coordinate pairs representing the vertices of the a segment
  of the set of isodose lines running through the samples array at the
  supplied threshold.

  The caller may optionally supply an 'unmarked' keyword, which must be
  a 2D array of t/nil entries whose dimensions are at least as large as 
  the samples array; this unmarked array is used internally by the function.
  If omitted, an unmarked array will be dynamically allocated automatically,
  each time this routine is called.  This allocation reduces the efficiency 
  of this function, and it is intended that the caller explicitly allocate 
  an unmarked array once and then reuse it on successive calls to this 
  function for different levels.

  If the 'complete' keyword is true (its default), then the complete
  set of isodose curves is returned, unaffected by the presence of 0.0's
  in the samples matrix.  If this keyword is nil, then this routine
  does not consider points on an isolevel line which border areas 
  of zero level to be part of the resulting isodose curve; these areas 
  are not included in the returned polylines and/or contours.  This is at
  the request of the dosimetrists who don't want to see multiple contour 
  lines piled up on top of each other near the skin surface."

  (let* ((curves   nil)
         (last-i   (1- (array-dimension samples 0)))
         (last-j   (1- (array-dimension samples 1))))

    (declare (type (simple-array single-float 2) samples))
    (declare (fixnum last-i last-j))

    ;; if no unmarked array, create one dynamically

    (unless unmarked
      (setq unmarked (make-array (array-dimensions samples) 
                       :element-type '(member t nil))))

    ;; initialize all entries of unmarked array to t

    (initialize-unmarked-array unmarked (1+ last-i) (1+ last-j))

    ;; Search algorithm to find isolevel curves in samples: check consecutive
    ;; elements of samples along bottom row, right column, top row, and left 
    ;; column to see if an isolevel contour crosses enters the samples array
    ;; from the outside.  Then check remaining adjacent elements of samples
    ;; array, row by row, to find isolevel curves completely inside the array.
    ;; If an isolevel curve is found between two adjacent array entries at 
    ;; any time, follow that curve through the array and, when finished, add 
    ;; the curve to the list to be returned.  This list consists of 2-tuples, 
    ;; of the format (status vertex-list) where status is one of :open or
    ;; :closed.

    ;;---------- check bottom row, left to right

    (do ((i 0 (1+ i))) ((= i last-i))
      (when (found-new-contour unmarked samples level i 0 (1+ i) 0)
        (dolist (s (follow-contour 
                     unmarked samples level i 0 (1+ i) 0
                     :checking :top :complete complete))
          (push s curves))))

    ;;---------- check right column, bottom to top

    (do ((j 0 (1+ j))) ((= j last-j))
      (when (found-new-contour unmarked samples level last-i j last-i (1+ j))
        (dolist (s (follow-contour 
                     unmarked samples level last-i (1+ j) last-i j 
                     :checking :left :complete complete))
          (push s curves))))

    ;;---------- check top row, right to left

    (do ((i last-i (1- i))) ((zerop i))
      (when (found-new-contour unmarked samples level i last-j (1- i) last-j)
        (dolist (s (follow-contour 
                     unmarked samples level (1- i) last-j i last-j 
                     :checking :bottom :complete complete))
          (push s curves))))

    ;;---------- check left column, top to bottom

    (do ((j last-j (1- j))) ((zerop j))
      (when (found-new-contour unmarked samples level 0 j 0 (1- j)) 
        (dolist (s (follow-contour 
                     unmarked samples level 0 j 0 (1- j) 
                     :checking :right :complete complete))
          (push s curves))))

    ;;---------- check remaining rows

    (do ((j 1 (1+ j))) ((= j last-j))
      (dotimes (i last-i)
        (when (found-new-contour unmarked samples level i j (1+ i) j)
          (dolist (s (follow-contour 
                        unmarked samples level i j (1+ i) j 
                        :checking :top :complete complete))
            (push s curves)))))

    ;; filter out curves with less than 3 vertices on them - it is possible,
    ;; though extremely rare, that follow-contour such curves will return
    ;; such curves, if samples array values are encountered that are exactly
    ;; equal to the supplied threshold.

    (setq curves 
      (remove-if-not #'(lambda (curve) (rest (rest (second curve)))) curves))

    ;; scale curves from 'array space' to patient space

    (setq curves 
      (normalize-isodose-curves curves samples x-size y-size x-orig y-orig))

    ;; duplicate first point to last of each curve if it is closed.

    (mapcar #'(lambda (curve)
                (case (first curve)
                  (:open (second curve))
                  (:closed (append 
                             (second curve)
                             (list 
                               (first (second curve)) 
                               (second (second curve)))))))
      curves)
))

;;;---------------------------------------------
