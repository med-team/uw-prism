;;;
;;; point-dose-panels
;;;
;;; The Prism point-dose panel class definition and associated functions.
;;;
;;; 21-Apr-1994 J. Unger created.
;;; 22-Apr-1994 J. Unger lots more work.
;;; 05-May-1994 J. Unger change valid to valid-points, add compute-dose btn.
;;; 01-Jun-1994 J. Unger add-notify changes to beam names, elim extra code,
;;; other misc modifications.
;;; 05-Jun-1994 J. Unger fix bugs - typing into empty textlines and such.
;;; 17-Jun-1994 J. Unger add pt ID's to name textline, make name textline
;;;             bigger, sort point lines by ID, two rows of beam tlns.
;;; 13-Jul-1994 J. Unger fix bug - mu's wouldn't update when chg frac btn.
;;; 26-Jul-1994 J. Unger add :numeric & limits to numerical textline defs.
;;; 29-Aug-1994 J. Unger fix bug - crash when press down arrow w/ empty pnl
;;; 30-Aug-1994 J. Unger remove code to sort points - list should always be
;;; in correct order now.
;;; 11-Sep-1994 J. Unger increase volatile border width interior textlines.
;;; 18-Sep-1994 J. Unger add omitted remove-notify for beam names.  Also
;;;             fix bug in beam name drawing that caused names not to get
;;;             updated properly when a beam was deleted.
;;; 12-Jan-1995 I. Kalet destroy comp-dose button also.  Get and cache
;;; plan and patient as passed parameters.
;;;  8-Jun-1997 I. Kalet use new SLIK widget, icon-button, with
;;;  make-arrow-button fn.
;;; 27-Feb-1998 I. Kalet strip down, use new spreadsheet widget in SLIK.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;; 21-Oct-1999 I. Kalet protect against entering a zero total dose
;;; for a point.
;;; 11-May-2000 I. Kalet parametrize lower and upper dose input limits
;;; 16-Dec-2000 I. Kalet add plan name to title of panel.
;;;  5-May-2002 I. Kalet handle possibility of button off event (info=0)
;;;  2-Nov-2003 I. Kalet remove use of reader macro #. in *pdp-cells*
;;; to allow compile without first loading
;;;  1-Dec-2003 I. Kalet use backquote in array initialization instead
;;; of quote, to allow eval of parameters.
;;;

(in-package :prism)

;;;---------------------------------------------

(defvar *pdp-row-heights*
  (append '(30 50) (make-list 13 :initial-element 30))
	  "The second row is a little bigger.")

;;;---------------------------------------------

(defvar *pdp-col-widths* '(160 10 90 40 90 90 90 90 40))

;;;---------------------------------------------

(defparameter *pdp-dose-min* 0.0)
(defparameter *pdp-dose-max* 10000.0)

;;;---------------------------------------------

(defvar *pdp-cells*
  (make-array '(15 9)
	      :initial-contents
	      `(((:button "Delete Panel") nil
		 (:button "Compute" nil nil :button-type :momentary)
		 nil nil (:label "Beams") nil nil nil)
		(nil nil nil
		 (:left-arrow nil nil nil :fg-color sl:red)
		 (:label "") (:label "")
		 (:label "") (:label "")
		 (:right-arrow nil nil nil :fg-color sl:red))
		;; the monitor units row
		((:label "Points") nil
		 (:button "ALL FRAC" nil nil :button-type :momentary)
		 (:label "MU")
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:up-arrow nil nil nil :fg-color sl:red)
		 nil (:label "Dose") nil nil nil nil nil nil)
		;; ten rows of point doses
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:label "") nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 (:number nil ,*pdp-dose-min* ,*pdp-dose-max*)
		 nil)
		((:down-arrow nil nil nil :fg-color sl:red)
		 nil nil nil nil nil nil nil nil))))

;;;---------------------------------------------

(defclass point-dose-panel (generic-panel)

  ((fr :accessor fr
       :documentation "The SLIK spreadsheet panel that contains
all the control buttons, name cells, data cells and arrow buttons.")

   (plan :type plan
         :accessor plan
         :initarg :plan
         :documentation "The plan for this point-dose panel.")
   
   (pat :accessor pat
	:initarg :pat
	:documentation "The current patient.")

   (mode-factor :type single-float
                :accessor mode-factor
                :initform 1.0
                :documentation "A cached multiplicative factor for
computation of dose and mu per fraction -- equals 1.0 when display
mode is ALL FRAC, or the inverse of the number of treatments when
display mode is ONE FRAC.")

   (beam-pos :type fixnum
	     :accessor beam-pos
	     :initform 0
	     :documentation "The position in the plan's collection of
beams of the beam currently in the first beam column in the point dose
panel spreadsheet.")

   (point-pos :type fixnum
	      :accessor point-pos
	      :initform 0
	      :documentation "The position in the patient point list
of the point in the first row of the points part of the point dose
panel spreadsheet.")

   )

  (:documentation "The point-dose panel contains a table which
displays the dose at each defined point of interest under each beam,
and provides a mechanism for the user to specify dose to particular
points and monitor units to beams.")

  )

;;;---------------------------------------------

(defun make-point-dose-panel (&rest initargs)

  "make-point-dose-panel &rest initargs

Creates and returns a point-dose panel with the specified initargs."

  (apply #'make-instance 'point-dose-panel
	 ;; :font sl:helvetica-bold-18
	 initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((pdp point-dose-panel)
				       &rest initargs)

  "Initializes the user interface for the point-dose panel."

  (let* ((pln (plan pdp))
	 (frm (apply #'sl:make-spreadsheet
		     *pdp-row-heights* *pdp-col-widths*
		     *pdp-cells*
		     :title (format t "Point Dose Panel for ~A"
				    (name pln))
		     initargs)))
    (setf (fr pdp) frm)
    (display-beam-names pdp)
    (display-mu pdp)
    (display-point-names pdp)
    (if (valid-points (sum-dose pln)) (display-all-doses pdp))
    ;; register with panel user input event
    (ev:add-notify pdp (sl:user-input frm)
		   #'(lambda (pan sheet i j info)
		       (let* ((bmlist (coll:elements
				       (beams (plan pan))))
			      (lastcol (min (+ 4 (- (length bmlist)
						    (beam-pos pan)))
					    8))
			      (pts (coll:elements (points (pat pan))))
			      (lastrow (min (+ 4 (- (length pts)
						    (point-pos pan)))
					    14)))
			 (cond ((and (= i 0) (= j 0))
				(when (= info 1) (destroy pan)))
			       ((and (= i 0) (= j 2))
				(when (= info 1)
				  (compute-dose-points (plan pan) (pat pan))))
			       ((and (= i 2) (= j 2))
				(when (= info 1) (switch-mu-frac pan i j)))
			       ((and (= i 2) (> j 3) (< j lastcol))
				;; user entered new MU
				(setf (monitor-units
				       (nth (+ j -4 (beam-pos pan))
					    bmlist))
				  (coerce (/ info (mode-factor pan))
					  'single-float)))
			       ((and (> i 3) (< i lastrow) (= j 2))
				;; user entered new total dose for a point
				(new-dose-total pan i info))
			       ((and (> i 3) (< i lastrow)
				     (> j 3) (< j lastcol))
				;; user entered new point dose from beam
				(new-beam-dose pan i j info))
			       ;; arrow buttons
			       ((and (= i 1) (= j 3)) ;; left arrow
				(beam-scroll pan (case info
						   (1 -1)
						   (2 -4))))
			       ((and (= i 1) (= j 8)) ;; right arrow
				(beam-scroll pan (case info
						   (1 1)
						   (2 4))))
			       ((and (= i 3) (= j 0)) ;; up arrow
				(point-scroll pan (case info
						    (1 -1)
						    (2 -10))))
			       ((and (= i 14) (= j 0)) ;; down arrow
				(point-scroll pan (case info
						    (1 1)
						    (2 10))))
			       ;; no other cases
			       (t (sl:acknowledge "That cell is empty")
				  (sl:erase-contents sheet i j))))))
    ;; register changes to beam names and beam MU's - note that MU
    ;; change does not affect valid flag so must handle it here.
    (dolist (b (coll:elements (beams pln)))
      (ev:add-notify pdp (new-name b) 
		     #'(lambda (pan bm newname)
			 (let ((pos (- (position bm (coll:elements
						     (beams (plan pan))))
				       (beam-pos pan))))
			   (when (and (>= pos 0) (< pos 4))
			     (sl:set-contents (fr pan) 1 (+ pos 4)
					      newname)))))
      (ev:add-notify pdp (new-mu b) 
		     #'(lambda (pan bm newmu)
			 (let ((pos (+ 4 (- (position bm (coll:elements
							  (beams
							   (plan pan))))
					    (beam-pos pan)))))
			   (when (and (> pos 3) (< pos 8))
			     (sl:set-contents (fr pan) 2 pos
					      (format nil "~6,1F"
						      (* (mode-factor pan)
							 newmu)))
			     (when (valid-points (sum-dose (plan pan)))
			       (display-beam-doses bm pan pos)))
			   (when (valid-points (sum-dose (plan pan)))
			     (display-total-doses pan))))))
    ;; register with status change to the plan's dose result
    (ev:add-notify pdp (points-status-changed (sum-dose pln))
		   #'(lambda (pan a v)
		       (declare (ignore a v))
		       (if (valid-points (sum-dose (plan pan)))
			   (display-all-doses pan)
			 (erase-all-doses pan))))
    ))

;;;---------------------------------------------

(defmethod destroy :before ((pdp point-dose-panel))

  "Releases X resources used by this panel and removes event notifies
where needed."

  (ev:remove-notify pdp (points-status-changed
			 (sum-dose (plan pdp))))
  (dolist (b (coll:elements (beams (plan pdp))))
    (ev:remove-notify pdp (new-name b))
    (ev:remove-notify pdp (new-mu b)))
  (sl:destroy (fr pdp)))

;;;---------------------------------------------

(defun display-beam-names (panel)

  (let ((col 3)
	(sheet (fr panel)))
    (dolist (bm (nthcdr (beam-pos panel)
			(coll:elements (beams (plan panel)))))
      (if (< (incf col) 8) ;; don't go too far to the right!
	  (sl:set-contents sheet 1 col (name bm))))))

;;;---------------------------------------------

(defun display-point-names (panel)

  (let ((row 3)
	(sheet (fr panel)))
    (dolist (pt (nthcdr (point-pos panel)
			(coll:elements (points (pat panel)))))
      (if (< (incf row) 14) ;; don't go past the bottom!
	  (sl:set-contents sheet row 0 ;; write number with name
			   (format nil "~2@A. ~A"
				   (id pt) (name pt)))))))

;;;---------------------------------------------

(defun display-mu (panel)

  (let ((col 3)
	(sheet (fr panel))
	(mode-fac (mode-factor panel)))
    (dolist (bm (nthcdr (beam-pos panel)
			(coll:elements (beams (plan panel)))))
      (if (< (incf col) 8) ;; don't go off the end!
	  (sl:set-contents sheet 2 col
			   (format nil "~6,1F"
				   (* mode-fac (monitor-units bm))))))))

;;;---------------------------------------------

(defun display-beam-doses (bm pan col)

  (let ((mu (monitor-units bm))
	(sheet (fr pan))
	(mode-fac (mode-factor pan))
	(row 3))
    (dolist (pt (nthcdr (point-pos pan) (points (result bm))))
      (if (< (incf row) 14)
	  (sl:set-contents sheet row col
			   (format nil "~6,1F" (* mode-fac mu pt)))))))

;;;---------------------------------------------

(defun display-total-doses (panel)

  "this function just has to compute the scaled total dose from the
plan's dose result."

  (let ((sheet (fr panel))
	(mode-fac (mode-factor panel))
	(row 3))
    (dolist (pt (nthcdr (point-pos panel)
			(points (sum-dose (plan panel)))))
      (if (< (incf row) 14)
	  (sl:set-contents sheet row 2
			   (format nil "~6,1F" (* mode-fac pt)))))))

;;;---------------------------------------------

(defun display-all-doses (panel)

  "this function does both the individual beams and the totals."

  (let ((col 3))
    (dolist (bm (nthcdr (beam-pos panel)
			(coll:elements (beams (plan panel)))))
      (if (< (incf col) 8) (display-beam-doses bm panel col))))
  (display-total-doses panel))

;;;---------------------------------------------

(defun switch-mu-frac (panel i j)

  (let ((sheet (fr panel))
	(beams (coll:elements (beams (plan panel))))
	(doses (valid-points (sum-dose (plan panel)))))
    (if (string-equal (sl:contents sheet i j) "ONE FRAC")
	(progn
	  (sl:set-contents sheet i j "ALL FRAC")
	  (setf (mode-factor panel) 1.0)
	  (display-mu panel)
	  (if doses (display-all-doses panel))) ;; and update the display!
      ;; need to check first on this one, if it is possible
      (if (apply #'= (mapcar #'n-treatments beams))
	  (progn
	    (sl:set-contents sheet i j "ONE FRAC")
	    (setf (mode-factor panel) (/ 1.0 (n-treatments
					      (first beams))))
	    (display-mu panel)
	    (if doses (display-all-doses panel))) ;; and update the display!
	(progn
	  (sl:acknowledge '("Cannot change to single fraction"
			    "Not all beams have same number of fractions."))
	  (sl:set-button sheet i j nil))))))

;;;---------------------------------------------

(defun new-beam-dose (panel row col info)

  "this function should compute and set the MU for one beam."

  (let* ((bm (nth (+ col -4 (beam-pos panel))
		  (coll:elements (beams (plan panel)))))
	 (dose-per-mu (if (valid-points (result bm))
			  (nth (+ row -4 (point-pos panel))
			       (points (result bm))))))
    (if dose-per-mu ;; result exists!
	(if (zerop dose-per-mu)
	    (progn ;; it's zero, can't change dose!
	      (sl:acknowledge '("Zero dose per MU"
				"You cannot set this"))
	      (sl:set-contents (fr panel) row col 0.0))
	  (setf (monitor-units bm)
	    (coerce (/ info (* (mode-factor panel) dose-per-mu))
		    'single-float)))
      (progn
	(sl:acknowledge "No point dose result for this beam")
	(sl:erase-contents (fr panel) row col)))))

;;;---------------------------------------------

(defun new-dose-total (panel row info)

  "this function should compute and set all the MU for all the beams."

  (if (zerop info)
      (progn
	(sl:acknowledge "You cannot set this dose to zero.")
	(sl:erase-contents (fr panel) row 2))
    (if (valid-points (sum-dose (plan panel)))
	(let ((dose (nth (+ row -4 (point-pos panel))
			 (points (sum-dose (plan panel))))))
	  (if (zerop dose)
	      (progn
		(sl:acknowledge '("Dose got set to zero."
				  "Please adjust MU first."))
		(sl:erase-contents (fr panel) row 2))
	    (let ((ratio (/ info (* (mode-factor panel) dose))))
	      (dolist (bm (coll:elements (beams (plan panel))))
		(setf (monitor-units bm)
		  (* ratio (monitor-units bm)))))))
      (progn
	(sl:acknowledge "No point dose results yet")
	(sl:erase-contents (fr panel) row 2)))))

;;;---------------------------------------------

(defun erase-all-doses (panel)

  (let ((sheet (fr panel)))
    (dotimes (i 10)
      (let ((row (+ i 4)))
	(sl:erase-contents sheet row 2)
	(dotimes (j 4)
	  (sl:erase-contents sheet row (+ j 4)))))))

;;;---------------------------------------------

(defun erase-names-mu (panel)

  (let ((sheet (fr panel)))
    (dotimes (i 4)
      (sl:erase-contents sheet 1 (+ i 4)) ;; beam names
      (sl:erase-contents sheet 2 (+ i 4))) ;; beam MU
    (dotimes (i 10)
      (sl:erase-contents sheet (+ i 4) 0)))) ;; point names

;;;---------------------------------------------

(defun pdp-refresh (panel)

  (erase-names-mu panel)
  (erase-all-doses panel)
  (display-beam-names panel)
  (display-point-names panel)
  (display-mu panel)
  (if (valid-points (sum-dose (plan panel)))
      (display-all-doses panel)))

;;;---------------------------------------------

(defun beam-scroll (panel amt)

  (when amt ;; could be nil - see case above
    (let ((tmp (+ (beam-pos panel) amt))
	  (bmlist (coll:elements (beams (plan panel)))))
      (when (and (>= tmp 0) (< tmp (length bmlist)))
	(setf (beam-pos panel) tmp)
	(pdp-refresh panel)))))

;;;---------------------------------------------

(defun point-scroll (panel amt)

  (when amt ;; could be nil - see case above
    (let ((tmp (+ (point-pos panel) amt))
	  (ptlist (coll:elements (points (pat panel)))))
      (when (and (>= tmp 0) (< tmp (length ptlist)))
	(setf (point-pos panel) tmp)
	(pdp-refresh panel)))))

;;;---------------------------------------------
;;; End.
