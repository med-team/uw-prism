;;;
;;; mlc.cl
;;;
;;; Functions for working with multileaf collimators
;;;
;;; 22-Jul-1994 J. Jacky Complete compute-mlc, compute-mlc-vertices + support
;;; 26-Jul-1994 J. Unger modify set-leaf-pair slightly to avoid wierd bug.
;;; 27-Jul-1994 J. Jacky Complete compute-vj-block-vertices
;;; 29-Jul-1994 J. Jacky Complete compute-vj-block, fiddle with coincidences
;;;  1-Aug-1994 J. Jacky compute-mlc-vertices: strip closed leaves,
;;;  redund pts
;;; 07-Aug-1994 J. Unger change name of remove-repeats (now in poly pkg and
;;; is called remove-adjacent-redundant-vertices).  Also add postprocessing
;;; to compute-vj-block-vertices and compute-vj-block to clean up returned 
;;; vertex lists some.
;;; 12-Aug-1994 J. Jacky fix compute-step-ys for 0, 1 or 2 open leaf pairs
;;; 15-Aug-1994 J. Jacky fix error computing last leaf step in
;;; compute-step-ys
;;; 23-Aug-1994 J. Jacky delete hopeless compute-step-ys, add find-centers
;;; replace centerline-list with edge-list throughout
;;; 28-Oct-1994 J. Unger add some contour cleanup code to compute-mlc-verts
;;; 15-Nov-1994 J. Jacky Fix bug where it back-computed portal contour
;;; did not match leaf settings typed in by user, due to make-notch
;;; placing notch at interior side of concave portal (for example
;;; against a midline block). make-notch now chooses shallowest
;;; connector of all -- changed make-notch and most.  Fortuitously
;;; this change also helps prevent leaves assigned by system from
;;; "creeping" 1 mm from values typed in by user *even without*
;;; contemplated decrease of dj,dn in compute-vj-block.
;;; 17-Nov-1994 J. Jacky ...but not always.  Today in compute-vj-block
;;; change dj, dn from 0.1, 0.05 to 0.03, 0.01
;;; 24-Jan-1997 I. Kalet portal function now returns just the
;;; vertices.
;;;  1-Mar-1997 I. Kalet update calls to nearly- functions
;;; 03-Jul-1997 BobGian updated nearly-xxx -> poly:nearly-xxx .
;;; 07-Jul-1997 BobGian mlc-post-process -> poly:canonical-contour
;;;   (same functionality, but is a utility in polygons system).
;;; 09-Jul-1997 BobGian added commentary about results of processing
;;;   degenerate (zero-area) contours.
;;;  2-Oct-1997 BobGian tighten coding of compute-mlc-vertices.  Sprinkle
;;;   debugging code around.  Bug fixed, debug code removed 7-Oct-1997.
;;; 14-Oct-1997 BobGian comment vertex-list-difference's assumptions about
;;;   orientation guarantees on its input contours [namely, none].
;;;  5-Sep-1999 I. Kalet move get-mlc-vertices here from mlc-panels,
;;; formerly leaf-panels.  Used in charts and write-neutron also.
;;;

(in-package :prism)

;;;---------------------------------------------

(defun get-mlc-vertices (bm)

  "get-mlc-vertices bm

For beams with a collimator of type multileaf-coll, returns the
vertices attribute of the collimator.  For beams with a collimator of
type cnts-coll, calls compute-vj-block-vertices to get a collimator
outline computed from the collim jaws minus the blocks.  Returns nil
for any other type of collimator."

  (let ((coll (collimator bm)))
    (typecase coll
      (multileaf-coll (vertices coll))
      (cnts-coll (poly:rotate-vertices
		  (compute-vj-block-vertices
		   coll (coll:elements (blocks bm)))
		  (collimator-angle bm)))
      (t nil))))

;;;--------------------------------------------------

(defun compute-mlc (collimator-angle vertices edge-list)
  
  "compute-mlc collimator-angle vertices edge-list

Returns leaf-settings for leaves defined by edge-list that match
portal shape (in gantry system) defined by vertices when collimator is
rotated to collimator-angle."

  (let ((centerline-list (find-centers edge-list))
	(r-vertices (poly:rotate-vertices vertices (- collimator-angle))))
    (mapcar #'(lambda (centerline) (set-leaf-pair centerline r-vertices))
	    centerline-list)))

;;;------------------------------

(defun find-centers (edge-list)

  "find-centers edge-list

Return list of n centers defined by n+1 edges in edge-list"

  (let ((lower-edge-list (rest edge-list)))
    (mapcar #'(lambda (yu yl) (/ (+ yu yl) 2.0))
	    edge-list lower-edge-list)))

;;;------------------------------

(defun set-leaf-pair (y vertices)
  
  "set-leaf-pair y vertices

Returns list of two x-coords: lower, upper leaf settings at y coord
that touch opposite sides of polygon defined by vertices."

;; NOTE: If the (list 0.0 0.0) below is replaced with '(0.0 0.0), then
;; strange results sometimes occur when the function is evaluated.
;; In particular, the function seems to return the wrong clause of 
;; the 'if' form if xs is nil.

  (let ((xs (crossings y vertices)))
    (if (null xs) (list 0.0 0.0)
      (list (apply #'min xs) (apply #'max xs)))))

;;;------------------------------

(defun crossings (y vertices)

  "crossings y vertices

Returns list of x-coords where polygon defined by vertices crosses y
coord."

  ;; Tried mapcan but got only NIL back, so did (remove nil ...)
  ;; instead
  (remove nil (mapcar #'(lambda (seg) (cross-x y seg))
		      (segments vertices))))

;;;------------------------------

(defun segments (vertices)

  "segments vertices

For list of (xi yi) vertices, returns list of ((xi yi) (xi+1 yi+1))
segments."

  ;; cons because we have to make a special case of the closing segment
  (cons (list (car (last vertices))	; (last xs) is (x) not x
	      (first vertices))
	(mapcar #'(lambda (x y) (list x y)) vertices (cdr vertices))))

;;;------------------------------

(defun cross-x (y seg)

  "cross-x y seg

Returns x coordinate where y crosses seg ((x1 y1) (x2 y2)), or NIL if
no crossing."

  ;; handling this special case here is easier than using stuff in
  ;; polys

  (let* ((seg1 (first seg)) (seg2 (second seg)) 
	 (y1 (second seg1)) (y2 (second seg2)))
    (if (or (<= y1 y y2) (>= y1 y y2))	; crossing found
	(let ((x1 (first seg1)) (x2 (first seg2))
	      (fr (if (poly:nearly-equal y1 y2)
		      0.0
		    (/ (- y y1) (- y2 y1)))))
	  (+ x1 (* fr (- x2 x1))))	; linear interpolation
      nil)))

;;;------------------------------

(defun compute-mlc-vertices (collimator-angle leaf-settings edge-list)

  "compute-mlc-vertices collimator-angle leaf-settings edge-list

Returns vertices of portal shape (in gantry system) formed by
leaf-settings and edge-list rotated to collimator-angle.
Returns nil for degenerate (zero-area) portal contour."

  (let* ((open-field (remove-closed-ends leaf-settings edge-list))
	 (open-leaves (first open-field))
	 (open-edges (second open-field)))
    (poly:rotate-vertices
     ;; strip out any duplicate vertices at adjacent leaf corners
     (poly:canonical-contour		; trace down lower X side, then up
      (append
       (compute-steps (mapcar #'first open-leaves) open-edges)
       (reverse (compute-steps (mapcar #'second open-leaves) open-edges))))
     collimator-angle)))

;;;--------------------------------

(defun remove-closed-ends (leaf-settings edge-list)

  "remove-closed-ends leaf-settings edge-list

Returns a list of two lists: new leaf-settings and edge-list.  They
are like the input except without the entries for leaves at the +y and
-y ends of the field that leaf-settings indicates are closed.  Entries
remain for any interior closed leaves, like midline blocks."

  (let ((frontless (remove-closed-front leaf-settings edge-list)))
    (mapcar #'reverse
	    (apply #'remove-closed-front
		   (mapcar #'reverse frontless)))))

;;;--------------------------------

(defun remove-closed-front (leaf-settings edge-list)

  "remove-closed-front leaf-settings edge-list

Half of remove-closed-ends --- remove closed leaves from front of
list."

  (let* ((first-leaf-pair (first leaf-settings))
	 (xl (first first-leaf-pair))	; lower leaf setting 	 
	 (xu (second first-leaf-pair)))	; upper leaf setting 
    (if (< xl xu)			; if first leaf pair is open,
 	(list leaf-settings edge-list)	; return
      (remove-closed-front (rest leaf-settings) (rest edge-list)))))

;;;----------------------------------

(defun compute-steps (xs edge-ys)

  "compute-steps xs ys

Return stepped polyline defined by list of x-coords xs and y-coords
edge-ys."

  (let* ((l-edge-ys (rest edge-ys))	; edge-ys must have one more
					; elt than xs
	 (steps (mapcar #'(lambda (x yu yl)
			    (list (list x yu) (list x yl)))
			xs edge-ys l-edge-ys))) ; each step has 1 x
					; but 2 y's
    (apply #'append steps)))		; flatten out top level of lists

;;;----------------------------------

(defun compute-vj-block-vertices (vj-coll blocks)

  "compute-vj-block-vertices vj-coll blocks

Returns vertices of the portal shape in the collimator system formed
by the four independent jaws of vj-coll and the list (not collection)
of blocks (instances of beam-block)."

  (let ((portal-vs (portal vj-coll)))
    ;; Subtract all block contours, and then post process
    ;; NB: Returns nil if portal-vs contains a degenerate portal
    ;; contour (zero area) - this should never happen.
    (dolist (blk blocks (poly:canonical-contour portal-vs))
      ;; vertex-list-difference returns a list of lists.  It also makes
      ;; NO assumptions about orientation of input contours, by virtue
      ;; of non-supplied optional 4th argument.
      (setq portal-vs
	    (first (poly:vertex-list-difference portal-vs
						(vertices blk)))))))

;;;------------------------------------

(defun compute-vj-block (c-vertices)

  "compute-vj-block c-vertices

Returns a list of two items: first, a variable-jaw-coll, and second, a
list of vertices that define the shape of a single C-shaped block,
that together match the interior portal shape defined by c-vertices."

  ;; Adjusting the collimator settings and notch cutout by dj and dn
  ;; are *essential* parts of this routine!  They are necessary because
  ;; the vertex-list-difference routine *cannot* handle situations
  ;; where vertices or segments in the two contours are coincident
  ;; (vertices coincide, or pieces of segments coincide, or a vertex
  ;; from one one lands exactly on a segment from the other).   If
  ;; coincidences are present, the routine sometimes crashes, hangs, or
  ;; returns garbage --- either here in compute-vj-block, or later when
  ;; we pass the results of this routine to compute-vj-block-vertices.
  ;; Our inelegant solution is to simply perturb the computed contours
  ;; by dj and dn to ensure they will not coincide.  It is essential
  ;; that dj and dn be *different* from each other.  Initially I chose
  ;; dj=0.1 (1 mm) and dn=0.05; smaller numbers might work as well.

  (let* ((box (poly:bounding-box c-vertices))
	 (llc (first box)) (llc-x (first llc)) (llc-y (second llc))
	 (urc (second box)) (urc-x (first urc)) (urc-y (second urc))
	 (dj 0.03)			; expand jaws to avoid coincidences
	 (dn 0.01)			; expand notch, but different amount
	 (margin 1.0)			; width of C-block
	 (vj-coll (make-instance 'variable-jaw-coll
				 :x-inf (- (- llc-x dj))
				 :y-inf (- (- llc-y dj))
				 :x-sup (+ urc-x dj)
				 :y-sup (+ urc-y dj)))
	 (c-blk (let* ((vj-portal (list (list llc-x llc-y)
					(list llc-x urc-y)
					(list urc-x urc-y)
					(list urc-x llc-y)))
		       (border (poly:ortho-expand-contour vj-portal margin))
		       ;; Make our own notch because cut annulus calc'ed
		       ;; by vertex-list-difference not quite right
		       ;; and crashes.
		       (notch (make-notch border c-vertices))
		       ;; If we don't expand notch in both directions, 
		       ;; vertex-list-difference returns nil.
		       (bigger-notch (poly:ortho-expand-contour notch dn))
		       (notched-border (first (poly:vertex-list-difference 
					       border bigger-notch))))
		  (first (poly:vertex-list-difference notched-border 
						      c-vertices)))))
    ;; Note that vertex-list-difference in both usages above makes no
    ;; assumptions about orientation of its input contours [due to optional
    ;; fourth argument defaulting to nil].
    ;; NB: poly:canonical-contour returns nil if c-blk contains a
    ;; degenerate portal contour (zero area) - this should never happen.
    (list vj-coll (poly:canonical-contour c-blk))))

;;;--------------------------------------

(defun make-notch (border interior)
  
  "make-notch border interior

Given two vertex lists, where border is a rectangle parallel to the
axes that completely encloses interior, return the vertex list of the
shallowest quadrilateral notch connecting an entire interior segment
with the border."

  (let* ((connectors			; list of lists of connectors
	  (mapcar #'(lambda (seg)	; outer level is one list per
					; interior seg
		      (mapcar #'(lambda (side)
				  (make-connector seg side))
			(segments border))) ; inner level is one
	    (segments interior)))	; connector per border seg
	 (shallow-connectors (mapcar #'(lambda (side) (most '< side))
			       connectors))) ; return list of
					; shallow connectors
    (caddr (most '< shallow-connectors)))) ; dig out shallowest
					; connector from list created
					; by make-connector

;;;-----------------------------------------

(defun most (r ls)

  "most r ls

Return element of list-of-lists ls whose car is ``most'' according to
binary relation predicate r.  For example If r is <, most is
smallest."

  ;; very un-functional - is there a Lisp-ier way?
  (let ((er (first ls)))
    (dolist (e (rest ls) er)
      (if (funcall r (first e) (first er))
	  (setq er e)))))

;;;-----------------------------------------

(defun make-connector (shortseg longseg)

  "make-connector shortseg longseg

Make trapezoidal vertex list where one side is shortseg and the
opposide side is the projection of shortseg on longseg.  Longseg must
be parallel to one of the coordinate axes and shortseg's projection
must fit within longseg."

;;; Returns a list: first element is depth of connector, second is its
;;; length, third and last element is connector itself.  It's easiest
;;; to do all this boring brute force arithmetic in one place.

  (let* ((s1 (first shortseg)) (s1x (first s1)) (s1y (second s1))
	 (s2 (second shortseg)) (s2x (first s2)) (s2y (second s2))
	 (l1 (first longseg)) (l1x (first l1)) (l1y (second l1))
	 (l2 (second longseg)) (l2y (second l2))) ; l2x never needed
    (if (poly:nearly-equal l1y l2y 0.01) 
	;; longseg parallel to x-axis
	(list (max (abs (- s1y l1y)) (abs (- s2y l1y))) ; depth 
	      (abs (- s1x s2x))		; length
	      (list s1 (list s1x l1y) (list s2x l1y) s2)) ; connector itself
      ;; longseg parallel to y-axis
      (list (max (abs (- s1x l1x)) (abs (- s2x l1x))) ; depth 
	    (abs (- s1y s2y))		; length
	    (list s1 (list l1x s1y) (list l1x s2y) s2))))) ; connector itself

;;;------------------------------
