;;;
;;; bev-draw-all
;;;
;;; contains the bev-draw-all function in order to break circularity
;;; through beam-mediators.
;;;
;;; 10-May-1997 I. Kalet created
;;;  2-Dec-2000 I. Kalet take out display-view - redundant call
;;;

(in-package :prism)

;;;---------------------------------------------

(defun bev-draw-all (bev pln pat &optional omit)

  "bev-draw-all bev pln pat &optional omit

draws the organs etc. from pat, beams from pln into view bev, omitting
the object specified by omit, either a beam or a block, then refreshes
the view pixmap."

  (compute-pstruct-transform bev)
  ;; create all the primitives from all the objects
  (dolist (tar (coll:elements (targets pat))) (draw tar bev))
  (dolist (tum (coll:elements (findings pat))) (draw tum bev))
  (dolist (org (coll:elements (anatomy pat))) (draw org bev))
  (dolist (pt (coll:elements (points pat))) (draw pt bev))
  (dolist (bm (coll:elements (beams pln)))
    (unless (eq omit bm)
      (draw bm bev)
      (dolist (blk (coll:elements (blocks bm)))
	(unless (eq omit blk)
	  (draw-beam-block blk bev bm))))))

;;;----------------------------------------------
