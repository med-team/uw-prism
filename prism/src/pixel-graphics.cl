;;;
;;; pixel-graphics
;;;
;;; defines some low level functions for pixel/real space transforms
;;; etc.
;;;
;;; 13-Oct-1992 I. Kalet put cm to pixel transforms here
;;; 22-Apr-1994 I. Kalet move pix-x and pix-y here from
;;; dose-grid-mediators
;;;  4-May-1994 I. Kalet add coerce single-float to cm-x and cm-y
;;;  8-Jan-1995 I. Kalet remove proclaim optimize form
;;;  5-Sep-1995 I. Kalet remove proclaim inline also - it is ignored.
;;;  Add declarations, use pix-x and pix-y in the other functions.
;;;  Move pixel-segments and compute-tics here to keep module
;;;  dependencies in order, rewrite some code for speed.
;;;  8-Oct-1996 I. Kalet move clipping code here from beam-graphics,
;;;  also draw-plus-icon and draw-diamond-icon, which can be used
;;;  other places.
;;; 20-Jun-1997 BobGian fixed clipping code:
;;;   Removed declarations for vars which won't work in macros because others
;;;   will be substituted during macro-expansion; installed THE to declare
;;;   types of inputs/outputs.  Removed COERCE when result must already be
;;;   of appropriate type.  Simplified CLIP, OUTCODE, and CUT to make more
;;;   understandable (and maybe a hair faster).
;;; 26-Jun-1997 BobGian CLIP -> CLIP-FIXNUM in SCALE-AND-CLIP-LINES.
;;;   Converted polymorphic and internally-consing CLIP to specialized
;;;   argument-type specific and non-consing CLIP-FIXNUM, CLIP-FLONUM.
;;;   This requires specializing CUT as well (used internally in CLIP-xxx).
;;; 08-Jul-1997 BobGian fixed some possibly misleading comments in
;;;   in CLIP-FIXNUM and CLIP-FLONUM involving meaning of values returned
;;;   from OUTCODE and returned by CLIP-xxx themselves.
;;; 12-Aug-1997 BobGian converted CLIP-FIXNUM back to CLIP and eliminated
;;;   CLIP-FLONUM.  Ditto CUT-xxx (used inside clipping code).  Reason: dose
;;;   calculation has own interpolation functions (based on old CLIP-FLONUM)
;;;   and therefore CLIP/CUT-FLONUM macros no longer needed.
;;; 19-Jan-1998 I. Kalet change some setf to setq, experimented with
;;; truncate instead of round but did not make much difference.
;;; 21-Jan-2002 I. Kalet in compute-tics make every fifth tic a little
;;; larger.  Also, downcase names.
;;;

(in-package :prism)

;;;--------------------------------------

(defmacro pix-x (x x0 ppcm)

  `(the fixnum (+ (the fixnum ,x0)
		  (the fixnum (round (* (the single-float ,x)
					(the single-float ,ppcm)))))))

;;;--------------------------------------

(defmacro pix-y (y y0 ppcm)

  `(the fixnum (- (the fixnum ,y0)
		  (the fixnum (round (* (the single-float ,y)
					(the single-float ,ppcm)))))))

;;;--------------------------------------

(defun cm-x (x x0 ppcm)

  (declare (fixnum x x0) (single-float ppcm))
  (/ (- x x0) ppcm))

;;;--------------------------------------

(defun cm-y (y y0 ppcm)

  (declare (fixnum y y0) (single-float ppcm))
  (/ (- y0 y) ppcm))

;;;--------------------------------------

(defun pixel-contour (cont pix-per-cm xorig yorig)

  "pixel-contour cont pix-per-cm xorig yorig

returns a list of pixel coordinates from cont, a list of vertices,
each an x, y pair in cm in real space, using scale factor pix-per-cm.
The xorig and yorig parameters are the pixel coordinates of the real
space origin."

  (declare (fixnum xorig yorig) (single-float pix-per-cm))
  (let ((result nil))
    (dolist (pt cont)
      (push (pix-x (first pt) xorig pix-per-cm) result)
      (push (pix-y (second pt) yorig pix-per-cm) result))
    (nreverse result)))

;;;--------------------------------------

(defun cm-contour (pixcon pix-per-cm xorig yorig)

  "cm-contour pixcon pix-per-cm xorig yorig

returns a list of vertices, each an x, y pair in cm or real
coordinates, from pixcon, a list of pixel coordinates alternating x y
x y, using scale factor pix-per-cm.  The xorig and yorig parameters
are the pixel coordinates of the real space origin."

  (declare (fixnum xorig yorig) (single-float pix-per-cm))
  (cond ((null pixcon) nil)
	(t (cons (list (cm-x (first pixcon) xorig pix-per-cm)
		       (cm-y (second pixcon) yorig pix-per-cm))
		 (cm-contour (rest (rest pixcon)) pix-per-cm xorig yorig)))))

;;;-----------------------------------

(defun pixel-segments (segs pix-per-cm xorig yorig)

  "pixel-segments segs pix-per-cm xorig yorig

returns a list of pixel coordinates from segs, a list of (x1 y1 x2 y2)
4-tuples, each tuple defining a segment in model space.  The pix-per-cm, 
xorig, and yorig parameters are the scale factor, x coord of the model
space origin, and y coord of the model space origin, respectively."

  (declare (single-float pix-per-cm) (fixnum xorig yorig))
  (when segs
    (let ((result nil))
      (dolist (seg segs)
	(push (pix-x (first seg) xorig pix-per-cm) result)
	(push (pix-y (second seg) yorig pix-per-cm) result)
	(push (pix-x (third seg) xorig pix-per-cm) result)
	(push (pix-y (fourth seg) yorig pix-per-cm) result))
      (nreverse result))))

;;;--------------------------------------
;;; compute-tics is used to draw the tape measure tics, the beam central
;;; axis tics, and (when implemented) the view scale tics.

(defun compute-tics (x1 y1 x2 y2 scale x-origin y-origin tic-length)

  "compute-tics x1 y1 x2 y2 scale x-origin y-origin tic-length

Computes a series of tic marks, spaced 1.0 cm apart, between the
points (x1 y1) and (x2 y2) in model space.  The scale and origin
parameters are used to convert the coordinates to pixel space.  The
length of each tic is tic-length pixels.  Returns a list of the form
{x1 y1 x2 y2}*, suitable for passing to clx:draw-segments."

  (unless (and (= x1 x2) (= y1 y2))
    (do* ((len (distance x1 y1 x2 y2))
 	  (rlen (/ 1.0 len))
          (tlen (truncate len))
	  (dx (* rlen (- x2 x1))) ;; draw tape tics 1 cm apart
	  (dy (* rlen (- y2 y1)))
	  (c (+ x1 dx)) ;; start 1 cm from end
	  (d (+ y1 dy))
	  (tl (float (/ tic-length scale)))
	  (px (* dx tl))
	  (py (* dy tl))
	  (segs nil)
	  (tx1 (+ c (* tl (- dy))) (+ tx1 dx)) ;; tic end 1
	  (tx2 (- c (* tl (- dy))) (+ tx2 dx)) ;; tic end 2
	  (ty1 (+ d (* tl dx)) (+ ty1 dy))
	  (ty2 (- d (* tl dx)) (+ ty2 dy))
	  (i 0 (1+ i)))
	((= i tlen)
	 (pixel-segments segs scale x-origin y-origin))
      ;; make every fifth tic double size
      (push (list (if (zerop (mod (1+ i) 5)) (- tx1 py) tx1)
		  (if (zerop (mod (1+ i) 5)) (+ ty1 px) ty1)
		  (if (zerop (mod (1+ i) 5)) (+ tx2 py) tx2)
		  (if (zerop (mod (1+ i) 5)) (- ty2 px) ty2))
	    segs))))

;;;----------------------------------------------

(defun draw-plus-icon (pt scl x-orig y-orig radius)

  "draw-plus-icon pt scl x-orig y-orig radius

Draws a plus icon situated at the supplied point, with the given
radius, under the supplied scale and origin parameters.  Returns a list
of the form {x1 y1 x2 y2}*, suitable for passing to clx:draw-segments."

  (let ((x (pix-x (first pt) x-orig scl))
	(y (pix-y (second pt) y-orig scl)))
    (list (- x radius) y (+ x radius) y
	  x (- y radius) x (+ y radius))))

;;;----------------------------------------------

(defun draw-diamond-icon (pt scl x-orig y-orig radius)

  "draw-diamond-icon pt scl x-orig y-orig radius

Draws a diamond icon situated at the supplied point, with the given
radius, under the supplied scale and origin parameters.  Returns a list
of the form {x1 y1 x2 y2}*, suitable for passing to clx:draw-segments."

  (let* ((x (pix-x (first pt) x-orig scl))
         (y (pix-y (second pt) y-orig scl))
         (x-plus (+ x radius))
         (x-minus (- x radius))
         (y-plus (+ y radius))
         (y-minus (- y radius)))
    (list x y-minus x-plus y
	  x-plus y x y-plus
	  x y-plus x-minus y
	  x-minus y x y-minus)))

;;;----------------------------------------------
;;; Projecting points or contours may sometimes generate outlandish
;;; coordinates, and X will misinterpret them as negative values if
;;; given the raw data.  Therefore, the following is for clipping
;;; contours such as beam portals to fit a reasonable range. 
;;;----------------------------------------------

;;; x and y args to outcode should be declared in containing code.

(defmacro outcode (x y x-min y-min x-max y-max)

  "outcode x y x-min y-min x-max y-max

          min    max
      0101 | 0100 | 0110
 min  -----+------+-----
      0001 | 0000 | 0010
 max  -----+------+-----
      1001 | 1000 | 1010 "

  `(let ((clip-code 0))
     (declare (fixnum clip-code))
     (when (< ,x ,x-min)
       (setq clip-code (logior clip-code #b0001)))
     (when (> ,x ,x-max)
       (setq clip-code (logior clip-code #b0010)))
     (when (< ,y ,y-min)
       (setq clip-code (logior clip-code #b0100)))
     (when (> ,y ,y-max)
       (setq clip-code (logior clip-code #b1000)))
     clip-code))

;;;----------------------------------------------

;;; All args are declared fixnum in containing function definition.

(defmacro cut (a b c d bound)

  "cut a b c d bound

b is out of bounds. Cut it at bound and interpolate thereby
where a should go.  a, b, c, d must be symbols.  fixnum only"

  `(progn
     ;;
     ;; Convert fixnums to single-float before division so result of /
     ;; is a single-float (to be rounded to fixnum) rather than a ratio.
     ;;
     ;; Note: Function interpolate in beam-dose does same calculation
     ;; as this code, but for float-only arguments.  It has
     ;; optimizing code to select end closer to bound from which to
     ;; interpolate, so as to reduce chance of roundoff error
     ;; affecting result.  This approach is not taken here because all
     ;; values are "small" (ie, abs value under 1000) integers and we
     ;; want to keep this code as small as possible since it is
     ;; expanded inline.  See comments in interpolate, file beam-dose.
     ;;
     (setq ,a (+ ,a (round (/ (float (* (- ,c ,a) (- ,bound ,b)))
			      (float (- ,d ,b))))))
     (setq ,b ,bound)))

;;;----------------------------------------------

;;; All arguments to clip must be symbols (ie, variables) which are
;;; declared fixnum in the containing function - because macro is expanded
;;; inline and declarations must go outside macro's scope.

(defmacro clip (x1 y1 x2 y2 x-min y-min x-max y-max)

  "clip x1 xy x2 y2

clip the line segment from (x1,y1) to (x2,y2) by the bounds x-min,
y-min, x-max, y-max.  This is Cohen-Sutherland clipping (see
Foley/VanDam/Feiner/Hughes) except we don't swap codes and
coordinates, since we want to keep the list of coordinate pairs 
stable.  x1, y1, x2, y2  are symbols; rest are symbols or numbers.
Return t if line segment crosses or is in central window, nil
if line segment is totally outside central window.  fixnum-only version."

  `(let ((code1 0)
	 (code2 0))
     (declare (fixnum code1 code2))
     (loop
      (setq code1 (outcode ,x1 ,y1 ,x-min ,y-min ,x-max ,y-max)
	    code2 (outcode ,x2 ,y2 ,x-min ,y-min ,x-max ,y-max))
      (cond ((zerop (logior code1 code2))
	     ;; (x1 y1), (x2 y2) were already or have been clipped into
	     ;; range - there is a line segment in the central window.
	     (return t))
	    ((logtest code1 code2)
	     ;; Both x or both y (or both) are out of range on same side
	     ;; of common boundary - no line segment crosses central window.
	     (return nil))
	    ((logtest #b1000 code1)
	     ;; y1 is too large - clip to y-max.
	     (cut ,x1 ,y1 ,x2 ,y2 ,y-max))
	    ((logtest #b0100 code1)
	     ;; y1 is too small - clip to y-min.
	     (cut ,x1 ,y1 ,x2 ,y2 ,y-min))
	    ((logtest #b0010 code1)
	     ;; x1 is too large - clip to x-max.
	     (cut ,y1 ,x1 ,y2 ,x2 ,x-max))
	    ((logtest #b0001 code1)
	     ;; x1 is too small - clip to x-min.
	     (cut ,y1 ,x1 ,y2 ,x2 ,x-min))
	    ((logtest #b1000 code2)
	     ;; y2 is too large - clip to y-max.
	     (cut ,x2 ,y2 ,x1 ,y1 ,y-max))
	    ((logtest #b0100 code2)
	     ;; y2 is too small - clip to y-min.
	     (cut ,x2 ,y2 ,x1 ,y1 ,y-min))
	    ((logtest #b0010 code2)
	     ;; x2 is too large - clip to x-max.
	     (cut ,y2 ,x2 ,y1 ,x1 ,x-max))
	    (t
	     ;; x2 is too small - clip to x-min
	     ;; only possibility left - no need to test
	     (cut ,y2 ,x2 ,y1 ,x1 ,x-min))))))

;;;----------------------------------------------

(defun scale-and-clip-lines 
  (pts scale x-origin y-origin x-min y-min x-max y-max)
  
  "scale-and-clip-lines pts x-origin y-origin scale x-min y-min x-max y-max

Returns list in the form {x1 y1 x2 y2}* suitable for passing to
clx:draw-segments, clipped to the bounds x-min y-min x-max y-max.
pts is a list of 2D points in the form of 2-element lists, in the
viewing coordinate system.  Scale is pixels per centimeter.
x-origin and y-origin together are the origin of the window."

  (declare (fixnum x-origin y-origin x-min y-min x-max y-max)  
           (single-float scale)
	   (list pts))
  (let ((p1 (car pts))
	(clipped-pts nil)
	(x1 0) (y1 0) (x2 0) (y2 0))
    (declare (type list clipped-pts)
             (fixnum x1 y1 x2 y2))
    (dolist (p2 (cdr pts))
      (declare (type list p2))
      (setq x1 (pix-x (first  p1) x-origin scale)
	    y1 (pix-y (second p1) y-origin scale)
	    x2 (pix-x (first  p2) x-origin scale)
	    y2 (pix-y (second p2) y-origin scale))
      (when (clip x1 y1 x2 y2 x-min y-min x-max y-max)
	(setq clipped-pts (list* x1 y1 x2 y2 clipped-pts)))
      (setq p1 p2))
    clipped-pts))

;;;----------------------------------------------
;;; End.
