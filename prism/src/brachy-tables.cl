;;;
;;; brachy-tables
;;;
;;; Defines the Sievert integral tables and lookup functions that are
;;; needed for the brachytherapy dose computation.
;;;
;;;  7-Mar-1997 I. Kalet created
;;;  9-May-1997 BobGian added stub defs so will compile OK.
;;; 19-Dec-1999 I. Kalet added selection and table editing support.
;;;  7-Feb-2000 I. Kalet completed Sievert integral table generation.
;;; 27-Feb-2000 I. Kalet add type declarations.
;;; 25-Apr-2000 I. Kalet add activity units slot.
;;;  8-May-2000 I. Kalet make gamma into separate slots for dose rate
;;; constant and anisotropy factor.  Add slot for string naming
;;; calibration protocol.
;;; 31-Mar-2002 I. Kalet add slot for half life, to support permanent
;;; implants.
;;;  1-May-2002 I. Kalet add edit support for half life slot.
;;; 27-Dec-2002 I. Kalet add two more coefficients to polynomial
;;; tissue correction data.
;;; 29-Jan-2003 I. Kalet change half-life upper limit to a more
;;; realistic value
;;;

(in-package :prism)

;;;--------------------------------------------------

(defvar *brachy-tables* nil
  "The list of tables defining the available brachytherapy sources")

(defvar *radii* '(0.1 0.15 0.2 0.3 0.4 0.6 0.8 1.2 1.6 2.4 3.2 4.8 6.4)
  "The list of radii for which the Sievert integral is tabulated.")

;;;--------------------------------------------------

(defclass source-table ()

  ((src-type :type string
	     :accessor src-type
	     :initform ""
	     :documentation "The string identifying the source type,
i.e., 3M-6711, or whatever...")

   (dose-rate-const :type single-float
		    :accessor dose-rate-const
		    :initform 8.81
		    :documentation "The dose rate factor for this
source type.")

   (anisotropy-fn :accessor anisotropy-fn
		  :initform 1.0
		  :documentation "A separate factor that accounts for
anisotropic seed alignment.")

   (protocol :type string
	     :accessor protocol
	     :initform ""
	     :documentation "Text identifying the seed calibration
protocol")

   (activity-units :type string
		   :accessor activity-units
		   :initform ""
		   :documentation "The short text string that is
displayed or printed with activity of a source, showing the units in
which the activity is specified.  This is related to the choice of
value for the dose rate factor.")

   (half-life :type single-float
	      :accessor half-life
	      :initform 0.0
	      :documentation "The half life in hours, in order to do
	      permanent implant calculations")

   (mu-wall :type single-float
	    :accessor mu-wall
	    :initform '((0.04 1.10) (0.06 1.10) (0.08 1.10) (0.1 1.10)
			(0.2 1.10) (0.4 1.10) (0.6 1.10) (0.8 1.10)
			(1.0 1.10) (2.0 1.10))
	    :documentation "An association list of attenuation values
as a function of thickness, to account for beam hardening")

   (diameter :type single-float
	     :accessor diameter
	     :initform 0.0)

   (wall-thickness :type single-float
		   :accessor wall-thickness
		   :initform 0.0)

   (endcap-thickness :type single-float
		     :accessor endcap-thickness
		     :initform 0.0)

   (mu-water :type single-float
	     :accessor mu-water
	     :initform 0.0
	     :documentation "The attenuation coefficient for
exponential attenuation of tissue beyond the maximum radius for the
polynomial correction factor.")

   (poly-range :type single-float
	       :accessor poly-range
	       :initform 0.0
	       :documentation "The distance in cm beyond which the
polynomial tissue correction should not be used.")

   (a0 :type single-float
       :accessor a0
       :initform 0.0
       :documentation "The constant coefficient in the polynomial
tissue correction.")

   (a1 :type single-float
       :accessor a1
       :initform 0.0
       :documentation "The linear term coefficient in the polynomial
tissue correction.")

   (a2 :type single-float
       :accessor a2
       :initform 0.0
       :documentation "The coefficient of r squared in the polynomial
tissue correction.")

   (a3 :type single-float
       :accessor a3
       :initform 0.0
       :documentation "The coefficient of r cubed in the polynomial
tissue correction.")

   (a4 :type single-float
       :accessor a4
       :initform 0.0
       :documentation "The coefficient of r fourth power in the
       polynomial tissue correction.")

   (a5 :type single-float
       :accessor a5
       :initform 0.0
       :documentation "The coefficient of r fifth power in the
       polynomial tissue correction.")

   (actlen :type single-float
	   :accessor actlen
	   :initform 0.0
	   :documentation "The active length of the source, applicable
only to line sources.")

   (physlen :type single-float
	    :accessor physlen
	    :initform 0.0
	    :documentation "The physical length of the source, as it
would be seen on a radiograph - applicable only to line sources.")

   (sievert-table :accessor sievert-table
		  :initform nil
		  :documentation "The Prism modified Sievert integral
table for calculation of dose from line sources.")

   )

  (:documentation "A source-table represents the characteristics of a
single source type, i.e., isotope, capsule type and size, but not
activity.")

  )

;;;--------------------------------------------------

(defun source-data (source-id)

  (find source-id *brachy-tables*
	:key #'src-type :test #'string-equal))

;;;--------------------------------------------------

(defun trapezoid (ll ul nsteps fn)

  "trapezoid ll ul nsteps fn

returns the definite integral of fn, a real valued function of one
real value,  from ll to ul, using the trapezoidal rule, and dividing
the interval into nsteps divisions."

  (if (< nsteps 5) (setq nsteps 5))
  (let ((delta (/ (- ul ll) nsteps))
	(sum (* 0.5 (+ (funcall fn ll) (funcall fn ul))))
	(x ll))
    ;; (declare (single-float ll ul delta sum x))
    (dotimes (i (- nsteps 1) (* delta sum))
      (incf x delta)
      (incf sum (funcall fn x)))))

;;;--------------------------------------------------

(defun effective-thickness (filt diam)

  (let* ((big-r (/ diam 2.0))
	 (rs (- big-r filt))
	 (rs2 (* rs rs))
	 (big-r2 (* big-r big-r))
	 (pi1 (coerce pi 'single-float)))
    (if (<= rs 0.0) filt
      (- (* (/ 2.0 (* pi1 rs2))
	    (trapezoid (- rs) rs 1000
		       #'(lambda (r)
			   (let ((r2 (* r r)))
			     (sqrt (* (- rs2 r2) (- big-r2 r2)))))))
	 (/ (* 2.4 rs) pi1)))))

;;;--------------------------------------------------

(defun findslot (given table)

  "findslot given table

returns two values, the index of the lower bounding element of table
for the value of given, and the fraction of the way given is between
the lower bounding element and the next element.  If given is less
than the lowest value in table, the index is 0 and the fraction is 0.0
and if the value is larger than the largest value in the table, the
index is one less than the index of the last element, and the fraction
is 1.0.  The table is assumed to be in order of increasing values."

  (declare (single-float given))
  (cond ((< given (first table)) (values 0 0.0))
	((> given (first (last table)))
	 (values (- (length table) 2) 1.0))
	(t (let ((leftend 0)
		 (rightend (1- (length table))))
	     (declare (fixnum leftend rightend))
	     (loop
	       (if (> rightend (1+ leftend))
		   (let ((newend (round (/ (+ leftend rightend) 2))))
		     (if (>= (nth newend table) given)
			 (setq rightend newend)
		       (setq leftend newend)))
		 (return (values leftend
				 (/ (- given (nth leftend table))
				    (- (nth (1+ leftend) table)
				       (nth leftend table)))))))))))

;;;--------------------------------------------------

(defun calc-sievert-table (table-entry)

  "calc-sievert-table table-entry

calculates a table of modified Sievert integrals and stores them in
the table record, for later lookup for line sources.  This is the
table generation function."

  (let* ((stab (or (sievert-table table-entry)
		   (setf (sievert-table table-entry)
		     (make-array (list (length *radii*) 11)
				 :element-type 'single-float
				 :initial-element 0.0))))
	 (wall-thicknesses (mapcar #'first (mu-wall table-entry)))
	 (atten-list (mapcar #'second (mu-wall table-entry)))
	 (d (effective-thickness (wall-thickness table-entry)
				 (diameter table-entry)))
	 (mu-salt (* 0.1 (nth 3 atten-list) ;; approx of mu of salt
		     (actlen table-entry)))
	 (endcap (endcap-thickness table-entry))
	 (end-mu (multiple-value-bind (index fr)
		     (findslot endcap wall-thicknesses)
		   (+ (* (- 1.0 fr) (nth index atten-list))
		      (* fr (nth (1+ index) atten-list)))))
	 (end-coeff (exp (- (* end-mu endcap))))
	 (actlen (actlen table-entry)))
    (flet ((sievert-integrand (theta)
	     (let* ((slant (/ d (cos theta)))
		    (mu (multiple-value-bind (index fr)
			    (findslot slant wall-thicknesses)
			  (+ (* (- 1.0 fr) (nth index atten-list))
			     (* fr (nth (1+ index) atten-list))))))
	       (exp (- (* mu slant))))))
      (dotimes (i (length *radii*))
	(let ((r (nth i *radii*)))
	  (dotimes (j 10)
	    (let* ((costh (* 0.1 (- 10 j))) ;; theta is 90 - phi
		   (sinth (if (zerop j) 0.0 ;; when j is 0, sin theta is 0
			    (sqrt (- 1.0 (* costh costh)))))
		   (x (* r sinth))
		   (y (* r costh)))
	      (setf (aref stab i j)
		(* (/ r costh)
		   (trapezoid (atan (/ (- x 0.5) y))
			      (atan (/ (+ x 0.5) y))
			      200
			      #'sievert-integrand)))))
	  (setf (aref stab i 10)
	    (if (<= r 0.5) 0.0
	      (* end-coeff r r
		 (trapezoid -0.5 0.5 200
			    #'(lambda (x)
				(/ (exp (- (* mu-salt (- 0.5 x) actlen)))
				   (* (- r x) (- r x)))
				))))))))))

;;;--------------------------------------------------

(defun source-menu (line)

  "source-menu line

returns an association list of pairs, each consisting of a source type
string and a full description string, one pair for each entry from the
current list of source tables, the value of *brachy-tables*.  If line
is t, line sources are listed, otherwise seed."

  (mapcar #'(lambda (tab)
	      (list (src-type tab)
		    (format nil "~A ~A C=~4,2F A=~4,2F ~A"
			    (src-type tab) (protocol tab)
			    (dose-rate-const tab) (anisotropy-fn tab)
			    (activity-units tab))))
	  (funcall (if line #'remove-if #'remove-if-not)
		   #'zerop *brachy-tables* :key #'actlen)))

;;;--------------------------------------------------

(defun mu-wall-edit (mu-wall-alist)

  "a little panel for entering or modifying wall attenuation coeffs."

  (sl:push-event-level)
  (let* ((btw 170)
	 (bth 30)
	 (wall-frame (sl:make-frame (+ btw 10)
				    (+ (* 11 (+ bth 5)) 5)
				    :title "Wall Attenuations"))
	 (win (sl:window wall-frame))
	 (tmp-list (copy-tree mu-wall-alist))
	 (accept-btn (sl:make-exit-button 80 bth :parent win
					  :ulc-x 5 :ulc-y (bp-y 5 bth 10)
					  :bg-color 'sl:green
					  :label "Accept"))
	 textline-list
	 (update nil))
    (ev:add-notify wall-frame (sl:button-on accept-btn)
		   #'(lambda (fr btn)
		       (declare (ignore fr btn))
		       (setf update t)))
    (push accept-btn textline-list)
    (push (sl:make-exit-button 80 bth :parent win
			       :ulc-x 90 :ulc-y (bp-y 5 bth 10)
			       :label "Cancel")
	  textline-list)
    (dotimes (i 10)
      (let ((tln (sl:make-textline btw bth :parent win
				   :ulc-x 5 :ulc-y (bp-y 5 bth i)
				   :numeric t
				   :lower-limit 0.0 :upper-limit 100.0
				   :label
				   (format nil "~5A cm: "
					   (first (nth i tmp-list))))))
	(ev:add-notify i (sl:new-info tln)
		       #'(lambda (n tl info)
			   (declare (ignore tl))
			   (setf (second (nth n tmp-list))
			     (coerce (read-from-string info) 'single-float))))
	(setf (sl:info tln) (second (nth i tmp-list)))
	(push tln textline-list)))
    (sl:process-events)
    (dolist (tln textline-list) (sl:destroy tln))
    (sl:destroy wall-frame)
    (sl:pop-event-level)
    ;; if change requested return new list, otherwise return original
    (if update tmp-list mu-wall-alist)))

;;;--------------------------------------------------

(defun edit-source-table (table)

  "edit-source-table table

provides a popup panel to enter or modify all the attributes of the
source defined by table."

  (sl:push-event-level)
  (let* ((bth 30)
	 (btw 170)
	 (dx 5)
	 (dx2 (+ btw (* 2 dx)))
	 (dx3 (+ dx2 btw dx))
	 (top-y 5)
	 (frm (sl:make-frame (+ dx (* 3 (+ dx btw)))
			     (+ top-y (* 7 (+ top-y bth)))
			     :title "Prism source table editor"))
	 (win (sl:window frm))
	 (update-btn (sl:make-exit-button 80 bth :parent win
					  :ulc-x dx :ulc-y top-y
					  :fg-color 'sl:black
					  :bg-color 'sl:green
					  :label "Update"))
	 (cancel-btn (sl:make-exit-button 80 bth :parent win
					  :ulc-x (+ dx 90) :ulc-y top-y
					  :label "Cancel"))
	 (name-tln (sl:make-textline btw bth :parent win
				     :ulc-x dx :ulc-y (bp-y top-y bth 1)
				     :label "Type: "))
	 (drate-tln (sl:make-textline btw bth :parent win
				      :ulc-x dx :ulc-y (bp-y top-y bth 2)
				      :label "DR const: "
				      :numeric t
				      :lower-limit 0.0 :upper-limit 100.0))
	 (actunits-tln (sl:make-textline btw bth :parent win
				      :ulc-x dx :ulc-y (bp-y top-y bth 3)
				      :label "Act units: "))
	 (actlen-tln (sl:make-textline btw bth :parent win
				       :ulc-x dx :ulc-y (bp-y top-y bth 4)
				       :label "Active len: "
				       :numeric t
				       :lower-limit 0.0 :upper-limit 100.0))
	 (physlen-tln (sl:make-textline btw bth :parent win
					:ulc-x dx :ulc-y (bp-y top-y bth 5)
					:label "Phys. len: "
					:numeric t
					:lower-limit 0.0 :upper-limit 100.0))
	 (halflife-tln (sl:make-textline btw bth :parent win
					:ulc-x dx :ulc-y (bp-y top-y bth 6)
					:label "Half-life: "
					:numeric t
					:lower-limit 0.0 :upper-limit 2000.0))
	 (poly-range-tln (sl:make-textline btw bth :parent win
					   :ulc-x dx2
					   :ulc-y top-y
					   :label "P-Range: "
					   :numeric t
					   :lower-limit 0.0
					   :upper-limit 100.0))
	 (a0-tln (sl:make-textline btw bth :parent win
				   :ulc-x dx2 :ulc-y (bp-y top-y bth 1)
				   :label "A0: "
				   :numeric t
				   :lower-limit -100.0 :upper-limit 100.0))
	 (a1-tln (sl:make-textline btw bth :parent win
				   :ulc-x dx2 :ulc-y (bp-y top-y bth 2)
				   :label "A1: "
				   :numeric t
				   :lower-limit -100.0 :upper-limit 100.0))
	 (a2-tln (sl:make-textline btw bth :parent win
				   :ulc-x dx2 :ulc-y (bp-y top-y bth 3)
				   :label "A2: "
				   :numeric t
				   :lower-limit -100.0 :upper-limit 100.0))
	 (a3-tln (sl:make-textline btw bth :parent win
				   :ulc-x dx2 :ulc-y (bp-y top-y bth 4)
				   :label "A3: "
				   :numeric t
				   :lower-limit -100.0 :upper-limit 100.0))
	 (a4-tln (sl:make-textline btw bth :parent win
				   :ulc-x dx2 :ulc-y (bp-y top-y bth 5)
				   :label "A4: "
				   :numeric t
				   :lower-limit -100.0 :upper-limit 100.0))
	 (a5-tln (sl:make-textline btw bth :parent win
				   :ulc-x dx2 :ulc-y (bp-y top-y bth 6)
				   :label "A5: "
				   :numeric t
				   :lower-limit -100.0 :upper-limit 100.0))
	 (proto-tln (sl:make-textline btw bth :parent win
				      :ulc-x dx3 :ulc-y top-y
				      :label "Proto: "))
	 (aniso-tln (sl:make-textline btw bth :parent win
				      :ulc-x dx3 :ulc-y (bp-y top-y bth 1)
				      :label "Aniso: "
				      :numeric t
				      :lower-limit 0.0 :upper-limit 1.0))
	 (mu-water-tln (sl:make-textline btw bth :parent win
					 :ulc-x dx3 :ulc-y (bp-y top-y bth 2)
					 :label "Mu-water: "
					 :numeric t
					 :lower-limit 0.0 :upper-limit 100.0))
	 (mu-wall-btn (sl:make-button btw bth :parent win
				      :ulc-x dx3 :ulc-y (bp-y top-y bth 3)
				      :label "Mu-wall table"))
	 (diam-tln (sl:make-textline btw bth :parent win
				     :ulc-x dx3 :ulc-y (bp-y top-y bth 4)
				     :label "Diam: "
				     :numeric t
				     :lower-limit 0.0 :upper-limit 1.0))
	 (wallthick-tln (sl:make-textline btw bth :parent win
					  :ulc-x dx3 :ulc-y (bp-y top-y bth 5)
					  :label "Wall thick: "
					  :numeric t
					  :lower-limit 0.0 :upper-limit 1.0))
	 (endcap-tln (sl:make-textline btw bth :parent win
				       :ulc-x dx3 :ulc-y (bp-y top-y bth 6)
				       :label "End thick: "
				       :numeric t
				       :lower-limit 0.0 :upper-limit 1.0))
	 ;; local temporary variables
	 (src-type (src-type table))
	 (drate (dose-rate-const table))
	 (aniso (anisotropy-fn table))
	 (proto (protocol table))
	 (actunits (activity-units table))
	 (actlen (actlen table))
	 (physlen (physlen table))
	 (halflife (half-life table))
	 (mu-wall (mu-wall table))
	 (mu-water (mu-water table))
	 (poly-range (poly-range table))
	 (a0 (a0 table))
	 (a1 (a1 table))
	 (a2 (a2 table))
	 (a3 (a3 table))
	 (a4 (a4 table))
	 (a5 (a5 table))
	 (diameter (diameter table))
	 (wall-thickness (wall-thickness table))
	 (endcap-thickness (endcap-thickness table))
	 (update nil))
    (setf (sl:info name-tln) (src-type table)
	  (sl:info drate-tln) (dose-rate-const table)
	  (sl:info aniso-tln) (anisotropy-fn table)
	  (sl:info proto-tln) (protocol table)
	  (sl:info actunits-tln) (activity-units table)
	  (sl:info actlen-tln) (actlen table)
	  (sl:info physlen-tln) (physlen table)
	  (sl:info halflife-tln) (half-life table)
	  (sl:info mu-water-tln) (mu-water table)
	  (sl:info poly-range-tln) (poly-range table)
	  (sl:info a0-tln) (a0 table)
	  (sl:info a1-tln) (a1 table)
	  (sl:info a2-tln) (a2 table)
	  (sl:info a3-tln) (a3 table)
	  (sl:info a4-tln) (a4 table)
	  (sl:info a5-tln) (a5 table)
	  (sl:info diam-tln) (diameter table)
	  (sl:info wallthick-tln) (wall-thickness table)
	  (sl:info endcap-tln) (endcap-thickness table))
    (ev:add-notify table (sl:new-info name-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf src-type info)))
    (ev:add-notify table (sl:new-info drate-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf drate
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info aniso-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf aniso
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info proto-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf proto info)))
    (ev:add-notify table (sl:new-info actunits-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf actunits info)))
    (ev:add-notify table (sl:button-on mu-wall-btn)
		   #'(lambda (tab btn)
		       (declare (ignore tab))
		       (setf mu-wall (mu-wall-edit mu-wall))
		       (setf (sl:on btn) nil)))
    (ev:add-notify table (sl:new-info actlen-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf actlen
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info physlen-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf physlen
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info halflife-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf halflife
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info mu-water-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf mu-water
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info poly-range-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf poly-range
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info a0-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf a0
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info a1-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf a1
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info a2-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf a2
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info a3-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf a3
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info a4-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf a4
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info a5-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf a5
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info diam-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf diameter
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info wallthick-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf wall-thickness
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:new-info endcap-tln)
		   #'(lambda (tab tl info)
		       (declare (ignore tab tl))
		       (setf endcap-thickness
			 (coerce (read-from-string info) 'single-float))))
    (ev:add-notify table (sl:button-on update-btn)
		   #'(lambda (tab btn)
		       (declare (ignore btn))
		       ;; set all the values...
		       (setf (src-type tab) src-type
			     (dose-rate-const tab) drate
			     (anisotropy-fn tab) aniso
			     (protocol tab) proto
			     (activity-units tab) actunits
			     (actlen tab) actlen
			     (physlen tab) physlen
			     (half-life tab) halflife
			     (mu-water tab) mu-water
			     (mu-wall tab) mu-wall
			     (poly-range tab) poly-range
			     (a0 tab) a0 (a1 tab) a1
			     (a2 tab) a2 (a3 tab) a3
			     (a4 tab) a4 (a5 tab) a5
			     (diameter tab) diameter 
			     (wall-thickness tab) wall-thickness 
			     (endcap-thickness tab) endcap-thickness 
			     update t)
		       (if (not (zerop (actlen tab)))
			   (calc-sievert-table tab))))
    (sl:process-events)
    (sl:destroy update-btn)
    (sl:destroy cancel-btn)
    (sl:destroy name-tln)
    (sl:destroy drate-tln)
    (sl:destroy actunits-tln)
    (sl:destroy mu-wall-btn)
    (sl:destroy actlen-tln)
    (sl:destroy physlen-tln)
    (sl:destroy mu-water-tln)
    (sl:destroy poly-range-tln)
    (sl:destroy a0-tln)
    (sl:destroy a1-tln)
    (sl:destroy a2-tln)
    (sl:destroy a3-tln)
    (sl:destroy a4-tln)
    (sl:destroy a5-tln)
    (sl:destroy diam-tln)
    (sl:destroy wallthick-tln)
    (sl:destroy endcap-tln)
    (sl:destroy frm)
    (sl:pop-event-level)
    update))

;;;--------------------------------------------------

(defun brachy-table-manager ()

  (let* ((items (append (source-menu nil) (source-menu t)))
	 (selection (sl:popup-menu (cons "New table"
					 (mapcar #'second items)))))
    (if selection
	(let* ((table (if (= selection 0)			 
			  (make-instance 'source-table)
			(source-data (first (nth (1- selection) items)))))
	       (update (edit-source-table table)))
	  (if (and (= selection 0) update)
	      (push table *brachy-tables*))
	  (if (and update
		   (sl:confirm "Update the brachy source catalog file?"))
	      (put-all-objects *brachy-tables*
			       (merge-pathnames "source-catalog"
						*brachy-database*)))))))

;;;--------------------------------------------------

(defun sievert (radius costh table)

  "sievert radius costh table

returns the interpolated value of the Prism modified Sievert integral
from the specified source table.  This is the lookup function, used in
the actual dose calculation."

  ;; (declare (type (simple-array single-float (13 11)) table))
  (let* ((st10 (if (>= costh 1.0) 10.0
		 (- 10.0 (* 10.0 (sqrt (- 1.0 (* costh costh)))))))
	 (j (min (truncate st10) 9)) ;; j has to be < 10, array is dim 11
	 (fr2 (- st10 j)))
    (declare (fixnum j) (single-float fr2))
    (multiple-value-bind (i fr1)
	(findslot radius *radii*)
      (declare (fixnum i) (single-float fr1))
      (+ (* (- 1.0 fr2) (- 1.0 fr1) (aref table i j))
	 (* fr1 (- 1.0 fr2) (aref table (+ i 1) j))
	 (* fr2 (- 1.0 fr1) (aref table i (+ j 1)))
	 (* fr1 fr2 (aref table (+ i 1) (+ j 1)))))))

;;;--------------------------------------------------
;;; End.
