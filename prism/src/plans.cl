;;;
;;; plans
;;;
;;; The Prism plan class and associated functions.
;;;
;;; 30-Jul-1992 I. Kalet created from rtp-objects and view-test
;;; 17-Aug-1992 I. Kalet add events to plan, busy bits and action
;;; functions to plan panel, destroy method for plan-panel
;;; 29-Nov-1992 I. Kalet condense view-set-mediator stuff, add
;;; table-position slot to cache this info, put in a beam when
;;; creating one, and arrange to forward to all beams when updated.
;;; 16-Dec-1992 I. Kalet/J. Unger add object manager code and slots in
;;; plan class to maintain objects in views, also add image manager
;;; 31-Dec-1992 I. Kalet provide setf method for images so can create
;;; mediators anyway even if images are loaded after plans are
;;; created.  Also anatomy, tumor, target object mediators created by
;;; code in patients module, not here.
;;;  2-Mar-1993 I. Kalet don't save new-time-stamp
;;; 11-Oct-1993 J. Unger replace old dose attributes with dose-grid,
;;; dose-result, and dose-surfaces attributes.
;;; 15-Oct-1993 J. Unger add dose-view manager and dose-result manager
;;; attributes to plan definition and init-inst :after method.
;;; 18-Oct-1993 J. Unger add organs and marks attributes to plan, add 
;;; compute-dose function to plan.
;;; 20-Oct-1993 J. Unger add organ-dose manager slot to plan definition,
;;; add rudimentary destroy method for plans (still needs work).
;;; 01-Nov-1993 J. Unger add (temporary) *save-plan-dose* reference in 
;;; plan's not-saved method - determines whether dose-results are saved.
;;; 05-Nov-1993 J. Unger add pat-id and case-id slots to plan def'n & to 
;;; plan's not-saved method.  Also add table-position to not-saved method.
;;;  6-Jan-1994 I. Kalet add pointer to patient, eliminate slots for
;;;  stuff from patient, provide reader method for table-position as
;;;  if it were a slot.  Fix dose comp to get organs from patient.
;;; 07-Feb-1994 J. Unger set back pointer from view to plan when view
;;; is added to plan's collection of views.
;;; 15-Feb-1994 J. Unger move initialization of (grid-vm p) from plan's
;;; init-inst (creation time) to plan's setf patient-of :after method
;;; (insertion time into patient's collection of plans).
;;; 16-Feb-1994 J. Unger define plan's dose-grid from the patient's 
;;; anatomy limits.
;;; 18-Feb-1994 D. Nguyen add copy-plan, fix dose-grid, sum-dose initargs.
;;; 25-Apr-1994 J. Unger add code to initialize point-view manager.
;;;  5-May-1994 J. Unger split compute-dose into compute-dose-points
;;; and compute-dose-grid.
;;; 17-May-1994 I. Kalet change type of comments to list of strings
;;; 01-Jun-1994 J. Unger add :points arg to make-dose-specification-mgr
;;; 02-Jun-1994 J. Unger change time-stamp attribute from read-only to 
;;; read-write, implement way to update plan's time-stamp when appropriate.
;;;  8-Jun-1994 J. Unger remove refs to tsm (vestigial).  Add function
;;;  write-dose-info; take out old system for saving dose to the
;;;  checkpoint database.
;;; 13-Jun-1994 I. Kalet make destroy primary method instead of :before
;;; 21-Jun-1994 I. Kalet declare time-stamp to be slot type :timestamp
;;; 30-Jun-1994 I. Kalet delete function references to brachy for now.
;;; 07-Jul-1994 J. Unger add copy-name param to copy-beam in copy-plan.
;;; 25-Aug-1994 J. Unger change :overwrite to :supersede in write-dose-info.
;;; 26-Sep-1994 J. Unger enhance copy-plan to copy dose surfaces, comments,
;;;  and plan author as well.
;;; 11-Oct-1994 J. Unger modify documentation string for copy-plan.
;;; 19-Jan-1995 I. Kalet Delete all the views when destroying a plan.
;;; Add notify on beam's update-plan event when inserting a beam into
;;; the beam set.  Remove table-position, not needed.  Remove refs to
;;; plan-of back-pointer for views.  Pass plan to beam-view-mediator.
;;; Move compute-dose stuff to dosecomp.  Don't set beam back-pointer,
;;; it has been deleted.  Do new-coll-set registration on insert of
;;; beam in beam set, for restoration from file system.
;;;  5-Mar-1995 I. Kalet finally remove patient-of back pointer, move
;;; code to initialize-instance method and to patient-plan-mediator.
;;; Now can destroy view on deletion here since it is not drawn into
;;; from elsewhere on deletion.
;;;  1-Jun-1995 I. Kalet name is now a required parameter to
;;; make-dose-surface, not a keyword parameter.  Also must initialize
;;; dose surfaces as they are inserted here, for reading from file.
;;; 27-Jul-1995 I. Kalet add missing initarg declaration for dose-grid.
;;;  9-Jun-1996 I. Kalet add support for line sources and seeds, take
;;;  out redundant registrations when beams inserted.
;;; 20-May-1997 I. Kalet only pass view set, not plan, to
;;; beam-view-mediator constructor, to avoid circularity.
;;; 26-Jun-1997 I. Kalet take out redundant setting name of dose
;;; surface, use flet in init-inst method, init stuff in make-plan.
;;;  4-Jul-1997 I. Kalet fix error - actually return plan in
;;;  make-plan.
;;; 15-Aug-1997 I. Kalet put make-grid-geometry in initform, not in
;;; make-plan.  If it gets replaced by dose grid from data file, it is
;;; still ok, since registrations happen afterward.
;;;  5-Mar-2000 I. Kalet replace copy-beam with just copy.
;;; 29-Mar-2000 I. Kalet mods for brachy, made and rescinded.
;;; 14-Oct-2001 I. Kalet copy retains original name and time stamp,
;;; so if change is desired, caller must do it to the copy.  This is
;;; the same semantics of copy for other things.
;;;  6-Oct-2002 I. Kalet combined line and seed view-mediators into
;;; single brachy-view-mediator class so just use that.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass plan (generic-prism-object)

  ((comments :type list
	     :initarg :comments
	     :accessor comments
	     :documentation "Multiple lines of text to be printed on
the chart")
   
   (new-comments :type ev:event
		 :initform (ev:make-event)
		 :accessor new-comments
		 :documentation "Announced when the comments are
updated.")

   (time-stamp :type string 
	       :initform (date-time-string)
	       :accessor time-stamp)

   (new-time-stamp :type ev:event
		   :accessor new-time-stamp
		   :initform (ev:make-event)
		   :documentation "Announced when the time-stamp is
updated.")

   (plan-by :type string ; nice to know who did it
	    :initarg :plan-by
	    :accessor plan-by)

   (new-plan-by :type ev:event
		:initform (ev:make-event)
		:accessor new-plan-by
		:documentation "Announced when the plan-by attribute
is updated.")

   (prescription-used :initarg :prescription-used ; a target object
		      :accessor prescription-used)

   (beams :initform (coll:make-collection)
	  :accessor beams)

   (line-sources :initform (coll:make-collection)
		 :accessor line-sources)

   (seeds :initform (coll:make-collection)
	  :accessor seeds)

   (beam-vm :accessor beam-vm
	    :documentation "The beams-views-manager.")

   (line-vm :accessor line-vm
	    :documentation "The line-sources-views-manager.")

   (seed-vm :accessor seed-vm
	    :documentation "The seeds-views-manager.")

   (history :initarg :history ; past modifications
	    :accessor history)

   (dose-grid :type grid-geometry
	      :initarg :dose-grid
              :accessor dose-grid
	      :initform (make-grid-geometry)
              :documentation "The plan's dose grid specification.")

   (sum-dose :type dose-result
	     :initarg :sum-dose
	     :initform (make-dose-result)
	     :accessor sum-dose
	     :documentation "The plan's summed dose results from all
radiation sources are stored here.  It can be created blank since
results are not saved in files.")

   (dose-surfaces ; :type coll:collection
                  :initform (coll:make-collection)
                  :accessor dose-surfaces
                  :documentation "A collection of dose-surface objects")

   (plan-views :initform (coll:make-collection)
	       :accessor plan-views)
   
   (vsm :accessor vsm
	:documentation "The view-set-mediator for this plan.  Needed
to manage the locator bars that should appear in the various
cross-sectional views.")

   (dose-vm :accessor dose-vm
            :documentation "The dose-view manager.")

   (grid-vm :accessor grid-vm
            :documentation "The grid-view manager.")

   (drm :accessor drm
        :documentation "The plan's dose-result manager.")

   )

  (:default-initargs :name "" :comments '("") :plan-by ""
		     :prescription-used nil :history nil)

  (:documentation "A plan specifies how a given patient is to be
treated, but does not needlessly replicate the anatomy or other
patient information that is the same for all of a collection of
plans.")

  )

;;;--------------------------------------

(defmethod slot-type ((object plan) slotname)

  (case slotname
    ((beams line-sources seeds dose-surfaces) :collection)
    ((dose-grid sum-dose) :object)
    (patient-of :ignore)
    (time-stamp :timestamp)
    (otherwise :simple)))

;;;--------------------------------------

(defmethod not-saved ((object plan))

  (append 
   (call-next-method)
   '(new-comments new-time-stamp new-plan-by prescription-used
     sum-dose history plan-views
     beam-vm line-vm seed-vm dose-vm grid-vm vsm drm)))

;;;--------------------------------------

(defmethod (setf name) :after (text (p plan))

  (declare (ignore text))
  (setf (time-stamp p) (date-time-string)))

;;;--------------------------------------

(defmethod (setf comments) :after (text (p plan))

  (setf (time-stamp p) (date-time-string))
  (ev:announce p (new-comments p) text))

;;;--------------------------------------

(defmethod (setf plan-by) :after (text (p plan))

  (setf (time-stamp p) (date-time-string))
  (ev:announce p (new-plan-by p) text))

;;;--------------------------------------

(defmethod (setf time-stamp) :after (new-time (p plan))

  (ev:announce p (new-time-stamp p) new-time))

;;;--------------------------------------

(defun make-plan (name &rest initargs)

  "make-plan name &rest initargs

returns a plan with the specified initial values.  Certain
initialization is done here rather than in the initialize-instance
method, in order to correctly initialize object-valued slots both here
and from files."

  (let ((pl (apply #'make-instance 'plan
		   :name (if (equal name "")
			     (format nil "~A" (gensym "PLAN-"))
			   name)
		   initargs)))
    ;; update the plan's timestamp when dose grid changes
    (ev:add-notify pl (new-coords (dose-grid pl)) 
		   #'(lambda (pln a)
		       (declare (ignore a))
		       (setf (time-stamp pln) (date-time-string))))
    (ev:add-notify pl (new-voxel-size (dose-grid pl)) 
		   #'(lambda (pln a v)
		       (declare (ignore a v))
		       (setf (time-stamp pln) (date-time-string))))
    ;; set internal components of initial dose surfaces
    (dolist (s (coll:elements (dose-surfaces pl)))
      (setf (dose-grid s) (dose-grid pl)
	    (result s) (sum-dose pl)))
    ;; and arrange for each new dose surface to get set similarly
    (ev:add-notify pl (coll:inserted (dose-surfaces pl))
		   #'(lambda (pln ann ds)
		       (declare (ignore ann))
		       (setf (dose-grid ds) (dose-grid pln)
			     (result ds) (sum-dose pln))))
    (setf (grid-vm pl) (make-object-view-manager 
			(coll:make-collection (list (dose-grid pl)))
			(plan-views pl)
			#'make-grid-view-mediator))
    pl))

;;;--------------------------------------

(defmethod initialize-instance :after ((p plan) &rest initargs)

  "Takes care of things local to plans."

  (declare (ignore initargs))
  (setf (vsm p) (make-view-set-mediator (plan-views p)))
  (setf (beam-vm p) (make-object-view-manager
		     (beams p) (plan-views p)
		     #'(lambda (bm vw) ;; needed for extra parameter
			 (make-beam-view-mediator bm vw
						  (plan-views p)))))
  (setf (line-vm p) (make-object-view-manager
		     (line-sources p) (plan-views p)
		     #'make-brachy-view-mediator))
  (setf (seed-vm p) (make-object-view-manager
		     (seeds p) (plan-views p)
		     #'make-brachy-view-mediator))
  (setf (dose-vm p) (make-object-view-manager
		     (dose-surfaces p) (plan-views p)
		     #'make-dose-view-mediator))
  (setf (drm p) (make-dose-result-manager
		 :beams (beams p) :seeds (seeds p)
		 :line-sources (line-sources p)
		 :result (sum-dose p)))
  (ev:add-notify p (coll:deleted (plan-views p))
		 #'(lambda (pln vs vw)
		     (declare (ignore pln vs))
		     (destroy vw)))
  (flet ((plan-update-action (pln coll src)
	   (declare (ignore coll))
	   (ev:add-notify pln (update-plan src)
			  #'(lambda (pl s)
			      (declare (ignore s))
			      (setf (time-stamp pl) (date-time-string))))
	   (setf (time-stamp pln) (date-time-string))))
    (ev:add-notify p (coll:inserted (beams p))
		   #'plan-update-action)
    (ev:add-notify p (coll:inserted (line-sources p))
		   #'plan-update-action)
    (ev:add-notify p (coll:inserted (seeds p))
		   #'plan-update-action))
  (ev:add-notify p (coll:deleted (beams p))
		 #'(lambda (pln ann bm)
		     (declare (ignore ann bm))
                     (setf (time-stamp pln) (date-time-string))))
  (ev:add-notify p (coll:deleted (line-sources p))
		 #'(lambda (pln ann ln)
		     (declare (ignore ann ln))
                     (setf (time-stamp pln) (date-time-string))))
  (ev:add-notify p (coll:deleted (seeds p))
		 #'(lambda (pln ann sd)
		     (declare (ignore ann sd))
                     (setf (time-stamp pln) (date-time-string)))))

;;;--------------------------------------

(defmethod copy ((pl plan))

  "Copies and returns a new instance of a plan."

  (let ((new-plan (make-plan (name pl)
			     :history (history pl)
			     :dose-grid (copy (dose-grid pl))
			     :sum-dose (copy (sum-dose pl))
			     :prescription-used (prescription-used pl)
                             :plan-by (copy-seq (plan-by pl))
                             :comments (mapcar #'copy-seq
					       (comments pl)))))
    ;; copy the beams, line sources, seeds, and dose surfaces, and add
    ;; them to the new plan's respective collections
    (dolist (bm (coll:elements (beams pl)))
      (coll:insert-element (copy bm) (beams new-plan)))
    (dolist (src (coll:elements (line-sources pl)))
      (coll:insert-element (copy src) (line-sources new-plan)))
    (dolist (sd (coll:elements (seeds pl)))
      (coll:insert-element (copy sd) (seeds new-plan)))
    (dolist (ds (coll:elements (dose-surfaces pl)))
      (coll:insert-element (copy ds) (dose-surfaces new-plan)))
    (setf (time-stamp new-plan) (time-stamp pl))
    new-plan))

;;;--------------------------------------

(defmethod destroy ((p plan))

  (dolist (vw (coll:elements (plan-views p)))
    (coll:delete-element vw (plan-views p))))

;;;---------------------------------------------    
