;;;
;;; autovolume
;;;
;;; Lee Zeman's code to extend the volume editor to do a whole
;;; collection of contours at one mouse click.
;;;
;;; 11-Apr-2000 I. Kalet created from Lee Zeman's version of the
;;;  volume-editor module.
;;; 11-May-2000 I. Kalet ongoing re-engineering
;;; 30-Jun-2000 L. Zeman begins to reorganize into more efficient and
;;;  accurate functions. Creates generate-externals.
;;; 10-Jul-2000 L. Zeman finish generate-*-start functions for more accurate
;;;   contouring.
;;; 12-Jul-2000 L. Zeman removes minor bug from generate-vertebrae and set 
;;;   thresh to return nil if threshing impossible with the given criteria.
;;; 20-Jul-2000 L. Zeman removes extend-contour-v and extend-contour-h 
;;; routines. 
;;; 25-Jul-2000 L. Zeman removes start-vert-cont and debug clauses, 
;;;  adding extentions.
;;;  7-Sep-2000 L. Zeman finishes testing, removes debugging code, better
;;;  documentation.
;;; 17-Dec-2000 I. Kalet cosmetic cleanup, pass volume as parameter to
;;; remove circular dependency with volume editor.
;;;  1-May-2004 I. Kalet reorganize to eliminate remaining circular
;;; dependency with easel code, and other peculiar coding quirks.
;;; Move update-pstruct here from volume-editor.  Also use
;;; legal-contour with new flag, and remove quiet-legal-contour.
;;;

(in-package :prism)

;;;----------------------------------

(defun update-pstruct (pstr verts z)

  "update-pstruct pstr verts z

Replaces the vertices of the contour in pstruct pstr at the plane
specified by z with the vertices verts, or adds a new contour to pstr
if no contour previously existed at the given z plane, or deletes the
existing contour if an old one exist but verts is nil."

  (let ((temp-con (find z (contours pstr) 
			:key #'z
			:test #'(lambda (a b)
				  (poly:nearly-equal
				   a b *display-epsilon*)))))
    (cond
     ((and temp-con verts) (setf (vertices temp-con) verts))
     (verts (push (make-contour :z z :vertices verts)
		  (contours pstr)))
     (temp-con (setf (contours pstr) 
		 (remove temp-con (contours pstr))))))
  (ev:announce pstr (new-contours pstr)) ;; so other stuff can update
  (ev:announce pstr (update-case pstr))) ;;  "  "

;;;---------------------------------

(defun generate-externals (window level fs vol images z-min z-max)

  "generate-externals window level fs vol images z-min z-max

Attempts to generate an external contour for each image with a z coordinate
between z-min and z-max, using an already generated image as a guideline.
Thresholds at the first break."

  ;; deal with each image in turn
  (dolist (img images)
    (let ((z (vz (origin img))))
      (if (and (>= z z-min) (<= z z-max))
	  (let* ((x-start 0)
		 (y-start (round (/ (array-dimension (pixels img) 0) 2)))
		 (ppcm (pix-per-cm img))
		 (x-orig (round (* -1 ppcm (vx (origin img)))))
		 (y-orig (round (*  ppcm (vy (origin img)))))
		 ;; note: removing the -1 factor from the y-term makes
		 ;; this work. not sure why.
		 (mapped (sl:map-raw-image (pixels img) window 
					   level (range img)))
		 (size (array-dimension mapped 0))
		 (threshed (thresh mapped size size 
				   (/ sl:*num-gray-pixels* 8)
				   sl:*num-gray-pixels*))
		 (new-contour nil))
	    ;; start contour in an alternate location if this one will
	    ;; not work.
	    (if (not (equal (aref threshed x-start y-start) 0))
		(progn (format t "    Generating alternate start")
		       (setf x-start 0 y-start 0)))
	    (format t "~%Now contouring  z= ~A" z)
	    (setf new-contour
	      (poly:canonical-contour
	       (mapcar #' (lambda (coord-pair)
			    (list (cm-x (first coord-pair) x-orig ppcm)
				  (cm-y (second coord-pair) y-orig ppcm)))
			  (autocontour threshed x-start y-start 0 0 (1- size)
				       (1- size) *ce-sketch-tolerance*))))
	    (if (legal-contour new-contour t) ;; t for quiet operation
		(progn
		  ;;add new contours in.
		  (update-pstruct vol new-contour z)
		  (fs-delete-contour vol z fs)
		  (fs-add-contour vol
				  (make-contour :z z
						:vertices new-contour)
				  fs)))))))
  ;; announce new volumes, to get things updated.
  (ev:announce vol (new-contours vol))
  (ev:announce vol (update-case vol)))

;;;---------------------------------

(defun thresh (image cols rows low hi
	       &optional (check-valid? nil) (valid 255) (invalid 0))

  "thresh image cols rows lo hi check-valid?

Thresholds an image passed to it, setting the values of all pixels 
whose original values fall between low and hi to valid (defaults to 255)
and all other pixels to invalid (defaults to 0). image is an array of 
greyscale numerical values, cols * rows in size. if check-valid? is true,
thresh will return nil if the threshed image contains no positive pixels."

  (let ((empty t)
	(final (make-array (list cols rows) :element-type 'number 
			   :initial-element invalid)))
    (dotimes (i cols)
      (dotimes (j rows)
	(if (and (>= (aref image i j) low)
		 (<= (aref image i j) hi))
	    (progn (setf empty nil)
		   (setf (aref final i j) valid)))))
    (if (and check-valid? empty) nil final)))

;;;----------------------------------

(defun generate-vertebrae (window level fs vol images z-min z-max)

  "generate-vertebrae window level fs vol images z-min z-max

Attempts to generate a vertebral contour for each image with a z coordinate
between z-min and z-max, using an already generated image as a guideline.
Thresholds at the last break."

  ;; deal with each image in turn
  (dolist (img images)
    (let ((z (vz (origin img))))
      (if (and (>= z z-min) (<= z z-max))
	  (let* ;; set variables relevant to this image
	      ((new-contour nil)
	       (ppcm (pix-per-cm img))
	       (x-orig (round (* -1 ppcm (vx (origin img)))))
	       (y-orig (round (*  ppcm (vy (origin img)))))
	       ;; note: removing -1 factor from the y-term makes this work.
	       (mapped (sl:map-raw-image (pixels img) window 
					 level (range img)))
	       (size (array-dimension mapped 0))
	       (threshed (thresh mapped size size 
				 (* (/ sl:*num-gray-pixels* 8) 7) 
				 sl:*num-gray-pixels* t))
	       (x-start (round (/ size 3)))
	       (y-start (round (/ size 4)))
	       )     
	    (format t "~%Now contouring  z= ~A" z)
	    (if (null threshed) (format t " -- a null")
	      (progn (setf new-contour
		       (poly:canonical-contour
			(mapcar #'(lambda (coord-pair)
				    (list (cm-x (first coord-pair)
						x-orig ppcm)
					  (cm-y (second coord-pair)
						y-orig ppcm)))
				(autocontour threshed x-start y-start
					     0 0 (1- size) (1- size)
					     *ce-sketch-tolerance*))))
		     (if (legal-contour new-contour t)
			 (progn
			   ;; add new contours in. 
			   (update-pstruct vol new-contour z)
			   (fs-delete-contour vol z fs)
			   (fs-add-contour vol
					   (make-contour
					    :z z
					    :vertices new-contour)
					   fs))))))))
    ;; announce new volumes, to get things updated.
    (ev:announce vol (new-contours vol))
    (ev:announce vol (update-case vol))))

;;;----------------------------------

(defun generate-internal (window level z fs pe vol
			  images z-min z-max vertices)

  "generate-internal window level z fs pe vol images z-min z-max vertices

Attempts to generate an internal contour for each image with a z coordinate
between z-min and z-max, using an already generated image as a guideline.
Attempts to determine an appropriate break for thresholding, though not very
accurate."

  (let* ((threshold 0)
	 (cur-img (find z images :key #'(lambda (im) (vz (origin im)))))
	 (starts (start-int-cont pe cur-img vertices)))
    (setf threshold (aref (sl:map-raw-image (pixels cur-img) 
					    window level (range cur-img))
			  (+ 5 (pix-x (caar vertices) (x-origin pe)
				      (pix-per-cm cur-img)))
			  (pix-y (cadar vertices) (y-origin pe)
				 (pix-per-cm cur-img))))
    (format t "threshold = ~S" threshold)
    ;; deal with each image in turn
    (dolist (img images)
      (let ((z (vz (origin img))))
	(if (and (>= z z-min) (<= z z-max))
	    (let* ;; set variables relevant to this image
		((new-contour nil)
		 (ppcm (pix-per-cm img))
		 (x-orig (round (* -1 ppcm (vx (origin img)))))
		 (y-orig (round (*  ppcm (vy (origin img)))))
		 ;; note: removing -1 factor from the y-term makes this work
		 (mapped (sl:map-raw-image (pixels img) window 
					   level (range img)))
		 (size (array-dimension mapped 0))
		 (threshed (thresh mapped size size 
				   threshold  
				   sl:*num-gray-pixels* t))
		 (x-start (car starts))
		 (y-start (cadr starts)))     
	      (format t "~%Now contouring  z= ~A" z)
	      (if (null threshed) (format t " -- a null")
		(progn (setf new-contour
			 (poly:canonical-contour
			  (mapcar #' (lambda (coord-pair)
				       (list (cm-x (first coord-pair)
						   x-orig ppcm)
					     (cm-y (second coord-pair)
						   y-orig ppcm)))
				     (autocontour threshed x-start y-start
						  0 0 (1- size) (1- size)
						  *ce-sketch-tolerance*))))
		       (if (legal-contour new-contour t)
			   (progn
			     ;;add new contours in. 
			     (update-pstruct vol new-contour z)
			     (fs-delete-contour vol z fs)
			     (fs-add-contour vol
					     (make-contour
					      :z z
					      :vertices new-contour)
					     fs))))))))
      ;; announce new volumes, to get things updated.
      (ev:announce vol (new-contours vol))
      (ev:announce vol (update-case vol)))))

;;;----------------------------------

(defun threshold-int-cont (img verts window level x0 y0)

  "threshold-int-cont img verts esl

Determines the Otsu Thresholding point for a given object based
upon a user-drawn contour, but examining shade values at the corners of
a contour.  X0 and Y0 are the x-origin and y-origin from the contour
editor."

  (let ((ppcm (pix-per-cm img))
	(max-thresh 2)
	(mapped (sl:map-raw-image (pixels img) window level (range img))))
    (dolist (point verts)
      (if (> (aref mapped (pix-x (car point) x0 ppcm) 
		   (pix-y (cadr point) y0 ppcm)) max-thresh)
	  (setf max-thresh (aref mapped (pix-x (car point) x0 ppcm) 
				 (pix-y (cadr point) y0 ppcm)))))
    max-thresh))

;;;----------------------------------

(defun start-int-cont (pe img vertices)

  "start-int-cont esl img vertices

Examines a contour scan image to guess where the best starting
place for a specific organ lies. chooses a place a bit to the left of
the center of the edge of the organ in question."

  (let* ((size (array-dimension (pixels img) 0))
	 (offset (/ size 32))
	 (bounds (contour-bounding-box vertices))
	 (ppcm (pix-per-cm img))
	 (x-orig (x-origin pe)) 
	 (y-orig (y-origin pe))
	 (x-start (- (pix-x  (caar bounds) x-orig ppcm) offset))
	 (y-start (pix-y (/ (+ (cadar bounds) (cadadr bounds)) 2)
			 y-orig ppcm)))
    (list x-start y-start)))

;;;----------------------------------

(defun non-empty-img (img height width)

  "non-empty-img img height width

Examines an image in the form of an array, to determine whether the 
image is empty (no array value greater than 0)"

  (dotimes (i height)
    (dotimes (j width)
      (if (not (eq (aref img i j) 0))
	  (return-from non-empty-img t))))
  (return-from non-empty-img nil))

;;;----------------------------------

(defun contour-bounding-box (contour)

  "contour-bounding-box contour

Returns the upper left and lower right coordinates of a bounding box
for the contour."

  (let* ((point (pop contour))
	 (min-x (first point))
	 (min-y (second point))
	 (max-x (first point))
	 (max-y (second point)))
    (dolist (point contour)
      (if (< (first point) min-x) 
	  (setf min-x (first point)))
      (if (< (second point) min-y)
	  (setf min-y (second point)))
      (if (> (first point) max-x)
	  (setf max-x (first point)))
      (if (> (second point) max-y)
	  (setf max-y (second point))))
    (list (list min-x min-y) (list max-x max-y))))

;;;----------------------------------
;;; End.
