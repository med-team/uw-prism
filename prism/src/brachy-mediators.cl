;;;
;;; brachy-mediators
;;;
;;; defines brachy-view-mediator and support code
;;;
;;;  2-Jun-1996 I. Kalet created
;;; 31-Mar-1998 I. Kalet cosmetic changes
;;;  6-Oct-2002 I. Kalet with event name change, combine line and seed
;;; into single mediator class.
;;;

(in-package :prism)

;;;--------------------------------------

(defclass brachy-view-mediator (object-view-mediator)

  ()

  (:documentation "This mediator connects a brachy source, line or
  seed, with a view.")
  )

;;;--------------------------------------

(defmethod initialize-instance :after ((bvm brachy-view-mediator)
				       &rest initargs)

  (declare (ignore initargs))
  (ev:add-notify bvm (new-location (object bvm)) #'update-view)
  (ev:add-notify bvm (new-color (object bvm)) #'update-view))

;;;--------------------------------------

(defmethod destroy :after ((bvm brachy-view-mediator))

  (ev:remove-notify bvm (new-location (object bvm)))
  (ev:remove-notify bvm (new-color (object bvm))))

;;;--------------------------------------

(defun make-brachy-view-mediator (src view)

  (make-instance 'brachy-view-mediator :object src :view view))

;;;--------------------------------------
;;; End.
