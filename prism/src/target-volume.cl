;;;
;;; target-volume
;;;
;;; Target-volume creates boost and initial target-volumes from a tumor
;;; volume for treatment planning.
;;;
;;; 14-Apr-1991 S. Kromhout-Schiro - rewritten from target-vol2. to
;;; reflect use of rules instead of table of margins.
;;; 25-Nov-1991 S. Kromhout-Schiro Added nstage ruler function,
;;; changed root-mean-square algorithm to root-of-sum-of-squares,
;;; (x,y,z) margins  and decremental margins incorporated in tv
;;; algorithm, list-squared, and npms functions added.
;;; 26-Nov-1991 S. Kromhout-Schiro Changed boost-contours so that
;;; contours are calculated as tumor plus probabilistic margins.
;;; Overlap with critical organs is subtracted if critical organs are
;;; given as a parameter to the function call.
;;;  1-May-1992 S. Kromhout-Schiro Added chi-sq function.
;;; 28-Apr-1993 I. Kalet update to current Prism system and clean up
;;; 30-Jul-1993 I. Kalet finish cleanup.
;;; 22-Mar-1994 J. Unger change some parameters in code per MAS -- see
;;; 'MAS/JMU-2/3/94' below.
;;; 28-Mar-1994 J. Unger cleanup remove-overlap some.
;;; 29-Mar-1994 J. Unger move this code from :prism to :ptvt package.
;;;  4-May-1994 J. Unger split target-volume into initial-target-volume
;;; and boost-target-volume.  Each returns only a single target-volume.
;;; 31-May-1994 J. Unger if the computed margins in initial-target-volume &
;;; boost-target-volume are nil, then return a target with no contours.
;;;  3-Jul-1997 BobGian update NEARLY-xxx -> poly:NEARLY-xxx .
;;; 14-Oct-1997 BobGian update call to VERTEX-LIST-DIFFERENCE.
;;; 13-Sep-2005 I. Kalet remove generate-margins, combine initial and
;;; boost target functions, use Graham inference code instead of RULER.
;;;

(in-package :prism)

;;;----------------------------------------

(defvar *chi-sq-factor* 1.88 ; <-- MAS/JMU-2/3/94

"The 75 percentile value of chi-square for 2 degrees of freedom")

;;;----------------------------------------

(defun rms (l1 l2 l3)

  "RMS l1 l2 l3

returns a list whose elements are each the square root of the sums of
the squares of each of the elements of the lists l1, l2 and l3."

  (mapcar #'(lambda (a b c)
	      (sqrt (+ (* a a) (* b b) (* c c))))
	  l1 l2 l3))

;;;----------------------------------------

(defun copy-contour (pstr old-z new-z)

  "COPY-CONTOUR pstr old-z new-z

returns a contour with vertices from the contour in pstr that is at
old-z but with the new contour z set to new-z."

  (make-instance 'pr::contour
		 :z new-z
		 :vertices (dolist (c (pr::contours pstr))
			     (when (poly:nearly-equal (pr::z c) old-z)
			       (return (pr::vertices c))))))

;;;----------------------------------------

(defun expand-volume (vol margin-list)

   "EXPAND-VOLUME vol margin-list

Returns a list of contours generated from the contours of pstruct vol
by the specified margins."

  (let ((min-z (apply #'min (mapcar #'pr::z (pr::contours vol))))
	(max-z (apply #'max (mapcar #'pr::z (pr::contours vol)))))

    (append (list (copy-contour vol min-z (- min-z (third margin-list)))
                  (copy-contour vol max-z (+ max-z (third margin-list))))
            (mapcar #'(lambda (c)
                        (make-instance 'pr::contour
			  :z (pr::z c)
			  :vertices
			  (poly:scale-contour (pr::vertices c)
					      margin-list)))
		    (pr::contours vol)))))

;;;----------------------------------------

(defun match-contour-z (con org)

  "MATCH-CONTOUR-Z con org

returns the first contour in the list of contours of pstruct org whose
z matches the z of contour con.  Returns nil if no match."

  (find con (pr::contours org)
	:test #'(lambda (c1 c2)
		  (poly:nearly-equal (pr::z c1) (pr::z c2)))))

;;;----------------------------------------

(defun contour-difference (c1 c2 &optional c3)

  "CONTOUR-DIFFERENCE c1 c2 &optional c3

Given two contour objects, c1 and c2, returns a list of contour objects
  which enclose the region of space that remains when c2 is subtracted
  from c1."

  (mapcar #'(lambda (v)
	      (make-instance 'pr::contour :z (pr::z c1) :vertices v))
    ;;
    ;; Argument contour orientation NOT guaranteed to be CCW.
    ;; Let VERTEX-LIST-DIFFERENCE check it to be safe [opt 4th arg = NIL].
    ;;
    (poly:vertex-list-difference 
     (pr::vertices c1)
     (pr::vertices c2)
     (and c3 (pr::vertices c3)))))

;;;----------------------------------------

(defun remove-overlap (con organ-list tumor)

  "REMOVE-OVERLAP con organ-list tumor

Subtracts off the overlap with contour con from any of the contours in
the pstructs of organ-list.  If organ-list is empty, con is returned
unchanged.  The tumor is supplied to the call to contour-difference
to minimize the chance that the result of the the subtraction cuts 
across the tumor (see poly:VERTEX-LIST-DIFFERENCE documentation)."

  (let ((result (list con))
        (tumor-con (match-contour-z con tumor)))
    (dolist (org organ-list result)
      (unless (equalp (pr::name org) "PATIENT OUTLINE")
        (let ((co (match-contour-z (first result) org)))
          (when co 
            (setf result 
              (reduce #'append 
                (mapcar #'(lambda (res) 
                            (contour-difference res co tumor-con))
                   result)))))))))

;;;----------------------------------------

(defun target-volume (tumor immob &optional organ-list)

  "TARGET-VOLUME tumor immob &optional organ-list

returns a Prism target instance by expanding the contours in tumor, an
instance of Prism class tumor, accounting for immobilization by the
device specified by immob, a symbol specifying an immobilization
device or nil for no immob.  dev.  If organ-list is provided the
volumes of those organ instances will be excluded from the target volume."

  ;; (declare (special tumor))

  (inf:replace-assert-value 'site tumor (site tumor))
  (inf:replace-assert-value 'immob-type immob)
  (let* ((setup-m (inf:with-answer (setup-error ?y ?x)
		    (if (eql ?y tumor)
			(return ?x))))
	 (tumor-m (inf:with-answer (tumor-movement ?y ?x)
		    (if (eql ?y tumor)
			(return ?x))))
	 (pt-m (inf:with-answer (pt-movement ?y ?x)
		    (if (eql ?y tumor)
			(return ?x))))
	 ;;chi-squared times sqrt of sums of squares of above margins
	 ;;  = list of prob x, y, z values
	 (prob-m (mapcar #'(lambda (m) (* m *chi-sq-factor*))
			 (rms setup-m tumor-m pt-m))))
    (make-instance 'target
      :target-type (if organ-list "boost" "initial")
      :name "Planning Target Volume"
      :site (site tumor)
      :how-derived "Planning target volume tool"
      :contours
      (when prob-m ;; could be no rules for this site!
	(if organ-list ;; optionally take out overlap with critical organs
	    (reduce #'append
		    (mapcar #'(lambda (con)
				(remove-overlap con organ-list tumor))
			    (expand-volume tumor prob-m)))
	  (expand-volume tumor prob-m))))))

;;;----------------------------------------
;;; End.
