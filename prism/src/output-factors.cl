;;;
;;; output-factors
;;;
;;; Contains functions related to output-factor and its inverse lookup.
;;;
;;; 13-Mar-1998 BobGian created from excess material in beam-dose.
;;; 22-May-1998 BobGian inline inverse outputfactor lookup using
;;;   binary/linear search in INV-OUTPUTFACTOR (MULTILEAF-COLL method).
;;; 11-Jun-1998 BobGian Bug fix - raise threshold for degenerate sector
;;;   in MLC output factor sector integration.
;;; 26-Jun-1998 BobGian tighten code in INV-OUTPUTFACTOR method for
;;;    MULTILEAF-COLL (improves binary and sequential search).
;;; 03-Feb-2000 BobGian cosmetic fixes (case regularization).
;;; 08-Feb-2000 BobGian more cosmetic cleanup.
;;; 02-Mar-2000 BobGian add declarations in OUTPUTFACTOR-COL (for MLC).
;;; 29-Jun-2000 BobGian cosmetics - comments, whitespace.
;;; 30-Aug-2000 BobGian MYATAN -> FAST-ATAN.
;;; 30-May-2001 BobGian (part of upgrade to electron dosecalc):
;;;    Wrap generic arithmetic with THE-declared types.
;;;    Change a few local var names to not conflict with generic fcn names.
;;;    Cleaner return from sector-integration routine in MLC outputfactor.
;;; 03-Jan-2003 BobGian:
;;;   Flush macro FAST-ATAN - not accurate enough.
;;; 27-Feb-2003 BobGian - add THE declarations for better inlining.
;;; 01-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 31-Jan-2005 AMSimms - corrected use of Allegro specific coercions, now 
;;;   using coerce explicitly.
;;;

(in-package :prism)

;;;=============================================================

(defmethod outputfactor-col ((coll collimator) wc dosedata)

  "The RECTANGULAR collimator method for Output-Factor."

  (declare (type single-float wc))

  (the single-float
    (1d-lookup (outputfactor-vector dosedata)       ;Outputfactor Lookup.
	       wc
	       (outputfactor-fieldsizes dosedata)
	       (outputfactor-fss-mapper dosedata)
	       (outputfactor-table dosedata))))

;;;-------------------------------------------------------------

(defmethod outputfactor-col ((coll multileaf-coll) wc dosedata)

  "The MULTILEAF collimator method for Output-Factor."

  ;; NB: ALL computations done in this method are done as projected onto
  ;; the ISOCENTER plane.  Area computation and sector integration are
  ;; orientation-independent - CW or CCW both OK.
  (declare (type single-float wc)
	   (ignore wc))

  (let ((vert-list (vertices coll))
	(area-component 0.0)                        ;Accumulating area
	(integrated-component 0.0)           ;Clarkson-like integration result
	(outputfactor-min-diam (of-min-diam dosedata))
	(portal-coeff (portal-area-coeff dosedata))
	(of-vector (outputfactor-vector dosedata))
	(of-fssmap (outputfactor-fss-mapper dosedata))
	(outfactor-fieldsizes (outputfactor-fieldsizes dosedata))
	(outfactor-table (outputfactor-table dosedata)))

    ;; AREA-COMPONENT will provide [after accumulating portal area]
    ;; Output-Factor component due to portal area.
    ;;
    ;; INTEGRATED-COMPONENT is Output-Factor computed from Clarkson-like
    ;; sector-integration of portal [summed over all sectors].  This value
    ;; is clamped above a minimum value as determined by OUTPUTFACTOR-MIN-DIAM.
    ;;
    ;; SECTOR-COMPONENT is portion of integral due to single sector before
    ;; being scaled by sector angle and accumulated into INTEGRATED-COMPONENT.
    (declare (type (simple-array t 1) of-fssmap)
	     (type (simple-array single-float (3)) of-vector)
	     (type (simple-array single-float 1)
		   outfactor-fieldsizes outfactor-table)
	     (type single-float area-component integrated-component
		   outputfactor-min-diam portal-coeff))

    ;; Compute area of collimator portal by the equivalent of inlining
    ;; poly:AREA-OF-POLYGON.  Requires portal to have at least 3 vertices.
    (let* ((p0 (first vert-list))         ;First vertex - fixed, all triangles
	   (x0c (first p0))                         ;Its X coord - fixed
	   (y0c (second p0))                        ;Its Y coord - fixed
	   (p1 (second vert-list))      ;Second vertex - rotates around portal
	   (x1c (first p1))                         ;Its X coord - rotates
	   (y1c (second p1))                        ;Its Y coord - rotates
	   (p2) (x2c 0.0) (y2c 0.0)      ;Third vertex - rotates around portal
	   (ps (cddr vert-list)))                ;List whose CAR is 3rd vertex

      (declare (type single-float x0c y0c x1c y1c x2c y2c))

      (loop
	(setq p2 (car ps)                    ;Compute 3rd vertex as it rotates
	      x2c (first p2)
	      y2c (second p2))

	;; This computes twice the area of a triangle whose vertices are
	;; (x0c y0c), (x1c y1c), (x2c y2c).  Result is positive if triangle
	;; vertices are traversed CCW, negative if traversed CW.
	(incf area-component (- (+ (* x0c y1c)
				   (* y0c x2c)
				   (* x1c y2c))
				(+ (* y0c x1c)
				   (* x0c y2c)
				   (* x2c y1c))))
	(cond ((consp (setq ps (cdr ps)))
	       ;; If more vertices, pass 3rd to 2nd and loop; otherwise done.
	       (setq x1c x2c y1c y2c))
	      (t (return)))))

    ;; Accumulated area was twice actual - so multiply by 0.5 here.
    ;; Also inline ABS here - AREA-COMPONENT is always non-negative.
    (setq area-component (* 0.5 (if (>= area-component 0.0)
				    area-component
				    (- area-component))))

    ;; Clarkson-like sector integration coming up for INTEGRATED-COMPONENT.
    (do ((v1-nodes vert-list (cdr v1-nodes))
	 (v1-node) (v2-nodes) (v2-node)
	 (len-v1 0.0) (len-v2 0.0) (num-sectors 0)
	 (v1x 0.0) (v1y 0.0) (v2x 0.0) (v2y 0.0) (vjx 0.0) (vjy 0.0)
	 (v1-cross-vj 0.0) (len-vj 0.0) (perp-distance 0.0)
	 (theta-j 0.0) (theta-per-sector 0.0))
	((null v1-nodes))
	
	(declare (type single-float v1x v1y v2x v2y len-v1 len-v2 vjx vjy
		     theta-per-sector v1-cross-vj perp-distance len-vj theta-j)
	       (type fixnum num-sectors))

      ;; Vectors V1 and V2 [equivalently, nodes V1-NODE and V2-NODE] are
      ;; vertices of portal as we successively CDR down portal vertex list.
      ;; V1-NODE and V2-NODE are (X Y) coord pairs of the vertex at head of
      ;; V1 and V2 vectors, respectively.  V1X, V1Y, V2X, V2Y are X and Y
      ;; coordinates of vectors V1 and V2 from isocenter to portal vertices
      ;; V1-NODE and V2-NODE [projected onto isocenter plane].  VJ [variable
      ;; not used] is vector from V1-NODE [vertex at tail] to V2-NODE [vertex
      ;; at head].  VJX and VJY are its X and Y coordinates, respectively.
      (cond
	((eq v1-nodes vert-list)
	 ;; First time around must compute everything.  On each
	 ;; successive iteration we can pass V2-values back to V1.
	 (setq v1-node (car v1-nodes)
	       v1x (first v1-node)
	       v1y (second v1-node)
	       len-v1 (sqrt (the (single-float 0.0 *)
			      (+ (the (single-float 0.0 *) (* v1x v1x))
				 (the (single-float 0.0 *) (* v1y v1y)))))))
	(t (setq v1x v2x
		 v1y v2y
		 len-v1 len-v2)))

      (setq v2-nodes (or (cdr v1-nodes) vert-list)
	    v2-node (car v2-nodes)
	    v2x (first v2-node)
	    v2y (second v2-node))

      (setq len-v2 (sqrt (the (single-float 0.0 *)
			   (+ (the (single-float 0.0 *) (* v2x v2x))
			      (the (single-float 0.0 *) (* v2y v2y)))))
	    vjx (- v2x v1x)
	    vjy (- v2y v1y)
	    len-vj (sqrt (the (single-float 0.0 *)
			   (+ (the (single-float 0.0 *) (* vjx vjx))
			      (the (single-float 0.0 *) (* vjy vjy)))))
	    v1-cross-vj (- (* v1x vjy)
			   (* v1y vjx)))

      (setq perp-distance (cond ((< len-vj 1.0e-5) len-v1)
				((< v1-cross-vj 0.0)
				 (/ (- v1-cross-vj) len-vj))
				(t (/ v1-cross-vj len-vj))))

      ;; THETA-J and THETA-PER-SECTOR are always POSITIVE.
      (setq theta-j (the single-float
		      (abs (the single-float
			     (atan (- (* v1x v2y)   ;V1-CROSS-V2
				      (* v1y v2x))
				   (+ (* v1x v2x)   ;V1-DOT-V2
				      (* v1y v2y)))))))

      ;; If segment is degenerate, the contribution of this contour segment
      ;; to the sector integral is zero.  Thresholds are experimental.
      (unless (or (< len-v1 1.0e-5)                 ;V1 tip touches isocenter
		  (< len-v2 1.0e-5)                 ;V2 tip touches isocenter
		  (< len-vj 1.0e-5)                 ;Degenerate segment
		  (< theta-j 1.0e-6)                ;Degenerate segment
		  (< perp-distance 1.0e-5))         ;Degenerate segment

	;; Experiment with the 1 and 10.0 here.  We are currently using a
	;; minimum of 1 sector per segment, each at most 10.0 degrees
	;; pie-width angle.
	(setq num-sectors (the fixnum
			    (ceiling theta-j
				     #.(coerce (* pi (/ 10.0d0 180.0d0))
					       'single-float)))
	      theta-per-sector (/ theta-j (coerce num-sectors 'single-float)))

	(do ((psi (+ (- #.(coerce pi 'single-float)
			(the single-float
			  (abs (the single-float
				 (atan v1-cross-vj
				       (+ (* v1x vjx)   ;V1-DOT-VJ
					  (* v1y vjy)))))))
		     (* 0.5 theta-per-sector))
		  (+ psi theta-per-sector))
	     (sector-component 0.0)
	     (cnt num-sectors (the fixnum (1- cnt))))
	    ((= cnt 0)
	     ;; SECTOR-COMPONENT is always non-negative; thus
	     ;; INTEGRATED-COMPONENT should be INCREMENTED for CCW
	     ;; integration and DECREMENTED for CW integration.
	     ;; Done by reversing sign of THETA-PER-SECTOR.
	     (when (< v1-cross-vj 0.0)
	       (setq theta-per-sector (- theta-per-sector)))
	     (incf integrated-component (* sector-component theta-per-sector)))

	  (declare (type single-float psi sector-component)
		   (type fixnum cnt))

	  ;; PSI is always 0.0 < PSI < PI.
	  ;;
	  ;; Increment SECTOR-COMPONENT by Output-Factor for square field
	  ;; with same average radius.  1.782214 is ratio of side of square
	  ;; to radius of circle such that the square has same average radius
	  ;; as does the circle.  OutputFactor Lookup.
	  ;;
	  ;; The SIN calculation here is in innermost loop.  Investigate
	  ;; whether replacing it by pre-tabulated lookup helps speedup.
	  (incf sector-component
		(the single-float
		  (1d-lookup of-vector              ;OutputFactor Lookup.
			     (* 1.782214
				(/ perp-distance
				   (sin (the (single-float 0.0 *) psi))))
			     outfactor-fieldsizes
			     of-fssmap
			     outfactor-table))))))

    ;; Integrated component must be normalized by 1/2*PI since integral of
    ;; sector angle around circle gives 2*PI.  Also inline ABS, since
    ;; INTEGRATED-COMPONENT will be computed to wrong sign if sector
    ;; integration happens to traverse portal in CW rather than CCW direc.
    (setq integrated-component
	  (* #.(coerce (/ 1.0d0 (* 2.0d0 pi)) 'single-float)
	     (if (>= integrated-component 0.0)
		 integrated-component
		 (- integrated-component))))

    ;; If the portal area is at least that of a circle of diameter
    ;; OUTPUTFACTOR-MIN-DIAM, then the Output-Factor is not allowed to go
    ;; below that which a square portal would have whose area is that
    ;; of such a circular portal.
    (unless (< area-component                       ;Area of actual portal
	       ;;Area of circle of diameter OUTPUTFACTOR-MIN-DIAM.
	       (* #.(coerce (* 0.25d0 pi) 'single-float)
		  outputfactor-min-diam
		  outputfactor-min-diam))

      (let ((min-integ-component
	      ;; Get Output-Factor for square field whose size is such that
	      ;; it has same area as circle of diameter OUTPUTFACTOR-MIN-DIAM.
	      ;; The factor 0.891107 is ratio of side of square to diameter
	      ;; of circle where square has same average radius as circle.
	      (1d-lookup of-vector                  ;OutputFactor Lookup.
			 (* 0.891107 outputfactor-min-diam)
			 outfactor-fieldsizes of-fssmap outfactor-table)))

	(declare (type single-float min-integ-component))

	(when (< integrated-component min-integ-component)
	  ;; Clamp INTEGRATED-COMPONENT so it goes no lower than
	  ;; MIN-INTEG-COMPONENT.
	  (setq integrated-component min-integ-component))))

    ;; Area component is derived from portal area.  Compute Output-Factor
    ;; for equivalent square field whose side is square root of portal area.
    (setq area-component
	  (1d-lookup of-vector                      ;OutputFactor Lookup.
		     (sqrt (the (single-float 0.0 *) area-component))
		     outfactor-fieldsizes of-fssmap outfactor-table))

    ;; Weight AREA-COMPONENT by PORTAL-COEFF [0.0 <= value <= 1.0]
    ;; and INTEGRATED-COMPONENT by one minus that value.
    (+ (* portal-coeff area-component)
       (* (- 1.0 portal-coeff) integrated-component))))

;;;-------------------------------------------------------------

(defmethod inv-outputfactor ((coll collimator) wc outputfactor dosedata)

  "inv-outputfactor (coll collimator) wc outputfactor dosedata

Returns WC, the fieldsize which would produce Output-Factor
OUTPUTFACTOR for all but MLCs."

  (declare (type single-float wc outputfactor)
	   (ignore outputfactor dosedata))
  wc)

;;;-------------------------------------------------------------

(defmethod inv-outputfactor ((coll multileaf-coll) wc outputfactor dosedata)

  "inv-outputfactor (coll multileaf-coll) wc outputfactor dosedata

For MLCs, returns the fieldsize which would produce Output-Factor
OUTPUTFACTOR, computed by inverting the FieldSize/Output-Factor relation."

  (declare (type single-float wc outputfactor)
	   (ignore wc))

  ;; Inverse OutputFactor Lookup using Binary/Linear Search.
  (let ((input-table (outputfactor-table dosedata))
	(output-table (outputfactor-fieldsizes dosedata))
	(index- 0) (index+ 0) (lo-limit 0)
	(input-lowerbound 0.0)
	(input-upperbound 0.0))

    ;; Values in INPUT-TABLE array must be monotonic increasing.
    ;; INPUT-TABLE must have at least 3 slots for binary search to work.
    (declare (type (simple-array single-float 1) input-table output-table)
	     (type single-float input-lowerbound input-upperbound)
	     (type fixnum index- index+ lo-limit))

    (let* ((hi-limit (the fixnum (1- (array-total-size input-table))))
	   (idx (the fixnum (ceiling hi-limit 2))))

      (declare (type fixnum hi-limit idx))

      (cond
	((> hi-limit 8)
	 (loop
	   (setq input-lowerbound (aref input-table (the fixnum (1- idx)))
		 input-upperbound (aref input-table idx))

	   (cond
	     ((<= outputfactor input-lowerbound)
	      (setq hi-limit idx
		    idx (the fixnum
			  (+ lo-limit
			     (floor (the fixnum (- hi-limit lo-limit)) 2))))
	      (when (= idx lo-limit)
		(setq index- (setq index+ lo-limit))
		(return)))
	     ((< outputfactor input-upperbound)
	      (setq index- (the fixnum (1- idx))
		    index+ idx)
	      (return))
	     ((= outputfactor input-upperbound)
	      (setq index- (setq index+ idx))
	      (return))
	     ((< idx hi-limit)
	      (setq lo-limit idx
		    idx (the fixnum
			  (+ lo-limit
			     (the fixnum
			       (ceiling (the fixnum
					  (- hi-limit lo-limit)) 2))))))
	     (t (setq index- (setq index+ hi-limit))
		(return)))))

	;; INPUT-TABLE is too small for binary search.  Use sequential.
	(t (do ((idx 0 (the fixnum (1+ idx)))
		(old-input-value 0.0 new-input-value)
		(new-input-value 0.0))
	       ((> idx hi-limit)
		;; Ran off end - return highest IDX.
		(setq index- (setq index+ hi-limit)))

	     (declare (type single-float old-input-value new-input-value)
		      (type fixnum idx))

	     (when (<= outputfactor
		       (setq new-input-value (aref input-table idx)))
	       (cond ((or (= idx 0)
			  (= outputfactor new-input-value))
		      ;; If first iteration [input < first entry] or exact
		      ;; match, return index of exact [or first] value.
		      (setq index- (setq index+ idx)))
		     ;; Otherwise, interpolate output between values
		     ;; corresponding to input values fcn arg straddles.
		     (t (setq index- (the fixnum (1- idx))
			      index+ idx
			      input-lowerbound old-input-value
			      input-upperbound new-input-value)))
	       (return))))))

    (the single-float
      (cond ((= index- index+)
	     (aref output-table index-))
	    (t (interpolate-delta input-lowerbound
				  outputfactor
				  input-upperbound
				  (aref output-table index-)
				  (aref output-table index+)))))))

;;;=============================================================
;;; End.
