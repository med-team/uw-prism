;;;
;;; tape-measure
;;;
;;; A tape measure can appear in a view and manipulated by the user.
;;; It has a length, can be stretched and contracted, and moved.
;;;
;;;  1-Feb-1994 I. Kalet split off from old contour editor code and
;;;  reorganized as an independent entity.
;;; 28-Feb-1994 I. Kalet continue filling in details
;;; 06-Jun-1994 J. Unger work on implementation; make usable by either a
;;; planar editor or a view.
;;; 16-Jun-1994 J. Unger finish implementation.
;;; 24-Jun-1994 I. Kalet really finish implementation.
;;; 11-Jul-1994 J. Unger finish up impl, but this is a temporary impl,
;;; since there are still some design decisions to be made.
;;; 10-May-1997 I. Kalet use new global *ruler-color* for initial
;;; color of the tape measure.  Redesign to eliminate dependency on
;;; planar editor or other "owner".
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;;

(in-package :prism)

;;;-----------------------------------

(defparameter *tape-disk-radius* 3 "The radius of the grab disk on
either end of the tape measure, in pixels.")

(defparameter *tape-tic-length* 6 "The length of each tic on the tape
measure, in pixels.")

;;;-----------------------------------

(defclass tape-measure ()

  ((picture :accessor picture
	    :initarg :picture
	    :documentation "The picture in which this tape measure is
drawn.  Must be provided as an initialization argument.")

   (scale :type single-float
	  :accessor scale
	  :initarg :scale
	  :documentation "The pixels per cm from model space to
picture space.")

   (origin :type list
	   :accessor origin
	   :initarg :origin
	   :documentation "A two element list, the x and y pixel
coordinates of the origin of model space on the picture.")

   (x1 :type single-float
       :accessor x1
       :initarg :x1
       :documentation "End 1 x coordinate in model space, e.g. cm.")

   (y1 :type single-float
       :accessor y1
       :initarg :y1
       :documentation "End 1 y coordinate in model space, e.g. cm.")

   (x2 :type single-float
       :accessor x2
       :initarg :x2
       :documentation "End 2 x coordinate in model space, e.g. cm.")

   (y2 :type single-float
       :accessor y2
       :initarg :y2
       :documentation "End 2 y coordinate in model space, e.g. cm.")

   (spine ;; :type sl:segment
    :accessor spine
    :documentation "The spine of the ruler, a pickable object.")

   (end1 ;; :type sl:circle
    :accessor end1
    :documentation "One end of the ruler, a pickable object.")

   (end2 ;; :type sl:circle
    :accessor end2
    :documentation "The other end of the ruler, a pickable object.")

   (new-length :type ev:event
	       :accessor new-length
	       :initform (ev:make-event)
	       :documentation "Announced when the tape measure's
length changes.")

   (refresh :type ev:event
	    :accessor refresh
	    :initform (ev:make-event)
	    :documentation "Announced when the tape measure changes
and the picture should be redrawn.")

   (deleted :type ev:event
	    :accessor deleted
	    :initform (ev:make-event)
	    :documentation "Announced when the tape measure has
received a button 2 input on its spine, signalling to delete it.")

   )

  (:documentation "A tape measure can appear in a view or planar editor, 
and can be stretched and contracted by the user.  If its length
changes it announces new-length.")

  )

;;;-----------------------------------

(defun make-tape-measure (&rest initargs)

  (apply #'make-instance 'tape-measure initargs))

;;;-----------------------------------

(defun draw-tape-measure-tics (tpm)

  "draw-tape-measure-tics tpm 

Draws tape measure tpm into its owner's picture."

  (clx:draw-segments (sl:pixmap (picture tpm))
		     (sl:color (spine tpm))
		     (compute-tics (x1 tpm) (y1 tpm)
				   (x2 tpm) (y2 tpm)
				   (scale tpm)
				   (first (origin tpm))
				   (second (origin tpm))
				   *tape-tic-length*)))

;;;-----------------------------------

(defun tape-length (tape)

  "tape-length tape

returns the length of the tape measure."

  (distance (x1 tape) (y1 tape) (x2 tape) (y2 tape)))

;;;-----------------------------------

(defun rescale-tape (tape)

  "rescale-tape tape

Resets the coordinates of the tape's spine and endpoints, based upon
the current origin and scale of the tape's owner."

  (let* ((sp (spine tape))
         (e1 (end1 tape))
         (e2 (end2 tape))
         (x-orig (first (origin tape)))
         (y-orig (second (origin tape)))
         (scl (scale tape))
         (x1-pix (pix-x (x1 tape) x-orig scl))
         (y1-pix (pix-y (y1 tape) y-orig scl))
         (x2-pix (pix-x (x2 tape) x-orig scl))
         (y2-pix (pix-y (y2 tape) y-orig scl)))
    (setf (sl:x1 sp) x1-pix        (sl:y1 sp) y1-pix
          (sl:x2 sp) x2-pix        (sl:y2 sp) y2-pix
          (sl:x-center e1) x1-pix  (sl:y-center e1) y1-pix
          (sl:x-center e2) x2-pix  (sl:y-center e2) y2-pix)))

;;;-----------------------------------

(defmethod (setf scale) :after (new-scale (tp tape-measure))

  "Updates the model space coordinates of the tape measure, since its
pixel space coordinates don't change."

  (let ((sp (spine tp))
	(x-orig (first (origin tp)))
	(y-orig (second (origin tp))))
    (setf (x1 tp) (cm-x (sl:x1 sp) x-orig new-scale)
	  (y1 tp) (cm-y (sl:y1 sp) y-orig new-scale)
	  (x2 tp) (cm-x (sl:x2 sp) x-orig new-scale)
	  (y2 tp) (cm-y (sl:y2 sp) y-orig new-scale))
    (ev:announce tp (new-length tp) (tape-length tp))))

;;;-----------------------------------

(defmethod (setf origin) :after (new-origin (tp tape-measure))

  "Updates the model space coordinates of the tape measure, since its
pixel space coordinates don't change."

  (let ((sp (spine tp))
	(x-orig (first new-origin))
	(y-orig (second new-origin))
	(ppcm (scale tp)))
    (setf (x1 tp) (cm-x (sl:x1 sp) x-orig ppcm)
	  (y1 tp) (cm-y (sl:y1 sp) y-orig ppcm)
	  (x2 tp) (cm-x (sl:x2 sp) x-orig ppcm)
	  (y2 tp) (cm-y (sl:y2 sp) y-orig ppcm))))

;;;-----------------------------------

(defmethod initialize-instance :after ((tp tape-measure) &rest initargs)

  "Makes the pickable objects for the tape measure and defines the
action functions for them."

  (declare (ignore initargs))
  (let* ((scale (scale tp))
	 (x-origin (first (origin tp)))
	 (y-origin (second (origin tp)))
         (x1-pix (pix-x (x1 tp) x-origin scale))
         (y1-pix (pix-y (y1 tp) y-origin scale)) 
         (x2-pix (pix-x (x2 tp) x-origin scale)) 
         (y2-pix (pix-y (y2 tp) y-origin scale))
         (pic (picture tp))
	 )
    (setf 
	(spine tp) (sl:make-segment tp x1-pix y1-pix x2-pix y2-pix
				    :color (sl:color-gc *ruler-color*)
				    :tolerance 2)
	(end1 tp)  (sl:make-circle tp x1-pix y1-pix 
				   :radius *tape-disk-radius*
				   :color (sl:color-gc *ruler-color*)
				   :filled t)
	(end2 tp)  (sl:make-circle tp x2-pix y2-pix 
				   :radius *tape-disk-radius*
				   :color (sl:color-gc *ruler-color*))
	)
    (sl:add-pickable-obj (spine tp) pic)
    (sl:add-pickable-obj (end1 tp)  pic)
    (sl:add-pickable-obj (end2 tp)  pic)
  ;;; NOTE NOTE NOTE NOTE NOTE
  ;;; ------------------------
  ;;; In the case form below, the "1" clause has a call to INTERNAL
  ;;; slik code.  This issue needs to be resolved in the final impl.
  ;;; ------------------------
  ;;; NOTE NOTE NOTE NOTE NOTE
    (ev:add-notify tp (sl:selected (spine tp))
		   #'(lambda (tp sp code x y)
		       (case code
			 (1 ;; this is currently a call to unexported
			  ;; code in slik
			  (setf (sl::last-x sp) x (sl::last-y sp) y))
			 (2 (destroy tp))
			 (3 (let ((new-col (sl:color-gc
					    (sl:popup-color-menu))))
			      (when new-col
				(setf (sl:color (spine tp)) new-col
				      (sl:color (end1 tp))  new-col
				      (sl:color (end2 tp))  new-col)
				(ev:announce tp (refresh tp))))
			    (setf (sl:active sp) nil)))))
    (ev:add-notify tp (sl:motion (end1 tp))
		   #'(lambda (tp e1 xp yp state)
		       (when (member :button-1
				     (clx:make-state-keys state))
			 (let* ((ppcm (scale tp))
				(sp (spine tp)))
			   (sl:update-pickable-object e1 xp yp)
			   (setf (sl:x1 sp) xp
				 (sl:y1 sp) yp)
			   (setf (x1 tp)
			     (cm-x xp (first (origin tp)) ppcm))
			   (setf (y1 tp)
			     (cm-y yp (second (origin tp)) ppcm))
			   (ev:announce tp (refresh tp))
			   (ev:announce tp (new-length tp)
					(tape-length tp))))))
    (ev:add-notify tp (sl:motion (end2 tp))
		   #'(lambda (tp e2 xp yp state)
		       (when (member :button-1
				     (clx:make-state-keys state))
			 (let* ((ppcm (scale tp))
				(sp (spine tp)))
			   (sl:update-pickable-object e2 xp yp)
			   (setf (sl:x2 sp) xp
				 (sl:y2 sp) yp)
			   (setf (x2 tp)
			     (cm-x xp (first (origin tp)) ppcm))
			   (setf (y2 tp)
			     (cm-y yp (second (origin tp)) ppcm))
			   (ev:announce tp (refresh tp))
			   (ev:announce tp (new-length tp)
					(tape-length tp))))))
    (ev:add-notify tp (sl:motion (spine tp))
		   #'(lambda (tp sp xp yp state)
		       (when (member :button-1 (clx:make-state-keys state))
			 (let* ((ppcm (scale tp))
				(x-orig (first (origin tp)))
				(y-orig (second (origin tp))))
			   (sl:update-pickable-object sp xp yp)
			   (sl:update-pickable-object (end1 tp)
						      (sl:x1 sp)
						      (sl:y1 sp))
			   (sl:update-pickable-object (end2 tp)
						      (sl:x2 sp)
						      (sl:y2 sp))
			   (setf (x1 tp) (cm-x (sl:x1 sp) x-orig ppcm)
				 (y1 tp) (cm-y (sl:y1 sp) y-orig ppcm)
				 (x2 tp) (cm-x (sl:x2 sp) x-orig ppcm)
				 (y2 tp) (cm-y (sl:y2 sp) y-orig ppcm))
			   (ev:announce tp (refresh tp))))))
    ))

;;;-----------------------------------

(defmethod destroy ((tp tape-measure))

  (sl:remove-pickable-objs tp (picture tp))
  (ev:announce tp (deleted tp)))

;;;-----------------------------------
;;; End.
