;;;
;;; beam-mediators
;;;
;;; defines beam-view-mediator and support code
;;;
;;; 18-Jan-1993 I. Kalet move add-notify code from beams-eye-views
;;; that updates the beams-eye-view slots to beam-view-mediator
;;; 15-Feb-1993 I. Kalet add action for new-color announcement and
;;; new-machine announcement in beam-view-mediator
;;; 15-Apr-1993 I. Kalet handle new-coll-set announcement from
;;; collimators
;;; 22-Jul-1993 I. Kalet put add and remove notify for BEV here in
;;; beam-view-mediator.
;;;  5-Sep-1993 I. Kalet split off from beam-graphics module
;;; 18-Oct-1993 I. Kalet make destroy an :after method, not primary
;;;  2-Jun-1994 I. Kalet add notifications for beam block insertion,
;;;  deletion, and cleanup when mediator is destroyed.
;;; 21-Jun-1994 I. Kalet add code to destroy method to delete a beam's
;;; eye view when its beam is deleted.
;;; 28-Jun-1994 J. Unger add code to handle view updates when wedge
;;; rotation changes.
;;;  3-Oct-1994 J. Unger redraw view when axis-changed.
;;; 12-Jan-1995 I. Kalet add plan-of here so can remove from beams and
;;; views.
;;;  7-Sep-1995 I. Kalet unregister block events in destroy, also
;;;  handle wedge-id and block display-color.
;;;  8-Oct-1996 I. Kalet fix error in registration for block inserted.
;;;  Provide an :after method for update-view, to draw the blocks.
;;;  Draw the blocks in the initialization also, since the basic
;;;  object-view-mediator will only draw the beam.  In initialization
;;;  of the beam-view-mediator, replace the general action for
;;;  refresh-fg with one that draws the blocks as well as the beam.
;;; 20-May-1997 I. Kalet use plan view set in constructor function
;;; instead of plan, to avoid circularity with plan definition.
;;; 11-Mar-2001 I. Kalet update name of BEV when name of beam for that
;;; view changes.
;;;

(in-package :prism)

;;;----------------------------------------------

(defclass beam-view-mediator (object-view-mediator)

  ((view-set :initarg :view-set
	     :accessor view-set
	     :documentation "The set of views of the plan containing
the beam, needed to delete a bev for a beam that is deleted.")
   )

  (:documentation "This mediator connects a beam with a view.")
  )

;;;----------------------------------------------

(defun make-beam-view-mediator (beam view vset)

  (make-instance 'beam-view-mediator
    :object beam :view view :view-set vset))

;;;----------------------------------------------

(defmethod initialize-instance :after ((bvm beam-view-mediator)
                                        &rest initargs)

  (declare (ignore initargs))
  (let ((bm (object bvm))
	(vw (view bvm)))
    (ev:add-notify bvm (new-coll-angle bm) #'update-view)
    (ev:add-notify bvm (new-color bm) #'update-view)
    (ev:add-notify bvm (axis-changed bm) #'update-view)
    (ev:add-notify bvm (new-coll-set (collimator bm)) #'update-view)
    (ev:add-notify bvm (new-id (wedge bm)) #'update-view)
    (ev:add-notify bvm (new-rotation (wedge bm)) #'update-view)
    (ev:add-notify bvm (new-machine bm)
		   #'(lambda (med b mach)
		       (declare (ignore mach))
		       (ev:add-notify med (new-coll-set (collimator b))
				      #'update-view)
		       (update-view med b)))
    (ev:add-notify bvm (coll:inserted (blocks bm))
		   #'(lambda (med blk-set blk)
		       (declare (ignore blk-set))
		       (ev:add-notify med (new-vertices blk)
				      #'update-view)
		       (ev:add-notify med (new-color blk)
				      #'update-view)
		       (update-view med blk)))
    (ev:add-notify bvm (coll:deleted (blocks bm))
		   #'(lambda (med blk-set blk)
		       (declare (ignore blk-set))
		       (let ((mvw (view med)))
			 (setf (foreground mvw)
			   (remove blk (foreground mvw) :key #'object))
			 (display-view mvw))))
    (ev:add-notify bvm (refresh-fg vw) ;; replaces the general one
		   #'(lambda (med v)
		       (let ((b (object med)))
			 (draw b v) ;; draw the beam and the blocks
			 (dolist (bl (coll:elements (blocks b)))
			   (draw-beam-block bl v b)))))
    ;; initially register with and draw the blocks
    (dolist (blk (coll:elements (blocks bm)))
      (ev:add-notify bvm (new-vertices blk) #'update-view)
      (ev:add-notify bvm (new-color blk) #'update-view)
      (draw-beam-block blk vw bm))
    ;; which view redraw depends on whether it is a BEV for this beam
    (if (and (typep vw 'beams-eye-view) (eq bm (beam-for vw)))
	(progn
	  (ev:add-notify vw (new-gantry-angle bm) #'refresh-bev)
	  (ev:add-notify vw (new-couch-angle bm) #'refresh-bev)
	  (ev:add-notify vw (new-couch-lat bm) #'refresh-bev)
	  (ev:add-notify vw (new-couch-ht bm) #'refresh-bev)
	  (ev:add-notify vw (new-couch-long bm) #'refresh-bev)
	  (ev:add-notify vw (new-machine bm) #'refresh-bev)
	  (ev:add-notify vw (new-name bm)
			 #'(lambda (v b newname)
			     (declare (ignore b))
			     (setf (name v)
			       (format nil "BEV for ~A" newname)))))
      (progn
	(ev:add-notify bvm (new-gantry-angle bm) #'update-view)
	(ev:add-notify bvm (new-couch-angle bm) #'update-view)
	(ev:add-notify bvm (new-couch-lat bm) #'update-view)
	(ev:add-notify bvm (new-couch-ht bm) #'update-view)
	(ev:add-notify bvm (new-couch-long bm) #'update-view))
      )))

;;;----------------------------------------------

(defmethod destroy :before ((bvm beam-view-mediator))

  (let ((bm (object bvm))
	(vw (view bvm)))
    (dolist (blk (coll:elements (blocks bm)))
      (setf (foreground vw)
	(remove blk (foreground vw) :key #'object)))))

;;;----------------------------------------------

(defmethod destroy :after ((bvm beam-view-mediator))

  (let ((bm (object bvm))
	(vw (view bvm)))
    (ev:remove-notify bvm (new-coll-angle bm))
    (ev:remove-notify bvm (new-color bm))
    (ev:remove-notify bvm (axis-changed bm))
    (ev:remove-notify bvm (new-coll-set (collimator bm)))
    (ev:remove-notify bvm (new-machine bm))
    (ev:remove-notify bvm (coll:inserted (blocks bm)))
    (ev:remove-notify bvm (coll:deleted (blocks bm)))
    (ev:remove-notify bvm (new-id (wedge bm)))
    (ev:remove-notify bvm (new-rotation (wedge bm)))
    (dolist (blk (coll:elements (blocks bm)))
      (ev:remove-notify bvm (new-vertices blk))
      (ev:remove-notify bvm (new-color blk)))
    (if (and (typep vw 'beams-eye-view) (eq bm (beam-for vw)))
	(progn
	  (ev:remove-notify vw (new-gantry-angle bm))
	  (ev:remove-notify vw (new-couch-angle bm))
	  (ev:remove-notify vw (new-couch-lat bm))
	  (ev:remove-notify vw (new-couch-ht bm))
	  (ev:remove-notify vw (new-couch-long bm))
	  (ev:remove-notify vw (new-machine bm))
	  (let ((vs (view-set bvm)))
	    (when (coll:collection-member vw vs) ;; if not deleted
	      (coll:delete-element vw vs)))) ;; then delete it
      (progn
	(ev:remove-notify bvm (new-gantry-angle bm))
	(ev:remove-notify bvm (new-couch-angle bm))
	(ev:remove-notify bvm (new-couch-lat bm))
	(ev:remove-notify bvm (new-couch-ht bm))
	(ev:remove-notify bvm (new-couch-long bm))))))

;;;----------------------------------------------

(defmethod update-view :after ((med beam-view-mediator) obj
			       &rest pars)

  "draws the blocks after the beam is drawn by the primary method."

  (declare (ignore obj pars))
  (let ((bm (object med))
	(vw (view med)))
    (dolist (blk (coll:elements (blocks bm)))
      (draw-beam-block blk vw bm))))

;;;----------------------------------------------
;;; End.
