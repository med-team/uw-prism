;;;
;;; margin-rules
;;;
;;; 13-Sep-2005 I. Kalet transcribed from Sharon Kromhout-Schiro's
;;; work to use Graham inference code instead of RULER.
;;; 25-Jun-2008 I. Kalet move use-package inference to prism defpackage
;;;

(in-package :prism)

;;;------------------------------------------------------------------
;;; Measurements in cm 
;;; x: right-left
;;; y: front-back
;;; z: sup-inf
;;;------------------------------------------------------------------

;;;------------------------------------------------------------------
;;; Head and neck rules
;;;------------------------------------------------------------------

(<- (setup-error ?x (0.8 0.8 0.8))  ;; (0.8,?,?) Verhey82,
    ;; approved SH 3/5/92 
    (AND (within ?x head-and-neck)
	 (immob-type none)))

(<- (pt-movement ?x (0.3 0.3 0.3)) ;; MAS/JMU-2/3/94, approved SH 3/5/92
    (AND (within ?x head-and-neck)
	 (immob-type none)))

(<- (setup-error ?x (0.5 0.5 0.5))   ;; SH 3/5/92
    (AND (within ?x head-and-neck)
	 (immob-type mask)))

(<- (pt-movement ?x (0.1 0.1 0.1)) ;; MAS/JMU-2/3/94
    (AND (within ?x head-and-neck)
	 (immob-type mask)))

;;;---------------------------------------------------------------
;;; Nasopharynx rules

(<- (tumor-movement ?x (0.0 0.0 0.0))
    (within ?x nasopharynx))

;;;----------------------------------------------------------
;;; rules for lung
;;;----------------------------------------------------------

(<- (tumor-movement ?x (0.0 0.6 1.0))  ;; Ross89, West74 (z)
    (AND (within ?x lung)
	 (region ?x nil)))

(<- (tumor-movement ?x (0.0 0.0 0.0))  ;; MAS-2/26/92
    (AND (within ?x lung)
	 (fixed ?x yes)))

(<- (tumor-movement ?x (0.0 0.6 0.0)) ;; Ross89
    (AND (within ?x lung)
	 (region ?x upper-lobe)))

(<- (tumor-movement ?x (0.9 0.0 0.0)) ;; Ross89, MAS 2/26/92
    (AND (within ?x lung)
	 (region ?x hilum)))

(<- (tumor-movement ?x (0.8 0.0 0.0)) ;; Ross89, MAS 2/26/92
    (AND (within ?x lung)
	 (region ?x mediastinum)))

(<- (tumor-movement ?x (0.5 0.5 1.0)) ;; Ross89, West74 (z), MAS 2/26/92
    (AND (within ?x lung)
	 (region ?x lower-lobe)))

(<- (setup-error ?x (0.8 0.8 0.8)) ;; MAS/JMU-2/3/94, approved SH 3/5/92
    (AND (within ?x lung)
	 (immob-type none)))

(<- (setup-error ?x (0.6 0.6 0.6)) ;; MAS/JMU-2/3/94, approved SH 3/5/92
    (AND (within ?x lung)
	 (immob-type alpha-cradle)))

;; check these numbers!!
(<- (setup-error ?x (0.4 0.4 0.4)) ;; MAS/JMU-2/3/94
    (AND (within ?x lung)
	 (immob-type plaster-shell)))

(<- (pt-movement ?x (0.4 0.4 0.4)) ;; MAS/JMU-2/3/94, approved SH 3/5/92
    (AND (within ?x lung)
	 (immob-type none)))

(<- (pt-movement ?x (0.2 0.2 0.2)) ;; MAS/JMU-2/3/94, approved SH 3/5/92
    (AND (within ?x lung)
	 (immob-type alpha-cradle)))

;; check these numbers!!
(<- (pt-movement ?x (0.1 0.1 0.1)) ;; MAS/JMU-2/3/94
    (AND (within ?x lung)
	 (immob-type plaster-shell)))

;;;-------------------------------------------------------
;;; End.
