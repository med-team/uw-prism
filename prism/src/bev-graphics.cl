;;;
;;; bev-graphics
;;;
;;; Defines draw methods for contours and volumes in beams-eye-views
;;;
;;;  1-Apr-1994 I. Kalet extracted from contour-graphics and
;;;  volume-graphics to reduce dependencies.
;;; 18-Apr-1994 I. Kalet changed refs to view origin
;;; 25-Apr-1994 J. Unger add draw method for points into bev
;;;  1-Jun-1994 I. Kalet add bev-draw-all here, taken from coll-panels
;;;  7-Jul-1994 J. Unger add drawing of points to bev-draw-all.
;;;  7-Sep-1994 J. Unger reorder drawing in bev-draw-all so tumor drawn
;;; last, and appears 'above' organs.
;;; 10-Oct-1994 J. Unger fix omission in mark bev draw method that caused
;;; points not to draw correctly for bev planes off the isocenter.
;;; 12-Jan-1995 I. Kalet use isodist function.  Pass plan and patient to
;;; bev-draw-all.
;;;  5-Sep-1995 I. Kalet eliminate some local variables to improve
;;;  performance.  Also, absorb draw method for contour in bev into
;;;  draw method for pstruct, and rearrange code for speed.
;;;  9-Oct-1996 I. Kalet explicitly draw blocks in
;;; bev-draw-all, since the beam draw method does not do it anymore.
;;; Also, make parameters to bev-draw-all required, not keywords.
;;; Also, move draw method for beams-eye-view and other beam in
;;; beams-eye-view code here from beam-graphics.  Add package name for
;;; find-dashed-color, now in SLIK.  Move marker constants here from
;;; beam-graphics, used only here.
;;;  5-Dec-1996 I. Kalet don't generate graphic primitives if color is
;;;  invisible
;;; 12-Dec-1996 I. Kalet pass vertices, not portal, to draw-bev-wedge
;;; 24-Jan-1997 I. Kalet eliminate reference to geometry package. Also
;;; portal is now just the vertices, not a contour object.
;;; 10-May-1997 I. Kalet move bev-draw-all from here to separate file
;;; to eliminate circularity.
;;; 20-Jan-1998 I. Kalet beam transform now array, not multiple
;;; values, and array cached in bev, not individual slots.
;;; 21-Apr-1999 I. Kalet change sl:invisible to 'sl:invisible.
;;; 23-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;; 23-Oct-1999 I. Kalet preserve declutter information if beam was
;;; already in view - the visible attribute of the graphic prim.
;;; 30-Jul-2002 I. Kalet slight mod for point method to keep
;;; consistent with addition of :around general method.
;;;

(in-package :prism)

;;;--------------------------------------

(defconstant *bev-marker-radius* 2 "Determines the size of markers along
the primary beam's portal outline (and those of its blocks) in a bev.")

(defconstant *bev-marker-size* (* 2 *bev-marker-radius*) "Twice the radius")

;;;--------------------------------------

(defmethod draw ((pstr pstruct) (bev beams-eye-view))

  "draw (pstr pstruct) (bev beams-eye-view)

This method draws all the contours in the pstruct into a beam's eye
view."

  (if (eql (display-color pstr) 'sl:invisible)
      (setf (foreground bev) (remove pstr (foreground bev) :key #'object))
    (let* ((prim (find pstr (foreground bev) :key #'object))
	   (color (sl:color-gc (display-color pstr)
			       (sl:colormap (picture bev))))
	   (bev-tr (bev-transform bev))
	   (r00 (aref bev-tr 0))
	   (r01 (aref bev-tr 1))
	   (r02 (aref bev-tr 2))
	   (r03 (aref bev-tr 3))
	   (r10 (aref bev-tr 4))
	   (r11 (aref bev-tr 5))
	   (r12 (aref bev-tr 6))
	   (r13 (aref bev-tr 7))
	   (r20 (aref bev-tr 8))
	   (r21 (aref bev-tr 9))
	   (r22 (aref bev-tr 10))
	   (r23 (aref bev-tr 11))
	   (sid (isodist (beam-for bev)))
	   (diffpix (* (the single-float (scale bev))
		       (- sid (the single-float (view-position bev)))))
	   (xorig (x-origin bev))
	   (yorig (y-origin bev))
	   (fac 0.0))
      (declare (single-float r00 r01 r02 r03 r10 r11 r12 r13
			     r20 r21 r22 r23 sid diffpix fac)
	       (fixnum xorig yorig)
	       (type (simple-array single-float (12)) bev-tr))
      (unless prim
	(setq prim (make-lines-prim nil color :object pstr))
	(push prim (foreground bev)))
      (setf (color prim) color
	    (points prim) nil)
      (dolist (con (contours pstr))
	;; no need to have separate draw method for contour in bev -
	;; just do it all right here for efficiency.
	(let* ((px 0.0)
	       (py 0.0)
	       (pz (z con))
	       (z0 (+ (* r02 pz) r03)) ;; cache loop invariants
	       (z1 (+ (* r12 pz) r13))
	       (z2 (+ (* r22 pz) r23))
	       (pix-list nil))
	  (declare (single-float px py pz z0 z1 z2))
	  (dolist (pt (vertices con))
	    (setq px (first pt) 
		  py (second pt))
	    (setq fac (/ diffpix (- sid (+ (* r20 px) (* r21 py) z2))))
	    (push (- yorig (round (* fac (+ (* r10 px) (* r11 py) z1))))
		  pix-list) ;; push y first
	    (push (+ xorig (round (* fac (+ (* r00 px) (* r01 py) z0))))
		  pix-list)) ;; then x
	  (push (nconc pix-list (list (first pix-list) (second pix-list)))
		(points prim)))))))

;;;--------------------------------------

(defmethod draw ((pt mark) (bev beams-eye-view))

  "draw (pt mark) (bev beams-eye-view)

This method draws a point in a beam's eye view."

  (let* ((s-prim (find-if #'(lambda (prim) 
			      (and (eq (object prim) pt) 
				   (typep prim 'segments-prim)))
			  (foreground bev)))
	 (c-prim  (find-if #'(lambda (prim) 
			       (and (eq (object prim) pt) 
				    (typep prim 'characters-prim)))
			   (foreground bev)))
	 (color (sl:color-gc (display-color pt)
			     (sl:colormap (picture bev))))
	 (bev-tr (bev-transform bev))
	 (px (x pt))
	 (py (y pt))
	 (pz (z pt))
	 (sid (isodist (beam-for bev)))
	 (fac (/ (- sid (the single-float (view-position bev)))
		 (- sid (+ (* (aref bev-tr 8) px)
			   (* (aref bev-tr 9) py)
			   (* (aref bev-tr 10) pz)
			   (aref bev-tr 11)))))
	 (ppcm (scale bev)))
    (declare (single-float px py pz sid fac ppcm)
	     (type (simple-array single-float (12)) bev-tr))
    (unless s-prim 
      (setq s-prim (make-segments-prim nil color :object pt))
      (push s-prim (foreground bev))
      (setq c-prim (make-characters-prim nil nil nil color :object pt))
      (push c-prim (foreground bev)))
    (setf (color s-prim) color)
    (setf (color c-prim) color)
    (setf (characters c-prim) (write-to-string (id pt)))
    (multiple-value-bind (hatchmark x-anchor y-anchor)
	(pixel-point (+ (* (aref bev-tr 0) px)
			(* (aref bev-tr 1) py)
			(* (aref bev-tr 2) pz)
			(aref bev-tr 3))
		     (+ (* (aref bev-tr 4) px)
			(* (aref bev-tr 5) py)
			(* (aref bev-tr 6) pz)
			(aref bev-tr 7))
		     (* ppcm fac)
		     (x-origin bev)
		     (y-origin bev))
      (setf (points s-prim) hatchmark)
      (setf (x c-prim) x-anchor)
      (setf (y c-prim) y-anchor))))

;;;----------------------------------------------

(defmethod draw ((b beam) (v beams-eye-view))

  "draw (b beam) (v beams-eye-view)

Computes the projection of beam b into beams-eye-view v and adds two
graphics primitives, solid and dashed, containing the projected
segments to v's foreground display list.  This includes the drawing of
the beam's isocenter and central axis, and the wedge.  Does NOT draw
the beam's blocks."

  (if (eql (display-color b) 'sl:invisible)
      (setf (foreground v) (remove b (foreground v) :key #'object))
    (if (eq b (beam-for v)) (draw-primary-beam-into-bev b v)
      (progn
	;; start with new gp's each time, to avoid having to look for 
	;; and disambiguate the solid and dashed segment-prims, which
	;; would be very complicated, but first catch the visible
	;; attribute of a beam graphic prim if present.
	(let ((visible (aif (find b (foreground v) :key #'object)
			    (visible it) t)))
	  (setf (foreground v) (remove b (foreground v) :key #'object))
	  (let* ((pic (picture v))
		 (solid-clr (sl:color-gc (display-color b)
					 (sl:colormap pic)))
		 (solid-prim (get-segments-prim b v solid-clr))
		 (dashed-prim (get-segments-prim
			       b v
			       (sl:find-dashed-color solid-clr)))
		 (bt (beam-transform b v))
		 (sad (isodist (beam-for v)))
		 (scale (* (scale v) (/ (- sad (the single-float
						 (view-position v)))
					sad)))
		 (x-orig (x-origin v))
		 (y-orig (y-origin v))
		 (wdg (wedge b)))
	    (setf (visible solid-prim) visible)
	    (setf (visible dashed-prim) visible)
	    (draw-portal dashed-prim (portal (collimator b)) bt sad v)
	    (draw-isocenter solid-prim bt scale x-orig y-orig)
	    (when (display-axis b)
	      (draw-central-axis solid-prim bt sad scale x-orig y-orig))
	    (unless (zerop (id wdg))
	      (draw-wedge solid-prim
			  (beam-transform b v t)
			  sad
			  (rotation wdg) 
			  scale x-orig y-orig
			  (sl:width pic) (sl:height pic)))))))))

;;;----------------------------------------------

(defun draw-primary-beam-into-bev (b v)

  "draw-primary-beam-into-bev b v

Draws beam b into view v.  The view is assumed to be a beam's eye
view, and the beam is assumed to be the primary beam for the view.
Draws the wedge also."

  ;; start with new gp's each time, to avoid having to look for and
  ;; disambiguate the solid and dashed segment-prims, which would be
  ;; very complicated.  But first catch the visible attribute of a
  ;; beam graphic prim if present.
  (let ((visible (aif (find b (foreground v) :key #'object)
		      (visible it) t)))
    (setf (foreground v) (remove b (foreground v) :key #'object))
    (let* ((pic (picture v))
	   (solid-clr (sl:color-gc (display-color b)
				   (sl:colormap pic)))
	   (solid-prim (get-segments-prim b v solid-clr))
	   (dashed-prim (get-segments-prim
			 b v (sl:find-dashed-color solid-clr)))
	   (marker-prim (get-rectangles-prim b v solid-clr))
	   (col-ang (* (the single-float (collimator-angle b))
		       *pi-over-180*))
	   (adj-col-ang (if (typep (collimator b) 'multileaf-coll) 
			    0.0
			  col-ang))
	   (portal (portal (collimator b)))
	   (sad (isodist b))
	   (wdg (wedge b))
	   (scale (scale v))
	   (x0 (x-origin v))
	   (y0 (y-origin v)))
      (setf (visible solid-prim) visible)
      (setf (visible dashed-prim) visible)
      (setf (visible marker-prim) visible)
      (draw-primary-portal dashed-prim marker-prim
			   portal adj-col-ang sad v)
      ;; draw isocenter plus sign in middle of view
      (setf (points solid-prim)
	(append (draw-plus-icon '(0.0 0.0) scale x0 y0 *isocenter-radius*)
		(points solid-prim)))
      (unless (zerop (id wdg))
	(draw-bev-wedge solid-prim
			portal
			(mapcar #'vertices (coll:elements (blocks b)))
			col-ang sad
			(rotation wdg)
			(view-position v)
			scale x0 y0
			(sl:width pic) (sl:height pic))))))

;;;----------------------------------------------

(defun get-bev-markers (verts)

  "get-bev-markers verts

Returns a list of (ulc-x ulc-y width height) 4-tuples, suitable for
insertion into a rectangles-prim's rectangles list, and subsequent
drawing by clx:draw-rectangles."

  (do ((vts verts (nthcdr 4 vts))
       (rects nil))
      ((null vts) rects)
    ;; push the four components of the tuple onto rects, backwards
    (push *bev-marker-size* rects)
    (push *bev-marker-size* rects)   
    (push (- (the fixnum (second vts)) *bev-marker-radius*)
	  rects)
    (push (- (the fixnum (first vts)) *bev-marker-radius*)
	  rects)))

;;;----------------------------------------------

(defun draw-primary-portal (b-prim m-prim portal col-ang sad bev)

  "draw-primary-portal b-prim m-prim portal col-ang sad bev

Draws portal for object obj into view bev's foreground, in color clr,
using collimator angle col-ang and source-to-axis distance sad.  The
portal drawn is a beam portal contour or block contour for the primary
beam in a beam's eye view."

  (let* ((sin-c (sin col-ang))
         (cos-c (cos col-ang))
         (pt-x 0.0) (pt-y 0.0)
         (xt 0.0) (yt 0.0)
         (old-xt 0.0) (old-yt 0.0)
         (last-pt (first (last portal)))
         (last-x (first last-pt))
         (last-y (second last-pt))
         (fac (/ (- sad (the single-float (view-position bev)))
		 sad))
         (proj-list nil)
	 (verts nil))
    (declare (single-float sin-c cos-c pt-x pt-y xt yt old-xt old-yt
			   last-x last-y fac))
    (setq old-xt (* fac (- (* last-x cos-c) (* last-y sin-c)))
          old-yt (* fac (+ (* last-x sin-c) (* last-y cos-c))))
    (dolist (pt portal)
      (setq pt-x (first pt) 
            pt-y (second pt)
            xt (* fac (- (* pt-x cos-c) (* pt-y sin-c)))
            yt (* fac (+ (* pt-x sin-c) (* pt-y cos-c))))
      (push (list old-xt old-yt xt yt) proj-list)
      (setq old-xt xt old-yt yt))
    (setf verts (pixel-segments proj-list
				(scale bev)
				(x-origin bev)
				(y-origin bev)))
    (setf (points b-prim) verts)
    (setf (rectangles m-prim) (get-bev-markers verts))))

;;;--------------------------------------
;;; End.
