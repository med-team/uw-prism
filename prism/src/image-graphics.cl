;;;
;;; image-graphics
;;;
;;; the draw methods for medical images in views
;;;
;;; 30-Jul-2000 I. Kalet split off from medical images, to make things
;;; more modular.
;;;  6-Aug-2000 I. Kalet move get-transverse-image back to
;;; medical-images, since not view related.
;;;  3-Sep-2000 I. Kalet take out resizing of image - not needed.
;;;  7-Nov-2000 I. Kalet fix DRR size and position according to beam
;;; and image data set, not the view, since GL rescales it anyway.
;;; 13-Dec-2000 I. Kalet add use of drr-cache in beam, to avoid
;;; unnecessary recomputing of DRR for beam's eye view, MLC panel,
;;; block panel, and electron portal editor.
;;;  2-Oct-2002 I. Kalet punt on generate-image-from-set for views
;;; that don't have more specific methods
;;;  3-Jan-2009 I. Kalet change draw method to use CLX instead of
;;; OpenGL - do pan and zoom with scale-image and call write-image-clx.
;;;

(in-package :prism)

;;;------------------------------------------

(defmethod draw ((im image-2d) (v view))

  "Draws image im in view v.  Same code for almost all types of views
- the caller must provide the right image data for whatever view is
drawn into."

  (let* ((scale (* (scale v)
		   (if (typep v 'beams-eye-view)
		       (/ (- (isodist (beam-for v))
			     (view-position v))
			  (isodist (beam-for v)))
		     1.0)))
	 (im-ppcm (pix-per-cm im))
	 (mag (/ scale im-ppcm))
	 (im-x0 (- (round (* (view-x0-from-image v im) im-ppcm))))
	 (im-y0 (round (* (view-y0-from-image v im) im-ppcm)))
	 (x0 (- im-x0 (/ (x-origin v) mag)))
	 (y0 (- im-y0 (/ (y-origin v) mag)))
	 (imtmp (or (image-cache v)
		    (setf (image-cache v)
		      (sl:map-image (sl:make-graymap (window v) (level v)
						     (range im))
				    (pixels im))))))
    (scale-image imtmp (scaled-image v) mag x0 y0)
    (sl:write-image-clx (scaled-image v) (background v))))

;;;------------------------------------------

(defmethod view-x0-from-image ((v transverse-view) im)

  "returns the appropriate coordinate corresponding to the type of
view."

  (vx (origin im)))

;;;------------------------------------------

(defmethod view-x0-from-image ((v coronal-view) im)

  (vx (origin im)))

;;;------------------------------------------

(defmethod view-x0-from-image ((v sagittal-view) im)

  (vz (origin im)))

;;;------------------------------------------

(defmethod view-x0-from-image ((v beams-eye-view) im)

  (vx (origin im)))

;;;------------------------------------------

(defmethod view-y0-from-image ((v transverse-view) im)

  "returns the appropriate coordinate corresponding to the type of
view."

  (vy (origin im)))

;;;------------------------------------------

(defmethod view-y0-from-image ((v coronal-view) im)

  (- (vz (origin im))))

;;;------------------------------------------

(defmethod view-y0-from-image ((v sagittal-view) im)

  (vy (origin im)))

;;;------------------------------------------

(defmethod view-y0-from-image ((v beams-eye-view) im)

  (vy (origin im)))

;;;------------------------------------------

(defmethod view-pos-from-image ((v transverse-view) im)

  "returns the appropriate coordinate corresponding to the type of
view."

  (vz (origin im)))

;;;------------------------------------------

(defmethod view-pos-from-image ((v coronal-view) im)

  (vy (origin im)))

;;;------------------------------------------

(defmethod view-pos-from-image ((v sagittal-view) im)

  (vx (origin im)))

;;;------------------------------------------

(defmethod view-pos-from-image ((v beams-eye-view) im)

  (declare (ignore im))
  (view-position v))

;;;------------------------------------------

(defmethod generate-image-from-set ((v transverse-view) images)

  "generate-image-from-set v images

Selects the transverse image that matches the view v."

  (find-transverse-image (view-position v) images *display-epsilon*))

;;;------------------------------------------

(defmethod generate-image-from-set ((v coronal-view) images)

  (make-coronal-image (view-position v) images))

;;;------------------------------------------

(defmethod generate-image-from-set ((v sagittal-view) images)

  (make-sagittal-image (view-position v) images))

;;;------------------------------------------

(defmethod generate-image-from-set ((v beams-eye-view) images)

  "Returns a computed radiograph image-2d to use as background image
for the view v."

  (remove-bg-drr v)

  (let* ((fi (first images)) ;; need to compute these from the images
	 (orig (origin fi))
	 (size (size fi))
	 (xmin (aref orig 0))
	 (xmax (+ xmin (first size)))
	 (ymax (aref orig 1))
	 (ymin (- ymax (second size)))
	 (im-pix (pixels fi))
	 (xpix (array-dimension im-pix 0))
	 (ypix (array-dimension im-pix 1))
	 (pix-per-cm (pix-per-cm fi))
	 (x-cm (/ xpix pix-per-cm))
	 (y-cm (/ ypix pix-per-cm))
	 (bm (beam-for v))
	 (couch-displacement (make-array 3 :element-type 'single-float
					 :initial-contents 
					 (list
					   (couch-lateral bm)
					   (couch-height bm)
					   (couch-longitudinal bm))))
	 (g-to-p (coll-to-couch-transform (couch-angle bm)
					  (gantry-angle bm)
					  0.0))
	 (eyept (matrix-multiply g-to-p 0.0 0.0 (isodist bm)))
	 (centerpt couch-displacement)
	 (uppt (matrix-multiply g-to-p 0.0 (/ y-cm 2.0) 0.0)))

    ;; handle couch-space to patient-space conversion
    (dotimes (i 3)
      (decf (aref eyept i) (aref couch-displacement i))
      (decf (aref uppt i) (aref couch-displacement i))
      ;; note - the following is a reuse of the couch-displacement array
      (setf (aref centerpt i) (- (aref couch-displacement i))))
    (multiple-value-bind (voxarray zarray) (make-3d-image images)
      (make-instance 'image-2d
	:id 3                                           ;; arbitrary
	:description "Prism drr image"
	:acq-date (date-time-string)
	:acq-time ""
	:scanner-type (scanner-type fi)
	:hosp-name (hosp-name fi)
	:img-type (concatenate 'string "DRR computed from "
			       (img-type fi))
	:origin (let* ((bev-tr (bev-transform v))
		       (iso-x (+ (* (aref bev-tr 0) (aref centerpt 0))
				 (* (aref bev-tr 1) (aref centerpt 1))
				 (* (aref bev-tr 2) (aref centerpt 2))
				 (aref bev-tr 3)))
		       (iso-y (+ (* (aref bev-tr 4) (aref centerpt 0))
				 (* (aref bev-tr 5) (aref centerpt 1))
				 (* (aref bev-tr 6) (aref centerpt 2))
				 (aref bev-tr 7))))
		  (vector (- iso-x (/ x-cm 2.0))
			  (+ iso-y (/ y-cm 2.0))
			  0.0))
	:size (list x-cm y-cm)
	:range (range fi)
	:units (units fi)
	:thickness 1.0

	;; not correct - should represent the orientation of the BEV
	:x-orient (vector 1.0 0.0 0.0)
	:y-orient (vector 0.0 -1.0 0.0)

	:pix-per-cm pix-per-cm
	:pixels (or (drr-cache bm)
		    (setf (drr-cache bm)
		      (drr (list xmin ymin)
			   (list xmax ymax)
			   zarray
			   eyept centerpt uppt
			   xpix ypix voxarray v)))))))

;;;------------------------------------------

(defmethod generate-image-from-set ((v view) images)

  "If no better method, just return nil"

  (declare (ignore images))
  nil)

;;;------------------------------------------
;;; End.
