;;;
;;; object-manager
;;;
;;; This is the code that supports the maintenance of consistency
;;; between objects and the views they appear in, while the object
;;; attributes change, the view parameters change, and objects and
;;; views are created and destroyed.
;;;
;;; To use this you must provide a mediator definition, and a mediator
;;; constructor function.  The constructor function takes exactly two
;;; parameters, an object and a view.  It returns a mediator for that
;;; pair.  The mediator must have a function named object that returns
;;; the object for the mediator, and a function named view that
;;; returns the view for that mediator.
;;;
;;; 20-Oct-1992 I. Kalet created from paper sketch
;;; 02-Dec-1992 J. Unger modify object-refresh to pass params down to
;;; draw and to always get both object and view from mediator.  Also
;;; add flush output to object-refresh.
;;; 13-Dec-1992 J. Unger remove (sl:flush-output) from object-refresh.
;;; 31-Dec-1992 I. Kalet reorganize order of forms
;;; 11-Apr-1993 I. Kalet make object-refresh a lambda - not needed
;;; elsewhere.  Also add remove-notify to complement add-notify
;;; 15-Apr-1993 I. Kalet make update-view use the object in the
;;; mediator, not the object making the announcement.
;;; 23-Jul-1993 I. Kalet add code to object-view-manager initialize
;;; method so that views appear with graphics already displayed.
;;; 18-Oct-1993 I. Kalet add code to object-view-mediator destroy
;;; method to remove graphic primitive of deleted object.
;;; 12-Jan-1995 I. Kalet destroy view when deleted from view set.  Do
;;; it here rather than in plans because it is the mediator's job
;;; since the mediator can control the order of things (destroy the
;;; mediator before destroying the view).
;;;  5-Mar-1995 I. Kalet add destroy method for object-view-manager.
;;;  Move display-view call from mediator destroy method to action
;;;  function for object deleted.  Don't call it for view deleted.
;;;  This then allows the view to be destroyed when it is deleted.
;;; 25-Jul-1995 I. Kalet almost right, but not quite.  The beam's eye
;;; view mediator deletes the beam's eye view, so cannot call
;;; display-view.  So check if it is still in the view set before
;;; calling display-view, on object deleted.
;;;  8-Oct-1996 I. Kalet change calls to draw to conform to new
;;; signature without keywords or &rest and change update-view to
;;; generic function.
;;;

(in-package :prism)

;;;-------------------------------------

(defclass object-view-mediator ()

  ((object :reader object
	   :initarg :object
	   :documentation "The object this mediator manages views
for.")

   (view :reader view
	 :initarg :view
	 :documentation "The view in which this object may appear.")

   )

  (:documentation "This is the generic object-view-mediator class")

  )

;;;-------------------------------------

(defmethod initialize-instance :after ((ovm object-view-mediator)
                                       &rest initargs)

  "Initially draws the object in the view, and registers to regenerate
the graphic primitives of the object in the view when refresh-fg is
announced.  This might be supplemented and/or replaced in some more
specialized mediators."

  (declare (ignore initargs))
  (ev:add-notify ovm (refresh-fg (view ovm))
		 #'(lambda (med vw) (draw (object med) vw)))
  (draw (object ovm) (view ovm))
  )

;;;-------------------------------------

(defmethod destroy ((ovm object-view-mediator))

  (let ((obj (object ovm))
	(vw (view ovm)))
    (ev:remove-notify ovm (refresh-fg vw))
    (setf (foreground vw) (remove obj (foreground vw) :key #'object))
    ))

;;;-------------------------------------

(defclass object-view-manager ()

  ((object-set :accessor object-set
	       :initarg :object-set
	       :initform (coll:make-collection)
	       :documentation "The set of objects that are to appear
in the views.  Usually provided by initialization arguments, as it is
already part of some container object, e.g., the organ set is a part
of a patient, a set of beams is a part of a plan, etc.")

   (view-set :accessor view-set
	     :initarg :view-set
	     :initform (coll:make-collection)
	     :documentation "The set of views for some plan.  Usually
provided by an initialization argument when a plan is created.")

   (mediator-set :accessor mediator-set
		 :initform (coll:make-collection)
		 :documentation "The set of object-view mediators.
Each one handles updates of a particular view for a particular object.
They are created when either an object or a view is created and added
to the above sets.  They are deleted when an object or view is
deleted.")

   )

  (:documentation "This is the object that creates and deletes the
mediators for any given set of objects to appear in a given set of
views.")

  )

;;;-------------------------------------

(defmethod initialize-instance :after ((m object-view-manager)
				       &key mediator-fn
				       &allow-other-keys)

  "Fills the mediator set by iterating over objects and views, and
creates the links to dynamically create and delete mediators as
necessary when objects and views are created and deleted."

  (let ((os (object-set m))
	(vs (view-set m))
	)
    (dolist (obj (coll:elements os))
	    (dolist (v (coll:elements vs))
		    (coll:insert-element (funcall mediator-fn obj v)
					 (mediator-set m))))
  (ev:add-notify m (coll:inserted os)
		 #'(lambda (md obj-set obj)
		     (declare (ignore obj-set))
		     (dolist (v (coll:elements (view-set md)))
		       (coll:insert-element (funcall mediator-fn obj v)
					    (mediator-set md))
		       (display-view v))))
  (ev:add-notify m (coll:inserted vs)
		 #'(lambda (md obj-set v)
		     (declare (ignore obj-set))
		     (dolist (obj (coll:elements (object-set md)))
			     (coll:insert-element
			      (funcall mediator-fn obj v)
			      (mediator-set md)))
		     (display-view v)
		     ))
  (ev:add-notify m (coll:deleted os)
		 #'(lambda (md obj-set obj)
		     (declare (ignore obj-set))
		     (let ((med-set (mediator-set md)))
		       (dolist (med (coll:elements med-set))
			 (when (eq (object med) obj)
			   (let ((vw (view med)))
			     (coll:delete-element med med-set)
			     (destroy med)
			     (when (coll:collection-member vw vs)
			       (display-view vw)))
			   ))
		       )))
  (ev:add-notify m (coll:deleted vs)
		 #'(lambda (md obj-set v)
		     (declare (ignore obj-set))
		     (let ((med-set (mediator-set md)))
		       (dolist (med (coll:elements med-set))
			 (when (eq (view med) v)
			   (coll:delete-element med med-set)
			   (destroy med)
			   ))
		       )))
  ))

;;;-------------------------------------

(defun make-object-view-manager (object-set view-set
					    mediator-function)

  "MAKE-OBJECT-VIEW-MANAGER object-set view-set mediator-function

returns an instance of an object-view-manager, a mediator between a
set of objects and a set of views they appear in.  The mediator
function is a function that creates a mediator between an object and a
view, given the object and the view."

  (make-instance 'object-view-manager :object-set object-set
		 :view-set view-set :mediator-fn mediator-function)
  )

;;;-------------------------------------

(defmethod destroy ((ovm object-view-manager))

  (let ((os (object-set ovm))
	(vs (view-set ovm))
	)
    (dolist (med (coll:elements (mediator-set ovm))) (destroy med))
    (ev:remove-notify ovm (coll:inserted os))
    (ev:remove-notify ovm (coll:inserted vs))
    (ev:remove-notify ovm (coll:deleted os))
    (ev:remove-notify ovm (coll:deleted vs))
    ))

;;;-------------------------------------

(defmethod update-view ((med object-view-mediator) obj &rest pars)

  "produces a new graphic primitive for the object and view connected
by object-view-mediator med.  Used to redraw a single object that has
changed."

  (declare (ignore obj pars))
  (draw (object med) (view med)))

;;;-------------------------------------

(defmethod update-view :around ((med object-view-mediator) obj
				&rest pars)

  "displays the view after the primary method and all the before and
after methods are called."

  (declare (ignore obj pars))
  (call-next-method)
  (display-view (view med)))

;;;-------------------------------------
