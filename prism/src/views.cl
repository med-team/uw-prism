;;;
;;; views
;;;
;;; This is the implementation of Prism views and view management
;;; machinery.  It includes the view class, and the mediators required
;;; to maintain the locator bars that reference other views in a given
;;; view.  It does not include the management of the data that
;;; actually appear in a view, such as the image data or line graphics
;;; rendering anatomy or beams or other stuff.
;;;
;;; 11-Jun-1992 I. Kalet started
;;;  6-Nov-1992 I. Kalet/J. Unger condense set mediators into one,
;;;  eliminate intersects relation, go direct from view sets to
;;;  mediator sets.  Also fix up redraw when view attributes change,
;;;  put window, level with view, not image-2d
;;; 15-Dec-1992 I. Kalet/J. Unger reorganize refresh announcements and
;;; events to handle pixmap as double buffer, add update-view function.
;;; 31-Dec-1992 I. Kalet don't announce refresh-bg on origin or scale
;;; change, make background attribute a pixmap, not a display list.
;;; 03-Jan-1993 I. Kalet only announce refresh-bg if background-displayed
;;; 14-Jan-1992 J. Unger modify add-intersect and delete-intersect to do
;;; nothing when either of the view parameters are beams eye views.
;;; 18-Jan-1993 I. Kalet move bev and locator code to separate modules
;;; 19-Jan-1993 J. Unger modify interactive-make-view to clean up properly
;;; 15-Feb-1993 I. Kalet init origin so table position is reasonable
;;;  2-Mar-1993 I. Kalet init background pixmap to black, update
;;;  documentation strings.
;;; 26-Mar-1993 I. Kalet take out declare ignore initargs for CMUCL
;;; compatibility
;;;  2-Apr-1993 I. Kalet more mods for CMUCL
;;; 24-Apr-1993 I. Kalet add pan, i.e., move view origin with mouse
;;; 23-Jul-1993 I. Kalet added initial display code, then moved it to
;;; object-manager module.
;;;  5-Nov-1993 I. Kalet make pan use right button, not left
;;; 17-Nov-1993 I. Kalet change view size in interactive-make-view
;;; from textline to menu.
;;;  7-Jan-1994 I. Kalet use gensym to provide default view name as
;;;  for other objects
;;; 28-Jan-1994 I. Kalet move a little bit of code here from locators
;;; 07-Feb-1994 J. Unger add back pointer to plan to view definition.
;;; 18-Apr-1994 I. Kalet change erase in display-view to
;;; display-picture, also add code to synchronize locator grab boxes
;;; with pan and zoom operations.  Move some code to locators module.
;;; Change origin slot to x-origin and y-origin slots but still
;;; provide a setf origin method.
;;;  2-Jun-1994 I. Kalet use constant symbols for view sizes
;;; 30-Jun-1994 I. Kalet table-position is now at origin so put it in
;;; middle of all views initially.
;;; 11-Aug-1994 J. Unger remove minimum size restriction in make-view.
;;; 28-Nov-1994 J. Unger add destroy method for views.
;;; 12-Jan-1995 I. Kalet cache table-position in view.  It is *not* a
;;; back pointer and does not change during a prism session.  Remove
;;; plan-of back-pointer.
;;; 21-Jan-1997 I. Kalet eliminate table-position and references to
;;; geometry package.
;;; 30-Apr-1997 I. Kalet remove name textline from
;;; interactive-make-view - there is now a name textline in the view
;;; panel.
;;; 13-May-1998 I. Kalet fix gaff in initialization of a view - only
;;; set the scale and origin if these slots are unbound.
;;; 11-Jun-1998 I. Kalet use beam-for keyword in make-view calls, as
;;; it is the slot name and has an initarg.
;;; 12-Aug-1998 I. Kalet add event to indicate when
;;; background-displayed is toggled.
;;; 25-Apr-1999 I. Kalet changes to support multiple colormaps.
;;;  4-Mar-2000 I. Kalet add long awaited support for tape measure.
;;; 16-Jul-2000 I. Kalet add gl-buffer and image-cache for OpenGL support.
;;;  3-Sep-2000 I. Kalet be more careful about redundant updates when
;;; background flag toggles.
;;; 11-Dec-2000 I. Kalet add use-gl flag to avoid crashes in plot code.
;;; 31-Jul-2002 I. Kalet add oblique-view class
;;;  5-Aug-2002 J. Sager add room-view class to interactive-make-view
;;; 20-Aug-2002 J. Sager modify motion notification to change origin for 
;;; room-view class
;;; 22-Sep-2002 I. Kalet fix up to be consistent with evolving OpenGL support
;;; 15-Jul-2005 I. Kalet move glflush call in display-view up and
;;; change it to glfinish to insure immediate screen update.
;;;  3-Jan-2009 I. Kalet move all OpenGL stuff to room-views, no
;;; longer using GL for images.
;;; 25-May-2009 I. Kalet remove all support for room-view.
;;;

(in-package :prism)

(defvar *default-view-width* 30.0 "Default view width in cm, used to
determine initial scale.")

;;;----------------------------------------------------

(defclass view (generic-prism-object)

  ((picture :type sl:picture
	    :accessor picture
	    :documentation "The picture part of the view, the frame
that has the image and graphic data displayed in it along with the
locator bars.")

   (view-position :type single-float
		  :accessor view-position
		  :initarg :view-position
		  :documentation "The view-position is the x, y or z
coordinate specifying the position of this view on the axis orthogonal
to it.")

   (new-position :accessor new-position
		 :initform (ev:make-event)
		 :documentation "Announced when the view-position is
changed.")

   (x-origin :type fixnum
	     :accessor x-origin
	     :initarg :x-origin
	     :documentation "The origin is a pair of pixel space
coordinates that specify the location of the modeling space origin or
projection of the origin to the picture plane.")

   (y-origin :type fixnum
	     :accessor y-origin
	     :initarg :y-origin
	     :documentation "The origin is a pair of pixel space
coordinates that specify the location of the modeling space origin or
projection of the origin to the picture plane.")

   (new-origin :accessor new-origin
	       :initform (ev:make-event)
	       :documentation "Announced when the origin of the view
changes.")

   (scale :type single-float
	  :accessor scale
	  :initarg :scale
	  :documentation "The scale factor in pixels per cm.")

   (new-scale :accessor new-scale
	      :initform (ev:make-event)
	      :documentation "Announced when the scale factor is
changed.")

   (background-displayed :accessor background-displayed
			 :initarg :background-displayed
			 :documentation "Just an indicator specifying
whether the background should be included in the view.")

   (bg-toggled :accessor bg-toggled
	       :initform (ev:make-event)
	       :documentation "Announced when background-displayed is
changed.")

   (window :type fixnum ;; gray scale window
	   :accessor window
	   :initarg :window
	   :documentation "The window and level attributes determine
what part of the range of image pixel values are assigned the
intermediate gray level intensities.")

   (level :type fixnum ;; gray scale level
	  :accessor level
	  :initarg :level
	  :documentation "See window.")

   (new-winlev :accessor new-winlev
	       :initform (ev:make-event)
	       :documentation "Announced when either the window or level
value changes.")

   (foreground :accessor foreground
	       :initarg :foreground
	       :documentation "A list of graphic primitives for all
the foreground objects displayed in the view, i.e., contours and beam
portals.")

   (background :accessor background
	       :initarg :background
	       :documentation "A pixmap containing the background
image.")

   (image-cache :accessor image-cache
		:initform nil
		:documentation "The gray scale mapped image pixel
array, cached so it can be scaled and panned without recomputing the
mapping.")

   (scaled-image :accessor scaled-image
		 :initform nil
		 :documentation "A scratch array used for pan and zoom
so we don't generate garbage on repeated operations.")

   (refresh-fg :accessor refresh-fg
	       :initform (ev:make-event)
	       :documentation "Announced when everything in the view
foreground, i.e., the foreground display list, should be redrawn.")

   (locators ;; :type coll:collection
	     :accessor locators
	     :initform (coll:make-collection)
	     :documentation "The set of locator bars that appear in
this view.")

   (local-bars-on :accessor local-bars-on
		  :initarg :local-bars-on
		  :documentation "The boolean variable that indicates
if the locator bars appear in this view.")

   (remote-bars-toggled :type ev:event
			:accessor remote-bars-toggled
			:initform (ev:make-event)
			:documentation "Announced when the locators
for this view in other views should be turned on or off.  The on or
off value, t or nil, is passed as a parameter.")

   (ptr-loc :type list
	    :accessor ptr-loc
	    :documentation "The location of the screen pointer in the
window while the left mouse button is down.  No need to initialize.")

   (button-down :type (member t nil)
		:accessor button-down
		:initform nil
		:documentation "A flag indicating that the left mouse
button is down or up, t for down, nil for up.")

   (tape-measure :accessor tape-measure
		 :initform nil
		 :documentation "A cm tape measure that appears in the
view on demand.")

   )

  (:default-initargs :view-position 0.0 :background nil :foreground nil
		     :background-displayed nil :local-bars-on t
		     :window 500 :level 1024 :use-gl t)

  (:documentation "The view class contains the graphics and the
locator bars.")

  )

;;;-------------------------------------

(defmethod initialize-instance :after ((v view) &rest initargs
				       &key pic-w pic-h mapped
				       &allow-other-keys)
  (let* ((p (apply #'sl:make-picture
		   pic-w pic-h :mapped mapped initargs))
	 (w (sl:window p))
	 (width (clx:drawable-width w))
	 (height (clx:drawable-height w))
	 (px (clx:create-pixmap :width width
				:height height
				:depth (clx:drawable-depth w)
				:drawable w)))
    (setf (picture v) p)
    (setf (background v) px)
    ;; make background pixmap initially all black
    (clx:draw-rectangle (background v) (sl:color-gc 'sl:black)
			0 0 width height t)
    ;; provide a scratch array for pan and zoom
    (setf (scaled-image v) (make-array (list width height)
				       :element-type
				       '(unsigned-byte 32)))
    ;; this initial scale and origin only apply as a default
    (unless (slot-boundp v 'scale)
      (setf (scale v) (/ (float width) *default-view-width*)))
    (unless (slot-boundp v 'x-origin)
      (setf (origin v) (list (round (/ width 2))
			     (round (/ height 2)))))
    ;; pointer motion with right mouse button down in view moves the
    ;; view origin, only when background not displayed
    (ev:add-notify v (sl:button-press p)
		   #'(lambda (vw pic code x y)
		       (declare (ignore pic))
		       (when (= code 3)
			 (setf (button-down vw) t)
			 (setf (ptr-loc vw) (list x y)))))
    (ev:add-notify v (sl:button-release p)
		   #'(lambda (vw pic code x y)
		       (declare (ignore pic x y))
		       (if (= code 3)
			   (setf (button-down vw) nil))))
    (ev:add-notify v (sl:motion-notify p)
		   #'(lambda (vw pic x y state)
		       (declare (ignore pic state))
		       (if (button-down vw)
			   (let ((xp (first (ptr-loc vw)))
				 (yp (second (ptr-loc vw))))
			     (setf (origin vw)
			       (list (+ (x-origin vw) (- x xp))
				     (+ (y-origin vw) (- y yp))))
			     (setf (ptr-loc vw) (list x y))))))
    ))

;;;-------------------------------------

(defmethod display-view ((v view))

  "refresh pixmap and window."

  (let* ((pic (picture v))
	 (px (sl:pixmap pic))
	 (width (clx:drawable-width px))
	 (height (clx:drawable-height px)))
    ;; copy background to pixmap, or erase pixmap
    (if (background-displayed v)
	(clx:copy-area (background v) (sl:color-gc 'sl:white)
		       0 0 width height px 0 0)
      (clx:draw-rectangle px (sl:color-gc 'sl:black)
			  0 0 width height t))
    ;; paint foreground primitives in pixmap
    (dolist (prim (foreground v)) (draw prim v))
    (when (tape-measure v) (draw-tape-measure-tics (tape-measure v)))
    ;; make pixmap appear in window, refresh grab boxes and border
    (sl:display-picture pic)
    ))

;;;--------------------------------------

(defmethod (setf view-position) :after (new-pos (v view))

  "Announce and make entire view regenerate and redraw."

  (ev:announce v (new-position v) new-pos)
  (ev:announce v (refresh-fg v))
  (display-view v))

;;;--------------------------------------

(defmethod (setf origin) (new-org (v view))

  "Takes a list, new-org, and puts the values in the right places,
then announces and makes entire view regenerate and redraw."

  (setf (x-origin v) (first new-org)
	(y-origin v) (second new-org))
  (when (tape-measure v) (setf (origin (tape-measure v)) new-org))
  (ev:announce v (new-origin v) new-org)
  (ev:announce v (refresh-fg v))
  (display-view v))

;;;--------------------------------------

(defmethod (setf scale) :after (new-scl (v view))

  "Announce and make entire view regenerate and redraw."

  (when (tape-measure v) (setf (scale (tape-measure v)) new-scl))
  (ev:announce v (new-scale v) new-scl)
  (ev:announce v (refresh-fg v))
  (display-view v))

;;;--------------------------------------

(defmethod (setf window) :after (new-window (v view))

  (declare (ignore new-window))
  (ev:announce v (new-winlev v))
  (when (background-displayed v) (display-view v)))

;;;--------------------------------------

(defmethod (setf level) :after (new-level (v view))

  (declare (ignore new-level))
  (ev:announce v (new-winlev v))
  (when (background-displayed v) (display-view v)))

;;;--------------------------------------

(defmethod (setf background-displayed) :after (displayed (v view))

  (declare (ignore displayed))
  (ev:announce v (bg-toggled v))
  (display-view v))

;;;-------------------------------------
;;; an :after method for (setf local-bars-on) is provided in the
;;; locators module
;;;--------------------------------------

(defun make-view (pic-w pic-h &optional (view-type 'transverse-view)
			&rest other-initargs)

  (apply #'make-instance view-type :allow-other-keys t
	 :pic-w pic-w :pic-h pic-h other-initargs))

;;;-------------------------------------

(defclass transverse-view (view)

  ()

  (:default-initargs :name "Transverse View")

  (:documentation "The transverse view is a specialization of a view,
for the x-y plane, in which view-position represents the z coordinate of
the view.")

  )

;;;-------------------------------------

(defclass coronal-view (view)

  ()

  (:default-initargs :name "Coronal View")

  (:documentation "The coronal view is a specialization of a view,
for the x-z plane, in which view-position represents the y coordinate of
the view.")

  )

;;;-------------------------------------

(defclass sagittal-view (view)

  ()

  (:default-initargs :name "Sagittal View")

  (:documentation "The sagittal view is a specialization of a view,
for the y-z plane, in which view-position represents the x coordinate of
the view.")

  )

;;;-------------------------------------

(defclass oblique-view (view)

  ((azimuth :type single-float
	    :accessor azimuth
	    :initarg :azimuth
	    :documentation "The azimuthal rotation angle for this view.")

   (altitude :type single-float
	     :accessor altitude
	     :initarg :altitude
	     :documentation "The altitude rotation for this view,
performed after the azimuth rotation.")

   )

  (:default-initargs :name "Oblique view" :azimuth 0.0 :altitude 0.0)

  (:documentation "An oblique view is one that can be rotated to more
  or less any arbitrary position, like in the old UWPLAN system.")

  )

;;;--------------------------------------

(defmethod (setf azimuth) :after (new-azi (v oblique-view))

  "Announce and make entire view regenerate and redraw."

  (declare (ignore new-azi))
  (ev:announce v (refresh-fg v))
  (display-view v))

;;;--------------------------------------

(defmethod (setf altitude) :after (new-alt (v oblique-view))

  "Announce and make entire view regenerate and redraw."

  (declare (ignore new-alt))
  (ev:announce v (refresh-fg v))
  (display-view v))

;;;-------------------------------------

(defun interactive-make-view (view-name &key beams)

  "interactive-make-view view-name &key beams

returns a view instance whose basic parameters are specified by the
user through a dialog box at a nested event processing level."

  (sl:push-event-level)
  (let* ((size medium) ;; default - constant defined in prism-globals
	 (view-type 'transverse-view)
	 (vbox (sl:make-frame 375 155 :title "New view parameters"))
	 (win (sl:window vbox))
	 (ok-b (sl:make-exit-button 70 30 :parent win
				    :label "Accept"
				    :ulc-x 10 :ulc-y 10
				    :bg-color 'sl:blue))
	 (vsize (sl:make-radio-menu '("Small" "Medium" "Large")
				    :parent win
				    :ulc-x 10 :ulc-y 50))
	 (vmenu (sl:make-radio-menu
		 (if beams '("Transverse" "Coronal" "Sagittal" "Beam's Eye")
		   '("Transverse" "Coronal" "Sagittal" "Oblique"))
		 :parent win
		 :ulc-x 95 :ulc-y 10))
         (beam-list (sl:make-radio-scrolling-list 150 135 :parent win
                                                  :ulc-x 215 :ulc-y 10))
         (beam-choice nil))
    (sl:select-button 0 vmenu)		; default - transverse
    (sl:select-button 1 vsize)		; default - medium
    (ev:add-notify vbox (sl:selected vsize)
		   #'(lambda (l a item)
		       (declare (ignore l a))
		       (setq size (case item
				    (0 small) ;; constants defined in
				    (1 medium) ;; prism-globals
				    (2 large)))))
    (ev:add-notify vbox (sl:selected vmenu)
		   #'(lambda (l a item)
		       (declare (ignore l a))
		       (setq view-type
			 (case item
			   (0 'transverse-view)
			   (1 'coronal-view)
			   (2 'sagittal-view)
			   (3 (if beams 'beams-eye-view 'oblique-view))))))
    (dolist (b beams)
      (sl:insert-button (sl:make-list-button beam-list (name b))
			beam-list))
    (when beams
      (sl:select-button (first (sl:buttons beam-list))
			beam-list))
    (sl:process-events)
    (when (eql view-type 'beams-eye-view) ; do this before the beam-list
      (setq beam-choice			; is destroyed
        (find (sl:label (find-if #'sl:on (sl:buttons beam-list)))
	      beams 
	      :key #'name)))
    ;; don't neet to ev:remove-notify: all controls are destroyed anyway
    (sl:destroy vmenu)
    (sl:destroy beam-list)
    (sl:destroy vsize)
    (sl:destroy ok-b)
    (sl:destroy vbox)
    (sl:pop-event-level)
    (make-view size size view-type
	       :name (if (equal "" view-name) (symbol-name view-type)
		       view-name)
	       :beam-for beam-choice)))

;;;---------------------------------------

(defmethod destroy ((vw view))

  "destroy (vw view)

Deallocates resources associated with a view.  The locators are
destroyed elsewhere."

  (setf (image-cache vw) nil)
  (clx:free-pixmap (background vw))
  (sl:destroy (picture vw)))

;;;---------------------------------------
;;; End.
