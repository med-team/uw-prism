;;;
;;; replace-coll
;;;
;;; contains all the methods for the generic function replace-coll
;;;
;;; 21-May-1997 I. Kalet move here from collimators.
;;; 24-Jun-1997 I. Kalet add electron-coll per spec.
;;;

(in-package :prism)

;;;---------------------------------------------

(defmethod replace-coll ((old-coll collimator) new-coll-type)

  "REPLACE-COLL old-coll new-coll-type

returns a new collimator of type new-coll-type, with settings matching
as near as possible the settings from collimator old-coll.  The
default method just creates a new collimator with default values."

  (make-instance new-coll-type))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll collimator)
			 (new-coll-type (eql 'multileaf-coll)))

  "When the new collimator type is multileaf, the vertices of the new
collimator are obtained by computing the portal of the old one, the
same for all old collimator types, including electron-coll."

  (make-instance 'multileaf-coll :vertices (portal old-coll)))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll symmetric-jaw-coll)
			 (new-coll-type (eql 'variable-jaw-coll)))

  (let ((hx (* 0.5 (x old-coll)))
	(hy (* 0.5 (y old-coll))))
    (make-instance 'variable-jaw-coll
      :x-sup hx :x-inf hx :y-sup hy :y-inf hy)))
	
;;;---------------------------------------------

(defmethod replace-coll ((old-coll symmetric-jaw-coll)
			 (new-coll-type (eql 'combination-coll)))

  (let ((hx (* 0.5 (x old-coll))))
    (make-instance 'combination-coll
      :x-sup hx :x-inf hx :y (y old-coll))))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll variable-jaw-coll)
			 (new-coll-type (eql 'symmetric-jaw-coll)))

  (make-instance 'symmetric-jaw-coll
    :x (+ (x-sup old-coll) (x-inf old-coll))
    :y (+ (y-sup old-coll) (y-inf old-coll))))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll variable-jaw-coll)
			 (new-coll-type (eql 'combination-coll)))

  (make-instance 'combination-coll
    :x-sup (x-sup old-coll)
    :x-inf (x-inf old-coll)
    :y (+ (y-sup old-coll) (y-inf old-coll))))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll combination-coll)
			 (new-coll-type (eql 'symmetric-jaw-coll)))

  (make-instance 'symmetric-jaw-coll
    :x (+ (x-sup old-coll) (x-inf old-coll))
    :y (y old-coll)))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll combination-coll)
			 (new-coll-type (eql 'variable-jaw-coll)))

  (let ((hy (* 0.5 (y old-coll))))
    (make-instance 'variable-jaw-coll
      :x-sup (x-sup old-coll)
      :x-inf (x-inf old-coll)
      :y-sup hy :y-inf hy)))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll electron-coll)
			 (new-coll-type (eql 'symmetric-jaw-coll)))

  (let ((size (cone-size old-coll)))
    (make-instance 'symmetric-jaw-coll :x size :y size)))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll electron-coll)
			 (new-coll-type (eql 'combination-coll)))

  (let* ((size (cone-size old-coll))
	 (hs (* 0.5 size)))
    (make-instance 'combination-coll
      :x-sup hs :x-inf hs :y size)))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll electron-coll)
			 (new-coll-type (eql 'variable-jaw-coll)))

  (let* ((size (* 0.5 (cone-size old-coll))))
    (make-instance 'variable-jaw-coll
      :x-sup size :x-inf size
      :y-sup size :y-inf size)))

;;;---------------------------------------------

(defmethod replace-coll ((old-coll collimator)
			 (new-coll-type (eql 'electron-coll)))

  "When the new collimator type is electron, the vertices of the new
collimator are obtained by computing the portal of the old one, the
same for all old collimator types.  But since there is no access to
the list of available cone sizes, the cone size is arbitrarily set to
the default."

  (make-instance 'electron-coll :vertices (portal old-coll)))

;;;---------------------------------------------
