;;;
;;; mlc-collimators
;;;
;;; Functions and constants for dealing with collimators
;;; which have both leaves and jaws.
;;; Contains functions used in Client only.
;;;
;;; These collimators are instances of the multileaf-coll class,
;;; but much of this code is specific to Elekta SL20 machines.
;;; We made some effort to factor out the machine-specific features
;;; to the multileaf-collim-info class, represented here by
;;; the instance *sl-collim-info*,  and some of the code here
;;; might be made more machine independent by passing an instance of
;;; multileaf-collim-info to some functions.
;;;
;;; But other features here
;;; are specific to Elekta machines and are not parameterized:
;;; the constraints on movement and proximity of leaves and jaws
;;; which make "flagpole" configurations necessary, and the
;;; nomenclature that appears in messages.
;;;
;;; Changes made in the old constraints.cl:
;;; 10-Jul-2001  J. Jacky  Started
;;; 13-Jul-2001  J. Jacky  Correct def'n of exposed
;;; 27-Jul-2001  J. Jacky  Change 3,1 formats to 4,2
;;; 28-Aug-2001  J. Jacky  Remove "Upper", "Lower" from msgs, remove bank var
;;; Changes made in the old dicom-panel.cl:
;;; 11-Jul-2001  J. Jacky  defparameter *sl-collim-info* not SL20A-6MV-MLC
;;; 27-Jul-2001  J. Jacky  Don't overcenter last open leaf
;;;  1-Aug-2001  J. Jacky  make leaf-pair-map in *sl-collim-info* match RTD
;;; 28-Aug-2001  J. Jacky  new make-flagpole
;;; 30-Aug-2001  J. Jacky  adjust-ends: handle very small fields, flagpole
;;; 31-Aug-2001  J. Jacky  make-flagpole: refine criteria for flagpole left/rt.
;;;  5-Sep-2001  J. Jacky  adjust-ends: fix when ymax < half leaf width
;;; 10-Sep-2001  J. Jacky  beam-warnings uses new shape-diff
;;; 11-Sep-2001  J. Jacky  beam-warnings: include tol in check, use nleaves,
;;;                         if flagpole but field shape preserved say so
;;;                        shape-diff: use nleaves not hard-coded 39
;;; Changes made in the new sl-collimators.cl:
;;; 11-Sep-2001  J. Jacky  Begun, extracted from dicom-panel.cl, constraints.cl
;;;                        Move magic numbers to (let...) at start of each fcn.
;;;                        rename beam-warnings to collim-warnings, pass colls
;;;                        rename constraint-violations to collim-constraint-..
;;; 12-Sep-2001  J. Jacky  new make-multileaf-coll
;;;                        make-flagpole takes, returns a multileaf-coll
;;;                        make-adjusted-ends takes, returns a multileaf-coll
;;; 31-Jan-2002 I. Kalet move round-digits here from dicom-panel to
;;; remove circular module dependency.
;;; 28-May-2002 I. Kalet parametrize minimum leaf separation (local
;;; variable mls below).
;;; 27-Aug-2003 BobGian Uniformize variable names in preparation
;;;   for adding Dose Monitoring Points.
;;; 06-Oct-2003 BobGian Uniformize indentation/commenting style.
;;;   LET* -> LET where possible (vars not bound sequentially).
;;; 12-May-2004 BobGian:
;;;   COLLIM-WARNINGS - reversed coll args [consistent with other comparisons].
;;;   FLAG-DIFF - reversed coll args [as above], plus fixed bogus doc string.
;;;   SHAPE-DIFF - reversed collimator args [as above].
;;; 17-May-2004 BobGian complete process of extending all instances of Original
;;;   and Current Prism beam instances to include Copied beam instance too,
;;;   to provide copy for comparison with Current beam without mutating
;;;   Original beam instance.
;;; 05-Oct-2004 BobGian fixed a few lines to fit within 80 cols.
;;;

(in-package :prism)

;;;=============================================================

;;; This information also appears in each of the SL20 machine data files
;;; but we can't depend on knowing any of their names so we need this parameter

(defparameter *sl-collim-info*
  (make-instance 'multileaf-collim-info
    :col-headings  " X  Y2 Leaves                Y1 Leaves   X"
    :num-leaf-pairs 40
    :edge-list '(20.0 19.0 18.0 17.0 16.0 15.0 14.0 13.0 12.0 11.0
		 10.0 9.0 8.0 7.0 6.0 5.0 4.0 3.0 2.0 1.0 0.0 -1.0
		 -2.0 -3.0 -4.0 -5.0 -6.0 -7.0 -8.0 -9.0 -10.0 -11.0
		 -12.0 -13.0 -14.0 -15.0 -16.0 -17.0 -18.0 -19.0 -20.0)
    :leaf-pair-map '((1 1) (2 2) (3 3) (4 4) (5 5) (6 6) (7 7) (8 8) (9 9)
		     (10 10) (11 11) (12 12) (13 13) (14 14) (15 15) (16 16)
		     (17 17) (18 18) (19 19) (20 20) (21 21) (22 22) (23 23)
		     (24 24) (25 25) (26 26) (27 27) (28 28) (29 29) (30 30)
		     (31 31) (32 32) (33 33) (34 34) (35 35) (36 36) (37 37)
		     (38 38) (39 39) (40 40))
    :inf-leaf-scale -1.0
    :leaf-open-limit 20.0
    :leaf-overcenter-limit 12.5))

;;;-------------------------------------------------------------

(defvar *minimum-leaf-gap* 0.5 "the minimum gap between leaves
required, changes sometimes with machine control software updates.")

;;;=============================================================

(defun open-pair (i leaves)

  "open-pair i leaves

Return t if i'th pair in leaves is open"

  ;; Just checks whether there is a gap between leaves.
  ;; Code in SHAPE-DIFF uses a different criteria, checks jaws and leaves.

  (let ((eps 0.1))
    (< (+ (first (nth i leaves)) eps) (second (nth i leaves)))))

;;;-------------------------------------------------------------

(defun round-digits (x n)

  "round-digits x n

Round float x to n decimal digits: (round-digits 7.46342 2) -> 7.46 exactly"

  ;; Purpose: ensure that displayed number in ~m,nF format equals stored number
  ;; so program doesn't complain that numbers that look okay aren't within tol.
  ;; n digits not always possible because some neat-looking decimal numbers
  ;; don't have an exact floating point representation so we get 0.59999996
  ;; not 0.6 and 38.800003 not 38.80  But these are close enough for us.

  (let ((k (expt 10 n)))
    (float (/ (round (* x k)) k))))

;;;-------------------------------------------------------------

(defun make-multileaf-coll (coll-angle coll-vertices coll-info)

  "make-multileaf-coll coll-angle coll-vertices coll-info

Return an instance of multileaf-coll with leaves and jaws set
to fit portal contour defined by coll-vertices in collimator rotated
by coll-angle, where number/dimensions of leaves is defined in coll-info."

  ;; This function computes the simplest leaf and jaw settings that fit.
  ;; It does not set flagpole make any other adjustments to accommodate Elekta.

  (let* ((coll-r-vertices (poly:rotate-vertices
			    coll-vertices (- coll-angle)))
	 (coll-box (poly:bounding-box coll-r-vertices))
	 (xmin-f (first (first coll-box)))
	 (xmax-f (first (second coll-box)))
	 (ymin-f (second (first coll-box)))
	 (ymax-f (second (second coll-box)))
	 (xmin (round-digits xmin-f 2))
	 (xmax (round-digits xmax-f 2))
	 (ymin (round-digits ymin-f 2))
	 (ymax (round-digits ymax-f 2))
	 (edges (edge-list coll-info))
	 (leaf-pos-f (compute-mlc coll-angle coll-vertices edges))
	 (leaf-pos (mapcar #'(lambda (leaf-pair)
			       (mapcar #'(lambda (leaf) (round-digits leaf 2))
				 leaf-pair))
		     leaf-pos-f)))
    ;; multileaf-coll has accessors but not initargs, so we must be long-winded
    (let ((c (make-instance 'multileaf-coll)))
      (setf (x1 c) xmin (x2 c) xmax (y1 c) ymin (y2 c) ymax
	    (vertices c) (copy-tree coll-vertices)
	    (leaf-settings c) leaf-pos)
      c)))

;;;-------------------------------------------------------------

(defun make-flagpole (collim)

  "make-flagpole collim

Return an instance of multileaf-coll which uses a flagpole configuration
to achieve the same (or similar) field shape as the input multileaf-coll"

  ;; "Flagpole" refers to a configuration of jaws and leaves that defines a
  ;; field that does not cross Prism x axis but still meets Elekta constraints:
  ;; y-jaws (Elekta X1 and X2 jaws) cannot overcenter, leaves must not touch

  (let ((ymin (y1 collim))
	(ymax (y2 collim))
	(xmin (x1 collim))
	(xmax (x2 collim))
	(new-leaf-settings (copy-tree (leaf-settings collim))))
    (if (and (<= ymin 0.0) (>= ymax 0.0))
	;; no flagpole needed but return new instance, can't share structure
	(let ((c (copy collim)))      ; copies z, vertices but not other items
	  (setf (x1 c) xmin (x2 c) xmax (y1 c) ymin (y2 c) ymax
		(leaf-settings c) new-leaf-settings)
	  c)
	;; make flagpole - min leaf separation in cm + margin for rounding err
	(let* ((mls (+ *minimum-leaf-gap* 0.001))
	       (marg 0.2)          ; margin of diaphragm over leaf in flagpole
	       (dtol 0.3) ; if xldiff > xrdiff by this much,put f'pole on left
	       (last-top 19)    ; index of last leaf in top half (touches cax)
	       (first-bottom 20) ; index of first leaf in bottom half (at cax)
	       (r-overlim (leaf-overcenter-limit *sl-collim-info*)) ; 12.5
	       (l-overlim (- r-overlim))            ; left overcenter limit
	       (top (> ymin 0.0))      ; open leaves in top half, all in 0..19
	       ;; lcent is index of open leaf nearest center
	       ;; compute-mlc opens leaf if portal intrudes at least .5cm
	       (lcent (if top (- last-top (round ymin))
			  (- first-bottom (round ymax))))
	       (ltop (if top (+ lcent 1) last-top)) ; index of leaf at top
	       (lbottom (if top first-bottom (- lcent 1)))
	       ;; leaf might not be open if portal contour very small
	       (cpair (nth lcent new-leaf-settings))
	       (cpair-open (open-pair lcent new-leaf-settings))
	       (xlcpair (if cpair-open (first cpair) xmin))
	       (xrcpair (if cpair-open (second cpair) xmax))
	       (xldiff (- xlcpair xmin)) ; central lf intrudes past jaw on left
	       (xrdiff (- xmax xrcpair))    ; pos diff measures badness of fit
	       (xl-l (- xmin mls marg))   ; left edge of flagpole on left side
	       (xr-l (- xmin marg))      ; right edge of flagpole on left side
	       (xl-r (+ xmax marg))
	       (xr-r (+ xmax mls marg))
	       ;; if flagpole would exceed overcenter limit, put on other side
	       ;; if xrdiff, xldiff equal within 3 mm, put flagpole more
	       ;; central; otherwise put flagpole on side where diff is least
	       (left (cond ((> xl-r r-overlim) t) ; right exceeds overctr limit
			   ((< xr-l l-overlim) nil)
			   ((< (abs (- xldiff xrdiff)) dtol)    ; equal fit,
			    (< (abs xl-l) (abs xr-r)))  ; choose more central
			   (t (< xldiff xrdiff))))  ; ch side with least diff
	       (xl (if left xl-l xl-r))             ; left edge of flagpole
	       (xr (if left xr-l xr-r))
	       (ymin-p (if top 0.0 ymin))
	       (ymax-p (if top ymax 0.0)))
	  #+ignore
	  (format
	    t
	    "xmin ~A  xmax ~A  xlcpair ~A  xrcpair ~A  xldiff ~A  xrdiff ~A~%"
	    xmin xmax xlcpair xrcpair xldiff xrdiff)
	  #+ignore
	  (format t "xl-l ~A  xr-l ~A  xl-r ~A  xr-r ~A  left ~A~%"
		  xl-l xr-l xl-r xr-r left)
	  (if left
	      (setf (first (nth lcent new-leaf-settings)) xl
		    (second (nth lcent new-leaf-settings)) xrcpair)
	      (setf (first (nth lcent new-leaf-settings)) xlcpair
		    (second (nth lcent new-leaf-settings)) xr))
	  (do ((i ltop (+ i 1)))     ; indices increase toward bottom of field
	      ((> i lbottom))
	    (setf (first (nth i new-leaf-settings)) xl)
	    (setf (second (nth i new-leaf-settings)) xr))

	  (let ((c (copy collim)))
	    (setf (x1 c) xmin (x2 c) xmax (y1 c) ymin-p (y2 c) ymax-p
		  (leaf-settings c) new-leaf-settings)
	    c)))))

;;;-------------------------------------------------------------

(defun make-adjusted-ends (collim)

  "make-adjusted-ends collim

Return multileaf-coll with more leaf pairs at ends of field opened if needed"

  ;; To satify Elekta requirement that field ends (in Prism y-direction)
  ;; must be formed by collimator jaws (Elekta X1, X2 jaws), not leaves
  ;; (you can't have closed leaves exposed by Elekta X jaws)
  ;; *sl-collim-info* is hardwired in, used to find coords of leaf edges

  (let* ((xmin (x1 collim))
	 (xmax (x2 collim))
	 (ymin (y1 collim))
	 (ymax (y2 collim))
	 (collim-data *sl-collim-info*)
	 (edge-lst (edge-list collim-data))
	 (old-leaf-settings (leaf-settings collim))
	 (new-leaf-settings (copy-tree old-leaf-settings)) ; may update w/setf
	 (l-last 39)                                ; index of last leaf
	 (last-top 19)           ; index of last leaf in top half, touches cax
	 (first-bottom 20)   ; index of first leaf in bottom half, touches cax
	 (mmax 100.0)                        ; greater than any expected value
	 (mmin -100.0)                          ; less than any expected value
	 (y-ulimit (first edge-lst))                ; 20.0 for SL20
	 (y-llimit (nth (+ l-last 1) edge-lst))     ; -20.0
	 ;; ltop is index of top open leaf
	 (ltop (do ((i 0 (+ i 1))) ((> i l-last) nil)  ; nil if no open leaves
		 (if (open-pair i old-leaf-settings) (return i))))
	 ;; ytop is y-coord of top edge of aperture formed by open leaves
	 (ytop (if ltop (nth ltop edge-lst)
		   mmin))                           ; force ytop < ymax
	 (lbottom (do ((i l-last (- i 1))) ((< i 0) nil)
		    (if (open-pair i old-leaf-settings) (return i))))
	 (ybottom (if lbottom (nth (+ lbottom 1) edge-lst)
		      mmax))
	 (xl mmax)                              ; place-holder, reassign later
	 (xr mmin))
    #+ignore
    (format t "xmin ~A  xmax ~A  ymin ~A  ymax ~A~%" xmin xmax ymin ymax)
    #+ignore
    (format t "ltop ~A  ytop ~A  lbottom ~A  ybottom ~A~%"
	    ltop ytop lbottom ybottom)
    ;; if leaf under top jaw edge is not open, open it
    (if (< ytop ymax y-ulimit)                  ; ymax might be past last leaf
	(progn
	  (if (and ltop (open-pair ltop old-leaf-settings))
	      (setf xl (first (nth ltop old-leaf-settings))
		    xr (second (nth ltop old-leaf-settings)))
	      (setf xl xmin xr xmax))
	  (setf ltop (- first-bottom (ceiling ymax))
		(first (nth ltop new-leaf-settings)) xl
		(second (nth ltop new-leaf-settings)) xr
		ytop (nth ltop edge-lst))))
    #+ignore
    (format t "open leaf under top jaw: new ltop ~A  ytop ~A  xl ~A  xr ~A~%"
	    ltop ytop xl xr)
    ;; open one more leaf past top jaw, but never overcenter this one
    (if (> ltop 0)
	(let ((ltop-outer (- ltop 1)))
	  (setf xl (first (nth ltop new-leaf-settings))
		xr (second (nth ltop new-leaf-settings))
		(first (nth ltop-outer new-leaf-settings))
		(if (<= xl 0.0) xl -1.0)
		(second (nth ltop-outer new-leaf-settings))
		(if (>= xr 0.0) xr 1.0))))
    #+ignore
    (format t "open leaf past top jaw ltop ~A  ltop-outer ~A  xl ~A  xr ~A~%"
	    ltop ltop-outer xl xr)

    ;; if leaf under bottom jaw edge is not open, open it
    (if (> ybottom ymin y-llimit)
	(progn
	  (if (and lbottom (open-pair lbottom old-leaf-settings))
	      (setf xl (first (nth lbottom old-leaf-settings))
		    xr (second (nth lbottom old-leaf-settings)))
	      (setf xl xmin xr xmax))
	  (setf lbottom (- last-top (floor ymin))
		(first (nth lbottom new-leaf-settings)) xl
		(second (nth lbottom new-leaf-settings)) xr
		ybottom (nth lbottom edge-lst))))
    ;; open one more leaf past bottom jaw, but never overcenter this one
    (if (< lbottom l-last)
	(let ((lbottom-outer (+ lbottom 1)))
	  (setf xl (first (nth lbottom new-leaf-settings))
		xr (second (nth lbottom new-leaf-settings))
		(first (nth lbottom-outer new-leaf-settings))
		(if (<= xl 0.0) xl -1.0)
		(second (nth lbottom-outer new-leaf-settings))
		(if (>= xr 0.0) xr 1.0))))
    (let ((c (copy collim)))
      (setf (x1 c) xmin (x2 c) xmax (y1 c) ymin (y2 c) ymax
	    (leaf-settings c) new-leaf-settings)
      c)))

;;;-------------------------------------------------------------

(defun collim-constraint-violations (collim)

  "collim-constraint-violations collim

Return list of strings describing constraint violations in collimator
with respect to *sl-collim-info*, or nil if there are none."

  ;; Constraints on minimum leaf separation are specific to the Elekta SL20
  ;; Text in strings uses Elekta coord systems and nomenclature, not Prism

  ;; minimum permissible separation between opposite leaves
  (let* ((mls *minimum-leaf-gap*)
	 (x1 (x1 collim))
	 (x2 (x2 collim))
	 (y1 (y1 collim))
	 (y2 (y2 collim))
	 (collim-data *sl-collim-info*)
	 (leaves (leaf-settings collim))
	 (n (num-leaf-pairs collim-data))
	 (lnums (leaf-pair-map collim-data))
	 (edges (edge-list collim-data))
	 (ymax (first edges))
	 (xmax (leaf-open-limit collim-data))
	 (xover (leaf-overcenter-limit collim-data))
	 (vl nil))                       ; vl is list of constraint violations

    ;; Calculations are performed in Prism coordinates,
    ;; but messages all refer to Elekta coords on DICOM panel

    ;; Prism -y1, y2 are Elekta X2, X1, hard-code y limits, not in COLLIM-DATA
    (if (< y1 (- ymax))
	(push (format nil "X2 diaphragm open too far: ~4,2F" (- y1)) vl))
    (if (> y1 0.0)
	(push (format nil "X2 diaphragm overcentered: ~4,2F" y1) vl))
    (if (> y2 ymax)
	(push (format nil "X1 diaphragm open too far: ~4,2F" y2) vl))
    (if (< y2 0.0)
	(push (format nil "X1 diaphragm overcentered: ~4,2F" (- y2)) vl))

    ;; Prism -x1, x2 are Elekta Y2, Y1
    (if (> x1 x2)
	(push
	  (format nil "Y diaphragms collide:  Y2 ~4,2F  Y1 ~4,2F" (- x1) x2)
	  vl))
    (if (< x1 (- xmax))
	(push (format nil "Y2 diaphragm open too far: ~4,2F" (- x1))
	      vl))
    (if (> x1 xover)
	(push (format nil "Y2 diaphragm overcentered too far: ~4,2F" x1)
	      vl))
    (if (> x2 xmax)
	(push (format nil "Y1 diaphragm open too far: ~4,2F" x2)
	      vl))
    (if (< x2 (- xover))
	(push (format nil "Y1 diaphragm overcentered too far: ~4,2F" (- x2))
	      vl))

    (do* ((i 0 (1+ i)))
	 ((= i n) vl)   ; vl is the return value from the do and the whole fcn
      (let* ((lnum (first (nth i lnums)))           ; leaf number
	     (onum (second (nth i lnums)))          ; opposite leaf number
	     ;; in general leaves are variable width, diaphragms can overcenter
	     (exposed (or (>= y2 (nth i edges) y1)  ; so must check both edges
			  (>= y2 (nth (+ i 1) edges) y1)))
	     (pair (nth i leaves))
	     (xl (first pair))
	     (xr (second pair))
	     (exposed-prev
	       (if (= i 0) nil
		   (or (>= y2 (nth (- i 1) edges) y1)
		       (>= y2 (nth i edges) y1))))
	     (xr-prev (if (= i 0) xmax (second (nth (- i 1) leaves))))
	     (exposed-next
	       (if (= i (- n 1)) nil
		   (or (>= y2 (nth (+ i 1) edges) y1)
		       (>= y2 (nth (+ i 2) edges) y1))))
	     (xr-next (if (= i (- n 1)) xmax (second (nth (+ i 1) leaves)))))
	(if (< xl (- xmax))
	    (push (format nil "Y2 leaf ~A open too far: ~4,2F"
			  lnum (- xl)) vl))
	(if (> xl xover)
	    (push (format nil "Y2 leaf ~A overcentered too far: ~4,2F"
			  lnum xl) vl))
	(if (> xr xmax)
	    (push (format nil "Y1 leaf ~A open too far: ~4,2F"
			  onum xr) vl))
	(if (< xr (- xover))
	    (push (format nil "Y1 leaf ~A overcentered too far: ~4,2F"
			  onum (- xr)) vl))
	(if (and exposed (< (- xr xl) mls))
	    (push (format
		    nil
		    #.(concatenate 'string
				   "Leaf ~A too close to directly opposite"
				   " leaf ~A:  Y2 ~4,2F  Y1 ~4,2F")
		    lnum onum (- xl) xr)
		  vl))
	(if (and exposed exposed-prev (< (- xr-prev xl) mls))
	    (push (format
		    nil
		    #.(concatenate 'string
				   "Leaf ~A too close to opposite neighbor"
				   " leaf ~A:  Y2 ~4,2F  Y1 ~4,2F")
		    lnum (second (nth (- i 1) lnums))   ; onum prev.
		    (- xl) xr-prev)
		  vl))
	(if (and exposed exposed-next (< (- xr-next xl) mls))
	    (push (format
		    nil
		    #.(concatenate 'string
				   "Leaf ~A too close to opposite neighbor"
				   " leaf ~A:  Y2 ~4,2F  Y1 ~4,2F")
		    lnum (second (nth  (+ i 1) lnums))  ; onum next
		    (- xl) xr-next)
		  vl))))))

;;;-------------------------------------------------------------

(defun flag-diff (copy-coll curr-coll)

  "flag-diff copy-coll curr-coll

 Returns T if CURR-COLL is a flagpole version of COPY-COLL, NIL otherwise."

  ;; Both args are copied collimators in beams which are copies
  ;; made from original beam in Prism plan.  First is unmodified while
  ;; second may be modified by user.

  ;; for now, don't even check leaves - just look at jaws
  (let ((ymin-o (y1 copy-coll))
	(ymax-o (y2 copy-coll))
	(ymin (y1 curr-coll))
	(ymax (y2 curr-coll)))
    (or (and (< ymax-o 0.0) (= ymax 0.0))
	(and (> ymin-o 0.0) (= ymin 0.0)))))

;;;-------------------------------------------------------------

(defstruct lpair open open-o xl xl-o xr xr-o)

;;;-------------------------------------------------------------

(defun shape-diff (copy-coll curr-coll end-tol)

  "shape-diff copy-coll curr-coll end-tol

Return list of lpair structures, one for each leaf pair,
describing differences between field shapes defined by CURR-COLL
and COPY-COLL, considering both jaws and leaves"

  ;; Both args are copied collimators in beams which are copies
  ;; made from original beam in Prism plan.  First is unmodified while
  ;; second may be modified by user.

  ;; This function does not contain any Elekta SL20 particulars

  (let* ((edges (edge-list *sl-collim-info*))
	 (xmin-o (x1 copy-coll))
	 (xmax-o (x2 copy-coll))
	 (ymin-o (y1 copy-coll))
	 (ymax-o (y2 copy-coll))
	 (leaves-o (leaf-settings copy-coll))
	 (xmin (x1 curr-coll))
	 (xmax (x2 curr-coll))
	 (ymin (y1 curr-coll))
	 (ymax (y2 curr-coll))
	 (leaves (leaf-settings curr-coll))
	 (nleaves (length leaves))
	 (lpairs nil))
    (dotimes (irev nleaves lpairs)
      (let* ((i (- (- nleaves 1) irev))             ; reverse order, then push
	     (ytop (nth i edges))
	     (ybottom (nth (+ i 1) edges))
	     (pair (nth i leaves))
	     (xleft (first pair))
	     (xright (second pair))
	     (open (and (> (- ytop ymin) end-tol)
			(> (- ymax ybottom) end-tol)
			(> xright xmin) (> xmax xleft)))
	     (xl (max xleft xmin))
	     (xr (min xright xmax))
	     (pair-o (nth i leaves-o))
	     (xleft-o (first pair-o))
	     (xright-o (second pair-o))
	     (open-o (and (> (- ytop ymin-o) end-tol)
			  (> (- ymax-o ybottom) end-tol)
			  (> xright-o xmin-o) (> xmax-o xleft-o)))
	     (xl-o (max xleft-o xmin-o))
	     (xr-o (min xright-o xmax-o)))
	;; (format t "open ~A  open-o ~A  xl ~A  xl-o ~A  xr ~A  xr-o ~A~%"
	;;     open open-o xl xl-o xr xr-o)
	(push (make-lpair :open open :open-o open-o
			  :xl xl :xl-o xl-o :xr xr :xr-o xr-o)
	      lpairs)))))

;;;-------------------------------------------------------------

(defun collim-warnings (copy-coll curr-coll)

  "collim-warnings copy-coll curr-coll

Returns list of strings describing warnings about collimator in current beam,
concerning differences going from first arg to second arg, or NIL if there
are no user-provided changes."

  ;; Both args are copied collimators in beams which are copies
  ;; made from original beam in Prism plan - first is unmodified,
  ;; while second may be modified by user.

  ;; Message text uses Elekta coordinate systems and nomenclature, not Prism.

  (let* ((end-tol 0.3)
	 (tol 0.3)                                  ; edge of leaf or jaw tol
	 (min-field 2.0)        ; warn if field is smaller in either dimension
	 (nleaves (length (leaf-settings curr-coll)))
	 (xmin-o (x1 copy-coll))
	 (xmax-o (x2 copy-coll))
	 (ymin-o (y1 copy-coll))
	 (ymax-o (y2 copy-coll))
	 (xmin (x1 curr-coll))
	 (xmax (x2 curr-coll))
	 (ymin (y1 curr-coll))
	 (ymax (y2 curr-coll))
	 (flagpole? (flag-diff copy-coll curr-coll))
	 (changed (and (not flagpole?)
		       (or (/= ymax ymax-o) (/= ymin ymin-o)
			   (/= xmax xmax-o) (/= xmin xmin-o))))
	 (shapes (shape-diff copy-coll curr-coll end-tol))
	 (nopen 0)                             ; number of exposed open leaves
	 (maxwidth 0.0)                             ; max leaf opening
	 (wl nil))                              ; wl is warning list to return

    ;; push warning messages in opposite order they will appear
    (dotimes (irev nleaves)
      (let* ((i (- (- nleaves 1) irev))
	     (lf (nth i shapes))
	     (open (lpair-open lf))
	     (xl (lpair-xl lf))
	     (xr (lpair-xr lf))
	     (xwidth (- xr xl))
	     (open-o (lpair-open-o lf))
	     (xl-o (lpair-xl-o lf))
	     (xr-o (lpair-xr-o lf)))

	(if open (setf nopen (+ nopen 1)))
	(if (and open (> xwidth maxwidth)) (setf maxwidth xwidth))

	(cond
	  ((and open-o (not open))
	   (setf changed t)
	   (push (format nil "Leaf pair ~A changed from open to closed"
			 (+ i 1)) wl))
	  ((and (not open-o) open)
	   (setf changed t)
	   (push (format nil "Leaf pair ~A changed from closed to open"
			 (+ i 1)) wl))
	  ((and open-o open)
	   ;; two when's are not mutually exclusive
	   (when (> (abs (- xl-o xl)) tol)
	     (setf changed t)
	     (push
	       (format
		 nil "At leaf pair ~A, left edge changed from ~5,2F to ~5,2F"
		 (+ i 1) (- xl-o) (- xl))
	       wl))
	   (when (> (abs (- xr-o xr)) tol)
	     (setf changed t)
	     (push
	       (format
		 nil
		 "At leaf pair ~A, right edge changed from ~5,2F to ~5,2F"
		 (+ i 1) xr-o xr)
	       wl))))))

    (unless flagpole?
      (if (> (abs (- xmin xmin-o)) tol)
	  (push (format nil "Y2 changed from ~5,2F to ~5,2F"
			(- xmin-o) (- xmin)) wl))
      (if (> (abs (- xmax xmax-o)) tol)
	  (push (format nil "Y1 changed from ~5,2F to ~5,2F" xmax-o xmax) wl))
      (if (> (abs (- ymin ymin-o)) tol)
	  (push (format nil "X2 changed from ~5,2F to ~5,2F"
			(- ymin-o) (- ymin)) wl))
      (if (> (abs (- ymax ymax-o)) tol)
	  (push (format nil "X1 changed from ~5,2F to ~5,2F" ymax-o ymax) wl)))

    (if changed (push "Field shape has been changed from planned shape" wl))

    (if (< maxwidth min-field)
	(push (format nil "Field width (jaws and leaves) less than ~3,1F cm"
		      min-field)
	      wl))
    (case nopen
      (0 (push "No leaves open" wl))
      (1 (push "Only one leaf open" wl))
      (2 (push "Only two leaves open" wl)))
    (if (< (- ymax ymin) min-field)
	(push (format nil "X jaws open less than ~3,1F cm" min-field) wl))
    (if (< (- xmax xmin) min-field)
	(push (format nil "Y jaws open less than ~3,1F cm" min-field) wl))

    (when flagpole?
      (if (not changed) (push "prescribed field shape preserved" wl))
      (push
	(format nil "Adjusted jaws and leaves to meet Elekta constraints~A"
		(if (not changed) "," ""))
	wl))

    wl))

;;;=============================================================
;;; End.
