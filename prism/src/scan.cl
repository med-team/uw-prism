;;;
;;; scan
;;;
;;; Functions which implement scanner.
;;;
;;; 09-Feb-1994 D. Nguyen created and adapted file from previous works.
;;; 16-Oct-1995 D. Nguyen cleaned up code to comply with documentation.
;;;

(in-package :prism)

(defvar *scan-default-max-dose* 20000)

           
;;; Requires spots file loaded first.


;;;--------------------------------------------------
;;; STRUCTURES USED IN DOSE PSTRUCT SCANNING...
;;;

(defstruct active-edge 
  "ACTIVE-EDGE structure.
This structure holds the information needed about an edge currently
being scanned by the pstruct scanning routines.

ending-y = the y value this edge will terminate at (int).
delta-x = the change in x value for each single increment in y (float).
curr-x = the current location on the x axis (float).

bel 7/19/90"
  (ending-y 0 :type integer)
  (delta-x 0.0 :type single-float)
  (curr-x 0.0 :type single-float))


(defstruct scan
  "SCAN structure
This simple structure is used by the pstruct scanning routines to return a
strip of points along the x axis... two integers, beginning x and the
length of the strip are included.  Not extraodinarily useful.  It is
assumed that the y and z locations are known.

bel 7/19/90"
  (min-x 0 :type integer)
  (max-x 0 :type integer)
  (len 0 :type integer))


(defstruct image-slice
  "IMAGE-SLICE structure
This structure is used by the pstruct scanning routines to hold 
information regarding the current state of a single contour slice
within a pstruct.

max-z, min-z = the range in the z axis this slice covers.
active-edges = a list of active-edge structures for edges.
waiting-edges = a list of edges that have NOT yet been activated.

bel 7/19/90"
  (max-z 0 :type integer)
  (min-z 0 :type integer)
  active-edges
  waiting-edges)


(defstruct seg
  "SEG structure
This structure contains all of the information to id a plane in the
3-d image space of interest.  It assumes the plane is flat in the x-z
plane."
  (min-x 0 :type integer)
  (max-x 0 :type integer)
  (y 0 :type integer)
  (z 0 :type integer))


(defstruct (unfin-spot
  (:print-function
   (lambda (p s k)
     (declare (ignore k))
     (format s "<UNFIN-SPOT ~a>"
	     (unfin-spot-id p)))))

  "UNFIN-SPOT structure.
This is an internal structure used when building new spots in the 
scanning routines.  During this time period each spot is grouped
with the segments across its leading edge.  After the spot is
finished these segments can be thrown away.  <well actually Chris says
that she needs that information for other routines... I have tried to
accumulate the segments in the segment accumulator slot but have been
having trouble because they seem to end up as a linked ring with no end!
Possibly because of using some deletes instead of removes in the code??>

bel 8/22/90."

  all-segs
  prev-segs
  curr-segs
  peak-dose
  all-doses
  voxel-count
  limit
  min-box
  max-box
  id)


;;;
;;; MACRO ROUTINES...
;;;

(defun form-edges (conts)
  "FORM-EDGES
INPUTS= a list of contours.
OUTPUTS= a list of vertex pairs of the edges in the contour.

This is a function used by the pstruct scanning routines.  It forms
a list of edges (((x1 y1) (x2 y2)) ((x2 y2) (x3 y3))) from the vertices
of the contours.  The edges are sorted such that the first point is
below the second point. 

bel 7/19/90"
  (let ((verts (apply 'append
		      (mapcar #'(lambda (con) (pr:vertices con))
			      conts))))
    (mapcar #'(lambda (v1 v2)
		(if (< (cadr v1) (cadr v2))
		    (list v1 v2)
		  (list v2 v1)))
	    verts
	    (append (cdr verts) (list (car verts))))))


(defmacro activate-edges (ready-edges)
  "ACTIVATE-EDGES
INPUTS= list of edges ready to be activated.
OUTPUTS= list of active-edge structures.

This MACRO is used by the pstruct scanners to initialize active edge
structures.  curr-x is set to the x value of the first (bottom)
vertex of the edge; delta-x is set to the slope of the edge; and 
ending-y is set to the y value of the second vertex (upper)

bel 7/19/90"
  `(mapcan #'(lambda (edge)
	       (let ((start-point (car edge))
		     (end-point (cadr edge)))
		 (list (make-active-edge
			:ending-y (cadr end-point)
			:delta-x
			(coerce (/
				 (- (car end-point) (car start-point))
				 (- (cadr end-point) (cadr start-point)))
				'single-float)
			:curr-x (coerce (car start-point) 'single-float)))))
	   ,ready-edges))


(defun get-average (nums)
  "GET-AVERAGE 
INPUTS = a list of numbers
OUTPUTS = the average of the numbers

cms 11/91"
  (let ((count 0)
	(total 0))
    (dolist (n nums)
      (setf count (+ 1 count))
      (setf total (+ total n)))
    (float (/ total count))))


(defun finish-spot (done-spot)
  "FINISH-SPOT
INPUTS= a finished spot structure.
OUTPUT= a spot object.

bel 8/24/90."

  (make-instance 'spot
    :peak-dose (unfin-spot-peak-dose done-spot)
    :average-dose (get-average (unfin-spot-all-doses done-spot))
    :limit (unfin-spot-limit done-spot)
    :voxel-count (unfin-spot-voxel-count done-spot)
    :center (mapcar #'half-between 
		    (unfin-spot-min-box done-spot)
		    (unfin-spot-max-box done-spot))
    :all-segs (unfin-spot-all-segs done-spot)))


(defmacro init-unfin-spot (new-seg max-seg-dose all-doses lim)
  "INIT-UNFIN-SPOT
INPUTS= the new segement of out of bounds points.
	the max out of bounds dosage.
	the limit value the segment violated.
OUTPUT= a unfin-spot structure properly initialized.

bel 8/29/90."
  `(make-unfin-spot
    :peak-dose ,max-seg-dose
    :all-doses ,all-doses
    :voxel-count (1+ (- (seg-max-x ,new-seg)
		      (seg-min-x ,new-seg)))
    :limit ,lim
    :curr-segs (list ,new-seg)
    :min-box (list 
	      (seg-min-x ,new-seg)
	      (seg-y ,new-seg)
	      (seg-z ,new-seg))
    :max-box (list
	      (seg-max-x ,new-seg)
	      (seg-y ,new-seg)
	      (seg-z ,new-seg))
    :id (gensym "us")
    :prev-segs nil))


(defmacro update-spot-lists (curr-spots found-spots)
  "UPDATE-SPOT-LISTS
INPUTS= the name of a list of current unfin-spots.
        the name of a list of found spots.
OUTPUT= Updates the unfin-spots in the current list.  Removes unfin-spots
   from the current list that no longer have any segments on the leading 
   edge.  Transforms those unfin-spots into spot objects and adds them to 
   the list of found-spots.

This macro is intended solely for use by the scan-pstruct routine.  It updates
the lists of unfinished cold or hot spots given to it and moves those spots
that cannot be adjacent to anything in the future (by the fact that no
new points were found at the current y level) to the found-spots list.

bel 8/29/90."
  `(setq ,curr-spots 
	 (multiple-value-bind
	  (done-spots not-done-yet)
	  (split-sequence #'null

			  (mapcar 
			   #'(lambda (unfin-spot)
			       (setf (unfin-spot-all-segs unfin-spot)
				 (append (unfin-spot-curr-segs unfin-spot)
					 (unfin-spot-all-segs unfin-spot)))
			       (setf (unfin-spot-prev-segs unfin-spot)
				     (unfin-spot-curr-segs unfin-spot))
			       (setf (unfin-spot-curr-segs unfin-spot) nil)
			       unfin-spot)
			   ,curr-spots)

			  :key 'unfin-spot-prev-segs)
	  (setq ,found-spots (append ,found-spots 
				    (mapcar 'finish-spot done-spots)))
	  not-done-yet)))

  
(defun advance-edges (act-edges)
  "ADVANCE-EDGES
INPUTS= a list of active-edge structures;
OUTPUTS= a list of active-edge structures incremented in y axis.

This function is used by the pstruct scanners to advance active edges
up the y axis.  All that really happens is that delta-x is added to 
curr-x and the new list returned.

bel 7/19/90"
  (mapcar #'(lambda (edge)
	      (incf (active-edge-curr-x edge)
		    (active-edge-delta-x edge))
	      edge)
	  act-edges))


(defmacro sort-active-edges (edges)
  "SORT-ACTIVE-EDGES
INPUTS= a list of active edge structures
OUTPUT= the same list sorted by increasing current y value.

bel 8/24/90."
  `(sort ,edges #'< :key #'active-edge-curr-x))


(defun start-slice (contours init-y min-z max-z)
  "START-SLICE
INPUTS= a list of contours in the current slice;
	a starting y value;
	min-z and max-z for depth of slice.
OUTPUT= an image slice correctly initialized.

This is an initialization routine for a the pstruct scanner.  Given
a contour and a current y value for the *PSTRUCT*, information is 
extracted from the contour to build an initial image slice.  In
particular edges are formed, those ready to be activated are 
activated and put on the active-edges list, those not ready are
placed on the waiting-edges list.  It is assumed that the starting y
value is less-than-or-equal to any vertices in the contour.

Note also that any horizontile edges are removed at this point since
the edges at each end of the horizontile one may be used to scan
the points along the horizontile edge.

bel 7/19/90"
  (let* ((edges 
	  (remove-if #'(lambda (edge)
		     ; eliminate horizontile edges, edges at each end will
		     ;  activate instead.
			 (let ((start-point (car edge))
			       (end-point (cadr edge)))
			   (= (cadr end-point) (cadr start-point))))
		     (form-edges contours))))

    (multiple-value-bind 
	(ready-edges not-ready-edges)
	(split-sequence 
	 #'(lambda (y) (= y init-y))
	 edges
	 :key #'cadar)
      
      (make-image-slice
       :max-z max-z
       :min-z min-z
       :active-edges
       (sort-active-edges
	(activate-edges
	 ready-edges))
       :waiting-edges not-ready-edges))))


(defun advance-slice (slice y)
  "ADVANCE-SLICE
INPUT= an image-slice structure.
OUTPUT= The image-slice is directly modified to advance it along
the y axis.  Additionally the image-slice is returned.

The scanning routine advances an image slice once up the y-axis.
Curr-y is of course incremented; all active edges are advanced;
those that are completed are removed from the active edge list;
new edges ready to be activated are and added to the active-edge
list as well as removed from the waiting-edge list.

The new active edge list is sorted in order to keep the edges in
left to right order.

Please see the source code for doc on handling special cases.

bel 7/19/90"

;A special case occures with edges that are ending, normally an
;edge that is ending is removed as the ending y value of the edge
;is reached.  This allows the connecting edge that will start at
;that y-value to start and be used for that y scan.   However, in
;the case of two edges which connect in a local max point, normal
;removal of the edges would cause the scanner to ignore the final
;scan at the top y-value (this is particularly critical if two
;edges end with a horizontile connecting them at the top, such
;that an long length of points would not be properly scanned). 

;As a result edges are removed prior to their final y when they
;connect with a new edge.  Otherwise the are allowed to remain
;for one more y-pass to complete the scanning properly.
  (with-accessors ((active image-slice-active-edges)
		   (waiting image-slice-waiting-edges))
		  slice
   (multiple-value-bind
    (ready not-ready)
    (split-sequence
     #'(lambda (this-y) (= this-y y))
     waiting
     :key #'cadar)
 
    (setq active 
	  (sort-active-edges
	   (append
	    (remove-if #'(lambda (edge) 
			   (or (< (active-edge-ending-y edge) y)
			       (and (<= (active-edge-ending-y edge) y)
				    (find (round (active-edge-curr-x edge))
					  ready
					  :key #'caar))))
		       (advance-edges active))
	    (activate-edges ready))))
    (setq waiting not-ready))
   slice))


(defun scan-strips (slice)
  "SCAN-STRIPS
INPUTS= an image slice.
OUTPUTS= a list of scan strips along the x axis which are included inside the
pstruct of interest.

This scanning routine accepts an image slice and returns the strips
which are between the active edges.  The slice is NOT advanced.

bel 7/19/90"
  (with-accessors ((y image-slice-curr-y)
		   (max-z image-slice-max-z)
		   (min-z image-slice-min-z)
		   (active image-slice-active-edges))
		  slice
    (flet ((scan-strip (start-edge end-edge)
	      (let ((start-x (round (active-edge-curr-x start-edge))))
		(make-scan
		 :min-x start-x
		 :len (1+ (- (round (active-edge-curr-x end-edge))
			     start-x))
		 :max-x (round (active-edge-curr-x end-edge))))))
       (mapcar #'scan-strip
	       active
	       (cdr active)))))


(defun half-between (minpt maxpt)
  "HALF-BETWEEN
INPUTS= a first value
	a second value such that first <= second.
OUTPUT= the floating point value 1/2 way between the two inputs.

bel 8/24/90."
  (+ (/ (coerce (- maxpt minpt) 'single-float) 2.0) minpt))


(defun start-all-slices (contours init-y
				  &optional (begin-z 
					     (pr:z (car contours))))
  "START-ALL-SLICES
INPUTS= a list of contours describing a pstruct.
	an initial y value to use for starting ready edges.
	an optional min-z value, beginning z val of current slice.
OUTPUTS= an initialized list of image-slice structures for the pstruct.

This is the primary initialization routine for SCAN-PSTRUCT.  It takes
each contour, and builds an image slice around it.  The min-z and max-z
values are set such that they equaly divide the distance between the
contours.  The first contour has a min-z equal to its z value, the final
contour has a max-z equal to its z value.

bel 7/19/90."

  (let ((this-z (pr:z (car contours)))
	end-z)
    (multiple-value-bind
	(conts-at-this-z remaining-conts)
	(split-sequence
	 #'(lambda (z)
	     (= z this-z))
	 contours
	 :key #'(lambda (cont) (pr:z cont)))
      
      (cond
       ((null remaining-conts) 
	(values (list (start-slice conts-at-this-z init-y begin-z this-z))))
       (t
	(setq end-z (floor
		     (half-between 
		      this-z
		      (pr:z (car remaining-conts)))))
	
	(values
	 (cons (start-slice conts-at-this-z init-y begin-z end-z)
	       (start-all-slices remaining-conts init-y (1+ end-z)))))))))


(defun merge-spots (new-spot existing-spots 
			     &key hot)
  "MERGE-SPOTS
INPUTS: an unfin-spot struc for a new spot to be merged
	a list of unfin-spots that already exist
OUTPUT:	A list of unfin-spots with the new segment merged into it.

first find any spots in the list that are adjacent to the new one.
if there are none, return a list of all of them (new & old).
if there are 1 or more merge them and return a list of the resulting
spots.

bel 8/20/90."

  (flet ((adjacent-to-new (seg-list)
	   (some #'(lambda (test-seg)
		     (seg-overlap 
		      (car (unfin-spot-curr-segs new-spot))
		      test-seg))
		 seg-list)))
    
    (multiple-value-bind
	(adjacent-spots all-the-rest)
	(split-sequence #'adjacent-to-new
			existing-spots
			:key #'unfin-spot-prev-segs)
      
      (let ((spots-to-merge (cons new-spot adjacent-spots)))
	
	(setf (unfin-spot-peak-dose new-spot)
	  (cond (hot
		 (apply 'max 
			(mapcar #'unfin-spot-peak-dose spots-to-merge)))
		(t
		 (apply 'min 
			(mapcar #'unfin-spot-peak-dose spots-to-merge)))))
	
	(setf (unfin-spot-min-box new-spot)
	  (apply 'mapcar 
		 (cons 'min 
		       (mapcar #'unfin-spot-min-box spots-to-merge))))
	
	(setf (unfin-spot-max-box new-spot)
	  (apply 'mapcar 
		 (cons 'max
		       (mapcar #'unfin-spot-max-box spots-to-merge))))
	
	(setf (unfin-spot-voxel-count new-spot)
	  (apply '+ (mapcar 'unfin-spot-voxel-count spots-to-merge)))
	
	(setf (unfin-spot-all-doses new-spot)
	  (apply 'append (mapcar 'unfin-spot-all-doses spots-to-merge)))
	
	(setf (unfin-spot-all-segs new-spot)
	  (apply 'append (mapcar 'unfin-spot-all-segs spots-to-merge)))
	
	(setf (unfin-spot-curr-segs new-spot)
	  (apply 'append (mapcar 'unfin-spot-curr-segs spots-to-merge)))
	
	(setf (unfin-spot-prev-segs new-spot)
	  (apply 'append (mapcar 'unfin-spot-prev-segs spots-to-merge)))
	
	(values (cons new-spot all-the-rest))))))


(defun seg-overlap (seg1 seg2)
  "SEG-OVERLAP
INPUTS= a segment structure.
	a segment struct from the previous y row.
OUTPUT= t if segments overlap.
	nil otherwise.

bel 8/15/90"
  (if (and 
       (>= (seg-max-x seg1) (1- (seg-min-x seg2)))
       (<= (seg-min-x seg1) (1+ (seg-max-x seg2)))
       (>= (seg-z seg1) (1- (seg-z seg2)))
       (<= (seg-z seg1) (1+ (seg-z seg2))))
      t
    nil))


(defun split-sequence (test 
		       sequence
		       &key (key nil skey))
  "SPLIT-SEQUENCE
INPUTS: a test function of 1 argument
	a sequence of arguments to be tested
 	optional-- :key-- access key function.
OUTPUT: a sequence formed as if (remove-if-not test sequence) were called.
	a sequence formed as if (remove-if test sequence) were called.

bel 8/20/90."

  (let (rem-if-not rem-if)
    (dolist (item sequence)
      (cond
       ((apply test (list 
		     (if skey (funcall key item)
		       item)))
	(setq rem-if-not (append rem-if-not (list item))))
       (t 
	(setq rem-if (append rem-if (list item))))))
    (values rem-if-not rem-if)))


(defmacro unconvert (coordinate size dimension origin)
  `(+ ,origin (* ,coordinate (/ ,size (- ,dimension 1)))))


(defmacro convert (coordinate size dimension origin)
  `(round (/ (* (- ,dimension 1) (- ,coordinate ,origin)) ,size)))


;;;
;;; scan
;;; convert-pstruct-to-dosecomp-scheme
;;; call scan-pstruct
;;; convert-spot-from-dosecomp-scheme
;;;

(defun scan (pstruct dose-grid dose-array 
	     &key (upper-lim 1000) 
                  (lower-lim 0)
		  (dvh-bin-size 0)
		  (max-dose *scan-default-max-dose*))
  (let* (new-contours
	 (x-origin (pr:x-origin dose-grid))
	 (y-origin (pr:y-origin dose-grid))
	 (z-origin (pr:z-origin dose-grid))
	 (x-dimension (pr:x-dim dose-grid))
	 (y-dimension (pr:y-dim dose-grid))
	 (z-dimension (pr:z-dim dose-grid))
	 (x-size (pr:x-size dose-grid))
	 (y-size (pr:y-size dose-grid))
	 (z-size (pr:z-size dose-grid))
	 center)

    ;; convert contours from patient space to dose-grid space
    (setf new-contours
      (mapcar 
       #'(lambda (cont)
	   (make-instance 'pr:contour
	     :z (convert (pr:z cont) z-size z-dimension z-origin)
	     :vertices
	     (mapcar 
	      #'(lambda (vertex)
		  (list (convert (first vertex) x-size x-dimension x-origin)
			(convert (second vertex) y-size y-dimension y-origin)))
	      (pr:vertices cont))))
       (pr:contours pstruct)))
    
    ;; function scan-pstruct does the actual scanning work
    (multiple-value-bind (spots dvh-array total-volume)
	(scan-pstruct pstruct new-contours dose-array
		      :upper-lim upper-lim 
		      :lower-lim lower-lim
		      :dvh-bin-size dvh-bin-size 
		      :max-dose max-dose)
    
      ;; Unconvert spots back to patient space.   Ideally, everything about a
      ;; spot does not get saved and we don't have to unconvert everything.
      
      (dolist (spot spots)
	(dolist (seg (all-segs spot))
	  (setf (seg-min-x seg) 
		(unconvert (seg-min-x seg) x-size x-dimension x-origin))
	  (setf (seg-max-x seg) 
		(unconvert (seg-max-x seg) x-size x-dimension x-origin))
	  (setf (seg-y seg) 
		(unconvert (seg-y seg) y-size y-dimension y-origin))
	  (setf (seg-z seg) 
		(unconvert (seg-z seg) z-size z-dimension z-origin)))
	(setf center (slot-value spot 'center))
	(rplaca center 
		(unconvert (first center) x-size x-dimension x-origin))
	(rplaca (cdr center) 
		(unconvert (second center) y-size y-dimension y-origin))
	(rplaca (cddr center) 
		(unconvert (third center) z-size z-dimension z-origin)))

      (values spots dvh-array total-volume))))


(defun scan-pstruct (pstruct contours image 
			     &key (upper-lim 1000)
			          (lower-lim 0)
				  (dvh-bin-size 0)
				  (max-dose *scan-default-max-dose*))
  "SCAN-PSTRUCT
INPUTS= a pstruct;
        contours, converted to dose-array indices
	a 3-d image array, a dose-array;
        key'd inputs...
	  :upper-lim == a max radiation limit testing for hot spots 
	  :lower-lim == a min radiation limit testing for cold spots
          :dvh-bin-size == bin size for dvh (if desired)
          :max-dose == determines size of dvh array
OUTPUTS= a list of spot objects, both hot and cold in a single list;
         a dvh-array;
         total volume of pstruct (in voxels), also equal to sum of
          all the elements in the dvh-array.

This is a complex routine that sorts the contours of pstruct into 
increasing z order, then calls START-ALL-SLICES to intialize image
slices.  

The pstruct specified is then scanned through and spots are assembled 
according to the limits passed.  All spots found outside either limit
are returned as a list at the end.

bel 7/19/90."
  (flet ((null-slice (slice)
	   (and (null (image-slice-active-edges slice))
		(null (image-slice-waiting-edges slice)))))	
    (let* ((conts 
	    (sort (copy-list contours)
		  #'<
		  :key #'(lambda (cont) (pr:z cont))))
	   (init-y
	    (apply 'min (apply 'append
			       (mapcar #'(lambda (cont) 
					   (mapcar #'cadr (pr:vertices cont)))
				       conts))))
	   (init-slices (start-all-slices conts init-y))
	   found-spots
	   hot-spots
	   cold-spots
	   (dvh-array-size (if (> dvh-bin-size 0)
			       (ceiling (/ (1+ max-dose) dvh-bin-size))))
	   (dvh-array (if (> dvh-bin-size 0)
			  (make-array dvh-array-size
				      :element-type 'integer
				      :initial-element 0
				      :adjustable T)))
	   (pstruct-volume 0)
	   (actual-max-dose 0))
      
      (do ((slices init-slices
		   (remove-if #'null-slice 
			      (mapcar #'(lambda (slice)
					  (advance-slice slice (1+ y)))
				      slices)))
	   (y init-y (1+ y)))
	  ((null slices))    ; do this loop until all image slices run out
                             ;   of contour edges to follow.
	(dolist (this-slice slices)
					;for each slice...
	     (dolist 
		 (strip (remove-duplicates 
			 (scan-strips this-slice)
			 :test #'(lambda (edge1 edge2)
				   (and (equal (active-edge-ending-y edge1)
					       (active-edge-ending-y edge2))
					(equal (active-edge-curr-x edge1)
					       (active-edge-curr-x edge2))))))
	       
					; get the scan strips between edge pairs
					;   at the current y value in the slice.
	       
	       (do ((z (image-slice-min-z this-slice) (1+ z))
		    (prev-status 0 0)
		    (new-seg () ())
		    (max-seg-dose () ())
		    (all-doses () ()))
		   
		   ((> z (image-slice-max-z this-slice)))

;strip is a rectangular patch, length from contour edge to contour edge,
;  depth from min z to max z.  We want to scan the length of between
;  the contour edges progressively moving across the z axis.
		     
		 (do ((x (scan-min-x strip) (1+ x))
		      (image-value 0))
		     ((> x (scan-max-x strip)))

;here we are scanning along the length of the scan strip, progressing
;  in the x dimension.  At each point in the array we just compare or 
;  not the value is outside the limits of interest.

;since we know the direction of scan, when we begin to see values outside
;  of the tolerance limits we mark the beginning, then wait to see where
;  it again falls within the tolerance.  These one-dimensional segments
;  are represented by a 3-d beginning point and an ending x value.  Also
;  maintained are max dosage beyond tolerance values.  An flet routine
;  named "init-unfin-spot" below is used to initialize an unfin-spot struct
;  which is passed to merge-spots along with lists of either hot-spots
;  or cold-spots that already exist.

;Merge-spot takes care of clumping these unfin-spots.  

;a spot is finished when there are no longer any segments found adjacent
;  to it at the current y scanning level.  When a spot is finished the
;  spot object is taken from the unfin-spot structure and placed on the
;  found-spots list.  Found-spots is then returned at the end of the
;  routine.

			 ;; make sure it's within array bounds
			 ;; (check that array-dimensions are not negative, too)

		   (if (and (< x (array-dimension image 0))
			    (>= x 0)
			    (< y (array-dimension image 1))
			    (>= y 0)
			    (< z (array-dimension image 2))
			    (>= z 0))
		       (setq image-value (aref image x y z))
		     
		     ;; This used to set it to nil, but I want it
		     ;; to be zero, since I consider this to be
		     ;; getting no dose at all.
		     (setq image-value 0))

		   ;; update dvh array if desired.  Also increment pstruct
		   ;; volume, and reset actual-max-dose if applicable.
		   (when dvh-array
			 (let* ((pre-index (floor (/ image-value dvh-bin-size)))
				(index (if (< pre-index dvh-array-size)
					   pre-index
					 (1- dvh-array-size))))
			   (setf (aref dvh-array index)
			     (1+ (aref dvh-array index))))
			 (incf pstruct-volume)
			 (if (> image-value actual-max-dose)
			     (setf actual-max-dose image-value)))
		   
		   (cond 
		    ((null image-value)	;this point is not in the cube
		     (format t "We've hit a dimension out of bounds~%")
		     (cond 
					;prev point was normal.
		      ((zerop prev-status) t)
		      
					;prev point was cold.
					;close off existing segment
		      ((minusp prev-status)
		       (setf (seg-max-x new-seg) (1- x))
		       (setq prev-status 0
			     cold-spots (merge-spots
					 (init-unfin-spot
					  new-seg
					  max-seg-dose
					  all-doses
					  lower-lim)
					 cold-spots
					 :hot nil)
			     new-seg nil ;; CMS
			     all-doses nil))
		      
					;prev point must been hot.
		      (t
		       (setf (seg-max-x new-seg) (1- x))
			       (setq prev-status 0
				     hot-spots (merge-spots
						(init-unfin-spot
						 new-seg
						 max-seg-dose
						 all-doses
						 upper-lim)
						hot-spots
						:hot t)
				     new-seg nil ;;CMS
				     all-doses nil))))
		    
					;this point cold!
		    ((< image-value lower-lim)
		     (cond
					;prior point was in range.
					;start a new segment.
		      ((zerop prev-status)
		       (setq prev-status -1
			     new-seg (make-seg :min-x x
					       :y y
					       :z z)
			     max-seg-dose image-value
			     all-doses (list image-value)))
		      
					;prior point also cold.
					;just update max out of tolerance pt.
			      ((minusp prev-status)
			       (setq max-seg-dose
				 (min image-value max-seg-dose)
				 all-doses (cons image-value all-doses)))
			      
					;prior point must have been hot.
					;close existing seg and start a 
					;new one.
			      (t
			       (setf (seg-max-x new-seg) (1- x))
			       (setq hot-spots (merge-spots
						(init-unfin-spot
						 new-seg
						  max-seg-dose
						  all-doses
						  upper-lim)
						hot-spots
						:hot t)
				     prev-status -1
				     new-seg (make-seg :min-x x
						       :y y
						       :z z)
				     max-seg-dose image-value
				     all-doses (list image-value)))))
		    
					;this point is hot!
		    ((> image-value upper-lim)
		     (cond
					;prior point was in range.
					;start a new segment.
		      ((zerop prev-status)
		       (setq prev-status +1
			     new-seg (make-seg :min-x x
					       :y y
					       :z z)
			     max-seg-dose image-value
			     all-doses (list image-value)))
		      
					;prior point also hot.
					;just update max out of tolerance pt.
		      ((plusp prev-status)
		       (setq max-seg-dose
			 (max image-value max-seg-dose)
			 all-doses (cons image-value all-doses)))
		      
					;prior point must have been cold.
					;close existing seg and start a 
					;new one.
		      (t
		       (setf (seg-max-x new-seg) (1- x))
		       (setq cold-spots (merge-spots
					 (init-unfin-spot
					  new-seg
					  max-seg-dose
					  all-doses
					  lower-lim)
					 cold-spots
					 :hot nil)
			     prev-status +1
			     new-seg (make-seg :min-x x
					       :y y
					       :z z)
			     max-seg-dose image-value
			     all-doses (list image-value)))))
		    
		    (t			;this point within limits.
		     (cond 
					;prev point was too.
		      ((zerop prev-status) t)
		      
					;prev point was cold.
					;close off existing segment
		      ((minusp prev-status)
		       (setf (seg-max-x new-seg) (1- x))
		       (setq prev-status 0
			     cold-spots (merge-spots
					 (init-unfin-spot
					  new-seg
					  max-seg-dose
					  all-doses
					  lower-lim)
					 cold-spots
					 :hot nil)
			     new-seg nil ;;CMS
			     all-doses nil))
		      
					;prev point must been hot.
		      (t
		       (setf (seg-max-x new-seg) (1- x))
		       (setq prev-status 0
			     hot-spots (merge-spots
					(init-unfin-spot
					 new-seg
					 max-seg-dose
					 all-doses
					 upper-lim)
					hot-spots
					:hot t)
			     new-seg nil ;;CMS
			     all-doses nil))))
		    )			; terminate cond
		   )			; should terminate "do x..."
		 
		 (cond
		  ((zerop prev-status) t)
		  ((minusp prev-status)
		   (setf (seg-max-x new-seg) (scan-max-x strip))
		   (setq cold-spots (merge-spots
				     (init-unfin-spot
				      new-seg
				      max-seg-dose
				      all-doses
				      lower-lim)
				     cold-spots
				     :hot nil)
			 new-seg nil ;;CMS
			 ))
		  (t
		   (setf (seg-max-x new-seg) (scan-max-x strip))
		   (setq hot-spots (merge-spots
				    (init-unfin-spot
				     new-seg
				     max-seg-dose
				     all-doses
				     upper-lim)
				    hot-spots
				    :hot t)
			 new-seg nil ;;CMS
			 )))
		 
		 )			; should terminate "do z..."
	       )			;should terminate "dolist seg..."
	  )				; should terminate "dolist this-slice..."
	
	(update-spot-lists hot-spots found-spots)
	(update-spot-lists cold-spots found-spots)
	)
      
      (dolist (done-spot (append hot-spots cold-spots))
	(push (finish-spot done-spot) found-spots))
      
      (dolist (spot found-spots)
	(setf (surrounding-pstruct spot) pstruct))
      
      ;; Right now, save the segments.
      ;; Ultimately this routine should do something smart with
      ;; them (like find the diameter and depth, or make contours).
      ;;       (dolist (spot found-spots)
      ;;	 (setf (all-segs spot) nil))

      ;; Readjust dvh-array dimension to actual-max-dose.
      ;; And reset bins to partial volumes.
      (when dvh-array
	(let ((actual-array-size (ceiling (/ (1+ actual-max-dose)
					     dvh-bin-size))))
	  (adjust-array dvh-array actual-array-size)
	  (do ((index 0 (1+ index)))
	      ((= index actual-array-size))
	    (setf (aref dvh-array index)
	      (/ (aref dvh-array index) pstruct-volume)))))
      
      (values found-spots dvh-array pstruct-volume))))


;; Assumes hot spots for organs are 5% above tolerated dose and
;; cold spots for target are below 5% of required dose.
;; Returns two values:
;;  - a list of all spots
;;  - a list of dvh results, where each result is of the form
;;     (target/organ dvh-array total-volume).

(defun get-spots (plan patient &key (dvh-bin-size 0)
				    (max-dose *scan-default-max-dose*)
				    (beam nil))
  (let ((dose-grid (pr:dose-grid plan))
	(dose-array (if (null beam)
			(pr:grid (pr:sum-dose plan))
		      (pr:grid (pr:result beam))))
	required-dose
	tolerance-dose
	target
	delta
	all-spots
	dvh-results)
;; CAW @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
;    (setf target (pr:prescription-used plan))
    (setf target (first (coll:elements (targets patient))))
    (when (eq target nil)
      (format t "Error: there is not a target defined for ths patient.~%")) 
    (setf required-dose (if (null beam)
			    (pr:required-dose target)
			  (/ (pr:required-dose target)
			     (length (coll:elements (pr:beams plan))))))
;(setf required-dose 1000)
;; CAW @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    (setf delta (* .05 required-dose))
    
    ;; upper limit for target is a hack, because scanning code does not
    ;; deal with infinity as an upper limit.
    (multiple-value-bind (spots dvh-array volume)
                         (scan target dose-grid dose-array
			       :upper-lim most-positive-fixnum
			       :lower-lim (- required-dose delta)
			       :dvh-bin-size dvh-bin-size
			       :max-dose max-dose)
      (setf all-spots (append all-spots spots))
      (if (> dvh-bin-size 0)
	  (push (list target dvh-array volume) dvh-results)))
    
    ;; get spots for each organ.
    (dolist (organ (coll:elements (pr:anatomy patient)))
      (setf tolerance-dose (if (null beam)
			       (tolerance-dose organ)
			     (/ (tolerance-dose organ)
				(length (coll:elements (pr:beams plan))))))
      (setf delta (* .05 tolerance-dose))

      (multiple-value-bind (spots dvh-array volume)
                           (scan organ dose-grid dose-array
				 :upper-lim (+ tolerance-dose delta)
				 :lower-lim 0
				 :dvh-bin-size dvh-bin-size
				 :max-dose max-dose)
	(setf all-spots (append all-spots spots))
	(if (> dvh-bin-size 0)
	    (setf dvh-results (append dvh-results
				      (list (list organ dvh-array volume)))))))
    
    ;; The following is a hack, because the scan-pstruct code can't
    ;; deal with infinite upper-limit.  Thus, since it can find hot
    ;; spots in targets at the moment, we must get rid of them.
    (setf all-spots (remove-if-not #'(lambda (s) (or (high-dose-region? s)
						     (low-dose-region? s)))
			       all-spots))
    (values all-spots dvh-results)))


(defun get-beam-spots (plan patient 
		       &key (dvh-bin-size 0)
			    (max-dose *scan-default-max-dose*))
  (mapcar #'(lambda (beam)
	      (cons beam (get-spots plan patient 
				    :dvh-bin-size dvh-bin-size
				    :max-dose max-dose
				    :beam beam)))
	  (coll:elements (pr:beams plan))))
