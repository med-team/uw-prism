;;;
;;; coll-panels
;;;
;;; defines the various types of collimator panels and their methods
;;;
;;;  5-Sep-1993 I. Kalet split off from collimators module
;;;  1-Nov-1993 J. Unger add destroy method stub for mlc panel
;;; 30-Dec-1993 I. Kalet add full support for MLC
;;; 16-May-1994 I. Kalet add really full support for MLC
;;;  2-Jun-1994 I. Kalet change display-contour-editor to
;;; display-planar-editor, make make-collimator-panel a generic
;;; function, move update-portal-bev to bev-graphics, make size use a
;;; constant symbol, large.
;;;  3-Jun-1994 J. Unger redefine attributes of combination-coll, edit
;;; code involving combination-coll.
;;; 27-Jun-1994 I. Kalet change labels on SFD and BEAM PORTAL buttons
;;; on MLC subpanel.
;;; 12-Jul-1994 J. Unger coerce some incoming numbers announced from 
;;; textlines to single-float before assigning to collim attributes.
;;; 21-Jul-1994 J. Unger impl button for leaf-panel, impl & pass beam-for
;;; attribute to variable-jaw-collimator upon creation.
;;; 02-Aug-1994 J. Unger turn on leaf-panel for neutron vj coll beams.
;;; 04-Aug-1994 J. Unger turn off again.
;;; 05-Aug-1994 J. Unger add cnts-coll-panel class def & supporting code, 
;;; take hacks out of var-jaw-coll-panel, make nice, & move to cnts-coll-pnl
;;; 16-Sep-1994 J. Unger add leaf chart button to mlc & cnts
;;; collimator panels.
;;; 11-Jan-1995 I. Kalet destroy bev of mlc panel. Make beam-for an
;;; attribute of cnts and mlc panels, not the collimators, and name
;;; it beam-of instead.  Add plan-of, patient-of and pass to
;;; leaf-panel, etc.
;;; 30-Apr-1995 I. Kalet finish code to set the digitizer mag in the
;;; MLC panel contour editor according to the SFD.
;;; 15-Jan-1996 I. Kalet split multileaf-coll-panel into
;;; portal-coll-panel and multileaf-coll-panel, add
;;; electron-coll-panel and srs-coll-panel, latter from M. Phillips.
;;; 30-Sep-1996 I. Kalet update calls to bev-draw-all for new
;;; signature and make SFD textline numeric, put in-line code for
;;; electron cone square in refresh-portal-editor method.
;;; 21-Jan-1997 I. Kalet eliminate table-position.
;;; 29-Apr-1997 I. Kalet put beam name on portal editor title bar
;;;  4-May-1997 I. Kalet use label, not title, in sliderboxes.
;;;  5-Jun-1997 I. Kalet machine returns object, not name
;;; 23-Jun-1997 I. Kalet put in missing make-collimator-panel method
;;; for cnts-coll.  Fix electron portal and cone drawing code.
;;;  3-Oct-1997 BobGian inline-expand AVERAGE - keep it simple.
;;;  2-May-1998 I. Kalet use new chart-panel function for leaf chart.
;;; 11-Jun-1998 I. Kalet change :beam to :beam-for in make-view call.
;;; 17-Dec-1998 I. Kalet add energy selection to electron collimator
;;; panel.
;;; 25-Feb-1999 I. Kalet move find-center-vol from here to volumes.
;;; 23-Apr-1999 I. Kalet changes for support of multiple colormaps.
;;;  5-Sep-1999 I. Kalet modify portal-coll-panel, mlc-coll-panel and
;;; electron-coll-panel for new combined mlc-panel that does both
;;; portal contour and leaf settings.
;;; 19-Mar-2000 I. Kalet revisions for new chart code.
;;; 30-May-2000 I. Kalet correct error in call to chart-panel.
;;; 10-Sep-2000 I. Kalet remove obsolete srs collimator support,
;;; modify mlc-panel per new arrangements.
;;; 23-Nov-2001 I. Kalet add open-portal button, remove obsolete
;;; :angle input to mlc-panel.
;;; 15-May-2002 I. Kalet fix electron-coll-panel so that cutout
;;; contour rotates with collimator.  Add DRR and declutter controls.
;;; 19-Jan-2005 I. Kalet change make-contour-editor to make-planar-editor
;;;

(in-package :prism)

;;;---------------------------------------------
;;; these numbers represent space available on the beam panel
;;;---------------------------------------------

(defvar *coll-pan-width* 290)
(defvar *coll-pan-height* 325)

;;;---------------------------------------------

(defclass collimator-panel ()

  ((coll-for :accessor coll-for
	     :initarg :coll-for
	     :documentation "The collimator controlled by this panel")

   (panel-frame :accessor panel-frame
		:documentation "The SLIK frame containing the
collimator controls")

   (busy :accessor busy
	 :initform nil
	 :documentation "The busy flag for updates of settings")

   )

  (:documentation "The base collimator panel class has the common
elements of a reference to the collimator itself, the frame for the
panel and the busy flag.")

  )

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp collimator-panel)
				       &rest initargs)

  (setf (panel-frame cp)
    (apply #'sl:make-frame *coll-pan-width* *coll-pan-height*
	   :border-width 0 initargs)))

;;;---------------------------------------------

(defmethod destroy ((cp collimator-panel))

  (sl:destroy (panel-frame cp)))

;;;---------------------------------------------

(defclass symmetric-jaw-coll-panel (collimator-panel)

  ((sx :accessor sx
       :documentation "The slider for the x jaw setting")

   (sy :accessor sy
       :documentation "The slider for the y jaw setting")

   )

  )

;;;---------------------------------------------

(defmethod make-collimator-panel ((coll symmetric-jaw-coll) &rest initargs)

  "returns a collimator panel for collimator coll, and connects the
collimator settings to the panel sliders or other controls.  The type
of panel returned matches the type of collimator provided."

  (apply #'make-instance 'symmetric-jaw-coll-panel
	 :coll-for coll :allow-other-keys t initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp symmetric-jaw-coll-panel)
				       &rest initargs)

  (let* ((sw 260) ;; magic numbers from beam panel
	 (sh 30)
	 (fr (panel-frame cp))
	 (win (sl:window fr))
	 (font (sl:font fr)) ;; use font provided or defaulted
	 (coll (coll-for cp))
	 (x-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
		      :label "COLL X: " :parent win
		      :ulc-x 0 :ulc-y 0
		      :setting (x coll) :font font
		      initargs))
	 (y-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
		      :label "COLL Y: " :parent win
		      :ulc-x 0 :ulc-y 70
		      :setting (y coll) :font font
		      initargs)))
    ;; install them and connect them up to the collimator settings
    (setf (sx cp) x-sl
	  (sy cp) y-sl)
    (ev:add-notify cp (sl:value-changed x-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (x (coll-for cp)) 
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-x coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sx pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:value-changed y-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (y (coll-for cp)) 
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-y coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sy pan)) val)
			 (setf (busy pan) nil))))))

;;;---------------------------------------------

(defmethod destroy :before ((cp symmetric-jaw-coll-panel))

  (sl:destroy (sx cp))
  (sl:destroy (sy cp))
  (ev:remove-notify cp (new-coll-x (coll-for cp)))
  (ev:remove-notify cp (new-coll-y (coll-for cp))))

;;;---------------------------------------------

(defclass variable-jaw-coll-panel (collimator-panel)

  ((sx-sup :accessor sx-sup
	   :documentation "The slider for the x-sup jaw setting")

   (sy-sup :accessor sy-sup
	   :documentation "The slider for the y-sup jaw setting")

   (sx-inf :accessor sx-inf
	   :documentation "The slider for the x-inf jaw setting")

   (sy-inf :accessor sy-inf
	   :documentation "The slider for the y-inf jaw setting")

   )

  )

;;;---------------------------------------------

(defmethod make-collimator-panel ((coll variable-jaw-coll) &rest initargs)

  (apply #'make-instance 'variable-jaw-coll-panel
	 :coll-for coll :allow-other-keys t initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp variable-jaw-coll-panel)
				       &rest initargs)

  (let* ((sw 260) ;; magic numbers from beam panel
	 (sh 30)
	 (fr (panel-frame cp))
	 (win (sl:window fr))
	 (font (sl:font fr)) ;; use font provided or defaulted
	 (coll (coll-for cp))
	 (xsup-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
			 :label "COLL X SUP: " :parent win
			 :ulc-x 0 :ulc-y 0
			 :setting (x-sup coll) :font font
			 initargs))
	 (ysup-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
			 :label "COLL Y SUP: " :parent win
			 :ulc-x 0 :ulc-y 70
			 :setting (y-sup coll) :font font
			 initargs))
	 (xinf-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
			 :label "COLL X INF: " :parent win
			 :ulc-x 0 :ulc-y 140
			 :setting (x-inf coll) :font font
			 initargs))
	 (yinf-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
			 :label "COLL Y INF: " :parent win
			 :ulc-x 0 :ulc-y 210
			 :setting (y-inf coll) :font font
			 initargs)))
    ;; install them and connect them up to the collimator settings
    (setf (sx-sup cp) xsup-sl
	  (sy-sup cp) ysup-sl
	  (sx-inf cp) xinf-sl
	  (sy-inf cp) yinf-sl)
    (ev:add-notify cp (sl:value-changed xsup-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (x-sup (coll-for cp))
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-x-sup coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sx-sup pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:value-changed ysup-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (y-sup (coll-for cp))
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-y-sup coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sy-sup pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:value-changed xinf-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (x-inf (coll-for cp))
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-x-inf coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sx-inf pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:value-changed yinf-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (y-inf (coll-for cp)) 
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-y-inf coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sy-inf pan)) val)
			 (setf (busy pan) nil))))))

;;;---------------------------------------------

(defmethod destroy :before ((cp variable-jaw-coll-panel))

  (sl:destroy (sx-sup cp))
  (sl:destroy (sy-sup cp))
  (sl:destroy (sx-inf cp))
  (sl:destroy (sy-inf cp))
  (ev:remove-notify cp (new-coll-x-sup (coll-for cp)))
  (ev:remove-notify cp (new-coll-y-sup (coll-for cp)))
  (ev:remove-notify cp (new-coll-x-inf (coll-for cp)))
  (ev:remove-notify cp (new-coll-y-inf (coll-for cp))))

;;;---------------------------------------------

(defclass cnts-coll-panel (variable-jaw-coll-panel)

  ((beam-of :initarg :beam-of
	    :accessor beam-of
	    :documentation "The beam containing the collimator.")
   
   (plan-of :initarg :plan-of
	    :accessor plan-of
	    :documentation "The plan containing the beam.")
   
   (patient-of :initarg :patient-of
	       :accessor patient-of
	       :documentation "The current patient.")
   
   (leaf-btn :accessor leaf-btn
             :documentation "The leaf display button.")

   (leaf-panel :accessor leaf-panel
               :initform nil
               :documentation "The leaf panel for this collimator panel.")

   (chart-btn  :accessor chart-btn
               :documentation "The leaf chart button for this coll panel.")

   )

  )

;;;---------------------------------------------

(defmethod make-collimator-panel ((coll cnts-coll) &rest initargs) 

  (apply #'make-instance 'cnts-coll-panel
	 :coll-for coll :allow-other-keys t initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp cnts-coll-panel) &rest initargs)

  (setf (leaf-btn cp) 
    (apply #'sl:make-button 120 25
	   :label "LEAF EDIT" :parent (sl:window (panel-frame cp))
	   :ulc-x 0 :ulc-y 290 ;; arbitrary
	   initargs))
  (setf (chart-btn cp)
    (apply #'sl:make-button 120 25
           :label "LEAF CHART" :parent (sl:window (panel-frame cp))
           :ulc-x 150 :ulc-y 290
	   initargs))
  (ev:add-notify cp (sl:button-on (leaf-btn cp))
		 #'(lambda (pan bt)
		     (declare (ignore bt))
		     (setf (leaf-panel pan) 
		       (make-mlc-panel :beam-of (beam-of pan)
				       :plan-of (plan-of pan)
				       :patient-of (patient-of pan)))
		     (ev:add-notify pan (deleted (leaf-panel pan))
				    #'(lambda (pan lp)
					(declare (ignore lp))
					(setf (leaf-panel pan) nil)
					(unless (busy pan)
					  (setf (busy pan) t)
					  (setf (sl:on (leaf-btn cp)) nil)
					  (setf (busy pan) nil))))))
  (ev:add-notify cp (sl:button-off (leaf-btn cp))
		 #'(lambda (pan bt)
		     (declare (ignore bt))
		     (unless (busy pan)
		       (setf (busy pan) t)
		       (destroy (leaf-panel pan))
		       (setf (busy pan) nil))))
  (ev:add-notify cp (sl:button-on (chart-btn cp))
                 #'(lambda (pan bt)
                     (declare (ignore bt))
                     (chart-panel 'leaf
				  (patient-of pan) (plan-of pan)
				  (beam-of pan))
                     (setf (sl:on (chart-btn cp)) nil))))

;;;---------------------------------------------

(defmethod destroy :before ((cp cnts-coll-panel))

  (when (sl:on (leaf-btn cp)) (setf (sl:on (leaf-btn cp)) nil))
  (sl:destroy (leaf-btn cp))
  (sl:destroy (chart-btn cp)))

;;;---------------------------------------------

(defclass combination-coll-panel (collimator-panel)

  ((sx-sup :accessor sx-sup
	   :documentation "The slider for the x-sup jaw setting")

   (sx-inf :accessor sx-inf
	   :documentation "The slider for the x-inf jaw setting")

   (sy :accessor sy
       :documentation "The slider for the y jaw setting")

   )

  )

;;;---------------------------------------------

(defmethod make-collimator-panel ((coll combination-coll)
				  &rest initargs) 

  (apply #'make-instance 'combination-coll-panel
	 :coll-for coll :allow-other-keys t initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp combination-coll-panel)
				       &rest initargs)

  (let* ((sw 260) ;; magic numbers from beam panel
	 (sh 30)
	 (fr (panel-frame cp))
	 (win (sl:window fr))
	 (font (sl:font fr)) ;; use font provided or defaulted
	 (coll (coll-for cp))
	 (xsup-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
			 :label "COLL X SUP: " :parent win
			 :ulc-x 0 :ulc-y 0
			 :setting (x-sup coll) :font font
			 initargs))
	 (xinf-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
			 :label "COLL X INF: " :parent win
			 :ulc-x 0 :ulc-y 70
			 :setting (x-inf coll) :font font
			 initargs))
	 (y-sl (apply #'sl:make-sliderbox sw sh 0.0 45.0 45.0
		      :label "COLL Y: " :parent win
		      :ulc-x 0 :ulc-y 140
		      :setting (y coll) :font font
		      initargs)))
    ;; install them and connect them up to the collimator settings
    (setf (sx-sup cp) xsup-sl
	  (sx-inf cp) xinf-sl
          (sy cp) y-sl)
    (ev:add-notify cp (sl:value-changed xsup-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (x-sup (coll-for cp))
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-x-sup coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sx-sup pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:value-changed xinf-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (x-inf (coll-for cp))
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-x-inf coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sx-inf pan)) val)
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:value-changed y-sl)
		   #'(lambda (pan sl val)
		       (declare (ignore sl))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (y (coll-for cp))
			   (coerce val 'single-float))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (new-coll-y coll)
		   #'(lambda (pan c val)
		       (declare (ignore c))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:setting (sy pan)) val)
			 (setf (busy pan) nil))))))

;;;---------------------------------------------

(defmethod destroy :before ((cp combination-coll-panel))

  (sl:destroy (sx-sup cp))
  (sl:destroy (sx-inf cp))
  (sl:destroy (sy cp))
  (ev:remove-notify cp (new-coll-x-sup (coll-for cp)))
  (ev:remove-notify cp (new-coll-x-inf (coll-for cp)))
  (ev:remove-notify cp (new-coll-y (coll-for cp))))

;;;---------------------------------------------

(defclass portal-coll-panel (collimator-panel)

  ((beam-of :initarg :beam-of
	    :accessor beam-of
	    :documentation "The beam containing the collimator.")
   
   (plan-of :initarg :plan-of
	    :accessor plan-of
	    :documentation "The plan containing the beam.")
   
   (patient-of :initarg :patient-of
	       :accessor patient-of
	       :documentation "The current patient.")
   
   (sfd-box :accessor sfd-box
	    :documentation "The textline for the source-to-film
distance, when using the digitizer for input.")

   (filmdist :type single-float
	     :accessor filmdist
	     :initarg :filmdist
	     :documentation "The source to film distance when using
simulator or port films on the digitizer.")

   )

  (:default-initargs :filmdist 100.0)

  )

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp portal-coll-panel)
				       &rest initargs)

  (setf (sfd-box cp) (apply #'sl:make-textline 120 25
			    :label "SFD: "
			    :parent (sl:window (panel-frame cp))
			    :ulc-x (floor (- *coll-pan-width* 120) 2)
			    :ulc-y 50 ;; arbitrary - lots of room
			    :font (sl:font (panel-frame cp))
			    :numeric t :lower-limit 10.0 :upper-limit 200.0
			    initargs))
  ;; initial values here, but register action in child classes
  (setf (filmdist cp) (isodist (beam-of cp)))
  (setf (sl:info (sfd-box cp)) (filmdist cp)))

;;;---------------------------------------------

(defmethod destroy :before ((mp portal-coll-panel))

  (sl:destroy (sfd-box mp)))

;;;---------------------------------------------

(defclass multileaf-coll-panel (portal-coll-panel)

  ((leaf-button :accessor leaf-button
		:documentation "The button that brings up the mlc
contour and leaf editing panel.")

   (leaf-panel :accessor leaf-panel
	       :initform nil
               :documentation "The mlc panel for this collimator panel.")

   (chart-button :accessor chart-button
                 :documentation "The leaf chart button for this collim pnl.")

   )

  )

;;;---------------------------------------------

(defmethod make-collimator-panel ((coll multileaf-coll) &rest initargs)

  (apply #'make-instance 'multileaf-coll-panel
	 :coll-for coll :allow-other-keys t initargs))

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp multileaf-coll-panel)
				       &rest initargs)

  (let* ((btw 120) ;; magic numbers for button size etc.
	 (bth 25)
	 (ulc-x (floor (- *coll-pan-width* btw) 2))
	 (fr (panel-frame cp))
	 (win (sl:window fr))
	 (font (sl:font fr)) ;; use font provided or defaulted
	 (leaf-b (apply #'sl:make-button btw bth
			:label "LEAF/PORTAL EDIT" :parent win
			:ulc-x ulc-x
			:ulc-y 120 ;; below portal SFD button
			:font font
			initargs))
         (chart-b (apply #'sl:make-button btw bth
                         :label "LEAF CHART" :parent win
                         :ulc-x ulc-x :ulc-y 170
                         :font font
                         initargs)))
    ;; install and connect up to the collimator settings
    (setf (leaf-button cp) leaf-b
          (chart-button cp) chart-b)
    (ev:add-notify cp (sl:button-on leaf-b)
		   #'(lambda (pan bt)
                       (declare (ignore bt))
		       (setf (leaf-panel pan) 
			 (make-mlc-panel :beam-of (beam-of pan)
					 :plan-of (plan-of pan)
					 :patient-of (patient-of pan)
					 :filmdist (filmdist pan)))
		       (ev:add-notify pan (deleted (leaf-panel pan))
				      #'(lambda (pn lp)
					  (declare (ignore lp))
					  (setf (leaf-panel pn) nil)
					  (unless (busy pn)
					    (setf (busy pn) t)
					    (setf (sl:on leaf-b) nil)
					    (setf (busy pn) nil))))))
    (ev:add-notify cp (sl:button-off leaf-b)
                   #'(lambda (pan bt)
                       (declare (ignore bt))
                       (unless (busy pan)
                         (setf (busy pan) t)
                         (destroy (leaf-panel pan))
                         (setf (busy pan) nil))))
    (ev:add-notify cp (sl:new-info (sfd-box cp))
		   #'(lambda (pan tl info)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (let ((fd (coerce (read-from-string info)
					   'single-float)))
			   (setf (filmdist pan) fd)
			   (setf (sl:info tl) (format nil "~5,1F" fd))
			   (when (leaf-panel pan)
			     (setf (filmdist (leaf-panel pan)) fd)))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:button-on chart-b)
                   #'(lambda (pan bt)
                       (declare (ignore bt))
		       (chart-panel 'leaf (patient-of pan)
				    (plan-of pan) (beam-of pan))
                       (setf (sl:on chart-b) nil)))))

;;;---------------------------------------------

(defmethod destroy :before ((mp multileaf-coll-panel))

  (let ((lb (leaf-button mp)))
    (when (sl:on lb) (setf (sl:on lb) nil)))
  (sl:destroy (leaf-button mp))
  (sl:destroy (chart-button mp)))

;;;---------------------------------------------

(defclass electron-coll-panel (portal-coll-panel)

  ((energy-button :accessor energy-button
		  :documentation "The button that provides a menu to
select the energy of the electron beam.")

   (cone-size-button :accessor cone-size-button
		     :documentation "The button that provides a menu
to select the cone size from the available ones.")

   (open-portal-button :accessor open-portal-button
		       :documentation "Pressing this button resets the
		       portal contour to match the cone opening.")

   (contour-button :accessor contour-button
		   :documentation "The button that brings up and
removes the contour editor panel for drawing the electron cutout
contour.")

   (contour-ed :accessor contour-ed
	       :initform nil
	       :documentation "A slot for the contour editor that
appear on the screen on demand for cutout editing.")

   (bev :accessor bev
	:initform nil
	:documentation "A beam's eye view that is not displayed but
used as the background for the cutout contour editor.")

   (image-mediator :accessor image-mediator
		   :initform nil
		   :documentation "A single image-view-mediator to
manage creation of DRR images as necessary for the view background.")

   (window-control :accessor window-control
		   :documentation "The textline that displays and sets
the window for the background view's image.")

   (level-control :accessor level-control
		  :documentation "The textline that displays and sets
the level for the background view's image.")

   (image-button :accessor image-button
		 :documentation "The button that toggles display of
image data in this view.")

   (fg-button :accessor fg-button
	      :documentation "Brings up a popup menu of objects in the
view to display them or not on a view by view basis.")

   (viewlist-panel :accessor viewlist-panel
		   :initform nil
		   :documentation "Temporarily holds the list of
objects visible on the screen.")

   )

  )

;;;---------------------------------------------

(defmethod make-collimator-panel ((coll electron-coll) &rest initargs)

  (apply #'make-instance 'electron-coll-panel
	 :coll-for coll :allow-other-keys t initargs))

;;;---------------------------------------------

(defun refresh-portal-editor (pan)

  "Draws the background - everything but the portal being edited, and
adds the electron cone square to the planar editor background.
This is done by the electron beam draw method, but this beam is not
drawn in the portal editor background view by bev-draw-all."

  (let* ((bm (beam-of pan))
	 (coll (coll-for pan))
	 (side (* 0.5 (cone-size coll)))
	 (bev (bev pan))
	 (color (sl:color-gc (display-color bm)))
	 (prim (find coll (foreground bev) :key #'object))
	 (pts (pixel-contour (poly:rotate-vertices
			      (counter-clockwise-rectangle
			       (- side) (- side) side side)
			      (collimator-angle bm))
			     (scale bev)
			     (x-origin bev)
			     (y-origin bev))))
    (bev-draw-all bev (plan-of pan) (patient-of pan) bm)
    (unless prim
      (setq prim (make-lines-prim nil color :object coll))
      (push prim (foreground bev)))
    (setf (color prim) color ;; note - points is a list of lists
	  (points prim) (list (nconc pts (list (first pts)
					       (second pts)))))
    (display-view bev)) ;; redraw the primitives into the pixmap
  (display-planar-editor (contour-ed pan)))

;;;---------------------------------------------

(defmethod initialize-instance :after ((cp electron-coll-panel)
				       &rest initargs)

  (let* ((btw 120) ;; magic numbers for button size etc.
	 (bth 25)
	 (ulc-x 10) ;; was (floor (- *coll-pan-width* btw) 2))
	 (mid-x (floor *coll-pan-width* 2))
	 (size large) ;; size of drawing area
	 (fr (panel-frame cp))
	 (win (sl:window fr))
	 (font (sl:font fr)) ;; use font provided or defaulted
	 (coll (coll-for cp))
	 (energy-b (apply #'sl:make-button btw bth
			  :label (format nil "ENERGY: ~4,1F"
					 (energy coll))
			  :parent win :font font
			  :ulc-x ulc-x :ulc-y 100 ;; below portal SFD button
			  initargs))
	 (cone-b (apply #'sl:make-button btw bth
			:label (format nil "CONE: ~4,1F"
				       (cone-size coll))
			:parent win :font font
			:ulc-x ulc-x :ulc-y 135 ;; below energy button
			initargs))
	 (open-b (apply #'sl:make-button btw bth
			:label "OPEN PORTAL"
			:parent win :font font
			:ulc-x ulc-x :ulc-y 175 ;; below cone size button
			initargs))
	 (cont-b (apply #'sl:make-button btw bth
			:label "CUTOUT CONTOUR" :parent win :font font
			:ulc-x ulc-x :ulc-y 235 ;; below open portal button
			initargs))
	 (bev (make-view size size 'beams-eye-view
			 :beam-for (beam-of cp)
			 :display-func
			 #'(lambda (vw)
			     (setf (image-cache vw) nil)
			     (draw (image (image-mediator cp)) vw)
			     (display-view vw)
			     (when (contour-ed cp)
			       (display-planar-editor (contour-ed cp)))))))
    ;; install and connect up to the collimator settings
    (setf (energy-button cp) energy-b
	  (open-portal-button cp) open-b
	  (contour-button cp) cont-b
	  (bev cp) bev)
    (setf (fg-button cp) (apply #'sl:make-button btw bth
				:font font :label "Objects" :parent win
				:ulc-x mid-x :ulc-y 100
				initargs))
    (setf (image-button cp) (apply #'sl:make-button btw bth
				   :font font :label "Image" :parent win
				   :ulc-x mid-x :ulc-y 135
				   initargs))
    (setf (window-control cp)
      (apply #'sl:make-sliderbox btw bth 1.0 2047.0 9999.0
	     :parent win :font font :label "Win: "
	     :ulc-x (- mid-x 5) :ulc-y 170
	     :border-width 0 :display-limits nil initargs))
    (setf (level-control cp)
      (apply #'sl:make-sliderbox btw bth 1.0 4095.0 9999.0
	     :parent win :font font :label "Lev: "
	     :ulc-x (- mid-x 5) :ulc-y 230
	     :border-width 0 :display-limits nil initargs))
    (setf (sl:setting (window-control cp)) (coerce (window bev) 'single-float))
    (setf (sl:setting (level-control cp)) (coerce (level bev) 'single-float))
    (ev:add-notify cp (sl:button-on energy-b)
		   #'(lambda (pan bt)
		       (let* ((energies (energies (collimator-info
						   (machine (beam-of pan)))))
			      (e-num (sl:popup-menu
				      (mapcar #'write-to-string energies))))
			 (when e-num
			   (setf (energy (coll-for pan))
			     (nth e-num energies))))
		       (setf (sl:on bt) nil)))
    (ev:add-notify cp (new-energy coll)
		   #'(lambda (pan col new-en)
		       (declare (ignore col))
		       (setf (sl:label (energy-button pan))
			 (format nil "ENERGY: ~4,1F" new-en))))
    (setf (cone-size-button cp) cone-b)
    (ev:add-notify cp (sl:button-on cone-b)
		   #'(lambda (pan bt)
		       (let* ((cones (cone-sizes (collimator-info
						  (machine (beam-of pan)))))
			      (size-no (sl:popup-menu
					(mapcar #'write-to-string cones))))
			 (when size-no
			   (setf (cone-size (coll-for pan))
			     (nth size-no cones))))
		       (setf (sl:on bt) nil)))
    (ev:add-notify cp (sl:button-on open-b)
		   #'(lambda (pan bt)
		       (let* ((coll (coll-for pan))
			      (size (* 0.5 (cone-size coll))))
			 (setf (vertices coll)
			   (counter-clockwise-rectangle (- size) (- size)
							size size))
			 (when (contour-ed pan)
			   (setf (vertices (contour-ed pan))
			     (poly:rotate-vertices
			      (vertices coll)
			      (collimator-angle (beam-of pan))))
			   (refresh-portal-editor pan)))
		       (setf (sl:on bt) nil)))
    (ev:add-notify cp (new-cone-size coll)
		   #'(lambda (pan col new-size)
		       (declare (ignore col))
		       (setf (sl:label (cone-size-button pan))
			 (format nil "CONE: ~4,1F" new-size))
		       (when (contour-ed pan)
			 (refresh-portal-editor pan))))
    (ev:add-notify cp (sl:new-info (sfd-box cp))
		   #'(lambda (pan tl info)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (let ((fd (coerce (read-from-string info)
					   'single-float)))
			   (setf (filmdist pan) fd)
			   (setf (sl:info tl) (format nil "~5,1F" fd))
			   (when (contour-ed pan)
			     (setf (digitizer-mag (contour-ed pan))
			       (/ fd (isodist (beam-of pan))))))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:button-on (image-button cp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (bev pan)) t)
			 (if (contour-ed pan) (refresh-portal-editor pan))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:button-off (image-button cp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (background-displayed (bev pan)) nil)
			 (if (contour-ed pan) (refresh-portal-editor pan))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:button-2-on (image-button cp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (case (drr-state (bev pan))
			   ;;'stopped is a noop
			   ('running (setf (drr-state (bev pan)) 'paused))
			   ('paused (setf (drr-state (bev pan)) 'running)
				    (drr-bg (bev pan))))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:button-on (fg-button cp))
		   #'(lambda (pan bt)
		       (if (contour-ed pan)
			   (progn
			     (setf (viewlist-panel pan)
			       (make-instance 'viewlist-panel
				 :refresh-fn #'(lambda (vw)
						 (display-view vw)
						 (display-planar-editor
						  (contour-ed pan)))
				 :view (bev pan)))
			     (ev:add-notify pan (deleted (viewlist-panel pan))
					    #'(lambda (pnl vlpnl)
						(declare (ignore vlpnl))
						(setf (viewlist-panel pnl)
						  nil)
						(when (not (busy pnl))
						  (setf (busy pnl) t)
						  (setf (sl:on bt) nil)
						  (setf (busy pnl) nil)))))
			 (progn
			   (setf (busy pan) t)
			   (setf (sl:on bt) nil)
			   (setf (busy pan) nil)))))
    (ev:add-notify cp (sl:button-off (fg-button cp))
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (destroy (viewlist-panel pan))
			 (setf (busy pan) nil))))
    (if (image-set (patient-of cp))
	(setf (image-mediator cp)
	  (make-image-view-mediator (image-set (patient-of cp)) bev)))
    (setf (image-button bev) (image-button cp))
    (setf (drr-state bev) (drr-state bev)) ;; to init the button
    (ev:add-notify cp (bg-toggled bev)
		   #'(lambda (pan vw)
		       (when (not (busy pan))
			 (setf (busy pan) t)
			 (setf (sl:on (image-button pan))
			   (background-displayed vw))
			 (setf (busy pan) nil))))
    (ev:add-notify cp (sl:button-on cont-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (let* ((coll (coll-for pan))
			      (bm (beam-of pan))
			      (bev (bev pan))
			      (ce (make-planar-editor
				   :vertices (vertices coll)
				   :background (sl:pixmap (picture bev))
				   :x-origin (/ size 2)
				   :y-origin (/ size 2)
				   :scale (scale bev)
                                   :title (format nil
						  "Beam Portal for ~A"
						  (name bm))
				   :digitizer-mag (/ (filmdist pan)
						     (isodist bm))
				   :color (sl:color-gc
					   (display-color bm)))))
			 (setf (contour-ed pan) ce)
			 (refresh-portal-editor pan)
			 (ev:add-notify pan (new-coll-set coll)
					#'(lambda (pnl col)
					    (unless (busy pnl)
					      (setf (busy pnl) t)
					      (setf (vertices
						     (contour-ed pnl))
						(poly:rotate-vertices
						 (vertices col)
						 (collimator-angle bm)))
					      (setf (busy pnl) nil))))
			 (ev:add-notify pan (new-color (beam-of cp))
					#'(lambda (pnl bm newcolor)
					    (declare (ignore bm))
					    (setf (color (contour-ed pnl))
					      (sl:color-gc newcolor))
					    (refresh-portal-editor pnl)))
			 (ev:add-notify pan (new-vertices ce)
					#'(lambda (pnl ced new-verts)
					    (declare (ignore ced))
					    (unless (busy pnl)
					      (setf (busy pnl) t)
					      (setf (vertices
						     (coll-for pnl))
						(poly:rotate-vertices
						 new-verts
						 (- (collimator-angle
						     (beam-of pnl)))))
					      (setf (busy pnl) nil))))
			 (ev:add-notify pan (new-coll-angle (beam-of cp))
					#'(lambda (pnl bm new-ang)
					    (declare (ignore bm new-ang))
					    ;; (setf (vertices
					    ;;    (contour-ed pnl))
					    ;;   (poly:rotate-vertices
					    ;;    (vertices (contour-ed pnl))
					    ;;    (- new-ang old-ang)))
					    (refresh-portal-editor pnl)))
			 (ev:add-notify pan (new-scale ce)
					#'(lambda (pnl ced new-sc)
					    (declare (ignore ced))
					    (let ((bev (bev pnl)))
					      (setf (scale bev) new-sc)
					      (refresh-portal-editor pnl))))
			 (ev:add-notify pan (new-origin ce)
					#'(lambda (pnl ced new-org)
					    (declare (ignore ced))
					    (let ((bev (bev pnl)))
					      (setf (origin bev) new-org)
					      (refresh-portal-editor pnl))))

			 (ev:add-notify pan (sl:value-changed
					     (window-control pan))
					#'(lambda (pnl wc win)
					    (declare (ignore wc))
					    (setf (window (bev pnl))
					      (round win))
					    (if (background-displayed
						 (bev pnl))
						(display-planar-editor
						 (contour-ed pnl)))))
			 (ev:add-notify pan (sl:value-changed
					     (level-control pan))
					#'(lambda (pnl lc lev)
					    (declare (ignore lc))
					    (setf (level (bev pnl))
					      (round lev))
					    (if (background-displayed
						 (bev pnl))
						(display-planar-editor
						 (contour-ed pnl)))))
			 )))
    (ev:add-notify cp (sl:button-off cont-b)
		   #'(lambda (pan bt)
		       (declare (ignore bt))
		       (ev:remove-notify pan (sl:value-changed
					      (window-control pan)))
		       (ev:remove-notify pan (sl:value-changed
					      (level-control pan)))
		       (ev:remove-notify pan (new-coll-set (coll-for pan)))
		       (ev:remove-notify pan (new-color (beam-of pan)))
		       (ev:remove-notify pan (new-coll-angle (beam-of pan)))
		       (destroy (contour-ed pan))
		       (setf (contour-ed pan) nil)
		       (when (viewlist-panel pan)
			 (destroy (viewlist-panel pan)))))))

;;;---------------------------------------------

(defmethod destroy :before ((cp electron-coll-panel))

  (let ((vw (bev cp)))
    (when vw
      ;; ensure that there are not any lingering 
      ;;   background jobs for this view-panel
      (remove-bg-drr vw)
      (when (eq 'running (drr-state vw))
	(setf (drr-state vw) 'paused))
      (setf (image-button vw) nil)))
  (let ((cb (contour-button cp)))
    (when (sl:on cb) (setf (sl:on cb) nil)))
  (if (image-mediator cp) (destroy (image-mediator cp)))
  (if (bev cp) (destroy (bev cp)))
  (sl:destroy (contour-button cp))
  (sl:destroy (image-button cp))
  (sl:destroy (window-control cp))
  (sl:destroy (level-control cp))
  (if (sl:on (fg-button cp)) (setf (sl:on (fg-button cp)) nil))
  (sl:destroy (fg-button cp))
  (ev:remove-notify cp (new-energy (coll-for cp)))
  (sl:destroy (energy-button cp))
  (ev:remove-notify cp (new-cone-size (coll-for cp)))
  (sl:destroy (cone-size-button cp)))

;;;---------------------------------------------
;;; End.
