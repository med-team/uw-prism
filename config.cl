;;;
;;; config
;;;
;;; This file contains some environment setup needed by the SLIK
;;; toolkit.
;;;
;;; The defsystem file is required.  It is assumed that all the files
;;; are in the current working directory when this file is loaded.
;;;
;;;  9-Jun-1992 I. Kalet created
;;; 24-Jun-1992 I. Kalet put defpackage etc. for events and
;;; collections here.
;;; 20-Oct-1992 I. Kalet cosmetic fixes - delete making PCL a nickname
;;; for COMMON-LISP - not needed
;;; 02-Mar-1993 J. Unger add some cmu read-time conditionals.
;;; 27-Jul-1993 I. Kalet add some lucid read-time conditionals.
;;;  7-Jan-1995 I. Kalet move defpackage etc. for events and
;;;  collections into those files so they are standalone, and they
;;;  will then be modules in SLIK rather than systems of their own.
;;;  Also remove support for VAXlisp, Lucid.
;;;  7-Aug-1995 I. Kalet take out *dont-redefine-require* as it is
;;;  internal to defsystem.
;;; 10-Aug-2003 I. Kalet add location of central repository and
;;; location of defsystem.cl - files are no longer all in the user's
;;; default directory.
;;; 30-Nov-2003 I. Kalet make default location of defsystem and
;;; systemdefs be the user's default directory, so that systemdefs can
;;; be managed by CVS also.
;;; 21-Jun-2004 I. Kalet move CLX nickname form to SLIK - it is only
;;; used there and in systems that depend on SLIK.
;;;

;;;----------------------------------------------------------
;;; here is the compiler setting for the whole works - edit it for
;;; different compiler runs.  Since this file is loaded but not
;;; compiled it is ok to have this at top-level.

(proclaim '(optimize (speed 3) (safety 1) (space 0) (debug 0)))

;;;----------------------------------------------------------
;;; We use Mark Kantrowitz's defsystem facility.  Set
;;; defsystem-specific global variables here to avoid having to 
;;; answer questions about recompilation during a load-system.
;;;----------------------------------------------------------

;; change if defsystem is in a different place in your environment
(load "defsystem")

(setq mk::*load-source-if-no-binary* t)  ;; for load-system
(setq mk::*compile-during-load* nil)
(setq mk::*minimal-load* t) ;; so don't reload if not necessary

;; change if system definitions are in a different place in your environment
(setq mk::*central-registry* "systemdefs/")

;;; function to collect filenames for systems

(defun files (syslist)
  (apply #'append
	 (mapcar #'(lambda (system)
		     (mk:files-in-system system :all :binary))
		 syslist)))

;;;----------------------------------------------------------
;;; End.
