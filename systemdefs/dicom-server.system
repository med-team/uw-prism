;;;
;;; dicom-server.system
;;;
;;; Prism DICOM Server and RTPlan Client System Declarations.
;;;
;;; Contains declarations used by Server only.
;;;
;;; 06-Oct-2001 BobGian eliminate temporary directory variables.
;;; 15-Oct-2001 BobGian remove *FILE-MOVE-LIST*.
;;; 31-Dec-2001 BobGian begin modularizing PDS to use DEFSYSTEM and to
;;;   incorporate client as normal part of Prism (not loaded separately).
;;;   Client configuration now comes from "/radonc/prism/prism.config".
;;;   Server configuration still comes from "/radonc/prism/pds.config".
;;; 11-Feb-2002 BobGian move *Implementation-Version-Name* and
;;;   *Implementation-Class-UID* here as non-configurable parameters
;;;   but different values for Client versus Server.
;;; 30-Jul-2002 BobGian fix error in Implementation-Class-UID for server:
;;;   "1.2.840.113944.100.10.1.1" -> "1.2.840.113994.100.10.1.1"
;;; Jul/Aug 2002 BobGian:
;;;   DEFSYSTEM: filename change: "prism-images" -> "prism-data".
;;;   More mnemonic names for global vars naming directories:
;;;     *IMAGE-DATABASE* -> *MATCHED-PAT-IMAGE-DATABASE*
;;;     *UNMATCHED-NAME-DATABASE* -> *UNMATCHED-PAT-IMAGE-DATABASE*
;;;   Ditto for special vars used internally (*CACHED-xxxx, etc).
;;;     Special vars bound on main function entry and not at top-level.
;;;   New global var naming Structure-Set directory: *STRUCTURE-DATABASE*.
;;;   Each item (sublist) on *LOCAL-ENTITIES* and *REMOTE-ENTITIES* lists can
;;;     now contain one more optional element: directory for Structure-Sets.
;;; 08-May-2003 BobGian: Add a few more color names as SLIK exports.
;;; 30-Jul-2003 I. Kalet fix up for new cvs code management
;;; 21-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 21-Dec-2003 BobGian: Add variable *IGNORABLE-GROUPS-LIST* to specify
;;;   slots that PARSE-OBJECT should log but otherwise ignore.
;;; 24-Dec-2003 BobGian: Added var *REPORTABLE-VARIABLES* to hold list of
;;;   configurable variables whose values are logged at server startup.
;;; 27-Apr-2004 BobGian: Variable split in declaration - *STORED-IMAGE-COUNT*
;;;    -> *STORED-IMAGE-COUNT-PER-SET* [per-image-set count]
;;;    -> *STORED-IMAGE-COUNT-CUMULATIVE* [cumulative count over association].
;;; 15-Jul-2004 BobGian: Remove "DI" nickname from DICOM package declaration.
;;;  1-Dec-2008 I. Kalet change version number, and temporarily
;;; redefine pds.config location.
;;; 20-Jun-2009 I. kalet move package defs and other globals to
;;; wrapper-server to make independent of defsystem
;;;

;;;=============================================================
;;; Dicom SERVER system definition.

(mk:defsystem :dicom-server
  :depends-on (:dicom-common) ;; and slik too, sort of...
  :source-pathname "dicom/src/"
  :binary-pathname "dicom/bin/"
  :components
  (("wrapper-server")
   ("actions-server")
   ("prism-data")
   ("prism-output"  :depends-on ("prism-data"))
   ("object-parser" :depends-on ("prism-output"))
   ))

;;;=============================================================
;;; End.
