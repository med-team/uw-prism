;;;
;;; prism-system
;;;
;;; System definition file for the PRISM radiotherapy planning system
;;; Assumes CMU defsystem loaded, and other environmental factors set.
;;;
;;;  4-Jun-1992 I. Kalet started
;;; 25-Jun-1992 I. Kalet modify for CMU defsystem
;;; 17-Jul-1992 I. Kalet add contour editor, filmstrip and volume
;;; editor modules
;;; 31-Jul-1992 I. Kalet add misc and plans modules
;;;  9-Aug-1992 I. Kalet add geometry to :depends-on systems and put
;;;  exports in defpackage form.  Added patients module
;;; 16-Oct-1992 I. Kalet contour-editor depends on contours now, and
;;; add new module, object-manager
;;; 13-Nov-1992 I. Kalet medical-images now depends on views,
;;; rearrange other dependencies (and some history removed...)
;;; 24-Nov-1992 I. Kalet add image-manager module
;;;  5-Mar-1993 I. Kalet add patient-panels, change volume-editor to
;;;  easel, add definitions for global database variables.
;;; 24-Mar-1993 J. Unger add cmucl read time conditionals, add missing
;;;  dependencies to beam file in defsystem definition, undo cycle in
;;;  defsystem dependency list.  Also expanded the ~ on the two database
;;;  definitions.
;;; 01-Nov-1993 J. Unger add *sum-dose-option* to list of global
;;; variables (lots of others added previously)
;;; 16-Nov-1993 I. Kalet add genera read time conditionals
;;; 11-Feb-1994 I. Kalet add tools-panel for RTPT tools and also
;;;  modules for autoplan.  Add everything in Implementation report to
;;;  export list (and some history removed...)
;;; 11-Feb-1994 J. Unger add plots definition and dependencies
;;; 21-Feb-1994 D. Nguyen add copy functions, others to export list.
;;; 02-Mar-1994 J. Unger add charts definition and dependencies.
;;; 07-Mar-1994 D. Nguyen add get-transverse-beam-transform and
;;;  project-portal to export list.
;;; 15-Mar-1994 J. Unger add digitizer, fix bug in *dosecomp-command*
;;;  1-Apr-1994 I. Kalet delete dependency of filmstrip on
;;;  medical-images, delete some history above, add bev-graphics
;;; 15-Apr-1994 I. Kalet add dependency of view-panels on locators
;;; 25-Apr-1994 J. Unger add dependencies for point display modules, others
;;;  6-May-1994 J. Unger require polygons & ruler pkgs, move ptvt
;;; defpackage here, split valid to valid-grid & valid-points on
;;; exports list, add misc dependency to linear-expand.
;;; 16-May-1994 I. Kalet move globals to prism-globals, add prism,
;;; prism-globals modules and dependencies, add beam-blocks.
;;; 20-May-1994 J. Unger add dependencies for point editors.
;;; 26-May-1994 I. Kalet revise editor dependencies, add planar-editor
;;; 10-Jun-1994 I. Kalet revise easel, 3d editor, add volume editor,
;;; fix up dependencies, modify beam-blocks and other dependencies.
;;; 20-Jun-1994 J. Unger modify dependencies for contour-editor.
;;; 21-Jun-1994 I. Kalet add write-neutron module.
;;; 23-Jun-1994 J. Jacky charts, write-neutron depend on therapy-machines
;;; 29-Jun-1994 I. Kalet add wedges, dependencies.
;;; 11-Jul-1994 J. Unger add prism-top-level, dbmgr-top-level, and
;;; unpack-top-level to exports list, add tape-measure dependencies.
;;; 13-Jul-1994 D. Nguyen add autoplan exports.
;;; 20-Jul-1994 J. Unger add neutron panel stuff.
;;; 22-Jul-1994 J. Jacky add mlc
;;; 28-Jul-1994 J. Unger add deps for leaf-panel & leaf-editor.
;;;  5-Aug-1994 J. Unger add defs for block-editor, exports for cnts-coll.
;;; 11-Oct-1994 J. Unger modify exports for plots, fix prism-db dependency.
;;; 26-Jan-1995 I. Kalet clean up all dependencies and eliminate
;;; allegro pre-v4, vaxlisp and lucid support.  Take out dependency on
;;; events and collections systems - they are merged with slik.  Also,
;;; geometry is now a module in prism, not a system.
;;; 12-Mar-1995 I. Kalet add patient-plan-manager and fix up patient,
;;;  plan and other dependencies.
;;; 30-Apr-1995 I. Kalet remove block-editor, not needed anymore.
;;;  4-Sep-1995 I. Kalet add support for Harlequin Lispworks, fix
;;;  dependencies on misc, update dependencies on contour-graphics.
;;;  5-Jan-1996 I. Kalet add collim-info, split off from
;;;  therapy-machines, add dose-info, transfer-info and some exports.
;;;  1-Feb-1996 I. Kalet drop make-package, just assume defpackage.
;;;  4-Jun-1996 I. Kalet add new modules brachy-mediators,
;;;  brachy-graphics, brachy-panels.
;;;  8-Oct-1996 I. Kalet change contour-graphics to pixel-graphics,
;;; update dependencies, as draw code for contours is now merged into
;;; volume graphics.  Add beam-transforms, beam-block-graphics,
;;; wedge-graphics, separated from beam-graphics.  Update other
;;; dependencies as well for other moved code.
;;;  2-Jan-1997 I. Kalet add beam-dose and brachy-dose separate files
;;;  in anticipation of rewrite of beam dose module in Lisp.
;;; 21-Jan-1997 I. Kalet add pathlength module, delete geometry.
;;;  7-Mar-1997 I. Kalet add brachy-tables analog to therapy-machines
;;; 18-Apr-1997 I. Kalet drop support for CMU/PCL, assume native CLOS
;;; 22-May-1997 I. Kalet drop dose panel, now absorbed into plan
;;;  panel, consolidate some history above, add newly discovered
;;;  dependencies discovered by Bob G., resolve circularities between
;;;  bev-graphics, beam-block-graphics, also  tape-measure,
;;;  planar-editor, and plans, beam-mediators.  Add replace-coll.
;;; 25-Jun-1997 I. Kalet add dep. of plan-panels on brachy-panels and
;;;  beam-panels, export collimator-type, update filmstrip dep., add
;;;  dose-surface-panels for revised plan panel, delete
;;;  patient-plan-mediators, revise deps.
;;; 26-Jun-1997 BobGian remove export of OUTCODE (internal macro).
;;; 28-Jun-1997 I. Kalet add patdb-panels and irreg modules.
;;; 03-Jul-1997 BobGian removed all NEARLY-xxx functions - now living
;;;  in POLYGONS system.
;;;  1-Sep-1997 I. Kalet add irreg and irreg-panels, add brachy deps.,
;;;  add new dep. of prism-db on irreg, add spreadsheet, remove TPR and
;;;  OUTPUT-FACTOR from the exports list.
;;; 16-Sep-1997 I. Kalet refine dependencies.
;;;  3-Oct-1997 BobGian remove AVERAGE, LO-HI-COMPARE - now expanded
;;;  inline.
;;; 24-Oct-1997 I. Kalet plans does not depend on collimators or wedges.
;;; 27-Oct-1997 BobGian brachy-dose depends on misc because of SQR.
;;; 26-Dec-1997 I. Kalet take out spreadsheets -- moved it to SLIK
;;; 19-Jan-1998 I. Kalet revise dependencies after filmstrip overhaul,
;;; don't export get-transverse-beam-transform.
;;; 22-Jan-1998 BobGian add new file clipper containing polygon clipping
;;;  code formerly in pathlength.  beam-dose depends on clipper.
;;; 13-Mar-1998 BobGian add 2 new files: table-lookups and output-factors.
;;;  Slight reordering of dependencies between therapy-machines, dose-info,
;;;  beam-dose, and the new files.
;;; 30-Apr-1998 I. Kalet add irreg-point-panels, split off from
;;; irreg-panels, and irreg-dose, split from irreg.  Add postscript
;;; module.  Add irreg chart to charts, adjust dependencies.
;;; 22-May-1988 BobGian create new file "dosecomp-decls" to hold
;;;  dose-computation-wide DEFCONSTANTs and DEFMACROs.
;;; 25-May-1998 I. Kalet fix more dependencies, e.g., plots,
;;; irreg-dose
;;; 19-Jun-1998 I. Kalet add drr.
;;; 03-Nov-1998 C. Wilcox add scan, spots, & dvh-panel.
;;;  added dependency of patient-panel on dvh-panel.
;;; 22-Dec-1998 I. Kalet add electron-dose, stub at first, later
;;; Paul's code.
;;; 26-Jan-1999 I. Kalet ruler and dnet are now modules, not a subsystem.
;;; 25-Mar-1999 I. Kalet add quadtree for electron beam dose calc.
;;; 24-Jun-1999 J. Zeman move postscript package to slik-system
;;;  5-Sep-1999 I. Kalet revise dependencies for new mlc-panels and
;;; related stuff.
;;; 25-Oct-1999 I. Kalet remove dependencies in tools-panel, remove
;;; autoplan module and package.
;;; 16-Jan-2000 I. Kalet added brachy tables and other dependencies.
;;; 03-Feb-2000 BobGian update dependencies for electron dosecalc files.
;;; 28-Feb-2000 I. Kalet add another brachy module,
;;; brachy-specs-panels, add some more brachy dependencies.
;;;  5-Mar-2000 I. Kalet add dependencies for finally adding the tape
;;; measure to the views, also split off another brachy module.
;;; 17-Apr-2000 I. Kalet ...and yet another brachy module, seed-spreadsheet.
;;; 29-Jun-2000 I. Kalet export add-tool and add dependency for tools-panel
;;; 30-Jul-2000 I. Kalet split medical-images with draw stuff in
;;; separate module, image-graphics.
;;; 13-Aug-2000 I. Kalet add missing digitizer dependency.
;;; 26-Nov-2000 I. Kalet remove refs to SRS collim. and transfer-info,
;;; add dependency of beam-block-panels on attribute-editor.
;;; 11-Mar-2001 I. Kalet add dump-prism-image to exports list.
;;; 11-Jun-2001 BobGian remove type-specific arithmetic macros - not exported.
;;;  6-Jan-2002 I. Kalet add new dependency: beam-panels on misc
;;; 31-Jan-2002 I. Kalet add dicom modules for DICOM-RT support
;;; 28-Jul-2002 I. Kalet reorganize brachytherapy modules, replace
;;; ortho-film-entry with brachy-coord-panels
;;;  5-Aug-2002 J. Sager add room-view
;;; 23-Sep-2002 BobGian export PAT-POS (slot in IMAGE class, written by PDS).
;;;  6-Oct-2002 I. Kalet rename seed-spreadsheet to brachy-dose-panels.
;;; 12-Jun-2003 I. Kalet add import-structure-sets, remove import-anatomy.
;;; 23-Mar-2004 BobGian add dmp-panel in DICOM module.
;;; 21-Jun-2004 I. Kalet remove irreg modules, discontinued
;;; 21-Jun-2004 I. Kalet merge 2d-point-editor, contour-editor into
;;; planar-editor, merge 3d-point-editor, easel into volume-editor,
;;; add auto-extend-panels.
;;; 13-Sep-2005 I. Kalet remove ruler, replace with Graham inference,
;;; adjust dependencies in PTVT modules, remove PTVT package
;;; 25-Jun-2008 I. Kalet add INFERENCE defpackage here in order to add
;;; to PRISM use-package list - it is completed in inference.cl
;;; 25-May-2009 I. Kalet remove support for room-view.
;;; 20-Jun-2009 I. Kalet move defpackage to prism-globals to make
;;; independent of defsystem
;;;

;;;-------------------------------------
;;; PRISM defsystem.
;;;-------------------------------------

(mk:defsystem :prism
  :source-pathname "prism/src/"
  :binary-pathname "prism/bin/"
  :depends-on (:slik :polygons :dicom-client)
  :components
  (
   ;; Basic functions and global variables.
   ("prism-globals")
   ("misc")
   ;; Declaration for constants used in dosecomp functions.
   ("dosecomp-decls")

   ;; Basic objects of radiotherapy, except plans and patients.
   ("prism-objects")
   ("contours")
   ("volumes" :depends-on ("prism-objects" "contours"))
   ("points" :depends-on ("prism-objects"))
   ("medical-images" :depends-on ("prism-globals" "misc"))
   ("collimators" :depends-on ("prism-objects" "contours"))
   ("replace-coll" :depends-on ("collimators"))
   ("collim-info")
   ("table-lookups")
   ("dose-info" :depends-on ("table-lookups"))
   ("file-functions")
   ("therapy-machines" :depends-on ("file-functions" "dosecomp-decls"
				    "dose-info" "table-lookups"))
   ("dose-grids" :depends-on ("prism-objects" "prism-globals"))
   ("dose-results" :depends-on ("prism-objects"))
   ("wedges" :depends-on ("prism-objects"))
   ("beam-blocks" :depends-on ("prism-objects" "contours"))
   ("beams" :depends-on ("prism-globals" "prism-objects"
			 "beam-blocks" "wedges" "collimators"
			 "replace-coll" "therapy-machines" "dose-results"))
   ("mlc" :depends-on ("collimators" "beams" "beam-blocks"))
   ("pixel-graphics" :depends-on ("misc"))
   ("tape-measure" :depends-on ("misc" "pixel-graphics")) ;; for compute-tics
   ("views" :depends-on ("prism-objects" "tape-measure"))
   ("beams-eye-views" :depends-on ("prism-globals" "views" "beams"))
   ("beam-transforms" :depends-on ("prism-globals"
				   "beams" "collimators"
				   "views" "beams-eye-views"))
   ("drr")
   ("brachy-tables" :depends-on ("prism-globals" "file-functions"))
   ("brachy" :depends-on ("prism-objects" "brachy-tables"))

   ;; Graphics - define methods for generic function "draw".
   ("view-graphics" :depends-on ("views"))
   ("volume-graphics" :depends-on ("misc"
				   "contours" "volumes" "views"
				   "pixel-graphics" "view-graphics"))
   ("point-graphics" :depends-on ("points"
				  "misc" "pixel-graphics"
				  "views" "view-graphics"))
   ("wedge-graphics" :depends-on ("view-graphics" "pixel-graphics" "misc"))
   ("beam-graphics" :depends-on ("beams"
				 "collimators" "contours"
				 "views" "view-graphics"
				 "pixel-graphics" "beam-transforms"
				 "beams-eye-views" "wedges"
				 "wedge-graphics" "misc"))
   ("bev-graphics" :depends-on ("prism-globals"
				"beams" "beam-graphics"
				"beam-transforms" "collimators"
				"views" "view-graphics" "beams-eye-views"
				"wedges" "wedge-graphics"
				"points" "contours" "volumes"
				"point-graphics" "pixel-graphics"))
   ("beam-block-graphics" :depends-on ("prism-globals"
				       "beams" "beam-blocks"
				       "views" "view-graphics"
				       "beams-eye-views" "beam-transforms"
				       "beam-graphics" "bev-graphics"))
   ("brachy-graphics" :depends-on ("brachy"
				   "pixel-graphics" "views" "view-graphics"))
   ("dose-grid-graphics" :depends-on ("dose-grids"
				      "pixel-graphics"
				      "views" "view-graphics"))
   ("isocontour" :depends-on ("misc"))
   ("dose-surface-graphics" :depends-on ("dose-results"
					 "views" "view-graphics"
					 "dose-grids" "isocontour"
					 "pixel-graphics"))
   ("image-graphics" :depends-on ("prism-globals"
				  "misc" "views" "beams" "beams-eye-views"
				  "drr" "beam-transforms"))

   ;; Mediators - relate objects and views.
   ("object-manager" :depends-on ("prism-objects" "views"))
   ("locators" :depends-on ("views" "view-graphics"))
   ("point-mediators" :depends-on ("object-manager" "points" "views"))
   ("volume-mediators" :depends-on ("object-manager" "volumes" "views"))
   ("beam-mediators" :depends-on ("object-manager"
				  "collimators" "beams" "wedges"
				  "beam-blocks" "beam-block-graphics"
				  "views" "beams-eye-views"))
   ("brachy-mediators" :depends-on ("object-manager" "brachy" "views"))
   ("dose-grid-mediators" :depends-on ("object-manager"
				       "dose-grids" "views"
				       "pixel-graphics" "dose-grid-graphics"))
   ("dose-result-mediators" :depends-on ("dose-results" "beams" "brachy"))
   ("dose-view-mediators" :depends-on ("object-manager" "dose-results"))
   ("dose-spec-mediators" :depends-on ("dose-grids"
				       "beams" "volumes" "points"))
   ("image-manager" :depends-on ("prism-globals" "image-graphics" "views"))

   ;; Plans and patients.
   ("plans" :depends-on ("prism-objects"
			 "prism-globals" "misc"
			 "views" "locators" "object-manager"
			 "beams" "beam-mediators"
			 "brachy" "brachy-mediators"
			 "dose-grids" "dose-grid-mediators"
			 "dose-results" "dose-result-mediators"
			 "dose-view-mediators"))
   ("patients" :depends-on ("prism-globals"
			    "misc" "prism-objects" "medical-images"
			    "volumes" "contours" "points"
			    "plans" "image-manager" "object-manager"
			    "dose-spec-mediators" "volume-mediators"
			    "point-mediators"))

   ;; Dose calculation functions.
   ("pathlength" :depends-on ("dosecomp-decls" "volumes"))
   ("clipper" :depends-on ("dosecomp-decls"))
   ("output-factors" :depends-on ("table-lookups" "dose-info"))
   ("beam-dose" :depends-on ("beams"
			     "clipper" "output-factors"
			     "dosecomp-decls" "table-lookups"
			     "therapy-machines" "dose-info"
			     "volumes" "points" "contours"
			     "pathlength" "beam-transforms"
			     "wedges" "beam-blocks" "collimators"
			     "dose-grids" "dose-results"))
   ("quadtree" :depends-on ("pathlength"))
   ("electron-dose" :depends-on ("beams"
				 "therapy-machines" "dose-info"
				 "volumes" "points" "contours"
				 "pathlength" "quadtree" "dosecomp-decls"
				 "beam-transforms" "collimators"
				 "wedges" "beam-blocks"
				 "dose-grids" "dose-results"))
   ("brachy-dose" :depends-on ("brachy"
			       "misc" "brachy-tables" "dose-grids"
			       "points" "dose-results"))
   ("dosecomp" :depends-on ("beams"
			    "brachy" "dose-results"
			    "beam-dose" "electron-dose" "brachy-dose"
			    "plans" "patients"))
   ("spots" :depends-on ("volumes"))
   ("scan" :depends-on ("volumes" "dose-grids" "patients" "plans" "spots"))

   ;; DICOM-RT client support
   ("cstore-status")
   ("imrt-segments" :depends-on ("beams"))
   ("mlc-collimators" :depends-on ("collim-info" "collimators" "mlc"))
   ("dicom-rtplan" :depends-on ("patients" "plans" "beams" "cstore-status"))
   ("dmp-panel" :depends-on ("imrt-segments"))

   ;; Printed output.
   ("charts" :depends-on ("prism-globals"
			  "beams" "wedges" "collimators" "misc"
			  "plans" "patients" "contours" "dosecomp"
			  "dose-results" "therapy-machines"
			  "collim-info" "mlc" "mlc-collimators"
			  "points"))
   ("plots" :depends-on ("prism-globals"
			 "prism-objects" "misc" "plans" "dose-grids"
			 "views" "view-graphics" "beams-eye-views"
			 "dose-results" "dose-surface-graphics"
			 "pixel-graphics"))

   ;; Control panels.
   ("view-panels" :depends-on ("prism-objects"
			       "views" "beams-eye-views" "plots"
			       "tape-measure"))
   ("dvh-panel" :depends-on ("plans" "scan"))
   ("digitizer")
   ("bev-draw-all" :depends-on ("beam-block-graphics"
				"plans" "patients"
				"points" "point-graphics"
				"volumes" "volume-graphics"
				"beams" "beam-graphics"
				"views" "beams-eye-views"))
   ("autocontour")
   ("planar-editor" :depends-on ("prism-globals"
				 "misc" "autocontour" "points"
				 "prism-objects" "digitizer"
				 "pixel-graphics" "tape-measure"))
   ("mlc-panels" :depends-on ("prism-objects"
			      "views" "beams" "beam-blocks"
			      "therapy-machines" "collim-info"
			      "beams-eye-views"
			      "plans" "patients" "bev-draw-all"
			      "mlc" "planar-editor" "view-panels"))
   ("coll-panels" :depends-on ("collimators"
			       "beams" "views" "beams-eye-views"
			       "plans" "patients" "bev-draw-all"
			       "planar-editor" "planar-editor"
			       "mlc-panels" "charts" "volumes"))
   ("selector-panels" :depends-on ("prism-objects"))
   ("beam-block-panels" :depends-on ("prism-objects" "selector-panels"
				     "prism-globals" "attribute-editor"
				     "beams" "beam-blocks"
				     "therapy-machines"
				     "views" "beams-eye-views"
				     "planar-editor" "bev-draw-all"
				     "plans" "patients" "view-panels"))
   ("beam-panels" :depends-on ("prism-globals"
			       "prism-objects" "misc" "selector-panels"
			       "beams" "beam-blocks" "wedges"
			       "therapy-machines" "plans" "patients"
			       "beam-block-panels" "coll-panels"))
   ("brachy-specs-panels" :depends-on ("misc" "brachy-tables" "brachy"))
   ("brachy-coord-panels" :depends-on ("digitizer"
				       "brachy" "brachy-specs-panels"))
   ("brachy-dose-panels" :depends-on ("brachy" "dose-results" "brachy-dose"))
   ("brachy-panels" :depends-on ("prism-objects"
				 "brachy-tables" "brachy" "brachy-graphics"
				 "brachy-specs-panels" "brachy-dose-panels"
				 "brachy-coord-panels"))
   ("point-dose-panels" :depends-on ("prism-objects"
				     "plans" "patients" "points" "beams"
				     "dose-results" "dosecomp" "misc"))
   ("dose-surface-panels" :depends-on ("prism-objects" "dose-results"))

   ;; Patient database functions.
   ("prism-db" :depends-on ("file-functions"
			    "misc" "patients" "plans" "medical-images"))

   ;; The plan panel.
   ("plan-panels" :depends-on ("prism-objects"
			       "prism-globals" "selector-panels"
			       "plans" "patients"
			       "prism-db" "views" "charts"
			       "misc" "object-manager"
			       "dose-grids" "dose-results"
			       "dosecomp" "view-panels" "beam-panels"
			       "dose-surface-panels" "brachy-panels"
			       "point-dose-panels"))

   ;; Special functions for patient panel and subpanels.
   ("attribute-editor" :depends-on ("prism-objects" "volumes" "points"
				    "planar-editor"))
   ("filmstrip" :depends-on ("misc"
			     "contours" "medical-images"
			     "volume-graphics" "view-graphics"))

   ;; new version of PTV tool
   ("inference") ;; the Graham mock prolog code
   ("anatomy-tree" :depends-on ("inference" "file-functions"))
   ("margin-rules" :depends-on ("inference" "anatomy-tree"))
   ("target-volume" :depends-on ("inference"
				 "margin-rules" "contours" "volumes"))
   ("ptvt-expand" :depends-on ("target-volume" "volumes"))

   ("linear-expand" :depends-on ("contours"
				 "volumes" "misc"))
   ("autovolume" :depends-on ("medical-images"
			      "pixel-graphics" "contours" "volumes"
			      "planar-editor" "filmstrip" "autocontour"))
   ("auto-extend-panels" :depends-on ("medical-images"
				      "volumes" "contours" "autovolume"))
   ("volume-editor" :depends-on ("prism-globals"
				 "misc" "prism-objects"
				 "medical-images" "volumes" "contours"
				 "planar-editor" "filmstrip"
				 "selector-panels" "linear-expand"
				 "ptvt-expand" "attribute-editor"
				 "autovolume" "auto-extend-panels"
				 "volume-graphics" "view-graphics"))
   ("patdb-panels" :depends-on ("prism-globals"
				"plans" "patients" "prism-db"))
   ("write-neutron" :depends-on ("prism-globals"
				 "prism-objects" "charts"
				 "misc" "plans" "patients"
				 "beams" "wedges" "collimators"
				 "therapy-machines" "collim-info"
				 "mlc"))
   ("import-structure-sets" :depends-on ("prism-objects"
					 "file-functions" "volumes"
					 "contours" "patients"))
   ("dicom-panel" :depends-on ("prism-objects" "mlc-collimators"
			       "collimators" "therapy-machines"
			       "beams" "wedges" "plans" "patients"
			       "collim-info" "charts" "imrt-segments"
			       "cstore-status" "dmp-panel"))
   ("tools-panel" :depends-on ("prism-globals"))

   ;; The patient panel.
   ("patient-panels" :depends-on ("prism-objects"
				  "prism-globals" "prism-db"
				  "patients" "selector-panels"
				  "volumes" "volume-editor"
				  "plans" "plan-panels" "patdb-panels"
				  "tools-panel" "dvh-panel"))

   ;; The top level.
   ("prism" :depends-on ("prism-globals"
			 "misc" "file-functions" "patients" "plans"
			 "patient-panels" "digitizer" "brachy-tables"
			 "dosecomp" "therapy-machines"))))

;;;-------------------------------------
;;; End.
