;;;
;;; dicom-common.system
;;;
;;; Prism DICOM Server and RTPlan Client System Definition.
;;;
;;; Contains declarations common to Client and Server.
;;;
;;; 21-Dec-2000 BobGian change directories for config file from absolute
;;;   to relative (current) so others can use this more easily in testing.
;;; 26-Dec-2000 BobGian change directories for config file from absolute
;;;   to relative (current) so others can use this more easily in testing.
;;;   Change global names for consistency.
;;; 22-Mar-2001 BobGian add Math package.
;;; 27-Apr-2001 BobGian remove *SERVER-AE-TITLE* - server echoes acceptable
;;;  AE title used by client rather than fixed value from config variable.
;;; 03-May-2001 BobGian set default *MAX-DUMPLEN* to full PDU size.
;;; 09-May-2001 BobGian add RTPlan-Storage-Service to Object-Storage-Services.
;;;   For now, this is a debugging-printout stub for testing Dicom-RTD.
;;; 31-May-2001 BobGian remove Math package.
;;; 07-Sep-2001 BobGian remove redundant package defns - already present
;;;   in other files loaded with rest of Prism.
;;; 31-Dec-2001 BobGian begin modularizing PDS to use DEFSYSTEM and to
;;;   incorporate client as normal part of Prism (not loaded separately).
;;;   Client configuration now comes from "/radonc/prism/prism.config".
;;;   Server configuration still comes from "/radonc/prism/pds.config".
;;; 23-Jan-2002 BobGian add *DICOM-DUMP-FILE* for debugging trace.
;;; 25-Jan-2002 BobGian both Client and Server must bind *CONNECTION-STRM*,
;;;   so its declaration moved here from "dicom-client.system".
;;; 18-Feb-2002 BobGian flush *DICOM-DUMP-FILE*; use standard output instead.
;;;   Used for development/testing only, not production.
;;; 02-Mar-2002 BobGian add dependency of "functions" on "compiler".
;;; 15-Apr-2002 BobGian:
;;;   Remove RTPlan-Storage-Service from Object-Storage-Services.  Never
;;;     used and was erroneous.  Intention was to accept for RTPlans for debug
;;;     printout.  Instead this would have attempted to write them
;;;     as a Prism Image Set.
;;;   Add Structure-Set SOP class as server-handled object.
;;;   Add *Image-Storage-Services* as list of server-handled C-Store Image
;;;    types and *Object-Storage-Services* as similar list of all object types
;;;    (currently all image types, Structure-Sets, and RT-Plans).
;;; 30-Apr-2002 BobGian remove Presentation Context ID - constant #x01.
;;; 04-May-2002 BobGian remove *MAX-DUMPLEN* - use TCP-Bufsize [constant].
;;; 23-Sep-2002 BobGian export PAT-POS (slot in IMAGE class, written to file).
;;; 24-Sep-2002 BobGian add declaration for *DICOM-ALIST*.
;;; 08-May-2003 BobGian - Default log level 2 -> 0.
;;; 28-Aug-2003 I. Kalet remove obsolete PET-2 and PET-3 SOP class UIDs
;;; 21-Sep-2003 BobGian - remove obsolete version number in change log header.
;;; 15-Jul-2004 BobGian: Remove "DI" nickname from DICOM package declaration.
;;; 09-Nov-2004 BobGian - began modularization of server output functionality.
;;; 18-Apr-2005 I. Kalet add in global variables for SSL, per Tung Le.
;;; 20-Jun-2009 I. Kalet move defpackage and all globals to new dicom
;;; file to make independent of defsystem
;;;

;;;=============================================================
;;; Dicom system definition for portion common to Client and Server.

(mk:defsystem :dicom-common
  :source-pathname "dicom/src/"
  :binary-pathname "dicom/bin/"
  :components
  (("dicom")
   ("dictionary")
   ("utilities")
   ("compiler")
   ("parser-rules"    :depends-on ("compiler"))
   ;; "utilities" and "parser-rules" must load before "generator-rules"
   ("generator-rules" :depends-on ("utilities" "compiler" "parser-rules"))
   ("state-rules"     :depends-on ("compiler"))
   ("functions"       :depends-on ("utilities" "compiler"))
   ("generator"       :depends-on ("functions" "utilities"))
   ("parser"          :depends-on ("functions" "utilities"))
   ("actions-common"  :depends-on ("generator" "utilities"))
   ("mainloop"        :depends-on ("actions-common" "parser" "utilities"))
   ))

;;;=============================================================
;;; End.
