;;;
;;; polygons.system
;;;
;;; Some stuff that does polygon arithmetic.
;;;
;;;  1-Apr-1993 I. Kalet taken from ptvt
;;;  5-May-1994 J. Unger add ortho-expand-contour to exports list.
;;; 21-Jul-1994 J. Unger add bounding-box to exports list.
;;; 13-Sep-1994 J. Unger add area-of-triangle and area-of-polygon to
;;; exports.
;;; 23-Sep-1994 J. Unger add perimeter-of-polygon to exports.
;;;  1-Nov-1994 J. Unger add center to exports.
;;;  8-Jan-1995 I. Kalet make just allegro, not different versions,
;;;    also define a *pi-over-180* constant here for use in
;;;    contour-algebra.
;;;  1-Feb-1996 I. Kalet drop make-package, assume defpackage
;;;  2-May-1997 I. Kalet include exports in defpackage, not separate
;;;  2-Jul-1997 BobGian add CLOCKWISE-TRAVERSAL-P in export list.
;;;  3-Jul-1997 BobGian added exports for NEARLY-INCREASING and
;;;    NEARLY-DECREASING (moved here from PRISM system) and for
;;;    IN-BOUNDING-BOX and COLLINEAR (used in PRISM system).
;;;  7-Jul-1997 BobGian added CANONICAL-CONTOUR as replacement for
;;;    REMOVE-ADJACENT-COLLINEAR-VERTICES and
;;;    REMOVE-ADJACENT-REDUNDANT-VERTICES (both present but not exported).
;;; 23-Sep-1997 BobGian removed export for REMOVE-REDUNDANT-VERTICES;
;;;    used only in contour-algebra [in POLYGONS package].
;;;  2-Oct-1997 BobGian remove VERTEX-LIST-UNION - nowhere used.
;;;    Remove export for VERTEX-LIST-INTERSECTION - used only in own package.
;;;    Remove CENTER [renamed POLYCENTER] from export - ditto.
;;;    Remove AREA-OF-TRIANGLE, AREA-OF-POLYGON, PERIMETER-OF-POLYGON.
;;;  7-Oct-1997 BobGian add CONTOUR-ENCLOSES-P (moved PRISM -> POLYGONS).
;;; 03-Feb-2000 BobGian return AREA-OF-TRIANGLE and AREA-OF-POLYGON to
;;;    active duty (and exported) -- used in electron dose calc.
;;; 30-Jul-2003 I. Kalet fix up for new cvs code management
;;;  1-May-2004 I. Kalet new module segments has code from Prism
;;; contour editor, also export simple-polygon
;;; 20-Jun-2009 I. Kalet move defpackage and other stuff out to the
;;; math file to make the files independent of defsystem
;;;

;;;---------------------------------------------

(mk:defsystem :polygons
  :source-pathname "polygons/src/"
  :binary-pathname "polygons/bin/"
  :components
  (("math")
   ("contour-algebra" :depends-on ("math"))
   ("convex-hull" :depends-on ("math"))
   ("segments" :depends-on ("math" "contour-algebra"))
   ))

;;;---------------------------------------------
;;; End.
