;;;
;;; make-prism
;;;
;;; utility or convenience functions for building a standalone prism
;;; executable or a dumped prism image.
;;;
;;; 26-Jun-2009 I. Kalet separated from prism.cl file to avoid loading
;;; unnecessary modules.  Ditto from wrapper-server.
;;;

(in-package :common-lisp-user)

(defpackage "PRISM" (:use "COMMON-LISP"))

(defpackage "DICOM" (:use "COMMON-LISP"))

;;;--------------------------------------

#+allegro
(defun dump-prism-image (&optional (name "prism.dxl"))

  "dump-prism-image is a convenience function for creating a dumped
  lisp image file from the current loaded environment.  If name is not
  provided, the name is prism.dxl."

  ;; Assumes system has already been compiled.
  ;; Compile, load into a fresh Lisp, and then run this function.

  (setf (sys:gsgc-switch :print) nil)
  (setf (sys:gsgc-switch :stats) nil)
  (setf (sys:gsgc-switch :verbose) nil)
  (setq excl:*restart-app-function* 'prism::prism-top-level)
  (excl:gc t)
  (excl:dumplisp :name name)
  (values))

;;;--------------------------------------

#+allegro
(defun build-prism (&optional (dirname "prismsys/"))

  "build-prism is a convenience function to create a set of files that
comprise a standalone Prism system runnable without an installed
Allegro CL system."

  (excl:generate-application
   "prism" dirname
   (append (mk:files-in-system :slik :all :binary)
	   (mk:files-in-system :polygons :all :binary)
	   (mk:files-in-system :dicom-common :all :binary)
	   (mk:files-in-system :dicom-client :all :binary)
	   (mk:files-in-system :prism :all :binary))
   :restart-app-function 'prism::prism-top-level
   :discard-compiler t
   )
  "Standalone Prism system built")

;;;-------------------------------------------------------------

(defun dump-dicom-server (&optional (name "dicom.dxl"))
  ;; Assumes system has already been compiled.
  ;; Compile, load into a fresh Lisp, and then run this function.
  (setf (sys:gsgc-switch :print) nil)
  (setf (sys:gsgc-switch :stats) nil)
  (setf (sys:gsgc-switch :verbose) nil)
  (setq excl:*restart-app-function* 'dicom::run-server)
  (excl:gc t)
  (excl:dumplisp :name name)
  (values))

;;;--------------------------------------

#+allegro
(defun build-dicom (&optional (dirname "dicomsys/"))

  "build-dicom is a convenience function to create a set of files that
comprise a standalone Prism DICOM system runnable without an installed
Allegro CL system."

  (excl:generate-application
   "dicom" dirname
   (append (mk:files-in-system :dicom-common :all :binary)
	   (mk:files-in-system :dicom-server :all :binary))
   :restart-app-function 'dicom::run-server
   :discard-compiler t
   )
  "Standalone Prism DICOM system built")

;;;--------------------------------------
;;; End.
